
Narrative:
In order to don't show the email sign up again,
As a customer without login, 
I want to have a link don't show the email sign up again.

Meta:
@skip

GivenStories: ..\login\basic\access_without_login.story
              
Scenario: Verify the "don't show this again" link will get the email sign up don't show again
When I don't allow the email sign up show again from don't show this again link
Then I can't see the email sign up again
When I refresh the page again
Then I can't see the email sign up again