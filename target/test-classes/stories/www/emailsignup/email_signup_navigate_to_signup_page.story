
Narrative:
In order to navigate to email sign up page,
As a customer without login, 
I want to navigate to email sign up page from deals flyout.

GivenStories: ..\login\basic\access_without_login.story

Scenario: Verify navigate to email sign up page from deals flyout
When I navigate to email sign up page from email button
Then I should see the email sign up page