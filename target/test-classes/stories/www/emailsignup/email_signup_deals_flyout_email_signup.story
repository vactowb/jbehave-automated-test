
Narrative:
In order to save email sign up form deals flyout,
As a customer without login, 
I want to save my email for sign up.

GivenStories: ..\login\basic\access_without_login.story

Scenario: Verify see error message when i don't have a email
When I input without email from deals flyout
Then I should see the email sign up error message

Scenario: Verify see save email successful message from deals flyout
When I input email from deals flyout
Then I should see the thank you for signing up message