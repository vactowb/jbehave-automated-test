Narrative:
In order to be taken back to where i came from 
As a user, i click on the continue shopping link on the ATC lightbox
I want to apply new continue shopping logic.[WWWCU-26405]

Meta:
@username od_automation_continueshopping#test.com
@password tester


Scenario: Verify ink-toner-and-ribbons url apply new continue shopping logic

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_ink_and_toner_categories.story

When I chose one brand and go to sku details page
!-- Then I should see sku detail page for the selected brand
When I search sku from the header: "315515"
When I add the sku to cart from sku details page and continue shopping
Then I should see sku detail page for the search product detail

