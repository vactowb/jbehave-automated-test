
Meta:
@username od_automation_changestore#lt.com
@password Test1234

Scenario: Verify to OMAX Stores and Pick Up Flag Modal 
Meta:
@skip
GivenStories: ..\..\login\basic\login_od_account.story
              
When I search sku from the header: "common_sku"
And I add the sku to cart from sku details page
Then I should see the sku "common_sku" is in Delivery mode
When I select the Pickup mode and only for the sku "common_sku"
Then I should see the stores inventory is not available online
