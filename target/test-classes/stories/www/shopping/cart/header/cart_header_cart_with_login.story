
Narrative:
In order to easily manage my cart,
As a customer login, 
I want to see the header cat to show in the new header design.

Meta:
@username od_it_tester
@password tester

GivenStories: ..\basic\cart_operation.story

Scenario: Verify to show items' list
When I request to view items' list from header cart
Then I should see items' list in header cart

Scenario: Verify view cart link from header cart 
When I navigate to shopping cart page from header cart flyout view all link
Then I should see shopping cart page

Scenario: Verify view cart and checkout link from header cart 
When I navigate to view cart and checkout page from header cart
Then I should see view cart and checkout page in header cart


