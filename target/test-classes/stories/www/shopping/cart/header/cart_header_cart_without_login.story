
Narrative:
In order to easily manage my cart,
As a customer without login, 
I want to see the header cat to show in the new header design.

GivenStories: ..\..\..\login\basic\access_without_login.story

Scenario: Verify to view header cart show no items information 
When I request to view no items information without login from header cart
Then I should see no items information in header cart

Scenario: Verify pick-up check box from header cart 
Meta:
@skip
!-- pick up move to mix model
When I navigate to choose a store page from header cart
Then I should see choose a store page

Scenario: Verify delivery check box from header cart 
Meta:
@skip
!-- pick up move to mix model
When I navigate to shopping cart page from delivery box
Then I should see shopping cart page

Scenario: Verify view cart link from header cart 
Meta:
@skip
!-- pick up move to mix model
When I navigate to shopping cart page from header cart
Then I should see shopping cart page
