Meta:
@username od_automation_freedeliverycpd#lt.com
@password tester

Scenario: Verify "Add $XX For Free Delivery" message is not getting displayed under subtotal section on Shopping Cart Page for cpd item[WWWCU-29700]
GivenStories: ..\..\login\basic\login_od_account.story
              
When I view my order history
And I request to reorder for the order "333722098-001"
When I request to view the shopping cart from find your product page
Then I should see free delivery message is not displayed under subtotal section
When I search sku from the header: "common_sku"
When I add the sku to cart from sku details page
Then I should see the sku "common_sku" is in Delivery mode
Then I should see free delivery message is displayed under subtotal section




