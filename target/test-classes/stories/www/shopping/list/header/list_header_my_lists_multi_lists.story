
Narrative:
In order to easily manage my online shopping lists,
As a customer, 
I want to see My Lists to show in the new header design.

Meta: 
@username od_automation_4
@password tester

GivenStories: ..\..\..\login\basic\login_od_account.story

Scenario: Verify to view lists from my lists

When I request to view "My Lists" from header
Then I should see my shopping lists in My Lists view


Scenario: Verify to view all lists from my lists

When I request to view "My Lists" from header
And I request to view all my lists from My Lists view
Then I should see all my shopping lists in shopping lists page


Scenario: Verify the list item number from my lists
Meta:
@manual
When I request to view "My Lists" from header
Then I should see the item number for each list is displayed normally