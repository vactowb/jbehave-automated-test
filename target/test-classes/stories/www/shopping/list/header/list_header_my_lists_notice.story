
Narrative:
In order to easily manage my online shopping lists,
As a customer, 
I want to see My Lists to show in the new header design.


Scenario: Verify to view my list without login

GivenStories: ..\..\..\login\basic\access_without_login.story
When I request to view "My Lists" from header
Then I should be noticed that I need to log in or create an account


Scenario: Verify to create account from my lists view

When I request to view "My Lists" from header
And I create an account from my lists view
Then I should navigate to customer registration page


Scenario: Verify to log in from my lists view

When I request to view "My Lists" from header
And I login from my lists view
Then I should see account login dialog


Scenario: Verify to view my lists via no saved item for my account
Meta:
@username no_saved_items
@password tester

GivenStories: ..\..\..\login\basic\login_od_account.story
Given I have no saved items
When I request to view "My Lists" from header
Then I should be noticed that I have no saved items



