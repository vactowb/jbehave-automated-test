Meta:
@username od_automation_4
@password tester
@skuid 315515

Scenario: Verify that the share list container will be hidden when there is no sku in Personal Shopping List
GivenStories: ..\..\login\basic\login_od_account.story,
              ..\basic\search_for_common_sku.story
When I add the sku to my list
And I add sku to an existed personal shopping list
Then I should see added success message
