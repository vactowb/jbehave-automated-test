Meta:
@username od_it_tester
@password tester

Narrative:
In order to delete my list from my lists page,
As a customer with login, 
I want to delete one list of my lists.

Scenario: Verify to delete my list

GivenStories: ..\..\..\login\basic\login_od_account.story

When I navigate to my lists page
Then I should see my lists page
When I choose a list and edit it
And I delete the list
Then I should see the confirm to delete list page
When I confirm to delete list
Then I should see the school list disappear from my lists page
