
Scenario: Verify to create a school supply list

GivenStories: ..\..\..\login\basic\login_od_account.story,
              ..\..\basic\search_for_common_sku.story
When I add the sku to my list
Then I should see Choose or Start New List Dialog
When I Create a new List in Choose or Start New List Dialog
Then I should see start a new list dialog
When I Create New School List in Start New List Dialog
Then I should see edit list page
When I save the changes to school list
Then I should see share list container


Scenario: Verify that user could preview the created tppc list normally

When I preview the TPPC List
Then I should see the preview shared tppc list page

Scenario: Verify nagivate to the school list detail page from my lists page

When I go back to edit my list
And I navigate to view all my lists
And I view and edit my list
Then I should see my school list details page