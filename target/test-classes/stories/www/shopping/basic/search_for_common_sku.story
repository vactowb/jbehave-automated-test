Meta:
@basic

Scenario: Verify to search by sku id

When I search for SKU with the given SKU Id
Then I should see sku detail page for the given SKU
