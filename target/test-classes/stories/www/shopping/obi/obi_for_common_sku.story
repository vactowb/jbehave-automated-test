Scenario: Load sku using order by item page
Given I am an OD customer
When I enter SKU with the given SKU Id "315515" on obi page
Then I should see sku info loaded for the given SKU "315515" on the obi page
When I enter SKU with the given SKU Id "300315515" on obi page
Then I should see sku info loaded for the given SKU "300315515" on the obi page
When I enter SKU with the given SKU Id "9981523" on obi page
Then I should see sku info loaded for the given SKU "9981523" on the obi page
When I enter SKU with the given SKU Id "3009981523" on obi page
Then I should see sku info loaded for the given SKU "3009981523" on the obi page


