Scenario: Load sku detail page based on entering sku id in search box
Given I am an OD customer
When I search for SKU with the given SKU Id "315515" from home page
Then I should see sku detail page for the given SKU "315515"
When I search for SKU with the given SKU Id "300315515" from home page
Then I should see sku detail page for the given SKU "300315515"
When I search for SKU with the given SKU Id "9981523" from home page
Then I should see sku detail page for the given SKU "9981523"
When I search for SKU with the given SKU Id "3009981523" from home page
Then I should see sku detail page for the given SKU "3009981523"


