GivenStories:..\login\basic\open_lost_password_page.story

Scenario: Verify forgot login functionality with not input email address

When I retrieve login name with:
	|email |firstname |lastname |
	|null  |odtest    |odtest   |
Then I should see the warn message when login: "Please enter an Email Address."

Scenario: Verify forgot login functionality with not input firstname and lastname

When I retrieve login name with:
	|email            |firstname |lastname |
	|odtester@126.com |null      |null     |
Then I should see the warn message when login: "Please enter a First Name."
And I should see the warn message when login: "Please enter a Last Name."
And The page should not blank out the field in which email address was entered: "odtester@126.com"


Scenario: Verify forgot login functionality with wrong email address

When I retrieve login name with:
	|email                |firstname |lastname |
	|odtesterwrong@126.com|odtest    |odtest   |
Then I should see the warn message when login: "There are no Users matching the Email Address and FirstName/LastName."


Scenario: Verify forgot login functionality with right name and email address
!-- auto_reid
When I retrieve login name with:
	|email            |firstname |lastname |
	|test@officedepot.com  |reid    |tester   |
Then I should see the successful message when login "Email Successfully Sent"
When I login use the accout:
	|loginname                       |password |
	|auto_forgot_loginname3@test.com |tester   |
Then I should login successfully from header

