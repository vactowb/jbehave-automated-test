
Narrative:
In order to know if store pick-up is available for OfficeMax and Office Depot stores,
As a customer with login, 
I want to see the store pick-up link show in the new header design.

Meta:
@username suo.lu#officedepot.com
@password tester
@skip

GivenStories: ..\..\login\basic\login_od_account.story
Scenario: Verify occur store pick-up not avaliable text
When I open stores flyout from header stores
Then I should see store pick-up not available text