
Narrative:
In order to easily manage stores,
As a customer with login, 
I want to see the header stores to show in the new header design.

Meta:
@username od_automation_2
@password Tester1234

GivenStories: ..\..\login\basic\login_od_account.story

Scenario: Verify header flyout show "Your Store" text after login successfully
When I open stores flyout from header stores
Then I should see the "My Store" text
When I navigate to the local store page from my store
Then I should navigate on local store page

Scenario: Verify navigate to find a store page from "Change Store" link
When I navigate to find a store page from change my store link
Then I should see find a store page

Scenario: Verify navigate to store details page from "Store Details" link
When I navigate to store details page from store details link
Then I should navigate on local store page

Scenario: Verify navigate to store details page from "Map It" link
When I navigate to store details page from map it link
Then I should see store details page

Scenario: Verify find a store from "Find A Store" input box
When I navigate to find a store page from find a store input box
Then I should see find a store page

Scenario: Verify find a store from "More Search Options" input box
When I navigate to find a store page from more search options link
Then I should see find a store page

Scenario: Verify navigate to one hour store page from "1 Hour Store Pickup" link
When I navigate to one hour store page from one hour store pickup link
Then I should see one hour store page

Scenario: Verify to go to weekly ad from weekly ad link
When I go to shop weekly ad from strore weekly ad link
Then I should navigate to weekly ad page from store

Scenario: Verify navigate to find a store page from header stores
When I navigate to find a store page from header stores
Then I should see find a store page
