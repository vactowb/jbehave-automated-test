
Narrative:
In order to easily manage local store brand service,
As a customer with login, 
I want to see the store brand service from local store page.

Meta:
@username od_automation_2
@password Tester1234

Scenario: Verify navigate to see store brand service from local store page
GivenStories: ..\..\login\basic\login_od_account.story,
			  basic\stores_navigate_to_local_store_page.story
Then I should see the store brand service