
Narrative:
In order to easily manage bread scrumb,
As a customer with login, 
I want to see the bread scrumb from local store page.

Meta:
@username od_automation_2
@password Tester1234

Scenario: Verify navigate to find a store page from local store bread scrumb
GivenStories: ..\..\login\basic\login_od_account.story,
			  basic\stores_navigate_to_local_store_page.story
When I navigate to find a store page from local store bread scrumb
Then I should see find a store page

Scenario: Verify navigate to all states page from local store bread scrumb
GivenStories: basic\stores_navigate_to_local_store_page.story
When I navigate to all states page from local store bread scrumb
Then I should see all states page