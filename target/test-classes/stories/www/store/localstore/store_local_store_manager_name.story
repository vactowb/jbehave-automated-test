
Narrative:
In order to easily find store manager's name,
As a customer with login, 
I want to see the store manager's name on local store page.

Meta:
@username od_automation_2
@password Tester1234

Scenario: Verify navigate to find a store page from local store bread scrumb
GivenStories: ..\..\login\basic\login_od_account.story,
			  basic\stores_navigate_to_local_store_page.story
Then I should see store manager's name