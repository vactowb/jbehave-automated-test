
Narrative:
In order to easily manage find more stores,
As a customer with login, 
I want to see the store details page from find more stores.

Meta:
@username od_automation_2
@password Tester1234

Scenario: Verify navigate to store details page from find another store link
GivenStories: ..\..\login\basic\login_od_account.story,
			  basic\stores_navigate_to_local_store_page.story
When I navigate to store details page from find another store link
Then I should see store details page

Scenario: Verify navigate to store details page from search more stores
GivenStories: basic\stores_navigate_to_local_store_page.story
When I navigate to store details page from search more stores
Then I should see find a store page