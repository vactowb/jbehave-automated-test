
Narrative:
In order to easily navigate to find a store link page,
As a customer without login, 
I want to navigate to find a store link page.

Scenario: Verify navigate to find a store link page from View All Office Depot Locations link
GivenStories: ..\..\login\basic\access_without_login.story,
			           basic\store_navigate_to_find_a_store_page.story
When I navigate to all states page from view all office depot locations link
Then I should see all states page
When I navigate to the first state city page
Then I shoud see city page
When I navigate to the first city store page
Then I should see store link page