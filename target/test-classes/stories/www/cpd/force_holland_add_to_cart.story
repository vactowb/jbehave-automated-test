
Narrative:
In order to add a Holland item to cart
As a WWW customer
I want to add the included configured item to cart
					 
Scenario:  Append the provided to the url and execute the url to add the configured vendor item to cart.
Given I access WWW site
When I use the vendorString "holland" and configId "BNV1-AD950F25" to build the vendor add to cart url
Then the vendor item "797793" is added to cart