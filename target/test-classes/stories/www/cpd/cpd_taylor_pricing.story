Scenario: varify lbl price for taylor sku

GivenStories: ..\login\basic\access_without_login.story

When I search sku from the header: "505815"
Then I should navigate to product details page
When I customize sku in Product Details page
Then I should see price in "Taylor" configurator and compare it to the sku price