Scenario: Verify BCC sku to validate the price, but we may only be able to validate price > 0 

GivenStories: ..\login\basic\access_without_login.story

When I search sku from the header: "264980 "
When I customize the cpd sku
When I select one the bcc sku item
Then I should see the "bcc" price and price greater than zero