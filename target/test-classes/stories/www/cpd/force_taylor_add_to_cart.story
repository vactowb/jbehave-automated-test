
Narrative:
In order to add a Holland item to cart
As a WWW customer
I want to add the included configured item to cart
			
Meta:
@username od_add_cpd_checkout3
@password Test1234

Scenario:  Append the provided to the url and execute the url to add the configured vendor item to cart.
GivenStories: ..\login\basic\login_od_account.story		 
When I use the vendorString "taylor" and configId "DIV2-349C5E9F" to build the vendor add to cart url
Then I should see the sku "914829" is in Delivery mode

Scenario: Step 3 of Checkout, Receive the Alert message "Cash / Check payment options not available for pickup orders containing configurable / customizable items.' With use of Credit card. 
When I select the Pickup mode for one sku "914829"
Then I should see the sku "914829" is in pickup mode
When I checkout from shopping cart
When I continue to checkout at checkout second step
Then I should see message telling me that I cannot use cash check payment option