Narrative:
In order to verify USPS Stamp Fee function
As a WWW customer
I want to purchase USPS Stamp
 
Meta:
@username od_automation_fee
@password tester
@skuid 898782
 
 
Scenario: Verify basic USPS Stamp Fee progress

GivenStories: ..\login\basic\login_od_account.story,
			  ..\shopping\basic\search_for_common_sku.story
When I add the sku to cart with quantity : "1"
Then I should view the shopping cart
And I should see stamp sku with fee description in my shopping cart:
  | Number        | Fee Description                                | Fee Number    |
  | Item #  898782 | $1.00 PROCESSING FEE WILL BE ADDED AT CHECKOUT | Item # 357914 |
When I checkout from shopping cart
Then I should see fee sku in order details:
  | Description                           | 
  | POSTAGE PROCESSING FEE| 
When I continue to checkout at checkout second step
And I continue to checkout at checkout third step
And I place order at checkout fourth step
Then I see order palced successful
