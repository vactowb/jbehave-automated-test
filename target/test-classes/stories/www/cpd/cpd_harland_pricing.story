Scenario: Verify price for Harland Clarke product

GivenStories: ..\login\basic\access_without_login.story

When I search sku from the header: "777630"
Then I should navigate to product details page
When I customize sku in Product Details page
Then I should see price in "Harland" configurator and compare it to the sku price
