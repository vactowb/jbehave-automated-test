Scenario: Verify POD sku to validate the price, but we may only be able to validate price > 0 [CPD-2792]

GivenStories: ..\login\basic\access_without_login.story

When I search sku from the header: "870284"
When I customize the cpd sku
Then I should see the "pod" price and price greater than zero