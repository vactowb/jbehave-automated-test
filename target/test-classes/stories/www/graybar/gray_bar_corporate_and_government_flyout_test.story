
Narrative:
In order to easily navigate to OD bussiness solution and OM workplace page,
As a customer without login, 
I want to have a place in the gray bar header flyout.


GivenStories: ..\login\basic\access_without_login.story

Meta:
@skip

Scenario: Verify navigate to office depot BSD home page from gray bar flyout
When I navigate to office depot BSD home page from gray bar flyout
Then I should see the BSD home page

Scenario: Verify navigate to officemax workplace page successful from gray bar flyout
When I navigate to officemax workplace page from gray bar flyout
Then I should see the officemax workplace page