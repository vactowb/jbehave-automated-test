
Narrative:
In order to easily open od live chat window,
As a customer without login, 
I want to have a place in the gray bar header occur live chat window.

GivenStories: ..\login\basic\access_without_login.story
Meta:
@skip

Scenario: Verify open live chat window successful from gray bar live chat link
When I open live chat window from gray bar live chat link
Then I should see the chat now window from need help place