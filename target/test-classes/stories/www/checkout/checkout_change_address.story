Meta:
@username od_it_change_address#test.com
@password tester

Scenario: Verify to change/Add address button on checkout page 2 

GivenStories: ..\login\basic\login_od_account.story
When I trigger composite steps to add the common sku to shopping cart
When I change address at checkout second step
Then I should see change shipping information page
When I add other shipping address to checkout
|FirstName|LastName|Address    |City          |State|
|tester |od |6600 N MILITARY TRL # 6|BOCA RATON|FL - Florida  | 
And I continue to checkout at checkout second step
And I continue to checkout at checkout third step
And I place order at checkout fourth step
Then I see order palced successful


Scenario: Verify to Add/Change address button on my account overview page

When I open my account overview page
When I navigate to Shipping Address section
When I delete the shipto address
Then I should see the shipping address was disappear

Scenario: Verify to change/Add address button on checkout page 4 

GivenStories: ..\login\basic\access_make_sure_logout.story,..\login\basic\login_od_account.story
When I trigger composite steps to add the common sku to shopping cart
And I continue to checkout at checkout second step
And I continue to checkout at checkout third step
When I change address at checkout fourth step
Then I should see change shipping information page
When I select some address from shipping information page
Then I should go to checkout second step page
