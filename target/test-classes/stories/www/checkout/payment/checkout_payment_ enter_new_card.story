Meta:
@username od_auto_enter_new_card#test.com
@password tester

Scenario: As a user I would like CVV tool tip to display a simplified information on where to find CVV

GivenStories: ..\..\login\basic\login_od_account.story

When I trigger composite steps to add sku to shopping cart: "common_sku"
And I checkout from shopping cart
And I continue to checkout at checkout second step
When I change payment at checkout third step
When I enter a new card at checkout third step
Then I should see blank card input field
When I continue to checkout at checkout third step
Then I should see the error message credit card needed
When I change payment at checkout third step
When I enter a new card at checkout third step
When I choose to pay by given credit card
When I continue to checkout at checkout third step without input CVV
When I place order at checkout fourth step
Then I see order palced successful