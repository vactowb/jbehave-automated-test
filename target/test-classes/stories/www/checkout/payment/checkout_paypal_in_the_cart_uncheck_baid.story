Meta:
@username checkout_delete_baid#test.com
@password tester
@skuid 315515
!-- WWWCU-31127
Scenario: Verify to be able to uncheck the Save BAID check box and then have the PayPal method work for my current purchase, but not save it for my next visit so that the Payment is not saved. 
Meta:
@wip
GivenStories: ..\..\login\basic\login_od_account.story, 
              ..\..\shopping\basic\search_for_common_sku.story
When I add the sku to cart from sku details page
And I punch out to PayPal from shopping cart
Then I should return the page at checkout fourth step
When I uncheck the Save BAID check box at checkout fourth step
When I place order at checkout fourth step
Then I should see the order Details

When I trigger composite steps to add sku to shopping cart: "common_sku"
And I punch out to PayPal from shopping cart
Then I should see the PayPal page opened successful
When Log in the paypal with:
	|Username                |Password|
	|odtester@officedepot.com|odtester|
Then I should return the page at checkout fourth step
When I place order at checkout fourth step
Then I see order palced successful

