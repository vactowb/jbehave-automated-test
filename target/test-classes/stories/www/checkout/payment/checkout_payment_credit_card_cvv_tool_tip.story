Meta:
@username od_automation_cvv#test.com
@password tester

Scenario: As a user I would like CVV tool tip to display a simplified information on where to find CVV

GivenStories: ..\..\login\basic\login_od_account.story

When I trigger composite steps to add sku to shopping cart: "common_sku"
And I checkout from shopping cart
And I continue to checkout at checkout second step
When I open cvv tool tip
Then I should see cvv tool tip display amex and visa images

When I close the cvv tool tip
When I choose to pay by credit card in amex
When I open cvv tool tip
Then I should see cvv tool tip display amex image

When I close the cvv tool tip
When I choose to pay by given credit card
When I open cvv tool tip
Then I should see cvv tool tip display visa image
When I close the cvv tool tip