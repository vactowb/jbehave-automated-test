!-- WWWCU-28934
!-- Verify to re-auth If customer punches out to PP and has a saved BAID then comes back to step 4 and changes their shipping address 
!-- username od_card_drop_baid#lt.com || od_paypal_drop_baid#lt.com
Meta:
@username od_card_drop_baid#lt.com
@password tester

Scenario: Verify to re-auth If customer changes their shipping address at step 4

GivenStories: ..\..\login\basic\login_od_account.story,
				      ..\..\account\basic\account_add_od_credit_card.story,
				      ..\..\login\basic\access_make_sure_logout.story,
				      ..\..\login\basic\login_od_account.story

When I search sku from the header: "common_sku"
When I add the sku to cart from sku details page
When I checkout from shopping cart
When I continue to checkout at checkout second step
When I continue to checkout at checkout third step
When I edit the shipping address at checkout fourth step
Then I should get a message on step second to re-auth the od card
When I continue to checkout at checkout second step
Then I should see the payment information is blank at checkout third step


Scenario: Verify to re-auth If customer punches out to PP and has a saved BAID then comes back to step 4 and changes their shipping address [WWWCU-28934]
Meta:
@manual
GivenStories: ..\..\login\basic\login_od_account.story

When I search sku from the header: "common_sku"
When I add the sku to cart from sku details page
When I punch out to PayPal from shopping cart
Then I should see the PayPal page opened successful
When Log in the paypal with:
	|Username                |Password|
	|odtester@officedepot.com|odtester|
Then I should return the page at checkout fourth step
When I save my payment paypal billing information
When I place order at checkout fourth step
Then I see order palced successful

When I search sku from the header: "common_sku"
When I add the sku to cart from sku details page
When I punch out to PayPal from shopping cart
Then I should return the page at checkout fourth step
When I edit the shipping address at checkout fourth step
Then I should get a message on step second to re-auth the od card
When I continue to checkout at checkout second step
Then I should see the payment information is blank at checkout third step
