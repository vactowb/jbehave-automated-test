Meta:
@username od_automation_payment2
@password Test1234

Scenario: Verify to check out with payment of Cash

GivenStories: ..\basic\checkout_with_pickup_mode_mixed_mode.story

When I continue to checkout at checkout second step
And I choose to pay by: "Cash"
And I continue to checkout at checkout third step
Then I should see payment method is "Cash"
When I go back to shopping cart page
Then I should view the shopping cart
When I select the Delivery mode for the sku "common_sku"
Then I should see the sku "common_sku" is in Delivery mode
When I checkout from shopping cart
When I continue to checkout at checkout second step
And I continue to checkout at checkout third step
Then I should see the warning message payment information is required