Meta:
@username od_add_coupon
@password tester

Scenario: Verify to subscribed item from the Shopping cart for this to be reproduced

GivenStories: ..\login\basic\login_od_account.story
When I trigger composite steps to add sku to shopping cart: "404321"
When I add the sku to be subscribed
Then I should see the sku should be subscribed

Scenario: Verify to add any other SKU to cart
When I add item "common_sku" from the order
Then I see the item "common_sku" has been added to cart
Then I should see the item "common_sku" should not be subscribed

Scenario: Verify to delete a item from the order
When I delete the first item from the order
Then I see the item "404321" has been deleted

Scenario: Verify to add any other SKU to cart
When I add item "315-515" from the order
Then I see the item "315-515" has been added to cart

Scenario: Verify to add any other SKU to cart out of stock
Meta:
@skip
When I add item "339873" order by item "9999"
Then I should see Available & Backorder