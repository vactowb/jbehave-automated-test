Meta:
@username od_automation_storelocator
@password tester

Scenario: Verify to  to search for stores from the store locator modal from checkout step 2

GivenStories: basic\checkout_with_pickup_mode_mixed_mode.story

When I search for stores from the store locator modal at checkout second step
Then I should see the stores displayed
When I select store from the search result
Then I should see the Pickup info changed at checkout second step