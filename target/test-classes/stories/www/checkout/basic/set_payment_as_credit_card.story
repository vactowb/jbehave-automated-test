Meta:
@basic

Scenario: Set credit card as the default payment

When I navigate to My Account Overview
And I edit my payment information
And I update my payment method as "Credit Card"
And I manage my credit cards
And I set my default credit card
Then I should see my default card set successfully
