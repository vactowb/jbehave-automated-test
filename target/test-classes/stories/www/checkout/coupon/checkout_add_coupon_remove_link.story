
Narrative:
In order to easily navigate to coupon details page from coupons box,
As a customer with login, 
I want to have a coupon link from the coupon box.

Meta:
@username auto_reid#test.com
@password tester
@skuid 315515
@coupon 703389784
@skip

Scenario: Verify delete coupon from remove link
GivenStories: basic\checkout_add_coupon.story
When I remove current coupon from remove link
Then I should remove current coupon
