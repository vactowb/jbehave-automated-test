Narrative:
In order to easily navigate to coupon page for coupons with free or discounted items,
As a customer without login, 
I want to have a button from the added coupon.

Meta:
@username coupon_2a_same#tester.com
@password Test1234
@skuid 315515
@coupon 77315190
@skip

GivenStories: basic\checkout_add_coupon_navigate_to_coupon_details_page.story

Scenario: Verify add item to cart when I selected one item
When I select a item and add to cart by first add to cart button
Then I should see the item add to cart successful

