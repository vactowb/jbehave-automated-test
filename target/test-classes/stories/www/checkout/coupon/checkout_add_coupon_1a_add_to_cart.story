
Narrative:
In order to easily add product to shopping cart,
As a customer without login, 
I want to have a button on coupon details page.

Meta:
@username od_coupon_1a#tester.com
@password Test1234
@skuid 315515
@coupon 19233241
@skip

GivenStories: basic\checkout_add_coupon_navigate_to_coupon_details_page.story

Scenario: Verify add product to shopping cart from add to cart button
When I add coupon details page product to shopping cart from add to cart button
Then I should stay on coupon details page

Scenario: Verify add product to shopping cart successfully
When I navigate to shopping cart page from header cart
Then I should see the item add to cart successful