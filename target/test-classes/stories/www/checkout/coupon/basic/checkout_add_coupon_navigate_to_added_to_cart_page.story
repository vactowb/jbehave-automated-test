
Narrative:
In order to easily navigate to coupon page for coupons with free or discounted items,
As a customer without login, 
I want to have a button from the added coupon.
Meta:
@basic

GivenStories: checkout_add_coupon.story

Scenario: Verify navigate to coupon added to cart page by see offer details button
When I navigate to coupon added to cart page from see offer details button
Then I should see the coupon added to cart page