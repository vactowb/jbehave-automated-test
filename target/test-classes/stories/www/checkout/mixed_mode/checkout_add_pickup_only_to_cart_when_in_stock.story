Narrative:
In order to add a pick-up only item to the shopping cart if it is not available in my selected store
As a user, 
I want to be able to choose a store on the Product Detail (SKU) page.[WWWCU-24598]

Meta:
@username od_automation_pickuponly#lt.com
@password tester

!-- pick-up only sku 105159
Scenario: Verify to be able to choose a store on the Product Detail (SKU) page if it is pick up only
GivenStories: ..\..\login\basic\login_od_account.story
              
When I trigger composite steps to add pickup sku: "pickup_only_sku"
Then I should view pop the store locator modal
When I choose a store on the Product Detail page
Then I should see find your product page



