
Narrative:
In order to prompt you to set up security questions from checkout,
As a customer with login, 
I want to have a prompt set up security questions window from checkout.

Meta:
@username auto_reid
@password tester

GivenStories: ..\login\basic\login_od_account.story
         
Scenario: Verify prompt set up security questions window occur from checkout
Meta:
@manual
When I trigger composite steps to add the common sku to shopping cart
And I continue to checkout at checkout second step
And I continue to checkout at checkout third step
And I place order at checkout fourth step
Then I should see set up security questions window


Scenario: Verify don't set up now successful
Meta:
@manual
When I choose not set up now
Then I see order palced successful


Scenario: Verify to set up security questions successful
Meta:
@manual
When set up security questions
Then I should see security question has been saved window

