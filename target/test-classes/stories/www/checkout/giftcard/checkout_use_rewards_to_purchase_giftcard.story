Meta:
@username user_www
@password tester
@skuid 836880 

Scenario: Verify Do not allow user to use a rewards certificate to purchase a gift card
Meta:
@manual

GivenStories: ..\..\login\basic\login_od_account.story,
              ..\..\shopping\basic\search_for_common_sku.story
When I add the sku to cart from sku details page
And I add the gift card to shopping cart 
Then I should view the shopping cart
And I checkout from shopping cart
And I continue to checkout at checkout second step
And I select one Depot Rewards at checkout third step
And I continue to checkout at checkout third step
And I place order at checkout fourth step
Then I return to checkout second step with a warn message: "Certificates can not be applied to orders containing a gift card."

Scenario: Verify to link odr account  from my account page
Meta:
@manual

When I open My Account Page
And I link odr account
Then I should view the account has been linked
When I unlink the odr account
Then I should view the account has been unlinked