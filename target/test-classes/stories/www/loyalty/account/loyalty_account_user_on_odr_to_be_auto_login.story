Narrative:
In order to no one else can redeem my rewards,
As a customer without login, 
I want to my loyalty account not to be linked to more than one www account.[WWWCU-24323]

Meta:
@username od_automation_odraccount#test.com
@password Tester1234

Scenario: Verify Member number lookup link on ODR linking modal
GivenStories: ..\..\login\basic\login_od_account.story
When I open my account overview page
When I click add account on the my account page
When I add rewards account from look up my odr member id use phone number "7215412142"
Then I should see phone number is invalid when add rewards account
When I add rewards account from look up my odr member id use phone number "7205565823"
Then I should see member ID "1903071221" has been filled when add rewards account

Scenario: Verify my loyalty account not to be linked to more than one www account.
!-- WWWCU-31312
When I open my account overview page
When I click add account on the my account page
Then I should see message notifying the customer of single sign in conversion rules
When I link odr account with the given number: "111"
Then I should see error message invalid rewards number
When I link odr account with the given number: "1808710246"
Then I should see to choose the delivery method for your registration code

Scenario: Verify my loyalty account can be linked to more than one www account.
Meta:
@manual
When I open my account overview page
When I click add account on the my account page
When I link odr account with the given number: "1807964463"
Then I should see error message odr accout is already linked

Scenario: Verify Bring Loyalty Segment into www
Meta:
@manual
GivenStories: ..\..\login\basic\access_without_login.story
When I open my account overview page
Then Loyalty section on my account page will show either Green, Blue or Yellow based on what segment the user belongs to

Scenario: Verify  to be auto logged in to ODR.com from od My Account Overview page.

GivenStories: ..\..\login\basic\access_without_login.story
Meta:
@manual
When I open my account overview page
And I open the odr website use the link Office Depot Rewards
Then I should see the odr account information who is already linked and secured

Scenario: Verify  to be auto logged in to ODR.com from od My Account Overview page use another account.

GivenStories: ..\..\login\basic\access_without_login.story
Meta:
@manual
@username user_www
@password tester
When I open my account overview page
And I open the odr website use the link 'Office Depot Rewards'
Then I should see the odr account information  who is already linked and secured