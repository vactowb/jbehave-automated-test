!-- WWWCU-31311

Scenario: Login Authentication with member id
GivenStories: ..\basic\open_rewards_welcome_page.story
When I login from office depot rewards page
Then I should see the login rewards lightbox show
When I enter the needed id and password
|Username          |Password|
|1807964463	|tester  |
Then I should login successfully from loyalty login

Scenario: Verify Member Id Lookup

GivenStories: ..\..\login\basic\access_make_sure_logout.story,
						..\basic\open_rewards_welcome_page.story
When I login from office depot rewards page
Then I should see the login rewards lightbox show
When I login rewards from look up my odr member id with phone number "7215412142"
Then I should see the forgotLoy error message phone number is invalid when active rewards
When I login rewards from look up my odr member id with phone number "7205565823"
Then I should notice the member ID "1903071221" has been filled at account login page

!-- When I register from office depot rewards page
!-- Then I should see the register with rewards page shown

Scenario: Verify Member Id tropo validation
Meta:
@skip
GivenStories: ..\basic\open_rewards_welcome_page.story
When I login from office depot rewards page
Then I should see the login rewards lightbox show
When I enter the needed id and password
|Username          |Password|
|1808709537	|null  |

Then I should see verify your identity lightbox
Then I should see choose the delivery method for your registration code
When I choose the delivery method for your registration code
!-- Then I should see registration code has been sent to the following address
!-- When I enter invalid tropo registration code
!-- Then I should see registration code input invalid
!-- Then I should see user information display at ssi page
!-- Then I should see member number display at ssi page
!-- Then I should see warning message phone number is linked

Scenario: Verify Activate Member Id with new account

GivenStories: ..\basic\open_rewards_welcome_page.story
When I login from office depot rewards page
Then I should see the login rewards lightbox show
When I enter the needed id and password
|Username          |Password|
|1891901785	|null  |
When I activate the rewards with new account
When I continue to join rewards
Then I should notice that membership type is filled in "Personal"

Scenario: Verify Activate Member Id with given account

GivenStories: ..\basic\open_rewards_welcome_page.story
When I login from office depot rewards page
Then I should see the login rewards lightbox show
When I enter the needed id and password
|Username          |Password|
|1800016451	|null  |
When I activate the rewards with given account
When I login to link office depot account
|Username          |Password|
|auto_test_reward@test.com|tester|
Then I should see the error loyalty already associated to account


Scenario: Login Authentication Verify
Meta:
@skip
GivenStories: 	..\..\login\basic\access_make_sure_logout.story,
					 	..\basic\open_rewards_welcome_page.story

When I login from office depot rewards page
Then I should see the login rewards lightbox show
When I enter the needed id and password
|Username          |Password|
|auto_reid@test.com	|tester  |
!-- Then I should notice activate rewards account successfuly
Then I should login successfully from header
