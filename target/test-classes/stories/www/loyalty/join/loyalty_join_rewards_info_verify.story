!-- WWWCU-31309
Scenario: Join office depot rewards with new OD and  new ODR Enrollment

GivenStories: ..\basic\open_rewards_welcome_page.story

When I join the rewards with new od account
When I continue to join rewards
Then I should notice that all required fields must be filled out

Scenario: Verify input wrong confirm password and create loyalty account fail
When I input all the needed information with wrong confirm password
Then I should notice that the passwords do not match
Then I should notice that the password is not meet mini requirement
Then I should notice that the phone number is in use

Scenario: Verify input invalid email address and create loyalty account fail
When I input all the needed information with invalid email "it@itab"
Then I should notice that the email address is invalid


Scenario: Verify input used email address and create loyalty account fail
When I input all the needed information with invalid email "hao.chen2@officedepot.com"
Then I should notice that the email has already in used

Scenario: Verify input invalid phone number and create loyalty account fail
When I input all the needed information with invalid phone number
Then I should notice that the phone number is invalid

Scenario: Verify input invalid zip code and create loyalty account fail
When I input all the needed information with invalid zipcode
Then I should notice that the zip code number is invalid

Scenario: Verify input all information and create loyalty account succ
Meta:
@wip
When I input all the needed information create loyalty account
Then I should see a welcome message on my account overview page
Then I should see a member number on my account overview page
