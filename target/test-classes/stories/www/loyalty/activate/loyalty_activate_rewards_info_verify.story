!-- WWWCU-31310
Scenario: Active office depot rewards with new OD and  new ODR Enrollment

GivenStories: ..\basic\open_rewards_welcome_page.story
When I activate the rewards with the member id "22222"
Then I should see that the member id you entered is not valid

Scenario: Verify Member Id Lookup
When I active rewards from look up my odr member id use phone number "7215412142"
Then I should see the forgotLoy error message phone number is invalid when active rewards
When I active rewards from look up my odr member id use phone number "5614254125"
Then I should see the forgotLoy error message phone number is userd when active rewards
When I active rewards from look up my odr member id use phone number "7205565823"
!-- Then I should see verify your identity lightbox
!-- Then I should see choose the delivery method for your registration code
Then I should notice the member ID "1903071221" has been filled

Scenario: Active office depot rewards with new OD and new ODR Enrollment
Meta:
@skip
GivenStories: ..\basic\open_rewards_welcome_page.story
When I activate the rewards with the member id "1800016410"
When I activate the rewards with new account
Then I should notice that membership type is filled in "Customer"

Scenario: Active office depot rewards with new OD and exist ODR Enrollment

GivenStories: ..\basic\open_rewards_welcome_page.story
When I activate the rewards with the member id "1807964463"
Then I should see that the member id you entered can not used online

Scenario: Active office depot rewards with new OD and exist ODR Enrollment
Meta:
@skip
GivenStories: ..\basic\open_rewards_welcome_page.story
When I activate the rewards with the member id "1800016261"
When I activate the rewards with given account
When I login to link office depot account
|Username          |Password|
|auto_test_reward@test.com|tester|
Then I should see the error loyalty already associated to account

!-- Then I should notice activate rewards account successfuly