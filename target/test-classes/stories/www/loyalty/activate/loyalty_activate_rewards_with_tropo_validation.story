Scenario: Active office depot rewards with successful TROPO validation.

GivenStories: ..\basic\open_rewards_welcome_page.story
Meta:
@skip
When I activate the rewards with the member id "1808710246"
Then I should see verify your identity lightbox
Then I should see choose the delivery method for your registration code
When I choose the delivery method for your registration code
Then I should see registration code has been sent to the following address
When I enter invalid tropo registration code
Then I should see registration code input invalid
