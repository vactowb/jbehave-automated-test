
Narrative:
In order to open the first advertisement page from hero slider,
As a customer without login, 
I want to have a learn more link in the first hero slider.

GivenStories: ..\..\login\basic\access_without_login.story
 Meta:
 @skip

Scenario: Verify navigate to the first advertisement page from hero slider
When I navigate to the first advertisement page from learn more link
Then I should see the first advertisement page

