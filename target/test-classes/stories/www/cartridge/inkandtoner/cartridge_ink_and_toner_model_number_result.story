
Narrative:
In order to see the model number result page easy,
As a customer without login, 
I want to have a model link from the brand specific page.


GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_ink_and_toner_categories.story

Scenario: Verify find all first brands from "Your Ink&Toner finder" select a printer brand successfully
When I select the first printer brand from select printer brand drop down
Then I should see all first brands

Scenario: Verify finde all "calculator" brands from "Your Ink&Toner finder" select a printer type successfully
When I navigate to the model number result page from model link
Then I should see the model number result page

