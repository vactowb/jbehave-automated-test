
Narrative:
In order to easily navigate to coupon page,
As a customer without login, 
I want to have a place in the deal center page navigate to coupon page.

!-- Verify the MainColumnContent_Header in N=5+549384

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_deals.story

Scenario: Verify navigate to coupon page by shop current coupon offers link
When I navigate to coupon center page from shop current coupon offers link
Then I should see the coupon center page
