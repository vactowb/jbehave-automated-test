
Narrative:
In order to easily navigate to product details page,
As a customer without login, 
I want to have a place in the adlist carousel navigate to product details page.

!-- Verify the MainColumnContent_AdListCarousel categories in N=5+549384

GivenStories: ..\..\deals\basic\navigate_to_deal_center_page.story

Meta:
@skip

Scenario: Verify navigate to product details page by adlist carousel
When I navigate to product details page from adlist carousel
Then I should see the product details page from adlist carousel

Scenario: Verify change sku shown by adlist carousel
Meta:
@manual
When I change next four skus from adlist carousel
Then I should see next four skus from adlist carousel
When I change previous four skus from adlist carousel
Then I should see previous four skus from adlist carousel