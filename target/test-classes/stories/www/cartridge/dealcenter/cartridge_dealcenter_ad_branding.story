
Narrative:
In order to easily navigate to product details page,
As a customer without login, 
I want to have a place in the adBranding navigate to product details page.

!-- Verify the MasterColumnContent_DealCenterAdBranding categories in N=5+549383

Meta:
@skip

GivenStories: ..\..\deals\basic\navigate_to_deal_center_page.story

Scenario: Verify navigate to product details page by deal center adbranding
When I navigate to categories page from deal center adBranding
Then I should see the home page