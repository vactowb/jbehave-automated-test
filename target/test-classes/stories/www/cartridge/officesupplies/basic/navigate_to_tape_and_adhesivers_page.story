
Narrative:
In order to easily navigate to tape and adhesives categories page,
As a customer without login, 
I want to easily navigate to tape and adhesives categories page.

Meta:
@basic

GivenStories: ../../../login/basic/access_without_login.story

Scenario: Verify navigate to tape and adhesives categories page by url
When I access tape and adhesives page
Then I should see the tape and adhesives categories page