
Narrative:
In order to easily navigate to customer service page,
As a customer without login, 
I want to have a need help place navigate to customer service page.

!-- Verify the NavColumnContent_CategoryNeedHelp in N=5+1886

Scenario: Verify navigate to customer service page by need help place
GivenStories: ..\..\topnavigation\flyouts\topnavigation_officesupplies_flyout_navigate_to_basic_supplies_page.story
When I navigate to customer service page from need help place
Then I should see the customer service page from need help place

Scenario: Verify popup chat now window by chat now link
GivenStories: ..\..\topnavigation\flyouts\topnavigation_officesupplies_flyout_navigate_to_basic_supplies_page.story
Meta:
@skip
When I open chat now window from need help place
Then I should see the chat now window from need help place