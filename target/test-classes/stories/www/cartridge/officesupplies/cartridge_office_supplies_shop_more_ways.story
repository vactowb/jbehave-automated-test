
Narrative:
In order to easily navigate to different product page by shop more ways,
As a customer without login, 
I want to have two links navigate to different product page.

!-- Verify the NavColumnContent_ShopMoreWays categories in N=5+1886

Meta:
@skip

Scenario: Verify navigate to private brand page by office depot brand products link
GivenStories: ..\..\topnavigation\flyouts\topnavigation_officesupplies_flyout_navigate_to_basic_supplies_page.story
When I navigate to private brand page from office depot brand products link
Then I should see the private brand page

Scenario: Verify navigate to greener office page by greener office products link
GivenStories: ..\..\topnavigation\flyouts\topnavigation_officesupplies_flyout_navigate_to_basic_supplies_page.story
When I navigate to greener office page from greener office products link
Then I should see the private brand page


