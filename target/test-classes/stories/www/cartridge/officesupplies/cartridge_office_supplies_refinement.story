
Narrative:
In order to easily navigate to computer and  tablets categories page from url,
As a customer without login, 
I want to filter the computer and  tablets catridge by refinement.

!-- Verify the NavColumnContent_Refinement categories in N=5+509612+549384

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to computer and  tablets categories page from url
When I navigate to computer and  tablets categories page from url from url
Then I should see the computer and  tablets categories page

Scenario: Verify filter the  computer and  tablets categories by refinement
When I filter the  computer and  tablets categories by the first type
Then I should see the first type computer and  tablets categories page

Scenario: Verify filter the  computer and  tablets categories by refinement
When I filter the  computer and  tablets categories by the first brand
Then I should see the first brand computer and  tablets categories page

Scenario: Verify cancel currnet filter by X mark
Meta:
@skip
When I cancel currnet filter by X mark
Then I should see the first type computer and  tablets categories page

