
Narrative:
In order to see price in cart,
As a customer without login, 
I want to see price in cart.

GivenStories: ..\..\login\basic\access_without_login.story
Meta:
@skip

Scenario: Verify see price in cart successfully
When I access inkjet search page from url
Then I should see inkjet search page
When I open add your price window and keep it in my cart
Then I should see consider related items page
When I continue it to my cart
Then I should see the item add to cart successful