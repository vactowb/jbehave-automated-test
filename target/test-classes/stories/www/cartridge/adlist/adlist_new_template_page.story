
Narrative:
In order to test new template page,
As a customer without login, 
I want to see the new template page.

Scenario: Verify navigate to new template page
GivenStories: ..\..\login\basic\access_without_login.story

When I navigate to new template page from URL
Then I should see new template page
When I navigate to sku details page from shop me button
!-- Then I should see sku details page
Then I should see the cotegories page