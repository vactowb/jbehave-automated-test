
Scenario: click on one link "copy-and-multipurpose-paper" from the main menu, you should go to related category page
Meta:
@product pilot pen

Given I access WWW site
When I search for the given product
Then I should see the results of the given product
When I request to compare with "4" products selected
Then I should go to the product comparison page


Scenario: Verify to go to product comparison with no items selected
Meta:
@product Colored Paper

When I search for the given product
Then I should see the results of the given product
When I request to compare with no product selected
Then I should go to the product comparison page