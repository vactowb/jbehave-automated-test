
Scenario: Verify to register account

GivenStories: register_basic.story
When I register with email "<email>" and all the other required information
Then I should return to my account overview page

Examples:
|email|
|od_automation|
|od_automation_1|
|od_automation_2|
|od_automation_3|
|od_automation_4|
|od_automation_5|
|od_automation_6|
|od_automation_7|
|od_automation_8|
|od_automation_12|
|auto_automation_odr#od.com|
|auto_reid|
|od_it_mixedmode|
|od_it_mixedmode_1|
|od_automation_payment|
|checkout_test_withid|
|checkout_test|
|od_add_coupon|
|od_automation_storelocator|
|auto_forgot_loginname3|
|od_auto_forgot_pass#od.com|
|od_automation_pickuponly#lt.com|
|od_automation_orderhistory#test.com|
|od_automation_suppressstore#lt.com|
|od_automation_taxexempt#lt.com|
|od_automation_backorder#lt.com|
|no_saved_items|
|one_list|
|odtester@126.com|

