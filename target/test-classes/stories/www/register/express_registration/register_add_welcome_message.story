Narrative:
In order to be welcomed after I complete registration,
As a customer without login, 
I want to add a welcome massage at the top of the my account page, after user completes registration from the homepage.[WWWCU-24322]

Scenario: Verify Add Welcome message when create a new account

GivenStories: ..\basic\register_with_all_the_required_information.story

Then I should see a welcome message on my account overview page

Scenario: Verify to change login setting
Meta:
@manual
When I logout from header account
When I login by google account with the following account:
|Username          |Password|
|depotodtester@gmail.com|testertester  |
When I re-login with google account authorization
Then I should login successfully from header
When I navigate to My Account Overview
When I navigate to login setting page
Then I should open the login setting page
When I change the login name at login setting page
When I logout from header account
When I login by google account with the following account:
|Username          |Password|
|depotodtester1@gmail.com|testertester  |
Then I should login fault from Gigay with error message displayed