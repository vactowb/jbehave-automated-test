
Narrative:
In order to easily see discontinued sku page from not available sku,
As a customer without login, 
I want to have a not available sku.

Meta:
@skuid 119296

GivenStories: ..\login\basic\access_without_login.story

Scenario: Verify navigate to discontinued sku page from not available sku
When I search for SKU with the given SKU Id
Then I should see the discontinued sku page

Scenario: Verify navigate to available sku page from the first product alternate
When I navigate to available sku page from the first product alternate
Then I should see the sku detail page
