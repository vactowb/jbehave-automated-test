Narrative:
In order to verify Password Strength Or Minimum Requirements of Password to be valid
As a WWW Account user, if i create new user with invalid password, 
I want to see error message for invalid password

!-- This story need to be released in may'14 so skipping it as of now
Scenario: verify Password Strength Or Minimum Requirements of Password to be valid

GivenStories: ..\checkout\basic\checkout_with_anonymous.story
When I fill all mandatory info at checkout second step
And I continue to checkout at checkout second step
Then I should navigate to checkout third step
When I choose to pay by given credit card
And I continue to checkout at checkout third step without input CVV
When I place order at checkout fourth step
Then I should see the order Details
When I enter invalid password not meet mini requirement
Then I should see error message for password not meet mini requirement