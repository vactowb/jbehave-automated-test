Narrative:
In order to verify Password Strength Or Minimum Requirements of Password to be valid
As a WWW Account user, if i create new user with invalid password, 
I want to see error message for invalid password


Scenario: verify Password Strength Or Minimum Requirements of Password to be valid
Meta:
@username od_automation_2
@password Tester1234

GivenStories: ..\login\basic\login_od_account.story
When I navigate to my account login page
Then I should see my account login page
When I change login settings with password not meet mini requirement
Then I should see error message for password not meet mini requirement