Meta:
@username od_automation_orderhistory#test.com
@password tester

Scenario: Verify to have an expanded and collapsed view of my order history so that I can get a quick view to the information I need
GivenStories: ..\..\login\basic\login_od_account.story

When I search sku from the header: "315515"
And I add the sku to cart from sku details page
And I checkout from shopping cart
And I continue to checkout at checkout second step
And I continue to checkout at checkout third step
And I place order at checkout fourth step
Then I see order palced successful
And I should see the order Details
And I check the order history with the item "315515"

Scenario: Verify to view the record of order history
When I view my order history
And I search orders with "All" status criteria in showing all orders tab
And I choose one of the order history
Then I could see the details of the order history record

Scenario: Verify to cancel an order
When I cancel the order history
Then I should see the order history is cancelled

Scenario: Verify to search by Order Number
When I view my order history
And I search by order number
Then I could see the details of the order history record

Scenario: Verify to view order with specific id
Meta:
@manual
When I view order history detail with a specific order id : "314506131"
Then I see the detail for a Holland Order