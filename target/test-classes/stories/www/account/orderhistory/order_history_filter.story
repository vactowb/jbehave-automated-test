Meta:
@username od_automation_orderhistory#test.com
@password tester

GivenStories: ..\..\login\basic\login_od_account.story

Scenario: Verify the search status function
When I view my order history
And I search orders with "Cancelled" status criteria in showing all orders tab
Then I see the corresponding "Cancelled" status search results

Scenario: Verify the search date function
When I view my order history
And I search orders with date criteria in showing all orders tab
Then I see the corresponding date search results

Scenario: Verify the search by order type
When I view my order history
And I search orders by order type:"Store Purchase"
Then I see the date search results empty

Scenario: Verify the search by order type cpd
When I view my order history
And I search orders by order type:"Copy Print Depot"
Then I see the date search results empty

Scenario: Verify the search by order type cpd
When I view my order history
When I view order history detail with order number: "041281684"
Then I see the date search results empty
When I request to show all items
Then I should see navigate to Showing all items section

