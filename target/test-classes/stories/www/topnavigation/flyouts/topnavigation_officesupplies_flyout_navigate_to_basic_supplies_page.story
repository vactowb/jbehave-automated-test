
Narrative:
In order to easily navigate to basic supplies page,
As a customer without login, 
I want to have a place in the new top navigate fast link to basic supplies page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify office supplies flyout text link navigate to the Basic Supplies categories
When I navigate to basic supplies categories page frome office supplies flyout text link
Then I should see the basic supplies categories page
!-- Verify the MasterColumnContent_CategoryTopSeo categories N=5+1886