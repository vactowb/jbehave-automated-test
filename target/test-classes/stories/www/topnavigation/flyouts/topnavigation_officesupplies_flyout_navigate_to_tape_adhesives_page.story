
Narrative:
In order to easily navigate to tape and adhesives page,
As a customer without login, 
I want to have a place in the new top navigate fast link to tape and adhesives page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify office supplies flyout text link navigate to the tape and adhesives categories page
Meta:
@skip
When I navigate to tape and adhesives page frome office supplies flyout text link
Then I should see the tape and adhesives categories page
