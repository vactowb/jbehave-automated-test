
Narrative:
In order to easily navigate to school supplies page,
As a customer without login, 
I want to have a place in the top navigate fast link to school supplies information page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify shop by grade link navigate to school grades page successfull
Meta:
@manual
When I navigate to school grades page from shop by grade link
Then I should see school grades page

Scenario: Verify top categories link successfull navigate to school supplies page
Meta:
@manual
When I navigate to school supplies page from top categories link
Then I should see school supplies page

