
Narrative:
In order to easily navigate to technology categories page,
As a customer without login, 
I want to have a place in the top navigate fast link to technology categories page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to technology categories page seccessful from technology navigation
When I navigate to technology categories page from technology top navigation
Then I should see technology categories page