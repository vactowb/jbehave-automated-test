
Narrative:
In order to easily navigate to paper categories page,
As a customer without login, 
I want to have a place in the top navigate fast link to paper categories page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to paper categories page seccessful from paper top navigation
When I navigate to paper categories page from paper top navigation
Then I should see paper categories page