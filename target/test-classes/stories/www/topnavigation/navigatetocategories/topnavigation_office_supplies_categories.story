
Narrative:
In order to easily navigate to office supplies categories page,
As a customer without login, 
I want to have a place in the top navigate fast link to office supplies categories page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to office supplies categories page seccessful from office supplies top navigation
When I navigate to office supplies categories page from office supplies top navigation
Then I should see office supplies categories page