
Narrative:
In order to easily navigate to furniture page,
As a customer without login, 
I want to have a place in the top navigate fast link to furniture page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to cleaning page seccessful from furniture top navigation
When I navigate to furniture page from furniture top navigation
Then I should see the furniture page