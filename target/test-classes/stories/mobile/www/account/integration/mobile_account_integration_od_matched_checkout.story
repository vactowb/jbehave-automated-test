!-- ODNA-10851

Meta:
@username HFIELDS#GREENWOOD.NET
@password tester1


GivenStories: ..\..\checkout\basic\mobile_anonymous_checkout_from_cart.story

Scenario: Verify to checkout with OD matched account

When I login from checkout options with the given account
Then I should navigate on shopping cart page
When I process check out from shopping cart
And I request to place order
Then I should see merge accounts section is displayed on thank you page


Scenario: Verify to merge accounts from thank you page

When I request to merge accouts from thank you page
Then I should navigate on merge accounts options page


Scenario: Verify to keep seperate from merge accounts options
Meta:
@manual
When I choose to keep my accounts seperate from merge accounts options page
Then I should see the home page of mobile site


Scenario: Verify to cancel merge account

When I choose to merge my accounts from merge accounts options page
Then I should navigate on verify account page of merge accounts
!-- Manual
!-- And I should noticed that I am required to specify a OMX account