
Meta:
@username TEST-KEITHSAARELA#YAHOO.COM
@password test1


Scenario: Verify to merge my accounts

GivenStories: ..\..\login\basic\mobile_login_with_od_omx_matched.story
When I choose to merge my accounts from merge accounts options page
Then I should navigate on verify account page of merge accounts
!-- Manual
!-- And I should noticed that I am required to specify a OD account 
When I cancel to verify account from verify account page of merge accounts
Then I should navigate on merge accounts options page


Scenario: Verify to merge account with invalid OD account

When I choose to merge my accounts from merge accounts options page
And I login with the following account to merge:
     | Field    | Value     |
     | Username | RANDOM 10 |
     | Password | RANDOM 10 |
Then I should be noticed that the login account is invalid


Scenario: Verify to merge account with valid OD account
Meta:
@manual
When I login with the following account to merge:
     | Field    | Value     |
     | Username |           |
     | Password |           |
Then I should navigate on welcome page