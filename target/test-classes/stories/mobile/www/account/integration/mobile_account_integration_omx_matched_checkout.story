!-- ODNA-14241

Meta:
@username TEST-KEITHSAARELA#YAHOO.COM
@password test1
@skuid 315515


Scenario: Verify to checkout with OMX matched account

GivenStories: ..\..\checkout\basic\mobile_anonymous_checkout_from_cart.story
When I login from checkout options with the given account
Then I should navigate on merge accounts options page


Scenario: Verify to keep seperate from merge accounts options

When I choose to keep my accounts seperate from merge accounts options page
Then I should navigate on registration page
And I should be noticed to create OD account to access your OMX history and account information
And I should see all fields are pre-filled by OMX account info on registration page
And I should not see the ODR member number field section on page


Scenario: Verify to cancel merge account

GivenStories: ..\..\checkout\basic\mobile_anonymous_checkout_from_cart.story
When I login from checkout options with the given account
And I choose to merge my accounts from merge accounts options page
Then I should navigate on verify account page of merge accounts
!-- Manual
!-- And I should noticed that I am required to specify a OD account 


!-- Defect ODNA-14277
Scenario: Verify to back to cart from merge account page

When I navigate to cart from header
Then I should navigate on shopping cart page
And I should only see home and login options on main menu