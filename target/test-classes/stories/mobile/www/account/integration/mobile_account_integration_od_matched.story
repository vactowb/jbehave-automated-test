!-- ODNA-8440

Meta:
@username HFIELDS#GREENWOOD.NET
@password tester1


Scenario: Verify to keep my accounts seperate
Meta:
@manual
GivenStories: ..\..\login\basic\mobile_login_with_od_omx_matched.story
When I choose to keep my accounts seperate from merge accounts options page
Then I should see the home page of mobile site


Scenario: Verify to cancel merge account

GivenStories: ..\..\login\basic\mobile_login_with_od_omx_matched.story
When I choose to merge my accounts from merge accounts options page
Then I should navigate on verify account page of merge accounts
!-- Manual
!-- And I should noticed that I am required to specify a OMX account 
When I cancel to verify account from verify account page of merge accounts
Then I should navigate on merge accounts options page


Scenario: Verify to merge account with invalid OMX account

When I choose to merge my accounts from merge accounts options page
And I login with the following account to merge:
     | Field    | Value     |
     | Username | RANDOM 10 |
     | Password | RANDOM 10 |
Then I should be noticed that the login account is invalid


Scenario: Verify to merge account with valid OMX account
Meta:
@manual
When I choose to merge my accounts from merge accounts options page
And I login with the following account to merge:
     | Field    | Value     |
     | Username |           |
     | Password |           |
Then I should navigate on welcome page


Scenario: Verify to merge account from my profile

When I access home page of www mobile site
And I go manage my account
And I navigate to my profile page
And I choose to combine OMX account from my profile page
Then I should navigate on verify account page of merge accounts
