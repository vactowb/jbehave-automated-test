!-- ODNA-8442, ODNA-8444

Meta:
@username TEST-KEITHSAARELA#YAHOO.COM
@password test1


Scenario: Verify to keep my accounts seperate

GivenStories: ..\..\login\basic\mobile_login_with_od_omx_matched.story
When I choose to keep my accounts seperate from merge accounts options page
Then I should navigate on registration page
And I should be noticed to create OD account to access your OMX history and account information
And I should see all fields are pre-filled by OMX account info on registration page
And I should not see the ODR member number field section on page


Scenario: Verify to create account with invalid zip code

When I register with the following info:
	| Field    | Value |
    | Zip Code | abc12 |
Then I should be noticed that zip code is invalid


Scenario: Verify to create account with existing email

When I register with the following info:
	| Field    | Value              	   |
	| Zip Code | 33496                     |
    | Email    | hao.chen2@officedepot.com |
Then I should be noticed that email already in used


Scenario: Verify to create account with valid info
Meta:
@manual
When I create OD account with pre-filled OMX info
Then I should navigate on welcome page