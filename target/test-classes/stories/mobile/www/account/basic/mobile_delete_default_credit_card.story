Meta:
@basic

Scenario: Verify to delete default credit card

GivenStories: mobile_goto_my_account.story
When I choose to edit my payment information
And I edit the default credit card
And I delete this credit card
Then I should see the credit card has been successfully deleted