Meta:
@basic

Scenario: Verify to empty cart
When I access home page of www mobile site
And I navigate to cart from header
And I empty my cart
Then I should see my cart is empty
