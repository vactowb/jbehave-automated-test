Meta:
@username od_it_tester
@password tester
@skuid 315515

GivenStories: ..\login\basic\mobile_normal_login.story


Scenario: Verify to view cart form header

When I navigate to cart from header
Then I should navigate on shopping cart page


Scenario: Verify update the quntity of sku

GivenStories: basic\mobile_add_sku_to_cart.story
When I update the qty of no. "1" item to "2"
Then I should see the qty of no. "1" item is updated


Scenario: Verify remove the sku item
When I remove the no. "1" item
Then I should see the item has been removed


Scenario: Verify save sku for later

GivenStories: basic\mobile_add_sku_to_cart.story
When I save the no. "1" item for later
Then I should see the item has been saved


Scenario: Verify continue shopping at cart page

GivenStories: basic\mobile_add_sku_to_cart.story
When I request to continue shopping
Then I should navigate on choose category page


Scenario: Verify the checkout buttons

When I navigate to cart from header
Then I should see two checkout buttons on page


Scenario: Verify the checkout button if there's only one sku in cart

GivenStories: basic\mobile_empty_cart.story,
              basic\mobile_add_sku_to_cart.story
When I access home page of www mobile site
And I navigate to cart from header
Then I should see only one checkout button on page