Scenario: Verify invalid information
Meta:
@manual

GivenStories: ..\login\basic\mobile_without_login.story
When I request to find store from home page
And I provide the zipcode: "abc123"
And I find stores
Then I should see the find stores button is disabled


Scenario: Verfiy to get store list by zip code

GivenStories: ..\login\basic\mobile_without_login.story
When I request to find store from home page
Then I should navigate on store locator page
When I provide the zipcode: "33496"
And I find stores
Then I should see the store list which near "33496"


Scenario: Verfiy to get store list by city and state

When I request to find store from header
Then I should navigate on store locator page
When I provide city and state: "boca raton, FL"
And I find stores
Then I should see the store list which near "boca raton"


Scenario: Verify to go to weekly ads from store list

When I view weekly ads of the first store
Then I should navigate on weekly ads page


Scenario: Verify find story by current location
Meta:
@manual
GivenStories: ..\login\basic\mobile_without_login.story
When I request to find store from home page
Then I should navigate on store locator page
When I find a stor by current location
Then I can get the store at current location



