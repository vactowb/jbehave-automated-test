!-- WWWMOBILE-4131

Narrative:
In order to always have it handy,
As a mobile site user, 
I want to be able to access my store information in an easy way. 

Meta:
@username od_it_tester
@password tester


Scenario: Verify to set as my store without login

GivenStories: ..\login\basic\mobile_without_login.story
When I request to find store from header
And I provide the zipcode: "33496"
And I find stores
And I set a store as my store
Then I should navigate on store details page
And I should be noticed that the store has been successfully set as the default store


Scenario: Verfiy to set as my store via login user

GivenStories: ..\login\basic\mobile_normal_login.story
When I access home page of www mobile site
And I request to find store from home page
And I provide the zipcode: "33496"
And I find stores
And I set a store as my store
Then I should navigate on store details page
And I should be noticed that the store has been successfully set as the default store


Scenario: Verify to view my store from my account

When I access home page of www mobile site
And I go manage my account
And I choose to view my store from my account
Then I should navigate on store details page
And I shoulde see my store is the selected store before
And I should be noticed this store is my store


Scenario: Verify to view my store from store list

When I request to find store from header
And I provide the zipcode: "33496"
And I find stores
And I check details of my store from stores near list
Then I should navigate on store details page
And I should be noticed this store is my store


Scenario: Verify to change my store

When I request to change my store from store details page
Then I should navigate on store locator page
When I provide the zipcode: "33496"
And I find stores
And I check details of the first store which is not my store
Then I should navigate on store details page
When I set the store as my store from store details page
Then I should be noticed that the store has been successfully set as the default store


Scenario: Verfiy to get map for store
Meta:
@manual
When I view on google maps from store details page
Then I should navigate on google maps


Scenario: Verify to view my store from my account via no store selected before
Meta:
@manual
GivenStories: ..\login\basic\mobile_normal_login.story
Given My account has no store selected before
When I go manage my account
And I choose to view my store from my account
Then I should navigate on store locator page