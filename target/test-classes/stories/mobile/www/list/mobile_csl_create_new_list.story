!-- ODNA-8879

Meta:
@username od_automation_csl
@password Tester1234
@skuid 315515
@wip


Scenario: Verify to cancel create new shopping list via logged in

GivenStories: ..\login\basic\mobile_normal_login.story,
			  ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
And I request to create a new list for the added sku from select shopping list lighting box
Then I should navigate on create shopping list page
When I cancel to create shopping list from create shopping list page
!-- Then I should see sku details page of the given sku


Scenario: Verify to create new shopping list via logged in with invalid info

GivenStories: ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
And I request to create a new list for the added sku from select shopping list lighting box
And I create a new shopping list with no info input from create shopping list page
Then I should be noticed that list name is required from create shopping list page


Scenario: Verify to create new shopping list via logged in with valid info

When I create a new shopping list from create shopping list page
Then I should navigate on the shopping list details page


Scenario: Verify to create new list from select shopping list page with invalid info

GivenStories: ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
And I request to show more lists from select shopping list lighting box
!-- And I create a new shopping list with no info input from select shopping list page
!-- Then I should be noticed that list name is required from select shopping list page


Scenario: Verify to create new list from select shopping list page with valid info
Meta:
@skip
When I create a new shopping list from select shopping list page
Then I should navigate on the shopping list details page