Meta:
@username od_automation_4
@password tester

Scenario: Verify to delete all shopping list

GivenStories: ..\..\login\basic\mobile_normal_login.story
When I access home page of www mobile site
And I go manage my account
And I request view my shopping list from my account
Then I should see the shopping list page
When I delete all csl list
Then I see the no list prompt