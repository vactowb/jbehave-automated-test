!-- ODNA-8879

Meta:
@username od_automation_no_csl
@password Tester1234
@skuid 315515
@wip

Scenario: Verify to login with no list account

GivenStories: ..\login\basic\mobile_without_login.story,
			  ..\sku\basic\mobile_search_for_sku.story
Given I am a www mobile customer with the given account
When I add the sku to list
Then I should navigate on login page
When I login with my account
Then I should navigate on create new list page


Scenario: Verify to cancel create new list via no list account
Meta:
@skip
When I cancel to create a new shopping list with no info input from create new list page
Then I should see sku details page of the given sku


Scenario: Verify to create new list with invalid info via no list account

When I create a new shopping list with no info input from create new list page
Then I should be noticed that list name is required from create new list page


Scenario: Verify to create new list with valid info via no list account
Meta:
@manual
When I create a new shopping list and set as default from create new list page
Then I should navigate on the shopping list details page


Scenario: Verify to cancel create new list via user with no shopping list

GivenStories: ..\register\basic\mobile_express_register_basic.story,
			  ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
Then I should see create new list lighting box
When I cancel to create shopping list from create new list lighting box
Then I should see sku details page of the given sku


Scenario: Verify to create new list with no info input via user with no shopping list
Meta:
@skip
When I add the sku to list
And I create a new shopping list with no info input from create new list lighting box
Then I should be noticed that list name is required from create new list lighting box


Scenario: Verify to create new list with no info input via user with no shopping list

When I add the sku to list
When I create a new shopping list and set as default from create new list lighting box
Then I should navigate on the shopping list details page


Scenario: Verify the default list

GivenStories: ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
Then I should see the default list which is expected from select shopping list lighting box