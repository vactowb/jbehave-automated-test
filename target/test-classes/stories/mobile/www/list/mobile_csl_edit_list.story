!-- ODNA-8879

Meta:
@username od_automation_csl
@password Tester1234
@skuid 315515
@wip


Scenario: Verify to update shopping list name and comments

GivenStories: ..\login\basic\mobile_normal_login.story,
			  ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
And I add the sku to a existing list and set it as default from select shopping list lighting box
Then I should navigate on the shopping list details page
When I request to edit the shopping list
Then I should see the edit shopping list lighting box
When I update the list with new name and comments
Then I should see the list name has been changed as expected


Scenario: Verify to update the reminder of shopping list

When I request to edit the shopping list
And I update the reminder date of shopping list
Then I should see the reminder has been updated


Scenario: Verify to cancel quick add in shopping list

When I request to quick order
Then I should see the quick order entry lighting box
When I cancel to quick order entry
Then I should navigate on the shopping list details page


Scenario: Verify to quick add in shopping list

When I request to quick order
And I quick add sku "315515" with qty "10" into list
Then I should see the sku has been added


Scenario: Verify to remove with no entry selected

When I request to remove with no entry selected
Then I should be noticed that to select at least one entry


Scenario: Verify to remove entry from list

When I request to quick order
And I quick add sku "315515" with qty "10" into list
And I request to remove with a entry from list
Then I should see the entry has been removed


Scenario: Verify to add to cart with no entry selected

When I request to add to cart with no entry selected
Then I should be noticed that to select at least one entry


Scenario: Verify to add the skus of list into cart

When I add all the entries of list into cart
Then I should navigate on shopping cart page