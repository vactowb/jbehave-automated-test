!-- ODNA-4900, ODNA-4893, ODNA-4896, ODNA-4890

Meta:
@skuid 315515

GivenStories: ..\login\basic\mobile_without_login.story


Scenario: Verify to view description

GivenStories: basic\mobile_search_for_sku.story
When I request to view sku descritpion
Then I should navigate on sku description page
When I add the sku to cart from description page
Then I should navigate on shopping cart page


Scenario: Verify to view product details

GivenStories: basic\mobile_search_for_sku.story
When I request to view sku product details
Then I should navigate on the expected sku product details page
!-- Manual verification of product details
When I back to sku details from breadcrumb of sku product details page
Then I should see sku details page of the given sku
When I request to view sku product details
And I back to sku details from sku icon of sku product details page
Then I should see sku details page of the given sku
When I request to view sku product details
And I add the sku to cart from product details page
Then I should navigate on shopping cart page


Scenario: Verify to veiw reviews

GivenStories: basic\mobile_search_for_sku.story
When I request to view sku ratings and reviews
Then I should navigate on sku ratings and reviews page
!-- Manual verification of ratings and reviews details
When I back to sku details from breadcrumb of sku ratings and reviews page
Then I should see sku details page of the given sku
When I request to view sku ratings and reviews
And I back to sku details from sku icon of sku ratings and reviews page
Then I should see sku details page of the given sku
When I request to view sku ratings and reviews
And I add the sku to cart from ratings and reviews page
Then I should navigate on shopping cart page


Scenario: Verify the pagination of ratings and reviews
Meta:
@manual
When I request to view sku ratings and reviews
And I switch the reviews page by the page number
Then I should navigate on the expected page of the reviews
When I go to next/previous page of reviews
Then I should navigate on the expected page of the reviews


Scenario: Verify the also available in

GivenStories: basic\mobile_search_for_sku.story
When I request to check also available in
Then I should navigate on also available in page
When I back to sku details from breadcrumb of also available in page
Then I should see sku details page of the given sku
When I request to check also available in
And I back to sku details from sku icon of also available in page
Then I should see sku details page of the given sku
When I request to check also available in
And I add the sku to cart from also available in page
Then I should navigate on shopping cart page


Scenario: Verify the also available in skus

GivenStories: basic\mobile_search_for_sku.story
When I request to check also available in
And I request to view sku details from the also available in list
Then I should navigate on sku details page
