!-- ODNA-5106

Meta:
@skuid 315515

GivenStories: ..\login\basic\mobile_without_login.story

Scenario: Verify the sku details info
Meta:
@smoke

GivenStories: basic\mobile_search_for_sku.story
When I request to plus the sku qty
Then I should see the sku qty is "2"
When I request to minus the sku qty
Then I should see the sku qty is "1"


Scenario: Verify the max value of qty

When I change the sku qty into "99999" from sku details page
Then I should see the sku qty is "9999"


Scenario: Verify the stock status 
Meta:
@manual
When I make sure that the sku is in stock
Then I should see the sku is in status of "In Stock"


Scenario: Verify the sku of out of stock
Meta:
@manual
When I search for sku "xxxxxx"
Then I should see the sku is in status of "Out of Stock"


Scenario: Verify the pre-order sku
Meta:
@manual
When I search for sku "xxxxxx"
Then I should noticed that I can pre-order the sku