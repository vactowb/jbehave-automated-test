Meta:
@basic

Scenario: Verif to login with matched OMX account

Given I am a www mobile customer with the given account
When I access www mobile site
And I go to login page from header
And I login with my account
Then I should navigate on merge accounts options page