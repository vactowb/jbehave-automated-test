Narrative:
In order to reset my password when i forgot my password,
As a Msite customer forgot my password, 
I want to reset a new password.

!-- WWWMOBILE-2986

Meta:
@username od_it_tester 


Scenario: Verify navigate to request got password question page successfully

When I access www mobile site
And I go to login page from home page
Then I should navigate on login page
When I request to find my password back from login page
Then I should navigate on forgot login or password page


Scenario: Verify the text field checking
Meta:
@manual
When I submit with incomplete password question information
Then I should see an pop up error message


Scenario: Verify to submit with invaild question info

When I submit with invaild password question information
Then I should be noticed that my login name is invalid


Scenario: Verify to submit with vaild question info

When I submit with vaild password question information
Then I should navigate on forgot password page


Scenario: Verify change password with incorrect security question
Meta:
@manual
When I submit with invaild security question answer
Then I should be noticed that my answer is incorrect


Scenario: Verify to change password successfully

When I submit with vaild security question answer
Then I shoulde be noticed that the change password email successfully sent


Scenario: Verify password has been change successfully
Meta:
@manual
When I log out
And I login with my new password
Then I should login successfully
