Scenario: Verify the social login failed
When I access www mobile site
And I go to login page from header
And I login with the following social account:
|Type   |Username          |Password  |
|Google+|wunier@hotmail.com|wrong_pass|
Then I should get the error message for social log in