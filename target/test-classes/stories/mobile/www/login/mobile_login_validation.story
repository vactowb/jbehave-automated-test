
Scenario: Verify invalid account by wrong username or password
When I access www mobile site
And I go to login page from header
And I login with wrong username password
Then I see the error message for invalid login name

Scenario: Verify emtpy username login
When I access home page of www mobile site
And I go to login page from header
And I login with empty username password
Then I see the error message for empty login name