!-- WWWMOBILE-4438

Narrative:
In order to help OD confirm/improve the direction they are headed to,
As a mobile site visitor,
I want to be able to share insights on my experience navigating OD's mobile site.

Meta:
@username od_automation_10
@password tester
@skuid 315515


Scenario: Verify to take survey from home page

When I access www mobile site
And I take survey from home page
Then I should navigate to survey page


Scenario: Verify to submit survey
Meta:
@manual
When I submit survey with no answer selected
Then I should be noticed that the answers are required
When I submit survey with valid info
Then I should navigate to home page


Scenario: Verify to take checkout survey from thank you page

GivenStories: ..\login\basic\mobile_normal_login.story,
              ..\cart\basic\mobile_add_sku_to_cart.story
When I process check out from shopping cart
And I place order
And I take survey from thank you page
!-- Can't verify the page
!-- Then I should navigate to survey page


Scenario: Verify that the survey button should not show up if the survey was taken by the same user within the past 3 months

GivenStories: ..\cart\basic\mobile_add_sku_to_cart.story
When I process check out from shopping cart
And I place order
Then I should not see the take survey button from thank you page