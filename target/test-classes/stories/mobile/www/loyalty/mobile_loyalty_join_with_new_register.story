!-- ODNA-638, ODNA-637, ODNA-2232

Narrative:
In order to create my account,
As a mobile user,
I want to have Loyalty Rewards registration page to do that.


Scenario: Verify to join ODR via anonymous

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
Then I should navigate on office depot rewards landing page
When I request to join ODR
Then I should navigate on OD login page
When I request to register OD account for ODR enrollment
Then I should navigate on ODR express register page


Scenario: Verify to create account with existing phone number

When I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234          				 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | 5612221212    					 |
    | Membership Type   | Personal    				         | 
Then I should be noticed that the phone number is already linked to another ODR account


Scenario: Verify to create account with existing email
Meta:
@skip
When I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | hao.chen2@officedepot.com      	 |
    | Password          | Tester1234             	      	 |
    | Password Confirm  | Tester1234             		     |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | RANPHONE     					     |
    | Membership Type   | Personal    				         | 
Then I should be noticed that the email is already linked to another ODR account


Scenario: Verify to create loyal customer account
Meta:
@skip
When I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | RANPHONE    					     |
    | Membership Type   | Personal    				         | 
Then I should navigate on my rewards page
When I access home page of www mobile site
And I go manage my account
And I choose to view my rewards from my account
Then I should navigate on my rewards page


Scenario: Verify to create local business account
Meta:
@skip
GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to join ODR
And I request to register OD account for ODR enrollment
And I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | RANPHONE    					     |
    | Membership Type   | Business    				         | 
Then I should navigate on my rewards page


Scenario: Verify to create star teacher account
Meta:
@skip
GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to join ODR
And I request to register OD account for ODR enrollment
And I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | RANPHONE    					     |
    | Membership Type   | Teacher      				         | 
Then I should navigate on my rewards page
