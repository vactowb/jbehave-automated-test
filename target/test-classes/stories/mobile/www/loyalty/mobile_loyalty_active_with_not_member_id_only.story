!-- ODNA-1621, ODNA-2393

Narrative:
In order to make sure my reward points aren't given to another user,
As a mobile user, 
I want to validate that I am the owner of the member number being activated.


GivenStories: ..\login\basic\mobile_without_login.story


Scenario: Verify to cancel active member ID which linked

When I request to access ODR from home page
And I request to active ODR
And I request to active ODR account with member number "1924449760"
Then I should navigate on verify account page
When I cancel to verify account
Then I should navigate on office depot rewards landing page

Scenario: Verify to active member ID which linked with phone
Meta:
@wip

When I request to active ODR
And I request to active ODR account with member number "xxxxxxxxxx"
Then I should navigate on verify account page
And I should see I can select an option between voice and sms
When I select to verify account by "SMS"
Then I should navigate on "SMS" registration code verification page


Scenario: Verify to active member ID which linked with email
Meta:
@wip

When I request to access office depot rewards from header
And I request to active ODR
And I request to active ODR account with member number "xxxxxxxxxx"
Then I should navigate on verify account page
And I should see I can select an option of email
When I select to verify account by "Email"
Then I should navigate on "Email" registration code verification page


Scenario: Verify to active member ID which linked with phone number and email

When I request to access office depot rewards from header
And I request to active ODR
And I request to active ODR account with member number "1924449760"
Then I should navigate on verify account page
And I should see I can select an option among email, voice and sms
When I select to verify account by "Voice"
Then I should navigate on "Voice" registration code verification page


Scenario: Verify to cancel verify registration code

When I cancel to verify registration code
Then I should navigate on office depot rewards landing page


Scenario: Verify to verify account by invalid email/voice/sms registration code

When I request to active ODR
And I request to active ODR account with member number "1924449760"
And I select to verify account by "Email"
And I submit the invalid registration code
Then I should be noticed that the registration code is invalid


Scenario: Verify to verify account by valid email/voice/sms registration code
Meta:
@manual

When I submit the valid registration code which received by email/voice/sms
Then I should see the light box of asking me if I have OD account