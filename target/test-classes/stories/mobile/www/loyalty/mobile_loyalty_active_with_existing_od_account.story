!-- ODNA-1623, ODNA-1628, ODNA-1625, ODNA-2899, ODNA-3756

Narrative:
In order to easily access my reward points,
As a mobile user, 
I want to link my OD account to my member number.


Scenario: Verify to active with OD account login

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
Then I should navigate on office depot rewards landing page
When I request to active ODR
And I request to active ODR account with member number "1891901785"
Then I should see the light box of asking me if I have OD account
When I choose "Yes" for the question of asking me if I have OD account
Then I should navigate on OD login page


Scenario: Verify to login with invalid info
Meta:
@username invalid_username
@password invalid_password

When I login with my account from OD login page
Then I should be noticed that my login name or password is invalid


Scenario: Verify the security locking of invalid login
Meta:
@manual
When I login with my account with invalid password from ODR login page for three times
Then I should be noticed that the next incorrect login will deactivate my login ID
When I login with my account with invalid passwordfrom ODR login page again
Then I should be noticed that my login ID is no longer active and call customer service


Scenario: Verify to login with valid info
Meta:
@username od_automation_odr
@password Tester1234

When I login with my account from OD login page
Then I should navigate on join ODR link page
And I should see "Phone Number" is already prefilled on join ODR link page
And I should see "Email" is already prefilled on join ODR link page
!-- Manual
!-- And I should see the email and phone number are come from ODR


Scenario: Verify to link OD with invalid format phone number

When I continue to link the login account with member number by phone number "1234"
Then I should be noticed that phone number is invalid


Scenario: Verify to link OD with invalid phone number

When I continue to link the login account with member number by phone number "5612221212"
Then I should be noticed that phone number must be unique


Scenario: Verify to cancel the linking

When I cancel to link the login account with member number
Then I should see the home page of mobile site


Scenario: Verify to link the login OD with member number
Meta:
@manual

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to active ODR
And I request to active ODR account with member number "5000102854"
And I choose "Yes" for the question of asking me if I have OD account
And I login with my account from OD login page
And I continue to link the login account with member number
Then I should navigate on ODR thank you page
And I should see the member number, phone number and email is correct
When I request to access office depot rewards from header
Then I should navigate on my rewards page

