!-- ODNA-2358, ODNA-3600, ODNA-1785

Narrative:
In order to easily access my reward points,
As a mobile user, 
I want to link my OD account to my member number.


Scenario: Verify to create new member number
Meta:
@username od_automation_odr
@password Tester1234

GivenStories: ..\login\basic\mobile_normal_login.story
When I access home page of www mobile site
And I request to access ODR from home page
And I request to join ODR
And I request to register a new ODR account from member number entering page
Then I should navigate on create member ID page
And I should see "Phone Number" is already prefilled on create member ID page


Scenario: Verify to cancel the linking

When I cancel to link with OD account from create member ID page
Then I should see the home page of mobile site


Scenario: Verify to create new member number
Meta:
@username od_automation_odr
@password Tester1234

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to join ODR
And I login with my account from OD login page
And I request to register a new ODR account from member number entering page
Then I should navigate on create member ID page
And I should see "Phone Number" is already prefilled on create member ID page


Scenario: Verify to create new member number with invalid phone number

When I continue to link OD account with the following contact info from create member ID page:
	| Field             | Value     |
    | Phone Number      | RANPHONE  |
    | Membership Type   | Personal  | 
Then I should be noticed that the phone number is not valid on create member ID page


Scenario: Verify to create new member number with existing number

When I continue to link OD account with the following contact info from create member ID page:
	| Field             | Value      |
    | Phone Number      | 5612221212 |
    | Membership Type   | Business   | 
Then I should be noticed that the phone number is already linked to another ODR account on create member ID page


Scenario: Verify to create new member number and link to OD account
Meta:
@manual
When I continue to link OD account with the following contact info from create member ID page:
	| Field             | Value      |
    | Phone Number      | RANPHONE   |
    | Membership Type   | Teacher    | 
Then I should navigate on my rewards page