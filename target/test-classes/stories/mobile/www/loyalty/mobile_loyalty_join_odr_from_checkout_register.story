!-- ODNA-3475

Narrative:
In order to link my account with ODR after the registration of OD account,
As a mobile user, 
I want to have the option on express register page to do that.


Scenario: Verify to join with invalid phone number

GivenStories: ..\checkout\basic\mobile_anonymous_checkout_from_cart.story
When I request to register from checkout options page
And I register with join ODR by the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | ODR Phone Number  | RANDOM 10   					     |
    | Membership Type   | Personal    				         | 
Then I should be noticed that the phone number is invalid from register page


Scenario: Verify to join with existing phone number

When I register with join ODR by the following info:
    | Field             | Value       |
	| ODR Phone Number  | 5612221212  |
    | Membership Type   | Teacher     |
Then I should be noticed that the phone number is already linked to another ODR account from register page


Scenario: Verify to join with valid phone number

When I register with join ODR by the following info:
	| Field             | Value      |
    | ODR Phone Number  | RANPHONE   |
    | Membership Type   | Business   |
Then I should navigate on new shipping address page