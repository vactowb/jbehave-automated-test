!-- ODNA-3609, ODNA-2232


Scenario: Verify to login with invalid OD account
Meta:
@username invalid_username
@password invalid_password

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to join ODR
And I login with my account from OD login page
Then I should be noticed that my login name or password is invalid


Scenario: Verify to login with linked OD account
Meta:
@username srujan#od58.com
@password tester

When I login with my account from OD login page
Then I should navigate on my rewards page


Scenario: Verify to login with valid unlinked OD account
Meta:
@username od_automation_odr
@password Tester1234

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to join ODR
And I login with my account from OD login page
Then I should navigate on member number entering page


Scenario: Verify to active with invalid member number

When I submit my member number "12345abcd" from member number entering page
Then I should be noticed that the member number must be ten numeric digits from member number entering page


Scenario: Verify to active with invalid member number

When I submit my member number "1234567890" from member number entering page
Then I should be noticed that the member number is not valid from member number entering page


Scenario: Verify to active with linked member number

When I submit my member number "1891833822" from member number entering page
Then I should be noticed that the member number is already linked from member number entering page


Scenario: Verify to join with existing member number

When I submit my member number "1891901785" from member number entering page
Then I should navigate on join ODR link page