Meta:
@skuid 315515 


GivenStories: ..\register\basic\mobile_express_register_basic.story,
              ..\cart\basic\mobile_add_sku_to_cart.story
            

Scenario: Verify to checkout when the coupon not met all criteria
Meta:
@skip
When I apply with the coupon code "703389784"
And I process check out from shopping cart
Then I should see the coupon in cart light box
When I request to continue shopping from coupons in cart light box
Then I should navigate to shopping cart page
When I process check out from shopping cart
And I request to continue with checkout to ignore coupon criteria
Then I should navigate on new shipping address page
When I access home page of www mobile site
And I navigate to cart from header
Then I should see the coupon has been removed


Scenario: Verify checkout when multiple coupon not met all criteria
Meta:
@skip
When I apply with the coupon code "703389784"
And I apply with the coupon code "17326163"
And I back to my cart
And I process check out from shopping cart
Then I should be notice that I have not met qualifications for the selected coupons


Scenario: Verify to checkout when the coupon not met all criteria via no js
Meta:
@manual
Given Browser JS is disabled
When When I try to add a coupon from my cart
And I apply with the coupon code "21258225"
And I back to my cart
And I process check out from shopping cart
Then I should see the coupons in cart content on cart page
When I request to continue shopping from coupons in cart light box
Then I should navigate to shopping cart page
When I process check out from shopping cart
And I request to continue with checkout to ignore coupon criteria
Then I should navigate on review order page
When I navigate to cart from header
Then I should see the coupon has been removed


Scenario: Verify to add the 1A coupon
Meta:
@wip
When I apply with the coupon code "30293380"
And I add the qualification items into cart
Then I should navigate on coupon details page
When I request to view cart from coupon details page
Then I should be notice that I have not met qualifications for the selected coupons


Scenario: Verify to add the 1B coupon
Meta:
@wip
When I apply with the coupon code "134247320"
And I add the qualification items into cart
Then I should navigate on coupon details page
When I request to view cart from coupon details page
Then I should be notice that I have not met qualifications for the selected coupons
