Narrative:
In order to have a nice experience unveiling the coupon offer,
As a mobile site user, 
I want to be able to manage my SurPrize coupon experience with a Scratching action. 


Scenario: Verify to submit with invalid coupon

GivenStories: ..\login\basic\mobile_without_login.story
When I navigate on surprize page
And I submit with coupon "abc123" form surprize page
Then I should be noticed that entry is invalid


Scenario: Verify to submit with valid coupon
Meta:
@skip
When I submit with coupon "703389784" form surprize page
Then I should be request to reveal my surprize