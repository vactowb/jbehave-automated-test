!-- ODNA-9337

Scenario: Verfiy the sign up with no mandatory fields

GivenStories: basic\mobile_sign_up_offers.story
When I submit to signup
Then I should be noticed that the "Email Address" is required from sign up page
And I should be noticed that the "ZipCode" is required from sign up page


Scenario: Verfiy to sign up with no phone number

When I fill the sign up fields with following info:
	|Field        |Value                 |
    |Subscribe To |Text Message Only     |
And I submit to signup
Then I should be noticed that the "Phone number" is required from sign up page


Scenario: Verfiy to sign up with invalid email
Meta:
@manual
When I fill the sign up fields with following info:
	|Field        |Value                 |
    |Subscribe To |Email Only            |
    |Zip Code     |33496                 |
    |Email        |abcdefg               |
And I submit to signup
Then I should be noticed that the email is invalid from sign up page


Scenario: Verfiy to sign up with invalid phone number

When I fill the sign up fields with following info:
	|Field        |Value                 |
    |Subscribe To |Text Message Only     |
    |Phone Number |aaa-bbb-cccc          |
And I submit to signup
Then I should be noticed that the phone number is invalid from sign up page


Scenario: Verfiy to sign up with invalid zip code

When I fill the sign up fields with following info:
	|Field        |Value                 |
    |Subscribe To |Email Only            |
    |Zip Code     |abc123                |
    |Email        |test@od.com           |
And I submit to signup
Then I should be noticed that the zip code is invalid from sign up page


Scenario: Verfiy to sign up with all field

GivenStories: basic\mobile_sign_up_offers.story
When I fill the sign up fields with following info:
	|Field        |Value                 |
    |Subscribe To |Email & Text Messages |
    |Zip Code     |33496                 |
    |Phone Number |561-222-2424          |
    |Email        |test@od.com           |
And I submit to signup
Then I should see my request submitted
