!-- WWWMOBILE-4362

Narrative:
In order to facilitate my navigation through the process to buy,
As a mobile site user, 
I want to have breadcrumbs on Edit Billing Address when at Checkout showing that I am at step 4 of Checkout.


Meta:
@username od_automation_3
@password tester
@skuid 315515

GivenStories: ..\login\basic\mobile_normal_login.story,
              ..\cart\basic\mobile_add_sku_to_cart.story

Scenario: Verify to cancel edit billing info from review order

When I process check out from shopping cart
And I request to change billing info from review order page
Then I should navigate on edit billing info page
When I cancel to edit billing
Then I should navigate on review order page

Scenario: Verify to edit billing info from review order

When I request to change billing info from review order page
And I edit my biling information with following info:
	| Field        | Value               |
    | First Name   | RANDOM 6            |
    | Last Name    | RANDOM 6            |
    | Address 1    | 6600 N MILITARY TRL |
    | City         | Boca Raton          |
    | State        | FL - Florida        |
    | Post Code    | 33496               |
    | Phone Number | 5612222121          |
    | Email        | test@od.com         |
Then I should navigate on review order page
