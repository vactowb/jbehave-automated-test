Meta:
@skuid 315515
@basic

Scenario: Verify to anonymous checkout from cart
GivenStories: ..\..\login\basic\mobile_without_login.story,
			   ..\..\cart\basic\mobile_add_sku_to_cart.story
When I process check out from shopping cart
And I choose to login or register
Then I should navigate on checkout options page