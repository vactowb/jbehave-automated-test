!-- WWWMOBILE-3765, WWWMOBILE-3752, WWWMOBILE-4275, ODNA-14241

Narrative:
In order to verify checkout with out login successfully,
As a customer without login, 
I want to see the checkout successfully page.

Meta:
@skuid 315515

GivenStories: ..\login\basic\mobile_without_login.story,
              ..\cart\basic\mobile_add_sku_to_cart.story

Scenario: Verify to checkout anonymous in step 1    
When I process check out from shopping cart
And I choose to continue as guest
Then I should navigate on new shipping address page
And I should only see home and login options on main menu


Scenario: Verify to add new shipping address in step 2

When I add new shipping address with following info:
    | Field        | Value               |
    | First Name   | test                |
    | Last Name    | test                |
    | Address 1    | 6600 N MILITARY TRL |
    | City         | Boca Raton          |
    | State        | FL - Florida        |
    | Post Code    | 33496               |
    | Phone Number | 5612222121          |
    | Email        | test@od.com         |
Then I should navigate on enter new payment method page


Scenario: Verify to add new credit card in step 3

When I add a new credit card for the new account with following info:
	| Field        | Value            |
    | Nick Name    | RANDOM 6         |
    | Holder Name  | tester           |
    | Card Number  | 4111111111111111 |
    | Month        | 12               |
    | Year         | 2018             |
Then I should navigate on review order page


Scenario: Verify to place order in step 4

When I request to place order
And I enter my credit card security code
Then I should see my order is placed successfully


Scenario: Verify to view sku details from thank you page

When I request to view sku details from thank you page
Then I should see sku details page of the given sku


Scenario: Verify the CVV section if JS is disabled
Meta:
manual
Given No-JS setup for browser
When I navigate on review order page
Then I should see the CVV section on review order page
When I enter my credit card security code
And I request to place order
Then I should see my order is placed successfully