!--login with registed account
Meta:
@username od_automation_3
@password tester
@skuid 315515

GivenStories: ..\login\basic\mobile_normal_login.story,
              ..\cart\basic\mobile_add_sku_to_cart.story
              
Scenario: Apply the gift card
When I process check out from shopping cart
And I request to add gift card from review order page
Then I should navigate on gift card page
When I fill the gift card with following info:
|Card Number      |Pin |
|7777007852971879 |0833|
And I apply the gift card
Then I should see the gift card is apllied successfully


Scenario: Remove the gift card
When I remove the gift card
Then I should see the gift card has been removed successfully


Scenario: Cancel applying gift card
When I request to add gift card from review order page
Then I should navigate on gift card page
When I fill the gift card with following info:
|Card Number      |Pin |
|7777007852971879 |0833|
And I cancel to apply gift card
Then I should navigate on review order page


Scenario: Deny applying invalid gift card
When I request to add gift card from review order page
Then I should navigate on gift card page
When I fill the gift card with following info:
|Card Number      |Pin |
|abc4567891234567 |0833|
And I apply the gift card
Then I should see the error message
