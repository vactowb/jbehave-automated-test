!-- WWWMOBILE-3935

Narrative:
In order to get my order placed quickly
As a Mobile user
I want to have a clear Login Page when I am checking out


Scenario: Verify to login with no username and password input

GivenStories: basic\mobile_anonymous_checkout_from_cart.story
When I login from checkout options with the following info:
	| Username | Password |
	|          |          |
Then I should be noticed that to enter username and password


Scenario: Verify to login with invalid username and password

When I login from checkout options with the following info:
	| Username  | Password  |
	| RANDOM 10 | RANDOM 10 |
Then I should be noticed that the username or password is invalid


Scenario: Verify to register from checkout options

When I request to register from checkout options page
Then I should navigate on registration page
When I register with the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             		     |
    | Password Confirm  | Tester1234            	         |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
Then I should navigate on new shipping address page


Scenario: Verify to checkout as guest

GivenStories: basic\mobile_anonymous_checkout_from_cart.story
When I checkout as guest from checkout options page
Then I should navigate on new shipping address page