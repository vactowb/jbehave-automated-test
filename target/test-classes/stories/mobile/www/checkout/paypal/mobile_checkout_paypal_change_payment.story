!--login with registed account
Meta:
@skuid 315515
@username od_automation_7
@password tester

GivenStories: ..\..\login\basic\mobile_without_login.story,
			  ..\..\cart\basic\mobile_add_sku_to_cart.story

Scenario: Guest check out by Paypal without register

When I process check out from shopping cart
And I choose to login or register
And I login from checkout options with the given account
Then I should navigate on shopping cart page
When I process check out from shopping cart
Then I should navigate on review order page
When I requtest to change my payment from review order page
Then I should navigate on payment options page
When I choose paypal as my payment method
And I login paypal with the following account:
	|Username                |Password|
	|odtester@officedepot.com|odtester|
And I continue to check out with this paypal account
Then I should see the payment method of my order is paypal
When I request to place order
Then I should see my order is placed successfully