!--login with registed account
Meta:
@skuid 315515


Scenario: Verify to checkout by paypal with register account 
Meta:
@skip
GivenStories: ..\..\login\basic\mobile_without_login.story,
              ..\..\cart\basic\mobile_add_sku_to_cart.story
When I process check out from shopping cart
And I choose to check out with paypal
And I login paypal with the following account:
	|Username                |Password|
	|odtester@officedepot.com|odtester|
And I continue to check out with this paypal account
And I login with the tied OD account with password "odtester"
Then I should see the payment method of my order is paypal


Scenario: Verify to checkout by paypal with register account from checkout options
Meta:
@skip
GivenStories: ..\..\login\basic\mobile_without_login.story,
              ..\..\cart\basic\mobile_add_sku_to_cart.story
When I process check out from shopping cart
And I choose to login or register
And I choose to check out with paypal from checkout options page
And I login paypal with the following account:
	|Username                |Password|
	|odtester@officedepot.com|odtester|
And I continue to check out with this paypal account
And I login with the tied OD account with password "odtester"
Then I should see the payment method of my order is paypal