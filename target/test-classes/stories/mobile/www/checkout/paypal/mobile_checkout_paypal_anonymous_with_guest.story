!--login with registed account
Meta:
@skuid 313965

GivenStories: ..\..\login\basic\mobile_without_login.story,
              ..\..\cart\basic\mobile_add_sku_to_cart.story

Scenario: Verify to checkout by paypal with guest 

When I process check out from shopping cart
And I choose to check out with paypal
And I login paypal with the following account:
	|Username                 |Password|
	|zhen.yang@officedepot.com|asd@123,|
And I continue to check out with this paypal account
And I continue as guest
Then I should see the payment method of my order is paypal

