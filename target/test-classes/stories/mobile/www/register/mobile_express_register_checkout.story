Narrative:
In order to regist my account easily,
As a Msite user, 
I want to be able to register with minimal information needed for me to be able to get the benefits of a registered user.


Scenario:Verify checkout successfully by new register 
Meta:
@skuid 315515

GivenStories: basic\mobile_express_register_basic.story,
			  ..\cart\basic\mobile_add_sku_to_cart.story
When I process check out from shopping cart
Then I should navigate on new shipping address page
When I add new shipping address with following info:
    | Field        | Value               |
    | First Name   | test                |
    | Last Name    | test                |
    | Address 1    | 6600 N MILITARY TRL |
    | City         | Boca Raton          |
    | State        | FL - Florida        |
    | Post Code    | 33496               |
    | Phone Number | 5612222121          |
    | Email        | test@od.com         |
Then I should navigate on new payment page
When I add a new credit card with following info:
	| Field        | Value                   |
    | Nick Name    | RANDOM 6                |
    | Holder Name  | tester                  |
    | Card Number  | 4111111111111111        |
    | Month        | 12                      |
    | Year         | 2018                    |
    | Address      | 6600 N MILITARY TRL # 6 |
    | City         | Boca Raton              |
    | State        | FL - Florida            |
    | Post Code    | 33496                   |
Then I should navigate on review order page
When I request to place order
And I enter my credit card security code
Then I should see my order is placed successfully


