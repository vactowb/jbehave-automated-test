Meta:
@basic

Scenario: Verify the social login from registration 

When I login by the given social account from registration page
Then I should login successfully by social account
When I close the social login window via registration
Then I should see link account page