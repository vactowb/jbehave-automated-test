GivenStories: basic\mobile_register_basic.story


Scenario: Verify to create account with no security question

When I register with the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      			     |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Answer            | RANDOM 5                           |
Then I should be noticed that I need to set security question


Scenario: Verify to create account with invalid zip code

When I register with the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 123               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             		     |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
Then I should be noticed that zip code is invalid


Scenario: Verify to create account with existed email 

When I register with the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | hao.chen2@officedepot.com     	 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
Then I should be noticed that email already in used


Scenario: Verify to create account with valid info

When I register with the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
Then I should navigate on my account page
