Narrative:
In order to easily register,
As a customer without login, 
I want to register with my Gigya.

!-- WWWMOBILE-2949
Scenario: Verify the social login with register 
Meta:
@type Google+
@username email.not.linked.exist
@password odtesting

GivenStories: basic\mobile_register_basic.story,
              basic\mobile_social_login_from_registration.story
When I choose to register with social account
Then I should see social registration page


Scenario: Verify the social register with invalid zip code 

When I register with social by following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 123               			 	 |
    | Email             | random@od.com      				 |
Then I should be noticed that zip code is invalid via registration


Scenario: Verify the social register with existed email 

When I register with social by following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | hao.chen2@officedepot.com      	 |
Then I should be noticed that email already in used via registration


Scenario: Verify to register with social account 
Meta:
@manual

GivenStories: basic\mobile_register_basic.story,
              basic\mobile_social_login_from_registration.story
When I choose to register with social account
And I register with social by valid information
Then I should register with social successfully


Scenario: Verify the required information with facebook registration
Meta:
@type Facebook
@username wunier@hotmail.com
@password tester
@manual

GivenStories: basic\mobile_register_basic.story,
              basic\mobile_social_login_from_registration.story
When I choose to register with social account
Then I should see the required information: first/last name, zip code and email


Scenario: Verify the required information with facebook registration
Meta:
@type Google
@username email.not.linked.exist
@password odtesting
@manual

GivenStories: basic\mobile_register_basic.story,
              basic\mobile_social_login_from_registration.story
When I choose to register with social account
Then I should see the required information: first/last name, zip code and email


Scenario: Verify the required information with linkedin registration
Meta:
@type LinkedIn
@username wunier@hotmail.com
@password odtester
@manual

GivenStories: basic\mobile_register_basic.story,
              basic\mobile_social_login_from_registration.story
When I choose to register with social account
Then I should see the required information: first/last name, zip code and email


Scenario: Verify there is no password or security question in process of social registration
Meta:
@manual

GivenStories: basic\mobile_register_basic.story,
              basic\mobile_social_login_from_registration.story
When I choose to register with social account
Then I should see social registration page
And I should see no password or security questions are required


Scenario: Verify there is prompted to enter remaining fields for check out 
Meta:
@manual

When I checkout a sku
Then I should see a prompte witch enter remaining fields
Then I should not see the prompte to add security questions after checkout is complete