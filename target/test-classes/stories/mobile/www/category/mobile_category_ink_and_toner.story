Scenario: Check ink & toner thrill down

When I access www mobile site
And I navigate to "Ink & Toner" product list
And I select the ink with following details:
	| Brand | Model  |
	| Epson | 160    |
Then I should see the ink finder results are about: "Epson 160"


Scenario: Check the recodrs of recently search

When I request to find ink from header
And I select the previous search: "Epson 160"
Then I should see the ink finder results are about: "Epson 160"