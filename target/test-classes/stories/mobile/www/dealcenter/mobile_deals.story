Scenario: Verfiy get store list by zip code throgh weekly ads 

GivenStories: ..\login\basic\mobile_without_login.story
When I view deals from header
Then I should navigate on deals page
When I navigate to weekly ads from deals page
Then I should navigate on store locator page
When I provide the zipcode: "33496"
And I find stores
Then I should see the store list which near "33496"
When I check details of the first store which is not my store
Then I should navigate on store details page


Scenario: Verify to view weekly ads from store details page

When I view weekly ads from store details page
Then I should navigate on weekly ads page
When I view one weekly ad details
Then I should navigate on weekly ad pages page
When I view the list of items which on ad page
Then I should navigate on list view page


Scenario: Verfiy get store list by city and state throgh weekly ads

GivenStories: ..\login\basic\mobile_without_login.story
When I request to view deals from home page
Then I should navigate on deals page
When I navigate to weekly ads from deals page
Then I should navigate on store locator page
When I provide city and state: "boca raton, FL"
And I find stores
Then I should see the store list which near "boca raton"
When I view weekly ads of the first store
Then I should navigate on weekly ads page


Scenario: Verfiy get deal category in deal center

GivenStories: ..\login\basic\mobile_without_login.story
When I view deals from header
And I navigate to deal center from deals page
Then I should navigate on choose category page