!-- WWWMOBILE-4785

Narrative:
In order to better manage my orders,
As a mobile customer, 
I want to see a cleaner Order Details page including a product image.


Meta:
@skuid 315515

GivenStories: ..\register\mobile_express_register_checkout.story

Scenario: Verify to view the order details from thank you page

When I request to view order details from thank you page
Then I should navigate on expected order details page
And I shoulde see the order status is "In Process" on order details page


Scenario: Verify to cancel order

When I request to cancel order from order details page
Then I should see the cancel order light box
When I reject to cancel order
Then I should see the order is not cancelled
When I request to cancel order from order details page
And I accept to cancel order
Then I should see the order is cancelled successfully


Scenario: Verify to reorder

When I close the cancel order light box
And I navigate to manage orders from bread crumb
Then I should navigate on manage orders page
When I request to view details for the first order
Then I should navigate on expected order details page
And I shoulde see the order status is "Cancelled" on order details page
When I request to reorder items from order details page
Then I should navigate on shopping cart page
When I process check out from shopping cart
And I request to place order
Then I should see my order is placed successfully
When I request to track orders from header
Then I should see my placed order on manage orders page