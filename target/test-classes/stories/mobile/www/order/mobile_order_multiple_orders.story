!-- WWWMOBILE-3090

Meta:
@username od_automation_order
@password tester


GivenStories: ..\login\basic\mobile_normal_login.story

Scenario: Verify to place with multiple orders

When I access home page of www mobile site
And I add the sku "107809" to cart
And I access home page of www mobile site
And I add the sku "705769" to cart
And I access home page of www mobile site
And I add the sku "337464" to cart
And I process check out from shopping cart
And I request to place order
Then I should see there are "3" orders placed successfully