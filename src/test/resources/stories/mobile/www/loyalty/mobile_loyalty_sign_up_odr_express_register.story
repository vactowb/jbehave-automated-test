!-- ODNA-3475

Narrative:
In order to link my account with ODR after the registration of OD account,
As a mobile user, 
I want to have the option on express register page to do that.


Scenario: Verify to active with invalid member number 

GivenStories: ..\login\basic\mobile_without_login.story
When I go to login page from home page
And I request to register from login page
And I request to active ODR account with member number "12345abcd" from register page
Then I should be noticed that the member number must be ten numeric digits from register page


Scenario: Verify to active with invalid member number

When I request to active ODR account with member number "1234567890" from register page
Then I should be noticed that the member number was not found from register page


Scenario: Verify to active with linked member number

When I request to active ODR account with member number "1800015628" from register page
Then I should be noticed that the member number is already linked from register page


Scenario: Verify to active with valid member number

When I request to active ODR account with member number "1891901785" from register page
Then I should navigate on ODR express register page


Scenario: Verify to join with invalid phone number

GivenStories: ..\login\basic\mobile_without_login.story
When I go to login page from home page
And I request to register from login page
And I register with join ODR by the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | ODR Phone Number  | 1234567890      					 |
    | Membership Type   | Personal    				         | 
Then I should be noticed that the phone number is invalid from register page


Scenario: Verify to join with existing phone number

When I register with join ODR by the following info:
    | Field             | Value       |
	| ODR Phone Number  | 5612221212  |
    | Membership Type   | Teacher     |
Then I should be noticed that the phone number is already linked to another ODR account from register page


Scenario: Verify to join with valid phone number
Meta:
@skip
When I register with join ODR by the following info:
    | Field             | Value     |
	| ODR Phone Number  | RANPHONE  |
    | Membership Type   | Business  |
Then I should navigate on my account page
When I choose to view my rewards from my account
Then I should navigate on my rewards page