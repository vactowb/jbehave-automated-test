!-- ODNA-17393

Narrative:
In order to create or merge my existing MP acct to an ODOMR acct,
As a maxperks mobile user, 
I want to be redirected to an ODOMR page.


Scenario: Verify to login with invalid maxperks account

GivenStories: ..\..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I want to transform my maxperks rewards account
Then I should navigate on maxperks transition redirect page
When I request to login with the following maxperks:
     | Field    | Value     |
     | Account  | RANDOM 10 |
     | Password | RANDOM 10 |
Then I should be noticed that my account is invalid


Scenario: Verify to lookup maxperks with phone number not meet 10 digital
Meta:
@manual
When I fill the phone number with "123456789" for lookup my maxperks account info
Then I should noticed that the lookup button is not available


Scenario: Verify the phone number is in 10 digital at most

When I fill the phone number with "12345678901" for lookup my maxperks account info
Then I should see the filled phone number is "1234567890"


Scenario: Verify to lookup maxperks with not existing phone number

When I request to lookup my maxperks account info by phone number "5612221212"
Then I should be noticed that there's no member number with this phone number


Scenario: Verify to join rewards 

When I request to join rewards from maxperks transition redirect page
Then I should navigate on OD login page