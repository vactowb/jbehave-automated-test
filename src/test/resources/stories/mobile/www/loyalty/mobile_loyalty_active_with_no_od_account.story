!-- ODNA-1623, ODNA-1628, ODNA-1763, ODNA-2557

Narrative:
In order to easily access my reward points,
As a mobile user, 
I want to create an OD account to link to my member number.


GivenStories: ..\login\basic\mobile_without_login.story

Scenario: Verify to active with valid member number and no OD account

When I access home page of www mobile site
And I request to access office depot rewards from header
Then I should navigate on office depot rewards landing page
When I request to active ODR
And I request to active ODR account with member number "1891901785"
Then I should see the light box of asking me if I have OD account
When I choose "No" for the question of asking me if I have OD account
Then I should navigate on ODR express register page
And I should see "Phone Number" is already prefilled on join ODR page
!-- And I should see "Email" is already prefilled on join ODR page


Scenario: Verify to create OD account with existing phone number

When I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234          				 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | 5612221212    					 |
Then I should be noticed that the phone number is already linked to another ODR account


Scenario: Verify to create OD account with existing email

When I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | hao.chen2@officedepot.com      	 |
    | Password          | Tester1234             	      	 |
    | Password Confirm  | Tester1234             		     |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | RANPHONE    					     |
Then I should be noticed that the email is already linked to another ODR account


Scenario: Verify to create OD account and link with member number
Meta:
@manual

When I register with the following info for join ODR:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
    | Phone Number      | RANPHONE    					     |
Then I should navigate on ODR thank you page
And I should see the member number, phone number and email is correct
When I request to access office depot rewards from header
Then I should navigate on my rewards page


Scenario: Verify to view office depot rewards for the linked account
Meta:
@username srujan#od58.com
@password tester

GivenStories: ..\login\basic\mobile_normal_login.story
When I access home page of www mobile site
And I request to view my rewards from home page
Then I should navigate on my rewards page