!-- ODNA-1616

Narrative:
In order to claim my rewards points and activate my account,
As a mobile user,
I want to for my member number to be checked to see if it is already linked to an OD account.


Scenario: Verify to active with invalid member number

GivenStories: ..\login\basic\mobile_without_login.story
When I request to access ODR from home page
And I request to active ODR
Then I should navigate on active office depot rewards page
When I request to active ODR account with member number "12345abcd"
Then I should be noticed that the member number must be ten numeric digits


Scenario: Verify to active with invalid member number

When I request to active ODR account with member number "1234567890"
Then I should be noticed that the member number was not found


Scenario: Verify to active with linked member number

When I request to active ODR account with member number "1800015628"
Then I should be noticed that the member number is already linked

