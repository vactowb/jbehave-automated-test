!-- WWWMOBILE-2865

Meta:
@basic

Scenario: Verify to express register successfully

When I access www mobile site
And I go to login page from home page
And I request to register from login page
Then I should navigate on registration page
When I register with the following info:
	| Field             | Value              				 |
    | First Name        | RANDOM 6               			 |
    | Last Name         | RANDOM 6              			 |
    | Zip Code          | 33496               			 	 |
    | Email             | random@od.com      				 |
    | Password          | Tester1234             			 |
    | Password Confirm  | Tester1234             			 |
    | Security Question | What is your mother's maiden name? |
    | Answer            | RANDOM 5                           |
Then I should navigate on my account page