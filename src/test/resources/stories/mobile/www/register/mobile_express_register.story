!-- WWWMOBILE-3725, WWWMOBILE-4784

Narrative:
In order to regist my account quickly,
As a OD customer, 
I want to regist my account quickly.


GivenStories: basic\mobile_express_register_basic.story

Scenario: Verify to add new shipping address after express register

When I edit my shipping information
And I add new shipping address
Then I should navigate to add new shipping address page
When I add new shipping address from my account with following info:
	|Field       |Value               |
    |First Name  |RANDOM 5            |
    |Last Name   |RANDOM 5            |
    |Address 1   |6600 N MILITARY TRL |
    |City        |Boca Raton          |
    |State       |FL - Florida        |
    |Post Code   |33496               |
    |Phone Number|5612222424          |
    |Email       |test@od.com         |
Then I should see my new shipping address added


Scenario: Verify there's no orders for the new registered account

When I access home page from header
And I request to track orders from header
Then I should see no orders on manage orders page