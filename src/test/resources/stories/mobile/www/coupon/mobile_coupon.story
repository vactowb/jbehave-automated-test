!-- ODNA-15256

Meta:
@skuid 315515 
@username od_add_coupon 
@password tester

GivenStories: ..\login\basic\mobile_normal_login.story,
              ..\cart\basic\mobile_add_sku_to_cart.story

Scenario: Verify to add coupon with invalid code

When I apply with the coupon code "asdfasdf"
Then I should see the error message "Please make sure your coupon is valid for online purchases and re-enter the coupon code" from cart page
When I apply with the coupon code "1234"
Then I should see the error message "You entered an invalid format for the coupon code" from cart page


Scenario: Verify to add coupon with valid code
Meta:
@skip
When I update the qty of no. "1" item to "5"
And I apply with the coupon code "703389784"
Then I should see my items has been discounted in my cart
And I should see coupon added successfully from cart page


Scenario: Verify edit and remove coupon code
Meta:
@skip
When I edit the added coupon from my cart
Then I should navigate to coupon center page
And I should see the coupons which I added
When I removed the coupon
Then I should navigate to shopping cart page


Scenario: Verify to remove coupon from cart
Meta:
@skip
When I apply with the coupon code "703389784"
And I request to removed the coupon from cart
Then I should see the remove coupon confirm lighting box
When I choose "No" from the remove coupon confirm lighting box
Then I should navigate to shopping cart page
When I request to removed the coupon from cart
And I choose "Yes" from the remove coupon confirm lighting box
Then I should see the added coupon has been moved