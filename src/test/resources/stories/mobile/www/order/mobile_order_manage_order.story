!-- WWWMOBILE-4784

Narrative:
In order to view individual product shipping information when split shipping occurs,
As a mobile user, 
I want to have a new feature that allows me to do this.


Meta:
@skuid 315515

GivenStories: ..\register\mobile_express_register_checkout.story

Scenario: Verify to cancel order

When I request to track orders from header
Then I should see my placed order on manage orders page
And I should see the first order status is "In Process" on manage orders page
When I request to cancel the first order from manage orders page
Then I should see the cancel order light box on manage orders page
When I reject to cancel order from manage orders page
Then I should see the first order is not cancelled
When I request to cancel the first order from manage orders page
And I accept to cancel order from manage orders page
Then I should see the order cancelled
When I close the cancel order light box from manage orders page
Then I should see the first order status is "Cancelled" on manage orders page


Scenario: Verify to reorder the cancelled order

When I request to reorder the first order
Then I should navigate on shopping cart page
When I process check out from shopping cart
And I request to place order
Then I should see my order is placed successfully
When I request to track orders from header
Then I should see my placed order on manage orders page
And I should see the first order status is "In Process" on manage orders page


