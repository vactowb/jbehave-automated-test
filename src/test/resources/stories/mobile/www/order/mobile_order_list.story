!-- WWWMOBILE-4784

Narrative:
In order to view individual product shipping information when split shipping occurs,
As a mobile user, 
I want to have a new feature that allows me to do this.


Meta:
@username od_automation
@password tester

GivenStories: ..\login\basic\mobile_normal_login.story

Scenario: Verify to manage orders for logged in user

When I access home page of www mobile site
And I go manage my account
And I choose to track orders from my account
Then I should navigate on manage orders page


Scenario: Verify paging for order history

When I request to track orders from header
And I request to view next page
Then I should see the previous page link
And I should see the first page link
When I request to view next page
And I request to view previous page
And I request to view first page
Then I should see the next page link
