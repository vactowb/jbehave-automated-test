!-- WWWMOBILE-4350

Narrative:
In order to check the order's status in a breeze,
As a mobile customer, 
I want to be able to easily enter any order number.


GivenStories: ..\login\basic\mobile_without_login.story

Scenario: Verify to track order with no info

When I request to track orders from header
Then I should navigate on track orders page
When I submit to track order with no info inputted
Then I should be noticed to fill the required field


Scenario: Verify to track order with invalid format order number

When I request to track order with following info:
	| Field        | Value      |
	| Order Number | 12345678   |
	| Phone Number | 5612221212 |
Then I should be noticed that the order number with invalid format


Scenario: Verify to track order with invalid order number

When I request to track order with following info:
	| Field        | Value      |
	| Order Number | 123456789  |
	| Phone Number | 5612221212 |
Then I should be noticed that the order number is invalid


Scenario: Verify to track order with invalid phone number

When I request to track order with following info:
	| Field        | Value      |
	| Order Number | 333380112  |
	| Phone Number | 1231212123 |
Then I should be noticed that the phone number is invalid


Scenario: Verify to track order with invalid account number

When I request to track order with following info:
	| Field          | Value      |
	| Order Number   | 333380112  |
	| Account Number | 123456     |
Then I should be noticed that the account number is invalid


Scenario: Verify to track order with valid info

When I request to track order with following info:
	| Field          | Value      |
	| Order Number   | 333380112  |
	| Account Number | 00138881   |
Then I should navigate on expected order details page


Scenario: Verify to track order with valid info

When I request to track orders from header
And I request to track order with following info:
	| Field        | Value      |
	| Order Number | 333380112  |
	| Phone Number | 5612221212 |
Then I should navigate on expected order details page


Scenario: Verify to login to track order 
Meta:
@username od_automation
@password tester

Given I am a www mobile customer with the given account
When I request to track orders from header
And I request to login to track orders
Then I should navigate on login page
When I login with my account
Then I should navigate on manage orders page