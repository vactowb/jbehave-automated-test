Narrative:
In order to login when i forgot my username,
As a Msite customer forgot my username, 
I want to find my username.

!-- WWWMOBILE-2986

Scenario: Verify navigate to request got username question page successfully

When I access www mobile site
And I go to login page from home page
Then I should navigate on login page
When I request to find my username back from login page
Then I should navigate on forgot login or password page


Scenario: Verify the text field checking
Meta:
@manual
When I submit with incomplete username question information
Then I should see an pop up error message


Scenario: Verify to submit with invaild question info

When I submit with the following username question information:
	| Field       | Value           |
    | First Name  | invalid         |
    | Last Name   | invalid         |
    | Email       | invalid@od.com  |
Then I should be noticed that there are no users matching


Scenario: Verify get username succseefully
Meta:
@manual
When I submit with the following username question information:
	| Field       | Value                 |
    | First Name  | test                  |
    | Last Name   | chen                  |
    | Email       | test@officedepot.com  |
Then I should be noticed that the change login name email successfully sent


Scenario: Verify the username is correct
Meta:
@manual
When I log in with got username
Then I should login successfully