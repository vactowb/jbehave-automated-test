Scenario: Verify the gmail social login with gmail account linked
When I access www mobile site
And I go to login page from header
And I login with the following social account:
|Type   |Username               |Password|
|Google+|lizhiqiang.od@gmail.com|1Welcome| 
Then I should social login successfully

Scenario: Verify the facebook social login with OD account linked 
Meta:
@manual
When I access home page of www mobile site
And I go to login page from header
And I login by "facebook" with the following account
|Type    |Username          |Password|
|facebook|wunier@hotmail.com|tester  | 
Then I should login successfully

Scenario: Verify the gmail social login account linking
Meta:
@manual
Given I am a OD customer with the following OD account:
|Username        |Password |
|no_social_linked|welcome1 | 
And I have the following SNS account not linked to my OD account:
|Type |Username                  |Password |
|gmail|email.not.linked@gmail.com|odtesting| 
When I login by my "gmail" account
Then I should see link account page
And I should see message "We could't find your email address in our system, please login with your Office Depot account and then we will automatically link it!"
When I link my SNS account to my OD account
Then I should login successfully

Scenario: Verify the gmail social login account linking via email already exist in OD
Meta:
@manual
Given I am a OD customer with the following OD account:
|Username                        |Password |
|email.not.linked.exist@gmail.com|welcome1 | 
And I have the following SNS account not linked to my OD account:
|Type |Username                        |Password |
|gmail|email.not.linked.exist@gmail.com|odtesting| 
When I login by my "gmail" account
Then I should see link account page
And I should see message "Enter your credentials to link to your existing account" 
When I link my SNS account to my OD account
Then I should login successfully

Scenario: Verify the facebook social login account linking 
Meta:
@manual
Given I am a OD customer with the following OD account:
|Username        |Password |
|no_social_linked|welcome1 | 
And I have the following SNS account not linked to my OD account:
|Type    |Username                  |Password |
|facebook|email.not.linked@gmail.com|odtesting| 
When I login by my "facebook" account
And I link my SNS account with the following OD account
Then I should login successfully

Scenario: Verify the linkedin social login account linking
Meta:
@manual
Given I am a OD customer with the following OD account:
|Username        |Password |
|no_social_linked|welcome1 | 
And I have the following SNS account not linked to my OD account:
|Type    |Username                  |Password |
|linkedin|email.not.linked@gmail.com|odtesting| 
When I login by my "linkedin" account
And I link my SNS account with the following OD account
Then I should login successfully