!-- WWWMOBILE-4127

Narrative:
In order to know exactly which options I have near me if anything,
As a mobile site searching for specific in store services,
I want to have the opportunity to filter stores by services.


Scenario: Verify to refine with no result

GivenStories: ..\login\basic\mobile_without_login.story
When I request to find store from header
And I provide the zipcode: "33496"
And I find stores
And I request to refine the store search results
Then I should see the adjust results box
When I refine results with all services
Then I should be noticed that no results were found


Scenario: Verify to refine with services
Meta:
@manual

GivenStories: ..\login\basic\mobile_without_login.story
When I request to find store from header
And I provide the zipcode: "33496"
And I find stores
And I request to refine the store search results
Then I should see the adjust results box
When I refine result with any of service
Then I should see the store search results were refined by the selected service

