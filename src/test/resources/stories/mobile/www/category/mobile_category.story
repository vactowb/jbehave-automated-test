Scenario: Check tech category
When I access www mobile site
And I navigate to "Technology" product list
Then I should see the category list of "Technology"

Scenario: Verify to add the item to cart from category page
When I add a sku through category list
Then I should navigate on shopping cart page

Scenario: Check OfficeSupplies category
When I access home page from header
And I navigate to "Office Supplies" product list
Then I should see the category list of "Office Supplies"

Scenario: Check Furniture category
When I navigate to "Furniture" product list
Then I should see the category list of "Furniture"

Scenario: Check BreakRoom category
When I navigate to "Breakroom" product list
Then I should see the category list of "Breakroom"

Scenario: Check Cleaning category
When I navigate to "Cleaning" product list
Then I should see the category list of "Cleaning"

Scenario: Check Paper category
When I navigate to "Paper" product list
Then I should see the category list of "Paper"

