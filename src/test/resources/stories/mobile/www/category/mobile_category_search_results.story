Narrative:
In order to have a pleasant navigational experience,
As a mobile site user,
I want to be able to adapt my product browsing experience to the bandwidth I have.


Scenario: Verify the refine results
Meta:
@manual
GivenStories: ..\login\basic\mobile_without_login.story
When I search product with product name : "pens"
Then I should see the search results of "Pens"
When I refine search results with brands pilot
Then I should see the search results of "Pilot"


Scenario: Verify the search results pagination
Meta:
@manual
When I request to go to page "2" of search results
Then I should navigate on page "2" of search results
When I request to get next pagination
Then I should see the page navigator from page 5 to 8
When I request to get previous pagination
Then I should see the page navigator from page 1 to 4