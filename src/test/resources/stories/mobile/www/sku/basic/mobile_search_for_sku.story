Meta:
@basic

Scenario: Verify to search for sku

When I access home page of www mobile site
And I search for sku with the given sku id
Then I should see sku details page of the given sku
And I should see the sku is in status of "In Stock"