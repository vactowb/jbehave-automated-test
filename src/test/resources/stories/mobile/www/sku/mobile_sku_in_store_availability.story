!-- ODNA-1573

Narrative:
In order to see how many items are available by location,
As a mobile user,
I want to that I can choose a store to purchase my item from.

Meta:
@skuid 475806
@username od_it_tester
@password tester


Scenario: Verify to check in-store availbility for sku with invalid info via anonymous

GivenStories: ..\login\basic\mobile_without_login.story,
              basic\mobile_search_for_sku.story
When I request to check in-store availbility from sku details page
Then I should navigate on in-store availbility page for the expected sku
When I request to check availbility by "abc123"
Then I should notice that the check availbility button is not available


Scenario: Verify to check in-store availbility for sku with zip code via anonymous

When I request to check availbility by "33496"
Then I should see the store list of in-store availbility
!-- manual 
!-- And I should see the store details and in-store status are displayed as expected


Scenario: Verify to view store details from store list via anonymous

When I request to view store details from store list of in-store availbility
Then I should navigate on store details page


Scenario: Verify to check in-store availbility for sku with city and state via logged in

GivenStories: ..\login\basic\mobile_normal_login.story,
              basic\mobile_search_for_sku.story
When I request to check in-store availbility from sku details page
Then I should navigate on in-store availbility page for the expected sku
When I request to check availbility by "boca raton, FL"
Then I should see the store list of in-store availbility
!-- manual 
!-- And I should see the store details and in-store status are displayed as expected


Scenario: Verify to view store details from store list via logged in

When I request to view store details from store list of in-store availbility
Then I should navigate on store details page


Scenario: Verify to check in-store availbility of the stores nearby
Meta:
@manual
When I request to check in-store availbility for the stores nearby
Then I should see the store list of in-store availbility
And I should see the store details and in-store status are displayed as expected