!-- WWWMOBILE-4119

Narrative:
In order to signup for emails, follow OD on social media, and navigate to terms, policies, and the full site,
As a mobile user, 
I want to have a new footer to do that.


Scenario: Verify to sign up with invalid email

GivenStories: ..\login\basic\mobile_without_login.story
When I request to sign up with "abcdef@q" from footer
Then I should navigate on sign up page
And I should be noticed that the email or phone number is invalid


Scenario: Verify to sign up with valid email

GivenStories: ..\login\basic\mobile_without_login.story
When I request to sign up with "test@test.com" from footer
Then I should navigate on sign up page
And I should see the email is filled


Scenario: Verify to sign up with invalid phone number

GivenStories: ..\login\basic\mobile_without_login.story
When I request to sign up with "12345678" from footer
Then I should navigate on sign up page
And I should be noticed that the email or phone number is invalid


Scenario: Verify to sign up with valid phone number

GivenStories: ..\login\basic\mobile_without_login.story
When I request to sign up with "5612221212" from footer
Then I should navigate on sign up page
And I should see the phone number is filled


Scenario: Verify to view policy

GivenStories: ..\login\basic\mobile_without_login.story
When I request to view privacy policy from footer
Then I should navigate on privacy policy page


Scenario: Verify to view terms

When I request to view terms from footer
Then I should navigate on terms page


Scenario: Verify to navigate on full site

When I request to access full site from footer
Then I should navigate on full site
When I go back to mobile site
Then I should see the home page of mobile site


Scenario: Verify the social link
Meta:
@manual
GivenStories: ..\login\basic\mobile_without_login.story
When I request to access office depot social network from footer(facebook, twitter, linkedin, google+, path)
Then I should navigate on office depot social page