GivenStories: ..\login\basic\mobile_without_login.story

Scenario: Verify open contact us page successfully

When I request to access contact us from header
Then I should navigate on contact us page


Scenario: Verify without input to send email

When I send email without input
Then I should see attention message


Scenario: Verify input full items to send email

When I send email with input the full items
Then I should see contact us successful message


Scenario: Verify Terms link from header

When I request to access terms from header
Then I should navigate on terms page


Scenario: Verify Privacy link from header

When I request to access privacy policy from header
Then I should navigate on privacy policy page