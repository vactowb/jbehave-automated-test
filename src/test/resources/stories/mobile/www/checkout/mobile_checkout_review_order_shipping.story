!-- WWWMOBILE-3765
 
Narrative:
In order to be informed of the process and buy with confidence,
As a mobile site user, 
I want to be able to quickly change shipment.

Meta:
@username od_automation_3
@password tester
@skuid 315515

GivenStories: ..\login\basic\mobile_normal_login.story,
              ..\cart\basic\mobile_add_sku_to_cart.story
              

Scenario: Verify to edit default shipping info from review order

When I process check out from shopping cart
Then I should navigate on review order page
When I request to change shipping info from review order page
Then I should navigate on shipping options page
When I edit my default shipping address
And I update my shipping address with following info:
	| Field | Value       |
    | Email | test@od.com |
Then I should navigate on review order page


Scenario: Verify to change shipping info from review order

When I request to change shipping info from review order page
And I choose to ship to another address
Then I should navigate on review order page


Scenario: Verify to add new shipping address

When I request to enter new shipping address from review order page
Then I should navigate on new shipping address page
When I add new shipping address with following info:
    | Field        | Value               |
    | First Name   | test                |
    | Last Name    | test                |
    | Address 1    | 6600 N MILITARY TRL |
    | City         | Boca Raton          |
    | State        | FL - Florida        |
    | Post Code    | 33496               |
    | Phone Number | 5612222121          |
    | Email        | test@od.com         |
Then I should navigate on review order page
