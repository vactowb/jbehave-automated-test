!-- WWWMOBILE-3784, WWWMOBILE-3932, WWWMOBILE-3765, WWWMOBILE-4275, ODNA-14241

Narrative:
In order to be informed of the process and buy with confidence.
As a logged in mobile site user 
I want to have breadcrumbs that show me where I am in the process of checking out when I change/add ship to and payment info pages 

Meta:
@username od_automation_10
@password tester
@skuid 315515

GivenStories: ..\account\basic\mobile_delete_default_credit_card.story,
              ..\cart\basic\mobile_add_sku_to_cart.story

Scenario: Verify to add payment when check out

When I process check out from shopping cart
Then I should navigate on new payment page
And I should only see home and account options on main menu
When I add a new credit card with following info:
	| Field        | Value                   |
    | Nick Name    | RANDOM 10               |
    | Holder Name  | tester                  |
    | Card Number  | 4111111111111111        |
    | Month        | 12                      |
    | Year         | 2018                    |
    | Address      | 6600 N MILITARY TRL # 6 |
    | City         | Boca Raton              |
    | State        | FL - Florida            |
    | Post Code    | 33496                   |
Then I should navigate on review order page


Scenario: Verify to edit default payment from review order

When I requtest to edit my payment from review order page
And I update my credit card with following info:
	| Field  | Value |
    | Month  | 06    |
Then I should navigate on review order page


Scenario: Verify to change payment method from CVV light box

When I request to place order
Then I should be required to enter security code
When I request to change payment method from security code box
Then I should navigate on payment options page
When I edit my default credit card
And I update my credit card with following info:
	| Field  | Value |
    | Month  | 06    |
Then I should navigate on review order page


Scenario: Verify to place order

When I request to place order
And I enter my credit card security code
Then I should see my order is placed successfully


Scenario: Verify to view sku details from thank you page

When I request to view sku details from thank you page
Then I should see sku details page of the given sku


Scenario: Verify the CVV section if JS is disabled
Meta:
manual
Given No-JS setup for browser
When I navigate on review order page
Then I should see the CVV section on review order page
When I enter my credit card security code
And I request to place order
Then I should see my order is placed successfully