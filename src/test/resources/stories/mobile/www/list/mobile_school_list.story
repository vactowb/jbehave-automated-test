Narrative:
In order to ...
As a www mobile customer
I want to ... 

Scenario: Verify the school list on m-site

When I access www mobile site
And I access a school list
When I add items to cart
Then I should see cart page

Scenario: Verify the BTS landing page of grade 3 can show on m-site
When I access www mobile site
And I access a BTS link
When I choose grade 3
Then I see category under grade 3

