!-- ODNA-8879

Meta:
@username od_automation_csl
@password Tester1234
@skuid 315515
@wip
			  
Scenario: Verify to add to list without login

GivenStories: ..\login\basic\mobile_without_login.story,
			  ..\sku\basic\mobile_search_for_sku.story
Given I am a www mobile customer with the given account
When I add the sku to list
Then I should navigate on login page
When I login with my account
Then I should navigate on select shopping list page


Scenario: Verify to cancel add to list

When I cancel to add to list from select shopping list page
Then I should see sku details page of the given sku


Scenario: Verify to add to existing list

When I add the sku to list
And I add the sku to a existing list and set it as default from select shopping list page
Then I should navigate on the shopping list details page


Scenario: Verify the default list on select csl lighting box

GivenStories: ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
Then I should see the default list which is expected from select shopping list lighting box


Scenario: Verify to add to list via login

GivenStories: ..\login\basic\mobile_normal_login.story,
			  ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
Then I should see select shopping list lighting box
And I should see the default list which is expected from select shopping list lighting box


Scenario: Verify to cancel add to list from lighting box

When I cancel to add to list from select shopping list lighting box
Then I should see sku details page of the given sku


Scenario: Verify to add to existing list from lighting box

When I add the sku to list
And I add the sku to a existing list and set it as default from select shopping list lighting box
Then I should navigate on the shopping list details page


Scenario: Verify the default list on select csl page

GivenStories: ..\sku\basic\mobile_search_for_sku.story
When I add the sku to list
And I request to show more lists from select shopping list lighting box
Then I should navigate on select shopping list page
!-- And I should see the default list which is expected from select shopping list page