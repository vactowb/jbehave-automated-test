Narrative:
In order to change my password,
As a Msite customer with login, 
I want to change my password.

!-- WWWMOBILE-2973

Meta:
@username od_automation_2 
@password Tester1234

GivenStories: basic\mobile_goto_my_account.story

Scenario: Verify to change my password with invalid string

When I navigate to my profile page
And I navigate to change password page
Then I should see change password page
When I change my password with "tester"
Then I should be noticed that the password should contains at least 8 characters


Scenario: Verify to change my password with invalid string

When I change my password with "testertest"
Then I should be noticed that the password should contains at least one uppercase letter and one number



Scenario: Verify to change my password successfully 

When I change my password with "Tester1234"
Then I should see change password successful message and back to my account page
