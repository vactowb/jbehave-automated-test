Meta:
@username od_automation_2 
@password Tester1234

GivenStories: basic\mobile_delete_default_credit_card.story

Scenario: Verify to add new card

When I add a new credit card
And I fill with the following credit card information:
	| Field        | Value                   |
    | Holder Name  | tester                  |
    | Card Number  | 5105105105105100        |
    | Month        | 12                      |
    | Year         | 2018                    |
    | Address      | 6600 N MILITARY TRL # 6 |
    | City         | Boca Raton              |
    | State        | FL - Florida            |
    | Post Code    | 33496                   |
And I submit the new credit card
Then I should see the new credit card has been successfully added


Scenario: Verify to delete the added new card

When I edit the added credit card
And I delete this credit card
Then I should see the credit card has been successfully deleted

