Narrative:
In order to change my security questions,
As a Msite customer with login, 
I want to change my security questions.

!-- WWWMOBILE-2973

Meta:
@username od_automation_2 
@password Tester1234

GivenStories: basic\mobile_goto_my_account.story

Scenario: Verify change my security questions successfully 
When I navigate to my profile page
And I navigate to change security questions page
Then I should see change security questions page
When I change my security question
Then I should see change security questions successful message and back to my account page
