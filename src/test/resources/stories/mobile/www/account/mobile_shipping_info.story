Meta:
@username od_automation_2 
@password Tester1234


Scenario: Verify to cancel the edition of shipping information

GivenStories: basic\mobile_goto_my_account.story
When I edit my shipping information
Then I should see shipping options page


Scenario: Verify to cancel the edition of shipping address

GivenStories: basic\mobile_goto_my_account.story
When I edit my shipping information
And I edit my shipping address which in use
And I cancel to edit the shipping address
Then I should see shipping options page

Scenario: Verify to edit shipping information

When I edit my shipping address which in use
And I update my shipping address with the following information:
       |Field      |Value    |
       |First Name |RANDOM 5 |
       |Last Name  |RANDOM 8 |
Then I should see shipping options page


Scenario: Verify to change another shipping address
Meta:
@manual

GivenStories: basic\mobile_goto_my_account.story
When I edit my shipping information
And I change to use another shipping address
Then I should see my shipping address changed to another one

