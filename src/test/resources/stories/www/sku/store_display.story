
Scenario: Verify to go to sku detail page with the store NOT selected
Meta:
@skip
@product3 812295

Given I access WWW site

When I search for a product "812295"
Then I should see the sku page loaded for the product

Scenario: Verify to go to sku detail page with item  available and the store is selected
Meta:
@skip
@product1 315515

Given I access WWW site
When I click on stores icon
And I search the store list with zip code "33496"
And I choose the first one in the store list
Then My store should be set up correctly

When I search for a product "315515"
Then I should see the sku page loaded for the available product


Scenario: Verify to go to sku detail page with item NOT available and the store is selected
Meta:
@skip
@product2 812295

Given I access WWW site
When I click on stores icon
And I search the store list with zip code "33496"
And I choose the first one in the store list
Then My store should be set up correctly

When I search for a product "812295"
Then I should see the sku page loaded for the unavailable product
