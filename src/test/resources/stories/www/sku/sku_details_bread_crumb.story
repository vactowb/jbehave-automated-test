
Narrative:
In order to easily back to catories page from product details page bread srumb,
As a customer without login, 
I want to have a back button on bread scrumb.

Meta:
@skuid 315515

GivenStories: ..\login\basic\access_without_login.story,
			  ..\shopping\basic\search_for_common_sku.story

Scenario: Verify navigate to office supplies page from bread scrumb
When I navigate to office supplies page from bread scrumb
Then I should see office supplies categories page
