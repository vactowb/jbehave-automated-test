
Narrative:
In order to easily navigate to all category pages,
As a customer without login, 
I want to have a link in category page.

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_technology_categories.story

Meta:
@skip

Scenario: Verify link to technology categories result page successfully from view all items link
When I navigate to technology categories result page from view all items link
Then I should see the technology categories result page


