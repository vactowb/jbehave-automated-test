
Narrative:
In order to easily share my ad list to social,
As a customer without login, 
I want to have a place at the bottom of ad list item blocks in the ad list cartridges.

!-- WWWCU-26115

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_technology_categories.story

Scenario: Verify share ad list button can pull up a social network selection modal

Meta:
@skip

When I share the first ad list item in the ad list cartridges
Then I should see a social network selection modal window


Scenario: Verify i can share ad list item successfull

Meta:
@skip

When I log in with my facebook account
|Type    |Username          |Password|
|facebook|wunier@hotmail.com|tester  | 
Then I should see the share ad list item page
When I share the ad list item
Then I should see the ad list item page disappear

