
Narrative:
In order to easily navigate to audio page by show all categories,
As a customer without login, 
I want to have several links navigate to different categories page.

!-- Verify the MasterColumnContent_ShowAllCategories categories in N=5+9021

Scenario: Verify navigate to audio page by show all categories
Meta:
@skip

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_technology_categories.story
When I navigate to audio page from show all categories
Then I should see the audio page


