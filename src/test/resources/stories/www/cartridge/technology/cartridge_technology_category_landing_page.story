
Narrative:
In order to easily navigate to "Cameras & Camcorders" category landing page,
As a customer without login, 
I want to have a link in technology categories page.


Scenario: Navigate to "Cameras & Camcorders" category landing page successfully

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_technology_categories.story
Meta:
@skip

When I go to "Computer & Tablet Accessories" category landing page from Technology categories
Then I should see the "Computer & Tablet Accessories" category landing page
