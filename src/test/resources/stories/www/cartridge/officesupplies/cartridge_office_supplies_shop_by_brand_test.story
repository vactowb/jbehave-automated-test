
Narrative:
In order to easily shop by brand,
As a customer without login, 
I want to have a place in the left Nav Cartridge to shop by brand.

!-- Blocked by the new nav not available
Scenario: Verify navigate to scholastic browser page by left Nav Cartridge successfully
Meta:
@skip
GivenStories: ..\..\topnavigation\navigatetocategories\basic\topnavigation_office_supplies_categories.story
When I navigate to scholastic browser page from left nav cartridge functionality
Then I should see the scholastic browser page filtered by brand

