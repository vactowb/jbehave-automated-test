
Narrative:
In order to easily navigate to product detail page,
As a customer without login, 
I want to have a place in the categories page navigate to product detail page from RRZone.

GivenStories: ..\..\topnavigation\flyouts\topnavigation_officesupplies_flyout_navigate_to_basic_supplies_page.story

Scenario: Verify navigate to product detail page from RRZone
When I navigate to product details page from RRZone cartridge
Then I should see the product details page from RRZone cartridge
!-- Verify the MasterColumnContent_RRZone categories in N=5+1886
