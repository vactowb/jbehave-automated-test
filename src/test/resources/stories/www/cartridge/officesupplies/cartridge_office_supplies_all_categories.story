
Narrative:
In order to easily navigate to categories page,
As a customer without login, 
I want to have a place in the categories page navigate to another cotegories page.

GivenStories: basic/navigate_to_tape_and_adhesivers_page.story

Meta:
@skip

Scenario: Verify office supplies flyout text link navigate to the tape and adhesives categories page
Meta:
@manual
When I navigate to tape and adhesives page frome office supplies nav categories link
Then I should see the tape and adhesives categories page

Scenario: Verify navigate to another cotegories page by all categories cartridge
When I navigate to first categories page from all categories cartridge
Then I should see the first categories page from all categories cartridge
!-- Verify the MainColumnContent_AllCategories categories in N=5+371557