
Narrative:
In order to easily navigate to Deal Center categories page,
As a customer without login, 
I want to have a place in the adlist carousel navigate to Deal Center categories page.

!-- Verify the MainColumnContent_AdListCarousel categories in N=5+549384

GivenStories: ..\..\topnavigation\navigatetocategories\topnavigation_deals.story
Meta:
@skip

Scenario: Verify navigate to categories page by adlist carousel shop all link

When I navigate to categories page from shop all link
Then I should see the categories page


Scenario: Verify navigate to computers and tablets categories landing page by adlist carousel shop all computers and tablets link
Meta:
@skip
When I navigate to computers and tablets categories landing page from shop all computers and tablets link
Then I should see the computers and tablets categories landing page

Scenario: Verify navigate to the first computers and tablets categories page by category heading section
Meta:
@skip
!-- Verify the GraphicElement_CategoryHeadingSection cartridge in N=5+509612
When I navigate to the first computers and tablets categories page from category heading section
Then I should see the first computers and tablets categories page from category heading section