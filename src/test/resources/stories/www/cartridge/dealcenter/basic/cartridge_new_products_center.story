
Narrative:
In order to access new products center page,
As a customer without login, 
I want to access new products center page.


!-- Verify the MasterColumnContent_FeaturedCategoriesMatrixDealCenter categories in N=5+549383
Meta:
@basic

GivenStories: ../../../login/basic/access_without_login.story

Scenario: Verify navigate to new products center page by url
When I access new products center page
Then I should see the new products center page