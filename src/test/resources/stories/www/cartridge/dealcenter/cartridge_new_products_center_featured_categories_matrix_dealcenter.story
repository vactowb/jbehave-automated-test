
Narrative:
In order to easily navigate to categories page,
As a customer without login, 
I want to have a place in new products center page navigate to categories page.

!-- Verify the MasterColumnContent_FeaturedCategoriesMatrixDealCenter categories in N=5+549383

Meta:
@skip

GivenStories: basic/cartridge_new_products_center.story

Scenario: Verify navigate to categories page by featured categories matrix deal center
When I navigate to categories page from featured categories matrix deal center
Then I should see the categories page