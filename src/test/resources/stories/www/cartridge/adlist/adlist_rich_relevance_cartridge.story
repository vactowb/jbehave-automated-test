
Narrative:
In order to test adlist rich relevance cartridge,
As a customer without login, 
I want to see the adlist rich relevance cartridge.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to computer and  tablets categories page from url
When I navigate to computer and  tablets categories page from url from url
Then I should see the computer and  tablets categories page

Scenario: Verify filter the  computer and  tablets categories by refinement
When I filter the  computer and  tablets categories by the first type
Then I should see the first type computer and  tablets categories page

Scenario: Verify see the sku details page by the first rich relevance
When I navigate to the first sku details page by the first rich relevance
Then I should see the first rich relevance sku details page
