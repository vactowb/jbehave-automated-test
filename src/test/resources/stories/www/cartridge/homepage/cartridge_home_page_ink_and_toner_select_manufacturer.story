
Narrative:
In order to open selected manufacturer ink and toner page,
As a customer without login, 
I want to have a selecte manufacturer drop down in home page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to the selected manufacturer ink and toner page from selecte manufacturer
When I navigate to the selected manufacturer ink and toner page from selecte manufacturer by first brand
Then I should see all first manufacturer brands

