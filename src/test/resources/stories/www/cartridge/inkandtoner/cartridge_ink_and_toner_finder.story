
Narrative:
In order to find ink and toner cartridges easy,
As a customer with login, 
I want to have a link in category page.

Meta:
@username od_it_tester
@password tester

GivenStories: ..\..\login\basic\login_od_account.story
              
Scenario: Verify navigate to ink and toner categories page seccessful from ink and toner top navigation
When I navigate to ink and toner categories page from ink and toner top navigation
Then I should see ink and toner categories page

Scenario: Verify find all first brands from "Your Ink&Toner finder" select a printer brand successfully
When I select the first printer brand from select printer brand drop down
Then I should see all first brands

Scenario: Verify finde all "calculator" brands from "Your Ink&Toner finder" select a printer type successfully
When I select the first printer type from select printer type drop down
Then I should see all type brands

Scenario: Verify finde all "4615" brands from "Your Ink&Toner finder" select a printer model successfully
When I select the first printer model from select printer model drop down
Then I should see all model brands

Scenario: Verify click on the "Save Printer" link will open the name printer window
When I save this printer from save printer link
Then I should see the name the printer window
Then I should see the pre-populated name match the recent search name

Scenario: Verify there will be a "Duplicate printer name." message when I save a saved name
When I name the printer as a saved name "HP - A10 Printing Mailbox" in save printer text box
Then I should see the "Duplicate printer name." message in save my printer window

Scenario: Verify rename the printer name successfully
When I rename the printer name in save printer text box
Then I should see the name in the my saved printer drop down

Scenario: Verify click on the "Edit" link will open the manage printers window
When I manage saved printer from the "Edit" link
Then I should see the manage printers window open

Scenario: Verify click on the "Delete" link and delete the first saved printer
When I delete saved printer from saved printer "Delete" button
Then I should see the my saved printer been deleted