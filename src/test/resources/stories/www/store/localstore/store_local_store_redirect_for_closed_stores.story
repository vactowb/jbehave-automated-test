
Narrative:
In order to easily redirect local store page,
As a customer with login, 
I want to see the redirect 301 page.

Meta:
@username od_automation_2
@password Tester1234


Scenario: Verify redirect local store page for closed stores
GivenStories: ..\..\login\basic\login_od_account.story,
			           basic\stores_navigate_to_local_store_page.story
When I redirect local store page from url
Then I should see the no longer available page