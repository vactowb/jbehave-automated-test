
Narrative:
In order to easily manage office max stores,
As a customer without login, 
I want to see the office max stores from find a store page.

Meta:
@skip

Scenario: Verify show the office max stores from find a store page
GivenStories: ..\..\login\basic\access_without_login.story,
			           basic\store_navigate_to_find_a_store_page.story
When I choose office max stores from find a store page
Then I should see office max stores