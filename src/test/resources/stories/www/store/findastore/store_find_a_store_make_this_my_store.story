
Narrative:
In order to easily make this my stores,
As a customer without login, 
I want to make this my stores.

Meta:
@skip

Scenario: Verify make this my stores button is avaliable
GivenStories: ..\..\login\basic\access_without_login.story,
			           basic\store_navigate_to_find_a_store_page.story
When I choose the fist store as my store
Then I should navigate on local store page
Then I should see the choosed store is my store