Narrative:
In order to verify Password Strength Or Minimum Requirements of Password to be valid
As a WWW Account user, if i create new user with invalid password, 
I want to see error message for invalid password

Scenario: verify Password Strength Or Minimum Requirements of Password to be valid

GivenStories: ..\register\basic\register_through_checkout_basic.story
When I input all the required information with password: "tester"
Then I should be noticed that the passwords must: "Your password must meet minimum password requirements"
When I input all the required information with password: "tester1234"
Then I should be noticed that the passwords must: "Your password must meet minimum password requirements"
!-- Then I should be noticed that the passwords must: "contain 8-10 characters with at least one uppercase letter and one number"