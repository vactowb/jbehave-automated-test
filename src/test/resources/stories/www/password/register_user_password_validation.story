Narrative:
In order to verify Password Strength Or Minimum Requirements of Password to be valid
As a WWW Account user, if i create new user with invalid password, 
I want to see error message for invalid password

Scenario: verify Password Strength Or Minimum Requirements of Password to be valid

!-- This story need to be released in may'14 so skipping it as of now
GivenStories: ..\login\basic\access_make_sure_logout.story

!-- New Customer Registration
When I request to register from header
Then I should see register page
When I input all the required information with password: "tester"
Then I should see warning that password need meet mini requirement
When I input all the required information with password: "tester1234"
Then I should see warning that password need meet mini requirement