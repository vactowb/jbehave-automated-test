Meta:
@username od_automation_newdocs#test.com
@password Test1234

Scenario: Verify create newdocs order 
GivenStories: ..\login\basic\login_od_account.story
              
When I view my order history
When I search order by ordernumber: "336829934"
Then I could see the details of the order history record
When I reorder from the order details page
When I request to view the shopping cart from find your product page
Then I see the item "102842" has been added to cart
When I checkout from shopping cart
When I continue to checkout at checkout second step
When I continue to checkout at checkout third step
When I place order at checkout fourth step
Then I see order palced successful
Then I should see the order Details
Then I check the order history with the item "102842"





