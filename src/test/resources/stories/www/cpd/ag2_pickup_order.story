Narrative:
In order to see the order in RPF Printernet Queue,
As a WWW user,
I want to place an order a CPD Ag2 Delivery order from order history page

Meta:
@username CPDJBEHAVE
@password CpdAg2Sqs

Scenario: Verify Ag2 Delivery orders are in Printernet Queue[CPD-1963]

GivenStories: basic\reorder_cpd_ag2.story
When I request to view the shopping cart from find your product page
When I checkout as pickup
Then I see my order in associated Print Queue "Printernet"