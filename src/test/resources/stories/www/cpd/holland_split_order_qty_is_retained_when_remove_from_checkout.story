Meta:
@username od_automation
@password tester
@skuid 315515

Narrative:
In order to retain the quantity of a Holland item
As a WWW customer
I want to be able to remove another item on the checkout page
					 
Scenario: Verify that Holland quantity is retained for split order when other item is removed from Checkout page
GivenStories: ..\login\basic\login_od_account.story,
				  ..\shopping\basic\search_for_common_sku.story
When I add the sku to cart with quantity : "1"
Then I should view the shopping cart
When I use the vendorString "holland" and configId "BNV1-AD950F25" to build the vendor add to cart url
When I use the vendorString "taylor" and configId "DIV2-349C5E9F" to build the vendor add to cart url
When I checkout from shopping cart
Then I should see quantity at index "1" of "50"
When I click remove for sku "315515" which exists on index "0"
Then I should see quantity at index "1" of "50"