Scenario: varify lbl price for nowdocs sku

GivenStories: ..\login\basic\access_without_login.story

When I search sku from the header: "167067"
Then I should navigate to product details page
When I customize sku in Product Details page
Then I should see upload contain view
When I cancel upload a file
When I view sample file
Then I should see Lbl price in view sample file page and two prices match