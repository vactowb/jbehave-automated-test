Narrative:
This story aims to verify that the Holland Pricing is working after every new build. Go to site
find a Holland sku scrape price shown on OD site punchout scrape price from vendor site
Compare two prices (they should match)

Scenario: Verify Pricing is working fine for Holland Skus.
GivenStories: ..\login\basic\access_without_login.story

When I search sku from the header: "798189"
Then I should navigate to product details page
When I customize sku in Product Details page
Then I should see price in "Holland" configurator and compare it to the sku price