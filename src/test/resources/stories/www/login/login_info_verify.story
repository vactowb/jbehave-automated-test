Narrative:
In order to verify not me link occur,
As a customer with login and log out, 
I want to log out my account and verify not me link.
Meta:
@username od_automation_2
@password Tester1234

Scenario: Log out the account to suppress gigya header
GivenStories: ..\login\basic\login_od_account.story
When I logout from header account
Then I should not see the user info left

Scenario: verify not me link when i log out."This feature has been remove"
Meta:
@manual
GivenStories: basic\login_od_account.story,
			  basic\log_out_od_account.story
Then I should see not me link occur
When I open login page with not me link
Then I should see login page

Scenario: Create the new exempt account by existing od account
Meta:
@manual

GivenStories: ..\login\basic\access_without_login.story
When I navigate to the Exempt Account page
And I create the new exempt account with following data:
	| billToId | Phone1 | Phone2 | Phone3 | EMail         | firstName | lastName | loginName  | passWord |
	| 59153019 | 660    | 238    | 7785   | tester@od.com | xyz       | qwe      | random     | tester   |
And I logout from header account
Then I should not see my greeting information