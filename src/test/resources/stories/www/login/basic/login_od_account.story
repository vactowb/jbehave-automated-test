Meta:
@basic

Scenario: Login with an OD customer account

Given I am an OD customer with the given account
And I access WWW site
When I request to login from header
Then I should login successfully from header