Narrative:
In order to log in quickly every time.,
As a customer, 
I want to keep entering my social password after i've already authenticated the first time.[WWWCU-25907]

Scenario: Verify to login in with gigya website
Given I access WWW site
!-- binding OD_AUTOMATION_8
When I login by facebook with the following account:
|Username          |Password|
|wunier@hotmail.com|tester  | 
Then I should login successfully from Gigay

Scenario: Unable to login through a previously linked Gigya login,if change username on OD account[WWWCU-30140]
Meta:
@manual
GivenStories: basic\login_od_account.story
When I navigate to My Account Overview
When I navigate to login setting page
Then I should open the login setting page
When I change the login name at login setting page
When I logout from header account
When I login by facebook with the following account:
|Username          |Password|
|wunier@hotmail.com|tester  | 
Then I should login fault from Gigay with error message displayed

Scenario: Verify to click on the social provider they logged in with first and we should log them in right away
Meta:
@manual
When I logout from homepage
When I click on the social provider facebook
Then I should login successfully from Gigay

Scenario: Verify to click on the social provider from new header
Meta:
@manual
When I logout from homepage
When I click on the social provider facebook from homepage
Then I should login successfully from Gigay
