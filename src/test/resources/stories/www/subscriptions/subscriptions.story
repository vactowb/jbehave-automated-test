Scenario: The subscripted sku should be added to shopping cart
Meta:
@manual
GivenStories: ..\login\basic\access_without_login.story
When I search for SKU with specific SKU description: "Construction Paper"
Then I can get the searched result for : "Construction Paper"
When I select one sku to Subscribe
And I request to add the sku to cart
Then I can navigate to the subscription detail page
When I choose the frequency filter to "Monthly"
And I add the subscription to cart
Then I should see "1" of the subscritpion added to cart


Scenario: As a LogIn account , should go to subscript manager page directely
Meta:
@manual
Meta:
@username od_automation_6 
@password tester

GivenStories: ..\login\basic\login_od_account.story
When I check my subscription manager
Then I could redirect to the subscription manager page


Scenario: The subscripted sku should be added to shopping cart and checkout
Meta:
@manual
When I search for SKU with specific SKU description: "Construction Paper"
Then I can get the searched result for : "Construction Paper"
When I select one sku to Subscribe
And I request to add the sku to cart
Then I can navigate to the subscription detail page
When I choose the frequency filter to "Monthly"
And I add the subscription to cart
Then I should see "1" of the subscritpion added to cart


Scenario: Verify to check out with subscriptions
Meta:
@manual
When I navigate to shopping cart page
And I checkout from shopping cart
And I continue to checkout at checkout second step
When I accept the teams and conditions
And I continue to checkout at checkout third step
And I place order at checkout fourth step
Then I see order palced successful


Scenario: The non-subscripted sku should be transfermed to subscripted one
Meta:
@manual
Given I am an OD customer with the following account:
	| User Name    | Password |
	| od_it_tester | tester   | 
And I access WWW site
When I login with my account
And I search for SKU with specific SKU description: "Ballpoint Stick Pens"
Then I can get the searched result for : "Ballpoint Stick Pens"
When I add one sku is non-Subscribe into cart


Scenario: Add NonSubscribed Items To Cart Convert To Subscription and Update To Split and Checkout Resulting In Split Orders
Meta:
@manual
Given I am an OD customer with the following account:
	| User Name    | Password |
	| od_it_tester | tester   | 
And I access WWW site
When I login with my account
And I search for SKU with specific SKU description: "Ballpoint Stick Pens"
Then I can get the searched result for : "Ballpoint Stick Pens"
When I input the quntity "7" in one item
And I add the sku to cart from catalog search page


Scenario: As a account having several subscritpion itmes, should navigate to next page at subscript manager page
Meta:
@manual
Given I am an OD customer with the following account: 
	| User Name    | Password |
	| srujan_sqm6  | tester   | 
And I access WWW site
When I login with my account
And I check my subscription manager
And I navigate to the next page of subscription list
Then I can get the link to return first page


Scenario: As a account having several subscritpion itmes, should navigate to previous page at subscript manager page
Meta:
@manual
Given I am an OD customer with the following account:
	| User Name    | Password |
	| srujan_sqm6  | tester   | 
And I access WWW site
When I login with my account
And I check my subscription manager
And I navigate to the next page of subscription list
And I navigate to the previous page of subscription list
Then I cannot get to previous page


Scenario: As a account having several subscritpion itmes, should navigate to first page at subscript manager page
Meta:
@manual
Given I am an OD customer with the following account:
	| User Name    | Password |
	| srujan_sqm6  | tester   | 
And I access WWW site
When I login with my account
And I check my subscription manager
And I navigate to the next page of subscription list
And I navigate to the first page of subscription list
Then I cannot get to previous page


Scenario: Account could go through the subscription items by monthly frequency
Meta:
@manual
Given I am an OD customer with the following account:
	| User Name    | Password |
	| srujan_sqm6  | tester   | 
And I access WWW site
When I login with my account
And I check my subscription manager
And Set frequency filter to "Monthly"
Then The filter condition is correct with "Monthly"


Scenario: Account could go through the subscription items by every other month frequency
Meta:
@manual
Given I am an OD customer with the following account:
	| User Name    | Password |
	| srujan_sqm6  | tester   | 
And I access WWW site
When I login with my account
And I check my subscription manager
And Set frequency filter to "Every other month"
Then The filter condition is correct with "Every other month"