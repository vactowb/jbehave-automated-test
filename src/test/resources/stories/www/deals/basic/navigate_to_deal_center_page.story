Meta:
@basic
@skip

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to deal center page successfully

When I request to view "Deals" from header
And I go to deal center from deals view
Then I should see the deal center page
