
Narrative:
In order to easily find sale items,
As a customer, 
I want to have a place in the header to show deals, catalogs and other seasonal events.

GivenStories: ..\..\login\basic\access_without_login.story
Meta:
@skip

Scenario: Verify to go to weekly ad

When I request to view "Deals" from header
And I go to shop weekly ad from deals view
Then I should see to weekly ad page


Scenario: Verify to view seasonal savings
Meta:
@manual
When I request to view "Deals" from header
And I request to view the seasonal savings from deals view
Then I should navigate to back to school supplies page


Scenario: Verify to request catalog

When I request to view "Deals" from header
And I request a catalog from deals view
Then I should navigate to online catalog page