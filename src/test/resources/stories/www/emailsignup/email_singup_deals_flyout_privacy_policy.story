
Narrative:
In order to navigate to Privacy Statement page,
As a customer without login, 
I want to have a link navigate to Privacy Statement page from deals flyout.

GivenStories: ..\login\basic\access_without_login.story


Scenario: Verify navigate to privacy statement page from deals flyout
When I navigate to privacy statement page from deals flyout
Then I should see the privacy statement page