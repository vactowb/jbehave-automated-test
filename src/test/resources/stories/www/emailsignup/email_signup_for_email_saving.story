
Narrative:
In order to save email sign up,
As a customer without login, 
I want to save my email for sign up.

GivenStories: ..\login\basic\access_without_login.story
Meta:
@skip

Scenario: Verify navigate to email sign up page from deals flyout
When I want to save my email for sign up
Then I should see the save successfully message