Meta:
@username od_add_min_requirement
@password Test1234

Scenario:  if a product has a min qty or qty increment requirement on all pre-order pages and be restricted from purchasing the incorrect qty
Meta:
@skip
GivenStories: ..\..\login\basic\login_od_account.story

!-- When I search sku from the header: "min_requirement_sku"
!-- When I add the sku to cart with quantity : "1"
!-- Then I should see error message need meet the minimum quantity
When I search sku from the header: "min_requirement_sku"
When I add the sku to cart with quantity : "2"
Then I should see this sku has min qty and qty increment requirement

When I update the sku quantity "1" from cart
Then I should see error message need meet the minimum quantity
When I delete the first item from the order
Then I see the item "min_requirement_sku" has been deleted

Scenario: Verify to add any other SKU to cart from order
Meta:
@skip
When I add item "min_requirement_sku" from the order
Then I should see error message need meet the minimum quantity

