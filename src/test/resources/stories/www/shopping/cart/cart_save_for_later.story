
Scenario: Verify "Save for Later" behavior

!-- WWWCU-26985
GivenStories: ..\..\login\basic\access_without_login.story
Meta:
@manual
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I save for later the sku from shopping cart
When I register from the login page
Then I should see save for Later list is displayed
Then I see the item "common_sku" has been added to cart




