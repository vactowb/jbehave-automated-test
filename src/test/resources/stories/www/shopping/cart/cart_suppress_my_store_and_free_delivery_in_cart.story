Narrative:
In order to go to the store to pick up my order
As a user, 
I want to see the my store feature in my cart when I have items selected for pick up.[WWWCU-25139&&WWWCU-25927]

Meta:
@username od_automation_suppressstore#lt.com
@password tester

Scenario: Verify to Suppress the my store functionality in the cart unless there is are items selected for pick up 
GivenStories: ..\..\login\basic\login_od_account.story
              
When I search sku from the header: "common_sku"
And I add the sku to cart from sku details page
Then I should see the sku "common_sku" is in Delivery mode
And I should not see the my store feature in my cart
And I should see free delivery messaging at top of cart


Scenario: Verify that free delivery message disappear when cart has no delivery item and has pick up only item 
When I search sku from the header: "pickup_only_sku"
When I request to add the sku to cart
Then I should see the OMax and OD stores that have the In Store Pick-Up flag
When I choose a store on the Product Detail page
And I request to view the shopping cart from find your product page
And I select the Pickup mode and only for the sku "common_sku"
Then I should see the sku "common_sku" is in pickup mode
And I should see the my store feature in my cart
And I should not see free delivery messaging at top of cart



