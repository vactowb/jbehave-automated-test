Meta:
@username od_automation_3 
@password tester
@skuid 315515
@basic

Scenario: Verify to add sku to cart by anonymous access

GivenStories: ..\..\..\login\basic\login_od_account.story,
              ..\..\basic\search_for_common_sku.story
When I add the sku to cart from sku details page
Then I should view the shopping cart
And I should see the sku "315515" added to my shopping cart

