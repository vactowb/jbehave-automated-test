Meta:
@username auto_reid
@password tester
@skuid 534494
!-- od_automation_backorder#lt.com 35242
Scenario: Verify full backorder

GivenStories: ..\..\login\basic\login_od_account.story
!-- ..\basic\search_for_common_sku.story
!-- When I request to add the sku to cart with quantity : "9000"
!-- Then I should see the out of stock message
When I checkout from home page
When I add item "534494" order by item "9999"
Then I should see Available & Backorder

Scenario:  As a user, I dont want to see the backorder line unless I have backorders in my cart so that I dont get confused.[WWWCU-25892]
Meta:
@skip
When I continually add the out of stock sku to cart
Then I should see Available & Backorder

Scenario: Verify not see the backorder line
GivenStories: ..\..\login\basic\log_out_od_account.story
GivenStories: ..\..\login\basic\login_od_account.story
When I search sku from the header: "315515"
And I add the sku to cart from sku details page
Then I should not see Available & Backorder


