Narrative:
In order to go make tax exempt option more visible
As a user, 
I want to make the tax exempt option more visible so that users wont have trouble finding it.[WWWCU-25483]

Meta:
@username od_automation_taxexempt#lt.com
@password tester

Scenario: Verify to make the tax exempt option more visible so that users wont have trouble finding it
GivenStories: ..\..\login\basic\login_od_account.story
              
When I search sku from the header: "404321"
And I add the sku to cart from sku details page
Then I should see Estimate Shipping & Taxes link
When I want to display the current tax exempt link
Then I should see the Estimated Sales Tax
And I should see the Estimated Shipping Charges
And I should not see Estimate Shipping & Taxes link

Scenario: Verify to make the tax exempt option more visible and  more for free delivery
When I search sku from the header: "772141"
And I add the sku to cart with quantity : "100"
Then I should see Estimate Shipping & Taxes link
When I want to display the current tax exempt link
Then I should see the Estimated Sales Tax
And I should not see Estimate Shipping & Taxes link




