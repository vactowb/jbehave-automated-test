
Meta:
@bannerTerm backpack
@noBannerTerm glue

Scenario: Display search banner in configured in Experience Manager template when searching
Given I access WWW site
When I search with term which has search banner
Then I should see the search page with search banner

Scenario: No search banner displayed when banner for terms not configured in experience manager for banner
When I search with term which has no search banner
Then I should see the search page with no search banner


