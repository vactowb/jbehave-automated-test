
Narrative:
In order to easily manage my online shopping lists,
As a customer, 
I want to see My Lists to show in the new header design.

Meta:
@username one_list
@password tester
@skuid 315515

GivenStories: ..\..\..\login\basic\login_od_account.story,
              ..\basic\add_sku_to_shopping_list.story

Scenario: Verify to view my lists via only one list for my account
 
Given I have no created shopping list
When I request to view "My Lists" from header
Then I should see my shopping list "Save For Later" in My Lists view
And I should see my saved items in My Lists view


Scenario: Verify to view sku details from my lists

When I request to view the sku details from my lists
Then I should navigate to product details page


Scenario: Verify to manage lists from my lists

When I request to view "My Lists" from header
And I request to manage lists from My Lists view
Then I should navigate to my shopping lists page


Scenario: Verify to view list from my lists

When I request to view "My Lists" from header
And I request to view list from My Lists view
Then I should see the details of my shopping list "Save For Later"

Scenario: Verify to remove item  from my lists

When I select all list items
When I remove the selected items
Then I should see all the list items has been deleted


Scenario: Verify to create list from my lists

When I request to view "My Lists" from header
And I request to create a new list from My Lists view
Then I should navigate to my shopping lists page