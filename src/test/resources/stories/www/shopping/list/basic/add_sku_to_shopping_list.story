Meta:
@basic

Scenario: Verify to add sku to shopping list

GivenStories: ..\..\basic\search_for_common_sku.story
When I add the sku to my list
And I add sku to an existed personal shopping list
Then I should see added success message
When I continue shopping