
Narrative:
In order to easily navigate to order by item page,
As a customer without login, 
I want to have a link in the gray bar navigate to order by item page.

GivenStories: ..\login\basic\access_without_login.story

Scenario: Verify navigate to order by item page seccessful from gray bar order by item link
When I navigate to order by item page from gray bar order by item link
Then I should see the order by item page