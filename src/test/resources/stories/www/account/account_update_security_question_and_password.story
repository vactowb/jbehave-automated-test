
Narrative:
In order to update security question and password,
As a customer with login and has set up security question, 
I want to have a page to update security question and my password.

Meta:
@username od_automation_12
@password Test1234

GivenStories: ..\login\basic\login_od_account.story

When I navigate to my account login page
Then I should see my account login page
When I change login settings
And I update security question
Then I should see my account page