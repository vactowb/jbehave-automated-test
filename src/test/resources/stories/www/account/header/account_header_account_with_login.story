
Narrative:
In order to easily manage my account,
As a customer with login, 
I want to see the header account to show in the new header design.

Meta:
@username od_automation_2
@password Tester1234

GivenStories: ..\..\login\basic\login_od_account.story

Scenario: Verify to view header account show "welcome,First Name"

When I request to view welcome information from header account
Then I should see welcome information in header account

Scenario: Verify to navigate account overview page from header account
When I navigate to account overview page from header account
Then I should see account overview page

Scenario: Verify to navigate order tracking and history page from header account
When I navigate to tracking and history page from header account
Then I should see tracking and history page

Scenario: Verify to navigate subscriptions manager page from header account
When I navigate to subscriptions manager page from header account
Then I should see subscriptions manager page

Scenario: Verify to navigate payment options page from header account
When I navigate to payment options page from header account
Then I should see payment options page

Scenario: Verify to navigate submit return page from header account
When I navigate to submit return page from header account
Then I should see submit return page

Scenario: Verify to navigate reorder page from header account
When I navigate to reorder page from header account
Then I should see reorder page

Scenario: Verify to logout from header account
When I logout from header account
And I request to view without welcome information from header account
Then I should see without welcome information in header account



