

Narrative:
In order to easily manage my account,
As a customer without login, 
I want to see the header account to show in the new header design.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify to view header account show "welcome"
When I request to view without welcome information from header account
Then I should see without welcome information in header account

Scenario: Verify to open account login window from header account
When I open account login window from header account
Then I should see account login window

Scenario: Verify to navigate create an account page from header account
When I navigate to create an account page from header account
Then I should see create an account page

Scenario: Verify to navigate order tracking page from header account
When I navigate to order tracking page from header account
Then I should see order tracking page
