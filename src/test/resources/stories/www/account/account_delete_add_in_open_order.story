Meta:
@username auto_reid
@password tester

GivenStories: ..\login\basic\login_od_account.story
         
Scenario: Verify delete the address which is contained in an open order
When I navigate to My Account Overview
When I navigate to find order in My Account Overview page
When I search for open status order
Then I should see the result for orders
When I navigate to one open status order and record the address
When I navigate to My Account Overview
When I navigate to Shipping Address section
Then I should see there is no edit link for the address in open order