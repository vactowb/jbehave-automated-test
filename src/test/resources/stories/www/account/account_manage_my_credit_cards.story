Meta:
@username od_automation_12
@password Test1234

Scenario: Verify to add credit card

GivenStories: ..\login\basic\login_od_account.story
When I navigate to My Account Overview
When I navigate to manage credit cards
Then I should navigate to manage credit cards page
When I add a new card from my account page
|CardHolderName|Address    |City          |State|ZipCode|
|odAutomationTester|6600 N MILITARY TRL # 6|BOCA RATON|FL - Florida  | 33496|
Then I should see the credit card saved successfully
When I edit the credit card from my account page
Then I should see the credit card edit successfully

Scenario: Verify to delete the credit card

When I delete the credit card from my account page
Then I should see the credit card deleted successfully
