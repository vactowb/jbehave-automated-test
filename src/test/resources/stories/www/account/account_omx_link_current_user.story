Meta:
@username DAVISTEX@SWBELL.NET
@password Tester1
@skip
Scenario: Verify to merge OMX account to current user

GivenStories: ..\login\basic\login_od_account.story
When I navigate to My Account Overview
When I link OMX account to OD account in My Account Overview page
Then I should see the account linking dialog in My Account Overview page