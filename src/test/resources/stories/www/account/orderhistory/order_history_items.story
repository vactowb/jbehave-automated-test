
Meta:
@username od_automation_orderhistory#test.com
@password tester

GivenStories: ..\..\login\basic\login_od_account.story

Scenario: Verify the check all items function

When I view my order history
When I request to show all items
When I reorder from show all items tab
Then I should see error message prompt must specify which one want to reorder
When I select all items
Then I should see all items are selected
When I deselect all items
Then I should see all items are deselected

Scenario: Verify if check the checkbox the Reorder qty is same as the last order qty

When I select the first order history record
Then I see the Reorder qty is same as the last order qty
When I reorder from show all items tab
!-- When I request to view the shopping cart
When I request to view the shopping cart from find your product page
Then I should see the qty is the same in cart

Scenario: Verify the check all items function

When I view my order history
When I request to show all items
When I select the first order history record with a invalid number
When I reorder from show all items tab
Then I should see find your product page
!-- Then I should see error message with invalid qty to reorder



