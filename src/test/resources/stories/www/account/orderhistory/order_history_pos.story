Meta:
@username user_www
@password tester
@manual

GivenStories: ..\..\login\basic\login_od_account.story

Scenario: Verify the search status function
Meta:
@manual
When I view my order history
And I search orders by order type:"Store Purchase"
Then I see the order type is shown as "Store Purchase"

Scenario: Verify to view the record link of order history
Meta:
@manual
When I choose one of the order history
Then I could see the details of the order history record

Then I should see the barcode digits for printing on the order details page

Scenario: Verify to view the record function of order history
Meta:
@manual
When I view my order history
And I search orders by order type:"Store Purchase"
Then I see the order type is shown as "Store Purchase"
When I choose one of the order history record
Then I should see the order items on find your product page

Scenario: Verify to record from show all items page
Meta:
@manual
GivenStories: ..\..\login\basic\access_make_sure_logout.story
When I login from header hover my account
|Username          |Password|
|auto_reid |tester  | 
When I view my order history
When I request to show all items
When I select sku "315515" from show all items page
When I select sku "781386 " from show all items page
When I reorder from show all items tab
Then I should see the proucts added successfully

Scenario: Verify a bar code that is visible for printing on the order details page 
Meta:
@manual
Then I should see the bar code for printing on the order details page