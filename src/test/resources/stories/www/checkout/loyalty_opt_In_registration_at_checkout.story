Narrative:
In order to start earning rewards
As a new WWW customer
I want to be able to opt in for a loyalty account if I register during checkout, so that I can start earning rewards

Scenario: Opt in for Loyalty Account while registration
GivenStories: ..\login\basic\access_without_login.story
When I trigger composite steps to add the common sku to shopping cart
When I create a new account
Then I see loyalty opt in option