Meta:
@username od_add_customerpo
@password tester

Scenario: Verify to use my loyalty Member # at checkout so that I accrue MaxPerks points for my Office Depot order.[ODNA-6491]

GivenStories: ..\login\basic\login_od_account.story
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I checkout from shopping cart
When I continue to checkout at checkout second step

Then I should see linked office depot rewards number
When I select to apply this order to a different Office Depot Rewards or MaxPerks number
Then I should see Office Depot Rewards or MaxPerks number inputbox
When I enter Office Depot Rewards or MaxPerks number
When I apply the MaxPers number

Scenario: Verify to add custom po number  
When I enter a customer PO number at checkout third step
When I continue to checkout at checkout third step
Then I should see the customer PO number at the thank page
When I place order at checkout fourth step
Then I see order palced successful
Then I should see the customer PO number at the thank page
Then I should see Office Depot Rewards or MaxPerks number given