Meta:
@username od_automation_giftcard
@password Test1234

Scenario: Verify to add a new giftcard
!-- The gift card is not work
GivenStories: ..\..\login\basic\login_od_account.story
When I trigger composite steps to add the common sku to shopping cart
And I continue to checkout at checkout second step
And I add and apply a new gift card "4111111111122223" : "1234"
Then I see the new gift card applied
When I remove the gift card at checkout third step
Then I see the gift card has been removed
Then I should navigate to checkout third step

Scenario: Verify anonymous check out add a new giftcard
GivenStories: ..\..\login\basic\access_make_sure_logout.story,
					 ..\basic\checkout_with_anonymous.story
When I fill all mandatory info at checkout second step
And I continue to checkout at checkout second step
Then I should navigate to checkout third step
When I add and apply a new gift card "4111111111122223" : "1234"
Then I see the new gift card applied
When I remove the gift card at checkout third step
Then I should navigate to checkout third step
Then I see the gift card has been removed