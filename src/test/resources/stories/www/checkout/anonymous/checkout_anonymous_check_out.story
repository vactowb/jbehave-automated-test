Scenario: Verify anonymous check out
GivenStories:  ..\basic\checkout_with_anonymous.story
When I fill all mandatory info at checkout second step
And I continue to checkout at checkout second step
Then I should navigate to checkout third step
When I choose to pay by given credit card
And I continue to checkout at checkout third step without input CVV

Scenario: Verify anonymous check out while editing the payment information[WWWCU-22017]
When I edit the payment information at checkout fourth step
Then I should see the cvv blank at checkout third step

Scenario: Verify anonymous check out successfully
When I choose to pay by given credit card
When I continue to checkout at checkout third step without input CVV
Then I should see order summary at checkout fourth step
When I place order at checkout fourth step
Then I should see the order Details
Then I should see order summary the same as checkout fourth step page

Scenario: Verify anonymous check out tracking order

When I delete all cookies
When I go to the order tracking page
When I track my order with the order number
Then I could see the details of the order history record

