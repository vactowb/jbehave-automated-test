Meta:
@username od_automation_payment
@password tester

Scenario: Verify to check out with payment of Cash

GivenStories: ..\basic\checkout_with_pickup_mode_mixed_mode.story

When I continue to checkout at checkout second step
And I choose to pay by: "Cash"
And I continue to checkout at checkout third step
Then I should see payment method is "Cash"
When I place order at checkout fourth step
Then I see order palced successful


Scenario: Verify to check out with payment of Check

GivenStories: ..\basic\checkout_with_pickup_mode_mixed_mode.story
   
When I continue to checkout at checkout second step
And I choose to pay by: "Check"
And I continue to checkout at checkout third step
Then I should see payment method is "Check"
When I place order at checkout fourth step
Then I see order palced successful
