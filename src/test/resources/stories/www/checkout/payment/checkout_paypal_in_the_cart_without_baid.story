Meta:
@username checkout_test2
@password Test1234
@skuid 315515

Scenario: Verify to punch out to PayPal in the cart without baid

GivenStories: ..\..\login\basic\login_od_account.story, 
              ..\..\shopping\basic\search_for_common_sku.story
When I add the sku to cart from sku details page
And I punch out to PayPal from shopping cart
Then I should see the PayPal page opened successful
When Log in the paypal with:
	|Username                |Password|
	|odtester@officedepot.com|odtester|
Then I should return the page at checkout fourth step
When I donot save my billing information
And I place order at checkout fourth step
Then I see order palced successful

Scenario: Verify to punch out to PayPal in the cart as anonymous

GivenStories:  ..\..\login\basic\access_make_sure_logout.story,
			..\..\login\basic\access_without_login.story,
             ..\..\shopping\basic\search_for_common_sku.story
When I add the sku to cart from sku details page
And I punch out to PayPal from shopping cart
Then I should see the PayPal page opened successful
When Log in the paypal with:
	|Username                |Password|
    |odtester@officedepot.com|odtester|
Then I should return the page at checkout fourth step
When I place order at checkout fourth step
Then I see order palced successful