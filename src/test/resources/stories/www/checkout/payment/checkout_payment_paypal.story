Meta:
@username od_automation_7
@password tester

Scenario: Verify to check out with payment of paypal

GivenStories: ..\..\login\basic\login_od_account.story, 
              ..\basic\set_payment_as_credit_card.story
              
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I checkout from shopping cart
When I continue to checkout at checkout second step
When I continue to checkout at checkout third step
When I change payment at checkout fourth step
When I choose to pay by paypal
When I continue to checkout at checkout third step
Then I should return the page at checkout fourth step
Then I should see payment method is paypal
When I place order at checkout fourth step
Then I see order palced successful
