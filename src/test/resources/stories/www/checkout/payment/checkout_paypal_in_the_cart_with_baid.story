Meta:
@username checkout_test_withid
@password tester
@skuid 315515

Scenario: Verify to punch out to PayPal in the cart with baid

GivenStories: ..\..\login\basic\login_od_account.story, 
              ..\..\shopping\basic\search_for_common_sku.story
When I add the sku to cart from sku details page
And I punch out to PayPal from shopping cart
Then I should return the page at checkout fourth step
When I place order at checkout fourth step
Then I see order palced successful