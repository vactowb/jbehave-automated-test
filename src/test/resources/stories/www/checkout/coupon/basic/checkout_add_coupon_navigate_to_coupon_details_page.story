
Narrative:
In order to easily navigate to coupon details page from coupons box,
As a customer with login, 
I want to have a coupon link from the coupon box.
Meta:
@basic

Scenario: Verify navigate to coupon details page by see offer details button
GivenStories: checkout_add_coupon.story
When I navigate to coupon details page from see offer details button
Then I should see the coupon details page