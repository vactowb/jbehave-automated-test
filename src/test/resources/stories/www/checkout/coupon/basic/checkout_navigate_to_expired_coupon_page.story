
Narrative:
In order to easily navigate to expired coupon page from url,
As a customer with login, 
I want to navigate to expired coupon page.


Meta:
@basic

GivenStories: ..\..\..\login\basic\access_without_login.story

Scenario: Verify navigate to expired coupon page buy url
When I navigate to expired coupon page from url
Then I should see the expired coupon page