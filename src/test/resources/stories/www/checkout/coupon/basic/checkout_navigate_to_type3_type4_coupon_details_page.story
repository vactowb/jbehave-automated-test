
Narrative:
In order to easily navigate to type 3 and type 4 coupon page from url,
As a customer with login, 
I want to navigate to type 3 and type 4 coupon page.

Meta:
@basic

GivenStories: ..\..\..\login\basic\access_without_login.story

Scenario: Verify navigate to type3 and type4 coupon details page buy url
When I navigate to type3 and type4 coupon details page from url
Then I should see the type3 and type4 coupon details page