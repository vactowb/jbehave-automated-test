
Scenario: Verify to add coupon to cart
Meta:
@basic

GivenStories: ..\..\..\login\basic\login_od_account.story,
			  ..\..\..\shopping\basic\search_for_common_sku.story
When I add several skus into cart
When I ensure coupon is not applied and add it into cart
Then I should see the coupon has been added to cart

