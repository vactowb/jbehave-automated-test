
Narrative:
In order to easily add product to shopping cart,
As a customer without login, 
I want to have a button on coupon details page.

Meta:
@username od_coupon_2a#tester.com
@password Tester1234
@skuid 315515
@coupon 84842752
@skip

GivenStories: basic\checkout_add_coupon_navigate_to_coupon_details_page.story

Scenario: Verify met a message when select a product add to cart
When I select a product
Then I should see a message indicate the necessary number

Scenario: Verify add enough products to shopping cart successfully
When I select enough products
Then I should see the necessary number message disappear
When I add all products to cart
Then I should see the item add to cart successful