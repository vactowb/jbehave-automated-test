
Narrative:
In order to easily navigate to coupon page for coupons with free or discounted items,
As a customer without login, 
I want to have a button from the added coupon.

Meta:
@username od_coupon_1a#tester.com
@password Test1234
@skuid 315515
@coupon 19233241
@skip

GivenStories: basic\checkout_add_coupon_navigate_to_coupon_details_page.story

Scenario: Verify defaults navigate to the homepage from shop now button
When I navigate to the home page from shop now button
Then I should see the home page by default