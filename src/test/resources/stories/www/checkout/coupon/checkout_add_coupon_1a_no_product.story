Narrative:
In order to easily navigate to coupon page for coupons with free or discounted items,
As a customer with login, 
I want to have a shop now button from the coupon details page.


Meta:
@username 1a_no_product#tester.com
@password Test1234
@skuid 315515
@coupon 25719548
@skip

GivenStories: basic\checkout_add_coupon_navigate_to_coupon_details_page.story

Scenario: Verify defaults navigate to the homepage from shop now button
When I navigate to the coupon page from no product shop now button
Then I should see the home page by default