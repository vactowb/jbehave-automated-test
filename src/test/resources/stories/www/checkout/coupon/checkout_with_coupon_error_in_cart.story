Meta:
@username od_checkout_couponerror#test.com
@password tester
@coupon 21258225 
!--34110828 

Scenario: Verify to checkout with coupon error in cart
Meta:
@wip
GivenStories: ..\..\login\basic\login_od_account.story
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I ensure coupon is not applied and add it into cart
Then I should see the coupon error in cart
When I checkout from shopping cart
Then I should see the coupon error in cart
When I set the cookie enableAutoCouponRemovalCookie as true
When I checkout from shopping cart
Then I should go to checkout second step page
When I continue to checkout at checkout second step

Scenario: Verify to add coupon at checkout third step page
Meta:
@wip
When I add coupon at checkout third step page
Then I should see the coupon error in cart
When I checkout from shopping cart
Then I should go to checkout second step page

