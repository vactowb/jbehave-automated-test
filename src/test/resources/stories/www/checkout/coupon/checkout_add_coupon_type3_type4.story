
Narrative:
In order to easily add type 3 and type 4 coupon number,
As a customer with login, 
I want to have a type 3 and type 4 coupon details page to add coupon number.

GivenStories: basic\checkout_navigate_to_type3_type4_coupon_details_page.story	
Meta:
@skip

Scenario: Verify defaults navigate to the homepage from shop now button
When I navigate to the home page from shop now button
Then I should see the home page by default

Scenario: Verify type3 and type4 coupon add  to cart successfully
When I navigate to shopping cart page from header cart
Then I should see the type3 and type4 coupon add to cart



