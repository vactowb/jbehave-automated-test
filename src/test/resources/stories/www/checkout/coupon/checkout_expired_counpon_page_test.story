
Narrative:
In order to easily navigate to Coupon Center page and Deal Center page,
As a customer with login, 
I want to navigate to Coupon Center page and Deal Center page from Deal Center Button and Online Coupon Button.

GivenStories: basic\checkout_navigate_to_expired_coupon_page.story
Scenario: Verify navigate to deal center page by carousel title link 
When I navigate to deal center page from carousel title link
Then I should see the deal center page


Scenario: Verify navigate to deal center page by deal center button
GivenStories: ..\..\login\basic\access_without_login.story,
			  basic\checkout_navigate_to_expired_coupon_page.story		  
Meta:
@skip
When I navigate to deal center page from deal center button
Then I should see the deal center page

Scenario: Verify navigate to coupon center page by online coupon button
GivenStories: basic\checkout_navigate_to_expired_coupon_page.story
Meta:
@skip
When I navigate to coupon center page from online coupon button
Then I should see the coupon center page from expired coupon page