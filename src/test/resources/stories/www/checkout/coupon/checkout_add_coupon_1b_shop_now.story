
Narrative:
In order to easily navigate to coupon page for coupons with free or discounted items,
As a customer without login, 
I want to have a button from the added coupon.

Meta:
@username od_coupon_1b#test.com
@password Tester1234
@skuid 315515
@coupon 703389784
@skip

GivenStories: basic\checkout_add_coupon.story

Scenario: Verify navigate to coupon details page from see offer coupon describe link
When I navigate to coupon details page from see offer coupon describe link
Then I should see the coupon details page

Scenario: Verify navigate to coupons with free or discounted items by shop now button
When I navigate to the assignable destination page from shop now button
Then I should see the assignable destination page for coupons with free or discounted items