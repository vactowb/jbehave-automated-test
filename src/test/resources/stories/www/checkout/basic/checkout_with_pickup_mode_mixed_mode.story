Meta:
@basic

Scenario: Verify to check out from shopping cart in mixed mode

GivenStories: ..\..\login\basic\access_make_sure_logout.story,..\..\login\basic\login_od_account.story
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I select the Pickup mode for one sku "common_sku"
When I checkout from shopping cart
Then I should see the shipping option is pickup in mixed mode