Meta:
@basic

Scenario: Verify the anonymous checkout

GivenStories: ..\..\login\basic\access_without_login.story
When I trigger composite steps to add the common sku to shopping cart
When I continue checkout as a guest
Then I should see shipment summary