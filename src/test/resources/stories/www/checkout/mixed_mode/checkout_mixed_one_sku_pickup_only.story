Meta:
@username od_it_mixedmode
@password tester

Scenario: Verify to add the SKU is pick up only to cart
!-- pick-up only sku 105159
GivenStories: ..\..\login\basic\login_od_account.story
!-- When I make the shopping cart in mixed mode
When I trigger composite steps to add pickup sku: "pickup_only_sku"
When I choose a store on the Product Detail page
Then I should see find your product page
When I request to view the shopping cart from find your product page
Then I should view the shipping details of item "pickup_only_sku" is no "Delivery"

Scenario: Verify to apply in-store pickup to all items in your cart and change sku mode for all 

When I trigger composite steps to add sku to shopping cart: "common_sku"
And I select the Pickup mode for the sku "common_sku"
Then I should see the sku "common_sku" is in pickup mode
When I select the Delivery mode for the sku "common_sku"
Then I should see the sku "common_sku" is in Delivery mode
Then I should view the shipping details of item "pickup_only_sku" is no "Delivery"

Scenario: Verify to apply in-store pickup to the item self in your cart and change sku mode only
When I select the Pickup mode and only for the sku "common_sku"
Then I should see the sku "common_sku" is in pickup mode
Then I should view the shipping details of item "pickup_only_sku" is no "Delivery"

Scenario: Verify who will pick up the order
When I checkout from shopping cart
When I change someone to pick up the order
Then I should see who will pick up the order

Scenario: Verify Display store brand (OD and/or OMax) icon for store pick in Step 2 and 4 pages
Then I should see store brand icon displayed for store pick-up
When I continue to checkout at checkout second step
When I continue to checkout at checkout third step
Then I should see store brand icon displayed for store pick-up
When I place order at checkout fourth step
Then I see order palced successful
Then I should see who will pick up the order at the thank page
