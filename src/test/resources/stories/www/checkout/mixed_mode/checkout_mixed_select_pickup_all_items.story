Meta:
@username od_it_mixedmode_1
@password tester

Scenario: Verify to apply in-store pickup to the items self in your cart 
GivenStories: ..\..\login\basic\login_od_account.story

When I trigger composite steps to add sku to shopping cart: "common_sku2"
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I select the Pickup mode for the sku "common_sku2"
Then I should see the sku "common_sku2" is in pickup mode
Then I should see the sku "common_sku" is in pickup mode

Scenario: Verify to apply in-store pickup to the all items in your cart 
When I empty the shopping cart
When I search sku from the header: "common_sku"
When I add the sku to cart from sku details page
When I trigger composite steps to add sku to shopping cart: "common_sku2"

When I select the Pickup mode and only for the sku "common_sku2"
Then I should see the sku "common_sku2" is in pickup mode
Then I should see the sku "common_sku" is in Delivery mode

Scenario: Verify who will pick up the order
When I checkout from shopping cart
When I change someone to pick up the order
Then I should see who will pick up the order
When I continue to checkout at checkout second step
When I continue to checkout at checkout third step
When I place order at checkout fourth step
Then I see order palced successful
Then I should see who will pick up the order at the thank page