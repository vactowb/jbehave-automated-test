!-- WWWCU-31309
Scenario: Join office depot rewards with Existing OD and  new ODR Enrollment
GivenStories: ..\basic\open_rewards_welcome_page.story
When I join the rewards with existing od account
When I login to link office depot account
|Username          |Password|
|auto_test_join1@test.com|Test1234  | 
When I access join and active welcome page
When I join the rewards with od account
Then I should notice that all required fields has been filled out
|Username          |Address              |PhoneNumber         |
|test@officedepot.com|6600 N MILITARY TRL # 6   |5611231234     |

Scenario: Join office depot rewards with invalid OD
GivenStories: ..\..\login\basic\access_make_sure_logout.story,
						 ..\basic\open_rewards_welcome_page.story
When I join the rewards with existing od account
When I login to link office depot account
|Username          |Password|
|auto_test_join1@test.com|Test12345  | 
Then I should see the error warn your login name or password is incorrect
When I login to link office depot account
|Username          |Password|
|auto_test_join1@test.com|Test1234  | 
When I access join and active welcome page
When I join the rewards with od account
Then I should notice that all required fields has been filled out
|Username          |Address|PhoneNumber|
|test@officedepot.com|6600 N MILITARY TRL # 6   |5611231234     |
When I make the email address as blank
Then I notice error for blank email is displayed

Scenario: No error is displayed when logging in with a user already linked
GivenStories: ..\..\login\basic\access_make_sure_logout.story,
						 ..\basic\open_rewards_welcome_page.story
When I join the rewards with existing od account
When I login to link office depot account
|Username          |Password|
|odtest@163.com|tester  | 
Then I should notice activate rewards account is already linked
!-- Then I should notice activate rewards account successfuly
