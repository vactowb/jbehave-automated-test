!-- ODNA-383
Scenario: Logging in from My Account hover always pops up 'Please verify your info' modal
GivenStories: ..\basic\open_rewards_welcome_page.story
When I login from header hover my account
|Username          |Password|
|auto_test_join1@test.com|Test1234  | 
When I access join and active welcome page
Then I should notice welcome firstname is displayed

Scenario: ODR A weird page gets displayed after logging in from My Account hover
GivenStories: ..\..\login\basic\access_make_sure_logout.story,
                   ..\basic\open_rewards_welcome_page.story
When I join the rewards with new od account
When I continue to join rewards
Then I should notice that all required fields must be filled out
When I login from header hover my account
|Username          |Password|
|auto_test_join1@test.com|Test1234  | 
When I access join and active welcome page
When I join the rewards with od account
Then I should notice that all required fields has been filled out
|Username          |Address              |PhoneNumber         |
|test@officedepot.com|6600 N MILITARY TRL # 6   |5611231234     |
