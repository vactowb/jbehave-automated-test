!-- ODNA-383
Meta:
@username auto_test_join1
@password Test1234
Scenario: "Do you have an OD account?" is displayed for a user who is already logged in
GivenStories: ..\..\login\basic\login_od_account.story,
					  ..\basic\open_rewards_welcome_page.story
When I join the rewards with od account
Then I should notice that all required fields has been filled out
|Username          |Address              |PhoneNumber         |
|test@officedepot.com|6600 N MILITARY TRL # 6   |5611231234     |
When I activate with the member id "1111111" at the join page
Then I should see that the member id you entered invalid
