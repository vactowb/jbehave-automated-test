!-- ODNA-6490
Scenario: As an ODR member without a saved MP account, I want to have loyalty options at checkout so I can accrue points on Office Depot Rewards or MaxPerks, for my purchase
GivenStories:  ..\..\checkout\basic\checkout_with_anonymous.story
When I fill all mandatory info at checkout second step
And I continue to checkout at checkout second step
Then I should navigate to checkout third step
When I choose to pay by given credit card
Then I should see Office Depot Rewards or MaxPerks number inputbox
When I enter an invalid Office Depot Rewards or MaxPerks number
Then I should see Your Member Number is incorrect
When I enter Office Depot Rewards or MaxPerks number
When I continue to checkout at checkout third step without input CVV
Then I should see order summary at checkout fourth step
When I place order at checkout fourth step
Then I should see the order Details
Then I should see Office Depot Rewards or MaxPerks number given