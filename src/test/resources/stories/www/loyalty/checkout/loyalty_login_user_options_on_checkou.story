Meta:
@username MAHESH.VIRA#OD1.COM
@password Tester123

Scenario: Verify to Changing the payment info makes MP as the default even when ODR was entered

GivenStories: ..\..\login\basic\login_od_account.story
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I checkout from shopping cart
When I continue to checkout at checkout second step
!-- Then I should see linked office max rewards number
!-- When I select to apply this order to a different Office Depot Rewards or MaxPerks number
When I enter Office Depot Rewards or MaxPerks number
When I continue to checkout at checkout third step
Then I should see Office Depot Rewards or MaxPerks number given
When I change payment at checkout fourth step
When I continue to checkout at checkout third step
Then I should return the page at checkout fourth step
Then I should see Office Depot Rewards or MaxPerks number given