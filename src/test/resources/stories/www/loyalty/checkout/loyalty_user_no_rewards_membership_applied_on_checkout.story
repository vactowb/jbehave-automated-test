Meta:
@username auto_no_rewards#test.com
@password Test1234

Scenario: Verify to Changing the payment info makes MP as the default even when ODR was entered
GivenStories: ..\..\login\basic\login_od_account.story
When I trigger composite steps to add sku to shopping cart: "common_sku"
When I checkout from shopping cart
When I continue to checkout at checkout second step
When I continue to checkout at checkout third step
Then I should return the page at checkout fourth step
When I enter member number at checkout fourth step
When I place order at checkout fourth step
Then I should see Office Depot Rewards or MaxPerks number given