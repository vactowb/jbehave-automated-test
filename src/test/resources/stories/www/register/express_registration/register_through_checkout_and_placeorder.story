Scenario: Verify register other account,the shopping cart is not empty[WWWCU-25330]

GivenStories: ..\basic\register_through_checkout_basic.story
When I register with all the required information
Then I should go to checkout second step page
When I update the shipping information with express reg
When I continue to checkout at checkout second step
Then I should navigate to checkout third step

Scenario: Verify place order
When I choose to pay by given credit card
When I continue to checkout at checkout third step without input CVV
When I place order at checkout fourth step
Then I see order palced successful
