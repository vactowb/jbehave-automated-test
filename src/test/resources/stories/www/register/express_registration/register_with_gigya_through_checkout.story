Narrative:
In order to easily  register,
As a customer without login, 
I want to activates registration from checkout, pop a express reg modal.[WWWCU-23907]

Scenario: Verify to link to gigya website

GivenStories: ..\basic\register_through_checkout_basic.story
When I open the gigya website
Then I should see gigya website opened

Scenario: Verify the facebook social login with OD account linked 
Meta:
@manual
GivenStories: basic\register_through_checkout_basic.story
When I open the gigya website
And I login by "facebook" with the following account
|Type    |Username          |Password|
|facebook|wunier@hotmail.com|tester  | 
Then I should login successfully

Scenario: Verify the facebook social register a new account 
Meta:
@manual
GivenStories: basic\register_through_checkout_basic.story
When I open the gigya website
And I have the following SNS account not linked to my OD account:
|Type |Username                  |Password |
|facebook|wunier@hotmail.com|tester  | 
When I link my SNS account to create a new OD account
Then I should see the inputting shipping info page
When I input all the shipping info 
Then I should register successfully