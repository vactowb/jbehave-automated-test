Narrative:
In order to easily  register,
As a customer without login, 
I want to activates registration from checkout, pop a express reg modal.[WWWCU-23907]


Scenario: Verify to create a new account from checkout with all the needed information
GivenStories: ..\basic\register_through_checkout.story
When I register with all the required information
Then I should return to the shopping cart
!-- And I should see logout link on the page of header

Scenario: Verify to select the option 'Sign up to be alerted for the newest sales and exclusive promotions'
Meta:
@skip
Then I should see receive promotions selected

Scenario: Verify to unselect the option 'Sign up to be alerted for the newest sales and exclusive promotions'
Meta:
@skip
GivenStories: ..\basic\register_through_checkout.story
When I input all the required information with not selected ODMarketingEmails
Then I should return to the shopping cart
And I should see logout link on the page of header
And I should see receive promotions unselected

Scenario: Verify to change login setting
When I navigate to My Account Overview
When I navigate to login setting page
Then I should open the login setting page
When I change the login name at login setting page
When I logout from header account
When I re-login when I changed login name
Then I should login successfully from header



