Narrative:
In order to be able to remaining shipping information,
As a new customer, 
I want to remaining Information on checkout.[WWWCU-25030]

Scenario: Verify remaining Information on checkout.

GivenStories: ..\basic\register_with_all_the_required_information.story

When I trigger composite steps to add the common sku to shopping cart
Then I should see shipping information has filled with account info with express reg
When I update the shipping information with express reg
And I continue to checkout at checkout second step
Then I should navigate to checkout third step

Scenario: Verify to fill valid credit card info on checkout
When I choose to pay by given credit card
When I save credit card information to my account
And I continue to checkout at checkout third step without input CVV
And I place order at checkout fourth step
Then I see order palced successful

