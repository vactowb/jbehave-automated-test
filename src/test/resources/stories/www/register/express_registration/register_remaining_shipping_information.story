Narrative:
In order to be able to save time during checkout,
As a new customer, 
I want to fully register with Office Depot on the my account page.[WWWCU-19663]

Scenario: Verify remaining Information in the my account page.

GivenStories: ..\basic\register_with_all_the_required_information.story

Then I should not see the paymentInfo on my account overview page
When I edit my shipping information from my account overview page
Then I should see shipping information has filled with account info
When I update the shipping information
Then I should return to my account overview page
Then I should see the paymentInfo on my account overview page
Then I should see my billing address is the same

Scenario: Verify phone number is not blank at Account Settings page[WWWCU-29564]
When I edit contact information on my account overview page
Then I should see the phone number is not blank at account settings page

Scenario: verify to register and logout to my overview page to remain information in the my account page 
Meta:
@skip
GivenStories: ..\basic\register_with_all_the_required_information.story
When I logout and re-login
Then I should return to my account overview page


