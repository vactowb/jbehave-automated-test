Narrative:
In order to easily  register,
As a customer without login, 
I want to activates registration from checkout, pop a express reg modal.[WWWCU-23907]

GivenStories: ..\basic\register_through_checkout_basic.story
Scenario: Verify unselect security question and create account fail

When I input all the required information with unselect security question
Then I see register page with error message: "Choose a security question"

Scenario: Verify input blank zip code and create account fail

When I input all the required information with blank zip code
Then I see register page with error message: "enter a zip code"

Scenario: Verify input wrong confirm password and create account fail

When I input all the required information with wrong confirm password
Then I should be noticed that the passwords do not match

Scenario: Verify to register with password less than 8 characters

When I input all the required information with password less than 8 characters
Then I should be noticed that the passwords must be at least 8 characters


Scenario:Verify input invalid zip code and create account fail

When I input all the required information with invalid zip code "123"
Then I should be noticed that the zip code is invalid

Scenario: Verify input invalid email address and create account fail

When I input all the required information with invalid email "it@itab"
Then I should be noticed that the email address is invalid


Scenario: Verify input used email address and create account fail

When I input all the required information with invalid email "hao.chen2@officedepot.com"
Then I should be noticed that the email has already in used




