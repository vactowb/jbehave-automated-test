Scenario: Verify Error message will pop in a modal format[WWWCU-28313]
GivenStories: ..\basic\register_through_checkout_basic.story
When I register with all the required information
Then I should go to checkout second step page
When I fill all info at checkout second step with invalid address for express reg user
|Address    |City          |State|
|13621 Carlton St.|Mashpee|MA - Massachusetts  | 
When I continue to checkout at checkout second step
Then I should see error message will pop in checkout second step page for express reg: "13621 Carlton St."
When I edit the invalid address from error message pop
Then I should go to checkout second step page
When I continue to checkout at checkout second step
Then I should see error message will pop in checkout second step page for express reg: "13621 Carlton St."
When I continue with the invalid address from error message pop
Then I should navigate to checkout third step
Then I should see the billing information is not blank

Scenario: Verify place order with invalid billing address
Meta:
@manual
!-- invalid billing address can check out succ
When I fill all info at checkout third step with address for express reg user
|Address    |City          |State|
|13621 Carlton St.|Mashpee|MA - Massachusetts  | 
When I choose to pay by given credit card
When I continue to checkout at checkout third step without input CVV
Then I should see error message will pop in checkout third step page for express reg: "The billing address you entered was not recognized by our address standardization database"

Scenario: Verify place order

When I choose to pay by given credit card
When I continue to checkout at checkout third step without input CVV
When I place order at checkout fourth step
Then I see order palced successful
