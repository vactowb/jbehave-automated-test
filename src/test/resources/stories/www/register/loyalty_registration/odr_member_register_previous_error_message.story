Scenario: Verify look up my member id with the link on the Reg page

GivenStories: ..\basic\register_input_od_required_information.story
When I input invalid odr member from reg page: "12345"
Then I should see the forgotLoy error message member id is invalid
When I look up my odr member id with a valid phone number
Then I should see the odr member id being found
Then I should not see the forgotLoy error message member id is invalid