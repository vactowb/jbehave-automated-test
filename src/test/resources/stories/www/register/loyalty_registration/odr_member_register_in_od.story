
Scenario: Verify look up my member id with the link on the Reg page

GivenStories: ..\basic\register_input_od_required_information.story
When I look up my odr member id with a invalid phone number
Then I should see the forgotLoy error message phone number is invalid
When I look up my odr member id with a used phone number
Then I should see the forgotLoy error message phone number is being used
When I look up my odr member id with a valid phone number
Then I should see the odr member id being found

Scenario: Verify look up my member id with the link on the Reg page

GivenStories: ..\basic\register_input_od_required_information.story
When I input invalid odr member from reg page: "12345"
Then I should see the forgotLoy error message member id is invalid
When I input invalid odr member from reg page: "1800016261"
Then I should see the forgotLoy error message member id is used

Scenario: Verify look up my member id with the link on the Reg page
GivenStories: ..\basic\register_input_od_required_information.story
Then I see loyalty opt in option
When I join rewards when register a new account with noninput anything
Then I should see required input when join rewards as register
When I join rewards when register a new account with phone: "12-45-56"
Then I should see invalid input phone when join rewards as register
When I join rewards when register a new account with phone: "452-454-7854"
Then I should see linked input phone when join rewards as register

Scenario: Verify join reward on the Reg page
Meta:
@skip
When I join rewards when register new account
Then I should see a welcome message on my account overview page
Then I should see a member number on my account overview page