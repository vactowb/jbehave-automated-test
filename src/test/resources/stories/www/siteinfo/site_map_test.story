
Narrative:
In order to easily navigate to site map page,
As a customer, 
I want to see the site map page.


GivenStories: ..\login\basic\access_without_login.story

Scenario: Verify navigate to site map page successfully
When I navigate to site map page
Then I should see the site map page