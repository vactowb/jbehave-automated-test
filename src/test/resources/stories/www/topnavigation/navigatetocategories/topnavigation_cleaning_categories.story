
Narrative:
In order to easily navigate to cleaning page,
As a customer without login, 
I want to have a place in the top navigate fast link to cleaning page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to cleaning page seccessful from breakroom  top navigation
When I navigate to cleaning page from cleaning top navigation
Then I should see the cleaning page