
Narrative:
In order to easily navigate to deal center page,
As a customer without login, 
I want to have a place in the top navigate fast link to deal center page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to deal center page seccessful from deals top navigation
When I navigate to deal center page from deals top navigation
Then I should see the deal center page