
Narrative:
In order to easily navigate to ink and toner categories page,
As a customer without login, 
I want to have a place in the top navigate fast link to ink and toner categories page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to ink and toner categories page seccessful from ink and toner top navigation
When I navigate to ink and toner categories page from ink and toner top navigation
Then I should see ink and toner categories page