
Narrative:
In order to easily navigate to break room page,
As a customer without login, 
I want to have a place in the top navigate fast link to break room page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to break room page seccessful from breakroom  top navigation
When I navigate to break room page from breakroom top navigation
Then I should see the break room page