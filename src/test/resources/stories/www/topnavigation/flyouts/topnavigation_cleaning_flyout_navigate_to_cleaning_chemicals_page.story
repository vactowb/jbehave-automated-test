
Narrative:
In order to navigate to cleaning chemicals page easily
As a od customer without login
I want to have a place navigate to deals cleaning chemicals page from cleaning flyout


GivenStories: ..\..\login\basic\access_without_login.story
Meta:
@skip

Scenario: Verify navigate to cleaning chemicals page successful from cleaning flyout
When I navigate to cleaning chemicals page from cleaning flyout
Then I should see the cleaning chemicals page