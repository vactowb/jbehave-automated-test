
Narrative:
In order to easily navigate to recommended link page,
As a customer without login, 
I want to have a place in the new top navigate fast link to recommended link page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify office supplies flyout text link navigate to the Basic Supplies categories
When I navigate to basic supplies categories page frome office supplies flyout text link
Then I should see the basic supplies categories page


Scenario: Verify recommended link on top navigation is available
Meta:
@skip

When I navigate to recommended link page frome office supplies new top navigation
Then I should see the paper recommended link page
