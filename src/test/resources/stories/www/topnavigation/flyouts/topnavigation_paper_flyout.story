
Narrative:
In order to easily navigate to paper information page,
As a customer without login, 
I want to have a place in the new top navigate fast link to paper information page.


GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify top categories link successfull navigate to top categories papers page
Meta:
@manual
When I navigate to top categories papers page from top categories link
Then I should see top categories papers page

Scenario: Verify brands link successfull navigate to brands page
Meta:
@manual
When I navigate to paper brands page from paper brands link
Then I should see paper brands page


