
Narrative:
In order to navigate to deals page easily
As a od customer without login
I want to have a place navigate to deals page

GivenStories: ..\..\login\basic\access_without_login.story

Meta:
@skip

Scenario: Verify navigate to on sale page successful from deals flyout
Given I set new on sale icon ABtest cookie
When I navigate to on sale page from deals flyout
Then I should see the deal center page
