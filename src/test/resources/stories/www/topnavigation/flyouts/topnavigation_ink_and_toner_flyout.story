
Narrative:
In order to easily navigate to ink and toner information page,
As a customer without login, 
I want to have a place in the new top navigate fast link to ink and toner information page.

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify search ink and toner by Cartridge number from ink and toner flyout successful
Given I set new ink and toner flyout cookie "inkTonerFlyoutABTest" on home page
When I search "990291" by search cartridge number from ink and toner flyout
Then I should see the "990291" cartridge details page

Scenario: Verify search ink and toner by Printer Model from ink and toner flyout successful
When I search "hp" by search printer model from ink and toner flyout
Then I should see ink and toner search result page




