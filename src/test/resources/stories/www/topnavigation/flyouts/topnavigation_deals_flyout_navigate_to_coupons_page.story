
Narrative:
In order to navigate to deals page easily
As a od customer without login
I want to have a place navigate to deals page

Meta:
@wip

GivenStories: ..\..\login\basic\access_without_login.story

Scenario: Verify navigate to coupons page successful from deals flyout
When I navigate to coupons page from deals flyout
Then I should see the coupons page