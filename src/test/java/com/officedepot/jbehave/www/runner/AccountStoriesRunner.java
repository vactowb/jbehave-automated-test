package com.officedepot.jbehave.www.runner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import com.officedepot.jbehave.www.WWWBaseRunner;
import com.officedepot.test.common.annotation.Brand;
import com.officedepot.test.common.annotation.Locale;
import com.officedepot.test.common.model.Site;

@Brand(Site.OD)
@Locale(language = "en", country = "US")
public class AccountStoriesRunner extends WWWBaseRunner {

	public AccountStoriesRunner() {
		// configuredEmbedder().useExecutorService(new FixedThreadExecutors()
		// .create(configuredEmbedder()
		// .embedderControls()
		// .doGenerateViewAfterStories(true)
		// .doIgnoreFailureInStories(false)
		// .doIgnoreFailureInView(false)
		// .doVerboseFailures(true)
		// .doVerboseFiltering(true)
		// .useStoryTimeoutInSecs(600)));
	}

	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();
		return new StoryFinder().findPaths(codeLocation, asList("**/stories/www/**/account_delete_add_in_open_order.story"), asList(""));
	}
}
