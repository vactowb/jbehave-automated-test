package com.officedepot.jbehave.www.runner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import com.officedepot.jbehave.www.WWWBaseRunner;
import com.officedepot.test.common.annotation.Brand;
import com.officedepot.test.common.annotation.Locale;
import com.officedepot.test.common.model.Site;


@Brand(Site.OD)
@Locale(language="en", country="US")
public class SkuLowThresholdRunner extends WWWBaseRunner {
	
	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();
		return new StoryFinder().findPaths(codeLocation, asList("**/stories/www/category/skuthreshold/*.story"), asList(""));
	}

}
