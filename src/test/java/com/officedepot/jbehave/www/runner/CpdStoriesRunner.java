package com.officedepot.jbehave.www.runner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import com.officedepot.jbehave.www.WWWBaseRunner;
import com.officedepot.test.common.annotation.Brand;
import com.officedepot.test.common.annotation.Locale;
import com.officedepot.test.common.model.Site;

@Locale(language="en", country="US")
@Brand(Site.OD)
public class CpdStoriesRunner extends WWWBaseRunner {
	
    public CpdStoriesRunner() {
    }
     
    @Override
    protected List<String> storyPaths() {
    	// Specify story paths as URLs
        String codeLocation = codeLocationFromClass(this.getClass()).getFile();
        return new StoryFinder().findPaths(codeLocation, asList("**/stories/www/cpd/*.story"), asList(""));
    }
	
}
