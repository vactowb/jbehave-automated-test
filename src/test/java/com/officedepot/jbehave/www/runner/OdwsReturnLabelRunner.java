package com.officedepot.jbehave.www.runner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import com.officedepot.jbehave.www.WWWBaseRunner;
import com.officedepot.test.common.annotation.Brand;
import com.officedepot.test.common.annotation.Locale;
import com.officedepot.test.common.model.Site;
import com.officedepot.test.jbehave.annotation.Browsers;
import com.officedepot.test.jbehave.model.Browser;

/**
 * @author s-jattiprakash
 * NOTE: If these Tests fail in your local thats because the delay in loading labels, 
 * for that you have have to un-comment the "Thread.sleep(3000);" code to run successfully. 
 *
 */
@Locale(language="en", country="US")
@Brand(Site.OD)
public class OdwsReturnLabelRunner extends WWWBaseRunner {
	
	public OdwsReturnLabelRunner() {
	}

	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();
        return new StoryFinder().findPaths(codeLocation, asList("**/stories/www/odws/*.story"), asList(""));
	}

}
