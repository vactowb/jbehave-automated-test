package com.officedepot.jbehave.www;

import com.officedepot.test.jbehave.BaseRunner;

public abstract class WWWBaseRunner extends BaseRunner {

	@Override
	protected String[] getStepsBasePackages() {
		return new String[]{"com.officedepot.jbehave.www.steps"};
	}
	
}
