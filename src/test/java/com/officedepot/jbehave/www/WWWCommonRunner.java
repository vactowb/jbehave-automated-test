package com.officedepot.jbehave.www;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import com.officedepot.test.common.annotation.Brand;
import com.officedepot.test.common.annotation.Locale;
import com.officedepot.test.common.model.Site;
import com.officedepot.test.jbehave.annotation.Browsers;
import com.officedepot.test.jbehave.model.Browser;

@Brand(Site.OD)
@Locale(language="en", country="US")
public class WWWCommonRunner extends WWWBaseRunner{

	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();
		return new StoryFinder().findPaths(codeLocation, asList("**/stories/www/account/account_delete_add_in_open_order.story"), asList("**/stories/www/**/basic/*.story"));
	}

}
