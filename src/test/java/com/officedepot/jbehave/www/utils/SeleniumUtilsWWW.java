package com.officedepot.jbehave.www.utils;

import com.officedepot.test.jbehave.SeleniumUtils;

public class SeleniumUtilsWWW extends SeleniumUtils {

	public static String COMMON_STORE = "77505";//"33426,77511"
	public static String PASSWORD = "Test1234";
	public static String ODRMEMBER = "1800016261";

	public static enum SKUAlias {
		// 315515|772141                                                                               //139688
		common_sku("315515"), common_sku2("450152"), not_exist_sku("123456"), stamp_fee_sku("898782"), pickup_only_sku("890878"), min_requirement_sku("576481");
		private String skuId;

		private SKUAlias(String skuId) {
			this.skuId = skuId;
		}

		public String getSkuId() {
			return skuId;
		}
	}

	public static String parseSkuIdByType(String skuType) {
		String skuId = skuType;
		try {
			skuId = SKUAlias.valueOf(skuType).getSkuId();
		} catch (Exception e) {
		}
		if (skuId.startsWith("\""))
			skuId = skuId.substring(1, skuId.length() - 1);
		return skuId;
	}

	public static String randomNum(int n) {
		String random = "";
		for (int i = 0; i < n; i++) {
			int num = (int) (Math.random() * 10) % 10;
			random = random + num;
		}
		return random;
	}
}
