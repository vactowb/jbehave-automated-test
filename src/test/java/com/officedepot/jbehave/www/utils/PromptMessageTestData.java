package com.officedepot.jbehave.www.utils;

public class PromptMessageTestData {

	// -------------------------------common----------------------------------
	public static final String securityQuestionRequired = "You must choose a security question";

	// -------------------------------register----------------------------------
	public static final String registerInvalidPassword = "Your password must be at least 8 characters";
	public static final String registerConfirmPassword = "Password and confirm password must match";// Confirm password do not match
	public static final String registerInvalidZipcode = "Zip Code is invalid";
	public static final String registerInvalidEmail = "Email format is invalid";
	public static final String registerInvalidPhone = "Phone number is Invalid";
	public static final String registerInvalidPhone2 = "Sorry, we couldn't find a Member Number with that phone number";
	public static final String registerMiniReqPhone = "Password must contain 8-10 characters with at least one uppercase letter, at least one lower case letter and one number";
	public static final String registerUsedPhone = "Member Number is already linked to another officedepot.com account";
	public static final String registerRequirePhone = "Phone number is required";
	public static final String registerRequireMemType = "Please select a membership type";
	// -------------------------------------loyalty------------------------------
	public static final String loyaltyFieldRequired = "This field is required";
	public static final String loyaltyMemberTypeRequired = "You must choose a Membership Type";
	public static final String loyaltyMemberTypeRequired2 = "Select membership type";
	public static final String loyaltyPasswordConfirm = "enter the same value again";
	public static final String loyaltyInvalidEmail = "enter a valid email address";
	public static final String loyaltyUsedEmail = "email address is already in use";
	public static final String loyaltyUsedEmail2 = "The Login Name has already been used";
	public static final String loyaltyMemberNotFound = "Member Number could not be found";
	public static final String loyaltyMemberInvalid = "member number you entered is not valid";
	public static final String logyltyMemberLinked = "Member Number is already linked to another officedepot.com account";// "you are trying to link is already linked to another account";
	public static final String loyaltyMemberCanNotUse = "That member account cannot be used online at this time";
	public static final String loyaltyUsedPhone = "phone number is already associated";
	public static final String loyaltyAssociated = "Loyalty already associated to account";
	public static final String loyaltyInvalidPhone = "enter at least 4 characters";
	public static final String loyaltyInvalidPhone2 = "Sorry, we couldn't find a Member Number with that phone number";
	public static final String loyaltyInvalidPhone3 = "enter a valid phone number";
	public static final String loyaltyInvalidZipcode = "enter at least 5 characters";
	public static final String loyaltyVerifyIdentity = "verify your identity";
	public static final String loyaltyRegistrationCode = "choose the delivery method for your registration code";
	public static final String loyaltyMiniReqPassword = "Your password must meet minimum password requirements";
	public static final String loyaltyUserLinked = "User is already linked";
	public static final String loyaltyNumberUsed = "This phone number is already in use. Please enter a different phone number.";

	// ---------------------------------------login-------------------------------
	public static final String loginFail = "Your login name or Password is incorrect";
}
