package com.officedepot.jbehave.www.steps.store;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.others.NoLongerAvailablePage;
import com.officedepot.jbehave.www.pages.storelocator.LocalStorePage;
import com.officedepot.jbehave.www.pages.storelocator.StoreDetailsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class StoreLocalStoreRedirectForClosedStoresSteps extends BaseStep {

	private LocalStorePage localStorePage;
	private NoLongerAvailablePage noLongerAvailablePage;
	
    @When("I redirect local store page from url")
    public void iRedirectLocalStorePageFromUrl() throws Exception {
    	localStorePage = new LocalStorePage();
    	noLongerAvailablePage = localStorePage.navigateToRedirectLocalStorePageFromUrl();
    }

    @Then("I should see the no longer available page")
    public void iShouldSeeTheNoLongerAvailablePage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on no longer available page! ---", noLongerAvailablePage.isInCurrentPage());
    }

}
