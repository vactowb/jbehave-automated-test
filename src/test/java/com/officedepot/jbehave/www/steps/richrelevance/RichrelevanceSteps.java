package com.officedepot.jbehave.www.steps.richrelevance;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.richrelevance.CarouselPage;
import com.officedepot.jbehave.www.pages.search.ProductComparisonPage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class RichrelevanceSteps extends BaseStep {

	private HomePage homePage;
	private SearchResultsListPage searchResultsListPage;
	private SkuDetailPage skuDetail; 
	CarouselPage carouselSelector = new CarouselPage();


	@When("I search for the given sku")
    public void iSearchForTheKeyWord315515(@Named("sku") String sku) throws Exception {
    	homePage = new HomePage();
    	skuDetail = homePage.searchBy(sku);
		
    }

	 @Then("I should see the results of the given product")
	    public void iShouldSeeTheResultsOfThePaperDisplayed(@Named("product") String product) throws Exception {
	    	searchResultsListPage = new SearchResultsListPage();
			Assert.assertTrue("----- Failed to navigate on the search results! ------", searchResultsListPage.isNavigateToExceptedSearchResults(product));
	    }
    
    @When("I click the richrelevance carousel next button")
    public void iClickTheRichrelevanceCarouselNextButton() throws Exception {    	
    	
    	carouselSelector.selectNext();    	    	
    }

    @Then("I should see the products scrolled from right to left")
    public void iShouldSeeTheProductsScrolledFromRightToLeft() throws Exception {
    	Assert.assertTrue("----- Failed to access Carousel! ------", carouselSelector.isCarouselScrollsRight());
    }

    @When("I click the richrelevance carousel previous button")
    public void iClickTheRichrelevanceCarouselPreviousButton() throws Exception {
    	
    	carouselSelector.selectPrev();    	
    	    	
    }

    @Then("I should see the products scrolled from left to right")
    public void iShouldSeeTheProductsScrolledFromLeftToRight() throws Exception {
    	Assert.assertTrue("----- Failed to access Carousel! ------", carouselSelector.isCarouselScrollsLeft());
    }
}
