package com.officedepot.jbehave.www.steps.cartridge;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.InkAndTonerResultPage;
import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeInkAndTonerModelNumberResultSteps extends BaseStep {

	private InkAndTonerPage inkAndTonerPage;
	private InkAndTonerResultPage inkAndTonerResultPage;
	
    @When("I navigate to the model number result page from model link")
    public void iNavigateToTheModelNumberResultPageFromModelLink() throws Exception {
    	inkAndTonerPage = new InkAndTonerPage();
    	inkAndTonerResultPage = inkAndTonerPage.clickOnModelResultLink();
    }

    @Then("I should see the model number result page")
    public void iShouldSeeTheModelNumberResultPage() throws Exception {
    	Assert.assertTrue("---Failed to see the model number result page", inkAndTonerResultPage.isNavigateOnThisPage());
    }

}
