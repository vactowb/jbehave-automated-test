package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.ComputerAndTabletsCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.FilteredCategoriesByTheFirstBrandPage;
import com.officedepot.jbehave.www.pages.cartridge.FilteredCategoriesByTheFirstTypePage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeOfficeSuppliesRefinementSteps extends BaseStep {

	private HomePage homepage;
	private ComputerAndTabletsCategoriesPage computerAndTabletsCategoriesPage;
	private FilteredCategoriesByTheFirstTypePage filteredCategoriesByTheFirstTypePage;
	private FilteredCategoriesByTheFirstBrandPage filteredCategoriesByTheFirstBrandPage;
	
    @When("I navigate to computer and  tablets categories page from url from url")
    public void iNavigateToComputerAndTabletsCategoriesPageFromUrlFromUrl() throws Exception {
    	homepage = new HomePage();
    	computerAndTabletsCategoriesPage = homepage.openComputerAndTabletsCategoriesPage();
    }

    @Then("I should see the computer and  tablets categories page")
    public void iShouldSeeTheComputerAndTabletsCategoriesPage() throws Exception {
    	computerAndTabletsCategoriesPage = new ComputerAndTabletsCategoriesPage();
    	Assert.assertTrue("---Failed navigate to the computer and  tablets categories page!---", computerAndTabletsCategoriesPage.isInCurrentPage());
    }

    @When("I filter the  computer and  tablets categories by the first type")
    public void iFilterTheComputerAndTabletsCategoriesByTheFirstType() throws Exception {
    	filteredCategoriesByTheFirstTypePage = computerAndTabletsCategoriesPage.filterTheComputerAndTabletsCategoriesByTheFirstType();
    }

    @Then("I should see the first type computer and  tablets categories page")
    public void iShouldSeeTheTypeComputerAndTabletsCategoriesPage() throws Exception {
    	FilteredCategoriesByTheFirstTypePage  filteredCategoriesByTheFirstTypePage= new FilteredCategoriesByTheFirstTypePage();
    	Assert.assertTrue("---Failed navigate to the computer and  tablets categories page!---", filteredCategoriesByTheFirstTypePage.isInCurrentPage());
    }
    
    @When("I filter the  computer and  tablets categories by the first brand")
    public void iFilterTheComputerAndTabletsCategoriesByTheFirstBrand() throws Exception {
    	filteredCategoriesByTheFirstBrandPage = filteredCategoriesByTheFirstTypePage.filterTheComputerAndTabletsCategoriesByTheFirstBrand();
    }

    @Then("I should see the first brand computer and  tablets categories page")
    public void iShouldSeeTheBrandComputerAndTabletsCategoriesPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to the computer and  tablets categories page!---", filteredCategoriesByTheFirstBrandPage.isInCurrentPage());
    }

    @When("I cancel currnet filter by X mark")
    public void iCancelCurrnetFilterByXMark() throws Exception {
    	filteredCategoriesByTheFirstTypePage = filteredCategoriesByTheFirstBrandPage.cancelCurrnetFilterByXMark();
    }

}
