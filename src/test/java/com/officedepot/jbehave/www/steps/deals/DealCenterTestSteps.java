package com.officedepot.jbehave.www.steps.deals;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.deals.OfficeSuppliesPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class DealCenterTestSteps extends BaseStep {
	private DealCenterPage dealCenterPage;
	private OfficeSuppliesPage officeSuppliesPage;

	@When("I navigate to office supplies page from nav")
	public void iNavigateToOfficeSuppliesPage() throws Exception {
		dealCenterPage = new DealCenterPage();
		officeSuppliesPage = dealCenterPage.openOfficeSuppliesPage();
	}

	@Then("I should see the office supplies page")
	public void iShouldSeeTheOfficeSuppliesPage() throws Exception {
		Assert.assertTrue("---Failed navigate to the office supplies page!----", officeSuppliesPage.isNavigateOnThisPage());
	}

}
