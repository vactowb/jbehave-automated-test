package com.officedepot.jbehave.www.steps.cartridge;

import org.apache.commons.lang.RandomStringUtils;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeInkAndTonerSavePrinterNameSteps extends BaseStep {

	private InkAndTonerPage inkAndTonerPage;
	private String prepopulatedName;
	private String newPrinterName;

	@When("I save this printer from save printer link")
	public void iClickOnTheSavePrinterLinkAfterIFinderThePrinter() throws Exception {
		inkAndTonerPage = new InkAndTonerPage();
		inkAndTonerPage.clickOnSavePrinterLink();
	}

	@Then("I should see the name the printer window")
	public void iShouldSeeTheNameThePrinterWindow() throws Exception {
		Assert.assertTrue("---Failed open the name the printer window!---", inkAndTonerPage.isNavigateOnThisPage());
	}

	@Then("I should see the pre-populated name match the recent search name")
	public void iShouldSeeThePrepopulatedNameMatchTheRecentSearchName() throws Exception {
		prepopulatedName = inkAndTonerPage.getPrepopulatedNameFromSavePrinterNameWindow();
		Assert.assertEquals(prepopulatedName, "HP - 2225 ThinkJet");
	}

	@When("I name the printer as a saved name \"$savedName\" in save printer text box")
	public void iNameThePrinterAsASavedNameInSavePrinterTextBox(String savedName) throws Exception {
		inkAndTonerPage.renameThePrinterNameInSavePrinterTextBox(savedName);
	}

	@Then("I should see the \"$content\" message in save my printer window")
	public void iShouldSeeTheMessageInSaveMyPrinterWindow(String content) throws Exception {
		Assert.assertTrue("---Failed to see the Duplicate printer name message!---", inkAndTonerPage.isDuplicatePrinterNameMessageOccur(content));
	}

	@When("I rename the printer name in save printer text box")
	public void iRenameThePrinterNameInSavePrinterTextBox() throws Exception {
		newPrinterName = "test" + RandomStringUtils.randomAlphanumeric(5);
		inkAndTonerPage.renameThePrinterNameInSavePrinterTextBox(newPrinterName);
	}

	@Then("I should see the name in the my saved printer drop down")
	public void iShouldSeeTheNameInTheMySavedPrinterDropDown() throws Exception {
		Assert.assertTrue("---Failed to save the printer name!---", inkAndTonerPage.isSavedNameInMySavedPrinters(newPrinterName));
	}

}
