package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.ChatNowWindow;
import com.officedepot.jbehave.www.pages.cartridge.CustomerServicePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeOfficeSuppliesNeedHelpSteps extends BaseStep {

	private BasicSuppliesCategoriesPage basicSuppliesCategoriesPage;
	private CustomerServicePage customerServicePage;
	private ChatNowWindow chatNowWindow;

	@When("I navigate to customer service page from need help place")
	public void iNavigateToCustomerServicePageFromNeedHelpPlace() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		customerServicePage = basicSuppliesCategoriesPage.navigateToTheCustomerServicePage();
	}

	@Then("I should see the customer service page from need help place")
	public void iShouldSeeTheCustomerServicePageFromNeedHelpPlace() throws Exception {
		Assert.assertTrue("---Failed navigate to the customer service page!---", customerServicePage.isInCurrentPage());
	}

	@When("I open chat now window from need help place")
	public void iOpenChatNowWindowFromNeedHelpPlace() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		chatNowWindow = basicSuppliesCategoriesPage.openChatNowWindow();
	}

	@Then("I should see the chat now window from need help place")
	public void iShouldSeeTheChatNowWindowFromNeedHelpPlace() throws Exception {
		chatNowWindow = new ChatNowWindow();
		Assert.assertTrue("---Failed open the chat now window!---", chatNowWindow.isInCurrentPage());
	}

}
