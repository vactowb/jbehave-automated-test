package com.officedepot.jbehave.www.steps.deals;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.OnlineCatalogPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.deals.WeeklyAdPage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.shopping.list.SchoolSuppliesPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class DealsSteps extends BaseStep {

	private PageHeader pageHeader;
	private DealCenterPage dealCenterPage;
	private WeeklyAdPage weeklyAdPage;
	private SchoolSuppliesPage schoolSuppliesPage;
	private OnlineCatalogPage onlineCatalogPage;
	private String parentWindow;

	@When("I go to deal center from deals view")
	public void iGoToDealCenterFromDealsView() throws Exception {
		pageHeader = new PageHeader();
		dealCenterPage = pageHeader.goToDealCenterFromDeals();
	}

	@Then("I should see the deal center page")
	public void iShouldNavigateToDealCenterPage() throws Exception {
		dealCenterPage = new DealCenterPage();
		Assert.assertTrue("--- Failed to navigate on deal center page! ---", dealCenterPage.isNavigateOnThisPage());
	}

	@When("I go to shop weekly ad from deals view")
	public void iGoToShopWeeklyAdFromDealsView() throws Exception {
		pageHeader = new PageHeader();
		parentWindow = pageHeader.getCurrentWindowHandle();
		weeklyAdPage = pageHeader.goToWeeklyAdFromDeals();
	}

	@Then("I should see to weekly ad page")
	public void iShouldSeeToWeeklyAdPage() throws Exception {
		weeklyAdPage = new WeeklyAdPage();
		Assert.assertTrue("--- Failed to navigate on weekly ad page! ---", weeklyAdPage.isNavigateOnThisPage());
		weeklyAdPage.goBackToPreWindow(parentWindow);
	}

	@When("I request to view the seasonal savings from deals view")
	public void iRequestToViewTheSeasonalSavingsFromDealsView() throws Exception {
		pageHeader = new PageHeader();
		schoolSuppliesPage = pageHeader.goToBackToSchool();
	}

	@Then("I should navigate to back to school supplies page")
	public void iShouldNavigateToBackToSchoolSuppliesPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on school supplies page! ---", schoolSuppliesPage.isNavigateOnThisPage());
	}

	@When("I request a catalog from deals view")
	public void iRequestACatalogFromDealsView() throws Exception {
		pageHeader = new PageHeader();
		onlineCatalogPage = pageHeader.goToRequestCatalog();
	}

	@Then("I should navigate to online catalog page")
	public void iShouldNavigateToOnlineCatalogPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on online catalog page! ---", onlineCatalogPage.isNavigateOnThisPage());
	}

}
