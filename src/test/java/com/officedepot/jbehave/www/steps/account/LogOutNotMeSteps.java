package com.officedepot.jbehave.www.steps.account;

import static junit.framework.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.login.LoginPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class LogOutNotMeSteps extends BaseStep {
	private HomePage homePage = new HomePage();;
	private LoginPage loginPage;

	@Then("I should see not me link occur")
	public void iShouldSeeNotMeLinkOccur() throws Exception {
		assertTrue("---Failed to see not me link!---", homePage.isNotMeLinkOccur());
	}

	@When("I open login page with not me link")
	public void iOpenLoginPageWithNotMeLink() throws Exception {
		homePage.clickOnNotMeLink();
	}

	@Then("I should see login page")
	public void iShouldSeeLgoinPage() throws Exception {
		loginPage = new LoginPage();
		loginPage.isNavigateOnThisPage();
	}

}
