package com.officedepot.jbehave.www.steps.register;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.AccountOverviewPage;
import com.officedepot.jbehave.www.pages.account.SetSecurityQuestionPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutLoginPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutShipPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.register.CustomerRegistrationPage;
import com.officedepot.jbehave.www.pages.register.ExpressRegisterPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.utils.PromptMessageTestData;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class RegisterSteps extends BaseStep {

//	EComContext ecomSiteType = EComContext.OD_EN_US_BUSINESS;
	// String expressRegistrationFlag = ConfigurationService.lookup("/od/config/feature/mobile/express/registration/enabled", "true", ecomSiteType);
	private String expressRegistrationFlag = "true";
	private CheckoutLoginPage checkoutLoginPage;
	private ExpressRegisterPage expressRegisterPage;
	private ShoppingCartPage shoppingCartPage;
	private HomePage homePage;
	private AccountOverviewPage accountOverviewPage;
	private PageHeader pageHeader;
	private CheckoutShipPage checkoutShipPage;
	private CustomerRegistrationPage customerRegistrationPage;
	private SetSecurityQuestionPage setSecurityQuestionPage;

	@When("I create a new account")
	public void iCreaNewAccout() {
		checkoutLoginPage = new CheckoutLoginPage();
		checkoutLoginPage.createNewAccout();
	}

	@When("I checkout from home page")
	public void iCheckoutFromHomepage() {
		homePage = new HomePage();
		homePage.clickChickout();
	}

	@When("I input all the required information with blank zip code")
	public void InputInforWithblankZipcode() {
		if (expressRegistrationFlag.equals("true")) {
			initRegisterPage();
			expressRegisterPage.iInputAllInforWithblankZipcode();
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			customerRegistrationPage.registerWithBlankZipCode();
		}
	}

	@When("I input all the required information with wrong confirm password")
	public void inputWrongConfirmPassword() {
		if (expressRegistrationFlag.equals("true")) {
			initRegisterPage();
			expressRegisterPage.iputWrongConfirmPassword();
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			customerRegistrationPage.registerWithInvaildConfirmPassword();
		}
	}

	@When("I input all the required information with password less than 8 characters")
	public void inputInvaildPassword() {
		if (expressRegistrationFlag.equals("true")) {
			initRegisterPage();
			expressRegisterPage.inputInvaildPassword();
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			customerRegistrationPage.registerWithInvaildPasswords("test");
		}
	}

	@When("I input all the required information with password: \"$password\"")
	public void inputInvaildPassword(String password) {
		initRegisterPage();
		expressRegisterPage.inputInvaildPassword(password);
	}

	@When("I register with all the required information")
	public void iInputAllInfor() {
		if (expressRegistrationFlag.equals("true")) {
			initRegisterPage();
			expressRegisterPage.iInputAllInfor();
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			setSecurityQuestionPage = customerRegistrationPage.registerWithAllValidInfo();
			accountOverviewPage = setSecurityQuestionPage.cancelSetting();
		}
	}

	@When("I register with all the od required information")
	public void iInputAllODInfor() {
		initRegisterPage();
		expressRegisterPage.iInputInforWithoutSubmit();
	}

	@When("I register with email \"<email>\" and all the other required information")
	public void iInputAllInforWithGivenEmail(@Named("email") String email) {
		initRegisterPage();
		if (email.indexOf("#") == -1) {
			email = email + "@test.com";
		} else {
			email = email.replace('#', '@');
		}
		expressRegisterPage.iInputInforWithEmail(email);
	}

	@When("I input all the required information with invalid zip code \"$zipcode\"")
	public void iInputInforWithInvalidZipcode(String zipcode) {
		if (expressRegistrationFlag.equals("true")) {
			expressRegisterPage.iInputAllInforWithinvalidZipcode(zipcode);
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			customerRegistrationPage.registerWithInvaildZipCode(zipcode);
		}
	}

	@When("I input all the required information with invalid email \"$email\"")
	public void iInputInforWithInvalidEmail(String email) {
		if (expressRegistrationFlag.equals("true")) {
			expressRegisterPage.iInputInforWithEmail(email);
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			customerRegistrationPage.registerWithInvaildEmail(email);
		}
	}

	@When("I input all the required information with unselect security question")
	public void iInputInforWithUnselectQuestion() {
		initRegisterPage();
		expressRegisterPage.iInputInforWithUnselectQuestion();
	}

	@Then("I should be noticed that the passwords do not match")
	public void passwordNotMatch() {
		if (expressRegistrationFlag.equals("true")) {
			assertTrue(expressRegisterPage.registerWithError(PromptMessageTestData.registerConfirmPassword));
		} else {
			assertTrue(customerRegistrationPage.isErrorMessageDisplayed("The passwords you entered do not match"));
		}
	}

	@Then("I should be noticed that the passwords must: \"$message\"")
	public void passwordInvalid(String message) {
		if (expressRegistrationFlag.equals("true")) {
			assertTrue(expressRegisterPage.registerWithError(message));
		} else {
			assertTrue(customerRegistrationPage.isErrorMessageDisplayed("Your password must be at least 6 characters"));
		}
	}

	@Then("I should be noticed that the passwords must be at least 8 characters")
	public void passwordInvalid() {
		if (expressRegistrationFlag.equals("true")) {
			assertTrue(expressRegisterPage.registerWithError(PromptMessageTestData.loyaltyMiniReqPassword));
		} else {
			assertTrue(customerRegistrationPage.isErrorMessageDisplayed("Your password must be at least 6 characters"));
		}
	}

	@Then("I should be noticed that the zip code is invalid")
	public void zipCodeInvalid() {
		if (expressRegistrationFlag.equals("true")) {
			assertTrue(expressRegisterPage.registerWithError(PromptMessageTestData.registerInvalidZipcode));
		} else {
			assertTrue(customerRegistrationPage.isErrorMessageDisplayed("Zip Code is invalid"));
		}
	}

	@Then("I should be noticed that the email address is invalid")
	public void emailInvalid() {
		if (expressRegistrationFlag.equals("true")) {
			assertTrue(expressRegisterPage.registerWithError(PromptMessageTestData.registerInvalidEmail));
		} else {
			assertTrue(customerRegistrationPage.isErrorMessageDisplayed("Please correct Billing Address errors below."));
		}
	}

	@Then("I should be noticed that the email has already in used")
	public void emailInUsed() {
		if (expressRegistrationFlag.equals("true")) {
			assertTrue(expressRegisterPage.registerWithError(PromptMessageTestData.loyaltyUsedEmail));
		} else {
			assertTrue(customerRegistrationPage.isErrorMessageDisplayed("has already been used. Please enter another Login Name, then try your request again."));
		}
	}

	@Then("I see register page with error message: \"$msg\"")
	public void iSeeRegisterWithError(String msg) {
		assertTrue("I see register page with error", expressRegisterPage.registerWithError(msg));
	}

	@Then("I should return to the shopping cart")
	public void iShouldReturnShoppingCart() {
		shoppingCartPage = new ShoppingCartPage();
		assertTrue("I should return to the shopping cart", shoppingCartPage.isNavigateOnThisPage());
	}

	@Then("I should return to Step2 of checkoutpage")
	public void iShouldReturnCheckoutShipPage() {
		checkoutShipPage = new CheckoutShipPage();
		assertTrue("I should return to Step2 of checkoutpage", checkoutShipPage.isNavigateOnThisPage());
		homePage = new HomePage();
		homePage.closeRewardsDialog();
	}

	@Then("I should see logout link on the page of header")
	public void iShouldSeeLogoutLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		assertTrue("I should see logout link on the page of header", pageHeader.isLogoutLinkExistedFromNewHeader());
	}

	@Then("I should see receive promotions selected")
	public void iShouldSeeReceivePromotionsSelected() throws Exception {
		accountOverviewPage = homePage.viewMyAccountDetail();
		assertTrue("I should see receive promotions selected", accountOverviewPage.isReceivePromotionsSelected());
	}

	@Then("I should see receive promotions unselected")
	public void iShouldSeeReceivePromotionsUnSelected() throws Exception {
		accountOverviewPage = homePage.viewMyAccountDetail();
		assertFalse("I should see receive promotions unselected", accountOverviewPage.isReceivePromotionsSelected());
	}

	@When("I input all the required information with not selected ODMarketingEmails")
	public void iInputInforWithUnselectPromotions() {
		initRegisterPage();
		assertFalse(expressRegisterPage.selectPromotions());
	}

	@When("I input all the required information with autoLogin")
	public void iInputInforWithSelectLogmeinautomatically() {
		assertTrue(expressRegisterPage.selectAutoLogin());
	}

	@Then("I should see logout link disappear on the page of header")
	public void iShouldSeeLogoutLinkDisappear() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		assertFalse("I should return to the shopping cart", pageHeader.isLogoutLinkExistedFromNewHeader());
	}

	@When("I open the gigya website")
	public void iOpenGigyaWebsite() {
		initRegisterPage();
		expressRegisterPage.clickGigya();
	}

	@Then("I should see gigya website opened")
	public void iShouldSeeGigyaWebsiteOpened() throws Exception {
		assertTrue(expressRegisterPage.checkGigya());
	}

	@When("I request to register from header")
	public void iRegisterFromHeader() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnCreateAnAccountLink();
	}

	@Then("I should return to my account overview page")
	public void iShouldReturnAccountOverviewpaget() throws Exception {
		accountOverviewPage = new AccountOverviewPage();
		assertTrue("I should return to my account overview page", accountOverviewPage.isInCurrentPage());
	}

	@When("I logout and re-login")
	public void iLogoutandLoginToMyaccoutOverviewPage() {
		initRegisterPage();
		expressRegisterPage.iLogoutandRelogin();
	}

	@When("I re-login with google account authorization")
	public void reLoginWithGoogleAccount() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		expressRegisterPage.reLoginWithAuthorization();
	}

	@Then("I should see a welcome message on my account overview page")
	public void iShouldSeeWelcomeMessage() {
		if (expressRegistrationFlag.equals("true")) {
			initAccountOverviewPage();
			assertTrue("I should see a welcome message", accountOverviewPage.checkWelcomeMessage());
		}
	}

	@Then("I should not see the paymentInfo on my account overview page")
	public void iShouldNotSeePaymentInfo() {
		assertFalse("I should not see the paymentInfo", accountOverviewPage.isPaymentInfoPresent());
	}

	@Then("I should see the paymentInfo on my account overview page")
	public void iShouldSeePaymentInfo() {
		assertTrue("I should not see the paymentInfo", accountOverviewPage.isPaymentInfoPresent());
	}

	@Then("I should see register page")
	public void iShouldSeeRegisterPage() {
		if (expressRegistrationFlag.equals("true")) {
			initRegisterPage();
			assertTrue("--- Failed to navigate on express register page! ---", expressRegisterPage.isNavigateOnThisPage());
		} else {
			customerRegistrationPage = new CustomerRegistrationPage();
			assertTrue("--- Failed to navigate on register page! ---", customerRegistrationPage.isNavigateOnThisPage());
		}
	}

	@When("I request to register from top right header")
	public void iRegisterFromTopRightHeader() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.clickOnRegisterTopRight();
		pageHeader.clickOnRegisterNewAccountLink();
	}

	@When("I register from the login page")
	public void registerFromLoginPage() {
		initRegisterPage();
		expressRegisterPage.registerFromLoginPage();
		expressRegisterPage.iInputAllInfor();
	}

	@When("I look up my odr member id with a invalid phone number")
	public void lookUpMemberIdWithInvalidNumber() {
		expressRegisterPage.clickForgotLoyIdLink();
		expressRegisterPage.inputMemberPhoneNum("7215412142");
	}

	@When("I look up my odr member id with a valid phone number")
	public void lookUpMemberIdWithValidNumber() {
		expressRegisterPage.clickForgotLoyIdLink();
		expressRegisterPage.inputMemberPhoneNum("7205565823");
	}

	@When("I look up my odr member id with a used phone number")
	public void lookUpMemberIdWithUsedNumber() {
		expressRegisterPage.clickForgotLoyIdLink();
		expressRegisterPage.inputMemberPhoneNum("5614254125");
	}

	@Then("I should see the forgotLoy error message phone number is invalid")
	public void seeForgotLoyErrorMeg() {
		assertTrue(expressRegisterPage.seeForgotLoyErrorMeg(PromptMessageTestData.registerInvalidPhone2));
	}

	@Then("I should see the forgotLoy error message phone number is being used")
	public void seeUsedPhoneErrorMeg() {
		assertTrue(expressRegisterPage.seeForgotLoyErrorMeg(PromptMessageTestData.registerUsedPhone));
	}

	@Then("I should see the odr member id being found")
	public void seeOdrMemberBeFound() {
		assertTrue(expressRegisterPage.seeOdrMemberBeFound("1903071221"));
	}

	@When("I input invalid odr member from reg page: \"$memberId\"")
	public void inputInvalidOdrMember(String memberId) {
		expressRegisterPage.inputOdrMemberId(memberId);
	}

	@Then("I should see the forgotLoy error message member id is invalid")
	public void seeForgotLoyErrorMsgMemberId() {
		assertTrue(expressRegisterPage.seeForgotLoyErrorMsgMemberId(PromptMessageTestData.loyaltyMemberInvalid));
	}

	@Then("I should not see the forgotLoy error message member id is invalid")
	public void notSeeForgotLoyErrorMsgMemberId() {
		assertFalse(expressRegisterPage.seeForgotLoyErrorMsgMemberId());
	}

	@Then("I should see the forgotLoy error message member id is used")
	public void seeUsedErrorMsgMemberId() {
		assertTrue(expressRegisterPage.seeForgotLoyErrorMsgMemberId(PromptMessageTestData.logyltyMemberLinked));
	}

	@Then("I see loyalty opt in option")
	public void iSeeLoyaltyOptInCheckBox() {
		initRegisterPage();
		assertTrue(expressRegisterPage.seeLoyaltyOptinChkBox());
	}

	@When("I join rewards when register a new account with noninput anything")
	public void joinRewardsWhenRegisterNonInput() {
		expressRegisterPage.checkLoyaltyCheckbox();
		expressRegisterPage.joinRewardsRegister(null, false);
		expressRegisterPage.clickRegistrationButton();
	}

	@When("I join rewards when register a new account with phone: \"$phone\"")
	public void joinRewardsWhenRegister(String phone) {
		expressRegisterPage.joinRewardsRegister(phone, true);
		expressRegisterPage.clickRegistrationButton();
	}

	@When("I join rewards when register new account")
	public void joinRewardsWhenRegister() {
		String phone = SeleniumUtilsWWW.randomNum(3) + "-" + SeleniumUtilsWWW.randomNum(3) + "-" + SeleniumUtilsWWW.randomNum(4);
		expressRegisterPage.joinRewardsRegister(phone, true);
		expressRegisterPage.clickRegistrationButton();
	}

	@Then("I should see required input when join rewards as register")
	public void errorMsgInputNeedjoinReward() {
		assertTrue(expressRegisterPage.isErrorMsgPhone(PromptMessageTestData.registerRequirePhone));
		assertTrue(expressRegisterPage.isErrorMsgMemType(PromptMessageTestData.registerRequireMemType));
	}

	@Then("I should see invalid input phone when join rewards as register")
	public void errorMsgInvalidInputPhonejoinReward() {
		assertTrue(expressRegisterPage.isErrorMsgPhone(PromptMessageTestData.loyaltyInvalidPhone3));
	}

	@Then("I should see linked input phone when join rewards as register")
	public void errorMsgLinkedInputPhonejoinReward() {
		assertTrue(expressRegisterPage.seeForgotLoyErrorMsgMemberId("There is already a member number assigned to this telephone number"));
	}

	private void initRegisterPage() {
		if (null == expressRegisterPage)
			expressRegisterPage = new ExpressRegisterPage();
	}

	private void initAccountOverviewPage() {
		if (null == accountOverviewPage)
			accountOverviewPage = new AccountOverviewPage();
	}
}
