package com.officedepot.jbehave.www.steps.emailsignup;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class EmailsignupDoNotShowAgainSteps extends BaseStep {

	private HomePage homePage;

	@When("I don't allow the email sign up show again from don't show this again link")
	public void iDontAllowTheEmailSignUpShowAgainFromLink() throws Exception {
		homePage = new HomePage();
		homePage.clickOnEmailSignUpDontShowThisAgainLink();
	}

	@When("I refresh the page again")
	public void iRefreshThePageAgain() throws Exception {
		homePage.openHomePage();
	}

	@Then("I can't see the email sign up again")
	public void iCantSeeTheEmailSignUpAgain() throws Exception {
		Assert.assertFalse("---Failed to close email sign up!---", homePage.isEmailSignUpShow());
	}

}
