package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.GreenerOfficePage;
import com.officedepot.jbehave.www.pages.cartridge.PrivateBrandPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeOfficeSuppliesShopMoreWaysSteps extends BaseStep {

	private BasicSuppliesCategoriesPage basicSuppliesCategoriesPage;
	private PrivateBrandPage privateBrandPage;
	private GreenerOfficePage greenerOfficePage;

	@When("I navigate to private brand page from office depot brand products link")
	public void iNavigateToPrivateBrandPageFromOfficeDepotBrandProductsLink() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		privateBrandPage = basicSuppliesCategoriesPage.navigateToThePrivateBrandPage();
	}

	@Then("I should see the private brand page")
	public void iShouldSeeThePrivateBrandPage() throws Exception {
		privateBrandPage = new PrivateBrandPage();
		Assert.assertTrue("---Failed navigate to the private brand page!---", privateBrandPage.isInCurrentPage());
	}

	@When("I navigate to greener office page from greener office products link")
	public void iNavigateToGreenerOfficePageFromGreenerOfficeProductsLink() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		greenerOfficePage = basicSuppliesCategoriesPage.navigateToTheGreenerOfficePage();
	}

	@Then("I should see the greener office page")
	public void iShouldSeeTheGreenerOfficePage() throws Exception {
		Assert.assertTrue("---Failed navigate to the greener office page!---", greenerOfficePage.isInCurrentPage());
	}

}
