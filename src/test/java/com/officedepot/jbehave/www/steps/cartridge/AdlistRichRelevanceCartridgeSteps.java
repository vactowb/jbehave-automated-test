package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.sku.SkuDetailsPage;
import com.officedepot.jbehave.www.pages.cartridge.FilteredCategoriesByTheFirstTypePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class AdlistRichRelevanceCartridgeSteps extends BaseStep {

	private FilteredCategoriesByTheFirstTypePage  filteredCategoriesByTheFirstTypePage;
	private SkuDetailsPage skuDetailsPage;
	
    @When("I navigate to the first sku details page by the first rich relevance")
    public void iNavigateToTheFirstSkuDetailsPageByTheFirstRichRelevance() throws Exception {
    	filteredCategoriesByTheFirstTypePage= new FilteredCategoriesByTheFirstTypePage();
    	skuDetailsPage = filteredCategoriesByTheFirstTypePage.clickOnTheFirstRichRelevanceSku();
    }

    @Then("I should see the first rich relevance sku details page")
    public void iShouldSeeTheFirstRichRelevanceSkuDetailsPage() throws Exception {
    	Assert.assertTrue("---Failed see the first rich relevance sku details page!---", skuDetailsPage.isNavigateOnThisPage());
    }

}
