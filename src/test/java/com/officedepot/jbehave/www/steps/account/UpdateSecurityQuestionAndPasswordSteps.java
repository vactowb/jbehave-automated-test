package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.AccountOverviewPage;
import com.officedepot.jbehave.www.pages.account.LoginSettingPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class UpdateSecurityQuestionAndPasswordSteps extends BaseStep {
	private HomePage homePage;
	private AccountOverviewPage accountOverviewPage;
	private LoginSettingPage loginSettingPage;

	@When("I navigate to my account login page")
	public void iNavigateToMyAccountLoginPage() throws Exception {
		homePage = new HomePage();
		accountOverviewPage = homePage.viewMyAccountDetail();
		loginSettingPage = accountOverviewPage.clickOnLoginSettingsAndSecurityQuestionLink();
	}

	@Then("I should see my account login page")
	public void iShouldSeeMyAccountLoginPage() throws Exception {
		assertTrue("---Failed navigate to login page!---", loginSettingPage.isInCurrentPage());
	}

	@When("I change login settings")
	public void iChangeLoginSettings() throws Exception {
		loginSettingPage.changeLoginSetting(SeleniumUtilsWWW.PASSWORD);
	}

	@When("I update security question")
	public void iUpdateSecurityQuestion() throws Exception {
		accountOverviewPage = loginSettingPage.updateSecurityQuestion(SeleniumUtilsWWW.PASSWORD);
	}

	@Then("I should see my account page")
	public void iShouldSeeMyAccountPage() throws Exception {
		assertTrue("---Failed navigate to my account page!---", accountOverviewPage.isInCurrentPage());
	}

}
