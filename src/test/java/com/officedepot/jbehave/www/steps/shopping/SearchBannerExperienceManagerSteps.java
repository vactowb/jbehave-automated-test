package com.officedepot.jbehave.www.steps.shopping;

import junit.framework.Assert;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SearchBannerExperienceManagerSteps extends BaseStep {

	private HomePage homePage;
	private SearchResultsListPage searchResultsPage;

	@When("I search with term which has search banner")
	public void iSearchWithTermWhichHasSearchBanner(@Named("bannerTerm") String term) throws Exception {
		homePage = new HomePage();
		searchResultsPage = homePage.searchTerm(term);
	}

	@Then("I should see the search page with search banner")
	public void iShouldSeeTheSearchPageWithSearchBanner() throws Exception {
		Assert.assertTrue("--- Failed to display search banner ---", searchResultsPage.containsSearchBanner());
	}

	@When("I search with term which has no search banner")
	public void iSearchWithTermWhichHasNoSearchBanner(@Named("noBannerTerm") String term) throws Exception {
		homePage = new HomePage();
		searchResultsPage = homePage.searchTerm(term);
	}

	@Then("I should see the search page with no search banner")
	public void iShouldSeeTheSearchPageWithNoSearchBanner() throws Exception {
		Assert.assertFalse("--- Failed. Should not show search banner! ---", searchResultsPage.containsSearchBanner());
	}

}
