package com.officedepot.jbehave.www.steps.others;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.others.ErrorPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class ErrorPageSteps extends BaseStep {

	private HomePage homePage;
	private ErrorPage errorPage;
	
    @When("I access 404 error page from url")
    public void iAccess404ErrorPageFromUrl() throws Exception {
    	homePage = new HomePage();
    	errorPage = homePage.navigateTo404ErrorPageFromUrl();
    }

    @Then("should see 404 error page")
    public void shouldSee404ErrorPage() throws Exception {
    	assertTrue("--- Failed to navigate on terms page! ---", errorPage.isInCurrentPage());
    }

    @When("I navigate to home page from shop now button")
    public void iNavigateToHomePageFromShopNowButton() throws Exception {
    	errorPage.clickOnShopNowButton();
    }


}
