package com.officedepot.jbehave.www.steps.shopping;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.ShopByGradePage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.jbehave.www.pages.shopping.list.SchoolListDetailsPage;
import com.officedepot.jbehave.www.pages.shopping.list.TppcDisplayShoppingListsPage;
import com.officedepot.jbehave.www.pages.shopping.list.TppcEditListPage;
import com.officedepot.jbehave.www.pages.shopping.list.ViewSchoolListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class TppcListSteps extends BaseStep {

	private TppcEditListPage tppcEditListPage;
	private TppcDisplayShoppingListsPage tppcDisPlayShoppingListsPage;
	private ViewSchoolListPage viewSchoolListPage;
	private String exceptedListName;
	private MyListPage myListPage;
	private SchoolListDetailsPage schoolListDetailsPage;
	private ShopByGradePage shopByGradePage;

	@Then("I should see Choose or Start New List Dialog")
	public void iSeeCreateNewListButton() throws Exception {
		tppcDisPlayShoppingListsPage = new TppcDisplayShoppingListsPage();
		Assert.assertTrue(tppcDisPlayShoppingListsPage.isInChooseOrStartNewListDialog());
	}

	@When("I Create a new List in Choose or Start New List Dialog")
	public void iClickButtonStartNewList() throws Exception {
		tppcDisPlayShoppingListsPage.createNewList();
	}

	@Then("I should see start a new list dialog")
	public void iSeeCreateSchoolListButton() throws Exception {
		Assert.assertTrue(tppcDisPlayShoppingListsPage.isInStartANewListDialog());
	}

	@When("I Create New School List in Start New List Dialog")
	public void iClickButtonCreateSchoolList() throws Exception {
		tppcEditListPage = tppcDisPlayShoppingListsPage.createSchoolList();
	}

	@Then("I should see edit list page")
	public void iSeeAddItemToListPage() throws Exception {
		Assert.assertTrue(tppcEditListPage.isInAddItemToListPage());
	}

	@When("I save the changes to school list")
	public void iSaveTheChangedsToSchoolList() throws Exception {
		tppcEditListPage.saveChanges();
		exceptedListName = tppcEditListPage.getListName();
	}

	@Then("I should see share list container")
	public void iSeeShareListContainer() throws Exception {
		Assert.assertTrue(tppcEditListPage.isShareListContainerExists());
	}

	@When("I preview the TPPC List")
	public void iPreviewTPPCList() throws Exception {
		viewSchoolListPage = tppcEditListPage.previewTheList();
	}

	@Then("I should see the preview shared tppc list page")
	public void iSeePreviewTPPCListPage() throws Exception {
		Assert.assertTrue(viewSchoolListPage.isNavigateOnThisPage());
	}

	@When("I navigate to view all my lists")
	public void iNavigateMyListsPage() throws Exception {
		myListPage = tppcEditListPage.clickOnViewAllListLink();
	}

	@When("I go back to edit my list")
	public void iGoBackToEditMyList() throws Exception {
		tppcEditListPage = viewSchoolListPage.goBackToEditList();
	}

	@When("I view and edit my list")
	public void iViewAndEditMyList() throws Exception {
		schoolListDetailsPage = myListPage.clickOnViewEditMylist(exceptedListName);
	}

	@Then("I should see my school list details page")
	public void iSeeMyShoolListDetailsPage() throws Exception {
		Assert.assertTrue("---Failed to navigate to my school list details page!---", schoolListDetailsPage.isInCurrentPage());
		Assert.assertEquals(exceptedListName, schoolListDetailsPage.getListName());
	}

	@When("I navigate to shop by grade page from shop for more supplies link")
	public void iNavigateToShopByGradePageFromShopForMoreSuppliesLink() throws Exception {
		shopByGradePage = schoolListDetailsPage.navigateToShopByGradePage();
	}

	@Then("I should see the shop by grade page")
	public void iShouldSeeTheShopByGradePage() throws Exception {
		Assert.assertTrue("---Failed to navigate to the shop by grade page!---", shopByGradePage.isInCurrentPage());
	}

}
