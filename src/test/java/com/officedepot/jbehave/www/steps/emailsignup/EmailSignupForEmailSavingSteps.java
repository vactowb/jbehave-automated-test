package com.officedepot.jbehave.www.steps.emailsignup;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.emailsignup.EmailSignUpPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class EmailSignupForEmailSavingSteps extends BaseStep {

	private TopNavigationPage topNavigationPage;
	
    @When("I want to save my email for sign up")
    public void iNavigateToSaveMyEmailForSignUp() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewDealsNavigatorOnTopNavigation();
		topNavigationPage.inputEmailAndClickOnEmailSignUpGoButton();
    }

    @Then("I should see the save successfully message")
    public void iShouldSeeTheSaveSuccessfullyMessage() throws Exception {
    	Assert.assertTrue("---Failed to see the save successfully message!---", topNavigationPage.isSavingEmailMeassageOccor());
    }

}
