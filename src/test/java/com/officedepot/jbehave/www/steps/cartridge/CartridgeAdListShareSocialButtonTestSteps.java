package com.officedepot.jbehave.www.steps.cartridge;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.supplies.TechnologyPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeAdListShareSocialButtonTestSteps extends BaseStep {

	private TechnologyPage technologyPage;

	@When("I share the first ad list item in the ad list cartridges")
	public void iShareTheFirstAdListItemInTheAdListCartridges()
			throws Exception {
		technologyPage.shareTheFirstAdListItem();
	}

	@Then("I should see a social network selection modal window")
	public void iShouldSeeASocialNetworkSelectionModalWindow() throws Exception {

	}

	@When("I log in with my facebook account")
	public void iLogInWithMyFacebookAccount() throws Exception {

	}

	@Then("I should see the share ad list item page")
	public void iShouldSeeTheShareAdListItemPage() throws Exception {

	}

	@When("I share the ad list item")
	public void iShareTheAdListItem() throws Exception {

	}

	@Then("I should see the ad list item page disappear")
	public void iShouldSeeTheAdListItemPageDisappear() throws Exception {

	}

}
