package com.officedepot.jbehave.www.steps.shopping;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HeaderCartWithoutLoginSteps extends BaseStep {
	private PageHeader pageHeader = new PageHeader();

    @When("I request to view no items information without login from header cart")
    public void iRequestToViewNoItemsInformationWithoutLoginFromHeaderCart() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    }

    @Then("I should see no items information in header cart")
    public void iShouldSeeNoItemsInformationInHeaderCart() throws Exception {
    	String getWithoutLoginNoItemsInformation = pageHeader.viewWithoutLoginCartInformation();
    	Assert.assertEquals("You have no items in your cart", getWithoutLoginNoItemsInformation);
    }

}
