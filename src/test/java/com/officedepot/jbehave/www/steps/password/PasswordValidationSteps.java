package com.officedepot.jbehave.www.steps.password;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.LoginSettingPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutShipPage;
import com.officedepot.jbehave.www.pages.orderhistory.OrderDetailsPage;
import com.officedepot.jbehave.www.pages.register.CustomerRegistrationPage;
import com.officedepot.jbehave.www.utils.PromptMessageTestData;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class PasswordValidationSteps extends BaseStep {
	private CustomerRegistrationPage customerRegistrationPage;
	private LoginSettingPage loginSettingPage;
	private CheckoutShipPage checkoutShippingPage;
	private OrderDetailsPage orderDetailsPage;

	@When("I input all the required information to register with invalid password and submit")
	public void inputAllReqInfoWithInvalidPassword() {
		initCustomerRegistrationPage();
		customerRegistrationPage.inputAllReqInfoWithInvalidPwd();
	}

	@Then("I should see error message for password not meet mini requirement")
	public void iShouldSeeErrorMessageNotMeetRequirement() {
		initCustomerRegistrationPage();
		assertTrue("Error Message for Password did not came, something wrong!!!", customerRegistrationPage.seeErrorMessage(PromptMessageTestData.registerMiniReqPhone));
	}

	@Then("I should see error message for invalid password")
	public void iShouldSeeErrorMessageForInvalid() {
		initCustomerRegistrationPage();
		assertTrue("Failed to see error message!!!", customerRegistrationPage.seeErrorMessage(PromptMessageTestData.registerInvalidPassword));
	}

	@Then("I should see warning that password need meet mini requirement")
	public void iShouldSeeErrorPswMessage() {
		initCustomerRegistrationPage();
		assertTrue("Error Message for Password did not came, something wrong!!!", customerRegistrationPage.isPasswordStrengthError());
	}

	@When("I change login settings with password not meet mini requirement")
	public void iChangeLoginSettingsPsw() throws Exception {
		loginSettingPage = new LoginSettingPage();
		loginSettingPage.changeLoginSettingInvalidNewPassword("tester", "test1234");
	}

	@When("I fill up shipping information and continue checkout")
	public void iFillUpShippingInfo() throws Exception {
		checkoutShippingPage = new CheckoutShipPage();
		checkoutShippingPage.inputAddressAtCheckoutStepAll2("FirstName", "LastName", "5 TEJON ST", "DENVER", "CO - Colorado", "80223", "559", "999", "9999", "test@test.com", "test@test.com");
		checkoutShippingPage.continueCheckout();
	}

	@When("I enter invalid password not meet mini requirement")
	public void enterInvalidPswMiniReq() {
		orderDetailsPage = new OrderDetailsPage();
		orderDetailsPage.enterPasswordAndCreateAccount("test1234", "test1234");
	}

	@When("I fill up the account details for create new account")
	public void iFillUpTheAccountDetails() {
		customerRegistrationPage.createNewAccount();
		customerRegistrationPage.inputAllReqInfoWithInvalidPwd();
	}

	private void initCustomerRegistrationPage() {
		if (null == customerRegistrationPage) {
			customerRegistrationPage = new CustomerRegistrationPage();
		}
	}
}
