package com.officedepot.jbehave.www.steps.login;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.LostPasswordSQASetPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.login.LoginPage;
import com.officedepot.test.jbehave.BaseStep;
import com.officedepot.test.jbehave.SeleniumTestContext;

@Component
@Scope("prototype")
public class LoginSteps extends BaseStep {

	private HomePage homePage;
	private PageHeader pageHeader;
	private LoginPage loginPage;

	private String loginUsername;
	private String loginPassword;
	private LostPasswordSQASetPage lostPasswordSQASetPage;
	private String mainWindow;

	@Given("I am an OD customer with the following account: $accountDetails")
	public void iAmAnODCustomerWithAccount(ExamplesTable accountDetails) {
		loginUsername = accountDetails.getRow(0).get("User Name");
		loginPassword = accountDetails.getRow(0).get("Password");
	}

	@Given("I am an OD customer with the given account")
	public void iAmAnODCustomerWithGivenAccount(@Named("username") String username, @Named("password") String password) {
		if (username.indexOf("#") != -1) {
			username = username.replace('#', '@');
		} else if (SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("sqs") != -1 || SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("sqm") != -1) {
			if (!(username.equals("auto_reid") || username.equals("user_www30") || username.equals("user_www"))) {
				username = username + "@test.com";
			}
		}
		loginUsername = username;
		loginPassword = password;
	}

	@Given("I am an OD customer")
	public void iAmAnODCustomer() {

	}

	@Given("I access WWW site")
	public void iAccessWWWSite() throws Exception {
		homePage = new HomePage();
		homePage.openHomePage();
		// homePage.removeEmailSignUpDialogBox();
	}

	@Given("I make sure logout")
	public void iLogout() throws Exception {
		homePage.logout();
	}

	@When("I request to login from header")
	public void iRequestToLoginFromHeader() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.iRequestToLoginFromNewHeader(loginUsername, loginPassword);
	}

	@When("I confirm the current account not me")
	public void confirmNotMe() throws Exception {
		pageHeader.confirmNotMe();
	}

	@Given("I open lost password page")
	public void iOpenLostpasswordPage() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.openLostPasswordPage();
	}

	@When("I login from header hover my account $accountInfo")
	public void loginFromMyAccountHover(ExamplesTable accountInfo) throws Exception {
		String loginUsername = accountInfo.getRow(0).get("Username");
		String loginPassword = accountInfo.getRow(0).get("Password");
		pageHeader = new PageHeader();
		pageHeader.iRequestToLoginFromNewHeader(loginUsername, loginPassword);
	}

	@When("I retrieve login name with: $accountInfo")
	public void retrieveloginName(ExamplesTable accountInfo) throws Exception {
		String firstname = accountInfo.getRow(0).get("firstname");
		String lastname = accountInfo.getRow(0).get("lastname");
		String email = accountInfo.getRow(0).get("email");
		lostPasswordSQASetPage = new LostPasswordSQASetPage();
		lostPasswordSQASetPage.forgotLoginName(firstname, lastname, email);
	}

	@Then("I should see the warn message when login: \"$message\"")
	public void displayWarnMessage(String message) throws Exception {
		assertTrue(lostPasswordSQASetPage.checkWarnMessage(message));
	}

	@Then("I should see the successful message when login \"$message\"")
	public void displayTheMessageSuccessfully(String message) throws Exception {
		assertTrue(lostPasswordSQASetPage.checkSuccessMessage(message));
	}

	@When("I login use the accout: $accountInfo")
	public void iLoginUseTheAccount(ExamplesTable accountInfo) throws Exception {
		String name = accountInfo.getRow(0).get("loginname");
		String password = accountInfo.getRow(0).get("password");
		pageHeader.loginFromLostPasswordPage(name, password);
	}

	@Then("The page should not blank out the field in which email address was entered: \"$email\"")
	public void pageShouldNotBlank(String email) {
		assertTrue("The email address input should not blank", lostPasswordSQASetPage.checkEmailInputboxBlank(email));
	}

	@Then("I should login successfully from header")
	public void iShouldLoginSuccessFromNewHeader() throws Exception {
		pageHeader = new PageHeader();
		assertTrue("--- Failed to login! ---", pageHeader.isLoginSuccessfullyFromNewHeader());
		homePage.emptyCart();
	}

	@Then("I should login successfully from loyalty login")
	public void iShouldLoginSuccessFromHeader() throws Exception {
		loginPage = new LoginPage();
		assertTrue("--- Failed to login! ---", loginPage.isLoginSuccessfully());
	}

	@When("I empty the shopping cart")
	public void iEmptyCart() {
		homePage.emptyCart();
	}

	@When("I login by facebook with the following account: $accountDetails")
	public void iLoginbyFacebookWithFollowingAccount(ExamplesTable accountDetails) throws Exception {
		pageHeader = new PageHeader();
		mainWindow = pageHeader.getCurrentWindowHandle();
		loginUsername = accountDetails.getRow(0).get("Username");
		loginPassword = accountDetails.getRow(0).get("Password");
		loginPage = new LoginPage();
		loginPage.clickLoginLink();
		loginPage.clickGigya();
		loginPage.loginFacebook(loginUsername, loginPassword, mainWindow);

	}

	@When("I login by google account with the following account: $accountDetails")
	public void iLoginbyGoogleWithFollowingAccount(ExamplesTable accountDetails) throws Exception {
		pageHeader = new PageHeader();
		mainWindow = pageHeader.getCurrentWindowHandle();
		loginUsername = accountDetails.getRow(0).get("Username");
		loginPassword = accountDetails.getRow(0).get("Password");
		loginPage = new LoginPage();
		loginPage.clickLoginLink();
		loginPage.clickGigyaGoogle();
		loginPage.loginGoogle(loginUsername, loginPassword, mainWindow);

	}

	@When("I click on the social provider facebook")
	public void iClickSocialProviderFacebook() throws Exception {
		loginPage.clickLoginLink();
		loginPage.clickGigya();
		loginPage.clickCloseFacebookLoginPage(mainWindow);
	}

	@When("I logout from homepage")
	public void iClickLogout() {
		homePage.logout();
	}

	@Then("I should login successfully from Gigay")
	public void iShouldLoginSuccessFromGigay() throws Exception {
		loginPage.waitGigyaLogin();
		assertTrue("--- Failed to login! ---", loginPage.isLoginSuccessfully());
	}

	@When("I click on the social provider facebook from homepage")
	public void iClickSocialProviderFacebookFromHomepage() throws Exception {
		loginPage.clickGigyaFromHomePage();
		loginPage.clickCloseFacebookLoginPage(mainWindow);
	}

	@Then("I should see the home page")
	public void iShouldSeeTheHomePage() throws Exception {
		homePage = new HomePage();
		assertTrue("--- Failed to see the home page! ---", homePage.isNavigateOnThisPage());
	}

	@Then("I should see the home page by default")
	public void iShouldSeeTheHomePageByDefault() throws Exception {
		homePage = new HomePage();
		assertTrue("--- Failed to see the home page! ---", homePage.isNavigateOnThisPage());
	}

}
