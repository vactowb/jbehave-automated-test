package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.CategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.TapeAndAdhesivesPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeOfficeSuppliesAllCategoriesSteps extends BaseStep {

	private TapeAndAdhesivesPage tapeAndAdhesivesPage;
	private CategoriesPage categoriesPage;
	private String theFirstCategoriesName;

	@When("I navigate to first categories page from all categories cartridge")
	public void iNavigateToFirstCategoriesPageFromAllCategoriesCartridge() throws Exception {
		tapeAndAdhesivesPage = new TapeAndAdhesivesPage();
		theFirstCategoriesName = tapeAndAdhesivesPage.getTheFirstCategoriesNameFromAllCategories().substring(0, 15);
		categoriesPage = tapeAndAdhesivesPage.navigateToTheFirstCategoriesPageFromAllCategories();
	}

	@Then("I should see the first categories page from all categories cartridge")
	public void iShouldSeeTheFirstCategoriesPageFromAllCategoriesCartridge() throws Exception {
		Assert.assertTrue("---Failed navigate to the first categories page!---", categoriesPage.isNavigateOnThisPage(theFirstCategoriesName));
	}

}
