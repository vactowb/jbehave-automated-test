package com.officedepot.jbehave.www.steps.shopping;

import static com.officedepot.jbehave.www.utils.SeleniumUtilsWWW.parseSkuIdByType;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CheckoutShipPage;
import com.officedepot.jbehave.www.pages.checkout.CouponDialogPage;
import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.pages.shopping.list.CSLRouterPage;
import com.officedepot.test.jbehave.BaseStep;
import com.officedepot.test.jbehave.SeleniumUtils;

@Component
@Scope("prototype")
public class ShoppingCartSteps extends BaseStep {

	private ShoppingCartPage shoppingCartPage;
	private CouponDialogPage couponDialogPage;
	private CSLRouterPage cslRouterPage;
	private FindYourProductPage findYourProductPage;
	private CheckoutShipPage checkoutShipPage;
	private String couponNumber;

	@Then("I should see the coupon has been added to cart")
	public void iShouldSeeTheCouponAddedToCart() {
		assertTrue(couponDialogPage.isCouponAddedToCart(couponNumber));
	}

	@Then("I should see the coupon error in cart")
	public void iShouldSeeCouponError() {
		assertTrue(shoppingCartPage.seeCouponError());
	}

	@When("I set the cookie enableAutoCouponRemovalCookie as true")
	public void seeCookieEnableAutoCouponRemove() {
		shoppingCartPage.addCookieCheckoutWithCouponError();
	}

	@Then("I should see the sku \"$skuId\" added to my shopping cart")
	public void iShouldSeeTheSkuAddedToMyCart(String skuId) throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		assertTrue(shoppingCartPage.isSkuAddedToCart(skuId));
	}

	@Then("This sku shouldn't add to cart")
	public void checkWarningMessage() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		assertTrue("---Cannot filter out the unvalid sku!---", shoppingCartPage.getQtyInCart().equals("0"));
	}

	@Then("I should see the skus from shopping list are added in shopping cart successful")
	public void iSeeSkusFromShoppingListAddedIntoCartSuccessful() throws Exception {
		cslRouterPage = new CSLRouterPage();
		assertTrue(cslRouterPage.isSkusFromShoppingListAddedToCartSuccessful());
	}

	@When("I go back to shopping cart page")
	public void goBack2ShoppingCart() {
		shoppingCartPage = new ShoppingCartPage();
		shoppingCartPage.goBack2ShoppingCart();
	}

	@Then("I should view the shopping cart")
	public void iViewTheShoppingCart() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		assertTrue(shoppingCartPage.isInShoppingCart());
	}

	@When("I ensure coupon is not applied and add it into cart")
	public void iAddTheCopuonInCart(@Named("coupon") String coupon) throws Exception {
		couponNumber = coupon;
		shoppingCartPage = new ShoppingCartPage();
		// shoppingCartPage.removeSpecificCoupon(couponNumber);
		couponDialogPage = shoppingCartPage.addCouponInCart(couponNumber);
	}

	@Then("I see the coupon is added in cart")
	public void iSeeTheCouponIsAddedInCart() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		assertTrue(shoppingCartPage.isCouponAddedInCart());
	}

	@When("I ensure coupon \"$couponCode\" is not applied and proceed to checkout")
	public void iRemoveTheCouponAndProceedCheckout(String couponCode) throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		shoppingCartPage.removeSpecificCoupon(couponCode);
		shoppingCartPage.proceedCheckout();
	}

	@When("I clear the shopping cart")
	public void clearShoppingCart() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		shoppingCartPage.clearShoppingCart();
	}

	@When("I checkout from shopping cart")
	public void checkoutFromShoppingCart() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		shoppingCartPage.proceedCheckout();
	}

	@Then("I should see stamp sku with fee description in my shopping cart: $exceptedShoppingList")
	public void iShouldSeeStampSkuWithFeeDescriptionInMyShoppingCart(ExamplesTable exceptedShoppingList) throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		List<String> tableHeader = exceptedShoppingList.getHeaders();
		List<String> verifyItems = SeleniumUtils.transferItemFormater(tableHeader);
		for (Map<String, String> exceptedRow : exceptedShoppingList.getRows()) {
			for (int i = 0; i < tableHeader.size(); i++) {
				String exceptValue = exceptedRow.get(tableHeader.get(i));
				String realValue = shoppingCartPage.getShoppingListContent(verifyItems.get(i));
				assertEquals("---Assert the '" + tableHeader.get(i) + "' failed!---", exceptValue, realValue);
			}

		}
	}

	@When("I add the sku to be subscribed")
	public void iaddItemtoBeSubscribed() {
		initShoppingCartPage();
		shoppingCartPage.clickSubscribeFromShoppingCart();
		shoppingCartPage.chooseFrequency("Weekly");
		shoppingCartPage.clickUpdateFromSubscription();
	}

	@Then("I should see the sku should be subscribed")
	public void itemShouldBeSubscribed() {
		assertTrue("The item should be subscribed!", shoppingCartPage.itemShouldBeSubscribed());
	}

	@Then("I should see the item \"$sku\" should not be subscribed")
	public void itemShouldNotBeSubscribed(String sku) {
		assertFalse("The item should not be subscribed!", shoppingCartPage.checkItemAddToCartBeSubscribed(parseSkuIdByType(sku)));
	}

	@When("I add item \"$sku\" from the order")
	public void addItemFromOrder(String sku) {
		findYourProductPage = shoppingCartPage.addItemFromOrder(parseSkuIdByType(sku));
		findYourProductPage.viewShoppingCart();
	}

	@When("I add item \"$sku\" order by item \"$num\"")
	public void addItemFromOrder(String sku, String num) {
		initShoppingCartPage();
		findYourProductPage = shoppingCartPage.addItemFromOrder(parseSkuIdByType(sku), num);
		findYourProductPage.viewShoppingCart();
	}

	@When("I navigate to the shopping cart page")
	public void openShoppingCartPage() {
		shoppingCartPage = new ShoppingCartPage();
		shoppingCartPage.openShoppingCartPage();

	}

	@Then("I see the item \"$sku\" has been added to cart")
	public void seeItemAddToCart(String sku) {
		initShoppingCartPage();
		assertTrue("The item should be added to cart", shoppingCartPage.checkItemAddToCart(parseSkuIdByType(sku)));
	}

	@When("I delete the first item from the order")
	public void deleteThefirstItemFromTheOrder() {
		shoppingCartPage.deleteShoppingItem();
	}

	@Then("I should see this sku has min qty and qty increment requirement")
	public void isMinQtyIncrementReq() {
		initShoppingCartPage();
		assertTrue(shoppingCartPage.isMinQtyIncrementReq());
	}

	@Then("I should see error message need meet the minimum quantity")
	public void isMinQtyIncrementReqError() {
		assertTrue(shoppingCartPage.isMinQtyIncrementReqError());
	}

	@When("I update the sku quantity \"$num\" from cart")
	public void updateSkuQty(String num) {
		shoppingCartPage.updateSkuQty(num);
	}

	@Then("I see the item \"$sku\" has been deleted")
	public void seeItemDeleted(String sku) {
		assertFalse("The item should be added to cart", shoppingCartPage.checkItemAddToCart(parseSkuIdByType(sku)));
	}

	@When("I make the shopping cart in mixed mode")
	public void makeTheShoppingCartInmixedMode() {
		initShoppingCartPage();
		shoppingCartPage.addMixedMode();
	}

	@Then("I should view the shipping details of item \"$sku\" is no \"$mode\"")
	public void shouldViewtheshippingdetails(String sku, String mode) {
		initShoppingCartPage();
		assertTrue("The item should pick up only", shoppingCartPage.checkItemSkuAndMode(parseSkuIdByType(sku), mode));
	}

	@When("I select the Pickup mode for the sku \"$sku\"")
	public void selectPickupRadio(String sku) {
		initShoppingCartPage();
		shoppingCartPage.clickPickupRadio(parseSkuIdByType(sku));
		shoppingCartPage.clickYestoAllEligibleItems();
	}

	@When("I select the Pickup mode for one sku \"$sku\"")
	public void selectPickupRadioForOne(String sku) {
		initShoppingCartPage();
		shoppingCartPage.clickPickupRadio(parseSkuIdByType(sku));
		checkoutShipPage = new CheckoutShipPage();
		checkoutShipPage.outOfStoreSelectStore();
	}

	@When("I select the Pickup mode and only for the sku \"$sku\"")
	public void selectPickupRadioClickNo(String sku) {
		initShoppingCartPage();
		shoppingCartPage.clickPickupRadio(parseSkuIdByType(sku));
		shoppingCartPage.clickNotoAllEligibleItems();

	}

	@Then("I should see the sku \"$sku\" is in pickup mode")
	public void shouldSeeSkuInpickupMode(String sku) throws Exception {
		assertFalse("The item should pick up mode", shoppingCartPage.seeSkuInpickupMode(parseSkuIdByType(sku)));
	}

	@Then("I should see the my store feature in my cart")
	public void shouldSeeMystoreFeatureIncart() {
		assertTrue("The item should pick up mode", shoppingCartPage.shouldSeeMystoreFeatureIncart());
	}

	@Then("I should not see the my store feature in my cart")
	public void shouldNotSeeMystoreFeatureIncart() {
		assertFalse("The item should pick up mode", shoppingCartPage.shouldSeeMystoreFeatureIncart());
	}

	@Then("I should see free delivery messaging at top of cart")
	public void shouldSeeFreeDeliveryMessage() {
		assertTrue("I should see free delivery messaging at top of cart", shoppingCartPage.shouldSeeFreeDeliveryMsg());
	}

	@Then("I should not see free delivery messaging at top of cart")
	public void shouldNotSeeFreeDeliveryMessage() {
		assertFalse("I should not see free delivery messaging at top of cart", shoppingCartPage.shouldSeeFreeDeliveryMsg());
	}

	@When("I select the Delivery mode for the sku \"$sku\"")
	public void selectDeliveryRadio(String sku) {
		if (shoppingCartPage == null) {
			shoppingCartPage = new ShoppingCartPage();
		}
		shoppingCartPage.clickDeliveryRadio(parseSkuIdByType(sku));
	}

	@Then("I should see the sku \"$sku\" is in Delivery mode")
	public void shouldSeeSkuInDeliveryMode(String sku) throws Exception {
		initShoppingCartPage();
		assertTrue("The item should pick up mode", shoppingCartPage.seeSkuInpickupMode(parseSkuIdByType(sku)));
	}

	@Then("I should see Estimate Shipping & Taxes link")
	public void shouldSeeEstimateShippingTaxesLink() {
		initShoppingCartPage();
		assertTrue("I should see Estimate Shipping & Taxes link", shoppingCartPage.shouldSeeEstimateShippingTaxesLink());
	}

	@Then("I should not see Estimate Shipping & Taxes link")
	public void shouldNotSeeEstimateShippingTaxesLink() {
		assertFalse("The item should pick up mode", shoppingCartPage.shouldSeeEstimateShippingTaxesLink());
	}

	@When("I want to display the current tax exempt link")
	public void displayCurrentTaxExemptLink() {
		shoppingCartPage.clickEstimateShippingTaxesLink();
	}

	@Then("I should see the Estimated Sales Tax")
	public void shouldSeeEstimatedSalesTax() {
		assertTrue("I should see the Estimated Sales Tax", shoppingCartPage.shouldSeeEstimatedSalesTax());
	}

	@Then("I should see the Estimated Shipping Charges")
	public void shouldSeeEstimatedShippingCharges() {
		assertTrue("I should see the Estimated Sales Tax", shoppingCartPage.shouldSeeEstimatedShippingCharges());
	}

	@Then("I should see the Estimated Shipping Charges FREE")
	public void shouldNotSeeEstimatedShippingCharges() {
		assertTrue("I should see the Estimated Shipping Charges FREE", shoppingCartPage.shouldSeeEstimatedShippingChargesFree());
	}

	@Then("I should see Available & Backorder")
	public void shouldSeeAvailableAndBackorder() {
		initShoppingCartPage();
		assertTrue("I should see Available & Backorder", shoppingCartPage.shouldSeeAvailableAndBackorder());
	}

	@Then("I should not see Available & Backorder")
	public void shouldNotSeeAvailableAndBackorder() {
		initShoppingCartPage();
		assertFalse("I should see Available & Backorder", shoppingCartPage.shouldNotSeeAvailableAndBackorder());
	}

	@When("I trigger composite steps to add the common sku to shopping cart")
	@Composite(steps = { "When I search sku from the header: \"common_sku\"", "When I add the sku to cart from sku details page", "When I checkout from shopping cart" })
	public void iTriggerCompositeStepsToAddCommonSku2ShoppingCart() throws Exception {
		// do nothing
	}

	@When("I trigger composite steps to add sku to shopping cart: \"$skuid\"")
	@Composite(steps = { "When I search sku from the new header: <skuid>", "When I add the sku to cart from sku details page", "Then I should view the shopping cart" })
	public void iTriggerCompositeStepsToAddSku2ShoppingCart(@Named("skuid") String skuid) throws Exception {
		// do nothing
	}

	@When("I trigger composite steps to add pickup sku: \"$skuid\"")
	@Composite(steps = { "When I search sku from the new header: <skuid>", "When I request to add the sku to cart" })
	public void iTriggerCompositeStepsToAddSku2Cart(@Named("skuid") String skuid) throws Exception {
		// do nothing
	}

	@When("I save for later the sku from shopping cart")
	public void saveForLater() {
		shoppingCartPage.saveForLater();
	}

	@Then("I should see save for Later list is displayed")
	public void seeSaveForLaterList() {
		assertTrue(shoppingCartPage.seeSaveForLaterList());
	}

	@Then("I should see free delivery message is not displayed under subtotal section")
	public void iNotSeeFreeDeliveryMessageDisplay() {
		initShoppingCartPage();
		assertFalse("I should see free delivery message is not displayed under subtotal section", shoppingCartPage.iSeeFreeDeliveryMessageDisplay());
	}

	@Then("I should see free delivery message is displayed under subtotal section")
	public void iSeeFreeDeliveryMessageDisplay() {
		initShoppingCartPage();
		assertTrue("I should see free delivery message is displayed under subtotal section", shoppingCartPage.iSeeFreeDeliveryMessageDisplay());
	}

	@When("I change location from shopping cart page")
	public void changeLocation() {
		shoppingCartPage.changeLocationClk();
	}

	private void initShoppingCartPage() {
		if (null == shoppingCartPage) {
			shoppingCartPage = new ShoppingCartPage();
		}
	}
}
