package com.officedepot.jbehave.www.steps.checkout;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CouponDetailsPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckoutAddCouponType3Type4Steps extends BaseStep {

	private CouponDetailsPage couponDetailsPage;
	private ShoppingCartPage shoppingCartPage;
	
    @When("I navigate to the home page from shop now button")
    public void iNavigateToTheHomePageFromShopNowButton() throws Exception {
    	couponDetailsPage = new CouponDetailsPage();
    	couponDetailsPage.clickOnShopNowButton();
    }

    @Then("I should see the type3 and type4 coupon add to cart")
    public void iShouldSeeTheType3AndType4CouponAddToCart() throws Exception {
    	shoppingCartPage = new ShoppingCartPage();
    	assertTrue("--- Failed to add coupon in cart! ---", shoppingCartPage.isCouponAddedInCart());
    }

}
