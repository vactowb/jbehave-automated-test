package com.officedepot.jbehave.www.steps.shopping;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckBoxesForLeftNavSteps extends BaseStep {

	private HomePage homePage;
	private SearchResultsListPage searchResultsPage;

	@When("I search with term which triggers experience manager template")
	public void ISearchWithTermWhichTriggersExperienceManagerTemplate() throws Exception {
		homePage = new HomePage();
		searchResultsPage = homePage.searchTerm("backpack");
	}

	@Then("I should see the left nav with checkboxes")
	public void iShouldSeeTheLeftNavWithCheckboxes() throws Exception {
		Assert.assertTrue("--- Failed to display search banner ---", searchResultsPage.containsCheckboxInLeftNav());
	}

}
