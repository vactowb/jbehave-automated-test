package com.officedepot.jbehave.www.steps.emailsignup;

import junit.framework.Assert;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class EmailSingupDealsFlyoutEmailSingupSteps extends BaseStep {

	private TopNavigationPage topNavigationPage;
	
    @When("I input without email from deals flyout")
    public void iInputWithoutEmailFromDealsFlyout() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewDealsNavigatorOnTopNavigation();
		topNavigationPage.clickOnDealsFlyoutGoButton();
    }

    @Then("I should see the email sign up error message")
    public void iShouldSeeTheEmailSignUpErrorMessage() throws Exception {
    	Assert.assertTrue("---Failed to see the email sign up error message!---", topNavigationPage.isEmailSignUpErrorMessageOccur());
    }

    @When("I input email from deals flyout")
    public void iInputEmailFromDealsFlyout() throws Exception {
		topNavigationPage.viewDealsNavigatorOnTopNavigation();
		topNavigationPage.inputEmailAndClickOnEmailSignUpGoButton();
    }

    @Then("I should see the thank you for signing up message")
    public void iShouldSeeTheThankYouForSigningUpMessage() throws Exception {
    	Assert.assertTrue("---Failed to see the thank you for signing up message!---", topNavigationPage.isThankYouForSigningUpMessageOccur());
    }

}
