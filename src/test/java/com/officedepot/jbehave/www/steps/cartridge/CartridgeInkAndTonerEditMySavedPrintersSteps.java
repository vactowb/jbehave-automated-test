package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeInkAndTonerEditMySavedPrintersSteps extends BaseStep {

	private InkAndTonerPage inkAndTonerPage;
	private String theFirstSavedName;

	@When("I manage saved printer from the \"$editLinkName\" link")
	public void iClickOnTheLinkFromYourSavedPrinters(String editLinkName) throws Exception {
		inkAndTonerPage = new InkAndTonerPage();
		inkAndTonerPage.clickOnTheEditLink();
	}

	@Then("I should see the manage printers window open")
	public void iShouldSeeTheManagePrintersWindowOpen() throws Exception {
		Assert.assertTrue("---Failed to see the manage printers window!---", inkAndTonerPage.isManagePrintersWindowOccur());
	}

	@When("I delete saved printer from saved printer \"$DeleteButtonName\" button")
	public void iClickOnTheFirstSavedPrinterButton(String DeleteButtonName) throws Exception {
		inkAndTonerPage.clickOnTheFirstPrinterDelete();
		theFirstSavedName = inkAndTonerPage.getTheFirstSavedPrinterName();
	}

	@Then("I should see the my saved printer been deleted")
	public void iShouldSeeTheMySavedPrinterBeenDeleted() throws Exception {
		Assert.assertTrue("---Failed to delete the first saved printer!---", inkAndTonerPage.isManagePrintersWindowOccur());
	}

}
