package com.officedepot.jbehave.www.steps.store;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.storelocator.AllStatesPage;
import com.officedepot.jbehave.www.pages.storelocator.CityPage;
import com.officedepot.jbehave.www.pages.storelocator.FindAStorePage;
import com.officedepot.jbehave.www.pages.storelocator.LocalStorePage;
import com.officedepot.jbehave.www.pages.storelocator.StoreLinkPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class FindAStoreSteps extends BaseStep {

	private FindAStorePage findAStorePage;
	private LocalStorePage localStorePage;
	private AllStatesPage allStatesPage;
	private CityPage cityPage;
	private StoreLinkPage storeLinkPage;
	
    @When("I choose the fist store as my store")
    public void iChooseTheFistStoreAsMyStore() throws Exception {
		findAStorePage = new FindAStorePage();
		localStorePage = findAStorePage.makeThisMyStore();
    }

    @When("I navigate to all states page from view all office depot locations link")
    public void iNavigateToAllStatesPageFromViewAllOfficeDepotLocationsLink() throws Exception {
    	findAStorePage = new FindAStorePage();
    	allStatesPage = findAStorePage.clickOnViewAllOfficeDepotLocationsLink();
    }
    
    @When("I navigate to the first state city page")
    public void iNavigateToTheFirstStatePage() throws Exception {
    	cityPage = allStatesPage.clickOnFirstStateLink();
    }

    @Then("I shoud see city page")
    public void iShoudSeeCityPage() throws Exception {
    	Assert.assertTrue("--- Failed to navigate on cityPage! ---", cityPage.isInCurrentPage());
    }

    @When("I navigate to the first city store page")
    public void iNavigateToTheFirstCityStorePage() throws Exception {
    	storeLinkPage = cityPage.clickOnFirstCityLink();
    }

    @Then("I should see store link page")
    public void iShouldSeeStoreLinkPage() throws Exception {
    	Assert.assertTrue("--- Failed to navigate on store link page! ---", storeLinkPage.isInCurrentPage());
    }


}
