package com.officedepot.jbehave.www.steps.home;

import static com.officedepot.jbehave.www.utils.SeleniumUtilsWWW.parseSkuIdByType;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SearchSteps extends BaseStep {

	private HomePage homePage;
	

	@When("I search for SKU with the given SKU Id")
	public void iSearchFor(@Named("skuid") String skuId) throws Exception {
		homePage = new HomePage();
		homePage.searchBy(skuId);
	}
	
	@When("I search for SKU with specific SKU description: \"$description\"")
	public void searchForDescription(String description) throws Exception {
		homePage = new HomePage();
		homePage.searchDescritpion(description);
	}

	@When("I search sku from the new header: <skuid>")
	public void isearchSku2(@Named("skuid") String skuid) throws Exception {
		initHomePage();
		Thread.sleep(2000);
		homePage.searchBy(parseSkuIdByType(skuid));
	}
	
	@When("I search sku from the header: \"$skuid\"")
	public void isearchSku(String skuid) throws Exception {
		initHomePage();
		homePage.searchBy(parseSkuIdByType(skuid));
	}
	
	private void initHomePage() {
		if(homePage == null){
			homePage = new HomePage();
		}
	}
}
