package com.officedepot.jbehave.www.steps.shopping;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.shopping.list.CSLShareItemsDialogPage;
import com.officedepot.jbehave.www.pages.shopping.list.CslListDetailPage;
import com.officedepot.jbehave.www.pages.shopping.list.DisplayShoppingListsPage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CslListSteps extends BaseStep {

	private DisplayShoppingListsPage displayShoppingListsPage;
	private MyListPage myListPage;
	private CslListDetailPage cslListDetailPage;
	private CSLShareItemsDialogPage shareItemsDialogPage;


	@When("I Create new Personal List in Start New List Dialog")
	public void iCreateNewPersonListInStartNewListDialog() throws Exception {
		displayShoppingListsPage = new DisplayShoppingListsPage();
		displayShoppingListsPage.createPersonalListInterior();
	}

	@Then("I see Personal List created success message")
	public void iSeePersonalListCreatedSuccessMessage() throws Exception {
		displayShoppingListsPage = new DisplayShoppingListsPage();
		assertTrue(displayShoppingListsPage.isPersonalListCreatedSuccess());
	}

	@When("I add sku to an existed personal shopping list")
	public void iAddSkuToAnExisedtPersonalShoppingList() throws Exception {
		displayShoppingListsPage = new DisplayShoppingListsPage();
		displayShoppingListsPage.addSkuToExistedPersonalShoppingList();
	}

	@Then("I should see added success message")
	public void iShouldSeeAddedSuccessMessage() throws Exception {
		displayShoppingListsPage = new DisplayShoppingListsPage();
		assertTrue(displayShoppingListsPage.isAddedSuccessMessageDisplayed());
	}

	@When("I view or edit a csl list")
	public void iViewOrEditACslList() throws Exception {
		myListPage = new MyListPage();
		cslListDetailPage = myListPage.viewOrEditCslList();
	}

	@Then("I could view or eidt the detail of a csl list")
	public void iCouldViewOrEditTheDetailOfACslList() throws Exception {
		assertTrue(cslListDetailPage.isInCurrentPage());
	}

	@When("I add all skus in csl list into cart")
	public void iAddSkusInCslListIntoCart() throws Exception {
		cslListDetailPage.addAllSkusIntoCart();
	}
	
	@When("I add the selected skus in csl list into cart")
	public void iAddSelectedSkusInCslListIntoCart() throws Exception {
		cslListDetailPage.addSkusIntoCart();
	}

	@When("I share all the csl list items")
	public void iShareAllCslListItems() throws Exception {
		shareItemsDialogPage = cslListDetailPage.shareCslList();
	}

	@When("I send an email to share the csl list items")
	public void iSendEmailToShareCSLListItems() throws Exception {
		shareItemsDialogPage.sendEmail();
	}
	
	@Then("I should see the email successfully sent")
	public void iSendEmailSuccessfully() throws Exception {
		assertTrue(shareItemsDialogPage.isSendEmailSuccessful());
	}
	
	@When("I continue shopping")
	public void iContinueShopping() throws Exception {
		displayShoppingListsPage.continueShopping();
	}
}