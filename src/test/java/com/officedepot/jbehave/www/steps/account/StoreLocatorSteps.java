package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.pages.storelocator.ChooseStorePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class StoreLocatorSteps extends BaseStep {

	private HomePage homepage;
	private ChooseStorePage chooseStorePage;
	private ShoppingCartPage shoppingCartPage;

	@When("I search the store list with zip code \"$zipCode\"")
	public void searchStoresByZipCode(String zipCode) throws Exception {
		chooseStorePage = new ChooseStorePage();
		chooseStorePage.searchStoresByZipCode(zipCode);
	}

	@When("I choose the first one in the store list")
	public void chooseFirstStore() throws Exception {
		chooseStorePage = new ChooseStorePage();
		chooseStorePage.chooseFirstStore();
		shoppingCartPage = new ShoppingCartPage();
	}

	@Then("My store should be set up correctly")
	public void verifyStore() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		assertTrue("---Fali to verify my chosen store!---", shoppingCartPage.verifyChosenStore());
	}

	@When("I turn on the new header feature")
	public void openNewHeaderFeature() throws Exception {
		homepage = new HomePage();
		homepage.turnOnNewHeader();
	}

	@When("I hover my mouse on store icon of the new header")
	public void hoverOrClickOnStoreIcon() throws Exception {
		homepage.openStoreFlyOut();
	}

	@Then("I see the store fly out page is shown with Find A Store function")
	public void showStoreFlyoutPageWithFindAStore() {
		assertTrue("--Open Store Fly Out Failed", homepage.isStoreFlyOutWithFindAStoreShown());
	}
}
