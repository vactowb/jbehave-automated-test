package com.officedepot.jbehave.www.steps.cpd;

import org.jbehave.core.annotations.Then;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CheckoutShipPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HollandSplitOrderQtyIsRetainedWhenRemoveFromCheckoutSteps extends BaseStep {

	private CheckoutShipPage checkoutShipPage;
	
    @Then("I should see quantity at index \"$index\" of \"$quantityValue\"")
    public void iShouldSeeQuantityAtIndexOf(int index, String quantityValue) throws Exception {
    	checkoutShipPage = new CheckoutShipPage();
		String realValue = checkoutShipPage.getOrderDetailContent("QTY", index, true);
		Assert.assertEquals("---Assert QTY failed!---", quantityValue, realValue);
    }
}
