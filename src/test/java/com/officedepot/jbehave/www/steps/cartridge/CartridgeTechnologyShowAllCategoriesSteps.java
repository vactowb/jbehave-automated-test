package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.AudioCategoriesPage;
import com.officedepot.jbehave.www.pages.supplies.TechnologyPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeTechnologyShowAllCategoriesSteps extends BaseStep {

	private TechnologyPage technologyPage;
	private AudioCategoriesPage audioCategoriesPage;

	@When("I navigate to audio page from show all categories")
	public void iNavigateToAudioPageFromShowAllCategories() throws Exception {
		technologyPage = new TechnologyPage();
		audioCategoriesPage = technologyPage.navigateToAudioPageFromShowAllCategories();
	}

	@Then("I should see the audio page")
	public void iShouldSeeTheAudioPage() throws Exception {
		Assert.assertTrue("---Failed navigate to the audio page!---", audioCategoriesPage.isNavigateOnThisPage());
	}

}
