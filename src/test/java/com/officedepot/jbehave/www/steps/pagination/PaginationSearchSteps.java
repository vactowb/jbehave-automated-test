package com.officedepot.jbehave.www.steps.pagination;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.CategoryLandingPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class PaginationSearchSteps extends BaseStep {

	private CategoryLandingPage categoryLandingPage;


	@When("I go to the first Category search page by url : \"$url\"")
	public void goToCategoryPage(String url) throws Exception {
		categoryLandingPage = new CategoryLandingPage();
		categoryLandingPage.openSpecificUrl(url);
	}

	@Then("I could get the next link tag with property rel and href")
	public void displayNextRelatedCategories() throws Exception {
		categoryLandingPage = new CategoryLandingPage();
		String keyWord = "Next";

		WebElement webElement = categoryLandingPage.chooseLinkTag(keyWord);
		if (webElement != null) {
			Assert.assertTrue(categoryLandingPage.isNextElementPresent());
		}
	}
	
	@When("I go to the second Category search page by url : \"$url\"")
	public void goToSecondCategoryPage(String url) throws Exception {
		categoryLandingPage = new CategoryLandingPage();
		categoryLandingPage.openSpecificUrl(url);
	}
	
	@Then("I could get two link tag with property rel and href")
	public void displayTwoRelatedCategories() throws Exception {
		categoryLandingPage = new CategoryLandingPage();
		String keyWord = "Next";
		
		WebElement webElement = categoryLandingPage.chooseLinkTag(keyWord);
		if (webElement != null) {
			Assert.assertTrue(categoryLandingPage.isNextElementPresent());
		}
		
		keyWord = "Prev";
		webElement = categoryLandingPage.chooseLinkTag(keyWord);
		if (webElement != null) {
			Assert.assertTrue(categoryLandingPage.isPrevElementPresent());
		}
	}
	
	@When("I go to the last Category search page by url : \"$url\"")
	public void goToLastCategoryPage(String url) throws Exception {
		categoryLandingPage = new CategoryLandingPage();
		categoryLandingPage.openSpecificUrl(url);
	}
	
	@Then("I could get prev link tag with property rel and href")
	public void displayPrevRelatedCategories() throws Exception {
		categoryLandingPage = new CategoryLandingPage();
		String keyWord = "Prev";
		
		WebElement webElement = categoryLandingPage.chooseLinkTag(keyWord);
		if (webElement != null) {
			Assert.assertTrue(categoryLandingPage.isPrevElementPresent());
		}
	}

}
