package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.CashBoxesCategoriesPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeCategoryFeaturedCategoriesSteps extends BaseStep {

	private BasicSuppliesCategoriesPage basicSuppliesCategoriesPage;
	private CashBoxesCategoriesPage cashBoxesCategoriesPage;

	@When("I navigate to first categories page from category featured categories")
	public void iNavigateToFirstCategoriesPageFromCategoryFeaturedCategories() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		cashBoxesCategoriesPage = basicSuppliesCategoriesPage.navigateToTheFirstFeaturedCategoriesPage();
	}

	@Then("I should see the first categories page from category featured categories")
	public void iShouldSeeTheFirstCategoriesPageFromCategoryFeaturedCategories() throws Exception {
		Assert.assertTrue("---Failed navigate to the First Categories Page!---", cashBoxesCategoriesPage.isInCurrentPage());
	}

}
