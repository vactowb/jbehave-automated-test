package com.officedepot.jbehave.www.steps.storefrontlandingpage;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.browser.ScholasticBrowserPage;
import com.officedepot.jbehave.www.pages.deals.OfficeSuppliesPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class OfficeSuppliesStoreFrontLandingPageTestSteps extends BaseStep {

	private TopNavigationPage topNavigationPage;
	private OfficeSuppliesPage officeSuppliesPage;
	private ScholasticBrowserPage scholasticBrowserPage;
	
    @When("I navigate to office supplies store front landing page from office supplies navigation")
    public void iNavigateToOfficeSuppliesStoreFrontLandingPageFromOfficeSuppliesNavigation() throws Exception {
    	topNavigationPage = new TopNavigationPage();
    	officeSuppliesPage = topNavigationPage.clickOnOfficeSuppliesNavigation();
    }

    @Then("I should see the office supplies store front landing page")
    public void iShouldSeeTheOfficeSuppliesStoreFrontLandingPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to the office supplies store front landing page!---", officeSuppliesPage.isNavigateOnThisPage());
    }
    
    @When("I navigate to scholastic browser page from left nav cartridge functionality")
    public void iNavigateToScholasticBrowserPageFromLeftNavCartridgeFunctionality() throws Exception {
    	scholasticBrowserPage = officeSuppliesPage.clickOnScholasticLeftNav();
    }

    @Then("I should see the scholastic browser page filtered by brand")
    public void iShouldSeeTheScholasticBrowserPageFilteredByBrand() throws Exception {
    	
    	Assert.assertTrue("---Failed navigate to the scholastic browser page!---", scholasticBrowserPage.isNavigateOnThisPage());
    	Assert.assertTrue("---Failed filtered by scholastic!---", scholasticBrowserPage.isFilteredByScholastic());
    }
    

}
