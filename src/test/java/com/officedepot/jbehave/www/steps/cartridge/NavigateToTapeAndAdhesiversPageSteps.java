package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class NavigateToTapeAndAdhesiversPageSteps extends BaseStep {

	private HomePage homePage;

	@When("I access tape and adhesives page")
	public void iAccessTapeAndAdhesivesPage() throws Exception {
		homePage = new HomePage();
		homePage.openTapeAndAdhesivesPage();
	}

}
