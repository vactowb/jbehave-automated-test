package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.ExemptAccountDispalyPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class ExemptAccoutSteps extends BaseStep {

	private HomePage homePage;
	private ExemptAccountDispalyPage exemptAccountPage;
	private PageHeader pageHeader;

	@When("I navigate to the Exempt Account page")
	public void accessExemptAccountPage() throws Exception {
		homePage = new HomePage();
		exemptAccountPage = homePage.openExemptAccountPage();
	}

	@When("I create the new exempt account with following data: $data")
	public void createNewExemptAccount(ExamplesTable data) throws Exception {
		exemptAccountPage.createNewExemptAccount(data);
	}

	@Then("I should not see my greeting information")
	public void getNotMeLink() throws Exception {
		pageHeader = new PageHeader();
		assertTrue("---Cannot display 'Login or Register' at home page!---", pageHeader.isLoginLinkOccursFromNewHeader());
	}

}
