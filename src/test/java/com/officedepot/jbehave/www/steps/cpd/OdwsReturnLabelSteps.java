package com.officedepot.jbehave.www.steps.cpd;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.odws.ReturnLabelTestPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class OdwsReturnLabelSteps extends BaseStep {
	
	private ReturnLabelTestPage testPage;

    @Given("odws return label test page")
    public void odwsReturnLabelTestPage() throws Exception {
    	
    	testPage = new ReturnLabelTestPage(); 
    	testPage.openTestPage();

    }

    @When("I enter order number in the retrieve UPS label: \"$orderId\"")
    public void iEnterOrderNumberInTheRetrieveUPSLabel(String orderId) throws Exception {
    	testPage = new ReturnLabelTestPage();
    	testPage.retirveUPSLabel(orderId);

    }

    @Then("I should see my UPS return label")
    public void iShouldSeeMyUPSReturnLable() throws Exception {
    	testPage.validateUPSLabelRetrived();

    }
    
    @When("I enter order number in the retrieve OD label: \"$orderId\"")
    public void iEnterOrderNumberInTheRetrieveODLabel(String orderId) throws Exception {
    	testPage = new ReturnLabelTestPage();
    	testPage.retirveODLabel(orderId);

    }
    
    @Then("I should see my OD return label")
    public void iShouldSeeMyODReturnLabel() throws Exception {
    	testPage.validateODLabelRetrived();

    }

    @When("I enter order number in the generate UPS label: \"$orderId\"")
    public void iEnterOrderNumberInTheGenerateUPSLabel(String orderId) throws Exception {
    	testPage = new ReturnLabelTestPage();
    	testPage.generateUPSLabel(orderId);

    }
    
    @Then("I should see my generated UPS return label")
    public void iShouldSeeMyGeneratedUPSReturnLabel() throws Exception {
    	testPage.validateUPSLabelRetrived();

    }
    
    @When("I enter order number in the generate OD label: \"$orderId\"")
    public void iEnterOrderNumberInTheGenerateODLabel(String orderId) throws Exception {
    	testPage = new ReturnLabelTestPage();
    	testPage.generateODLabel(orderId);

    }

}
