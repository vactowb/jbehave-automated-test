package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.AccountOverviewPage;
import com.officedepot.jbehave.www.pages.account.EditPaymentInfoPage;
import com.officedepot.jbehave.www.pages.account.ManageCreditCardsPage;
import com.officedepot.jbehave.www.pages.account.RewardsPage;
import com.officedepot.jbehave.www.pages.account.ShippingListDisplayPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.jbehave.www.pages.storelocator.ChooseStorePage;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class AccountSteps extends BaseStep {

	private Map<String, String> map = new HashMap<String, String>();
	private HomePage homePage;
	private AccountOverviewPage accountOverviewPage;
	private ShippingListDisplayPage shippingListDisplayPage;
	private ChooseStorePage chooseStorePage;
	private EditPaymentInfoPage editPaymentInfoPage;
	private ManageCreditCardsPage manageCreditCardsPage;
	private HomePage homepage;
	private RewardsPage rewardsPage;
	private String cardHolderName = "";

	@When("I view all my list")
	public void iViewAllMyList() throws Exception {
		homePage = new HomePage();
		accountOverviewPage = homePage.viewMyAccountDetail();
		accountOverviewPage.viewAllLists();
	}

	@When("I navigate to My Account Overview")
	public void iNavigateToMyAccountOverview() throws Exception {
		homePage = new HomePage();
		accountOverviewPage = homePage.viewMyAccountDetail();
	}

	@When("I navigate to manage credit cards")
	public void manageCreditCard() {
		manageCreditCardsPage = accountOverviewPage.clickManageCreditCard();
	}

	@When("I navigate to login setting page")
	public void launchLoginSettingPage() {
		accountOverviewPage.clickLoginSetting();
	}

	@Then("I should open the login setting page")
	public void shouldOpenLoginSettingPage() {
		assertTrue(accountOverviewPage.shouldOpenLoginSettingPage());
	}

	@When("I change the login name at login setting page")
	public void changeLoginName() {
		accountOverviewPage.changeLoginName();
	}

	@When("I re-login when I changed login name")
	public void relogin() {
		accountOverviewPage.login();
	}

	@Then("I should login fault from Gigay with error message displayed")
	public void loginFaultgFromGigay() {
		assertTrue(accountOverviewPage.loginFaultgFromGigay());
	}

	@Then("I should navigate to manage credit cards page")
	public void shouldNavigateToManageCreditcardPage() {
		assertTrue(manageCreditCardsPage.isNavigateOnThisPage());
	}

	@When("I add a new card from my account page $creditCardInfro")
	public void addNewCardFromMyAccountPage(ExamplesTable creditCardInfro) {
		String cardHolderName = creditCardInfro.getRow(0).get("CardHolderName");
		String address = creditCardInfro.getRow(0).get("Address");
		String city = creditCardInfro.getRow(0).get("City");
		String state = creditCardInfro.getRow(0).get("State");
		String zipCode = creditCardInfro.getRow(0).get("ZipCode");
		manageCreditCardsPage.addNewCardFromMyAccountPage(cardHolderName, address, city, state, zipCode);
	}

	@Then("I should see the credit card saved successfully")
	public void shouldSeeCreditCardSaveSucc() {
		assertTrue(manageCreditCardsPage.isCreditCardSaveSucc());
	}

	@When("I edit the credit card from my account page")
	public void editCreditCardFromAccount() {
		cardHolderName = SeleniumUtilsWWW.randomNum(5);
		manageCreditCardsPage.editCreditCardFromAccount(cardHolderName);
	}

	@Then("I should see the credit card edit successfully")
	public void shouldSeeCreditCardEditSucc() {
		assertTrue(manageCreditCardsPage.shouldSeeCreditCardEditSucc(cardHolderName));
	}

	@When("I add a new OD card from my account page")
	public void addODcard() {
		manageCreditCardsPage.addODCardFromMyAccountPage();
	}

	@Then("I should see the OD credit card saved successfully")
	public void shouldSeeODCreditCardSaveSucc() {
		assertTrue(manageCreditCardsPage.isODCardSaveSucc());
	}

	@When("I delete the credit card from my account page")
	public void deleteCreditCard() {
		manageCreditCardsPage.clickDeleteCard();
	}

	@Then("I should see the credit card deleted successfully")
	public void isCreditCardDeleted() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		assertFalse(manageCreditCardsPage.isCreditCardSaveSucc());
	}

	@When("I edit my payment information")
	public void iEditMyPaymentInfo() throws Exception {
		editPaymentInfoPage = accountOverviewPage.editPaymentInfo();
	}

	@When("I update my payment method as \"$payment\"")
	public void iUpdateMyPaymentInfo(String payment) throws Exception {
		editPaymentInfoPage.setPaymentMethod(payment);
		editPaymentInfoPage.updatePayment();
	}

	@When("I manage my credit cards")
	public void iManageMyCreditCards() throws Exception {
		manageCreditCardsPage = accountOverviewPage.manageCreditCards();
		assertTrue("---- Failed to navigate on manage credit cards page! ---", manageCreditCardsPage.isNavigateOnThisPage());
	}

	@When("I set my default credit card")
	public void iSetMyDefaultCreditCards() throws Exception {
		manageCreditCardsPage.makeDefaultCreditCard();
	}

	@Then("I should see my default card set successfully")
	public void iShouldSeeMyDefaultCardSetSuccessfully() throws Exception {
		assertTrue("---- Failed to set default card! ---", manageCreditCardsPage.isDefaultCardSet());
	}

	@Then("I will see my list page")
	public void iWillSeeMyListPage() throws Exception {
		MyListPage myListPage = new MyListPage();
		assertTrue(myListPage.isInCurrentPage());
	}

	@When("I change the delivery mode to \"$mode\"")
	public void changeDeliveryMode(String mode) throws Exception {
		homePage = new HomePage();
		homePage.changeDeliveryMode(mode);
		if (mode.equals("PickUp")) {
			chooseStorePage = new ChooseStorePage();
			assertTrue("---Failed to change to pick up mode!---", chooseStorePage.isNavigateOnThisPage());
		}
	}

	@When("I add a new address with duplicated information")
	public void checkShippingAddress() throws Exception {
		homePage = new HomePage();
		homePage.addDuplicatedAddress();
	}

	@When("I create the new address using the existing information")
	public void createNewAddressWithExistingData() throws Exception {
		shippingListDisplayPage = new ShippingListDisplayPage();
		map = shippingListDisplayPage.createNewAddressWithExistingData();
	}

	@Then("I should see the duplicate address can't be submitted successfully")
	public void checkDuplicateData() throws Exception {
		if (!map.isEmpty()) {
			assertFalse("--- It's incorrect that the existing address submit successfully! ---", shippingListDisplayPage.isAddedAddressDuplicated(map));
		} else {
			throw new Exception("The testing data isn't ready for test!!!");
		}
	}

	@When("I check my subscription manager")
	public void checkSubscriptionManager() throws Exception {
		homePage = new HomePage();
		homePage.viewSubscriptionManager();
	}

	@When("I turn on the new header feature")
	public void openNewHeaderFeature() throws Exception {
		homepage = new HomePage();
		homepage.turnOnNewHeader();
	}

	@When("I hover my mouse on account icon of the new header")
	public void hoverOrClickOnAccountIcon() throws Exception {
		homepage.openAccountFlyOut();
	}

	@Then("I see the account fly out page is shown")
	public void showStoreFlyoutPageWithFindAStore() {
		assertTrue("--Open Account Fly Out Failed", homepage.isAccountFlyOut());
	}

	@When("I open my account overview page")
	public void iOpenMyAccountOverviewPage() {
		homepage = new HomePage();
		accountOverviewPage = homepage.openMyAccountOverviewPage();
	}

	@Then("I should view my birthday on the my account page")
	public void isViewBirthdayOnMyAccountPage() {
		assertFalse(accountOverviewPage.isViewBirthdayOnMyAccountPage());
	}

	@When("I navigate to Shipping Address section")
	public void navigateShippingAddress() {
		accountOverviewPage = new AccountOverviewPage();
		accountOverviewPage.changeShippingAddress();
	}

	@When("I delete the shipto address")
	public void deleteShiptoAddress() {
		accountOverviewPage.deleteShiptoAddress();
	}

	@Then("I should see the shipping address was disappear")
	public void shouldDeleteShippingAddressSucc() {
		assertTrue(accountOverviewPage.isDeleteShippingAddressSucc());
	}

	@When("I click add account on the my account page")
	public void clickAddODRAccount() {
		accountOverviewPage = new AccountOverviewPage();
		accountOverviewPage.clickAddAccount();
	}

	@Then("I should see message notifying the customer of single sign in conversion rules")
	public void isMsgNotifySingleSignIn() {
		assertTrue(accountOverviewPage.isMsgNotifySingleSignIn());
	}

	@When("I link odr account with the given number: \"$number\"")
	public void iLinkOdrAccount(String number) {
		accountOverviewPage.clickAddOdrAccount(number);
	}

	@Then("I should see error message invalid rewards number")
	public void iShouldSeeInvalidRewardNum() {
		assertTrue(accountOverviewPage.iShouldSeeInvalidRewardNum());
	}

	@Then("I should see error message odr accout is already linked")
	public void iShouldSeeOdrLinked() {
		assertTrue(accountOverviewPage.checkOdrAlreadyLinked());
	}

	@Then("I should see to choose the delivery method for your registration code")
	public void iShouldSeeChooseDeliveryMethodForRegistration() {
		assertTrue(accountOverviewPage.SeeChooseDeliveryMethodForRegistration());
	}

	@When("I edit my shipping information from my account overview page")
	public void iEditMyShippingInformation() {
		accountOverviewPage = new AccountOverviewPage();
		shippingListDisplayPage = accountOverviewPage.editMyShippingInformation();
	}

	@Then("I should see shipping information has filled with account info")
	public void iShouldSeeShippingInformation() {
		shippingListDisplayPage = new ShippingListDisplayPage();
		assertTrue("I should see shipping information has filled with account info", shippingListDisplayPage.isFirstShippingInformation());
	}

	@Then("I should see shipping information has filled with account info with express reg")
	public void iShouldSeeShippingInformationWithExpressReg() {
		shippingListDisplayPage = new ShippingListDisplayPage("express reg");
		assertTrue("I should see shipping information has filled with account info with express reg", shippingListDisplayPage.isSecondShippingInformation());
	}

	@When("I update the shipping information with express reg")
	public void iUpdateShippingInformationWithExpressReg() {
		shippingListDisplayPage = new ShippingListDisplayPage("express reg");
		shippingListDisplayPage.editSecondShippingInformation();
	}

	@When("I update the shipping information")
	public void iUpdateShippingInformation() {
		shippingListDisplayPage = new ShippingListDisplayPage();
		shippingListDisplayPage.editFirstShippingInformation();
	}

	@Then("I should see my billing address is the same")
	public void iShouldSeeMybillingAddressSame() {
		assertTrue("I should see my billing address is the same", shippingListDisplayPage.checkFirstPaymentInformation());
	}

	@When("I edit contact information on my account overview page")
	public void editContactInformation() {
		accountOverviewPage.editContactInfoLink();
	}

	@Then("I should see the phone number is not blank at account settings page")
	public void isPhoneNumberBlank() {
		assertTrue(accountOverviewPage.isPhoneNumberBlank());
	}

	@When("I add rewards account from look up my odr member id use phone number \"$phone\"")
	public void lookupRewardsWithPhone(String phone) {
		accountOverviewPage.lookupRewardsWithPhone(phone);
	}

	@Then("I should see phone number is invalid when add rewards account")
	public void isPhoneNumberInvalid() {
		assertTrue(accountOverviewPage.isPhoneNumberInvalid());
	}

	@Then("I should see member ID \"$memberId\" has been filled when add rewards account")
	public void isMemberIdFilled(String memberId) {
		assertTrue(accountOverviewPage.isMemberIdFilled(memberId));
	}

	@When("I change default store location from my account overview page")
	public void changeDefaultStore() {
		accountOverviewPage.clickChangeLocation();
	}

	@Then("I should see the select store button on the store locator modal")
	public void isShownSelectStoreButton() {
		assertTrue(accountOverviewPage.isShownSelectStoreButton());
	}

	@Then("I should see the merge account link")
	public void iShouldSeeTheMergeAccountLink() {
		assertTrue(accountOverviewPage.isMergeLinkAppear());
	}

	@Then("I should see the information hover text for the merge account link")
	public void iShouldSeeTheInfoForMergeLink() {
		assertTrue(accountOverviewPage.isInfoForMergeLinkAppear());
	}

	@When("I edit rewards phone number from my account overview page")
	public void editRewardsPhone() throws Exception {
		accountOverviewPage.editRewardsPhone();
	}

	@When("I input rewards phone number \"$number\"")
	public void editRewardsPhone(String number) {
		accountOverviewPage.editRewardsPhone(number);
	}

	@Then("I should see the error message the phone number is used")
	public void errorMsgPhoneUsed() {
		assertTrue(accountOverviewPage.errorMsgPhoneUsed());
	}

	@When("I merge my account in My Account Overview page")
	public void iMergeMyAccountInMyAccountOverviewPage() {
		accountOverviewPage.mergeOMXAccount();
	}

	@When("I link OMX account to OD account in My Account Overview page")
	public void iLinkOMXAccountToODAccountInMyAccountOverviewPage() {
		accountOverviewPage.linkToCurrentAccount();
	}

	@Then("I should see the account linking dialog in My Account Overview page")
	public void iShouldSeeTheAccountLinkingDialogInMyAccountOverviewPage() {
		assertTrue(accountOverviewPage.isAccountLinkingDialogAppear());
	}

	@When("I join OMX account to OD account in My Account Overview page")
	public void iJoinOMXAccountToODAccountInMyAccountOverviewPage() {
		accountOverviewPage.joinToCurrentAccount();
	}

	@Then("I should see merge account verify dialog appear")
	public void iShouldSeeMergeAccountVerifyDialogAppear() {
		rewardsPage = new RewardsPage();
		assertTrue(rewardsPage.isMergeAccountVerifyDialogAppear());
	}

	@When("I navigate to find order in My Account Overview page")
	public void iNavigateToFindOrderInMyAccountOverviewPage() {
		accountOverviewPage.navigateToFindOrderPage();
	}

	@Then("I should see there is no edit link for the address in open order")
	public void iShouldSeeThereisNoEditLinkForTheAddressInOpenOrder() {
		shippingListDisplayPage = new ShippingListDisplayPage();
		// to-do , should get the address from thread local
		assertTrue(shippingListDisplayPage.isAddressEditable(""));
	}

}
