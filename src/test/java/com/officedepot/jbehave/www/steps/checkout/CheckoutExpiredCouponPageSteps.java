package com.officedepot.jbehave.www.steps.checkout;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CouponExpiredPage;
import com.officedepot.jbehave.www.pages.deals.CouponCenterPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckoutExpiredCouponPageSteps extends BaseStep {

	private CouponExpiredPage couponExpiredPage;
	private DealCenterPage dealCenterPage;
	private CouponCenterPage couponCenterPage;

	@When("I navigate to expired coupon page from url")
	public void iNavigateToExpiredCouponPageFromUrl() throws Exception {
		couponExpiredPage = new CouponExpiredPage();
		couponExpiredPage.openNewCouponExpiredPage();
	}

	@Then("I should see the expired coupon page")
	public void iShouldSeeTheExpiredCouponPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on expired coupon page! ---", couponExpiredPage.isNavigateOnThisPage());
	}

	@When("I navigate to deal center page from deal center button")
	public void iNavigateToDealCenterPageFromDealCenterButton() throws Exception {
		dealCenterPage = couponExpiredPage.clickOnDealCenterButton();
	}

	@When("I navigate to coupon center page from online coupon button")
	public void iNavigateToCouponCenterPageFromOnlineCouponButton() throws Exception {
		couponCenterPage = couponExpiredPage.clickOnOnlineCouponButton();
	}

	@Then("I should see the coupon center page from expired coupon page")
	public void iShouldSeeTheCouponPage() throws Exception {
		Assert.assertTrue("---Failed navigate to the coupon page!---", couponCenterPage.isNavigateOnThisPage());
	}

	@When("I navigate to deal center page from carousel title link")
	public void iNavigateToCouponCenterPageFromCarouselTitleLink() throws Exception {
		dealCenterPage = couponExpiredPage.clickOnCarouselTitleLink();
	}

}
