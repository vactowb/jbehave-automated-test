package com.officedepot.jbehave.www.steps.home;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeHomePageInkAndTonerSteps extends BaseStep {

	private HomePage homePage;
	private InkAndTonerPage inkAndTonerPage;
	private String brandName;
	
    @When("I navigate to the selected manufacturer ink and toner page from selecte manufacturer by first brand")
    public void iNavigateToTheSelectedManufacturerInkAndTonerPageFromSelecteManufacturerByFirstBrand() throws Exception {
    	homePage = new HomePage();
    	inkAndTonerPage = homePage.selectTheFirstManufacturerBrand();
		brandName = inkAndTonerPage.getTheFirstPrinterBrandName();
    }

    @Then("I should see all first manufacturer brands")
    public void iShouldSeeAllFirstBrands() throws Exception {
    	Assert.assertTrue("---Failed find all the" + brandName + "!---", inkAndTonerPage.isBrandBefoundSelectByBrand(brandName));
    }

}
