
package com.officedepot.jbehave.www.steps.richrelevance;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Then;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class RrZoneSearchBrowsePageSteps extends BaseStep {

	private HomePage homePage;
	private SearchResultsListPage searchResultsListPage;
	
    @When("I search for the given search term")
    public void iSearchForTheGivenSearchTerm(@Named("searchterm") String searchterm) throws Exception {
    	homePage = new HomePage();
    	homePage.searchBy(searchterm);
    }

    @Then("I should see the results of the search term")
    public void iShouldSeeTheResultsOfTheSearchTerm(@Named("searchterm") String searchterm) throws Exception {
    	searchResultsListPage = new SearchResultsListPage();
    	Assert.assertTrue("----- Failed to navigate on the search results! ------", searchResultsListPage.isNavigateToExceptedSearchResults(searchterm));
    }

    @Then("I should see the rrzone as part of the master column")
    public void iShouldSeeTheRrzoneAsPartOfTheMasterColumn() throws Exception {
    	searchResultsListPage = new SearchResultsListPage();
    	Assert.assertTrue("----- Failed to navigate on the search results! ------", searchResultsListPage.goToRichRelevanceZone());
    }

}