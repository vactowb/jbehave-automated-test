package com.officedepot.jbehave.www.steps.compare;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class DiscontinuedSkuSteps extends BaseStep {

	private SkuDetailPage skuDetailPage;
	
    @Then("I should see the discontinued sku page loaded for the product")
    public void iShouldSeeTheDiscontinuedSkuPageLoadedForTheProduct(@Named("product") String product) throws Exception {
    	skuDetailPage = new SkuDetailPage();
		Assert.assertTrue("----- Failed to navigate to the discontinued sku details! ------", skuDetailPage.isNavigateToDiscontinuedSkuPage("The item is not currently available."));
    }

	
	
}
