package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.AddYourPriceWindow;
import com.officedepot.jbehave.www.pages.cartridge.ConsiderRelatedItemsPage;
import com.officedepot.jbehave.www.pages.cartridge.InkjetSearchPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeTechnologySeePriceInCartSteps extends BaseStep {

	private HomePage homePage;
	private InkjetSearchPage inkjetSearchPage;
	private AddYourPriceWindow addYourPriceWindow;
	private ConsiderRelatedItemsPage considerRelatedItemsPage;
	
    @When("I access inkjet search page from url")
    public void iAccessInkjetSearchPageFromUrl() throws Exception {
    	homePage = new HomePage();
    	inkjetSearchPage = homePage.navigateToInkjetSearchPageFromUrl();
    }

    @Then("I should see inkjet search page")
    public void iShouldSeeInkjetSearchPage() throws Exception {
    	Assert.assertTrue("---Failed to see inkjet search page", inkjetSearchPage.isNavigateOnThisPage());
    }

    @When("I open add your price window and keep it in my cart")
    public void iOpenAddYourPriceWindowAndKeepItInMyCart() throws Exception {
    	addYourPriceWindow = inkjetSearchPage.clickOnSeePriceInCartButton();
    	considerRelatedItemsPage = addYourPriceWindow.clickOnKeepInMyCartButton();
    }

    @Then("I should see consider related items page")
    public void iShouldSeeConsiderRelatedItemsPage() throws Exception {
    	Assert.assertTrue("---Failed to see consider related items page", considerRelatedItemsPage.isInCurrentPage());
    }

    @When("I continue it to my cart")
    public void iContinueItToMyCart() throws Exception {
    	considerRelatedItemsPage.clickOnContinueToCartButton();
    }

}
