package com.officedepot.jbehave.www.steps.deals;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.CleaningChemicalsPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class DealsFlyoutSteps extends BaseStep {

	private HomePage homePage;
	private TopNavigationPage topNavigationPage;
	private DealCenterPage dealCenterPage;
	
    @Given("I set new on sale icon ABtest cookie")
    public void iSetNewOnSaleIconABtestCookie() throws Exception {
    	homePage = new HomePage();
    	homePage.homePageSetCookie("dealsTxtOnSale");
    	homePage.openHomePage();
    }
	
    @When("I navigate to on sale page from deals flyout")
    public void iNavigateToOnSalePageFromDealsFlyout() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewDealsNavigatorOnTopNavigation();
		dealCenterPage = topNavigationPage.clickOnSaleIcon();
    }

    @Then("I should see the on sale page")
    public void iShouldSeeTheOnSalePage() throws Exception {

    }

    @When("I navigate to coupons page from deals flyout")
    public void iNavigateToCouponsPageFromDealsFlyout() throws Exception {

    }

    @Then("I should see the coupons page")
    public void iShouldSeeTheCouponsPage() throws Exception {

    }

    @When("I navigate to weekly ad page from deals flyout")
    public void iNavigateToWeeklyAdPageFromDealsFlyout() throws Exception {

    }

    @Then("I should see to weekly ad page")
    public void iShouldSeeToWeeklyAdPage() throws Exception {

    }

    @When("I navigate to spring essentials page from deals flyout")
    public void iNavigateToSpringEssentialsPageFromDealsFlyout() throws Exception {

    }

    @Then("I should see the spring essentials page")
    public void iShouldSeeTheSpringEssentialsPage() throws Exception {

    }

}
