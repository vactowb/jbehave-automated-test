package com.officedepot.jbehave.www.steps.shopping;

import junit.framework.Assert;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.register.CustomerRegistrationPage;
import com.officedepot.jbehave.www.pages.register.ExpressRegisterPage;
import com.officedepot.jbehave.www.pages.shopping.list.CslListDetailPage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HeaderMyListsSteps extends BaseStep {

	private PageHeader pageHeader;
	private MyListPage myListPage;
	private CustomerRegistrationPage customerRegistrationPage;
	private CslListDetailPage cslListDetailPage;
	private SkuDetailPage skuDetailPage;

	@Given("I have no saved items")
	public void ihavenosaveditmes() throws Exception {
	}

	@Given("I have no created shopping list")
	public void iHaveNoCreatedShoppingList() throws Exception {
	}

	@When("I request to view \"$navigator\" from header")
	public void irequesttoviewmylistsfromheader(String navigator) throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader(navigator);
	}

	@When("I request to view all my lists from My Lists view")
	public void irequesttoviewallmylistsfromMyListsview() throws Exception {
		myListPage = pageHeader.viewAllLists();
	}

	@Then("I should see all my shopping lists in shopping lists page")
	public void ishouldseeallmyshoppinglistsinshoppinglistspage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on my list page! ---", myListPage.isInCurrentPage());
	}

	@Then("I should be noticed that I need to log in or create an account")
	public void iShouldBeNoticedThatINeedToLogIn() throws Exception {
		Assert.assertTrue("--- Failed to verify the login notice! ---", pageHeader.isNoticeDisplayedViaNotLogin());
	}

	@When("I login from my lists view")
	public void iLoginfromMyListsview() throws Exception {
		pageHeader.loginFromMyLists();
	}

	@Then("I should see account login dialog")
	public void iShouldNavigateToLoginPage() throws Exception {
		pageHeader.isLoginDialogDisplayed();
	}

	@When("I create an account from my lists view")
	public void iCreateAccountfromMyListsview() throws Exception {
		customerRegistrationPage = pageHeader.createAccountFromMyLists();
	}

	@Then("I should navigate to customer registration page")
	public void iShouldNavigateTorRgistrationPage() throws Exception {
		// Assert.assertTrue("--- Failed to navigate on customer registration page! ---", customerRegistrationPage.isNavigateOnThisPage());
		Assert.assertTrue("--- Failed to navigate on customer registration page! ---", new ExpressRegisterPage().isNavigateOnThisPage());
	}

	@Then("I should be noticed that I have no saved items")
	public void iShouldBenoticedNoSavedItems() throws Exception {
		Assert.assertTrue("--- Failed to verify the notice of no saved item! ---", pageHeader.isNoSavedItemInMyLists());
	}

	@Then("I should see my shopping list \"$listName\" in My Lists view")
	public void iShouldSeeMyShoppingList(String listName) throws Exception {
		Assert.assertTrue("--- Failed to verify the shopping list! ---", pageHeader.isListDisplayedInMyLists(listName));
	}

	@Then("I should see my saved items in My Lists view")
	public void iShouldSeeOfMySavedItemsInMyListsView() throws Exception {
		Assert.assertTrue("--- Failed to verify the items in my lists! ---", pageHeader.getItemQtyOfMyLists());
	}

	@Then("I should see my shopping lists in My Lists view")
	public void iShouldSeeOfMyShoppingListInMyListsView() throws Exception {
		Assert.assertTrue("--- Failed to verify the my lists! ---", pageHeader.getListQtyOfMyLists());
	}

	@When("I request to manage lists from My Lists view")
	public void iRequestToManageListsFromMyListsView() throws Exception {
		myListPage = pageHeader.manageListsFromMyLists();
	}

	@Then("I should navigate to my shopping lists page")
	public void iShouldNavigateToMyShoppingListsPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on my list page! ---", myListPage.isInCurrentPage());
	}

	@When("I request to view list from My Lists view")
	public void iRequestToViewListFromMyListsView() throws Exception {
		cslListDetailPage = pageHeader.viewListFromMyLists();
	}

	@Then("I should see the details of my shopping list \"$listName\"")
	public void iShouldSeeTheDetailsOfMyShoppingList(String listName) throws Exception {
		cslListDetailPage.isInCurrentPage();
		Assert.assertEquals("--- Failed to verify the list name! ---", listName, cslListDetailPage.getListName());
	}

	@When("I request to create a new list from My Lists view")
	public void iRequestToCreateANewListFromMyListsView() throws Exception {
		myListPage = pageHeader.createNewListFromMyLists();
	}

	@When("I request to view the sku details from my lists")
	public void iRequestToViewSkuDetailsFromMyListsView() throws Exception {
		skuDetailPage = pageHeader.viewSkuDetials();
	}

	@Then("I should navigate to product details page")
	public void iShouldNavigateToProductDetailsPage() throws Exception {
		skuDetailPage = new SkuDetailPage();
		Assert.assertTrue("--- Failed to navigate on sku details page! ---", skuDetailPage.isInCurrentPage());
	}

	@When("I select all list items")
	public void selectAllListItems() {
		cslListDetailPage.selectAllListItems();
	}

	@When("I remove the selected items")
	public void removeSelectItems() {
		cslListDetailPage.removeSelectItems();
	}

	@Then("I should see all the list items has been deleted")
	public void isAllListItemsDeleted() throws Exception {
		Assert.assertTrue(cslListDetailPage.isAllListItemsDeleted());
	}

}
