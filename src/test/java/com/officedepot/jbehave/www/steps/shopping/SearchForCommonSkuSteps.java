package com.officedepot.jbehave.www.steps.shopping;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SearchForCommonSkuSteps extends BaseStep {

	private HomePage homePage;
	private SkuDetailPage skuDetailPage;

	@When("I search for SKU with the given SKU Id \"$skuid\" from home page")
	public void iSearchForSKUWithTheGivenSKUIdFromHomePage(String skuid) throws Exception {
		// homePage = new HomePage();
		// homePage.openHomePageWithNewHeader();
		// homePage.searchBy(skuid);
	}

	@Then("I should see sku detail page for the given SKU \"$skuid\"")
	public void iShouldSeeSkuDetailPageForTheGivenSKU(String skuid) throws Exception {
		// skuDetailPage = new SkuDetailPage();
		// Assert.assertFalse("---Failed to nsee sku details!---",
		// skuDetailPage.isTextPresentOnPage(skuid));
	}

}
