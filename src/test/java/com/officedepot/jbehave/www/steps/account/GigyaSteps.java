package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class GigyaSteps extends BaseStep {

	private HomePage homePage;

	@Then("I should get gigya header information")
	public void suppressGigyaHeader() throws Exception {
		homePage = new HomePage();
		assertTrue("---Cannot get the gigya header when you log out!---", homePage.getGigyaHeader());
	}

	@When("I move the mouse hover on Account")
	public void moveTheMouseHoverOnAccount() throws Exception {
		homePage = new HomePage();
		homePage.mouseHoverAccountFlyOut();
	}

	@Then("I should see social icons in the Account fly out under Log in")
	public void shouldSeeSocialIconsIntheAccountFlyOut() {
		homePage = new HomePage();
		assertTrue("---Cannot get social icons in the Account fly out under Log in!---", homePage.getSocialIconsUnderLogin());
	}
}
