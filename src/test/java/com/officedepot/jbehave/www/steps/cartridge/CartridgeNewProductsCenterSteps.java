package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.NewProductsCenterPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeNewProductsCenterSteps extends BaseStep {

	private NewProductsCenterPage newProductsCenterPage;

	@When("I access new products center page")
	public void iAccessNewProductsCenterPage() throws Exception {
		newProductsCenterPage = new NewProductsCenterPage();
		newProductsCenterPage.openNewProductsCenterPage();
	}

	@Then("I should see the new products center page")
	public void iShouldSeeTheNewProductsCenterPage() throws Exception {
		newProductsCenterPage = new NewProductsCenterPage();
		Assert.assertTrue("---Failed navigate to the new products center page!---", newProductsCenterPage.isInCurrentPage());
	}

	@When("I navigate to categories page from featured categories matrix deal center")
	public void iNavigateToCategoriesPageFromFeaturedCategoriesMatrixDealCenter() throws Exception {
		newProductsCenterPage.navigateToCategoriesPageFromFeaturedCategoriesMatrixDealCenter();
	}

}
