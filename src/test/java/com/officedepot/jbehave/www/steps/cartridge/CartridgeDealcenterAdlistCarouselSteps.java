package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.ProductDetailsPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeDealcenterAdlistCarouselSteps extends BaseStep {

	private DealCenterPage dealCenterPage;
	private ProductDetailsPage productDetailsPage;
	private String productName;

	@When("I navigate to product details page from adlist carousel")
	public void iNavigateToProductDetailsPageFromAdlistCarousel() throws Exception {
		dealCenterPage = new DealCenterPage();
		productName = dealCenterPage.getTheFirstAdlistProductName().substring(0, 10);
		productDetailsPage = dealCenterPage.navigateToTheFirstAdListProductDetailsPage();
	}

	@Then("I should see the product details page from adlist carousel")
	public void iShouldSeeTheProductDetailsPageFromAdlistCarousel() throws Exception {
		Assert.assertTrue("---Failed navigate to the product details page!---", productDetailsPage.isInCurrentPage(productName));
	}

	@When("I change next four skus from adlist carousel")
	public void iChangeNextFourSkusFromAdlistCarousel() throws Exception {

	}

	@Then("I should see next four skus from adlist carousel")
	public void iShouldSeeNextFourSkusFromAdlistCarousel() throws Exception {

	}

	@When("I change previous four skus from adlist carousel")
	public void iChangePreviousFourSkusFromAdlistCarousel() throws Exception {

	}

	@Then("I should see previous four skus from adlist carousel")
	public void iShouldSeePreviousFourSkusFromAdlistCarousel() throws Exception {

	}

}
