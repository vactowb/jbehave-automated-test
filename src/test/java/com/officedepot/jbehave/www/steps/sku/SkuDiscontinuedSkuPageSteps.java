package com.officedepot.jbehave.www.steps.sku;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.sku.DiscontinuedSkuDetailPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SkuDiscontinuedSkuPageSteps extends BaseStep {

	private DiscontinuedSkuDetailPage discontinuedSkuDetailPage = new DiscontinuedSkuDetailPage();
	private SkuDetailPage skuDetailPage;
	
    @Then("I should see the discontinued sku page")
    public void iShouldSeeTheDiscontinuedSkuPage() throws Exception {
    	Assert.assertTrue("----- Failed to see the discontinued sku page! ------", discontinuedSkuDetailPage.isInCurrentPage());
    }

    @When("I navigate to available sku page from the first product alternate")
    public void iNavigateToAvailableSkuPageFromTheFirstProductAlternate() throws Exception {
    	skuDetailPage = discontinuedSkuDetailPage.clickOnFirstProductAlternate();
    }

    @Then("I should see the sku detail page")
    public void iShouldSeeTheSkuDetailsPage() throws Exception {
		Assert.assertTrue("----- Failed to navigate on the sku details page! ------", skuDetailPage.isInCurrentPage());
    }

}
