package com.officedepot.jbehave.www.steps.home;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.SaveUpPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeHomePageHeroSliderSteps extends BaseStep {

	private HomePage homePage;
	private SaveUpPage saveUpPage;
	
    @When("I navigate to the first advertisement page from learn more link")
    public void iNavigateToTheFirstAdvertisementPageFromLearnMoreLink() throws Exception {
    	homePage = new HomePage();
    	saveUpPage = homePage.clickOnTheFirstLearnMoreLink();
    }

    @Then("I should see the first advertisement page")
    public void iShouldSeeTheFirstAdvertisementPage() throws Exception {
    	assertTrue("I should see the first advertisement page", saveUpPage.isInCurrentPage());
    }

}
