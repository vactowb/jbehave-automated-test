package com.officedepot.jbehave.www.steps.store;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.storelocator.FindAStorePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class StoreFindAStoreShowOfficeMaxStoresSteps extends BaseStep {

	private FindAStorePage findAStorePage;
	
    @When("I choose office max stores from find a store page")
    public void iChooseOfficeMaxStoresFromFindAStorePage() throws Exception {
    	findAStorePage = new FindAStorePage();
    	findAStorePage.chooseOfficeMaxStores();
    }

    @Then("I should see office max stores")
    public void iShouldSeeOfficeMaxStores() throws Exception {
    	Assert.assertTrue("--- Failed to see office max stores! ---", findAStorePage.isOnlyShowOfficemax());
    }

}
