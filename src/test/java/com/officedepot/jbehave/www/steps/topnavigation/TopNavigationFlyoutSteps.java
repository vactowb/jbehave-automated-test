package com.officedepot.jbehave.www.steps.topnavigation;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.CleaningChemicalsPage;
import com.officedepot.jbehave.www.pages.cartridge.TapeAndAdhesivesPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class TopNavigationFlyoutSteps extends BaseStep {

	private TopNavigationPage topNavigationPage;
	private BasicSuppliesCategoriesPage basicSuppliesCategoriesPage;
	private TapeAndAdhesivesPage tapeAndAdhesivesPage;
	private CleaningChemicalsPage cleaningChemicalsPage;

	@When("I navigate to basic supplies categories page frome office supplies flyout text link")
	public void iNavigateToBasicSuppliesCategoriesPageFromeOfficeSuppliesFlyoutTextLink() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewOfficeSuppliesNavigatorOnTopNavigation();
		basicSuppliesCategoriesPage = topNavigationPage.navigateToBasicSuppliesCategoriesPage();
	}

	@Then("I should see the basic supplies categories page")
	public void iShouldSeeTheBasicSuppliesCategoriesPage() throws Exception {
		Assert.assertTrue("---Failed navigate to the basic supplies categories page!---", basicSuppliesCategoriesPage.isInCurrentPage());
	}

	@When("I navigate to tape and adhesives page frome office supplies flyout text link")
	public void iNavigateToTapeAndAdhesivesPageFromeOfficeSuppliesFlyoutTextLink() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewOfficeSuppliesNavigatorOnTopNavigation();
		tapeAndAdhesivesPage = topNavigationPage.navigateToTapeAndAdhesivesPageCategoriesPage();
	}

	@Then("I should see the tape and adhesives categories page")
	public void iShouldSeeTheTapeAndAdhesivesCategoriesPage() throws Exception {
		tapeAndAdhesivesPage = new TapeAndAdhesivesPage();
		Assert.assertTrue("---Failed navigate to the tape and adhesives categories page!---", tapeAndAdhesivesPage.isInCurrentPage());
	}
	
    @When("I navigate to cleaning chemicals page from cleaning flyout")
    public void iNavigateToCleaningChemicalsPageFromCleaningFlyout() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewCleaningNavigatorOnTopNavigation();
		cleaningChemicalsPage = topNavigationPage.clickOnCleaningChemicalsLink();
    }

    @Then("I should see the cleaning chemicals page")
    public void iShouldSeeTheCleaningChemicalsPage() throws Exception {
    	Assert.assertTrue("---Failed to see the cleaning chemicals page", cleaningChemicalsPage.isInCurrentPage());
    }

}
