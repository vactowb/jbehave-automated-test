package com.officedepot.jbehave.www.steps.emailsignup;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.emailsignup.EmailSignUpPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class EmailSignupNavigateToSignupPageSteps extends BaseStep {

	private HomePage homePage;
	private EmailSignUpPage emailSignUpPage;
	
    @When("I navigate to email sign up page from email button")
    public void iNavigateToEmailSignUpPageFromEmailButton() throws Exception {
    	homePage = new HomePage();
    	emailSignUpPage = homePage.clickOnEmailSignUpEmailButton();
    }

    @Then("I should see the email sign up page")
    public void iShouldSeeTheEmailSignUpPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to the email sign up page!---", emailSignUpPage.isNavigateOnThisPage());
    }

}
