package com.officedepot.jbehave.www.steps.checkout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.ShippingListDisplayPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutBillingPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutLoginPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutReviewPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutShipPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutThankYouPage;
import com.officedepot.jbehave.www.pages.checkout.PayPalPage;
import com.officedepot.jbehave.www.pages.checkout.SecurityQuestionsWindow;
import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckoutSteps extends BaseStep {

	private CheckoutLoginPage checkoutLoginPage;
	private CheckoutBillingPage checkoutBillingPage;
	private CheckoutReviewPage checkoutReviewPage;
	private CheckoutShipPage checkoutShipPage;
	private ShoppingCartPage shoppingCartPage;
	private PayPalPage payPalPage;
	private SecurityQuestionsWindow securityQuestionsWindow;
	private ShippingListDisplayPage shippingListDisplayPage;
	private FindYourProductPage findYourProductPage;
	private String orderSummaryTotalPrice;
	private CheckoutThankYouPage checkoutThankYouPage;

	@When("I click remove for sku \"$sku\" which exists on index \"$index\"")
	public void iClickRemoveForSku(String sku, int index) throws Exception {
		checkoutShipPage = new CheckoutShipPage();
		assertTrue("--- Failed to navigate to checkout Step 2 of 2! ---", checkoutShipPage.isNavigateOnThisPage());
		checkoutShipPage.removeSku(sku, index);
	}

	@When("I continue to checkout at checkout second step")
	public void continueToCheckoutAtSecondStep() throws Exception {
		checkoutShipPage = new CheckoutShipPage();
		checkoutBillingPage = checkoutShipPage.continueCheckout();
	}

	@Then("I should see message telling me that I cannot use cash check payment option")
	public void seeMsgNotUseCash() {
		assertTrue(checkoutBillingPage.seeMsgNotUseCash());
	}

	@When("I change address at checkout second step")
	public void changeAddressCheckoutPage2() throws Exception {
		checkoutShipPage = new CheckoutShipPage();
		checkoutShipPage.changeAddressCheckoutPage2();
	}

	@When("I change address at checkout fourth step")
	public void changeAddressCheckoutPage4() throws Exception {
		checkoutShipPage.changeAddressCheckoutPage2();
	}

	@Then("I should see change shipping information page")
	public void iSeeChangeShippingInformationPage() {
		assertTrue(checkoutShipPage.seeChangeShippingInformationpage());
	}

	@When("I select some address from shipping information page")
	public void selectAddressFromShippingInforPage() {
		checkoutShipPage.shipThisAddress();
	}

	@When("I add other shipping address to checkout $addressInfo")
	public void iAddOtherShippingAddress2Checkout(ExamplesTable addressInfo) {
		String firstName = addressInfo.getRow(0).get("FirstName");
		String lastName = addressInfo.getRow(0).get("LastName");
		String address = addressInfo.getRow(0).get("Address");
		String city = addressInfo.getRow(0).get("City");
		String state = addressInfo.getRow(0).get("State");

		checkoutShipPage.addOtherShippingAddress2Checkout(firstName, lastName, address, city, state);
		checkoutShipPage.shipping2ThisAddress();
	}

	@Then("I should not see Available & Backorder at the order summary")
	public void shouldNotSeeAvailableBackorderOrderSummary() {
		checkoutShipPage = new CheckoutShipPage();
		assertTrue(checkoutShipPage.shoulSeeAvailableBackorderOrderSummary());
	}

	@Then("I should go to checkout second step page")
	public void shouldGoCheckoutAtSecondStep() throws Exception {
		checkoutShipPage = new CheckoutShipPage();
		assertTrue(checkoutShipPage.isNavigateOnThisPage());
	}

	@When("I fill all mandatory info at checkout second step")
	public void iFillAllMandatoryInfoAtCheckoutSecondStep() throws Exception {
		checkoutShipPage.continueAnonymousCheckoutAtStep2();
	}

	@When("I fill all info at checkout second step with invalid address")
	public void iFillInvalidAddressAtCheckoutSecondStep() throws Exception {
		checkoutShipPage.continueAnonymousCheckoutAtStep2WithInvalidAddress();
	}

	@When("I fill all info at checkout second step with invalid phone number")
	public void iFillInvalidPhoneNumberAtCheckoutSecondStep() throws Exception {
		checkoutShipPage.continueAnonymousCheckoutAtStep2WithInvalidPhoneNumber();
	}

	@When("I fill all info at checkout second step with email and retype Email enter different values")
	public void fillWithDiffRetypeEmail() {
		checkoutShipPage.fillWithDiffRetypeEmail();
	}

	@When("I fill all info at checkout second step with invalid address for express reg user $userInfro")
	public void iFillInvalidAddressAtCheckoutSecondStepForExpressRegUser(ExamplesTable userInfro) throws Exception {
		String address = userInfro.getRow(0).get("Address");
		String city = userInfro.getRow(0).get("City");
		String state = userInfro.getRow(0).get("State");
		checkoutShipPage = new CheckoutShipPage();
		checkoutShipPage.inputAddressAtCheckoutStep2ExpressReg(address, city, state);
	}

	@When("I fill all info at checkout third step with address for express reg user $userInfro")
	public void iFillInvalidAddressAtCheckoutThirdStepForExpressRegUser(ExamplesTable userInfro) throws Exception {
		String address = userInfro.getRow(0).get("Address");
		String city = userInfro.getRow(0).get("City");
		String state = userInfro.getRow(0).get("State");
		checkoutBillingPage.inputAddressAtCheckoutStep2ExpressReg(address, city, state);
	}

	@Then("I should see the billing information is not blank")
	public void isheBillingInforBlank() {
		assertTrue(checkoutBillingPage.isheBillingInforBlank());
	}

	@Then("I should see prompted with error for invalid billing address")
	public void iShouldSeePromptedWithErrorInvalidBillingAddress() {
		assertTrue("I should see prompted with error for invalid billing address", checkoutShipPage.isPromptedWithErrorInvalidBillingAddress());
	}

	@Then("I should see error message will pop in checkout second step page for express reg: \"$errorMessage\"")
	public void ishouldSeeErrorMessagePopForExpressReg(String errorMessage) {
		assertTrue(checkoutShipPage.checkErrorMessagePopForExpressReg());
		assertTrue(checkoutShipPage.checkErrorAddressPopForExpressReg(errorMessage));
	}

	@Then("I should see error message will pop in checkout third step page for express reg: \"$errorMessage\"")
	public void ishouldSeeErrorMessagePopForExpressRegThirdStep(String errorMessage) {
		assertTrue(checkoutBillingPage.checkErrorAddressPopForExpressReg(errorMessage));
	}

	@When("I edit the invalid address from error message pop")
	public void editAddressFromErrorMessagePop() {
		checkoutShipPage.clickEditShippingButton();
	}

	@When("I continue with the invalid address from error message pop")
	public void continueFromErrorMessagePop() {
		checkoutShipPage.clickContinue();
	}

	@Then("I should see prompted with error for invalid address")
	public void iShouldSeePromptedWithErrorInvalidAddress() {
		assertTrue("I should see prompted with error for invalid address", checkoutShipPage.isPromptedWithErrorInvalidAddress());
	}

	@Then("I should see shipment summary")
	public void checkShipmentSummary() throws Exception {
		assertTrue(checkoutShipPage.isShipmentSummaryExistedOnPage());
	}

	@Then("I should navigate to checkout third step")
	public void iShouldNavigateToCheckouThirdStep() throws Exception {
		assertTrue("--- Failed to navigated on check out billing page! ---", checkoutBillingPage.isNavigateOnThisPage());
	}

	@Then("I should see fee sku in order details: $exceptedOrderDetail")
	public void iShouldSeeFeeSkuInOrderDetails(ExamplesTable expectedOrderDetail) throws Exception {
		checkoutShipPage = new CheckoutShipPage();
		List<String> tableHeader = expectedOrderDetail.getHeaders();
		for (Map<String, String> expectedRow : expectedOrderDetail.getRows()) {
			for (int i = 0; i < tableHeader.size(); i++) {
				String expectedValue = expectedRow.get(tableHeader.get(i));
				String realValue = checkoutShipPage.getProcessingFeeContent();
				assertTrue("---Assert the '" + tableHeader.get(i) + "' failed!---", realValue.contains(expectedValue));
			}
		}
	}

	@When("I change payment at checkout fourth step")
	public void changePaymentAtCheckoutFourthStep() throws Exception {
		checkoutReviewPage.changePaymentMenthod();
	}

	@When("I place order at checkout fourth step")
	public void placeOrderAtCheckoutFourthStep() throws Exception {
		checkoutThankYouPage = checkoutReviewPage.placeOrder();
	}

	@When("I edit the payment information at checkout fourth step")
	public void editPaymentInfo() {
		checkoutReviewPage.changePayment();
	}

	@Then("I should see the cvv blank at checkout third step")
	public void isCvvBlankAtCheckoutThirdStep() {
		assertTrue(checkoutBillingPage.isCvvInputBlank());
	}

	@Then("I see order palced successful")
	public void checkOrderPlacedSuccessful() throws Exception {
		assertTrue(checkoutThankYouPage.isInThankYouPage());
	}

	@Then("I should see the customer PO number at the thank page")
	public void seeCustomerPONumber() {
		checkoutThankYouPage = new CheckoutThankYouPage();
		assertTrue(checkoutThankYouPage.isSeeCustomerPONumber());
	}

	@Then("I should see Office Depot Rewards or MaxPerks number given")
	public void seeOdrNumber() {
		checkoutThankYouPage = new CheckoutThankYouPage();
		assertTrue(checkoutThankYouPage.seeOdrNumber(SeleniumUtilsWWW.ODRMEMBER));
	}

	@Then("I should see payment method is paypal")
	public void checkIfPaymentMethodIsPayPal() throws Exception {
		assertTrue(checkoutReviewPage.isPayPalThePaymentMethod());
	}

	@When("I continue checkout as a guest")
	public void continueCheckoutAsGuest() throws Exception {
		checkoutLoginPage = new CheckoutLoginPage();
		checkoutShipPage = checkoutLoginPage.continueAsGuest();
	}

	@When("I add coupon at checkout third step page")
	public void iAddCouponAtCheckoutThirdStep(@Named("coupon") String coupon) throws Exception {
		checkoutBillingPage.addCouponAtCheckoutBillingStep(coupon);
	}

	@Then("I see the coupon \"$couponCode\" has been added at checkout third step")
	public void iSeeCouponHasBeenAddedAtCheckoutThirdStep(String couponCode) throws Exception {
		assertTrue(checkoutBillingPage.isCouponAppliedAtCheckoutThirdStep(couponCode));
	}

	@When("I add and apply a new gift card \"$giftcardNumber\" : \"$pinNumber\"")
	public void addAndApplyAGiftCard(String giftcardNumber, String pinNumber) throws Exception {
		checkoutBillingPage.applyAGiftcard(giftcardNumber, pinNumber);
	}

	@Then("I see the new gift card applied")
	public void checkIfnewGiftcardApplied() throws Exception {
		assertTrue(checkoutBillingPage.isGiftcardApplied());
	}

	@Then("I see the gift card has been removed")
	public void checkIfnewGiftcardRemove() throws Exception {
		assertFalse(checkoutBillingPage.isGiftcardApplied());
	}

	@When("I remove the gift card at checkout third step")
	public void removeGigtCard() {
		checkoutBillingPage.removeGigtCard();
	}

	@When("I continue to checkout at checkout third step")
	public void continueToCheckoutAtCheckoutThirdStep() throws Exception {
		checkoutBillingPage.inputCVVIfNeeded();
		checkoutReviewPage = checkoutBillingPage.continueCheckoutAtThirdStep();
	}

	@Then("I should see the warning message payment information is required")
	public void seeWarningMessagePaymentInforRequired() {
		assertTrue(checkoutBillingPage.seeWarningMessagePaymentInforRequired());
	}

	@Then("I should see the error message credit card needed")
	public void seeErrorMsgCreditCardNeeded() {
		assertTrue(checkoutBillingPage.seeErrorMsgCreditCardNeeded());
	}

	@When("I enter a customer PO number at checkout third step")
	public void enterCustomerPO() {
		checkoutBillingPage.inputCustomerPO();
	}

	@When("I select to apply this order to a different Office Depot Rewards or MaxPerks number")
	public void selectOtherODR() {
		checkoutBillingPage.clickSelectOtherODR();
	}

	@Then("I should see linked office depot rewards number")
	public void seeLinkedODRNumber() {
		assertTrue(checkoutBillingPage.seeLinkedODRNumber(SeleniumUtilsWWW.ODRMEMBER));
	}

	@Then("I should see linked office max rewards number")
	public void seeLinkedMaxNumber() {
		assertTrue(checkoutBillingPage.seeLinkedMaxNumber("123543060"));
	}

	@Then("I should see Office Depot Rewards or MaxPerks number inputbox")
	public void seeOtherODRAndOMNumber() {
		assertTrue(checkoutBillingPage.seeOtherODRAndOMNumber());
	}

	@When("I enter Office Depot Rewards or MaxPerks number")
	public void enterOdrNumber() {
		checkoutBillingPage.enterOdrNumber(SeleniumUtilsWWW.ODRMEMBER);
	}
	
	@When("I apply the MaxPers number")
	public void applyOdrNumber() {
		checkoutBillingPage.applyOdrNumber();
	}

	@When("I enter an invalid Office Depot Rewards or MaxPerks number")
	public void enterInvaildOdrNumber() {
		checkoutBillingPage.enterOdrNumber("1111");
	}

	@Then("I should see Your Member Number is incorrect")
	public void isYourMemIncorrect() {
		assertTrue(checkoutBillingPage.isYourMemIncorrect());
	}

	@When("I continue to checkout at checkout third step without input CVV")
	public void continueToCheckoutAtCheckoutThirdStepWithoutCvv() throws Exception {
		checkoutReviewPage = checkoutBillingPage.continueCheckoutAtThirdStep();
	}

	@Then("I should see prompted with error for invalid phone number")
	public void checkpromptedErrorInvalidPhoneNumber() {
		assertTrue(checkoutShipPage.checkpromptedErrorInvalidPhoneNumber());
	}

	@Then("I should see prompted with error for re-type email is invalid")
	public void isReTypeEmailInvalid() {
		assertTrue(checkoutShipPage.isReTypeEmailInvalid());
	}

	@When("I accept the teams and conditions")
	public void iAcceptTheTeamsAndConditions() throws Exception {
		checkoutBillingPage.acceptTermsConditions();
	}

	@When("I save credit card information to my account")
	public void iSaveCreditCartInformation() throws Exception {
		checkoutBillingPage.saveCreditCartInformation();
	}

	@When("I continue to checkout with paypal at checkout third step")
	public void continueToCheckoutWithPaypalAtCheckoutThirdStep() throws Exception {
		checkoutReviewPage = checkoutBillingPage.checkoutWithPaypal();
	}

	@When("I continue to checkout with anonymous at checkout third step")
	public void continueToCheckoutWithAnonymous() throws Exception {
		checkoutReviewPage = checkoutBillingPage.continueCheckoutAtThirdStep();
	}

	@When("I choose to pay by given credit card")
	public void iChooseToPayByCreditCard() throws Exception {
		if (checkoutBillingPage == null) {
			checkoutBillingPage = new CheckoutBillingPage();
		}
		checkoutBillingPage.payByCreditCard();
	}

	@When("I choose to pay by paypal")
	public void chooseToPayByPayPal() throws Exception {
		checkoutBillingPage.payByPayPal();
	}

	@When("I choose to add coupon at checkout fouth step")
	public void chooseToAddCoupon() throws Exception {
		checkoutBillingPage = checkoutReviewPage.addCoupon();
	}

	@When("I choose to add card at checkout fouth step")
	public void chooseToAddCard() throws Exception {
		checkoutBillingPage = checkoutReviewPage.addCard();
	}

	@When("I punch out to PayPal from shopping cart")
	public void ipouchOutToPayPalFromShoppingCart() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		shoppingCartPage.clickPayPalButton();

	}

	@Then("I should return the page at checkout fourth step")
	public void iShouldReturnthepageatCheckoutfourthstep() throws Exception {
		checkoutReviewPage = new CheckoutReviewPage();
		assertTrue(checkoutReviewPage.isCurrentPageCheckoutStepFourPage());
	}

	@When("I enter member number at checkout fourth step")
	public void enterMemberNum() throws Exception {
		checkoutReviewPage = new CheckoutReviewPage();
		checkoutReviewPage.enterMemberNum();
		checkoutBillingPage = new CheckoutBillingPage();
		checkoutBillingPage.enterOdrNumber(SeleniumUtilsWWW.ODRMEMBER);
		checkoutReviewPage.enterApplyMem();
	}

	@When("I uncheck the Save BAID check box at checkout fourth step")
	public void uncheckSaveBAIDCheckBox() {
		assertFalse(checkoutReviewPage.unselectCheckboxSaveMyBillingInfor());
	}

	@When("I edit the shipping address at checkout fourth step")
	public void changeShippingAddressAtFourthStep() {
		checkoutShipPage = checkoutReviewPage.changeShipInfo();
		checkoutShipPage.clickEditShippingLink();
		checkoutShipPage.clickSaveAndCheckoutButton();
	}

	@Then("I should get a message on step second to re-auth the od card")
	public void isReauthPaypalAccountMsgShow() {
		assertTrue(checkoutShipPage.isReauthPaypalAccountMsgShow());
	}

	@Then("I should see the PayPal page opened successful")
	public void iShouldSeeThePaypalPageOpenedSuccessful() throws Exception {
		payPalPage = new PayPalPage();
	}

	@When("Log in the paypal with: $paypalAccount")
	public void logInPaypal(ExamplesTable paypalAccount) throws Exception {
		if (null == payPalPage) {
			payPalPage = new PayPalPage();
		}
		payPalPage.loginPaypalPage(paypalAccount.getRow(0).get("Username"), paypalAccount.getRow(0).get("Password"));
		payPalPage.clickPaypalContinue();
	}

	@When("I donot save my billing information")
	public void idonotSaveMyBillingInformation() throws Exception {
		if (null == checkoutReviewPage) {
			checkoutReviewPage = new CheckoutReviewPage();
		}
		assertFalse(checkoutReviewPage.unselectCheckboxSaveMyBillingInfor());
	}

	@When("I save my payment paypal billing information")
	public void idoSaveMyBillingInformation() throws Exception {
		if (null == checkoutReviewPage) {
			checkoutReviewPage = new CheckoutReviewPage();
		}
		assertTrue(checkoutReviewPage.selectCheckboxSaveMyBillingInfor());
	}

	@Then("I should see set up security questions window")
	public void iShouldSeeSetUpSecurityQuestionsWindow() throws Exception {
		securityQuestionsWindow = new SecurityQuestionsWindow();
		assertTrue("---Failed to occur set up security questions window!---", securityQuestionsWindow.isSecurityQuestionsWindowOccur());
	}

	@When("I choose not set up now")
	public void iChooseNotSetUpNow() throws Exception {
		securityQuestionsWindow.clickOnNotNowButton();
	}

	@Then("I should see the shipping option is \"$mode\"")
	public void ishouldseeTheShippingOption(String mode) {
		assertTrue("The shipping option should be changed", checkoutShipPage.checkShippingOptions(mode));
	}

	@When("I choose to pay by: \"$payment\"")
	public void iChooseToPay(String payment) {
		checkoutBillingPage = new CheckoutBillingPage();
		checkoutBillingPage.changePayment(payment);
	}

	@Then("I should see payment method is \"$payment\"")
	public void ishouldSeePaymentMethod(String payment) {
		checkoutReviewPage = new CheckoutReviewPage();
		assertTrue("The payment method should be changed", checkoutReviewPage.checkPaymentMethod(payment));
	}

	@When("I search for stores from the store locator modal at checkout second step")
	public void iSearchForStores() {
		checkoutShipPage.clickChangeStores();
	}

	@Then("I should see the stores displayed")
	public void iShouldSeeStoresDisplayed() {
		assertTrue("I should see the stores displayed", checkoutShipPage.checkStoresSearchResult());
	}

	@When("I select store from the search result")
	public void iSelectStoreFromSearchResult() {
		checkoutShipPage.clickSelectStore();
	}

	@Then("I should see the Pickup info changed at checkout second step")
	public void iShouldSeeThePickupInfoChanged() {
		assertTrue("I should see the Pickup info changed", checkoutShipPage.checkStoreAddressChanged());
	}

	@Then("I should see the shipping option is pickup in mixed mode")
	public void iShouldSeeShippingOptionPickup() {
		checkoutShipPage = new CheckoutShipPage();
		assertTrue("I should see the Pickup info changed", checkoutShipPage.checkChangePickupMode());
	}

	@Then("I should view pop the store locator modal")
	public void iShouldViewPopStoreLocatorModal() {
		checkoutShipPage = new CheckoutShipPage();
		assertTrue("I should view pop the store locator modal", checkoutShipPage.checkPopStoreLocatorModal());
	}

	@When("I choose a store on the Product Detail page")
	public void iChooseStoreOnProductDetailPage() {
		checkoutShipPage = new CheckoutShipPage();
		findYourProductPage = checkoutShipPage.selectStoreAndAddToCartButton();
	}

	@Then("Fax field is present")
	public void faxFieldPresent() {
		shippingListDisplayPage = new ShippingListDisplayPage();
		assertTrue("Fax field is present", shippingListDisplayPage.faxFieldPresent());
	}

	@Then("I should see find your product page")
	public void iShouldSeeFindYourproductpage() {
		findYourProductPage = new FindYourProductPage();
		assertTrue("I should see find your product page", findYourProductPage.isNavigateOnThisPage());
	}

	@When("I open cvv tool tip")
	public void iOpenCvvToolTip() {
		checkoutBillingPage = new CheckoutBillingPage();
		checkoutBillingPage.clickCvvToolTip();
	}

	@Then("I should see cvv tool tip display amex and visa images")
	public void shouldSeeCvvToolTipBoth() {
		assertTrue(checkoutBillingPage.diplayAmexImage());
		assertTrue(checkoutBillingPage.diplayVisaImage());
	}

	@Then("I should see cvv tool tip display visa image")
	public void shouldSeeCvvToolTipVisaImage() {
		assertFalse(checkoutBillingPage.diplayAmexImage());
		assertTrue(checkoutBillingPage.diplayVisaImage());
	}

	@Then("I should see cvv tool tip display amex image")
	public void shouldSeeCvvToolTipaMexImage() {
		assertTrue(checkoutBillingPage.diplayAmexImage());
		assertFalse(checkoutBillingPage.diplayVisaImage());
	}

	@When("I close the cvv tool tip")
	public void closeCvvToolTip() {
		checkoutBillingPage.clickOnCloseCvvToolTip();
	}

	@When("I choose to pay by credit card in amex")
	public void inputCreditCard() throws Exception {
		checkoutBillingPage.InputCreditCard("378282246310005");
	}

	@When("I choose to pay by OD card")
	public void inputODCard() throws Exception {
		checkoutBillingPage.inputODCard("6011568404413502");
		checkoutBillingPage.saveCreditCartInformation();
	}

	@Then("I should see order summary at checkout fourth step")
	public void seeOrderSummaryAtFourthStep() {
		orderSummaryTotalPrice = checkoutReviewPage.orderSummaryPrice();
		assertTrue(!orderSummaryTotalPrice.isEmpty());
	}

	@Then("I should see order summary the same as checkout fourth step page")
	public void seeOrderSummaryTheSame() {
		assertEquals(orderSummaryTotalPrice, checkoutThankYouPage.orderSummaryPrice());
	}

	@Then("I should see the payment information is blank at checkout third step")
	public void seePaymentInfoBlank() {
		assertTrue(checkoutBillingPage.isCreditCardBlank());
	}

	@When("I change someone to pick up the order")
	public void changeSomeonePickUpOrder() {
		checkoutShipPage = new CheckoutShipPage();
		checkoutShipPage.changeToPickUpOrder();
	}

	@Then("I should see who will pick up the order")
	public void seeSomeonePickupOrder() {
		assertTrue(checkoutShipPage.pickupWho());
	}

	@Then("I should see who will pick up the order at the thank page")
	public void seeSomeonePickupOrder2() {
		assertTrue(checkoutThankYouPage.pickupWho());
	}

	@When("I change payment at checkout third step")
	public void changePaymentAtThirdStep() {
		checkoutBillingPage.changePaymentAtThirdStep();
	}

	@When("I enter a new card at checkout third step")
	public void enterNewCardAtThirdStep() {
		checkoutBillingPage.enterNewCardAtThirdStep();
	}

	@Then("I should see blank card input field")
	public void seeBlankCardInputField() {
		assertTrue(checkoutBillingPage.seeBlankCardInputField());
	}
}
