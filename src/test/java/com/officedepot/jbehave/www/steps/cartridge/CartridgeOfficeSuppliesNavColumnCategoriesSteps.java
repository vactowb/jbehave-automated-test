package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.CashBoxesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.TapeAndAdhesivesPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeOfficeSuppliesNavColumnCategoriesSteps extends BaseStep {

	private BasicSuppliesCategoriesPage basicSuppliesCategoriesPage;
	private CashBoxesCategoriesPage cashBoxesCategoriesPage;
	private TapeAndAdhesivesPage tapeAndAdhesivesPage;

	@When("I navigate to first categories page from nav column categories")
	public void iNavigateToFirstCategoriesPageFromNavColumnCategories() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		cashBoxesCategoriesPage = basicSuppliesCategoriesPage.navigateToTheFirstFeaturedCategoriesPage();
	}

	@When("I navigate to tape and adhesives page frome office supplies nav categories link")
	public void iNavigateToTapeAndAdhesivesPageFromNavColumnCategories() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		tapeAndAdhesivesPage = basicSuppliesCategoriesPage.navigateToTheTapeAndAdhesivesPage();
	}

	@Then("I should see the first categories page from nav column categories")
	public void iShouldSeeTheFirstCategoriesPageFromNavColumnCategories() throws Exception {
		Assert.assertTrue("---Failed navigate to the First Categories Page!---", cashBoxesCategoriesPage.isInCurrentPage());
	}

}
