package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.ProductDetailsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeOfficeSuppliesRrzoneSteps extends BaseStep {

	private BasicSuppliesCategoriesPage basicSuppliesCategoriesPage;
	private ProductDetailsPage productDetailsPage;
	private String productName;

	@When("I navigate to product details page from RRZone cartridge")
	public void iNavigateToProductDetailsPageFromRRZoneCartridge() throws Exception {
		basicSuppliesCategoriesPage = new BasicSuppliesCategoriesPage();
		productName = basicSuppliesCategoriesPage.getTheFirstRRZoneProductName().substring(0, 10);
		productDetailsPage = basicSuppliesCategoriesPage.navigateToTheFirstRRZoneProductDetailsPage();
	}

	@Then("I should see the product details page from RRZone cartridge")
	public void iShouldSeeTheProductDetailsPageFromRRZoneCartridge() throws Exception {
		Assert.assertTrue("---Failed navigate to the product details page!---", productDetailsPage.isInCurrentPage(productName));
	}

}
