package com.officedepot.jbehave.www.steps.topnavigation;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.BreakroomPage;
import com.officedepot.jbehave.www.pages.cartridge.CleaningPage;
import com.officedepot.jbehave.www.pages.cartridge.FurniturePage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.deals.OfficeSuppliesPage;
import com.officedepot.jbehave.www.pages.shopping.list.SchoolSuppliesPage;
import com.officedepot.jbehave.www.pages.supplies.ArtAndCraftPaperPage;
import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.jbehave.www.pages.supplies.InkDepotShowingResultsPage;
import com.officedepot.jbehave.www.pages.supplies.PaperPage;
import com.officedepot.jbehave.www.pages.supplies.TechnologyPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class NewTopNavigationSteps extends BaseStep {

	private TopNavigationPage topNavigationPage;
	private OfficeSuppliesPage officeSuppliesPage;
	private InkAndTonerPage inkAndTonerPage;
	private PaperPage paperPage;
	private TechnologyPage technologyPage;
	private InkDepotShowingResultsPage inkDepotShowingResultsPage;
	private ArtAndCraftPaperPage artAndCraftPaperPage;
	private SchoolSuppliesPage schoolSuppliesPage;
	private DealCenterPage dealCenterPage;
	private BreakroomPage breakroomPage;
	private CleaningPage cleaningPage;
	private FurniturePage furniturePage;

	@When("I navigate to office supplies categories page from office supplies top navigation")
	public void iNavigateToOfficeSuppliesCategoriesPageFromOfficeSuppliesTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		officeSuppliesPage = topNavigationPage.clickOnOfficeSuppliesNavigation();
	}

	@Then("I should see office supplies categories page")
	public void iShouldSeeOfficeSuppliesCategoriesPage() throws Exception {
		officeSuppliesPage = new OfficeSuppliesPage();
		Assert.assertTrue("---Failed navigate to office supplies categories page!---", officeSuppliesPage.isNavigateOnThisPage());
	}

	@When("I navigate to paper categories page from paper top navigation")
	public void iNavigateToPaperCategoriesPageFromPaperTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		paperPage = topNavigationPage.clickOnPaperNavigation();
	}

	@Then("I should see paper categories page")
	public void iShouldSeePaperCategoriesPage() throws Exception {
		Assert.assertTrue("---Failed navigate to paper categories page!---", paperPage.isNavigateOnThisPage());
	}

	@When("I navigate to deal center page from deals top navigation")
	public void iNavigateToDealCenterPageFromDealsTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		dealCenterPage = topNavigationPage.clickOnDealsTopNavigation();
	}

	@Then("I should see ink and toner categories page")
	public void iShouldSeeInkAndTonerCategoriesPage() throws Exception {
		Assert.assertTrue("---Failed navigate to ink and toner categories page!---", inkAndTonerPage.isNavigateOnThisPage());
	}

	@When("I navigate to technology categories page from technology top navigation")
	public void iNavigateToTechnologyCategoriesPageFromTechnologyTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		technologyPage = topNavigationPage.clickOnTechnologyNavigation();
	}

	@Then("I should see technology categories page")
	public void iShouldSeeTechnologyPage() throws Exception {
		Assert.assertTrue("---Failed navigate to technology page!---", technologyPage.isNavigateOnThisPage());
	}

	@Then("I should see ink and toner search result page")
	public void iShouldSeeInkAndTonerSearchResultPage() throws Exception {
		inkDepotShowingResultsPage = new InkDepotShowingResultsPage();
		Assert.assertTrue("---Failed navigate to search result page!---", inkDepotShowingResultsPage.isInCurrentPage());
	}

	@When("I navigate to top categories papers page from top categories link")
	public void iNavigateToTopCategoriesPapersPageFromTopCategoriesLink() throws Exception {
		topNavigationPage.viewPaperNavigatorOnTopNavigation();
		artAndCraftPaperPage = topNavigationPage.clickOnTopCategoriesPapersIcon();
	}

	@Then("I should see top categories papers page")
	public void iShouldSeeTopCategoriesPapersPage() throws Exception {
		Assert.assertTrue("---Failed navigate to art and craft paper page!---", artAndCraftPaperPage.isInCurrentPage());
	}

	@When("I navigate to school supplies page from school top navigation")
	public void iNavigateToSchoolSuppliesPageFromSchoolTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		schoolSuppliesPage = topNavigationPage.clickOnSchoolSuppliesNavigation();
	}

	@Then("I should see school supplies page")
	public void iShouldSeeSchoolSuppliesPage() throws Exception {
		Assert.assertTrue("---Failed navigate to school supplies page!---", schoolSuppliesPage.isNavigateOnThisPage());
	}

	@When("I navigate to school grades page from shop by grade link")
	public void iNavigateToSchoolGradesPageFromShopByGradeLink() throws Exception {
		topNavigationPage.viewPaperNavigatorOnTopNavigation();
		artAndCraftPaperPage = topNavigationPage.clickOnTopCategoriesPapersIcon();
	}

	@Then("I should see school grades page")
	public void iShouldSeeSchoolGradesPage() throws Exception {

	}

	@When("I chose one brand and go to sku details page")
	public void choseOneBrandGoSkuDetailPage() {
		inkAndTonerPage.choseOneBrandGoSkuDetailPage();
	}

	@Then("I should see sku detail page for the selected brand")
	public void shouldSeeSkuDetailPageForBrand() {
		Assert.assertTrue(inkAndTonerPage.isSeeSkuDetailPageForProductDetails().contains("848598"));
	}

	@Then("I should see sku detail page for the search product detail")
	public void shouldSeeSkuDetailPageForSearchProduct() {
		Assert.assertTrue(inkAndTonerPage.isSeeSkuDetailPageForProductDetails().contains("315515"));
	}

	@When("I navigate to ink and toner categories page from ink and toner top navigation")
	public void iNavigateToInkAndTonerCategoriesPageFromInkAndTonerTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		inkAndTonerPage = topNavigationPage.clickOnInkAndTonerNavigation();
	}

    @When("I navigate to break room page from breakroom top navigation")
    public void iNavigateToBreakRoomPageFromBreakroomTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		breakroomPage = topNavigationPage.clickOnBreakroomNavigation();
    }

    @Then("I should see the break room page")
    public void iShouldSeeTheBreakRoomPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to break room page!---", breakroomPage.isInCurrentPage());
    }
    
    @When("I navigate to cleaning page from cleaning top navigation")
    public void iNavigateToCleaningPageFromCleaningTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		cleaningPage = topNavigationPage.clickOnCleaningNavigation();
    }

    @Then("I should see the cleaning page")
    public void iShouldSeeTheCleaningPagePage() throws Exception {
    	Assert.assertTrue("---Failed navigate to cleaning page!---", cleaningPage.isInCurrentPage());
    }
    
    @When("I navigate to furniture page from furniture top navigation")
    public void iNavigateToFurniturePageFromFurnitureTopNavigation() throws Exception {
		topNavigationPage = new TopNavigationPage();
		furniturePage = topNavigationPage.clickOnFurnitureNavigation();
    }

    @Then("I should see the furniture page")
    public void iShouldSeeTheFurniturePage() throws Exception {
    	Assert.assertTrue("---Failed navigate to furniture page!---", furniturePage.isInCurrentPage());
    }
    
}
