package com.officedepot.jbehave.www.steps.topnavigation;

import junit.framework.Assert;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.jbehave.www.pages.supplies.InkDepotShowingResultsPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class TopnavigationInkAndTonerFlyoutSteps extends BaseStep {
	
	private TopNavigationPage topNavigationPage;
	private SkuDetailPage skuDetailPage;
	private InkDepotShowingResultsPage inkDepotShowingResultsPage;
	private HomePage homePage;
	
	
    @Given("I set new ink and toner flyout cookie  \"$newCookie\"  on home page")
    public void iSetNewInkAndTonerFlyoutCookieOnHomePage(String newCookie) throws Exception {
		homePage = new HomePage();
		homePage.homePageSetCookie(newCookie);
    }

    
    @When("I search \"$cartridgeNumber\" by search cartridge number from ink and toner flyout")
    public void iSearchBySearchCartridgeNumberFromInkAndTonerFlyout(String cartridgeNumber) throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewInkAndTonerFlyoutOnTopNavigation();
		skuDetailPage = topNavigationPage.searchByCartridgeNumber(cartridgeNumber);
    }

    @Then("I should see the \"$cartridgeNumber\" cartridge details page")
    public void iShouldSeeTheCartridgeDetailsPage(String cartridgeNumber) throws Exception {
    	Assert.assertTrue("---Failed navigate to categorie details page!---", skuDetailPage.isInCurrentPage());
    }

    @When("I search \"$printerModel\" by search printer model from ink and toner flyout")
    public void iSearchBySearchPrinterModelFromInkAndTonerFlyout(String printerModel) throws Exception {
//		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewInkAndTonerFlyoutOnTopNavigation();
		inkDepotShowingResultsPage = topNavigationPage.searchByPrinterModel(printerModel);
    }

}
