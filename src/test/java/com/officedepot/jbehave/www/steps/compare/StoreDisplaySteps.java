package com.officedepot.jbehave.www.steps.compare;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.jbehave.www.pages.storelocator.ChooseStorePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class StoreDisplaySteps extends BaseStep {

	private HomePage homePage;
	private ChooseStorePage storePage;
	private SkuDetailPage skuDetailPage;
	
    @When("I click on stores icon")
    public void iClickOnStoresIcon() throws Exception {
    	storePage = new ChooseStorePage();
    	storePage.goToChooseStorePage();
    	Assert.assertTrue("----- Failed to navigate on the excepted sku details! ------", storePage.isNavigateOnThisPage());
    }
	
    @When("I search for a product \"$num\"")
    public void iSearchForAProduct(String product) throws Exception {
    	homePage = new HomePage();
		homePage.searchBy(product);
    }

    @Then("I should see the sku page loaded for the product")
    public void iShouldSeeTheSkuPageLoadedForTheProduct(@Named("product3") String product) throws Exception {
    	skuDetailPage = new SkuDetailPage();
		Assert.assertTrue("----- Failed to navigate on the excepted sku details! ------", skuDetailPage.isNavigateToExceptedSkuDetails(product));
		skuDetailPage.containsStoresDetails("Sold in Stores");
		Assert.assertTrue("----- Failed to find Check Your Store Availability link on the sku details page! ------", skuDetailPage.isLinkPresent("Check Your Store Availablity"));
    }
    
    @Then("I should see the sku page loaded for the available product")
    public void iShouldSeeTheSkuPageLoadedForTheAvailableProduct(@Named("product1") String product) throws Exception {
    	skuDetailPage = new SkuDetailPage();
		Assert.assertTrue("----- Failed to navigate on the excepted sku details! ------", skuDetailPage.isNavigateToExceptedSkuDetails(product));
		Assert.assertTrue("----- Failed to find In Stock keyword on the sku details page! ------", skuDetailPage.isInStock());
		skuDetailPage.containsStoresDetails("at");
		Assert.assertTrue("----- Failed to find Your Selected Store link on the sku details page! ------", skuDetailPage.isLinkPresent("Your Selected Store"));
    }

    @Then("I should see the sku page loaded for the unavailable product")
    public void iShouldSeeTheSkuPageLoadedForTheUnavailableProduct(@Named("product2") String product) throws Exception {
    	skuDetailPage = new SkuDetailPage();
		Assert.assertTrue("----- Failed to navigate on the excepted sku details! ------", skuDetailPage.isNavigateToExceptedSkuDetails(product));
		skuDetailPage.containsStoresDetails("Sold in Stores");
		Assert.assertTrue("----- Failed to find Not in Stock keyword on the sku details page! ------", skuDetailPage.isNotInStock());
		skuDetailPage.containsStoresDetails("at");
		Assert.assertTrue("----- Failed to find Your Selected Store link on the sku details page! ------", skuDetailPage.isLinkPresent("Your Selected Store"));
    }

}
