package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.OrderTrackingWithoutLoginPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.register.ExpressRegisterPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HeaderAccountWithoutLoginSteps extends BaseStep {

	private PageHeader pageHeader = new PageHeader();
	private HomePage homePage = new HomePage();
	// private CustomerRegistrationPage customerRegistrationPage;
	private ExpressRegisterPage customerRegistrationPage;
	private OrderTrackingWithoutLoginPage orderTrackingWithoutLoginPage;

	@When("I request to view without welcome information from header account")
	public void iRequestToViewWithoutWelcomeInformationFromHeaderAccount() throws Exception {
		pageHeader.viewNavigatorOnHeader("Account");
	}

	@Then("I should see without welcome information in header account")
	public void iShouldSeeWithoutWelcomeInformationInHeaderAccount() throws Exception {
		String getWithoutLoginWelcomeInformation = pageHeader.viewWithoutLoginWelcomeInformation();
		assertEquals("Login or Register", getWithoutLoginWelcomeInformation);
	}

	@When("I open account login window from header account")
	public void iOpenAccountLoginWindowFromHeaderAccount() throws Exception {
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnLogInNowLink();
	}

	@Then("I should see account login window")
	public void iShouldSeeAccountLoginWindow() throws Exception {
		assertTrue("--- Failed to navigate on account login window! ---", pageHeader.isLoginWindowOccur());
		homePage.openHomePage();
	}

	@When("I navigate to create an account page from header account")
	public void inavigateToCreateAnAccountPageFromHeaderAccount() throws Exception {
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnCreateAnAccountLink();
	}

	@Then("I should see create an account page")
	public void iShouldSeeCreateAnAccountPage() throws Exception {
		// customerRegistrationPage = new CustomerRegistrationPage();
		customerRegistrationPage = new ExpressRegisterPage();
		assertTrue("--- Failed to navigate on create an account page! ---", customerRegistrationPage.isNavigateOnThisPage());

	}

	@When("I navigate to order tracking page from header account")
	public void inavigateToOrderTrackingPageFromHeaderAccount() throws Exception {
		pageHeader.viewNavigatorOnHeader("Account");
		orderTrackingWithoutLoginPage = pageHeader.clickOnOrderTrackingLink();
	}

	@Then("I should see order tracking page")
	public void iShouldSeeOrderTrackingPage() throws Exception {
		orderTrackingWithoutLoginPage = new OrderTrackingWithoutLoginPage();
		assertTrue("--- Failed to navigate on order tracking page! ---", orderTrackingWithoutLoginPage.isInCurrentPage());
	}

}
