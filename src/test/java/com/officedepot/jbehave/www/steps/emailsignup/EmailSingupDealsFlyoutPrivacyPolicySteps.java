package com.officedepot.jbehave.www.steps.emailsignup;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.emailsignup.PrivacyStatementPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class EmailSingupDealsFlyoutPrivacyPolicySteps extends BaseStep {
	
	private TopNavigationPage topNavigationPage;
	private PrivacyStatementPage privacyStatementPage;
	
    @When("I navigate to privacy statement page from deals flyout")
    public void iNavigateToPrivacyStatementPageFromDealsFlyout() throws Exception {
		topNavigationPage = new TopNavigationPage();
		topNavigationPage.viewDealsNavigatorOnTopNavigation();
		privacyStatementPage = topNavigationPage.clickOnPrivacyPolicyLink();
    }

    @Then("I should see the privacy statement page")
    public void iShouldSeeThePrivacyStatementPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to the privacy statement page!---", privacyStatementPage.isNavigateOnThisPage());
    }

}
