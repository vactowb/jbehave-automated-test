package com.officedepot.jbehave.www.steps.shopping;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.pages.storelocator.ChooseStorePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HeaderCartWithLoginSteps extends BaseStep {
	private PageHeader pageHeader = new PageHeader();
	private ShoppingCartPage shoppingCartPage = new ShoppingCartPage();
	private ChooseStorePage chooseStorePage = new ChooseStorePage();
	
    @When("I request to view items' list from header cart")
    public void iRequestToViewItemsListFromHeaderCart() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    }

    @Then("I should see items' list in header cart")
    public void iShouldSeeItemsListInHeaderCart() throws Exception {
    	Assert.assertTrue("---You have see no items list in your cart!---", pageHeader.isItemsListOccur());
    }

    @When("I navigate to choose a store page from header cart")
    public void iNavigateToChooseAStorePageFromHeaderCart() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    	pageHeader.clickOnPickUpButton();
    }

    @When("I navigate to choose a store page from header cart with login")
    public void iNavigateToChooseAStorePageFromHeaderCartWithLogin() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    	pageHeader.clickOnPickUpButton();
    	pageHeader.viewNavigatorOnHeader("Cart");
    	pageHeader.clickOnPickUpButton();
    }
   

    @Then("I should see choose a store page")
    public void iShouldSeeChooseAStorePageInHeaderCart() throws Exception {
    	Assert.assertTrue("---You failed navigate to choose a store page!---", chooseStorePage.isNavigateOnThisPage());
    }
    
    @When("I navigate to shopping cart page from delivery box")
    public void iNavigateToShoppingCartPageFromDeliveryBox() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    	pageHeader.clickOnDeliveryButton();
    }
    

    @When("I navigate to shopping cart page from header cart flyout view all link")
    public void iNavigateToShoppingCartPageFromCartFlyoutViewAllLink() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    	pageHeader.clickOnViewAllLink();
    }
    
    @When("I navigate to shopping cart page from header cart")
    public void iNavigateToShoppingCartPageFromHeaderCart() throws Exception {
    	pageHeader = new PageHeader();
    	shoppingCartPage = pageHeader.clickOnHeaderCart();
    }

    @Then("I should see shopping cart page")
    public void iShouldSeeShoppingCartPageInHeaderCart() throws Exception {
    	Assert.assertTrue("---Failed to navigate to shopping cart page!---", shoppingCartPage.isNavigateOnThisPage());
    }

    @When("I navigate to view cart and checkout page from header cart")
    public void iNavigateToViewCartAndCheckoutPageFromHeaderCart() throws Exception {
    	pageHeader.viewNavigatorOnHeader("Cart");
    	pageHeader.clickOnViewCartAndCheckoutLink();
    }

    @Then("I should see view cart and checkout page in header cart")
    public void iShouldSeeViewCartAndCheckoutPageInHeaderCart() throws Exception {
    	Assert.assertTrue("---Failed to navigate to view cart and checkout page!---", shoppingCartPage.isNavigateOnThisPage());
    }

}
