package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.CamerasAndCamcordersCategoryLandingPage;
import com.officedepot.jbehave.www.pages.supplies.TechnologyPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeTechnologyCategoryLandingPageSteps extends BaseStep {

	private TechnologyPage technologyPage;
	private CamerasAndCamcordersCategoryLandingPage camerasAndCamcordersCategoryLandingPage;
	
    @When("I go to \"$category\" category landing page from Technology categories")
    public void iGoToCategoryLandingPageFromTechnologyCategories(String category) throws Exception {
    	technologyPage = new TechnologyPage();
    	camerasAndCamcordersCategoryLandingPage = technologyPage.navigateToCamerasAndCamcordersCategoryLandingPage(category);
    }

    @Then("I should see the \"$category\" category landing page")
    public void iShouldSeeTheCategoryLandingPage(String category) throws Exception {
    	Assert.assertTrue("---Failed navigate to the cameras and camcorders category landingPage! ---", camerasAndCamcordersCategoryLandingPage.isNavigateOnThisPage());
    }

}
