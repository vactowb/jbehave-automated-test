package com.officedepot.jbehave.www.steps.sku;

import static junit.framework.Assert.assertTrue;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.jbehave.www.pages.sku.CustomizePage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SkuSteps extends BaseStep {

	private SkuDetailPage skuDetailPage;
	private FindYourProductPage findYourProductPage;
	private CustomizePage customizePage;

	@Then("I should see sku detail page for the given SKU")
	public void iSeeSKUDetailPage(@Named("skuid") String skuId) throws Exception {
		skuDetailPage = new SkuDetailPage();
		Assert.assertTrue("----- Failed to navigate on the excepted sku details! ------", skuDetailPage.isNavigateToExceptedSkuDetails(skuId));
	}

	@When("I add the sku to my list")
	public void iAddTheSKUToList() throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.clickAddToList();
	}

	@When("I request to add the sku to cart")
	public void addToCart() throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.addToCart();
	}

	@Then("I should see the OMax and OD stores that have the In Store Pick-Up flag")
	public void isShownOMaxODStores() {
		assertTrue(skuDetailPage.isShownOMaxODStores());
	}

	@Then("I should see store brand icon displayed for store pick-up")
	public void isShownOMaxODStoresBrand() {
		skuDetailPage = new SkuDetailPage();
		assertTrue(skuDetailPage.isShownOMaxODStoresBrand());
	}

	@Then("I should see the stores inventory is not available online")
	public void isInventoryAvailable() {
		assertTrue(skuDetailPage.isInventoryAvailable());
	}

	@When("I add the sku to cart from sku details page")
	public void addToCartAndCheckout() throws Exception {
		initSkuDetailPage();
		skuDetailPage.addSkuToCartAndViewCart();
	}

	@When("I request to view the shopping cart")
	public void iRequestToViewTheShoppingCart() throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.viewShoppingCart();
	}

	@When("I request to view the shopping cart from find your product page")
	public void iRequestToViewTheShoppingCartFromYourProductPage() throws Exception {
		if (findYourProductPage == null) {
			findYourProductPage = new FindYourProductPage();
		}
		findYourProductPage.viewShoppingCart();
	}

	@When("I add the sku to cart from sku details page and continue shopping")
	public void addToCartAndContinueShopping() throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.addToCartAndContinueShopping();
	}

	@When("I add several skus into cart")
	public void iAaddSeveralSkusIntoCart() throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.addSkusToCartWithQtyAndViewCart("1");
	}

	@When("I add the sku to cart with quantity : \"$qty\"")
	public void iAddSkuToCartWithQty(String qty) throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.addSkusToCartWithQtyAndViewCart(qty);
	}

	@When("I request to add the sku to cart with quantity : \"$qty\"")
	public void iTryToAddSkuToCartWithQty(String qty) throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.addSkusToCartWithQty(qty);
	}

	@Then("I should see the out of stock message")
	public void iSeeOutOfStockMessage() throws Exception {
		findYourProductPage = new FindYourProductPage();
		assertTrue("------ Failed to navigate on FindYourProductPage! -----", findYourProductPage.isNavigateOnThisPage());
		assertTrue(findYourProductPage.isOutOfStockMessageShowUp());
	}

	@When("I continually add the out of stock sku to cart")
	public void iContinuallyAddOutStockSkuToCart() {
		findYourProductPage.clickAddToCart();
		findYourProductPage.clickNoThanks();
	}

	@When("I customize the cpd sku")
	public void customizeCpdSku() {
		initSkuDetailPage();
		skuDetailPage.customizeCpdSku();
	}

	@When("I select one the bcc sku item")
	public void selectOneBccSku() {
		skuDetailPage.selectOneBccSku();
	}

	@Then("I should see the \"$flag\" price and price greater than zero")
	public void isPriceGreaterZero(String flag) {
		assertTrue(skuDetailPage.isPriceGreaterZero(flag));
	}

	@When("I customize sku in Product Details page")
	public void iCustomizeSkuInProductDetailsPage() {
		initSkuDetailPage();
		customizePage = skuDetailPage.customizeProduct();
	}

	@Then("I should see upload contain view")
	public void isUploadContainViewShown() {
		assertTrue(customizePage.isUploadContainViewShown());
	}

	@When("I cancel upload a file")
	public void cancelUploadFile() {
		customizePage.cancelUploadFile();
	}

	@When("I view sample file")
	public void viewSampleFile() {
		customizePage.viewSampleFile();
	}

	@Then("I should see Lbl price in view sample file page and two prices match")
	public void iShouldSeeLblPriceInViewSampleFilePage() {
		assertTrue("-------- Lbl Price doesn't show up --------", customizePage.iShouldSeeLblPriceInViewSampleFilePage());
	}

	private void initSkuDetailPage() {
		skuDetailPage = new SkuDetailPage();
	}
	
	@Then("I should see price in \"$vendorName\" configurator and compare it to the sku price")
	public void iShouldSeepriceInVendorConfigurator(String vendorName) {
		assertTrue("-------- " + vendorName + " Price doesn't show up --------", customizePage.isComparePriceShownInConfiguratorForVendor(vendorName));
	}
}
