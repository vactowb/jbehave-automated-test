package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.CategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.NewTemplatePage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class AdlistNewTemplatePageSteps extends BaseStep {

	private HomePage homePage;
	private NewTemplatePage newTemplatePage;
	private SkuDetailPage skuDetailPage;
	private CategoriesPage categoriesPage;
	
    @When("I navigate to new template page from URL")
    public void iNavigateToNewTemplatePageFromURL() throws Exception {
    	homePage = new HomePage();
    	newTemplatePage = homePage.navigateToNewTemplatePageFromUrl();
    }

    @Then("I should see new template page")
    public void iShouldSeeNewTemplatePage() throws Exception {
       	Assert.assertTrue("---Failed to see new template page", newTemplatePage.isNavigateOnThisPage());
    }

    @When("I navigate to sku details page from shop me button")
    public void iNavigateToSkuDetailsPageFromShopMeButton() throws Exception {
    	categoriesPage = newTemplatePage.clickOnShopMeButton();
    }

    @Then("I should see sku details page")
    public void iShouldSeeSkuDetailsPage() throws Exception {
    	Assert.assertTrue("---Failed to see sku details page", skuDetailPage.isInCurrentPage());
    }
    
    @Then("I should see the cotegories page")
    public void iShouldSeeTheCotegoriesPage() throws Exception {
    	Assert.assertTrue("---Failed to see the cotegories page", categoriesPage.isNavigateOnThisPage());
    }


}
