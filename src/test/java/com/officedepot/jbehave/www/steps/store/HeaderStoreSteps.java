package com.officedepot.jbehave.www.steps.store;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.deals.WeeklyAdPage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.storelocator.DrivingDirectionsPage;
import com.officedepot.jbehave.www.pages.storelocator.FindAStorePage;
import com.officedepot.jbehave.www.pages.storelocator.LocalStorePage;
import com.officedepot.jbehave.www.pages.storelocator.OneHourStorePage;
import com.officedepot.jbehave.www.pages.storelocator.StoreDetailsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HeaderStoreSteps extends BaseStep {

	private PageHeader pageHeader;
	private FindAStorePage findAStorePage;
	private DrivingDirectionsPage drivingDirectionsPage;
	private StoreDetailsPage storeDetailsPage;
	private LocalStorePage localStorePage;
	private String storeNumber;
	private String parentWindow;
	private WeeklyAdPage weeklyAdPage;
	private OneHourStorePage oneHourStorePage;

	@When("I open stores flyout from header stores")
	public void iOpenStoresFlyoutFromHeaderStores() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
	}

	@Then("I should see store pick-up not available text")
	public void iShouldSeeStorePickUpNotAvailableText() throws Exception {
		Assert.assertTrue("--- Failed to see store pick-up text! ---", pageHeader.isStorePickUpTextOccur());
	}
	
	@Then("I should see the \"$myStoreText\" text")
	public void iShouldSeeMyStoreText(String myStoreText) throws Exception {
		Assert.assertTrue("--- Failed to see my Store text! ---", pageHeader.isMyStoreTextOccur(myStoreText));
	}

	@When("I navigate to the local store page from my store")
	public void iNavigateToTheLocalStorePageFromMyStore() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		localStorePage = pageHeader.clickOnMyStoreLink();
	}

	@When("I navigate to find a store page from change my store link")
	public void iNavigateToFindAStorePageFromChangeStoreLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		findAStorePage = pageHeader.clickOnChangeStoreLink();
	}

	@When("I navigate to find a store page from header stores")
	public void iNavigateToFindAStorePageFromHeaderStore() throws Exception {
		pageHeader = new PageHeader();
		findAStorePage = pageHeader.clickOnHeaderStores();
	}

	@When("I navigate to store details page from store details link")
	public void iNavigateToStoreDetailsPageFromStoreDetailsLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		storeDetailsPage = pageHeader.clickOnStoreDetailsLink();
	}

	@When("I navigate to store details page from map it link")
	public void iNavigateToStoreDetailsPageFromMapItLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		storeDetailsPage = pageHeader.clickOnMapItLink();
	}

	@When("I navigate to store's page from the header store name link")
	public void iNavigateToStorePageFromTheHeaderStoreNameLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		storeNumber = pageHeader.getMyStoreNumberFromHeader();
		localStorePage = pageHeader.navigateToStorePageFromHeaderStoreNameLink();
	}

	@Then("I should navigate on local store page")
	public void iShouldSeeLocalStoresPage() throws Exception {
		localStorePage = new LocalStorePage();
		Assert.assertTrue("--- Failed to navigate on store's page! ---", localStorePage.isNavigateOnThisPage());
	}

	@When("I navigate to find a store page from find a store input box")
	public void iNavigateToFindAPageFromFindAStoreInputBox() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		findAStorePage = pageHeader.navigateToFindAPageFromFindAStoreInputBox();
	}

	@When("I navigate to find a store page from more search options link")
	public void iNavigateToFindAPageFromMoreSearchOptionLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		findAStorePage = pageHeader.navigateToFindAStorePageFromMoreSearchOptionLink();
	}

	@When("I navigate to one hour store page from one hour store pickup link")
	public void iNavigateToOneHourStorePageFromOneHourStorePickupLink() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		oneHourStorePage = pageHeader.navigateToOneHourStorePageFromOneHourStorePickupLink();
	}

	@Then("I should see one hour store page")
	public void iShouldSeeOneHourStorePage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on one hour store page! ---", oneHourStorePage.isInCurrentPage());
	}

	@When("I go to shop weekly ad from strore weekly ad link")
	public void iGoToShopWeeklyAdFromDealsView() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		parentWindow = pageHeader.getCurrentWindowHandle();
		weeklyAdPage = pageHeader.goToWeeklyAdFromStoreWeeklyAdLink();
	}

	@Then("I should navigate to weekly ad page from store")
	public void iShouldNavigateToWeeklyAdPage() throws Exception {
		weeklyAdPage = new WeeklyAdPage();
		Assert.assertTrue("--- Failed to navigate on weekly ad page! ---", weeklyAdPage.isNavigateOnThisPage());
		weeklyAdPage.goBackToPreWindow(parentWindow);
	}

	@When("I search stores by zip \"$zip\"")
	public void iSearchStoresByZip(String zip) throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		findAStorePage = pageHeader.serachStoreByZip(zip);
	}

	@Then("I should see find a store page")
	public void iShouldSeeFindAStorePage() throws Exception {
		findAStorePage = new FindAStorePage();
		Assert.assertTrue("--- Failed to navigate on find a store page! ---", findAStorePage.isInCurrentPage());
	}

	@When("I navigate store locator from header stores")
	public void openStoreLocator() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		pageHeader.viewStoreLocator();
	}

	@When("I choose the first store")
	public void iChooseTheFirstStore() throws Exception {
		findAStorePage.chooseThisStore();
	}

	@Then("I should see store information in header stores")
	public void iShouldSeeStoreInformationInHeaderStores() throws Exception {
		pageHeader.viewNavigatorOnHeader("Stores");
		Assert.assertTrue("--- Failed to choose a store! ---", pageHeader.isStoreInformationInHeaderStore());
	}

	@When("I navigate to driving directions page from header stores")
	public void iNavigateToDrivingDirectionsPageFromHeaderStores() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		drivingDirectionsPage = pageHeader.clickOnGetDrivingDirectionsLink();
	}

	@Then("I should see driving directions page")
	public void iShouldSeeDrivingDirectionsPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on driving directions page! ---", drivingDirectionsPage.isInCurrentPage());
	}

	@When("I navigate to store details page from header stores")
	public void iNavigateToStoreDetailsPageFromHeaderStores() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Stores");
		storeDetailsPage = pageHeader.clickOnSendAddressToMobileLink();
	}

	@Then("I should see store details page")
	public void iShouldSeeStoreDetailsPage() throws Exception {
		storeDetailsPage = new StoreDetailsPage();
		Assert.assertTrue("--- Failed to navigate on store details page! ---", storeDetailsPage.isInCurrentPage());
	}

}
