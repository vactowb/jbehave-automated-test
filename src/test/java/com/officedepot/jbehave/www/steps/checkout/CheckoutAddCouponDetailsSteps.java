package com.officedepot.jbehave.www.steps.checkout;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CouponDetailsPage;
import com.officedepot.jbehave.www.pages.checkout.CouponDialogPage;
import com.officedepot.jbehave.www.pages.checkout.CouponExpiredPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckoutAddCouponDetailsSteps extends BaseStep {

	private CouponDialogPage couponDialogPage;
	private CouponDetailsPage couponDetailsPage;
	private ShoppingCartPage shoppingCartPage;

	@When("I navigate to coupon details page from see offer details button")
	public void iNavigateToCouponDetailsPageFromSeeOfferDetailsButton() throws Exception {
		couponDialogPage = new CouponDialogPage();
		couponDetailsPage = couponDialogPage.navigateToCouponDetailsPage();
	}

	@Then("I should see the coupon details page")
	public void iShouldSeeTheCouponDetailsPage() throws Exception {
		couponDetailsPage = new CouponDetailsPage();
		Assert.assertTrue("---Failed find  coupon details page", couponDetailsPage.isInCurrentPage());
	}

	@When("I click on add to cart button when I selected none")
	public void iClickOnAddToCartButtonWhenISelectedNone() throws Exception {
		shoppingCartPage = couponDetailsPage.clickOnBottomAddToCartButton();
	}

	@Then("I should see the error message let me to select item")
	public void iShouldSeeTheErrorMessageLetMeToSelectItem() throws Exception {
		Assert.assertTrue("---Failed find  see the error message", couponDetailsPage.isErrorMessageOccur());
	}

	@When("I select a item add to cart")
	public void iSelectAItemAddToCart() throws Exception {
		couponDetailsPage.selectTheFirstItem();
		shoppingCartPage = couponDetailsPage.clickOnBottomAddToCartButton();
	}

	@Then("I should see the item add to cart successful")
	public void iShouldSeeTheItemAddToCartSuccessful() throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		Assert.assertTrue("---Failed add item to cart", shoppingCartPage.isSkusAddedToCartSuccess());
	}

	@When("I select a item and add to cart by first add to cart button")
	public void iSelectAItemAndAddToCartByFirstAddToCartButton() throws Exception {
		couponDialogPage = new CouponDialogPage();
		couponDetailsPage.selectTheFirstItem();
		shoppingCartPage = couponDetailsPage.clickOnTheFirstAddToCartButton();
	}
	
    @When("I navigate to type3 and type4 coupon details page from url")
    public void iNavigateToType3AndType4CouponDetailsPageFromUrl() throws Exception {
    	couponDetailsPage = new CouponDetailsPage();
    	couponDetailsPage.openType3AndType4CouponDetailsPage();
    }

    @Then("I should see the type3 and type4 coupon details page")
    public void iShouldSeeTheType3AndType4CouponDetailsPage() throws Exception {
    	Assert.assertTrue("--- Failed to navigate on type3 and type4 coupon details page! ---", couponDetailsPage.isInCurrentPage());
    }
    
    @When("I add coupon details page product to shopping cart from add to cart button")
    public void iAddCouponDetailsPageProductToShoppingCartFromAddToCartButton() throws Exception {
    	couponDetailsPage = new CouponDetailsPage();
    	couponDetailsPage = couponDetailsPage.clickOnProductAddToCartButton();
    }

    @Then("I should stay on coupon details page")
    public void iShouldStayOnCouponDetailsPage() throws Exception {
    	Assert.assertTrue("--- Failed to stay on coupon details page! ---", couponDetailsPage.isInCurrentPage());
    }
    
    @When("I select a product")
    public void iSelectAProduct() throws Exception {
    	couponDetailsPage = new CouponDetailsPage();
    	couponDetailsPage.selectTheFirstItem();
    }

    @Then("I should see a message indicate the necessary number")
    public void iShouldMetAMessageIndicateTheNecessaryNumber() throws Exception {
    	Assert.assertTrue("--- Failed to see a message indicate the necessary number! ---", couponDetailsPage.isIndicateTheNecessaryNumberMessageAccor());
    }

    @When("I select enough products")
    public void iSelectEnoughProducts() throws Exception {
    	couponDetailsPage = couponDetailsPage.selectEnoughItems();
    }

    @Then("I should see the necessary number message disappear")
    public void iShouldSeeTheNecessaryNumberMessageDisappear() throws Exception {
    	Assert.assertTrue("--- Failed to disappear a message indicate the necessary number! ---", couponDetailsPage.isIndicateTheNecessaryNumberMessageDisappear());
    }
    
    @When("I add all products to cart")
    public void iAddAllProductsToCart() throws Exception {
    	couponDetailsPage.clickOnBottomAddToCartButton();
    }
    
}
