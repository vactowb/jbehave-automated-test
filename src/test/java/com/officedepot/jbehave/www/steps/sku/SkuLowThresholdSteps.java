package com.officedepot.jbehave.www.steps.sku;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.search.ProductComparisonPage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SkuLowThresholdSteps extends BaseStep{
	
	private HomePage homePage;
	private SkuDetailPage skuDetailPage;
	
	
	@Given("I access WWW site for testing")
		public void iAccessWWWSiteForTesting() throws Exception {
			homePage = new HomePage();
			homePage.openHomePage();
		}
	
	@When("I search for a specific sku with low stock : \"$sku\"")
    public void iSearchForSpecificSkuWithLowStock(String sku) throws Exception {
    	homePage = new HomePage();
    	skuDetailPage = homePage.searchBy(sku);		
    }

    @Then("I should see sku detail page for sku : \"$sku\"")
    public void iShouldGoToTheSkuDetailPage(String sku) throws Exception {
	   Assert.assertTrue("----- Failed to navigate to sku detail page ------", skuDetailPage.isNavigateToExceptedSkuDetails(sku));

    }
   
    @Then("Page should have skuLowQtyMessage element")
    public void pageShouldHaveSkuLowQtyElement() throws Exception {
		Assert.assertTrue("----- Failed. Could not find skuLowQty element on page ------", skuDetailPage.containsSkuLowQtyMessageElement());
    }
    
	@When("I search for a specific sku with normal stock : \"$sku\"")
    public void iSearchForSpecificSkuWithNormalStock(String sku) throws Exception {
    	homePage = new HomePage();
    	skuDetailPage = homePage.searchBy(sku);
    }
	
    @Then("Page should not have skuLowQtyMessage element")
    public void pageShouldNotHaveSkuLowQtyElement() throws Exception {
		Assert.assertTrue("----- Failed. Could not find skuLowQty element on page ------", !skuDetailPage.containsSkuLowQtyMessageElement());
    }
    

}