package com.officedepot.jbehave.www.steps.checkout;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CartRemovedCouponPage;
import com.officedepot.jbehave.www.pages.checkout.CouponDialogPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckoutAddCouponBoxTestSteps extends BaseStep {
	private CouponDialogPage couponDialogPage;
	private CartRemovedCouponPage cartRemovedCouponPage;

	@When("I remove current coupon from remove link")
	public void iRemoveCurrentCouponFromRemoveLink() throws Exception {
		couponDialogPage = new CouponDialogPage();
		cartRemovedCouponPage = couponDialogPage.removeCurrentCoupon();
	}

	@Then("I should remove current coupon")
	public void iShouldRemoveCurrentCoupon() throws Exception {
		Assert.assertFalse("---Failed to remove coupon!---", cartRemovedCouponPage.isCouponRemoved());
	}

	@When("I navigate to coupon details page from see offer coupon describe link")
	public void iNavigateToCouponDetailsPageFromSeeOfferCouponDescribeLink() throws Exception {
		couponDialogPage = new CouponDialogPage();
		couponDialogPage.navigateToCouponDetailsPageByCouponDescribeLink();
	}

}
