package com.officedepot.jbehave.www.steps.account;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.account.AccountOverviewPage;
import com.officedepot.jbehave.www.pages.account.PaymentOptionsPage;
import com.officedepot.jbehave.www.pages.account.SubmitReturnPage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.orderhistory.ShowingAllOrdersPage;
import com.officedepot.jbehave.www.pages.subscription.SubscriptionManagerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class HeaderAccountWithLoginSteps extends BaseStep {

	private PageHeader pageHeader;
	private ShowingAllOrdersPage showingAllOrdersPage;

	@When("I request to view welcome information from header account")
	public void iRequestToViewWelcomeInformationFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
	}

	@Then("I should see welcome information in header account")
	public void iShouldSeeWelcomeInformationInHeaderAccount() throws Exception {
		assertTrue("--- Failed to verify the welcome info! ---", pageHeader.isWelcomeInfoDisplayed());
	}

	@When("I navigate to account overview page from header account")
	public void inavigateToAccountOverViewPageFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnAccountOverviewLink();
	}

	@Then("I should see account overview page")
	public void iShouldSeeAccountOverViewPage() throws Exception {
		AccountOverviewPage accountOverviewPage = new AccountOverviewPage();
		assertTrue("--- Failed to navigate on my account overview page! ---", accountOverviewPage.isInCurrentPage());
	}

	@When("I navigate to tracking and history page from header account")
	public void inavigateToTrackInAandHistoryPageFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnOrderTrackingAndHistoryLink();
	}

	@Then("I should see tracking and history page")
	public void iShouldSeeTrackingAndHistoryPage() throws Exception {
		ShowingAllOrdersPage showingAllOrdersPage = new ShowingAllOrdersPage();
		assertTrue("--- Failed to navigate on tracking and history page! ---", showingAllOrdersPage.isInCurrentPage());
	}

	@When("I navigate to subscriptions manager page from header account")
	public void inavigateToSubscriptionsManagerPageFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnsubscriptionsManagerLink();
	}

	@Then("I should see subscriptions manager page")
	public void iShouldSeeSubscriptionsManagerPage() throws Exception {
		SubscriptionManagerPage subscriptionManagerPage = new SubscriptionManagerPage();
		assertTrue("--- Failed to navigate on tracking and history page! ---", subscriptionManagerPage.isNavigateOnThisPage());
	}

	@When("I navigate to payment options page from header account")
	public void inavigateToPaymentOptionsPageFromHeaderAccount() {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnPaymentOptionsLink();
	}

	@Then("I should see payment options page")
	public void iShouldSeePaymentOptionsPage() {
		PaymentOptionsPage paymentOptionsPage = new PaymentOptionsPage();
		assertTrue("--- Failed to navigate on payment options page! ---", paymentOptionsPage.isInCurrentPage());
	}

	@When("I navigate to submit return page from header account")
	public void inavigateToSubmitReturnPageFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnSubmitReturnLink();
	}

	@Then("I should see submit return page")
	public void iShouldSeeSubmitReturnPage() throws Exception {
		SubmitReturnPage submitReturnPage = new SubmitReturnPage();
		assertTrue("--- Failed to navigate on submit return page! ---", submitReturnPage.isInCurrentPage());
	}

	@When("I navigate to reorder page from header account")
	public void inavigateToReorderPageFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnReorderLink();
	}

	@Then("I should see reorder page")
	public void iShouldSeeReorderPage() throws Exception {
		showingAllOrdersPage = new ShowingAllOrdersPage();
		assertTrue("--- Failed to navigate on tracking and history page! ---", showingAllOrdersPage.isInCurrentPage());
	}

	@When("I logout from header account")
	public void iLogoutFromHeaderAccount() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.viewNavigatorOnHeader("Account");
		pageHeader.clickOnLogOutLink();
	}

	@Then("I should not see the user info left")
	public void iShouldNotSeeTheUserInfoLeft() {
		assertTrue(pageHeader.isThereNoUserInfo());
	}
}
