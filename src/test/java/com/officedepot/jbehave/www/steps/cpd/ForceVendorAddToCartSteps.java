package com.officedepot.jbehave.www.steps.cpd;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class ForceVendorAddToCartSteps extends BaseStep {
	private HomePage homePage = new HomePage();
	private ShoppingCartPage shoppingCartPage;

	@When("I use the vendorString \"$vendorString\" and configId \"$configId\" to build the vendor add to cart url")
	public void iUseTheVendorStringAndConfigIdToBuildTheVendorAddToCartUrl(String vendorString, String configId) throws Exception {
		homePage.openCPDVendorAddToCartPage(vendorString, configId);
	}

	@Then("the vendor item \"$number\" is added to cart")
	public void theVendorItemIsAddedToCart(String number) throws Exception {
		shoppingCartPage = new ShoppingCartPage();
		Assert.assertTrue(shoppingCartPage.theVendorItemIsAddedToCart(number));
	}

}
