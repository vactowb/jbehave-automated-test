package com.officedepot.jbehave.www.steps.siteinfo;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.siteinfo.SiteMapPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SiteMapTestSteps extends BaseStep {
	private HomePage homgPage;
	private SiteMapPage siteMapPage;
	
    @When("I navigate to site map page")
    public void iNavigateToSiteMapPage() throws Exception {
    	homgPage = new HomePage();
    	siteMapPage = homgPage.openSiteMapPage();
    }

    @Then("I should see the site map page")
    public void iShouldSeeTheSiteMapPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to site map page!---", siteMapPage.isInCurrentPage());
    }

}
