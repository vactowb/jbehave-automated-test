package com.officedepot.jbehave.www.steps.loyalty;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.loyalty.JoinAndActiveRewardsPage;
import com.officedepot.jbehave.www.utils.PromptMessageTestData;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class LoyaltySteps extends BaseStep {

	private JoinAndActiveRewardsPage joinAndActiveRewardsPage;

	@When("I access join and active welcome page")
	public void iAccessWWWSite() throws Exception {
		initJoninAndActiveRewardsPage();
		joinAndActiveRewardsPage.openWelcomePage();
	}

	@Then("I should see join and active welcome page opened")
	public void isJoinAndActiveWelcomePageOpen() {
		assertTrue(joinAndActiveRewardsPage.isJoinAndActiveWelcomePageOpen());
	}

	@When("I join the rewards with existing od account")
	public void joinRewardsWithExistOd() {
		joinAndActiveRewardsPage.clickJoinNow();
		joinAndActiveRewardsPage.clickHaveODAccountY();
	}

	@When("I login to link office depot account $accountDetails")
	public void loginToLinkOdAccount(ExamplesTable accountDetails) {
		String loginUsername = accountDetails.getRow(0).get("Username");
		String loginPassword = accountDetails.getRow(0).get("Password");
		joinAndActiveRewardsPage.loginToLinkOdAccount(loginUsername, loginPassword);
	}

	@Then("I should notice activate rewards account is already linked")
	public void isUserAlreadyLinked() {
		assertTrue(joinAndActiveRewardsPage.isUserAlreadyLinked());
	}

	@Then("I should notice activate rewards account successfuly")
	public void isActivateRewardsAccountSucc() {
		assertTrue(joinAndActiveRewardsPage.isActivateRewardsAccountSucc());
	}

	@When("I make the email address as blank")
	public void makeEmailBlank() {
		joinAndActiveRewardsPage.makeEmailBlank();
	}

	@Then("I notice error for blank email is displayed")
	public void isBlankEmailErrorDisplay() {
		assertTrue(joinAndActiveRewardsPage.isBlankEmailErrorDisplay());
	}

	@Then("I should notice that all required fields has been filled out $accountDetails")
	public void isAllRequiredFieldsFilledOut(ExamplesTable accountDetails) {
		String username = accountDetails.getRow(0).get("Username");
		String phoneNumber = accountDetails.getRow(0).get("PhoneNumber");
		String address = accountDetails.getRow(0).get("Address");
		assertTrue(joinAndActiveRewardsPage.isAllRequiredFieldsFilledOut(username.toUpperCase(), phoneNumber, address));
	}

	@Then("I should see verify you info modal automatically pops-up")
	public void isVerifyInfoModalPopUp() {
		assertFalse(joinAndActiveRewardsPage.isVerifyInfoModalPopUp());
	}

	@When("I join the rewards with new od account")
	public void joinRewardsWithNewOd() {
		joinAndActiveRewardsPage.clickJoinNow();
		joinAndActiveRewardsPage.clickHaveODAccountN();
	}

	@When("I join the rewards with od account")
	public void joinRewards() {
		joinAndActiveRewardsPage.clickJoinNow();
	}

	@When("I continue to join rewards")
	public void continueToJoinRewards() {
		joinAndActiveRewardsPage.clickContinueButton();
	}

	@Then("I should notice that all required fields must be filled out")
	public void isErrorMsgAllRequiredFieldsNeed() {
		assertTrue(joinAndActiveRewardsPage.isErrorMsgAllReqFieldsNeed());
	}

	@Then("I should notice that membership type is filled in \"$membershipType\"")
	public void isMembershipTypeFilledIn(String membershipType) {
		assertTrue(joinAndActiveRewardsPage.isMembershipTypeFilledIn(membershipType));
	}

	@When("I input all the needed information with wrong confirm password")
	public void inputAllNeedInfoWithWrongConfirmPassword() {
		joinAndActiveRewardsPage.inputInforWithConfirmPsw("123456");
		// joinAndActiveRewardsPage.clickContinueButton();
	}

	@Then("I should notice that the passwords do not match")
	public void isErrorMsgPasswordNotMatch() {
		assertTrue(joinAndActiveRewardsPage.isErrorMsgPasswordNotMatch());
	}

	@When("I input all the needed information with invalid email \"$email\"")
	public void inputAllNeedInfoWithInvalidEmail(String email) {
		joinAndActiveRewardsPage.inputInforWithEmail(email);
	}

	@Then("I should notice that the email address is invalid")
	public void isErrorMsgInvalidEmail() {
		assertTrue(joinAndActiveRewardsPage.isErrorMsgInvalidEmail());
	}

	@Then("I should notice welcome firstname is displayed")
	public void isWelcomeFirstnameDisplayed() {
		assertTrue(joinAndActiveRewardsPage.isWelcomeFirstnameDisplayed());
	}

	@Then("I should notice linked user not display activate and join button")
	public void isActivateAndJoinButtonDisplay() {
		assertTrue(joinAndActiveRewardsPage.isActivateAndJoinButtonDisplay());
	}

	@When("I input all the needed information with invalid phone number")
	public void inputAllNeedInfoWithInvalidPhoneNumber() {
		joinAndActiveRewardsPage.inputAllNeedInfoWithInvalidPhoneNumber();
	}

	@Then("I should notice that the phone number is invalid")
	public void isPhoneInvalid() {
		assertTrue(joinAndActiveRewardsPage.isPhoneInvalid());
	}

	@When("I input all the needed information with invalid zipcode")
	public void inputAllNeedInfoWithInvalidZipcode() {
		joinAndActiveRewardsPage.inputAllNeedInfoWithInvalidZipcode();
	}

	@Then("I should notice that the zip code number is invalid")
	public void isZipcodeInvalid() {
		assertTrue(joinAndActiveRewardsPage.isZipcodeInvalid());
	}

	@Then("I should notice that the phone number is in use")
	public void isPhoneInUse() {
		joinAndActiveRewardsPage.clickPswConfirm();
		assertTrue(joinAndActiveRewardsPage.isPhoneInUse());
	}

	@Then("I should notice that the password is not meet mini requirement")
	public void isPhoneMiniReq() {
		assertTrue(joinAndActiveRewardsPage.isPasswordMiniReq());
	}

	@Then("I should notice that the email has already in used")
	public void isErrorMsgEmailLinked() {
		assertTrue(joinAndActiveRewardsPage.isErrorMsgEmailLinked());
	}

	@When("I input all the needed information create loyalty account")
	public void inputAllNeedInfor() {
		joinAndActiveRewardsPage.inputAllInfo();
		joinAndActiveRewardsPage.clickContinueButton();
	}

	@Then("I should see a member number on my account overview page")
	public void isMemberNumberShow() {
		initJoninAndActiveRewardsPage();
		joinAndActiveRewardsPage.isMemberNumberShow();
	}

	@When("I activate the rewards with the member id \"$memberId\"")
	public void activateRewardsEnterMember(String memberId) {
		joinAndActiveRewardsPage.activateRewardsEnterMember(memberId);
	}

	@When("I activate with the member id \"$memberId\" at the join page")
	public void activateEnterMember(String memberId) {
		joinAndActiveRewardsPage.activateEnterMember(memberId);
	}

	@When("I activate the rewards with new account")
	public void activateRewardsWithNewAccount() {
		joinAndActiveRewardsPage.clickHaveODAccountN();
	}

	@When("I activate the rewards with given account")
	public void accountRewardsWithGivenAccont() {
		joinAndActiveRewardsPage.clickHaveODAccountY();
	}

	@Then("I should see that the member id you entered is not valid")
	public void isMemberIdvalid() {
		assertTrue(joinAndActiveRewardsPage.isMemberIdvalid());
	}

	@Then("I should see that the member id you entered can not used online")
	public void isMemberCanUsed() {
		assertTrue(joinAndActiveRewardsPage.isMemberCanUsed());
	}

	@Then("I should see that the member id you entered invalid")
	public void isMemberInvalid() {
		assertTrue(joinAndActiveRewardsPage.isMemberIdvalid());
	}

	@When("I login from office depot rewards page")
	public void click2LoginRewards() {
		joinAndActiveRewardsPage.click2LoginRewards();
	}

	@Then("I should see the login rewards lightbox show")
	public void isLoginRewardsLightboxShow() {
		assertTrue(joinAndActiveRewardsPage.isLoginRewardsLightboxShow());
	}

	@When("I enter the needed id and password $accountDetails")
	public void enterNeededIdAndPassword(ExamplesTable accountDetails) {
		String loginUsername = accountDetails.getRow(0).get("Username");
		String loginPassword = accountDetails.getRow(0).get("Password");
		joinAndActiveRewardsPage.enter2LoginAuth(loginUsername, loginPassword);
	}

	@Then("I should see user information display at ssi page")
	public void isSSIPageDisplay() {
		assertTrue(joinAndActiveRewardsPage.isPageSSIDisplay());
	}

	@Then("I should see member number display at ssi page")
	public void isMemberNumDisplay() {
		assertTrue(joinAndActiveRewardsPage.isMemberNumDisplay());
	}

	@Then("I should see warning message phone number is linked")
	public void isMsgPhoneNumberLinked() {
		assertTrue(joinAndActiveRewardsPage.isMsgMemberNumDisplay());
	}

	@When("I active rewards from look up my odr member id use phone number \"$number\"")
	public void lookUpMemberIdWithInvalidNumber(String number) {
		joinAndActiveRewardsPage.clickForgotLoyIdLink();
		joinAndActiveRewardsPage.inputMemberPhoneNum(number);
	}

	@Then("I should notice the member ID \"$number\" has been filled")
	public void isMemberIdFilled(String member) {
		assertTrue(joinAndActiveRewardsPage.isMemberIdFilled(member));
	}

	@When("I login rewards from look up my odr member id with phone number \"$number\"")
	public void lookUpMemberIdWithInvalid(String number) {
		joinAndActiveRewardsPage.clickForgotLoyId();
		joinAndActiveRewardsPage.inputMemberPhoneNum(number);
	}

	@Then("I should notice the member ID \"$member\" has been filled at account login page")
	public void isMemberFilledAtAccountLoginPage(String member) {
		assertTrue(joinAndActiveRewardsPage.isMemberFilledAtAccountLoginPage(member));
	}

	@Then("I should see the forgotLoy error message phone number is invalid when active rewards")
	public void seeForgotLoyErrorMeg() {
		assertTrue(joinAndActiveRewardsPage.seeForgotLoyErrorMeg(PromptMessageTestData.registerInvalidPhone2));
	}

	@Then("I should see the forgotLoy error message phone number is userd when active rewards")
	public void seeForgotLoyErrorMegUsed() {
		assertTrue(joinAndActiveRewardsPage.seeForgotLoyErrorMeg(PromptMessageTestData.registerUsedPhone));
	}

	@When("I register from office depot rewards page")
	public void registerFromOdr() {
		joinAndActiveRewardsPage.clickRegisterNew();
	}

	@Then("I should see the register with rewards page shown")
	public void isRegisterRewardsPageShown() {
		assertTrue(joinAndActiveRewardsPage.isRegisterRewardsPageShown());
	}

	@Then("I should see the error warn your login name or password is incorrect")
	public void isErrorLoginShow() {
		assertTrue(joinAndActiveRewardsPage.isErrorLoginShow());
	}

	@Then("I should see the error loyalty already associated to account")
	public void isErrorLoyaltyAlreadyAssociated() {
		assertTrue(joinAndActiveRewardsPage.isErrorLoyaltyAlreadyAssociated());
	}

	@Then("I should see verify your identity lightbox")
	public void isVerifyIdentityLightboxShow() {
		assertTrue(joinAndActiveRewardsPage.isVerifyIdentityLightboxShow());
	}

	@Then("I should see choose the delivery method for your registration code")
	public void isShownChooseDeliveryMethodForCode() {
		joinAndActiveRewardsPage.click2TropoOk();
		assertTrue(joinAndActiveRewardsPage.isShownChooseDeliveryMethodForCode());
	}

	@When("I choose the delivery method for your registration code")
	public void chooseDeliveryMethod() {
		joinAndActiveRewardsPage.chooseDeliveryMethod();
	}

	@Then("I should see registration code has been sent to the following address")
	public void isRegistrationCodeSent() {
		assertTrue(joinAndActiveRewardsPage.isRegistrationCodeSent());
	}

	@When("I enter invalid tropo registration code")
	public void inputInvalidRegistrationCode() {
		joinAndActiveRewardsPage.inputInvalidRegistrationCode();
	}

	@Then("I should see registration code input invalid")
	public void isRegistrationCodeInvalid() {
		assertTrue(joinAndActiveRewardsPage.isRegistrationCodeInvalid());
	}

	@When("I input all the needed information with membership type: \"$type\"")
	public void inputInforWithMemberShipType(String type) {
		joinAndActiveRewardsPage.inputInforWithTpye(type);
	}

	@Then("I should see input field for membership type enter a \"$name\"")
	public void isInputFieldforMemberShipType(String name) {
		assertTrue(joinAndActiveRewardsPage.isInputFieldforMemberShipType(name));
	}

	private void initJoninAndActiveRewardsPage() {
		if (joinAndActiveRewardsPage == null)
			joinAndActiveRewardsPage = new JoinAndActiveRewardsPage();
	}
}
