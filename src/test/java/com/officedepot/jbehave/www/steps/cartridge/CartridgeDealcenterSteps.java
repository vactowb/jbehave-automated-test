package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.CategoriesPage;
import com.officedepot.jbehave.www.pages.deals.AllInOneComputersCategoriesPage;
import com.officedepot.jbehave.www.pages.deals.ComputersAndTabletsCategoriesLandingPage;
import com.officedepot.jbehave.www.pages.deals.CouponCenterPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeDealcenterSteps extends BaseStep {

	private HomePage homePage;
	private DealCenterPage dealCenterPage;
	private CategoriesPage categoriesPage;
	private CouponCenterPage couponCenterPage;
	private ComputersAndTabletsCategoriesLandingPage computersAndTabletsCategoriesLandingPage;
	private AllInOneComputersCategoriesPage allInOneComputersCategoriesPage;
	private String dealCenterTitle;

	@When("I navigate to categories page from shop all link")
	public void iNavigateToComputersAndTabletsCategoriesPageFromShopAllLink() throws Exception {
		dealCenterPage = new DealCenterPage();
		categoriesPage = dealCenterPage.navigateToTheCategoriesPage();
	}

	@Then("I should see the categories page")
	public void iShouldSeeTheComputersAndTabletsCategoriesPage() throws Exception {
		categoriesPage = new CategoriesPage();
		Assert.assertTrue("---Failed navigate to the categories page!---", categoriesPage.isNavigateOnThisPage());
	}

	@When("I navigate to computers and tablets categories landing page from shop all computers and tablets link")
	public void iNavigateToComputersAndTabletsCategoriesLandingPageFromShopAllComputersAndTabletsLink() throws Exception {
		computersAndTabletsCategoriesLandingPage = categoriesPage.navigateToTheComputersAndTabletsCategoriesLandingPage();
	}

	@Then("I should see the computers and tablets categories landing page")
	public void iShouldSeeTheComputersAndTabletsCategoriesLandingPage() throws Exception {
		Assert.assertTrue("---Failed navigate to the computers and tablets categories landing page!---", computersAndTabletsCategoriesLandingPage.isNavigateOnThisPage());
	}

	@When("I navigate to the first computers and tablets categories page from category heading section")
	public void iNavigateToTheFirstComputersAndTabletsCategoriesPageFromCategoryHeadingSection() throws Exception {
		allInOneComputersCategoriesPage = computersAndTabletsCategoriesLandingPage.navigateToTheAllInOneComputersCategoriesPage();
	}

	@Then("I should see the first computers and tablets categories page from category heading section")
	public void iShouldSeeTheFirstComputersAndTabletsCategoriesPageFromCategoryHeadingSection() throws Exception {
		Assert.assertTrue("---Failed navigate to the  first computers and tablets categories page!---", allInOneComputersCategoriesPage.isNavigateOnThisPage());
	}

	@When("I navigate to coupon center page from shop current coupon offers link")
	public void iNavigateToCouponPageFromShopCurrentCouponOffersLink() throws Exception {
		dealCenterPage = new DealCenterPage();
		dealCenterTitle = dealCenterPage.getTitle();
		couponCenterPage = dealCenterPage.navigateToTheCouponPageFromShopCurrentCouponOffersLink();
	}

	@Then("I should see the coupon center page")
	public void iShouldSeeTheCouponPage() throws Exception {
		Assert.assertTrue("---Failed navigate to the coupon page!---", couponCenterPage.isNavigateOnThisPage());
		Assert.assertFalse("---The coupon page title is same with the dealcenter page title!---", couponCenterPage.isTitleDifferentWithDealCenter(dealCenterTitle));
	}

	@When("I navigate to categories page from deal center adBranding")
	public void iNavigateToCategoriesPageFromDealCenterAdBranding() throws Exception {
		dealCenterPage = new DealCenterPage();
		homePage = dealCenterPage.clickOnAdBranding();
	}
}
