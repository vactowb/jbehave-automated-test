package com.officedepot.jbehave.www.steps.cartridge;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.TechnologyFeaturedCategoriesPage;
import com.officedepot.jbehave.www.pages.supplies.TechnologyPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeViewAllItemsSteps extends BaseStep {
	private TechnologyPage technologyPage;
	private TechnologyFeaturedCategoriesPage technologyFeaturedCategoriesPage;
    @When("I navigate to technology categories result page from view all items link")
    public void iNavigateToTechnologyCategoriesResultPageFromViewAllItemsLink() throws Exception {
    	technologyPage = new TechnologyPage();
    	technologyFeaturedCategoriesPage = technologyPage.clickOnViewAllItemsLink();
    }

    @Then("I should see the technology categories result page")
    public void iShouldSeeTheTechnologyCategoriesResultPage() throws Exception {
    	
    	Assert.assertTrue("---Failed navigate to the technology categories result page!---", technologyFeaturedCategoriesPage.isNavigateOnThisPage());
    	
    }

}
