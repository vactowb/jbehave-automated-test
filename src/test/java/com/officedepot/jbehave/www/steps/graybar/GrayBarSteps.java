package com.officedepot.jbehave.www.steps.graybar;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.cartridge.ChatNowWindow;
import com.officedepot.jbehave.www.pages.deals.WeeklyAdPage;
import com.officedepot.jbehave.www.pages.graybar.OrderByItemPage;
import com.officedepot.jbehave.www.pages.graybar.PageGrayBar;
import com.officedepot.jbehave.www.pages.home.BSDHomePage;
import com.officedepot.jbehave.www.pages.home.OfficemaxWorkplacePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class GrayBarSteps extends BaseStep {

	private PageGrayBar pageGrayBar;
	private String parentWindow;
	private WeeklyAdPage weeklyAdPage;
	private PageHeader pageHeader;
	private OrderByItemPage orderByItemPage;
	private ChatNowWindow chatNowWindow;
	private BSDHomePage bSDHomePage;
	private OfficemaxWorkplacePage officemaxWorkplacePage;

	@When("I navigate to weeky ad page from gray bar in header")
	public void iNavigateToWeekyAdPageFromGrayBarInHeader() throws Exception {
		pageGrayBar = new PageGrayBar();
		parentWindow = pageGrayBar.getCurrentWindowHandle();
		weeklyAdPage = pageGrayBar.clickOnWeeklyAdGrayBar();
	}

	@Then("I should navigate to weekly ad page from weeky ad")
	public void iShouldNavigateToWeeklyAdPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on weekly ad page! ---", weeklyAdPage.isNavigateOnThisPage());
		weeklyAdPage.goBackToPreWindow(parentWindow);
	}
	
    @When("I open account login window from gray bar login or register link")
    public void iOpenAccountLoginWindowFromGrayBarLoginOrRegisterLink() throws Exception {
    	pageGrayBar = new PageGrayBar();
    	pageGrayBar.clickOnGrayBarLoginAndRegister();
    }

    @Then("I should see the account login window")
    public void iShouldSeeTheAccountLoginWindow() throws Exception {
    	pageHeader = new PageHeader();
    	Assert.assertTrue("--- Failed to see he account login window! ---", pageHeader.isLoginDialogDisplayed());
    }
    
    @When("I navigate to order by item page from gray bar order by item link")
    public void iNavigateToOrderByItemPageFromGrayBarOrderByItemLink() throws Exception {
     	pageGrayBar = new PageGrayBar();
     	orderByItemPage = pageGrayBar.clickOnOrderByItemLink();
    }

    @Then("I should see the order by item page")
    public void iShouldSeeTheOrderByItemPage() throws Exception {
    	Assert.assertTrue("--- Failed to order by item page! ---", orderByItemPage.isNavigateOnThisPage());
    }

    @When("I open live chat window from gray bar live chat link")
    public void iOpenLiveChatWindowFromGrayBarLiveChatLink() throws Exception {
    	pageGrayBar = new PageGrayBar();
    	chatNowWindow = pageGrayBar.clickOnLiveChatLink();
    }

    @Then("I should see the BSD home page")
    public void iShouldSeeTheCorporateAndGovernmentPage() throws Exception {
    	Assert.assertTrue("--- Failed to BSD home page! ---", bSDHomePage.isInCurrentPage());
    }
    
    @When("I navigate to office depot BSD home page from gray bar flyout")
    public void iNavigateToOfficeDepotBSDHomePageFromGrayBarFlyout() throws Exception {
    	pageGrayBar = new PageGrayBar();
    	pageGrayBar.viewCorporateAndGovernmentFlyout();
    	bSDHomePage = pageGrayBar.navigateToBSDHomePage();
    }
    
    @When("I navigate to officemax workplace page from gray bar flyout")
    public void iNavigateToOfficemaxWorkplacePageFromGrayBarFlyout() throws Exception {
    	pageGrayBar = new PageGrayBar();
    	pageGrayBar.viewCorporateAndGovernmentFlyout();
    	officemaxWorkplacePage = pageGrayBar.navigateToOfficemaxWorkplacePage();
    }

    @Then("I should see the officemax workplace page")
    public void iShouldSeeTheOfficemaxWorkplacePage() throws Exception {
    	Assert.assertTrue("--- Failed to officemax workplace page! ---", officemaxWorkplacePage.isInCurrentPage());
    }

}
