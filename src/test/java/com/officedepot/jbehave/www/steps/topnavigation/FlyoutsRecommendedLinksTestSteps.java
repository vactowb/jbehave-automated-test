package com.officedepot.jbehave.www.steps.topnavigation;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.topnavigation.TopNavigationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class FlyoutsRecommendedLinksTestSteps extends BaseStep {

	private TopNavigationPage topNavigationPage;
	private DealCenterPage dealCenterPage;
	
    @When("I navigate to recommended link page frome office supplies new top navigation")
    public void iNavigateToRecommendedLinkPageFromePaperNewTopNavigation() throws Exception {
    	topNavigationPage = new TopNavigationPage();
    	topNavigationPage.viewOfficeSuppliesNavigatorOnTopNavigation();
    	dealCenterPage = topNavigationPage.clickOnRecommendedLink();
    }

    @Then("I should see the paper recommended link page")
    public void iShouldSeeThePaperRecommendedLinkPage() throws Exception {
    	Assert.assertTrue("---Failed navigate to deal center page!---", dealCenterPage.isNavigateOnThisPage());
    }

}
