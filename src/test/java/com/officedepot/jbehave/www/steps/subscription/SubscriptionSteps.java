package com.officedepot.jbehave.www.steps.subscription;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.sku.CatalogSearchPage;
import com.officedepot.jbehave.www.pages.sku.SubscriptionDetialPage;
import com.officedepot.jbehave.www.pages.subscription.SubscriptionManagerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SubscriptionSteps extends BaseStep {

	private SubscriptionManagerPage subscriptionManagerPage;
	private CatalogSearchPage catalogSearchPage;
	private SubscriptionDetialPage subscriptionDetialPage;

	@Then("I could redirect to the subscription manager page")
	public void checkSubscriptionManage() throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		assertTrue("---Fail to redircte subscription manager page!---", subscriptionManagerPage.checkPageTitle());
	}

	@When("I navigate to the next page of subscription list")
	public void navigateToNextPage() throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		subscriptionManagerPage.navigateToNextPage();
	}

	@Then("I can get the link to return first page")
	public void displayFirstLink() throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		assertTrue("---Fail to get 'First' link!---", subscriptionManagerPage.displayFirstLink());
	}

	@When("I navigate to the previous page of subscription list")
	public void navigateToPrevPage() throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		subscriptionManagerPage.navigateToPrevPage();
	}

	@Then("I cannot get to previous page")
	public void displayPrevLink() throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		assertFalse("---Still get 'Prev' link, behaviour is incorrect!---", subscriptionManagerPage.displayPrevLink());
	}

	@When("I navigate to the first page of subscription list")
	public void navigateToFirstPage() throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		subscriptionManagerPage.navigateToFirstPage();
	}

	@When("Set frequency filter to \"$condition\"")
	public void setFilterConditionForFrequency(String condition) throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		subscriptionManagerPage.setFilterConditionForFrequency(condition);
	}

	@Then("The filter condition is correct with \"$result\"")
	public void checkFrequencyConditionValue(String result) throws Exception {
		subscriptionManagerPage = new SubscriptionManagerPage();
		assertTrue("---The Filter condition is wrong!---", subscriptionManagerPage.getFrequencyConditionValue().equals(result));
	}

	@Then("I can get the searched result for : \"$description\"")
	public void checkCatalogSearchPageDisplay(String description) throws Exception {
		catalogSearchPage = new CatalogSearchPage();
		assertTrue("---Fail to navigate to catalog search page!---", catalogSearchPage.getCatologDescription().trim().equalsIgnoreCase(description));
	}

	@When("I select one sku to Subscribe")
	public void subscriptSku() throws Exception {
		catalogSearchPage = new CatalogSearchPage();
		catalogSearchPage.SubscribeSku();
	}

	@Then("I can navigate to the subscription detail page")
	public void openSubscriptionDetialPage() throws Exception {
		subscriptionDetialPage = new SubscriptionDetialPage();
		assertTrue("---Fail to navigate to subscritpion detail page!---", subscriptionDetialPage.isOpen());
	}

	@When("I choose the frequency filter to \"$type\"")
	public void chooseFrequency(String type) throws Exception {
		subscriptionDetialPage = new SubscriptionDetialPage();
		subscriptionDetialPage.chooseFrequency(type);
	}

	@When("I add the subscription to cart")
	public void addSubscriptionToCart() throws Exception {
		subscriptionDetialPage = new SubscriptionDetialPage();
		subscriptionDetialPage.addSubscriptionToCart();
	}

	@Then("I should see \"$qty\" of the subscritpion added to cart")
	public void verifyAddSubscriptionToCart(String qty) throws Exception {
		subscriptionDetialPage = new SubscriptionDetialPage();
		assertEquals("---Failed to assert the subscription Qty!---", subscriptionDetialPage.getSubscriptionQty(qty), qty);
	}

	@When("I navigate to shopping cart page")
	public void navigateToShoppingCart() throws Exception {
		new SubscriptionDetialPage().navigateToShoppingCart();
	}

	@When("I add one sku is non-Subscribe into cart")
	public void addNonsubscriptedSkuToCart() throws Exception {
		catalogSearchPage = new CatalogSearchPage();
		catalogSearchPage.addSkuToCart();
	}

	@When("I input the quntity \"$qty\" in one item")
	public void inputQunity(String qty) throws Exception {
		catalogSearchPage = new CatalogSearchPage();
		catalogSearchPage.inputQunity(qty);
	}

	@When("I add the sku to cart from catalog search page")
	public void addSkuToCart() throws Exception {
		catalogSearchPage = new CatalogSearchPage();
		catalogSearchPage.addSkuToCart();
	}

}
