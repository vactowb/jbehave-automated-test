package com.officedepot.jbehave.www.steps.shopping;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.obi.ObiPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class ObiForCommonSkuSteps extends BaseStep {

	ObiPage obiPage = new ObiPage();

	@When("I enter SKU with the given SKU Id \"$skuid\" on obi page")
	public void iEnterSKUWithTheGivenSKUIdOnObiPage(String skuid) throws Exception {
		// obiPage.openPage();
		// obiPage.enterSkuAndTabAway(skuid);
	}

	@Then("I should see sku info loaded for the given SKU \"$skuid\" on the obi page")
	public void iShouldSeeSkuInfoLoadedForTheGivenSKUOnTheObiPage(String skuid) throws Exception {
		// Assert.assertFalse("---Failed to see sku details!---",
		// obiPage.isSkuLoaded(skuid));
	}

}
