package com.officedepot.jbehave.www.steps.cartridge;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartridgeInkAndTonerFinderSteps extends BaseStep {

	private InkAndTonerPage inkAndTonerPage;
	private String brandName;

	@When("I select the first printer brand from select printer brand drop down")
	public void iSelectTheFirstPrinterBrandFromSelectPrinterBrandDropDown() throws Exception {
		inkAndTonerPage = new InkAndTonerPage();
		inkAndTonerPage.selectTheFirstPrinterBrand();
		brandName = inkAndTonerPage.getTheFirstPrinterBrandName();
	}

	@Then("I should see all first brands")
	public void iShouldSeeAllBrands() throws Exception {
		Assert.assertTrue("---Failed find all the" + brandName + "!---", inkAndTonerPage.isBrandBefoundSelectByBrand(brandName));
	}

	@When("I select the first printer type from select printer type drop down")
	public void iSelectTheFirstPrinterTypeFromSelectPrinterTypeDropDown() throws Exception {
		inkAndTonerPage.selectTheFirstPrinterType();
	}

	@Then("I should see all type brands")
	public void iShouldSeeAllTypeBrands() throws Exception {
		Assert.assertTrue("---Failed find all the" + brandName + "!---", inkAndTonerPage.isBrandBefoundSelectByBrand(brandName));
	}

	@When("I select the first printer model from select printer model drop down")
	public void iSelectTheFirstPrinterModelFromSelectPrinterModelDropDown() throws Exception {
		inkAndTonerPage.selectTheFirstPrinterModel();
	}

	@Then("I should see all model brands")
	public void iShouldSeeAllModelBrands() throws Exception {
		Assert.assertTrue("---Failed find all the model brands", inkAndTonerPage.isSelectedProductsDisplayed());
	}
}
