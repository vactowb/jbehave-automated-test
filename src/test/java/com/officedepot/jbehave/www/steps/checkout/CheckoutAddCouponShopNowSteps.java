package com.officedepot.jbehave.www.steps.checkout;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CouponAddedToCartPage;
import com.officedepot.jbehave.www.pages.checkout.CouponDetailsPage;
import com.officedepot.jbehave.www.pages.checkout.CouponDialogPage;
import com.officedepot.jbehave.www.pages.checkout.CouponPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CheckoutAddCouponShopNowSteps extends BaseStep {

	private CouponDialogPage couponDialogPage;
	private CouponAddedToCartPage couponAddedToCartPage;
	private CouponPage couponPage;
	private CouponDetailsPage couponDetailsPage;
	private HomePage homePage;

	@When("I navigate to coupon added to cart page from see offer details button")
	public void iNavigateToCouponAddedToCartPageFromSeeOfferDetailsButton() throws Exception {
		couponDialogPage = new CouponDialogPage();
		couponAddedToCartPage = couponDialogPage.navigateToCouponAddedToCartPage();
	}

	@Then("I should see the coupon added to cart page")
	public void iShouldSeeTheCouponAddedToCartPage() throws Exception {
		couponAddedToCartPage = new CouponAddedToCartPage();
		Assert.assertTrue("---Failed find  coupon added to cart page", couponAddedToCartPage.isInCurrentPage());
	}

	@When("I navigate to the coupon page from shop now button")
	public void iNavigateToTheCouponPageFromShopNowButton() throws Exception {
		couponPage = couponAddedToCartPage.navigateToCouponPage();
	}

	@Then("I should see the coupon page for coupons with free or discounted items")
	public void iShouldSeeTheCouponPageForCouponsWithFreeOrDiscountedItems() throws Exception {
		Assert.assertTrue("---Failed find  the coupon page for coupons with free or discounted items", couponPage.isInCurrentPage());
	}

	@When("I navigate to the assignable destination page from shop now button")
	public void iNavigateToTheAssignableDestinationPageFromShopNowButton() throws Exception {
		couponDetailsPage = new CouponDetailsPage();
		homePage = couponDetailsPage.clickOnShopNowButton();
	}

	@Then("I should see the assignable destination page for coupons with free or discounted items")
	public void iShouldSeeTheAssignableDestinationPageForCouponsWithFreeOrDiscountedItems() throws Exception {
		Assert.assertTrue("---Failed find the assignable destination page", homePage.isNavigateOnThisPage());
	}
	
	@When("I navigate to the coupon page from no product shop now button")
	public void iNavigateToTheCouponPageFromNoProductNowButton() throws Exception {
		couponDetailsPage = new CouponDetailsPage();
		homePage = couponDetailsPage.clickOnShopNowButton();
	}


}
