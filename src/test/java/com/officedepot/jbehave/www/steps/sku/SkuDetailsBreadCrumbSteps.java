package com.officedepot.jbehave.www.steps.sku;

import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SkuDetailsBreadCrumbSteps extends BaseStep {

	private SkuDetailPage skuDetailPage;

	// private OfficeSuppliesPage OfficeSuppliesPage;

	@When("I navigate to office supplies page from bread scrumb")
	public void iNavigateToOfficeSuppliesPageFromBreadScrumb() throws Exception {
		skuDetailPage = new SkuDetailPage();
		skuDetailPage.backToOfficeSuppliesPage();
	}

	// @Then("I should see office supplies categories page")
	// public void iShouldSeeOfficeSuppliesCategoriesPage() throws Exception {
	//
	// }

}
