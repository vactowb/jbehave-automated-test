package com.officedepot.jbehave.www.steps.shopping;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.shopping.list.DeleteListConfirmPage;
import com.officedepot.jbehave.www.pages.shopping.list.ListDetailsPage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.jbehave.www.pages.shopping.list.SchoolListDetailsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class DeleteMyListSteps extends BaseStep {
	private MyListPage myListPage;
	private boolean isListExist;
	private ListDetailsPage listDetailsPage;
	private By deleteAList;
	private DeleteListConfirmPage deleteListConfirmPage;
	private String listName;
	private SchoolListDetailsPage schoolListDetailsPage;

	@Then("I should see my lists page")
	public void iShouldSeeMyListsOnPage() throws Exception {
		myListPage = new MyListPage();
		Assert.assertTrue("---Failed navigate to my list page!---", myListPage.isInCurrentPage());
	}

	@When("I choose a list and edit it")
	public void iChooseAListAndEditIt() throws Exception {
		listDetailsPage = myListPage.clickOnViewEditLink();
		listName = listDetailsPage.getListName();
	}

	@When("I delete the list")
	public void iDeleteTheList() throws Exception {
		deleteListConfirmPage = listDetailsPage.deleteMyList();
	}

	@When("I delete the shool list")
	public void iDeleteTheSchoolList() throws Exception {
		schoolListDetailsPage = new SchoolListDetailsPage();
		deleteListConfirmPage = schoolListDetailsPage.deleteMySchoolList();
	}

	@Then("I should see the confirm to delete list page")
	public void iShouldSeeTheConfirmToDeleteListPage() throws Exception {
		Assert.assertTrue("---Failed navigate to confirm to delete list page!---", deleteListConfirmPage.isInCurrentPage());
	}

	@When("I confirm to delete list")
	public void iConfirmToDeleteList() throws Exception {
		deleteListConfirmPage.deleteListButton();
	}

	@Then("I should see the school list disappear from my lists page")
	public void iShouldSeeTheSchoolListDisappearFromMyListsPage() throws Exception {
		myListPage = new MyListPage();
		Assert.assertTrue("---Failed navigate to my list page!---", myListPage.isInCurrentPage());
		Assert.assertTrue("---Failed to delete my list!---", myListPage.isMylistAppearInList(listName));
	}

}
