package com.officedepot.jbehave.www.steps.compare;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.search.ProductComparisonPage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CompareSteps extends BaseStep {

	private HomePage homePage;
	private SearchResultsListPage searchResultsListPage;
	private ProductComparisonPage productComparisonPage;

	@When("I search for the given product")
    public void iSearchForTheKeyWordPilotPen(@Named("product") String product) throws Exception {
    	homePage = new HomePage();
		homePage.searchBy(product);
    }

    @When("I request to compare with \"$num\" products selected")
    public void iSelect4ItemsFromTheSearchResultsAndClickTheCompareButton(int num) throws Exception {
    	searchResultsListPage = new SearchResultsListPage();
    	searchResultsListPage.selectRecords(num);
    	productComparisonPage = searchResultsListPage.goToProductComparison();
    }

    @Then("I should go to the product comparison page")
    public void iShouldGoToTheProductComparisonPage() throws Exception {
		Assert.assertTrue("----- Failed to navigate on the product comparison! ------", productComparisonPage.isNavigateOnThisPage());
    }

    @Then("I should see the results of the given product")
    public void iShouldSeeTheResultsOfThePaperDisplayed(@Named("product") String product) throws Exception {
    	searchResultsListPage = new SearchResultsListPage();
		Assert.assertTrue("----- Failed to navigate on the search results! ------", searchResultsListPage.isNavigateToExceptedSearchResults(product));
    }

    @When("I request to compare with no product selected")
    public void iSelectNoItemsFromTheSearchResultsAndClickTheCompareButton() throws Exception {
    	searchResultsListPage = new SearchResultsListPage();
    	productComparisonPage = searchResultsListPage.goToProductComparison();
    }

}
