package com.officedepot.jbehave.www.steps.orderhistory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CheckoutThankYouPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.orderhistory.OrderDetailsPage;
import com.officedepot.jbehave.www.pages.orderhistory.OrderTrackPage;
import com.officedepot.jbehave.www.pages.orderhistory.SearchOrderPage;
import com.officedepot.jbehave.www.pages.orderhistory.ShowingAllOrdersPage;
import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class OrderHistorySteps extends BaseStep {

	private ShowingAllOrdersPage showingAllOrdersPage;
	private OrderDetailsPage orderDetailPage;
	private HomePage homePage;
	private String verifyOrderId;
	private CheckoutThankYouPage checkoutThankYouPage;
	private String orderNumber;
	private OrderTrackPage orderTrackPage;
	private PageHeader pageHeader;
	private ShoppingCartPage shoppingCartPage;
	private FindYourProductPage findYourProductPage;
	private SearchOrderPage searchOrderPage;

	@When("I view my order history")
	public void viewMyOrderHistory() throws Exception {
		homePage = new HomePage();
		showingAllOrdersPage = homePage.viewOrderHistroy();
	}

	@When("I choose one of the order history")
	public void chooseOneOrderHistory() throws Exception {
		showingAllOrdersPage = new ShowingAllOrdersPage();
		showingAllOrdersPage.chooseFirstRecord();
	}

	@When("I choose one of the order history record")
	public void chooseOneOrderHistoryRecord() throws Exception {
		findYourProductPage = showingAllOrdersPage.chooseOneRecord();
	}

	@Then("I should see the order items on find your product page")
	public void seeOrderItemsOnFindYourProductPage() throws Exception {
		assertTrue(findYourProductPage.isNavigateOnThisPage());
	}

	@When("I search by order number")
	public void iSearchByOrderNumber() {
		showingAllOrdersPage.iSearchByOrderNumber(orderNumber);
	}

	@When("I search order by ordernumber: \"$number\"")
	public void iSearchByOrderNumber(String number) {
		showingAllOrdersPage.iSearchByOrderNumber(number);
	}

	@Then("I could see the details of the order history record")
	public void checkDetailsOfAnOrderHistoryRecord() throws Exception {
		orderDetailPage = new OrderDetailsPage();
		assertTrue("---- Failed to verify the order detail ----", orderDetailPage.isNavigateToExceptedOrderDetail());
	}

	@When("I reorder from the order details page")
	public void reorderFromDetailsPage() {
		orderDetailPage.reorderFromDetailsPage();
	}

	@Then("I should see the barcode digits for printing on the order details page")
	public void checkBarcodeDigitsOnOrderDetailsPage() throws Exception {
		assertTrue("---- Failed to verify the order detail ----", orderDetailPage.checkBarcodeDigitsOnOrderDetailsPage());
	}

	@Then("I see the detail for a Taylor Order")
	public void checkDeailForTaylorOrder() throws Exception {
		orderDetailPage = new OrderDetailsPage();
		assertTrue("---- Failed to verify the order detail ----", orderDetailPage.isNavigateToExceptedOrderDetail());
	}

	@Then("I see the detail for a Holland Order")
	public void checkDetailForHollandOrder() throws Exception {
		orderDetailPage = new OrderDetailsPage();
		assertTrue("---- Failed to verify the order detail ----", orderDetailPage.isNavigateToExceptedOrderDetail());
	}

	@When("I cancel the order history")
	public void cancelOrderHistory() throws Exception {
		orderDetailPage = new OrderDetailsPage();
		orderDetailPage.cancelOrder();
	}

	@Then("I should see the order history is cancelled")
	public void checkIfOrderHistoryIsCancelled() throws Exception {
		orderDetailPage = new OrderDetailsPage();
		assertTrue(orderDetailPage.isOrderCancelled());
	}

	@When("I search orders with \"$searchStatus\" status criteria in showing all orders tab")
	public void changeTheStatusCriteriaAndSearchInShowingAllOrdersTab(String searchStatus) throws Exception {
		showingAllOrdersPage.changeStatusCriteriaAndSearch(searchStatus);
	}

	@Then("I see the corresponding \"$searchStatus\" status search results")
	public void checkIfGetTheCorrespondingStatusSearchResults(String searchStatus) throws Exception {
		assertTrue(showingAllOrdersPage.isThereAnySearchResultsForStatusSearch(searchStatus));
	}

	@Then("I see the order type is shown as \"$orderType\"")
	public void iSeeOrderTypeShow(String orderType) {
		assertTrue(showingAllOrdersPage.iSeeOrderTypeShow(orderType));
	}

	@When("I search orders with date criteria in showing all orders tab")
	public void changeTheDateCriteriaAndSearchInShowingAllOrdersTab() throws Exception {
		showingAllOrdersPage.changeDateCriteriaAndSearch();
	}

	@Then("I see the corresponding date search results")
	public void checkIfGetTheCorrespondingDateSearchResults() throws Exception {
		assertTrue(showingAllOrdersPage.isThereAnySearchResultsForDateSearch());
	}

	@Then("I should see the order Details")
	public void iShouldSeeTheOrderDetails() throws Exception {
		checkoutThankYouPage = new CheckoutThankYouPage();
		checkoutThankYouPage.closebrdialog();
		orderNumber = checkoutThankYouPage.shouldSeeOrderDetails();
		assertTrue(!orderNumber.isEmpty());
	}

	@Then("I check the order history with the item \"$skuid\"")
	public void iCheckOrderHistory(String skuid) throws Exception {
		homePage = new HomePage();
		showingAllOrdersPage = homePage.viewOrderHistroy();
		assertTrue(showingAllOrdersPage.findorderHistory(orderNumber, skuid));
	}

	@When("I search orders by order type:\"$orderType\"")
	public void searchOrderByOrderType(String orderType) {
		showingAllOrdersPage.searchOrderByOrderType(orderType);
	}

	@When("I view order history detail with order number: \"$orderId\"")
	public void searchOrderByNumber(String orderId) {
		showingAllOrdersPage.searchOrderByOrderNumber(orderId);
	}

	@Then("I see the date search results empty")
	public void iSeeDateSearchResultEmpty() {
		assertFalse(showingAllOrdersPage.iSeeDateSearchResultEmpty());
	}

	@When("I go to the order tracking page")
	public void go2OrderTrackingPage() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.clicktrackShipment();
	}

	@When("I track my order with the order number")
	public void trackMyOrderWithOrderNumber() {
		orderTrackPage = new OrderTrackPage();
		verifyOrderId = orderNumber;
		orderTrackPage.inputAllInfo2TrackOrder(orderNumber);
	}

	@When("I delete all cookies")
	public void deleteAllCookies() throws Exception {
		orderTrackPage = new OrderTrackPage();
		orderTrackPage.deleteCookies();
		homePage = new HomePage();
		homePage.openHomePage();
	}

	@When("I request to show all items")
	public void requestToShowAllItems() throws Exception {
		showingAllOrdersPage = new ShowingAllOrdersPage();
		showingAllOrdersPage.showAllItems();
	}

	@When("I select sku \"$sku\" from show all items page")
	public void selectSkuFromShowAllItemsPage(String sku) {
		showingAllOrdersPage.selectSkuFromShowAllItemsPage(sku);
	}

	@When("I select all items")
	public void selectAllOrderHistoryItems() throws Exception {
		showingAllOrdersPage.selectAllItems();
	}

	@Then("I should see all items are selected")
	public void seeAllItemsAreSelected() throws Exception {
		assertTrue(showingAllOrdersPage.isAllItemsSelected());
	}

	@When("I reorder from show all items tab")
	public void reorderFromShowAllItem() {
		showingAllOrdersPage.reorderFromShowAllItem();
	}

	@Then("I should see the proucts added successfully")
	public void seeProductsAdded() {
		findYourProductPage = new FindYourProductPage();
		assertTrue(findYourProductPage.reorderItemsSuccess(2));
	}

	@Then("I should see error message prompt must specify which one want to reorder")
	public void shouldSeeErrorMessageFromShowAllItem() {
		assertTrue(showingAllOrdersPage.isSeeErrorMessageFromShowAllItem());
	}

	@Then("I should see error message with invalid qty to reorder")
	public void shouldSeeErrorMessageQtyToReorder() {
		assertTrue(showingAllOrdersPage.isSeeQtyErrorMessageFromShowAllItem());
	}

	@When("I deselect all items")
	public void deselectAllOrderHistoryItems() throws Exception {
		showingAllOrdersPage.deSelectAllItems();
	}

	@Then("I should see all items are deselected")
	public void seeAllItemsAreDeselected() throws Exception {
		assertTrue(showingAllOrdersPage.isAllItemsDeselected());
	}

	@Then("I see the Reorder qty is same as the last order qty")
	public void checkIfReorderQTYIsSameAsLastOrderQTY() throws Exception {
		assertTrue(showingAllOrdersPage.isReorderQTYSameAsLastOrderQTY());
	}

	@Then("I should see the qty is the same in cart")
	public void isQtySameInCart() {
		assertTrue(showingAllOrdersPage.isQtySameInCart());
	}

	@When("I select the first order history record")
	public void selectTheFirstOrderHistoryRecord() throws Exception {
		showingAllOrdersPage.selectFirstRecord();
	}

	@When("I select the first order history record with a invalid number")
	public void selectRecordWithInvalidNumber() throws Exception {
		showingAllOrdersPage.selectInvalidFirstRecord();
	}

	@When("I view order history detail with a specific order id : \"$orderId\"")
	public void viewOrderHistoryDetailWithOrderId(String orderId) throws Exception {
		homePage = new HomePage();
		verifyOrderId = orderId;
		homePage.viewOrderDetailWithOrderId(orderId);
	}

	@Then("I should see navigate to Showing all items section")
	public void seeShowAllItemSection() {
		assertTrue(showingAllOrdersPage.seeShowAllItemSection());
	}

	@When("I request to reorder for the order \"$orderNum\"")
	public void iReorderOrder(String orderNum) throws Exception {
		showingAllOrdersPage = new ShowingAllOrdersPage();
		showingAllOrdersPage.reorderForSpecifiedOrder(orderNum);
	}

	@When("I search for open status order")
	public void iSearchForOpenStatusOrder() {
		searchOrderPage = new SearchOrderPage();
		searchOrderPage.searchOpenOrder();
	}

	@Then("I should see the result for orders")
	public void iShouldSeeTheResultFOrOrders() {
		assertTrue(searchOrderPage.isThereOpenStatusOrder());
	}

	@When("I navigate to one open status order and record the address")
	public void iNavigateToOneOpenStatusOrderAndRecordTheAddress() {

	}
}
