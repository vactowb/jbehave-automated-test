package com.officedepot.jbehave.www.steps.store;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.storelocator.AllStatesPage;
import com.officedepot.jbehave.www.pages.storelocator.FindAStorePage;
import com.officedepot.jbehave.www.pages.storelocator.LocalStorePage;
import com.officedepot.jbehave.www.pages.storelocator.StoreDetailsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class LocalStoreTestSteps extends BaseStep {

	private LocalStorePage localStorePage;
	private FindAStorePage findAStorePage;
	private AllStatesPage allStatesPage;
	private StoreDetailsPage storeDetailsPage;

	@When("I navigate to find a store page from local store bread scrumb")
	public void iNavigateToFindAStorePageFromLocalStoreBreadScrumb() throws Exception {
		localStorePage = new LocalStorePage();
		findAStorePage = localStorePage.clickOnBreadScrumbFindAStore();
	}

	@When("I navigate to all states page from local store bread scrumb")
	public void iNavigateToAllStatesPageFromLocalStoreBreadScrumb() throws Exception {
		localStorePage = new LocalStorePage();
		allStatesPage = localStorePage.clickOnBreadScrumbAllStates();
	}

	@Then("I should see all states page")
	public void iShouldSeeAllStatesPage() throws Exception {
		allStatesPage = new AllStatesPage();
		Assert.assertTrue("--- Failed to navigate on All States page! ---", allStatesPage.isInCurrentPage());
	}

	@When("I navigate to store details page from find another store link")
	public void iNavigateToStoreDetailsPageFromFindAnotherStoreLink() throws Exception {
		localStorePage = new LocalStorePage();
		storeDetailsPage = localStorePage.clickOnFindAnotherStoreLink();
	}

	@When("I navigate to store details page from search more stores")
	public void iNavigateToStoreDetailsPageFromSearchMoreStores() throws Exception {
		localStorePage = new LocalStorePage();
		findAStorePage = localStorePage.searchFindMoreStores();
	}
	
	@Then("I should see the store brand service")
	public void iShouldSeeTheStoreBrandService() throws Exception {
		localStorePage = new LocalStorePage();
		Assert.assertTrue("--- Failed to see the store brand service! ---", localStorePage.isStoreBrandServiceOccur());
	}

	@Then("I should see store manager's name")
	public void iShouldSeeTheStoreManagerName() throws Exception {
		localStorePage = new LocalStorePage();
		Assert.assertTrue("--- Failed to see the store manager's name! ---", localStorePage.isStoreManagerNameOccur());
	}
}
