package com.officedepot.jbehave.www.steps.cpd;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.www.pages.checkout.CheckoutBillingPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutReviewPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutShipPage;
import com.officedepot.jbehave.www.pages.checkout.CheckoutThankYouPage;
import com.officedepot.jbehave.www.pages.cpd.vendor.PrinternetPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class Ag2OrderSteps extends BaseStep {

	private ShoppingCartPage shoppingCartPage;
	private CheckoutThankYouPage thankYouPage;
	private CheckoutBillingPage checkoutBillingPage;
	private BasePage vendorPage;
	private String orderNumber;
	private CheckoutReviewPage checkOutReviewPage;

	@When("I checkout")
	public void iCheckout() throws Exception {

		boolean orderPlaced = false;
		String msg = "";
		shoppingCartPage = new ShoppingCartPage();
		this.shoppingCartPage.proceedCheckout();
		this.checkoutBillingPage = new CheckoutShipPage().continueCheckout();
		this.checkOutReviewPage = checkoutBillingPage.continueCheckoutAtThirdStep();
		this.thankYouPage = this.checkOutReviewPage.placeOrder();
		thankYouPage.closebrdialog();
		this.orderNumber = thankYouPage.getOrderNumberText();

		if (this.orderNumber.length() > 1)
			orderPlaced = true;
		msg = orderPlaced ? "order is placed, order number is : " + orderNumber.toString() : "order is not placed correctly";

		Assert.assertEquals(msg, orderPlaced, true);
		System.out.println("**" + msg + "**");
	}
	
	@When("I checkout as pickup")
	public void iCheckoutAsPickup() throws Exception {

		boolean orderPlaced = false;
		String msg = "";
		shoppingCartPage = new ShoppingCartPage();
		this.shoppingCartPage.clickPickupRadio("870284");
		this.shoppingCartPage.proceedCheckout();
		this.checkoutBillingPage = new CheckoutShipPage().continueCheckout();
		this.checkOutReviewPage = checkoutBillingPage.continueCheckoutAtThirdStep();
		this.thankYouPage = this.checkOutReviewPage.placeOrder();
		thankYouPage.closebrdialog();
		this.orderNumber = thankYouPage.getOrderNumberText();

		if (this.orderNumber.length() > 1)
			orderPlaced = true;
		msg = orderPlaced ? "order is placed, order number is : " + orderNumber.toString() : "order is not placed correctly";

		Assert.assertEquals(msg, orderPlaced, true);
		System.out.println("**" + msg + "**");
	}

	@Then("I see my order in associated Print Queue \"$printQUeue\"")
	public void iSeeMyOrderInAssociatedPrintQueue(String printQUeue) throws Exception {

		boolean orderFound = false;
		String msg = "";

		this.vendorPage = new PrinternetPage();
		((PrinternetPage) this.vendorPage).openPrinternetPage();

		try {
			orderFound = ((PrinternetPage) this.vendorPage).orderFound(orderNumber);
		} finally {
			msg = orderFound ? orderNumber + " order is in Printernet QUEUE" : orderNumber
					+ " order is not in Printernet QUEUE yet , checkback again, if it is not still there , ODWS might need a restart.";

			if(orderFound)Assert.assertEquals(msg, true, orderFound);
			else System.out.println("**" + msg + "**");
		}
	}

}
