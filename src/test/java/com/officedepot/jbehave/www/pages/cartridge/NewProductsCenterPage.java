package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class NewProductsCenterPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".brandColor_tp2.vspace_top.f_left");
	private WebElement matrixDealCenter;

	public NewProductsCenterPage openNewProductsCenterPage() throws Exception {
		openUrl("/a/browse/new-arrivals/N=5+549383/");
		return new NewProductsCenterPage();
	}

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "New Products Center");
	}

	public CategoriesPage navigateToCategoriesPageFromFeaturedCategoriesMatrixDealCenter() throws Exception {
		matrixDealCenter = this.findElementByCSS(".subheading_title");
		clickOn(matrixDealCenter);
		seleniumWaitForPageToLoad(30000);
		return new CategoriesPage();
	}

}