package com.officedepot.jbehave.www.pages.home;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class SaveUpPage  extends BasePage {

	private By pageVerifyItem = By.cssSelector(".row>a>img");

	public boolean isInCurrentPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}
}
