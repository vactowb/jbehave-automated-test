package com.officedepot.jbehave.www.pages.odws;

import junit.framework.Assert;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumTestContext;


/**
 * @author s-jattiprakash
 * NOTE: If these Tests fail in your local thats because the delay in loading labels, 
 * for that you have have to un-comment the "Thread.sleep(3000);" code to run successfully
 */
public class ReturnLabelTestPage extends  BasePage {
	
	private By searchFieldBy = By.xpath("/html/body/form[2]/input[1]");
	private By submitButton = By.xpath("/html/body/form[2]/input[3]");
	private By searchGenerateFieldBy = By.xpath("/html/body/form[1]/input[1]");
	private By submitGenerateButton = By.xpath("/html/body/form[1]/input[3]"); 

	public ReturnLabelTestPage openTestPage() {
	String currentURL = SeleniumTestContext.getInstance().getTargetBaseURL();
		if (currentURL.indexOf("sqs") != -1) {
			openSQSReturnLabelTestPage();
		} 
		else if(currentURL.indexOf("sqm") != -1) {
			openSQMReturnLabelTestPage();
		}
		else if(currentURL.indexOf("dev") != -1) {
			openDevReturnLabelTestPage();
		}
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage retirveUPSLabel(String orderId) throws Exception {
		typeTextBoxBy(searchFieldBy, orderId);
		clickBy(submitButton);
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage validateUPSLabelRetrived(){
		seleniumWaitForPageToLoad(30000);
		/*try {
			Thread.sleep(1500);
		} catch (InterruptedException e) { 
			System.out.println("Timeout occurred within the time specified "+e);
		}*/
		boolean isUPSLabel=findElementByXpath("/html/body/div[1]").getText().contains("UPS");
		Assert.assertTrue(isUPSLabel);
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage retirveODLabel(String orderId) throws Exception {
		typeTextBoxBy(searchFieldBy, orderId);
		clickBy(submitButton);
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage validateODLabelRetrived(){
		seleniumWaitForPageToLoad(30000);
		/*try {
			Thread.sleep(1500);
		} catch (InterruptedException e) { 
			System.out.println("Timeout occurred within the time specified "+e);
		}*/
		boolean isODLabel=findElementByXpath("/html/body/div[1]").getText().contains("Office Depot");
		Assert.assertTrue(isODLabel);
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage generateUPSLabel(String orderId) throws Exception {
		typeTextBoxBy(searchGenerateFieldBy, orderId);
		clickBy(submitGenerateButton);
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage validateUPSLabelGenerated(){
		seleniumWaitForPageToLoad(30000);
		/*try {
			Thread.sleep(1500);
		} catch (InterruptedException e) { 
			System.out.println("Timeout occurred within the time specified "+e);
		}*/
		boolean isUPSLabel=findElementByXpath("/html/body/div[1]").getText().contains("UPS");
		Assert.assertTrue(isUPSLabel);
		return new ReturnLabelTestPage();
	}
	
	public ReturnLabelTestPage generateODLabel(String orderId) throws Exception {
		typeTextBoxBy(searchGenerateFieldBy, orderId);
		clickBy(submitGenerateButton);
		return new ReturnLabelTestPage();
	}
	
	private void openSQSReturnLabelTestPage() {
		openUrl("http://odwssqs.uschecomrnd.net/ODWebServicesWeb/label/labelTest.jsp");
		seleniumWaitForPageToLoad(10000);
	}
	private void openSQMReturnLabelTestPage() {
		openUrl("http://odwssqm.uschecomrnd.net/ODWebServicesWeb/label/labelTest.jsp");
		seleniumWaitForPageToLoad(10000);
	}
	private void openDevReturnLabelTestPage() {
		openUrl("http://odwsdevtrunk.uschecomrnd.net/ODWebServicesWeb/label/labelTest.jsp");
		seleniumWaitForPageToLoad(10000);
	}
	

}


