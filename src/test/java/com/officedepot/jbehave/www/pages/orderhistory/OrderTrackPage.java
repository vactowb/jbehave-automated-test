package com.officedepot.jbehave.www.pages.orderhistory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class OrderTrackPage extends BasePage {
	
	private By orderNumberBox = By.name("orderNo");	
	private By phoneNumber1_2 = By.id("phoneNumber1-0");
	private By phoneNumber2_2 = By.id("phoneNumber2-0");
	private By phoneNumber3_2 = By.id("phoneNumber3-0");
	private By trackOrderButton = By.xpath("//input[@title='Track Order'][@value='Track Order']");
	
	public void inputAllInfo2TrackOrder(String orderNumber) {
		
		typeTextBoxBy(orderNumberBox, orderNumber);
		typeTextBoxBy(phoneNumber1_2, "559");
		typeTextBoxBy(phoneNumber2_2, "999");
		typeTextBoxBy(phoneNumber3_2, "9999");
		
		clickBy(trackOrderButton);
	}
	
	public void deleteCookies() {
		deleteAllCookies();
	}
}
