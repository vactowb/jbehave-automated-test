package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class AllInOneComputersCategoriesPage extends BasePage {
	private By pageVerifyItem = By.cssSelector("#skuList");

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "All-In-One Computers");
	}
}
