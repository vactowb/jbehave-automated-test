package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ShopByGradePage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".brandColor_sp2.vspace_top.f_left");
	private By featuredCategoriesMatrix = By.cssSelector("#categoryHead");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Shop By Grade");
	}

}
