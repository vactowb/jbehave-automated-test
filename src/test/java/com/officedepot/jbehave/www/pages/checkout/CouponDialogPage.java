package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CouponDialogPage extends BasePage {

	private WebElement closeLink;
	private WebElement seeOfferDetailsButton;
	private WebElement removeCouponLink;
	private WebElement couponDescribeLink;
	private By couponDetailsLink;

	public boolean isCouponAddedSuccessMessageShowUpInDialog() throws Exception {
		return findElementByCSS("#ui-dialog-title-dialog>p").getText().equals("Coupon Code Applied");
	}

	public void closeCouponDialog() throws Exception {
		closeLink = findElementByCSS(".ui-dialog-titlebar-close.ui-corner-all");
		clickOn(closeLink);
	}

	public boolean isCouponAddedToCart(String couponNumber) {
		couponDetailsLink = By.cssSelector(".coupon.without-border>a");
		return isTextPresentInElement(couponDetailsLink, couponNumber);
	}

	public CartRemovedCouponPage removeCurrentCoupon() {
		removeCouponLink = findElementByCSS(".iconstyle2");
		clickOn(removeCouponLink);
		seleniumWaitForPageToLoad(10000);
		return new CartRemovedCouponPage();
	}

	public CouponAddedToCartPage navigateToCouponAddedToCartPage() {
		seeOfferDetailsButton = findElementByLinkText("SEE OFFER DETAILS");
		clickOn(seeOfferDetailsButton);
		return new CouponAddedToCartPage();
	}

	public CouponDetailsPage navigateToCouponDetailsPage() {
		if(isElementPresentBySelenium("link=SEE OFFER DETAILS")&& selenium.isVisible("link=SEE OFFER DETAILS")){
		seeOfferDetailsButton = findElementByLinkText("SEE OFFER DETAILS");
		}
		else{
		seeOfferDetailsButton = findElementByLinkText("SHOP NOW");
		}
		clickOn(seeOfferDetailsButton);
		return new CouponDetailsPage();
	}

	public CouponAddedToCartPage navigateToCouponDetailsPageByCouponDescribeLink() {
		setCookie("coupon1BABTest", "true");
		couponDescribeLink = findElementByCSS(".coupon.without-border>a");
		clickOn(couponDescribeLink);
		this.seleniumWaitForPageToLoad(100000);
		return new CouponAddedToCartPage();
	}

}
