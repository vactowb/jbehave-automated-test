package com.officedepot.jbehave.www.pages.emailsignup;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class PrivacyStatementPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Privacy Statement");
	}
}
