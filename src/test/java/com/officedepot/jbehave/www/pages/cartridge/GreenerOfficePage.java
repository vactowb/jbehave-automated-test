package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class GreenerOfficePage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#categoryHead");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "GreenerOffice");
	}
}
