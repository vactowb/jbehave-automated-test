package com.officedepot.jbehave.www.pages.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;

public class CouponDetailsPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#couponInfo>h1");
	private By errorMessage = By.cssSelector(".promo_add_msg");
	private List<WebElement> selectButton;
	private WebElement shopNowButton;
	private WebElement addToCartButton;
	private WebElement bottomAddToCartButton;
	private List<WebElement> productAddToCartButton;
	private By needSelectMessage;

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Coupon Details");
	}

	public ShoppingCartPage clickOnBottomAddToCartButton() throws Exception {
		bottomAddToCartButton = findElementById("promoSubmit_bottom");
		clickOn(bottomAddToCartButton);
		seleniumWaitForPageToLoad(30000);
		return new ShoppingCartPage();
	}

	public boolean isErrorMessageOccur() throws Exception {
		return isElementPresent(errorMessage);
	}

	public void selectTheFirstItem() throws Exception {
		selectButton = findElementsByCSS(".button");
		clickOn(selectButton.get(2));
	}
	
	public CouponDetailsPage selectEnoughItems() throws Exception {
		selectButton = findElementsByCSS(".button");
		clickOn(selectButton.get(3));
		clickOn(selectButton.get(4));
		return new CouponDetailsPage();
	}

	public HomePage clickOnShopNowButton() throws Exception {
		shopNowButton = findElementById("shopNow");
		clickOn(shopNowButton);
		return new HomePage();
	}

	public ShoppingCartPage clickOnTheFirstAddToCartButton() throws Exception {
		addToCartButton = findElementById("promoSubmit_bottom");
		clickOn(addToCartButton);
		return new ShoppingCartPage();
	}
	
	public CouponDetailsPage openType3AndType4CouponDetailsPage() {
		setCookie("coupon3ABTest", "true");
		openUrl("/promo/redir.do?adid=148790");
		return new CouponDetailsPage();
	}
	
	public CouponDetailsPage clickOnProductAddToCartButton() {
		productAddToCartButton = findElementsByCSS(".button.skip_popup");
		clickOn(productAddToCartButton.get(0));
		return new CouponDetailsPage();
	}
	
	public boolean isIndicateTheNecessaryNumberMessageAccor() {
		needSelectMessage = By.cssSelector(".promo_add_right.reminder_message");
		return isElementPresent(needSelectMessage);
	}
	
	public boolean isIndicateTheNecessaryNumberMessageDisappear() {
		return isElementPresentBySelenium("css=.promo_add_right.reminder_message");
	}


}