package com.officedepot.jbehave.www.pages.register;

import org.openqa.selenium.By;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.loyalty.JoinAndActiveRewardsPage;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BasePage;

public class ExpressRegisterPage extends BasePage {
	private By pageVerifyItem = By.cssSelector(".wide_title.flcl.w100>h1");

	private static String emailAddress = "";
	public static final String FIRSTNAME = "ODTEST";
	public static final String LASTNAME = "TESTER";
	public static final String ZIPCODE = "33496";
	public static final String PASSWORD = SeleniumUtilsWWW.PASSWORD;
	public static final String CONFIRMPASSWORD = SeleniumUtilsWWW.PASSWORD;
	public static final String SECURITYANSWER = "tester";
	public static final String MEMBERSHIPTYPE = "loyMembershipType";

	private By expRegisterTitleText = By.cssSelector(".ui-dialog-title");

	private By firstNameField = By.name("firstName");
	private By lastNameField = By.name("lastName");
	private By postalCodeField = By.name("postalCode");
	private By emailField = By.name("email");
	private By passwordField = By.id("password-xr");
	private By passwordConfirmField = By.name("passwordConfirm");
	private By questionSelect = By.id("question");
	private By answerField = By.name("answer");
	private By cmdRegistrationButton = By.name("cmd_registration");
	private By subscriptionCheckbox = By.id("optInODMarketingEmails");
	private By autoLoginCheckbox = By.id("sameAsBilling1");
	private By gigidLinkButton = By.xpath("//div[@gigid='facebook']");
	private By gigidHomeLink = By.id("homelink");

	private By registerNewAccount = By.id("createNewAccount_Modal");

	private By forgotLoyIdLink = By.id("forgotLoyId");
	private JoinAndActiveRewardsPage joinAndActivateRewardsPage;
	private By forgotLoyIdDialogErrors = By.id("forgotLoyIdDialogErrors");
	private By loyMemberIdField = By.id("loyMemberId");
	private By xRegLoyErrorContainer = By.cssSelector(".xRegLoyError");

	private By loyOptInCheckBox = By.id("loyOptIn");
	private By phoneError = JoinAndActiveRewardsPage.getLabelWebElement("loyPhoneNumber");
	private By membership_type = By.id(MEMBERSHIPTYPE);
	private By membership_type_error = JoinAndActiveRewardsPage.getLabelWebElement(MEMBERSHIPTYPE);

	private By phone1Field = By.id("loyPhone0");

	private By phone2Field = By.id("loyPhone1");

	private By phone3Field = By.id("loyPhone2");

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Registration");
	}

	public static String setEmailAddress() {
		emailAddress = "test" + System.currentTimeMillis() + "@tester.com";
		return emailAddress;
	}

	public void iInputAllInfor() {
		setEmailAddress();
		iInputInforWithEmail(emailAddress);
	}

	public void iInputInforWithEmail(String email) {
		iInputAllInfor(email, ZIPCODE, SECURITYANSWER, PASSWORD, CONFIRMPASSWORD);
	}

	public void iInputInforWithoutSubmit() {
		setEmailAddress();
		iInputAllInforBasic(emailAddress, ZIPCODE, SECURITYANSWER, PASSWORD, CONFIRMPASSWORD, "select");
	}

	public void iInputAllInforWithinvalidZipcode(String zipcode) {
		setEmailAddress();
		iInputAllInfor(emailAddress, zipcode, SECURITYANSWER, PASSWORD, CONFIRMPASSWORD);
	}

	public boolean registerWithError() {
		return findElementBy(expRegisterTitleText).getText().contains("New Customer Registration");
	}

	public boolean registerWithError(String message) {
		return isTextPresentOnPage(message);
	}

	public void iInputAllInforWithblankZipcode() {
		iInputAllInforWithinvalidZipcode(" ");
	}

	public void iputWrongConfirmPassword() {
		setEmailAddress();
		iInputAllInfor(emailAddress, ZIPCODE, SECURITYANSWER, PASSWORD, "testerwrong");
	}

	public void inputInvaildPassword() {
		setEmailAddress();
		iInputAllInfor(emailAddress, ZIPCODE, SECURITYANSWER, "tester", "tester");
	}

	public void inputInvaildPassword(String password) {
		setEmailAddress();
		iInputAllInfor(emailAddress, ZIPCODE, SECURITYANSWER, password, password);
	}

	public void iInputAllInfor(String emailAddress, String zipCode, String securityanswer, String pwd, String pwdConfirm) {
		iInputAllInforBasic(emailAddress, zipCode, securityanswer, pwd, pwdConfirm, "select");
		clickRegistrationButton();
	}

	public void iInputAllInforBasic(String emailAddress, String zipCode, String securityanswer, String pwd, String pwdConfirm, String securityQuestionSelect) {
		seleniumWaitForPageToLoad(1000);
		typeTextBoxBy(firstNameField, FIRSTNAME);
		typeTextBoxBy(lastNameField, LASTNAME);
		typeTextBoxBy(postalCodeField, zipCode);
		typeTextBoxBy(emailField, emailAddress);
		typeTextBoxBy(passwordField, pwd);
		typeTextBoxBy(passwordConfirmField, pwdConfirm);
		if ("null" != securityQuestionSelect)
			selectOptionInListByText(findElementBy(questionSelect), "What was your first job?");
		typeTextBoxBy(answerField, securityanswer);
	}

	public void clickRegistrationButton() {
		clickBy(cmdRegistrationButton);
	}

	public boolean selectPromotions() {
		clickBy(subscriptionCheckbox);
		boolean flag = findElementBy(subscriptionCheckbox).isSelected();
		iInputAllInfor();
		return flag;
	}

	public boolean selectAutoLogin() {
		clickBy(autoLoginCheckbox);
		boolean flag = findElementBy(autoLoginCheckbox).isSelected();
		iInputAllInfor();
		return flag;
	}

	public void iInputInforWithUnselectQuestion() {
		setEmailAddress();
		iInputAllInforBasic(emailAddress, ZIPCODE, SECURITYANSWER, PASSWORD, CONFIRMPASSWORD, "null");
		clickRegistrationButton();
	}

	public void clickGigya() {
		clickBy(gigidLinkButton);
	}

	public boolean checkGigya() throws Exception {
		focusOnNewWindow();
		return isElementPresent(gigidHomeLink);
	}

	public void iLogoutandRelogin() {
		new HomePage().logout();
		new PageHeader().login(emailAddress, PASSWORD);
	}

	public void registerFromLoginPage() {
		clickBy(registerNewAccount);
	}

	public void reLoginWithAuthorization() {
		new PageHeader().inputLogin(emailAddress, PASSWORD);
	}

	public void clickForgotLoyIdLink() {
		clickBy(forgotLoyIdLink);
	}

	public void inputMemberPhoneNum(String phoneNumber) {
		joinAndActivateRewardsPage = new JoinAndActiveRewardsPage();
		joinAndActivateRewardsPage.inputMemberPhoneNum(phoneNumber);
	}

	public boolean seeForgotLoyErrorMeg(String msg) {
		return isPageLoaded(forgotLoyIdDialogErrors, msg);
	}

	public boolean seeOdrMemberBeFound(String memberId) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		return selenium.getValue("xpath=//input[@name='loyMemberId']").equalsIgnoreCase(memberId);
	}

	public void inputOdrMemberId(String memberId) {
		typeTextBoxBy(loyMemberIdField, memberId);
		clickBy(emailField);
		clickBy(loyMemberIdField);
		clickBy(emailField);
	}

	public boolean seeForgotLoyErrorMsgMemberId() {
		return isElementPresent(xRegLoyErrorContainer);
	}

	public boolean seeForgotLoyErrorMsgMemberId(String msg) {
		return isTextPresentOnPage(msg);
	}

	public boolean seeLoyaltyOptinChkBox() {
		return findElementBy(loyOptInCheckBox).isDisplayed() && findElementBy(loyOptInCheckBox).isEnabled();
	}

	public void checkLoyaltyCheckbox() {
		if (!findElementBy(loyOptInCheckBox).isSelected()) {
			clickBy(loyOptInCheckBox);
		}
	}

	public void joinRewardsRegister(String phone, boolean isSelectMType) {
		if (phone != null) {
			String[] phones = phone.split("-");
			String phoneNumber0 = phones[0];
			String phoneNumber1 = phones[1];
			String phoneNumber2 = phones[2];
			typeTextBoxBy(phone1Field, phoneNumber0);
			typeTextBoxBy(phone2Field, phoneNumber1);
			typeTextBoxBy(phone3Field, phoneNumber2);
		}
		if (isSelectMType)
			selectOptionInListByText(findElementBy(membership_type), "Customer");
		clickBy(phone3Field);
	}

	public boolean isErrorMsgPhone(String registerrequirephone) {
		return isPageLoaded(phoneError, registerrequirephone);
	}

	public boolean isErrorMsgMemType(String registerrequirememtype) {
		return isPageLoaded(membership_type_error, registerrequirememtype);
	}
}
