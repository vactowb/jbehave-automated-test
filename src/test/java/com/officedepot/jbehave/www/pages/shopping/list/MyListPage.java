package com.officedepot.jbehave.www.pages.shopping.list;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class MyListPage extends BasePage {

	private WebElement cslListContainer;
	private List<WebElement> myListName;
	private List<WebElement> viewAndEdit;
	private boolean isListNameExist;
	
	public boolean isInCurrentPage() throws Exception {
		return findElementByCSS(".csl_opt_block.shopping") != null && findElementByCSS(".csl_opt_block.school") != null;
	}

	public CslListDetailPage viewOrEditCslList() throws Exception {
		cslListContainer = findElementById("shoppingList");
		clickOn(cslListContainer.findElements(By.cssSelector(".first")).get(0));
		return new CslListDetailPage();
	}
	
	public SchoolListDetailsPage clickOnViewEditMylist(String exceptedListName) throws Exception {
		myListName = findElementsByCSS(".td1>a");
		for(int i=0; i<myListName.size(); i++){
			if(myListName.get(i).getText().equalsIgnoreCase(exceptedListName)) {
				clickOn(myListName.get(i));
				break;
			}
		}
		return new SchoolListDetailsPage();
	}
	
	public ListDetailsPage clickOnViewEditLink() throws Exception {
		viewAndEdit = findElementsByCSS(".first");
		clickOn(viewAndEdit.get(0));
		return new ListDetailsPage();
	}
	
	public boolean isMylistAppearInList(String listName) throws Exception {
		myListName = findElementsByCSS(".td1>a");
		
		for(int i=0 ; i<myListName.size() ; i++){
			if(myListName.get(i).getText().equalsIgnoreCase(listName)){
				isListNameExist = false;
				break;
			}else{
				isListNameExist = true;
			}
		}
		return isListNameExist;
	
	}

}
