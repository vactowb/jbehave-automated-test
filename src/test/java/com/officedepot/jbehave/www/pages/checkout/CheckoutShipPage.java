package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BasePage;

public class CheckoutShipPage extends BasePage {

	private By firstName_2 = By.id("firstName-2");
	private By lastName_2 = By.id("lastName-2");
	private By address1_2 = By.id("address1-2");
	private By city_2 = By.id("city-2");
	private By state_2 = By.id("state-2");
	private By postalCode1_2 = By.id("postalCode1-2");
	private By phoneNumber1_2 = By.id("phoneNumber1-2");
	private By phoneNumber2_2 = By.id("phoneNumber2-2");
	private By phoneNumber3_2 = By.id("phoneNumber3-2");
	private By phoneNumber4_2 = By.id("phoneNumber4-2");
	private By email_2 = By.id("email-2");
	private By emailConfirm_2 = By.id("emailConfirm-2");
	private WebElement shipmentSummaryInfo;
	private By pageVerifyItem = By.cssSelector(".oCol1of2.sub_heading.first>h1");
	private By selectStoreAndAddToCartButton = By.id("selectStoreAndAddToCartButton");
	private By orderSummary = By.cssSelector("#summaryFinal");
	private By continueCheckoutButton = By.id("confirm2");
	private WebElement removeButton;

	private By address1_1 = By.id("address1-1");
	private By phoneNumber1_1 = By.id("phoneNumber1-1");
	private By phoneNumber2_1 = By.id("phoneNumber2-1");
	private By phoneNumber3_1 = By.id("phoneNumber3-1");
	private By phoneNumber4_1 = By.id("phoneNumber4-1");
	private By city_1 = By.id("city-1");
	private By state_1 = By.id("state-1");

	private By firstName_0 = By.id("firstName-0");
	private By lastName_0 = By.id("lastName-0");
	private By address1_0 = By.id("address1-0");
	private By city_0 = By.id("city-0");
	private By state_0 = By.id("state-0");
	private By postalCode1_0 = By.id("postalCode1-0");
	private By phoneNumber1_0 = By.id("phoneNumber1-0");
	private By phoneNumber2_0 = By.id("phoneNumber2-0");
	private By phoneNumber3_0 = By.id("phoneNumber3-0");
	private By phoneNumber4_0 = By.id("phoneNumber4-0");
	private By email_0 = By.id("email-0");
	private By emailConfirm_0 = By.id("emailConfirm-0");

	private By shipThisAddress = By.name("shipToSeq.INDEX[1]");

	private By changeAddressButton = By.name("cmd_shippingListDisplay");

	private By selectStoreButton = By.name("cmd_selectStore");
	private By choseStoreButton = By.name("cmd_something");
	private By zipCodeField = By.id("f_zip");
	private By changeLocationButton = By.id("changeLocation");
	private By isChangePickupMode = By.name("cmd_selectStore");
	private By shipping2ThisAddressButton = By.xpath("//input[@value='Add this Address']");
	private By continueButton = By.cssSelector("#continue.button");

	private By errorMessageAddressPop = By.id("addressLineOne");
	private By editShippingButton = By.id("editShipping");

	private By editShippingLink = By.xpath("//a[contains(text(),'Edit')]");
	private By saveAndCheckoutButton = By.xpath("//input[@value='Save and Checkout']");

	private By showPickUpChange = By.id("showPickUpChange");
	private By pickupFirstName = By.id("pickupFirstName");
	private By pickupLastName = By.id("pickupLastName");
	private By pickupEmail = By.id("pickupEmail");
	private By pickupSaveUserButton = By.id("pickupSaveUser");

	private By pickWhoField = By.id("pickupWho");
	private By selectStoreLightBox = By.cssSelector(".acc");

	public void removeSku(String sku, int index) {
		removeButton = findElementByName("cmd_removeItem.button.INDEX[" + index + "]");
		clickOn(removeButton);
	}

	public boolean isNavigateOnThisPage() {
		return isPageLoaded(pageVerifyItem, "Ship");
	}

	public CheckoutBillingPage continueCheckout() throws Exception {
		clickBy(continueCheckoutButton);
		return new CheckoutBillingPage();
	}

	public void continueAnonymousCheckoutAtStep2() throws Exception {
		inputAddressAtCheckoutStep2("Eric", "Wei", "5 TEJON ST", "DENVER", "CO - Colorado", "80223");
	}

	public void continueAnonymousCheckoutAtStep2WithInvalidAddress() {
		inputAddressAtCheckoutStep2("tester", "test", "13621 Carlton St.", "Mashpee", "MA - Massachusetts", "02649");
	}

	public void continueAnonymousCheckoutAtStep2WithInvalidPhoneNumber() {
		inputAddressAtCheckoutStepAll2("Eric", "Wei", "5 TEJON ST", "DENVER", "CO - Colorado", "80223", "", "", "9999", "test@test.com", "test@test.com");
	}

	public void inputAddressAtCheckoutStep2(String firstName, String lastName, String address, String city, String state, String postCode) {
		inputAddressAtCheckoutStepAll2(firstName, lastName, address, city, state, postCode, "559", "999", "9999", "test@test.com", "test@test.com");
	}

	public void inputAddressAtCheckoutStepAll2(String firstName, String lastName, String address, String city, String state, String postCode, String number1, String number2, String number3,
			String email, String retypeEmail) {
		typeTextBoxBy(firstName_2, firstName);
		typeTextBoxBy(lastName_2, lastName);
		typeTextBoxBy(address1_2, address);
		typeTextBoxBy(city_2, city);
		selectOptionInListByText(findElementBy(state_2), state);
		typeTextBoxBy(postalCode1_2, postCode);
		typeTextBoxBy(phoneNumber1_2, number1);
		typeTextBoxBy(phoneNumber2_2, number2);
		typeTextBoxBy(phoneNumber3_2, number3);
		typeTextBoxBy(phoneNumber4_2, "9999");
		typeTextBoxBy(email_2, email);
		typeTextBoxBy(emailConfirm_2, retypeEmail);
	}

	public void inputAddressAtCheckoutStep2ExpressReg(String address, String city, String state) {
		typeTextBoxBy(address1_1, address);
		typeTextBoxBy(city_1, city);
		selectOptionInListByText(findElementBy(state_1), state);
		typeTextBoxBy(phoneNumber1_1, "559");
		typeTextBoxBy(phoneNumber2_1, "999");
		typeTextBoxBy(phoneNumber3_1, "9999");
		typeTextBoxBy(phoneNumber4_1, "9999");
	}

	public boolean isShipmentSummaryExistedOnPage() throws Exception {
		shipmentSummaryInfo = findElementById("deliveryInfo iFull");
		return shipmentSummaryInfo != null;
	}

	public enum OrderDetailColumn {
		DESCRIPTION, PRICE, QTY, TOTAL
	}

	public String getProcessingFeeContent() {
		return findElementsByCSS("TD.td2.description").get(1).getText();
	}

	public String getOrderDetailContent(String columnName, int row, boolean isConfiguredItem) {
		OrderDetailColumn listColumn = OrderDetailColumn.valueOf(OrderDetailColumn.class, System.getProperty("listColumn", columnName).toUpperCase());
		switch (listColumn) {
		case DESCRIPTION:
			return getText(findElementsByCSS(".td2.description>a>strong").get(row)).replaceAll("\n", " ");
		case PRICE:
			return getText(findElementsByXpath("//*[@id='searchRefine']/td[4]").get(row));
		case QTY:
			if (isConfiguredItem) {
				return getText(findElementsByCSS(".configQuantity").get(row));
			} else {
				return getText(findElementsByCSS(".quantityField>input").get(row));
			}
		case TOTAL:
			return getText(findElementsByCSS(".td8.extended_price").get(row));
		default:
			return null;
		}
	}

	public boolean checkShippingOptions(String mode) {
		if (mode.equals("pickup")) {
			return isElementPresentBySelenium("name=cmd_selectStore");
		}
		return false;
	}

	// search the store use zipcode
	public void clickChangeStores() {
		clickBy(selectStoreButton);
		searchStore();
	}

	// return the search result by zipcode
	public boolean checkStoresSearchResult() {
		for (WebElement e : findElementsByCSS(".acc.open")) {
			if (!e.getText().contains("ENGLEWOOD")) {
				return false;
			}
		}
		return true;
	}

	public void clickSelectStore() {
		outOfStoreSelectStore();
	}

	public void outOfStoreSelectStore() {
		if (isElementPresent(By.cssSelector("#dialogStoreModal"))) {
			searchStore();
			new ShoppingCartPage().clickShowMoreLink();
			for (WebElement webElement : findElementsByCSS(".acc")) {
				if (webElement.getText().contains("1 of 1 Items in Stock")) {
					if (webElement.findElement(By.name("cmd_something")).isDisplayed()) {
						clickOn(webElement.findElement(By.name("cmd_something")));
						break;
					}
					clickOn(webElement);
					clickOn(webElement.findElement(choseStoreButton));
					break;
				}
			}
		}
	}

	public boolean checkStoreAddressChanged() {
		return findElementByCSS(".address").getText().contains("Your selected store:");
	}

	public boolean checkChangePickupMode() {
		return isElementPresent(isChangePickupMode);
	}

	public boolean checkPopStoreLocatorModal() {
		return isElementPresentBySelenium("id=dialogStoreModal");
	}

	private void searchStore() {
		typeTextBoxBy(zipCodeField, SeleniumUtilsWWW.COMMON_STORE);
		clickBy(changeLocationButton);
		// Wait for result
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public FindYourProductPage selectStoreAndAddToCartButton() {
		searchStore();
		new ShoppingCartPage().clickShowMoreLink();
		for (WebElement webElement : findElementsBy(selectStoreLightBox)) {
			if (webElement.getText().contains("2 of 2 Items in Stock") || webElement.getText().contains("1 of 1 Items in Stock")) {
				if (webElement.findElement(selectStoreAndAddToCartButton).isDisplayed()) {
					clickOn(webElement.findElement(selectStoreAndAddToCartButton));
					break;
				}
				clickOn(webElement);
				clickOn(webElement.findElement(selectStoreAndAddToCartButton));
				break;
			}
		}

		return new FindYourProductPage();
	}

	public boolean shoulSeeAvailableBackorderOrderSummary() {
		// To-do
		try {
			isTextPresentInElement(orderSummary, "Backorder");
		} catch (Exception e) {
			return true;
		}
		return false;
	}

	public boolean isPromptedWithErrorInvalidAddress() {
		Boolean flag = isElementPresentBySelenium("css=.addressVeriErrorUser");
		if (flag) {
			clickContinue();
			return true;
		}
		return false;
	}

	public boolean isPromptedWithErrorInvalidBillingAddress() {
		return isElementPresentBySelenium("css=.error_message");
	}

	public void changeAddressCheckoutPage2() {
		clickBy(changeAddressButton);
	}

	public boolean seeChangeShippingInformationpage() {
		return isElementPresentBySelenium("id=firstName-0");
	}

	public void shipping2ThisAddress() {
		clickBy(shipping2ThisAddressButton);
	}

	public boolean checkErrorMessagePopForExpressReg() {
		return isElementPresent(By.cssSelector(".addressVeriErrorMessage"));
	}

	public boolean checkErrorAddressPopForExpressReg(String errorMessage) {
		return findElementBy(errorMessageAddressPop).getText().contains(errorMessage);
	}

	public void clickEditShippingButton() {
		clickBy(editShippingButton);
	}

	public void clickEditShippingLink() {
		clickBy(editShippingLink);
	}

	public void clickContinue() {
		clickBy(continueButton);
	}

	public void addOtherShippingAddress2Checkout(String firstName, String lastName, String address, String city, String state) {
		typeTextBoxBy(firstName_0, firstName);
		typeTextBoxBy(lastName_0, lastName);
		typeTextBoxBy(address1_0, address);
		typeTextBoxBy(city_0, city);
		selectOptionInListByText(findElementBy(state_0), state);
		typeTextBoxBy(postalCode1_0, "33496");
		typeTextBoxBy(phoneNumber1_0, "559");
		typeTextBoxBy(phoneNumber2_0, "999");
		typeTextBoxBy(phoneNumber3_0, "9999");
		typeTextBoxBy(phoneNumber4_0, "9999");
		typeTextBoxBy(email_0, "test@t.com");
		typeTextBoxBy(emailConfirm_0, "test@t.com");
	}

	public boolean checkpromptedErrorInvalidPhoneNumber() {
		return findElementBy(phoneNumber1_2).getAttribute("value").isEmpty() && findElementBy(phoneNumber2_2).getAttribute("value").isEmpty()
				&& findElementBy(phoneNumber3_2).getAttribute("value").contains("9999");
	}

	public void shipThisAddress() {
		clickBy(shipThisAddress);
	}

	public void clickSaveAndCheckoutButton() {
		clickBy(saveAndCheckoutButton);
	}

	public boolean isReauthPaypalAccountMsgShow() {
		return findElementByCSS(".errorList").getText().contains("For security reasons we have cleared your PayPal and Office Depot Card information after the address changes");
	}

	public void changeToPickUpOrder() {
		clickBy(showPickUpChange);

		typeTextBoxBy(pickupEmail, "odtester@126.com");
		typeTextBoxBy(pickupLastName, "tester");
		typeTextBoxBy(pickupFirstName, "tester");
		clickBy(pickupSaveUserButton);
	}

	public Boolean pickupWho() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		return findElementBy(pickWhoField).getText().contains("odtester@126.com");
	}

	public boolean isReTypeEmailInvalid() {
		return isTextPresentOnPage("Re-Type Email: is invalid") && !findElementBy(emailConfirm_2).getAttribute("value").equals("test@test.com");
	}

	public void fillWithDiffRetypeEmail() {
		inputAddressAtCheckoutStepAll2("Eric", "Wei", "5 TEJON ST", "DENVER", "CO - Colorado", "80223", "559", "999", "9999", "test@test.com", "test1@test.com");

	}
}
