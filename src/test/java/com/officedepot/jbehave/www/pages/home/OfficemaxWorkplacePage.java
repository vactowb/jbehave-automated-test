package com.officedepot.jbehave.www.pages.home;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class OfficemaxWorkplacePage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".bsd_login_hdr_stacked>a>img");

	public boolean isInCurrentPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}
	
}