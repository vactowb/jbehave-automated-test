package com.officedepot.jbehave.www.pages.shopping.cart;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.checkout.CouponDialogPage;
import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BasePage;

public class ShoppingCartPage extends BasePage {

	private WebElement couponCodeBox;
	private WebElement addCouponButton;
	private WebElement addCouponContainer;
	private WebElement proceedCheckoutButton;
	private WebElement confirmForm;
	private WebElement radioOff;
	private WebElement radioOn;
	private WebElement payPalButton;
	private WebElement subscribeLink;

	private static String radioId;

	private By pageVerifyItem = By.cssSelector(".h28");
	private By storeVerifyItem;
	private By emptyVerifyItem;
	private WebElement frequencyInsubscibe;
	private WebElement updateSubscription;
	private WebElement inputSkuId;
	private WebElement addToCartButton;

	private By orderSummary = By.cssSelector(".sc_summary");
	private By backOrder = By.cssSelector(".bo_avail.section");

	private By removeItemLink = By.name("cmd_removeFromCart.buttons.index[0]");
	private By saveForLaterLink = By.name("cmd_saveForLaterCartEntry.buttons.index[0]");
	private By quantityInput = By.name("cartRow[0].quantity");
	private By updateButton = By.name("cmd_updateCartEntry.buttons.index[0]");

	private By errorConponText = By.className("textblock");
	private By zipCodeField = By.id("f_zip");
	private By changeLocationButton = By.id("changeLocation");

	private By orderByQty = By.name("entryFormList[0].qty");
	private By changeLocationLink = By.id("triggerStoreLookupModalMultipleSkus");

	public boolean isNavigateOnThisPage() {
		return isPageLoaded(pageVerifyItem, "Shopping Cart");
	}

	public boolean isCartEmpty() {
		emptyVerifyItem = By.cssSelector(".empty");
		return this.isTextPresentInElement(emptyVerifyItem, "Your shopping cart is empty.");
	}

	public boolean isSkuAddedToCart(String skuId) throws Exception {
		return isTextPresentOnPage(skuId);
	}

	public boolean isSkusAddedToCartSuccess() throws Exception {
		return findElementByCSS(".td1.photo") != null;
	}

	public boolean verifyChosenStore() throws Exception {
		storeVerifyItem = By.cssSelector("p#cartStoreAddress");
		return waitForVisibilityOfElementLocated(storeVerifyItem) != null;
	}

	public String getWarningMessage() throws Exception {
		return findElementByCSS(".header>h2").getText().trim();
	}

	public boolean isInShoppingCart() throws Exception {
		return findElementByCSS(".cartTitle").getText().contains("Shopping Cart");
	}

	public CouponDialogPage addCouponInCart(String couponCode) throws Exception {
		// removeSpecificCoupon(couponCode);
		couponCodeBox = findElementById("referralCode");
		typeTextBox(couponCodeBox, couponCode);
		addCouponButton = findElementById("addCouponButton");
		clickOn(addCouponButton);
		return new CouponDialogPage();
	}

	public boolean isCouponAddedInCart() throws Exception {
		return findElementByCSS(".smCartSummary.sc_summary.sc_coupons") != null;
	}

	public void proceedCheckout() throws Exception {
		confirmForm = findElementByName("confirmForm");
		proceedCheckoutButton = findChildElementByCSS(confirmForm, ".button");
		clickOn(proceedCheckoutButton);
	}

	public void removeSpecificCoupon(String couponCode) throws Exception {
		openUrl("/cart/removeCoupon.do?couponCode=" + couponCode);
	}

	public void clearShoppingCart() throws Exception {
		openUrl("/cart/emptyCartDisplay.do?emptyCartConfirmed=1");
	}

	public String getQtyInCart() throws Exception {
		return findElementById("cartItemTotalCount").getText().trim();
	}

	public enum ShoppingListColumn {
		NUMBER, FEE_DESCRIPTION, FEE_NUMBER
	}

	public String getShoppingListContent(String columnName) {
		ShoppingListColumn listColumn = ShoppingListColumn.valueOf(ShoppingListColumn.class, System.getProperty("listColumn", columnName).toUpperCase());
		switch (listColumn) {
		case NUMBER:
			return getText(findElementByXpath("//table[@class='products']/tbody[1]/tr[1]/td[2]/div[1]"));
		case FEE_DESCRIPTION:
			return getText(findElementByXpath("//table[@class='products']/tbody[1]/tr[1]/td[2]/div[2]/div[1]"));
		case FEE_NUMBER:
			return getText(findElementByXpath("//table[@class='products']/tbody[1]/tr[1]/td[2]/div[2]/div[2]"));
		default:
			return null;
		}

	}

	public void clickPayPalButton() throws Exception {

		((JavascriptExecutor) driverProvider.get()).executeScript("$('.b_paypal').hide();$('.a_paypal').show();");
		payPalButton = waitForPresenceOfElementsLocated(By.xpath("//a[contains(@href,'paypalInCart=T')]")).get(0);
		// Thread.sleep(3000);
		// clickOn(payPalButton);
		openUrl("/cart/checkout.do?paypalInCart=T");
	}

	public void clickSubscribeFromShoppingCart() {
		subscribeLink = findElementById("subscribe_update_skuListFormID_INDEX_0");
		clickOn(subscribeLink);
	}

	public void chooseFrequency(String frequency) {
		frequencyInsubscibe = findElementById("convertRecurrenceFrequency_0");
		selectOptionInListByText(frequencyInsubscibe, frequency);
	}

	public void clickUpdateFromSubscription() {
		updateSubscription = findElementById("convertSubscribeCartViewSubmitButton0");
		clickOn(updateSubscription);
	}

	public boolean itemShouldBeSubscribed() {
		return findElementByCSS(".subscribe_trigger").getText().contains("Subscription");
	}

	public FindYourProductPage addItemFromOrder(String sku) {
		inputSkuId = findElementByName("entryFormList[0].sku");
		addToCartButton = findElementByName("cmd_addToCart.button");
		typeTextBox(inputSkuId, sku);
		clickOn(addToCartButton);
		return new FindYourProductPage();

	}

	public void deleteShoppingItem() {
		clickBy(removeItemLink);
	}

	public boolean checkItemAddToCart(String sku) {
		for (WebElement ele : findElementsByCSS(".item_sku")) {
			if (ele.getText().contains(sku)) {
				return true;
			}
		}
		return false;
	}

	public boolean checkItemAddToCartBeSubscribed(String sku) {
		for (WebElement ele : findElementsByCSS(".item_sku")) {
			if (ele.getText().contains(sku)) {
				try {
					ele.findElement(By.cssSelector(".js_show.brandColor_sp2.pointer"));
					return true;
				} catch (Exception e) {
					return false;
				}
			}
		}
		return false;
	}

	public void addMixedMode() {
		setCookie("MX_MIXED_MODE_TEST", "MM_ENABLED");
	}

	public void addCookieCheckoutWithCouponError() {
		setCookie("AUTO_REMOVE_ERROR_COUPON_ENABLED", "true");
	}

	public boolean checkItemSkuAndMode(String sku, String mode) {
		for (WebElement ele : findElementsByCSS(".cartEntry")) {
			if ((ele.findElement(By.cssSelector(".item_sku")).getText().contains(sku))) {
				if (!ele.getText().contains(mode)) {
					return true;
				}
				break;
			}
		}
		return false;
	}

	public void clickPickupRadio(String sku) {
		for (WebElement ele : findElementsByCSS(".cartEntry")) {
			if (ele.getText().contains(sku)) {
				WebElement flag = ele.findElement(By.cssSelector(".radio_pickup.radio_off"));
				clickOn(flag);
				break;
			}
		}
	}

	public void clickDeliveryRadio(String sku) {
		for (WebElement ele : findElementsByCSS(".cartEntry")) {
			if (ele.getText().contains(sku)) {
				WebElement flag = ele.findElement(By.cssSelector(".radio_off"));
				clickOn(flag);
				break;
			}
		}
	}

	public void clickYestoAllEligibleItems() {
		clickOn(findElementById("js_pickUpCart"));
		outOfStoreSelectStore();
	}

	public void clickNotoAllEligibleItems() {
		if (findElementById("js_pickUpSingle").isDisplayed()) {
			clickOn(findElementById("js_pickUpSingle"));
			outOfStoreSelectStore();
		}
	}

	public void outOfStoreSelectStore() {
		if (isElementPresent(By.cssSelector("#dialogStoreModal"))) {
			typeTextBoxBy(zipCodeField, SeleniumUtilsWWW.COMMON_STORE);
			clickBy(changeLocationButton);
			// Wait for result
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			for (WebElement webElement : findElementsByCSS(".acc")) {
				if (webElement.getText().contains("2 of 2 Items in Stock")) {
					if (webElement.findElement(By.name("cmd_something")).isDisplayed()) {
						clickOn(webElement.findElement(By.name("cmd_something")));
						break;
					}
					clickOn(webElement);
					clickOn(webElement.findElement(By.name("cmd_something")));
					break;
				}
			}
		}
	}

	public boolean seeSkuInpickupMode(String sku) throws Exception {
		for (WebElement ele : findElementsByCSS(".cartEntry")) {
			if (ele.getText().contains(sku)) {
				return ele.findElement(By.cssSelector(".radio_off")).getAttribute("title").contains("Pickup");
			}
		}
		return false;
	}

	public boolean shouldSeeMystoreFeatureIncart() {
		return isElementPresentBySelenium("id=cartStoreAddress");
	}

	public boolean shouldSeeEstimateShippingTaxesLink() {
		try {
			findChildElementByLinkText(findElementBy(orderSummary), "Estimate Shipping & Taxes");
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void clickEstimateShippingTaxesLink() {
		clickOn(findChildElementByLinkText(findElementBy(orderSummary), "Estimate Shipping & Taxes"));
	}

	public boolean shouldSeeAvailableAndBackorder() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		// return isTextPresentOnPage("Due to limited availability");
		return isTextPresentInElement(backOrder, "Available") && isTextPresentInElement(backOrder, "Backorder");
	}

	public boolean shouldSeeEstimatedSalesTax() {
		return isTextPresentInElement(orderSummary, "Estimated Sales Tax");
	}

	public boolean shouldSeeEstimatedShippingCharges() {
		return isTextPresentInElement(orderSummary, "Estimated Shipping Charges");
	}

	public boolean shouldSeeEstimatedShippingChargesFree() {
		return isTextPresentInElement(orderSummary, "FREE");
	}

	public boolean shouldSeeFreeDeliveryMsg() {
		return isElementPresentBySelenium("xpath=//a[contains(@href, '/a/static/misc/deliveryOptions/')]");
	}

	public boolean shouldNotSeeAvailableAndBackorder() {
		return isElementPresentBySelenium("css=bo_avail");
	}

	public boolean iSeeFreeDeliveryMessageDisplay() {
		return isTextPresentOnPage("FREE Delivery");
	}

	public void openShoppingCartPage() {
		openUrl("/cart/shoppingCart.do");
	}

	public void saveForLater() {
		clickBy(saveForLaterLink);
	}

	public boolean seeSaveForLaterList() {
		return isElementPresentBySelenium("id=destinationListId");
	}

	public boolean seeCouponError() {
		return findElementBy(errorConponText).getText().contains("$20 OFF $100 ORDER!");
	}

	public void goBack2ShoppingCart() {
		clickOn(findElementById("logo"));
		clickOn(findElementByCSS(".cart_icon"));
	}

	public void clickShowMoreLink() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (isElementPresentBySelenium("css=a.plus_reveal > span"))
			clickOn(findElementBy(By.cssSelector("a.plus_reveal > span")));
	}

	public boolean isMinQtyIncrementReq() {
		return isElementPresent(By.id("skuMinimumQtyDisplay")) && isElementPresentBySelenium("id=skuIncrementQtyDisplay");
	}

	public void updateSkuQty(String num) {
		typeTextBoxBy(quantityInput, num);
		clickBy(updateButton);
	}

	public boolean isMinQtyIncrementReqError() {
		return isTextPresentOnPage("Sorry. You have not met the minimum quantity");
	}

	public boolean theVendorItemIsAddedToCart(String number) {
		return isTextPresentOnPage(number);
	}

	public FindYourProductPage addItemFromOrder(String parseSkuIdByType, String num) {
		typeTextBoxBy(orderByQty, num);
		return addItemFromOrder(parseSkuIdByType);
	}

	public void changeLocationClk() {
		clickBy(changeLocationLink);
	}
}
