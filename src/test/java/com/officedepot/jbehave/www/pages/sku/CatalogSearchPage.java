package com.officedepot.jbehave.www.pages.sku;

import com.officedepot.test.jbehave.BasePage;

public class CatalogSearchPage extends BasePage {

	
	public String getCatologDescription() throws Exception {
		return (findElementByCSS(".lastBreadCrumb").getText());
	}
	
	public void SubscribeSku() throws Exception {
		clickOn(findElementsByXpath("//input[@value='Subscribe']").get(1));
	}
	
	public void addSkuToCart() throws Exception {
		clickOn(findElementByName("cmd_addSKU.button.INDEX[0]"));
	}
	
	public void inputQunity(String qty) throws Exception {
		typeTextBox(findElementById("quantityBox3"),qty);
	}
	
	

}
