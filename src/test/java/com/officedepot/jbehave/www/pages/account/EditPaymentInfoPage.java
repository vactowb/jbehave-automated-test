package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class EditPaymentInfoPage extends BasePage {

	private WebElement paymentMethod;
	private WebElement updateButton;

	public void setPaymentMethod(String payment) throws Exception {
		if (payment.equalsIgnoreCase("credit card")) {
			if (isElementPresentBySelenium("id=tenderRadioCR")) {
				paymentMethod = findElementById("tenderRadioCR");
				clickOn(paymentMethod);
			}
		} else if (isElementPresentBySelenium("id=tenderRadioPP")) {
			paymentMethod = findElementById("tenderRadioPP");
			clickOn(paymentMethod);
		}
	}

	public void updatePayment() throws Exception {
		updateButton = findElementByCSS("li > div.b1 > input.button");
		clickOn(updateButton);
	}

}
