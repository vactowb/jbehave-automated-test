package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CheckoutLoginPage extends BasePage {

	private WebElement continueAsGuestButton;

	public CheckoutShipPage continueAsGuest() throws Exception {
		continueAsGuestButton = findElementByXpath("//a[contains(text(),'Continue as Guest')]");
		clickOn(continueAsGuestButton);
		return new CheckoutShipPage();
	}

	public void createNewAccout() {
		clickOn(findElementById("createNewAccount_Modal"));
	}

}
