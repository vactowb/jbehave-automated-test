package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ComputerAndTabletsCategoriesPage extends BasePage{

	private By pageVerifyItem = By.cssSelector(".lastBreadCrumb.bold");
	private List<WebElement> refinement;

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Computers & Tablets");
	}
	
	public FilteredCategoriesByTheFirstTypePage filterTheComputerAndTabletsCategoriesByTheFirstType() throws Exception {
		refinement = findElementsByCSS(".refV2");
		clickOn(refinement.get(0));
		return new FilteredCategoriesByTheFirstTypePage();
	}
	
}
