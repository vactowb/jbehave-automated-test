package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SecurityQuestionsWindow extends BasePage {
	
	private WebElement notNowButton;
	private By verifyItem = By.cssSelector("#ui-id-1");

	public boolean isSecurityQuestionsWindowOccur() throws Exception {
		return isTextPresentInElement(verifyItem, "Security Question");
	}
	
	public void clickOnNotNowButton() throws Exception {
		notNowButton = findElementByXpath("//*[@id='sqformWrapper']/div/ul/li[1]/a");
		clickOn(notNowButton);
	}
	
	
}
