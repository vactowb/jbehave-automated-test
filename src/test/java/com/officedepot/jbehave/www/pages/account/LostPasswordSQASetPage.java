package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class LostPasswordSQASetPage extends BasePage {

	public void forgotLoginName(String firstName, String lastName, String email) {
		typeTextBox(findElementBy(By.id("firstName")), firstName.equals("null") ? "" : firstName);
		typeTextBox(findElementBy(By.id("lastName")), lastName.equals("null") ? "" : lastName);
		typeTextBox(findElementBy(By.id("emailAddress")), email.equals("null") ? "" : email);
		clickBy(By.name("cmd_forgotLogin"));
	}

	public boolean checkWarnMessage(String text) {
		return findElementBy(By.id("error")).getText().contains(text);
	}

	public boolean checkSuccessMessage(String text) {
		return findElementBy(By.cssSelector(".success")).getText().contains(text);
	}

	public boolean checkEmailInputboxBlank(String input) {
		return findElementBy(By.id("emailAddress")).getAttribute("value").equals(input);
	}
}
