package com.officedepot.jbehave.www.pages.home;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.account.AccountOverviewPage;
import com.officedepot.jbehave.www.pages.account.ExemptAccountDispalyPage;
import com.officedepot.jbehave.www.pages.account.ShippingListDisplayPage;
import com.officedepot.jbehave.www.pages.cartridge.CamerasAndCamcordersCategoryLandingPage;
import com.officedepot.jbehave.www.pages.cartridge.ComputerAndTabletsCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.InkjetSearchPage;
import com.officedepot.jbehave.www.pages.cartridge.NewTemplatePage;
import com.officedepot.jbehave.www.pages.cartridge.TapeAndAdhesivesPage;
import com.officedepot.jbehave.www.pages.deals.WeeklyAdPage;
import com.officedepot.jbehave.www.pages.emailsignup.EmailSignUpPage;
import com.officedepot.jbehave.www.pages.orderhistory.OrderDetailsPage;
import com.officedepot.jbehave.www.pages.orderhistory.ShowingAllOrdersPage;
import com.officedepot.jbehave.www.pages.others.ErrorPage;
import com.officedepot.jbehave.www.pages.search.SearchResultsListPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.pages.siteinfo.SiteMapPage;
import com.officedepot.jbehave.www.pages.sku.CatalogSearchPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.jbehave.www.pages.storelocator.ChooseStorePage;
import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumTestContext;

public class HomePage extends BasePage {

	private WebElement searchField;
	private WebElement searchButton;

	private By searchFieldBy = By.id("mainSearchField");
	private By searchButtonBy = By.xpath("//input[@value='Search']");
	private By myAccountLink = By.xpath("//ul[@id='accountList']/li/a");
	private WebElement categoryNavigator;
	private WebElement rewardsCloseButton;
	private String gigyaHeader = "css=#gigyaLoginDivHeader";
	private WebElement storeLocatorLink;
	private WebElement emailSignUpDontShowThisAgainLink;
	private String findAStore = "css=#searchStore";
	private String accountHeader = "css=#accountContent";
	private String pageVerifyItem = "//img[@alt='OfficeDepot.com - Office Supplies, Furniture, Technology & More!']";
	private By loginOrRegister = By.cssSelector(".accountLogin");
	private WebElement notMeLink;
	private By facebookIcon = By.xpath("//div[contains(@gigid, 'facebook')]");
	private By headerAccountLogin = By.id("headerAccountLogin");
	private WebElement siteMapLink;
	private WebElement navigatorLink;
	private WebElement weeklyAdGrayBarLink;
	private WebElement loginOrRegisterGrayBarLink;
	private WebElement theFirstLearnMoreLink;
	private WebElement selectManufacturerDropDown;
	private String brandName;
	private WebElement emailSignUpEmailButton;
	private WebElement removeEmailSignUpLink;

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium(pageVerifyItem);
	}

	public void logout() {
		openUrl("/account/signMeOut.do");
	}

	public HomePage openHomePage() {
		String currentURL = SeleniumTestContext.getInstance().getTargetBaseURL();
		if (currentURL.indexOf("wwwbeta") != -1 || currentURL.indexOf("sq") != -1) {
			openHomePageViaGetCookie();
		} else {
			openHomePageByDefault();
		}
		Assert.assertTrue("---Failed to navigate on OD home page!---", isNavigateOnThisPage());
		return new HomePage();
	}

	public void openHomePageByDefault() {
		openUrl("/");
	}

	public void openHomePageViaGetCookie() {
		openUrl("/iphone/");
		seleniumWaitForPageToLoad(10000);
		clickOn(findElementByLinkText("Click for Access Authorization"));
	}

	public void openCPDVendorAddToCartPage(String vendorString, String configId) {
		String tempAddToCartUrl = "/vendor/punchoutBack.do?vendor=" + vendorString + "&configid=" + configId;
		openUrl(tempAddToCartUrl);
	}

	public boolean getGigyaHeader() {
		return isElementPresentBySelenium(gigyaHeader);
	}

	public void homePageSetCookie(String cookie) {
		setCookie(cookie, "true");
	}

	public void closeRewardsDialog() {
		if (isElementPresentBySelenium("//a[@role='button']")) {
			rewardsCloseButton = findElementByXpath("//a[@role='button']");
			clickOn(rewardsCloseButton);
		}
	}

	public void emptyCart() {
		openUrl("/cart/emptyCartDisplay.do?emptyCartConfirmed=1");
		Assert.assertTrue("--- Failed to empty the cart!---", new ShoppingCartPage().isCartEmpty());
		// openUrl("/");
	}

	public SkuDetailPage searchBy(String searchItem) throws Exception {
		typeTextBoxBy(searchFieldBy, searchItem);
		clickBy(searchButtonBy);
		return new SkuDetailPage();
	}

	public ExemptAccountDispalyPage openExemptAccountPage() throws Exception {
		openUrl("/account/existingUSAExemptDisplay.do");
		return new ExemptAccountDispalyPage();
	}

	public AccountOverviewPage viewMyAccountDetail() throws Exception {
		viewNavigatorOnHeader("Account");
		clickBy(myAccountLink);
		seleniumWaitForPageToLoad(30000);
		return new AccountOverviewPage();
	}

	public void viewNavigatorOnHeader(String navigator) throws Exception {
		navigatorLink = findElementByLinkTextWait(navigator);
		mouseHoverOn(navigatorLink);
	}

	public void changeDeliveryMode(String mode) throws Exception {
		if (mode.toLowerCase().equals("pickup")) {
			clickOn(findElementByXpath("//input[@name='cmd_pickup']"));
		} else if (mode.toLowerCase().equals("delivery")) {
			clickOn(findElementByXpath("//input[@name='cmd_deliveryMode.button']"));
		}
	}

	public ShippingListDisplayPage addDuplicatedAddress() throws Exception {
		openUrl("/account/shippingListDisplay.do");
		return new ShippingListDisplayPage();
	}

	public CamerasAndCamcordersCategoryLandingPage openCategoryLandingPage(String category) throws Exception {
		categoryNavigator = findElementByXpath("//span[text()='" + category + "']");
		clickOn(categoryNavigator);
		return new CamerasAndCamcordersCategoryLandingPage();
	}

	public ChooseStorePage openStoreLocator() throws Exception {
		storeLocatorLink = findElementByCSS("#storeLocator > a.headingLink > strong");
		clickOn(storeLocatorLink);
		return new ChooseStorePage();
	}

	public void viewSubscriptionManager() throws Exception {
		openUrl("/orderhistory/subsManager.do");
	}

	public CatalogSearchPage searchDescritpion(String description) throws Exception {
		searchField = findElementById("mainSearchField");
		searchButton = findElementByCSS("div.b1 > input.button");
		typeTextBox(searchField, description);
		clickOn(searchButton);
		return new CatalogSearchPage();
	}

	public SearchResultsListPage searchTerm(String term) throws Exception {
		searchField = findElementById("mainSearchField");
		searchButton = findElementByCSS("div.b1 > input.button");
		typeTextBox(searchField, term);
		clickOn(searchButton);
		return new SearchResultsListPage();
	}

	public ShowingAllOrdersPage viewOrderHistroy() throws Exception {
		openUrl("/orderhistory/orderHistoryList.do");
		return new ShowingAllOrdersPage();
	}

	public OrderDetailsPage viewOrderDetailWithOrderId(String orderId) throws Exception {
		openUrl("/orderhistory/orderHistoryDetail.do?id=" + orderId);
		return new OrderDetailsPage();
	}

	public void turnOnNewHeader() {
		String currentURL = SeleniumTestContext.getInstance().getTargetBaseURL();
		openUrl(currentURL + "/?useNewHeader=true");
	}

	public void openStoreFlyOut() throws Exception {
		WebElement storeFlyOut = findElementByCSS("#headerStores > a.stores_icon");
		clickOn(storeFlyOut);
	}

	public boolean isStoreFlyOutWithFindAStoreShown() {
		return isElementPresentBySelenium(findAStore);
	}

	public boolean isEmailSignUpShow() {
		return isElementPresentBySelenium("id=headerEmailSignUp");
	}

	public void openAccountFlyOut() throws Exception {
		WebElement storeFlyOut = findElementByCSS("#headerAccount > a.account_icon");
		clickOn(storeFlyOut);
	}

	public boolean isAccountFlyOut() {
		return isElementPresentBySelenium(accountHeader);
	}

	public boolean isAccountLogOut() throws Exception {
		return this.isTextPresentInElement(loginOrRegister, "Login or Register");
	}

	public boolean isNotMeLinkOccur() throws Exception {
		return isElementPresentBySelenium("css=.small>a");
	}

	public void clickOnNotMeLink() throws Exception {
		notMeLink = findElementByLinkText("Not me?");
		clickOn(notMeLink);
	}

	public void clickOnEmailSignUpDontShowThisAgainLink() throws Exception {
		emailSignUpDontShowThisAgainLink = findElementById("removeEmailSignUp");
		clickOn(emailSignUpDontShowThisAgainLink);
	}

	public void mouseHoverAccountFlyOut() throws Exception {
		WebElement storeFlyOut = findElementByCSS("#headerAccount > a.account_icon");
		mouseHoverOn(storeFlyOut);
	}

	public boolean getSocialIconsUnderLogin() {
		return webDriver.findElement(facebookIcon).isDisplayed() && (webDriver.findElement(facebookIcon).getLocation().y > webDriver.findElement(headerAccountLogin).getLocation().y);
	}

	public AccountOverviewPage openMyAccountOverviewPage() {
		openUrl("/account/accountSummaryDisplay.do");
		return new AccountOverviewPage();
	}

	public ComputerAndTabletsCategoriesPage openComputerAndTabletsCategoriesPage() {
		openUrl("/a/browse/computers-and-tablets/N=5+592137/");
		return new ComputerAndTabletsCategoriesPage();
	}

	public void clickChickout() {
		openUrl("/cart/checkout.do");
	}

	public ErrorPage navigateTo404ErrorPageFromUrl() {
		openUrl("http://wwwsqs8.uschecomrnd.net/test");
		return new ErrorPage();
	}

	public InkjetSearchPage navigateToInkjetSearchPageFromUrl() {
		openUrl("http://wwwsqs8.officedepot.com/a/browse/inkjet-all-in-ones/N=5+521417/");
		return new InkjetSearchPage();
	}

	public NewTemplatePage navigateToNewTemplatePageFromUrl() {
		openUrl("/promo/list5.do?listtype=01&listId=20279&customerid=1");
		return new NewTemplatePage();
	}

	public SiteMapPage openSiteMapPage() {
		siteMapLink = findElementByLinkText("Site Map");
		clickOn(siteMapLink);
		return new SiteMapPage();
	}

	public WeeklyAdPage clickOnWeeklyAdGrayBar() throws Exception {
		weeklyAdGrayBarLink = findElementByCSS(".weeklyAdLink");
		clickOn(weeklyAdGrayBarLink);
		focusOnNewWindow();
		return new WeeklyAdPage();
	}

	public void clickOnGrayBarLoginAndRegister() throws Exception {
		loginOrRegisterGrayBarLink = findElementByLinkText("Login or Register");
		clickOn(loginOrRegisterGrayBarLink);
	}

	public EmailSignUpPage clickOnEmailSignUpEmailButton() throws Exception {
		emailSignUpEmailButton = findElementByCSS(".email>a>img");
		clickOn(emailSignUpEmailButton);
		return new EmailSignUpPage();
	}

	public SaveUpPage clickOnTheFirstLearnMoreLink() throws Exception {
		theFirstLearnMoreLink = findElementByCSS("#Home-HeroZone-Home_907_t");
		clickOn(theFirstLearnMoreLink);
		return new SaveUpPage();
	}

	public String getCurrentWindowHandle() throws Exception {
		return getWindowHandle();
	}

	public TapeAndAdhesivesPage openTapeAndAdhesivesPage() throws Exception {
		this.openUrl("http://wwwsqs8.uschecomrnd.net/a/browse/tape-and-adhesives/N=5+371557/");
		return new TapeAndAdhesivesPage();
	}

	public InkAndTonerPage selectTheFirstManufacturerBrand() {
		selectManufacturerDropDown = findElementById("inkTonerSelect");
		selectOptionInListByIndex(selectManufacturerDropDown, 1);
		seleniumWaitForPageToLoad(10000);
		return new InkAndTonerPage();
	}

	public String getTheFirstManufacturerBrand() {
		brandName = getTextOfOptionsInSelectList(selectManufacturerDropDown).get(1);
		return brandName;
	}

	public void removeEmailSignUpDialogBox() {
		removeEmailSignUpLink = findElementByCSS("#removeEmailSignUp");
		clickOn(removeEmailSignUpLink);
	}

}
