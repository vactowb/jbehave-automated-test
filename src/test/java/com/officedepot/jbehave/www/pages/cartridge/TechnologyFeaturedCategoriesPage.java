package com.officedepot.jbehave.www.pages.cartridge;

import com.officedepot.test.jbehave.BasePage;

public class TechnologyFeaturedCategoriesPage extends BasePage {

	private String pageVerifyItem = "css=.skuList_results";
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isElementPresentBySelenium(pageVerifyItem);
	}
	
}
