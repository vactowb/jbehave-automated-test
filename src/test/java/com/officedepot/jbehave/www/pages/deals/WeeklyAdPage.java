package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class WeeklyAdPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".officeDepotLogoB");

	public boolean isNavigateOnThisPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}

	public void goBackToPreWindow(String window) throws Exception {
		switchToMainWindow(window);
	}

}
