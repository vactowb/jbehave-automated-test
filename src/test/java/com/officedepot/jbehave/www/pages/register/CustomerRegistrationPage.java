package com.officedepot.jbehave.www.pages.register;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.account.SetSecurityQuestionPage;
import com.officedepot.jbehave.www.utils.PromptMessageTestData;
import com.officedepot.test.jbehave.BasePage;

public class CustomerRegistrationPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");
	private WebElement createAccountButton;
	private WebElement inputField;
	private String emailAddress;
	private By passwordError = By.xpath("//label[@class='error'][@for='password-xr']");

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Customer Registration");
	}

	public boolean isPasswordStrengthError() {
		return isPageLoaded(passwordError, PromptMessageTestData.loyaltyMiniReqPassword);
	}

	public boolean seeErrorMessage(String msg) {
		return isTextPresentOnPage(msg);
	}

	// ---------------------------------------------------old customer registration page-------------------------------------------------
	public boolean isErrorMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public SetSecurityQuestionPage registerWithAllValidInfo() {
		inputBillingInfo("33496", "");
		inputAccountInfo(emailAddress, "tester", "tester");
		submitToCreateAccount();
		return new SetSecurityQuestionPage();
	}

	public void registerWithBlankZipCode() {
		inputBillingInfo("", "");
		inputAccountInfo(emailAddress, "tester", "tester");
		submitToCreateAccount();
	}

	public void registerWithInvaildConfirmPassword() {
		inputBillingInfo("33496", "");
		inputAccountInfo(emailAddress, "teste", "123");
		submitToCreateAccount();
	}

	public void registerWithInvaildPasswords(String password) {
		inputBillingInfo("33496", "");
		inputAccountInfo(emailAddress, password, password);
		submitToCreateAccount();
	}

	public void registerWithInvaildZipCode(String zip) {
		inputBillingInfo(zip, "");
		inputAccountInfo(emailAddress, "tester", "tester");
		submitToCreateAccount();
	}

	public void registerWithInvaildEmail(String email) {
		inputBillingInfo("33496", email);
		inputAccountInfo(email, "tester", "tester");
		submitToCreateAccount();
	}

	public void inputBillingInfo(String zipCode, String email) {
		if (email.equals("")) {
			emailAddress = RandomStringUtils.randomAlphanumeric(10) + "@gmail.com";
		} else {
			emailAddress = email;
		}
		inputBillingInfoField("firstName-0", "Test");
		inputBillingInfoField("lastName-0", "Chen");
		inputBillingInfoField("address1-0", "6600 N MILITARY TRL");
		inputBillingInfoField("city-0", "BOCA RATON");
		inputBillingInfoField("state-0", "FL - Florida");
		if (!zipCode.equals("")) {
			inputBillingInfoField("postalCode1-0", zipCode);
		}
		inputBillingInfoField("phoneNumber1-0", "561");
		inputBillingInfoField("phoneNumber2-0", "487");
		inputBillingInfoField("phoneNumber3-0", "5300");
		inputBillingInfoField("email-0", emailAddress);
		inputBillingInfoField("emailConfirm-0", emailAddress);
	}

	public void inputAccountInfo(String email, String pwd, String pwdConfirm) {
		inputAccountInfoField("loginForm.loginName", email);
		inputAccountInfoField("loginForm.password", pwd);
		inputAccountInfoField("loginForm.passwordConfirm", pwdConfirm);
	}

	public void submitToCreateAccount() {
		createAccountButton = findElementById("createUser");
		clickOn(createAccountButton);
	}

	public void inputBillingInfoField(String id, String value) {
		inputField = findElementById(id);
		if (id.equals("state-0")) {
			selectOptionInListByText(inputField, value);
		} else {
			typeTextBox(inputField, value);
			if (id.indexOf("email") != -1) {
				inputField.sendKeys(Keys.TAB);
			}
		}
	}

	public void inputAccountInfoField(String name, String value) {
		inputField = findElementByName(name);
		typeTextBox(inputField, value);
	}

	public void inputAllReqInfoWithInvalidPwd() {

		typeTextBoxBy(By.id("email-xr"), "test145@gmail.com");
		typeTextBoxBy(By.id("firstName-xr"), "test");
		typeTextBoxBy(By.id("lastName-xr"), "test");
		typeTextBoxBy(By.id("postalCode1-xr"), "33433");

		// Invalid Password as it does not have Upper Case or digit
		typeTextBoxBy(By.id("password-xr"), "test");
		typeTextBoxBy(By.id("password2-xr"), "test");

		WebElement questionElement = findElementBy(By.id("question"));
		selectOptionInListByText(questionElement, "What is your pet's name?");

		typeTextBoxBy(By.id("answer"), "cat");

		WebElement submitBtn = findElementBy(By.name("cmd_registration"));
		clickOn(submitBtn);
	}

	public void createNewAccount() {
		WebElement newAccountButton = findElementBy(By.linkText("Create a New Account"));
		clickOn(newAccountButton);
	}

}
