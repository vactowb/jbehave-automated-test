package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ManageCreditCardsPage extends BasePage {

	private WebElement makeDefaultButton;
	private By pageVerifyItem = By.cssSelector(".multiuser");
	private By nickNameField = By.name("creditCardAlias");
	private By cardHolderNameField = By.name("creditCardHolderName");
	private By cardType = By.id("cardTypeId");
	private By creditCardNumber = By.name("creditCardNumber");
	private By month = By.id("monthId");
	private By year = By.id("yearId");
	private By address1_0 = By.id("address1-0");
	private By city_0 = By.id("city-0");
	private By state_0 = By.id("state-0");
	private By postalCode1_0 = By.id("postalCode1-0");
	private By addCardButton = By.xpath("//input[@value='ADD CARD']");
	private By cardNameList0 = By.id("dispCardName0");
	private By useBillingAddressCheckbox = By.name("useBillingAddress");

	private By deleteCard0 = By.xpath("//div[@id='deleteCard0']/a");
	private By cardEditButton0 = By.xpath("//div[@id='cardEditButton0']/a");
	private By creditCardHolderName = By.cssSelector(".creditCardHolderName");
	private By updateButton0 = By.cssSelector("#cardUpdate0 > input.button");

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Manage My Credit Cards");
	}

	public void makeDefaultCreditCard() throws Exception {
		if (!isDefaultCardSet()) {
			makeDefaultButton = findElementByXpath("//div[@id='cardView0']/div[3]/ul/li[1]/div/a");
			clickOn(makeDefaultButton);
		}
	}

	public boolean isDefaultCardSet() throws Exception {
		return isElementPresentBySelenium("css=.defaultCardHeader");
	}

	public void addNewCardFromMyAccountPage(String cardHolderName, String address, String city, String state, String zipCode) {
		typeTextBoxBy(nickNameField, "CR2");
		selectOptionInListByText(findElementBy(cardType), "MasterCard");
		typeTextBoxBy(creditCardNumber, "4111111111111111");
		selectOptionInListByText(findElementBy(month), "12");
		selectOptionInListByText(findElementBy(year), "2018");

		typeTextBoxBy(cardHolderNameField, cardHolderName);
		typeTextBoxBy(address1_0, address);
		typeTextBoxBy(city_0, city);
		selectOptionInListByText(findElementBy(state_0), state);
		typeTextBoxBy(postalCode1_0, zipCode);

		clickBy(addCardButton);
	}

	public void addODCardFromMyAccountPage() {
		if (!isElementPresentBySelenium("id=dispCardMask0") || !findElementById("dispCardMask0").getText().contains("3502")) {
			typeTextBoxBy(nickNameField, "CR2");
			selectOptionInListByText(findElementBy(cardType), "Office Depot/Office Max");
			typeTextBoxBy(creditCardNumber, "6011568404413502");
			typeTextBoxBy(cardHolderNameField, "cardHolderName");
			clickBy(useBillingAddressCheckbox);
			clickBy(addCardButton);
		}
	}

	public boolean isCreditCardSaveSucc() {
		return findElementBy(cardNameList0).getText().contains("ODAUTOMATIONTESTER");
	}

	public boolean isODCardSaveSucc() {
		return findElementById("dispCardMask0").getText().contains("3502");
	}

	public void clickDeleteCard() {
		clickBy(deleteCard0);
		acceptAlert("Are you sure you want to delete your Visa card");
	}

	public boolean shouldSeeCreditCardEditSucc(String cardHolderName) {
		return isPageLoaded(By.id("dispCardName0"), cardHolderName);
	}

	public void editCreditCardFromAccount(String holderName) {
		clickBy(cardEditButton0);
		typeTextBoxBy(creditCardHolderName, holderName);
		selectOptionInListByIndex(findElementByCSS(".creditCardExpMonth"), 4);
		selectOptionInListByIndex(findElementByCSS(".creditCardExpYear"), 7);

		clickBy(updateButton0);
	}

}
