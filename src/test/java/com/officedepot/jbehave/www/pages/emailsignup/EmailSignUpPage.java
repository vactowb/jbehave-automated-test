package com.officedepot.jbehave.www.pages.emailsignup;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class EmailSignUpPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".h28");

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Email Sign-up & Mobile Opt-in");
	}
}
