package com.officedepot.jbehave.www.pages.account;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class ExemptAccountDispalyPage extends BasePage {

	private WebElement pageVerifyItem;

	public ExemptAccountDispalyPage() {
		pageVerifyItem = findElementByCSS("div#siteBreadcrumb > span");
		assertTrue("---Failed to navigate on exempt account page!---", pageVerifyItem.getText().trim().equals("Customers with Exempt Account"));
	}

	public ExemptAccountDispalyPage openExemptAccountPage() {
		openUrl("/account/existingUSAExemptDisplay.do");
		return new ExemptAccountDispalyPage();
	}

	public AccountOverviewPage createNewExemptAccount(ExamplesTable data) {
		typeTextBox(findElementByXpath("//input[contains(@id,'billToId-0')]"), data.getRow(0).get("billToId"));
		typeTextBox(findElementByXpath("//input[contains(@id,'phoneNumber1-0')]"), data.getRow(0).get("Phone1"));
		typeTextBox(findElementByXpath("//input[contains(@id,'phoneNumber2-0')]"), data.getRow(0).get("Phone2"));
		typeTextBox(findElementByXpath("//input[contains(@id,'phoneNumber3-0')]"), data.getRow(0).get("Phone3"));
		typeTextBox(findElementByXpath("//input[@name='email']"), data.getRow(0).get("EMail"));
		typeTextBox(findElementByXpath("//input[contains(@id,'firstName-1')]"), data.getRow(0).get("firstName"));
		typeTextBox(findElementByXpath("//input[contains(@id,'lastName-1')]"), data.getRow(0).get("lastName"));
		typeTextBox(findElementByXpath("//input[contains(@id,'loginName-1')]"), SeleniumUtils.giveUniqueIdBefore(data.getRow(0).get("loginName")));
		typeTextBox(findElementByXpath("//input[contains(@id,'password-1')]"), data.getRow(0).get("passWord"));
		typeTextBox(findElementByXpath("//input[contains(@id,'passwordConfirm-1')]"), data.getRow(0).get("passWord"));
		clickOn(findElementByXpath("//input[@name='createAccountButton']"));
		return new AccountOverviewPage();
	}
}
