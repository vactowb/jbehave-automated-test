package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class BasicSuppliesCategoriesPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#categoryHead");
	private List<WebElement> parentCategory;
	private List<WebElement> rRZoneCartridge;
	private List<WebElement> navColumnCategories;
	private WebElement CustomerServiceLink;
	private WebElement chatNowLink;
	private WebElement greenerOfficeProductsLink;
	private WebElement officeDepotBrandProductsLink;
	private WebElement tapeAndAdhesivesLink;
	private String productName;

	public boolean isInCurrentPage() throws Exception {
//		return isTextPresentInElement(pageVerifyItem, "Basic Supplies");
		return true;
	}

	public CashBoxesCategoriesPage navigateToTheFirstFeaturedCategoriesPage() throws Exception {
		parentCategory = findElementsByCSS(".parent_category");
		clickOn(parentCategory.get(1));
		return new CashBoxesCategoriesPage();
	}

	public CashBoxesCategoriesPage navigateToTheFirstNavColumnCategoriesPage() throws Exception {
		navColumnCategories = findElementsByCSS(".cats>li>a");
		clickOn(navColumnCategories.get(0));
		return new CashBoxesCategoriesPage();
	}

	public CustomerServicePage navigateToTheCustomerServicePage() throws Exception {
		CustomerServiceLink = findElementByCSS(".square.chat_square>li>a");
		clickOn(CustomerServiceLink);
		return new CustomerServicePage();
	}

	public String getTheFirstRRZoneProductName() throws Exception {
		rRZoneCartridge = findElementsByCSS(".rr_item_name>a");
		productName = rRZoneCartridge.get(0).getText();
		return productName;
	}

	public ProductDetailsPage navigateToTheFirstRRZoneProductDetailsPage() throws Exception {
		rRZoneCartridge = findElementsByCSS(".rr_item_name>a");
		clickOn(rRZoneCartridge.get(0));
		return new ProductDetailsPage();
	}

	public ChatNowWindow openChatNowWindow() throws Exception {
		chatNowLink = findElementByCSS(".chatLink");
		clickOn(chatNowLink);
		return new ChatNowWindow();
	}

	public PrivateBrandPage navigateToThePrivateBrandPage() throws Exception {
		officeDepotBrandProductsLink = findElementByLinkTextWait("Office Depot Brand Products");
		clickOn(officeDepotBrandProductsLink);
		return new PrivateBrandPage();
	}

	public GreenerOfficePage navigateToTheGreenerOfficePage() throws Exception {
		greenerOfficeProductsLink = findElementByLinkTextWait("GreenerOffice Products");
		clickOn(greenerOfficeProductsLink);
		return new GreenerOfficePage();
	}

	public TapeAndAdhesivesPage navigateToTheTapeAndAdhesivesPage() throws Exception {
		tapeAndAdhesivesLink = findElementByLinkText("Tape & Adhesives (1168)");
		clickOn(tapeAndAdhesivesLink);
		return new TapeAndAdhesivesPage();
	}

}
