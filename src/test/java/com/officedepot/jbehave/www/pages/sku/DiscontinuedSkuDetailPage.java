package com.officedepot.jbehave.www.pages.sku;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class DiscontinuedSkuDetailPage  extends BasePage {
	
	private By pageVerifyItem = By.cssSelector(".no_longer_avail.fleft");
	private List<WebElement> productAlternate;
	
	public boolean isInCurrentPage() {
		return isPageLoaded(pageVerifyItem, "This item is no longer available");
	}
	
	public SkuDetailPage clickOnFirstProductAlternate() {
		productAlternate = findElementsByCSS(".rr_item_image");
		clickOn(productAlternate.get(0));
		return new SkuDetailPage();
	}
	
}
