package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class DeleteListConfirmPage extends BasePage{
	
	private By deleteListTitle = By.cssSelector(".span1");
	private WebElement deleteListButton;
	
	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(deleteListTitle, "Delete a List");
	}
	
	public void deleteListButton() throws Exception {
		deleteListButton = this.findElementByXpath("//*[@id='m02']/div/form/div/ul[2]/li[2]/div/input");
		clickOn(deleteListButton);
	}
}
