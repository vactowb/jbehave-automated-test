package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class LoginSettingPage extends BasePage {
	private By oldPassWordTextBox = By.name("oldPassword");
	private By newPassWordTextBox = By.name("password");
	private By confirmNewPassword = By.name("passwordConfirm");
	private WebElement securityQuestionSelect;
	private By answerTextBox = By.name("answer");
	private By answer2WordTextBox = By.name("answer2");
	private By updateButton = By.id("checkEmailError");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(By.cssSelector("#pagetitle>h1"), "Login");
	}

	public void changeLoginSetting(String password) throws Exception {
		changeLoginSettingWithNewPassword(password, password);
	}

	public void changeLoginSettingInvalidNewPassword(String oldPassword, String newPassword) throws Exception {
		changeLoginSettingWithNewPassword(oldPassword, newPassword);
		clickBy(updateButton);
	}

	public AccountOverviewPage updateSecurityQuestion(String answer) throws Exception {
		securityQuestionSelect = findElementByXpath("//*[@id='editSecurityQuestion']/div/div/ol/li[1]/span/select");
		selectOptionInListByIndex(securityQuestionSelect, 2);
		typeTextBoxBy(answerTextBox, answer);
		typeTextBoxBy(answer2WordTextBox, answer);
		clickBy(updateButton);
		return new AccountOverviewPage();
	}

	private void changeLoginSettingWithNewPassword(String oldPassword, String newPassword) throws Exception {
		typeTextBoxBy(oldPassWordTextBox, oldPassword);
		typeTextBoxBy(newPassWordTextBox, newPassword);
		typeTextBoxBy(confirmNewPassword, newPassword);
	}
}
