package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.deals.ComputersAndTabletsCategoriesLandingPage;
import com.officedepot.test.jbehave.BasePage;

public class CategoriesPage extends BasePage {
	private By pageVerifyItem = By.cssSelector("#categoryHead");
	private List<WebElement> shopAllComputersAndTabletsLink;
	private By categoryHead = By.id("categoryHead");

	public boolean isNavigateOnThisPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}

	public boolean isNavigateOnThisPage(String theFirstCategoriesName) throws Exception {
		return isTextPresentInElement(categoryHead, theFirstCategoriesName);
	}

	public ComputersAndTabletsCategoriesLandingPage navigateToTheComputersAndTabletsCategoriesLandingPage() throws Exception {
		shopAllComputersAndTabletsLink = findElementsByCSS(".f_right.shopall");
		clickOn(shopAllComputersAndTabletsLink.get(0));
		return new ComputersAndTabletsCategoriesLandingPage();
	}

}
