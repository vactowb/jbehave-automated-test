package com.officedepot.jbehave.www.pages.shopping;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;

public class FindYourProductPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#siteBreadcrumb>span");
	private By shoppingCartButton = By.cssSelector("a.button");
	private By addToCartButton = By.name("updateAll");
	private By continueToCart = By.xpath("//a[contains(text(),'Continue to cart')]");
	private By backOrderForm = By.cssSelector("validation_style");

	public boolean isNavigateOnThisPage() {
		if (isElementPresentBySelenium("id=itemContainer")) {
			return isPageLoaded(By.id("itemContainer"), "This item has been added");
		}
		return isPageLoaded(pageVerifyItem, "Find Your Product");
	}

	public ShoppingCartPage viewShoppingCart() {

		if (isElementPresentBySelenium("name=updateAll")) {
			clickAddToCart();
		} else if (isElementVisibleBySelenium("css=a.button")) {
			clickBy(shoppingCartButton);
		}

		if (isElementPresentBySelenium("link=Continue to cart")) {
			selenium.click("link=Continue to cart");
		}
		return new ShoppingCartPage();
	}

	// *********************out of stock backorder begin************************************
	public boolean isOutOfStockMessageShowUp() throws Exception {
		// isTextPresentOnPage("Temporarily out of stock") ||
		// return isElementPresentBySelenium("class=validation_style");
		return isTextPresentOnPage("Due to limited availability");
	}

	public void clickAddToCart() {
		clickBy(addToCartButton);
	}

	public void clickNoThanks() {
		clickBy(continueToCart);
		// openUrl("/cart/shoppingCart.do");
	}

	// *********************out of stock backorder end****************************************

	public boolean verifyPriceInProductPage() {
		WebElement el = findElementByCSS(".hproduct .price");
		Double pricefound = Double.parseDouble(el.getText().substring(1, el.getText().indexOf("/")).trim());
		boolean priceNotZero = (pricefound > 0);
		return priceNotZero;
	}

	public boolean reorderItemsSuccess(int i) {
		List<WebElement> lists = findElementById("addedItems").findElements(By.cssSelector(".item_sku"));
		String skus = "781386" + "315515";
		if (lists.size() != i)
			return false;
		for (WebElement item : lists) {
			if (!skus.contains(item.getText().split("#")[1].trim()))
				return false;
		}
		return true;
	}
}