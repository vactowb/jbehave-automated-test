package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CouponPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#categoryHead");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "File Cabinets");
	}

}