package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.account.AccountOverviewPage;
import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class TppcEditListPage extends BasePage {

	private WebElement listName;
	private WebElement saveChangesButton;
	private WebElement previewButton;
	private String filedListName;
	private WebElement myAccountLink;
	private WebElement viewAllListLink;

	public boolean isShareListContainerExists() {
		return findElementByCSS(".csl_share_list.full") != null;
	}

	public boolean isInAddItemToListPage() {
		return findElementByCSS(".moduleStructContent.printHide.modStyle0") != null;
	}

	public void saveChanges() {
		listName = findElementByName("listName");
		saveChangesButton = findElementByName("cmd_createList");
		filedListName = SeleniumUtils.giveUniqueIdAfter("BTS").substring(0, 9);
		typeTextBox(listName, filedListName);
		clickOn(saveChangesButton);
	}

	public ViewSchoolListPage previewTheList() {
		previewButton = findElementByName("cmd_preview");
		clickOn(previewButton);
		return new ViewSchoolListPage();
	}

	public String getListName() {
		return filedListName;
	}
	
	public AccountOverviewPage clickOnMyAccountButton() {
		myAccountLink = findElementByLinkText("My Account");
		clickOn(myAccountLink);
		return new AccountOverviewPage();
	}
	
	public MyListPage clickOnViewAllListLink() {
		viewAllListLink = findElementByLinkText("View All Lists");
		clickOn(viewAllListLink);
		seleniumWaitForPageToLoad(30000);
		return new MyListPage();
	}
	
}
