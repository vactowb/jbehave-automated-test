package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class AddYourPriceWindow extends BasePage {
	
	private By pageVerifyItem = By.cssSelector("#ui-id-1");
	private WebElement keepInMyCartButton;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "You've Added");
	}
	
	public ConsiderRelatedItemsPage clickOnKeepInMyCartButton() throws Exception {
		keepInMyCartButton = findElementByCSS("#map_addtocart");
		clickOn(keepInMyCartButton);
		return new ConsiderRelatedItemsPage();
	}
	
}
