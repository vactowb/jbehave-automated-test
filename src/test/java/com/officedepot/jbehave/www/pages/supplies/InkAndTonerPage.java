package com.officedepot.jbehave.www.pages.supplies;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.cartridge.InkAndTonerResultPage;
import com.officedepot.test.jbehave.BasePage;

public class InkAndTonerPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#siteBreadcrumb>span");
	private WebElement selectPrinterBrandDropDown;
	private WebElement selectPrinterModelDropDown;
	private WebElement selectPrinterTypeDropDown;
	private WebElement savePrinterLink;
	private WebElement renamePrinterTextBox;
	private WebElement saveThisPrinterButton;
	private WebElement savedPrinterEditLink;
	private List<WebElement> deletePrinterButton;
	private List<WebElement> savedPrinterNameList;
	private List<WebElement> modelResultLink;
	private By mySavedPrintersDropDown;
	private By savePrinterWindow;
	private By duplicatePrinterNameMessage;
	private By managePrintersWindow;
	private By brandId = By.cssSelector("#brandId");
	private String prepopulatedName;
	private String theFirstSavedName;
	private String theFirstBrandName;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Ink & Toner");
	}

	public boolean isSelectedProductsDisplayed() throws Exception {
		return isElementPresent(By.cssSelector(".photo.flcl>a>img"));
	}

	public void selectTheFirstPrinterBrand() {
		selectPrinterBrandDropDown = findElementByCSS("#manufSelect");
		selectOptionInListByIndex(selectPrinterBrandDropDown, 1);
		seleniumWaitForPageToLoad(10000);
	}

	public String getTheFirstPrinterBrandName() {
		selectPrinterBrandDropDown = findElementByCSS("#manufSelect");
		theFirstBrandName = getTextOfOptionsInSelectList(selectPrinterBrandDropDown).get(1);
		return theFirstBrandName;
	}

	public void selectTheFirstPrinterType() {
		selectPrinterTypeDropDown = findElementByCSS("#typeSelect");
		selectOptionInListByIndex(selectPrinterTypeDropDown, 1);
		waitForAjaxCall(5000);
	}

	public void selectTheFirstPrinterModel() {
		selectPrinterModelDropDown = findElementByCSS("#modelSelect");
		selectOptionInListByIndex(selectPrinterModelDropDown, 1);
		waitForAjaxCall(5000);
	}

	public boolean isBrandBefoundSelectByBrand(String brand) {
		return isTextPresentInElement(brandId, brand);
	}

	public boolean isBrandBefoundSelectByModel() {
		return isElementPresent(By.cssSelector(".photo.flcl"));
	}

	public void choseOneBrandGoSkuDetailPage() {
		clickOn(findElementByLinkText("ABM"));
		clickOn(findElementByLinkText("7200"));
	}

	public String isSeeSkuDetailPageForProductDetails() {
		return findElementById("skuHeading").findElement(By.cssSelector(".item_sku")).getText();
	}

	public void clickOnSavePrinterLink() {
		savePrinterLink = findElementById("savePrinterLink");
		clickOn(savePrinterLink);
	}

	public void clickOnTheEditLink() {
		savedPrinterEditLink = findElementById("EditSavedButton");
		clickOn(savedPrinterEditLink);
	}

	public void clickOnTheFirstPrinterDelete() {
		deletePrinterButton = findElementsByCSS(".removeLink");
		clickOn(deletePrinterButton.get(0));
	}

	public boolean isSaveThePrinterNameWindowOccur() {
		savePrinterWindow = By.cssSelector("#savePrinterContent");
		return isElementPresent(savePrinterWindow);
	}

	public boolean isManagePrintersWindowOccur() {
		managePrintersWindow = By.cssSelector("#userPrinterList>ul");
		return isElementPresent(managePrintersWindow);
	}

	public boolean isFirstPrinterNameBeenDeleted() {
		if (isTextPresentInElement(savePrinterWindow, theFirstSavedName)) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isDuplicatePrinterNameMessageOccur(String msg) {
		duplicatePrinterNameMessage = By.cssSelector("#duplicateNameMsg");
		return isTextPresentInElement(duplicatePrinterNameMessage, msg);
	}

	public String getPrepopulatedNameFromSavePrinterNameWindow() {
		renamePrinterTextBox = findElementById("newPrinterName");
		prepopulatedName = getValue(renamePrinterTextBox);
		return prepopulatedName;
	}

	public String getTheFirstSavedPrinterName() {
		savedPrinterNameList = findElementsByCSS(".printerNameLink");
		theFirstSavedName = savedPrinterNameList.get(0).getText();
		return theFirstSavedName;
	}

	public void renameThePrinterNameInSavePrinterTextBox(String newPrinterName) {
		renamePrinterTextBox = findElementById("newPrinterName");
		typeTextBox(renamePrinterTextBox, newPrinterName);
		saveThisPrinterButton = findElementByCSS("#addPrinterSubmit");
		clickOn(saveThisPrinterButton);
	}

	public boolean isSavedNameInMySavedPrinters(String newPrinterName) {
		mySavedPrintersDropDown = By.cssSelector("#printersSelect");
		return isTextPresentInElement(mySavedPrintersDropDown, newPrinterName);
	}
	
	public InkAndTonerResultPage clickOnModelResultLink() {
		modelResultLink = findElementsByCSS(".liststyle4>li>a");
		clickOn(modelResultLink.get(100));
		return new InkAndTonerResultPage();
	}

}
