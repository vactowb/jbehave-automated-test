package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ViewSchoolListPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".span1");
	private WebElement goBackToEditLink;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "View School List");
	}
	
	public TppcEditListPage goBackToEditList() {
		goBackToEditLink = findElementsByCSS(".flcl.printHide>a").get(0);
		clickOn(goBackToEditLink);
		return new TppcEditListPage();
	}

}
