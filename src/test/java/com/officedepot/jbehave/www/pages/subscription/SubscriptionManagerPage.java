package com.officedepot.jbehave.www.pages.subscription;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class SubscriptionManagerPage  extends BasePage {

	private By pageVerifyItem = By.cssSelector(".h1");
	
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Subscriptions Manager");
	}
	public boolean checkPageTitle() throws Exception {
		return isPageLoaded(pageVerifyItem, "Subscriptions Manager");
	}
	
	public void navigateToNextPage() throws Exception {
		clickOn(findElementByXpath("id('subscription_Results')/ul/li/div/ul/li/a"));
	}
	
	public boolean displayFirstLink() throws Exception {
		pageVerifyItem = By.xpath("id('subscription_Results')/ul/li/div/ul/li[1]/a");
		return isPageLoaded(pageVerifyItem, "First");
	}
	
	public void navigateToPrevPage() throws Exception {
		clickOn(findElementByXpath("id('subscription_Results')/ul/li/div/ul/li[2]/a"));
	}
	
	public boolean displayPrevLink() throws Exception {
		return !isTextPresentOnPage("Prev");
	}
	
	
	public void navigateToFirstPage() throws Exception {
		clickOn(findElementByXpath("id('subscription_Results')/ul/li/div/ul/li[1]/a"));
	}
	
	public void setFilterConditionForFrequency(String condition) throws Exception {
		selectOptionInListByText(findElementById("frequencyFilter"), condition);
		clickOn(findElementById("statusFilterButton"));
	}
	
	public String getFrequencyConditionValue() throws Exception {
		return findElementByXpath("id('frequencyFilter')/option[@selected='']").getText().trim();
	}
}
