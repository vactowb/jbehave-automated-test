package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CouponCenterPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".brandColor_tp2.vspace_top.f_left>span");
	private String couponPageTitle;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Coupon Savings");
	}

	public boolean isTitleDifferentWithDealCenter(String dealCenterTitle) throws Exception {
		couponPageTitle = findElementByCSS(".brandColor_tp2.vspace_top.f_left>span").getText();
		return couponPageTitle.equals(dealCenterTitle);
	}

}
