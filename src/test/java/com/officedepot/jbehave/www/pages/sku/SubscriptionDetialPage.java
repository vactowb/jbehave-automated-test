package com.officedepot.jbehave.www.pages.sku;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SubscriptionDetialPage extends BasePage {
	
	private WebElement subscriptionQty;


	public boolean isOpen() throws Exception {
		return isPageLoaded(By.cssSelector(".section.vspace_top>h1"), "Subscription Details");
	}
	
	public void addSubscriptionToCart() throws Exception {
		clickOn(findElementById("addSubsToCartButton"));
	}
	
	public void chooseFrequency(String type) throws Exception {
		selectOptionInListByText(findElementById("recurrenceFrequency_0"), type);
	}
	
	public String getSubscriptionQty(String qty) throws Exception {
		subscriptionQty = findElementByCSS("table.products > tbody > tr.hproduct > td.quantity");
		return this.getText(subscriptionQty);
	}
	
	public void navigateToShoppingCart() throws Exception {
		clickOn(findElementByLinkText("SHOPPING CART"));
	}
}
