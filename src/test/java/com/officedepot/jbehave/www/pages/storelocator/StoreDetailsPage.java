package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class StoreDetailsPage extends BasePage{
	
	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");
	
	public boolean isInCurrentPage() throws Exception{
		return isTextPresentInElement(pageVerifyItem, "Store Details");
	}
}
