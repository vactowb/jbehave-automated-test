package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.deals.CouponCenterPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.test.jbehave.BasePage;

public class CouponExpiredPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".coupon_expired");
	private WebElement dealsCenterButton;
	private WebElement onlineCouponButton;
	private WebElement carouselTitleLink;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "has expired");
	}

	public CouponExpiredPage openNewCouponExpiredPage() {
		openUrl("/promo/redir.do?adid=148744");
		return new CouponExpiredPage();
	}

	public DealCenterPage clickOnDealCenterButton() {
		dealsCenterButton = findElementByLinkText("DEALS CENTER");
		clickOn(dealsCenterButton);
		return new DealCenterPage();
	}

	public CouponCenterPage clickOnOnlineCouponButton() {
		onlineCouponButton = findElementByLinkText("ONLINE COUPONS");
		clickOn(onlineCouponButton);
		return new CouponCenterPage();
	}

	public DealCenterPage clickOnCarouselTitleLink() {
		carouselTitleLink = findElementById("dealcenter_title");
		clickOn(carouselTitleLink);
		return new DealCenterPage();
	}
}
