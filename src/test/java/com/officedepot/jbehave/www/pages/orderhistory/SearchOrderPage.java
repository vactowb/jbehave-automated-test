package com.officedepot.jbehave.www.pages.orderhistory;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class SearchOrderPage extends BasePage {

	private By orderStatusSelector = By.id("cmb-orderstatus");
	private By searchButton = By.id("searchButtonV2");

	public void searchOpenOrder() {
		selectOptionInListByIndex(findElementBy(orderStatusSelector), 1);
		clickBy(searchButton);
	}

	public boolean isThereOpenStatusOrder() {
		return findElementsBy(By.cssSelector("tbody tr")).size() > 2;
	}

}
