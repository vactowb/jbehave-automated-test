package com.officedepot.jbehave.www.pages.search;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ProductComparisonPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#content>form>div>h1");
	
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Product Comparison");
	}
	
}
