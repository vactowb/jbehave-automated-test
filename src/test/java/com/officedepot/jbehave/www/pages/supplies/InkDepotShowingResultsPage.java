package com.officedepot.jbehave.www.pages.supplies;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class InkDepotShowingResultsPage extends BasePage {
	private By inkDepot;
	
	public boolean isInCurrentPage(){
		inkDepot = By.cssSelector(".lastBreadCrumb.bold");
		return isTextPresentInElement(inkDepot, "Ink Depot");
	}
	
}
