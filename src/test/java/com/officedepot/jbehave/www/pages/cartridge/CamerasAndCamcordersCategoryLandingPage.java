package  com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CamerasAndCamcordersCategoryLandingPage extends BasePage {

	private By pageVerifyItem;
	
	public boolean isNavigateOnThisPage() {
		pageVerifyItem = By.cssSelector(".lastBreadCrumb");
		return this.isElementPresent(pageVerifyItem);
	}
}
