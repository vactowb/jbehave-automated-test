package com.officedepot.jbehave.www.pages.checkout;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import com.officedepot.test.jbehave.BasePage;

public class PayPalPage extends BasePage {

   private By pageVerifyItem = By.id("header");
	
	public PayPalPage() {
		Assert.assertTrue("---Failed to navigate on PayPalPage!---", isPageLoaded(pageVerifyItem, "Ancin Peter's Test Store"));
	}

	public void loginPaypalPage(String account, String password) {
		typeTextBox(findElementById("login_email"), account);
		typeTextBox(findElementById("login_password"), password);
		clickOn(findElementById("submitLogin"));
	}
	
	public CheckoutReviewPage clickPaypalContinue() {
		clickOn(findElementById("continue"));
		return new CheckoutReviewPage();
	}
}
