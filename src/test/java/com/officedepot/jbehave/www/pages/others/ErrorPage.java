package com.officedepot.jbehave.www.pages.others;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BasePage;


public class ErrorPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".brand_title1");
	private WebElement shopNowButton;

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "The page you are trying to access is no longer available.");
	}
	
	public HomePage clickOnShopNowButton() throws Exception {
		shopNowButton = findElementByCSS(".btn.arrow.vspace_top");
		clickOn(shopNowButton);
		return new HomePage();
	}
}