package com.officedepot.jbehave.www.pages.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CheckoutReviewPage extends BasePage {

	private List<WebElement> changePaymentButtons;
	private WebElement changePaymentMethodButton;
	private By addCouponLink = By.cssSelector("p > input[name='cmd_step3']");
	private By addCardLink = By.name("cmd_changePayment");
	private By checkboxsaveMybillingInfor = By.name("paymentFormInfo.storePayPal");

	private By changePayment = By.xpath("//div[@id='paymentInner']/div[2]/div/input");

	private By placeOrderButton = By.id("confirm");
	private By orderSummaryPrice = By.id("cartSummarySubtotal");
	private By changeShipInfo = By.name("cmd_shippingListDisplay");

	private By enterMemberNum = By.xpath("//input[@value='Enter Member Number']");
	private By applyMem = By.cssSelector("#loyaltyApplyButtonContainer>a.btn");

	public void changePaymentMenthod() throws Exception {
		changePaymentButtons = waitForPresenceOfElementsLocated(By.name("cmd_changePayment"));
		changePaymentMethodButton = changePaymentButtons.get(0);
		clickOn(changePaymentMethodButton);
	}

	public CheckoutThankYouPage placeOrder() throws Exception {
		clickBy(placeOrderButton);
		return new CheckoutThankYouPage();
	}

	public boolean isPayPalThePaymentMethod() throws Exception {
		return findElementById("billingSummaryPayPal") != null;
	}

	public CheckoutBillingPage addCoupon() throws Exception {
		clickBy(addCouponLink);
		return new CheckoutBillingPage();
	}

	// Redeem a Gift, Merchandise or Reward Card
	public CheckoutBillingPage addCard() throws Exception {
		clickBy(addCardLink);
		return new CheckoutBillingPage();
	}

	public boolean isCurrentPageCheckoutStepFourPage() {
		return findElementById("h02").getText().contains("Checkout: Review");
	}

	public boolean unselectCheckboxSaveMyBillingInfor() {
		if (findElementBy(checkboxsaveMybillingInfor).isSelected()) {
			clickBy(checkboxsaveMybillingInfor);
		}
		return findElementBy(checkboxsaveMybillingInfor).isSelected();
	}

	public boolean selectCheckboxSaveMyBillingInfor() {
		if (!findElementBy(checkboxsaveMybillingInfor).isSelected()) {
			clickBy(checkboxsaveMybillingInfor);
		}
		return findElementBy(checkboxsaveMybillingInfor).isSelected();
	}

	public boolean checkPaymentMethod(String payment) {
		return findElementById("paymentInner").getText().contains(payment);
	}

	public CheckoutBillingPage changePayment() {
		clickBy(changePayment);
		return new CheckoutBillingPage();
	}

	public String orderSummaryPrice() {
		return findElementBy(orderSummaryPrice).getText().trim();
	}

	public CheckoutShipPage changeShipInfo() {
		clickBy(changeShipInfo);
		return new CheckoutShipPage();
	}

	public void enterMemberNum() {
		clickBy(enterMemberNum);
	}

	public void enterApplyMem() {
		clickBy(applyMem);
	}
}
