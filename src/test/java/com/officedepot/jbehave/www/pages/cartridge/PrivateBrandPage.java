package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class PrivateBrandPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#categoryHead>span");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Featured Categories");
	}
}
