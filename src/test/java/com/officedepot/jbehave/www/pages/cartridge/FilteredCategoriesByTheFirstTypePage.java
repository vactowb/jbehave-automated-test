package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.sku.SkuDetailsPage;
import com.officedepot.test.jbehave.BasePage;

public class FilteredCategoriesByTheFirstTypePage extends BasePage{

		private By pageVerifyItem = By.cssSelector(".skuList_results");
		private WebElement xMark;
		private List<WebElement> refinement;
		private List<WebElement> theFirstRichRelevanceSku;

		public boolean isInCurrentPage() throws Exception {
			return isTextPresentInElement(pageVerifyItem, "Results");
		}
		
		public ComputerAndTabletsCategoriesPage cancelCurrnetFilterByXMark() throws Exception {
			xMark = findElementByCSS(".refV2.checked>span");
			clickOn(xMark);
			return new ComputerAndTabletsCategoriesPage();
		}
		
		public FilteredCategoriesByTheFirstBrandPage filterTheComputerAndTabletsCategoriesByTheFirstBrand() throws Exception {
			refinement = findElementsByCSS(".refV2");
			clickOn(refinement.get(1));
			return new FilteredCategoriesByTheFirstBrandPage();
		}
		
		public SkuDetailsPage clickOnTheFirstRichRelevanceSku() throws Exception {
			theFirstRichRelevanceSku = findElementsByCSS(".rr_item_image");
			clickOn(theFirstRichRelevanceSku.get(6));                        
			return new SkuDetailsPage();
		}
		
}
