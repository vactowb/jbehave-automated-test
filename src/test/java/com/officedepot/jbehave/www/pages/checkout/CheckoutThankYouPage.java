package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CheckoutThankYouPage extends BasePage {

	private By orderSummaryPrice = By.id("cartSummarySubtotal");
	private WebElement thankYouTitileContainer;
	private By customerPOInfo = By.id("custPO");

	public boolean isSuccessForCheckout() throws Exception {
		return isPageLoaded(By.tagName("h1"), "Thank you for your order!");
	}

	public String orderSummaryPrice() {
		return findElementBy(orderSummaryPrice).getText().trim();
	}

	public boolean isInThankYouPage() throws Exception {
		thankYouTitileContainer = findElementById("thankyou_page");
		return thankYouTitileContainer != null;
	}

	public String shouldSeeOrderDetails() {
		return findElementByCSS(".f_right.normal>strong").getText();
	}

	public String getOrderNumberText() throws Exception {
		WebElement orderNumber = findElementsBy(By.cssSelector(".nowrap")).get(0);
		return orderNumber.getText().trim().replace("Order Number: ", "");
	}

	public void closebrdialog() throws Exception {
		Thread.sleep(1000);
		if (isElementPresentBySelenium("id=OMXUserNewAccountSignUpTrigger")) {
			clickBy(By.id("OMXUserNewAccountSignUpTrigger"));
		}
		if (isElementPresentBySelenium("css=.brdialog-close")) {
			clickOn(findElementByCSS(".brdialog-close"));
		} else {
			String mainWindow = webDriver.getWindowHandle();
			focusOnNewWindow();
			webDriver.close();
			webDriver.switchTo().window(mainWindow);
		}
	}

	public Boolean pickupWho() {
		if (isElementPresentBySelenium("css=#storePickUpInfo"))
			return findElementByCSS("#storePickUpInfo").getText().contains("odtester@126.com");
		return findElementByCSS("#pickupWho").getText().contains("odtester@126.com");
	}

	public boolean isSeeCustomerPONumber() {
		return findElementBy(customerPOInfo).getText().contains("Customer PO#");
	}

	public boolean seeOdrNumber(String num) {
		return isTextPresentOnPage("Office Depot� | OfficeMax� Rewards Member # " + num) || isTextPresentOnPage(num);
	}

}
