package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CouponAddedToCartPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");
	private WebElement shopNowButton;

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Coupon Added to Cart");
	}

	public CouponPage navigateToCouponPage() throws Exception {
		shopNowButton = findElementById("qualifierButton");
		clickOn(shopNowButton);
		seleniumWaitForPageToLoad(30000);
		return new CouponPage();
	}

}