package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CouponSavingsPage extends BasePage{
	private By pageVerifyItem = By.cssSelector(".brandColor_tp2.vspace_top");
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Coupon Savings");
	}
	
}
