package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ComputersAndTabletsCategoriesLandingPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".gradient_brand>a");
	private WebElement categoryHeadingSection;
	private WebElement theFirstComputersCategorySection;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Computers & Tablets");
	}

	public AllInOneComputersCategoriesPage navigateToTheAllInOneComputersCategoriesPage() throws Exception {
		categoryHeadingSection = findElementByCSS(".gradient_brand>a");
		mouseHoverOn(categoryHeadingSection);
		theFirstComputersCategorySection = findElementByLinkText("All-In-One Computers");
		clickOn(theFirstComputersCategorySection);
		return new AllInOneComputersCategoriesPage();
	}

}