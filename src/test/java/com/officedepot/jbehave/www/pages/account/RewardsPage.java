package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class RewardsPage extends BasePage {

	private By mergeAccountVerifyDialog = By.id("verify_info");

	public boolean isMergeAccountVerifyDialogAppear() {
		return isElementPresent(mergeAccountVerifyDialog);
	}

}
