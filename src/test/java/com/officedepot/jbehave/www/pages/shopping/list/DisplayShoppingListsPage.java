package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class DisplayShoppingListsPage extends BasePage {

	private WebElement createNewListButton;
	private WebElement createSchoolListButton;
	private WebElement newPersonalListName;
	private WebElement personalListComments;
	private WebElement savePersonalListButton;
	private WebElement personalShoppingList;
	private By addedSuccessTitle = By.cssSelector("#ui-id-1");
	private WebElement continueShoppingButton;


	public boolean isInChooseOrStartNewListDialog() {

		return findElementById("createListButton") != null;
	}

	public boolean isInStartANewListDialog() {
		return findElementByName("startSchoolList") != null;
	}

	public DisplayShoppingListsPage createNewList() throws Exception {
		createNewListButton = findElementById("createListButton");
		clickOn(createNewListButton);
		return new DisplayShoppingListsPage();
	}

	public DisplayShoppingListsPage createSchoolList() throws Exception {
		createSchoolListButton = findElementByName("startSchoolList");
		clickOn(createSchoolListButton);

		return new DisplayShoppingListsPage();
	}

	public void createPersonalListInterior() throws Exception {
		newPersonalListName = findElementByName("newListName");
		personalListComments = findElementByName("newDescription");
		typeTextBox(newPersonalListName, SeleniumUtils.giveUniqueIdAfter("CSL").substring(0, 9));
		typeTextBox(personalListComments, SeleniumUtils.giveUniqueIdAfter("CSLComments").substring(0, 9));
		savePersonalListButton = findElementByName("saveListDetails");
		clickOn(savePersonalListButton);
	}

	public boolean isPersonalListCreatedSuccess() throws Exception {
		return findElementById("ui-dialog-title-generic-form-ready-dialog") != null;
	}

	public void addSkuToExistedPersonalShoppingList() throws Exception {
		personalShoppingList = waitForPresenceOfElementsLocated(By.cssSelector(".addToListShopping")).get(0);	
		clickOn(personalShoppingList);
	}
	
	public boolean isAddedSuccessMessageDisplayed() throws Exception {
		return this.isTextPresentInElement(addedSuccessTitle, "successfully added to your list");
	}
	
	public void continueShopping() throws Exception {
		continueShoppingButton = findElementById("continueShopping");
		clickOn(continueShoppingButton);
	}
	
}
