package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CombineYourAccountsPage extends BasePage {

	private final String PAGE_TITLE = "Combine Your Accounts";

	private By pageTitle = By.cssSelector(".brandColor_tp2");
	private By loginName = By.id("loginName");
	private By password = By.id("password");
	private By submitButton = By.cssSelector(".btn.primary");

	public boolean isInCurrentPage() {
		return findElementBy(pageTitle).getText().trim().equalsIgnoreCase(PAGE_TITLE);
	}

	public void combineAccount(String loginName, String passWord) {

	}
}
