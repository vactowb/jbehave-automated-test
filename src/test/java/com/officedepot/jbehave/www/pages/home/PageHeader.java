package com.officedepot.jbehave.www.pages.home;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.officedepot.jbehave.www.pages.account.OrderTrackingWithoutLoginPage;
import com.officedepot.jbehave.www.pages.cartridge.OnlineCatalogPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.deals.WeeklyAdPage;
import com.officedepot.jbehave.www.pages.register.CustomerRegistrationPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.jbehave.www.pages.shopping.list.CslListDetailPage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.jbehave.www.pages.shopping.list.SchoolSuppliesPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.jbehave.www.pages.storelocator.ChooseStorePage;
import com.officedepot.jbehave.www.pages.storelocator.DrivingDirectionsPage;
import com.officedepot.jbehave.www.pages.storelocator.FindAStorePage;
import com.officedepot.jbehave.www.pages.storelocator.LocalStorePage;
import com.officedepot.jbehave.www.pages.storelocator.OneHourStorePage;
import com.officedepot.jbehave.www.pages.storelocator.StoreDetailsPage;
import com.officedepot.test.jbehave.BasePage;

public class PageHeader extends BasePage {

	private final String LOGOUT_OR_REGISTER = "Login or Register";

	private WebElement loginLink;
	private WebElement loginDialog;
	private WebElement login_UserName;
	private WebElement login_Password;
	private WebElement login_SubmitLogin;
	private WebElement storeDetailsLink;

	private WebElement navigatorLink;
	private WebElement viewAllListsLink;
	private WebElement storeChangeStoreLink;
	private By myListsNotice;
	private By myListsTitle;
	private WebElement myListsLoginLink;
	private WebElement myListsCreateAccountLink;
	private List<WebElement> myListsItem;
	private List<WebElement> myListsList;
	private WebElement myListManageListsLink;
	private WebElement myListCreateNewListLink;
	private WebElement myListViewListLink;
	private WebElement mapItLink;

	private WebElement withoutLoginWelcomeInformation;
	private WebElement logInNowLink;
	private String accountLoginWindow = "css=.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix";
	private String storePickUpText;
	private WebElement createAnAccountLink;
	private WebElement trackAShipmentLink;
	private By loginWelcomeInfoInAccount = By.cssSelector("#accountTitle");
	private By loginWelcomeInfo = By.cssSelector(".login_state");
	private By myStoreTextLocator = By.cssSelector(".f_left");
	private WebElement accountOverviewLink;
	private WebElement orderTrackingAndHistoryLink;
	private WebElement subscriptionsManagerLink;
	private WebElement paymentOptionsLink;
	private WebElement submitReturnLink;
	private WebElement reorderLink;
	private WebElement logOutLink;
	private WebElement withoutLoginCartInformation;
	private WebElement viewAllLink;
	private WebElement viewCartAndCheckoutLink;
	private WebElement searchZipTextbox;
	private List<WebElement> searchZipButton;
	private WebElement getDrivingDirectionsLink;
	private WebElement sendAddressToMobileLink;
	private WebElement myStoreLink;

	private WebElement dealsDealCenterLink;
	private WebElement dealsWeeklyAdLink;
	private WebElement dealsBackToSchoolLink;
	private WebElement dealsRequestCatalogLink;
	private WebElement pickUpButton;
	private WebElement deliveryButton;
	private WebElement notMeLink;
	private WebElement storeLocatorLink;
	private WebElement headerStoreNameLink;
	private WebElement headerStores;
	private WebElement findAStoreInput;
	private WebElement storeGoButton;
	private WebElement storeWeeklyAdLink;
	private WebElement moreSearchOptionLink;
	private WebElement oneHourStorePickupLink;
	private WebElement headerCart;
	private WebElement orderTrackingLink;
	private By storePickUpTextLocator;
	private By logoutStatus = By.cssSelector(".accountLogin");

	public void login(String username, String password) {
		loginLink = findElementByCSS(".accountLogin");
		executeLogin(username, password);
	}

	public void executeLogin(String username, String password) {
		clickOn(loginLink);
		inputLogin(username, password);
	}

	public void inputLogin(String username, String password) {
		loginDialog = findElementById("loginContentBody");
		login_UserName = findChildElementByName(loginDialog, "loginName");
		login_Password = findChildElementByName(loginDialog, "password");
		login_SubmitLogin = findChildElementByCSS(loginDialog, ".btn.primary.omx_button");
		typeTextBox(login_UserName, username);
		typeTextBox(login_Password, password);
		clickOn(login_SubmitLogin);
	}

	public boolean isLoginDialogDisplayed() {
		if (findElementById("loginContentBody") != null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isMyStoreTextOccur(String myStoreText) throws Exception {
		return isTextPresentInElement(myStoreTextLocator, myStoreText);
	}

	public boolean isStorePickUpTextOccur() throws Exception {
		storePickUpTextLocator = By.cssSelector(".brandColor_tp2.vspace_top.vspace_bottom.clear");
		storePickUpText = "Store pick-up not available at this location";
		return isTextPresentInElement(storePickUpTextLocator, storePickUpText);
	}

	public void viewNavigatorOnHeader(String navigator) {
		navigatorLink = findElementByLinkTextWait(navigator);
		mouseHoverOn(navigatorLink);
	}

	public String getMyStoreNumberFromHeader() throws Exception {
		headerStoreNameLink = findElementByCSSWait(".modal_local_store>a");
		String storeNumber = getText(headerStoreNameLink);
		return storeNumber.substring(storeNumber.indexOf("#"), storeNumber.length());
	}

	public void iRequestToLoginFromNewHeader(String loginUsername, String loginPassword) throws Exception {
		openLoginLightBox();
		executeLogin(loginUsername, loginPassword);
	}

	private void openLoginLightBox() throws Exception {
		viewNavigatorOnHeader("Account");
		loginLink = findElementByCSS("#headerAccountLogin");
	}

	public void viewNavigatorOnHeader2(String navigator) throws Exception {
		navigatorLink = findElementByLinkTextWait(navigator);

		Actions actions = new Actions(webDriver);
		actions.moveToElement(navigatorLink).perform();
		actions.sendKeys(findElementByCSS("#searchStore>form>input"), "33496").click(findElementByXpath("//*[@id='searchStore']/form/input[2]")).perform();
	}

	public MyListPage viewAllLists() throws Exception {
		viewAllListsLink = findElementByLinkTextWait("View All");
		clickOn(viewAllListsLink);
		return new MyListPage();
	}

	public boolean isNoticeDisplayedViaNotLogin() {
		myListsNotice = By.cssSelector(".modal_text_center");
		findElementByCSSWait(".modal_text_center");
		return isTextPresentInElement(myListsNotice, "Please log in or create an account to manage saved items");
	}

	public void loginFromMyLists() {
		myListsLoginLink = findElementByIdWait("headerMyListLogin");
		clickOn(myListsLoginLink);
	}

	public CustomerRegistrationPage createAccountFromMyLists() {
		myListsCreateAccountLink = findElementByCSSWait("a.modal_link_tight.fright");
		clickOn(myListsCreateAccountLink);
		return new CustomerRegistrationPage();
	}

	public boolean isNoSavedItemInMyLists() throws Exception {
		myListsNotice = By.cssSelector(".modal_text_center");
		findElementByCSSWait(".modal_text_center");
		return isTextPresentInElement(myListsNotice, "No Items in the list");
	}

	public boolean isListDisplayedInMyLists(String listName) throws Exception {
		myListsTitle = By.cssSelector(".modal_title");
		this.findElementByCSSWait(".modal_title");
		return isTextPresentInElement(myListsTitle, listName);
	}

	public boolean getItemQtyOfMyLists() throws Exception {
		myListsItem = findElementsByCSS(".itemname>img");
		if (myListsItem != null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getListQtyOfMyLists() throws Exception {
		myListsList = findElementsByCSS(".qty");
		if (myListsList != null) {
			return true;
		} else {
			return false;
		}
	}

	public MyListPage manageListsFromMyLists() throws Exception {
		myListManageListsLink = findElementByCSSWait("a.modal_link_tight.fright");
		clickOn(myListManageListsLink);
		return new MyListPage();
	}

	public CslListDetailPage viewListFromMyLists() throws Exception {
		myListViewListLink = findElementByCSSWait("a.modal_title_link");
		clickOn(myListViewListLink);
		return new CslListDetailPage();
	}

	public MyListPage createNewListFromMyLists() throws Exception {
		myListCreateNewListLink = findElementByCSSWait("a.modal_link_tight.fleft");
		clickOn(myListCreateNewListLink);
		return new MyListPage();
	}

	public SkuDetailPage viewSkuDetials() throws Exception {
		clickOn(myListsItem.get(0));
		seleniumWaitForPageToLoad(30000);
		return new SkuDetailPage();
	}

	public String viewWithoutLoginWelcomeInformation() throws Exception {
		withoutLoginWelcomeInformation = findElementByCSS(".accountLogin");
		return getText(withoutLoginWelcomeInformation);
	}

	public String viewWithoutLoginCartInformation() throws Exception {
		withoutLoginCartInformation = findElementByXpath("//*[@id='headerCartEmpty']");
		return getText(withoutLoginCartInformation);
	}

	public boolean isWelcomeInfoDisplayed() throws Exception {
		viewNavigatorOnHeader("Account");
		return isTextPresentInElement(loginWelcomeInfoInAccount, "Welcome,");
	}

	public FindAStorePage clickOnChangeStoreLink() throws Exception {
		storeChangeStoreLink = findElementByLinkText("Change Store");
		clickOn(storeChangeStoreLink);
		seleniumWaitForPageToLoad(30000);
		return new FindAStorePage();
	}

	public FindAStorePage clickOnHeaderStores() throws Exception {
		headerStores = findElementByLinkText("Stores");
		clickOn(headerStores);
		seleniumWaitForPageToLoad(30000);
		return new FindAStorePage();
	}

	public ShoppingCartPage clickOnHeaderCart() throws Exception {
		headerCart = findElementByLinkText("Cart");
		clickOn(headerCart);
		seleniumWaitForPageToLoad(30000);
		return new ShoppingCartPage();
	}

	public StoreDetailsPage clickOnStoreDetailsLink() throws Exception {
		storeDetailsLink = findElementByLinkText("Store Details");
		clickOn(storeDetailsLink);
		seleniumWaitForPageToLoad(30000);
		return new StoreDetailsPage();
	}

	public StoreDetailsPage clickOnMapItLink() throws Exception {
		mapItLink = findElementByLinkText("Map It");
		clickOn(mapItLink);
		seleniumWaitForPageToLoad(30000);
		return new StoreDetailsPage();
	}

	public LocalStorePage clickOnMyStoreLink() throws Exception {
		myStoreLink = findElementByLinkText("My Store");
		clickOn(myStoreLink);
		seleniumWaitForPageToLoad(30000);
		return new LocalStorePage();
	}

	public void clickOnLogInNowLink() throws Exception {
		logInNowLink = findElementByCSS("#headerAccountLogin");
		clickOn(logInNowLink);
	}

	public boolean isLoginWindowOccur() throws Exception {
		return isElementPresentBySelenium(accountLoginWindow);
	}

	public void clickOnCreateAnAccountLink() throws Exception {
		createAnAccountLink = findElementByXpath("//*[@id='accountTitle']/div[2]/a");
		clickOn(createAnAccountLink);
	}

	public void clickOnTrackAShipmentPage() throws Exception {
		trackAShipmentLink = findElementByXpath("//*[@id='accountContent']/div[3]/a");
		clickOn(trackAShipmentLink);
	}

	public void clickOnAccountOverviewLink() throws Exception {
		accountOverviewLink = findElementByXpath("//*[@id='accountList']/li[1]/a");
		clickOn(accountOverviewLink);
	}

	public void clickOnOrderTrackingAndHistoryLink() throws Exception {
		orderTrackingAndHistoryLink = findElementByXpath("//*[@id='accountList']/li[2]/a");
		clickOn(orderTrackingAndHistoryLink);
	}

	public void clickOnsubscriptionsManagerLink() throws Exception {
		subscriptionsManagerLink = findElementByXpath("//*[@id='accountList']/li[3]/a");
		clickOn(subscriptionsManagerLink);
	}

	public void clickOnPaymentOptionsLink() {
		paymentOptionsLink = findElementByXpath("//*[@id='accountList']/li[4]/a");
		clickOn(paymentOptionsLink);
	}

	public void clickOnSubmitReturnLink() throws Exception {
		submitReturnLink = findElementByXpath("//*[@id='accountList']/li[5]/a");
		clickOn(submitReturnLink);
	}

	public void clickOnReorderLink() throws Exception {
		reorderLink = findElementByXpath("//*[@id='accountList']/li[6]/a");
		clickOn(reorderLink);
	}

	public void clickOnLogOutLink() throws Exception {
		logOutLink = findElementByCSS(".logout_link.modal_link.vspace_bottom");
		clickOn(logOutLink);
	}

	public boolean isItemsListOccur() throws Exception {
		return isElementPresentBySelenium("css=.modal_img");
	}

	public void clickOnViewAllLink() throws Exception {
		viewAllLink = findElementByXpath("//*[@id='headerCart']/div[2]/div[1]/a");
		clickOn(viewAllLink);
	}

	public void clickOnViewCartAndCheckoutLink() throws Exception {
		viewCartAndCheckoutLink = findElementByXpath("//*[@id='headerCart']/div[2]/div[2]/a");
		clickOn(viewCartAndCheckoutLink);
	}

	public FindAStorePage serachStoreByZip(String zipNum) throws Exception {
		searchZipTextbox = findElementById("modalStoreSearchInput");
		searchZipButton = findElementsByCSS(".button_go");
		typeTextBox(searchZipTextbox, zipNum);
		clickOn(searchZipButton.get(0));
		return new FindAStorePage();
	}

	public LocalStorePage navigateToStorePageFromHeaderStoreNameLink() throws Exception {
		headerStoreNameLink = findElementByCSSWait(".modal_local_store>a");
		clickOn(headerStoreNameLink);
		seleniumWaitForPageToLoad(30000);
		return new LocalStorePage();
	}

	public FindAStorePage navigateToFindAPageFromFindAStoreInputBox() throws Exception {
		findAStoreInput = findElementById("modalStoreInput");
		typeTextBox(findAStoreInput, "33496");
		storeGoButton = findElementByCSS(".button_go");
		clickOn(storeGoButton);
		seleniumWaitForPageToLoad(30000);
		return new FindAStorePage();
	}

	public FindAStorePage navigateToFindAStorePageFromMoreSearchOptionLink() throws Exception {
		moreSearchOptionLink = findElementByLinkText("More Search Options");
		clickOn(moreSearchOptionLink);
		seleniumWaitForPageToLoad(30000);
		return new FindAStorePage();
	}

	public OneHourStorePage navigateToOneHourStorePageFromOneHourStorePickupLink() throws Exception {
		oneHourStorePickupLink = findElementByLinkText("1 Hour Store Pickup");
		clickOn(oneHourStorePickupLink);
		seleniumWaitForPageToLoad(30000);
		return new OneHourStorePage();
	}

	public boolean isStoreInformationInHeaderStore() throws Exception {
		return isElementPresentBySelenium("css=#mapTile");
	}

	public DrivingDirectionsPage clickOnGetDrivingDirectionsLink() throws Exception {
		getDrivingDirectionsLink = findElementByLinkTextWait("Get Driving Directions");
		clickOn(getDrivingDirectionsLink);
		seleniumWaitForPageToLoad(30000);
		return new DrivingDirectionsPage();
	}

	public StoreDetailsPage clickOnSendAddressToMobileLink() throws Exception {
		sendAddressToMobileLink = findElementByLinkTextWait("Send Address to Mobile");
		clickOn(sendAddressToMobileLink);
		seleniumWaitForPageToLoad(30000);
		return new StoreDetailsPage();
	}

	public DealCenterPage goToDealCenterFromDeals() throws Exception {
		dealsDealCenterLink = findElementByCSS(".deals_icon");
		clickOn(dealsDealCenterLink);
		return new DealCenterPage();
	}

	public String getCurrentWindowHandle() throws Exception {
		return getWindowHandle();
	}

	public WeeklyAdPage goToWeeklyAdFromDeals() throws Exception {
		dealsWeeklyAdLink = findElementByCSSWait(".modal_link.nowrap");
		clickOn(dealsWeeklyAdLink);
		focusOnNewWindow();
		return new WeeklyAdPage();
	}

	public WeeklyAdPage goToWeeklyAdFromStoreWeeklyAdLink() throws Exception {
		storeWeeklyAdLink = findElementByCSSWait(".modal_link.tight.area");
		clickOn(storeWeeklyAdLink);
		focusOnNewWindow();
		return new WeeklyAdPage();
	}

	public SchoolSuppliesPage goToBackToSchool() throws Exception {
		dealsBackToSchoolLink = findElementByXpathWait("//div[@id='modalDealRight']/div[3]/div/a");
		clickOn(dealsBackToSchoolLink);
		return new SchoolSuppliesPage();
	}

	public OnlineCatalogPage goToRequestCatalog() throws Exception {
		dealsRequestCatalogLink = findElementByXpath("//*[@id='modalDealRight']/div[4]/a");
		clickOn(dealsRequestCatalogLink);
		return new OnlineCatalogPage();
	}

	public void clickOnPickUpButton() throws Exception {
		pickUpButton = findElementByXpath("//*[@id='headerCart']/div[2]/div[1]/ul/form/li[2]/input");
		clickOn(pickUpButton);
	}

	public void clickOnDeliveryButton() throws Exception {
		deliveryButton = findElementByXpath("//*[@id='headerCart']/div[2]/div[1]/ul/form/li[1]/input");
		clickOn(deliveryButton);
	}

	public void openLostPasswordPage() throws Exception {
		openLoginLightBox();
		clickOn(loginLink);
		loginDialog = findElementById("loginContentBody");
		clickOn(findChildElementById(loginDialog, "modalLoginPwdResetSQA"));
	}

	public void loginFromLostpasswordPage(String username, String password) {
		loginLink = findElementByLinkText("Login or Register");
		executeLogin(username, password);
	}

	public void loginFromLostPasswordPage(String username, String password) {
		loginLink = findElementById("lostLoginNameModalLogin");
		executeLogin(username, password);
	}

	public boolean isLogoutLinkExistedFromNewHeader() {
		return isElementPresent(By.linkText("Logout"));
	}

	public boolean isLoginSuccessfullyFromNewHeader() throws Exception {
		return isTextPresentInElement(loginWelcomeInfo, "Welcome");
	}

	public boolean isLoginLinkOccursFromNewHeader() throws Exception {
		return isElementPresentBySelenium("link=Login or Register");
	}

	public void confirmNotMe() throws Exception {
		notMeLink = findElementByCSS("li.login_state > span.small > a");
		clickOn(notMeLink);
	}

	public ChooseStorePage viewStoreLocator() throws Exception {
		storeLocatorLink = findElementByLinkText("Store Locator");
		clickOn(storeLocatorLink);
		seleniumWaitForPageToLoad(30000);
		return new ChooseStorePage();
	}

	public void rigisterFromOldHeader() {
		clickOn(findElementByLinkText("Register"));
	}

	public void clickOnRegisterTopRight() {
		loginLink = findElementByLinkText("Login or Register");
		clickOn(loginLink);
	}

	public void clickOnRegisterNewAccountLink() {
		loginDialog = findElementById("loginContentBody");
		clickOn(findChildElementById(loginDialog, "registerNew"));
	}

	public void clicktrackShipment() throws Exception {
		openUrl("/orderhistory/orderHistoryList.do");
	}

	public boolean isLoginSuccessfullyFromHeader() throws Exception {
		return isTextPresentOnPage("Logout");
	}

	public boolean isThereNoUserInfo() {
		return findElementBy(logoutStatus).getText().trim().equalsIgnoreCase(LOGOUT_OR_REGISTER);
	}
	
	public OrderTrackingWithoutLoginPage clickOnOrderTrackingLink() throws Exception {
		orderTrackingLink = findElementByLinkText("Order Tracking & History");
		clickOn(orderTrackingLink);
		return new OrderTrackingWithoutLoginPage();
	}
}
