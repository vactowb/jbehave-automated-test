package com.officedepot.jbehave.www.pages.graybar;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.cartridge.ChatNowWindow;
import com.officedepot.jbehave.www.pages.deals.WeeklyAdPage;
import com.officedepot.jbehave.www.pages.home.BSDHomePage;
import com.officedepot.jbehave.www.pages.home.OfficemaxWorkplacePage;
import com.officedepot.test.jbehave.BasePage;

	public class PageGrayBar extends BasePage {

		private WebElement weeklyAdGrayBarLink;
		private WebElement loginOrRegisterGrayBarLink;
		private WebElement orderByItemLink;
		private WebElement liveChatLink;
		private WebElement corporateAndGovernmentLink;
		private WebElement officeDepotBusinessSolutionLink;
		private WebElement officeMaxWorkplaceLink;

		public String getCurrentWindowHandle() throws Exception {
			return getWindowHandle();
		}

		public WeeklyAdPage clickOnWeeklyAdGrayBar() throws Exception {
			weeklyAdGrayBarLink = findElementByCSS(".weeklyAdLink");
			clickOn(weeklyAdGrayBarLink);
			focusOnNewWindow();
			return new WeeklyAdPage();
		}

		public OrderByItemPage clickOnOrderByItemLink() throws Exception {
			orderByItemLink = findElementByLinkText("Order by Item #");
			clickOn(orderByItemLink);
			return new OrderByItemPage();
		}
		
		public ChatNowWindow clickOnLiveChatLink() throws Exception {
			liveChatLink = findElementByLinkText("Live Chat 24/7");
			clickOn(liveChatLink);
			return new ChatNowWindow();
		}
		
		public void clickOnGrayBarLoginAndRegister() throws Exception {
			loginOrRegisterGrayBarLink = findElementByLinkText("Login or Register");
			clickOn(loginOrRegisterGrayBarLink);
		}
		
		public void viewCorporateAndGovernmentFlyout() {
			corporateAndGovernmentLink = findElementByLinkText("Corporate/Government");
			mouseHoverOn(corporateAndGovernmentLink);
		}
		
		public BSDHomePage navigateToBSDHomePage() throws Exception {
			officeDepotBusinessSolutionLink = findElementByCSS(".tight>a");
			clickOn(officeDepotBusinessSolutionLink);
			return new BSDHomePage();
		}
		
		public OfficemaxWorkplacePage navigateToOfficemaxWorkplacePage() throws Exception {
			officeMaxWorkplaceLink = findElementByCSS(".last>a");
			clickOn(officeMaxWorkplaceLink);
			return new OfficemaxWorkplacePage();
		}
		

	}

