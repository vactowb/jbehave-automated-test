package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;

public class ConsiderRelatedItemsPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#techPagetitle>h1");
	private WebElement continueToCartButton;

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Consider these related items");
	}
	
	public ShoppingCartPage clickOnContinueToCartButton() throws Exception {
		continueToCartButton = findElementByLinkText("Continue to cart");
		clickOn(continueToCartButton);
		return new ShoppingCartPage();
	}
	
}
