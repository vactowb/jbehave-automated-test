package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class SchoolSuppliesPage extends BasePage {

private By pageVerifyItem = By.cssSelector(".lastBreadCrumb");
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "School Supplies");
	}
	
	public void goBackToPreWindow(String window) throws Exception {
		switchToMainWindow(window);
	}
	
}
