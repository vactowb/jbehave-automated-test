package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class InkjetSearchPage extends BasePage  {

	private By pageVerifyItem = By.cssSelector("#skuList");
	private List<WebElement> seePriceInCartButton;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Inkjet All-In-Ones");
	}
	
	public AddYourPriceWindow clickOnSeePriceInCartButton() throws Exception {
		seePriceInCartButton = findElementsByCSS(".flcl.w100.font_size2");
		clickOn(seePriceInCartButton.get(0));
		return new AddYourPriceWindow();
	}
	
}
