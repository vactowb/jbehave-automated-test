package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CslListDetailPage extends BasePage {

	private WebElement addToCartButton;
	private WebElement shareListLink;
	private By pageVerifyItem = By.cssSelector(".modStyle1");
	private WebElement listNameTextBox;
	private By selectAllCheckBox = By.name("selectAll");
	private By removeLink = By.id("cslCollabRequestDialogRemove");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentOnPage("List Settings");
	}

	public void addAllSkusIntoCart() throws Exception {
		addToCartButton = findElementByName("cmd_addToCart.button");
		clickBy(selectAllCheckBox);
		clickOn(addToCartButton);
	}

	public void addSkusIntoCart() throws Exception {
		addToCartButton = findElementByName("cmd_addToCart.button");
		clickOn(addToCartButton);
	}

	public CSLShareItemsDialogPage shareCslList() throws Exception {
		clickBy(selectAllCheckBox);
		shareListLink = findElementById("cslCollabRequestDialog");
		clickOn(shareListLink);
		return new CSLShareItemsDialogPage();
	}

	public String getListName() throws Exception {
		for (WebElement e : findElementsByName("listName")) {
			if (e.isDisplayed()) {
				listNameTextBox = e;
				break;
			}
		}
		return getValue(listNameTextBox);
	}

	public boolean isAllListItemsDeleted() {
		return isTextPresentOnPage("There are no items in this list");
	}

	public void selectAllListItems() {
		clickBy(selectAllCheckBox);
	}

	public void removeSelectItems() {
		clickBy(removeLink);
	}

}
