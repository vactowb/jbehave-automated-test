package com.officedepot.jbehave.www.pages.account;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.register.ExpressRegisterPage;
import com.officedepot.test.jbehave.BasePage;

public class ShippingListDisplayPage extends BasePage {

	private By pageVerifyItem = By.tagName("h1");
	private WebElement firstName;
	private WebElement lastName;
	private WebElement address;
	private WebElement city;
	private WebElement company;
	private WebElement state;
	private WebElement zipCode;

	public ShippingListDisplayPage() {
		assertTrue("---Failed to navigate on ShippingAddressDisplay	Page!---", isTextPresentOnPage("My Account"));
	}

	public ShippingListDisplayPage(String anything) {
	}

	public Map<String, String> createNewAddressWithExistingData() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		String[] name = findElementsByCSS(".shiptoName>span").get(0).getText().trim().split(" ");
		String address = findElementsByCSS(".address1.required>span").get(0).getText();
		map.put("address", address);
		String city = findElementsByCSS(".city").get(0).getText();
		map.put("city", city);
		String state = findElementsByCSS(".state").get(0).getText();
		map.put("state", state);
		String postalCode = findElementsByCSS(".postalCode1.required>span").get(0).getText().trim();
		map.put("postalCode", postalCode);

		// Add the duplicate address
		typeTextBox(findElementByCSS("#firstName-0"), name[0]);
		typeTextBox(findElementByCSS("#lastName-0"), name[1]);
		typeTextBox(findElementByCSS("#address1-0"), address);
		typeTextBox(findElementByCSS("#city-0"), city.substring(0, city.indexOf(",")));
		selectOptionInListByText(findElementById("state-0"), findElementByXpath("//select[@id='state-0']/option[@value='" + state.trim() + "']").getText().trim());
		typeTextBox(findElementByCSS("#phoneNumber1-0"), "135");
		typeTextBox(findElementByCSS("#phoneNumber2-0"), "555");
		typeTextBox(findElementByCSS("#phoneNumber3-0"), "6666");
		typeTextBox(findElementByCSS("#postalCode1-0"), postalCode);
		typeTextBox(findElementByCSS("#email-0"), "yz@officedepot.com");
		typeTextBox(findElementByCSS("#emailConfirm-0"), "yz@officedepot.com");
		clickOn(findElementByXpath("//input[@title='Add this Address']"));
		return map;
	}

	public boolean isAddedAddressDuplicated(Map<String, String> map) throws Exception {
		return findElementByXpath("(//div[@class='addressElement currentAddressElement'])/div[@class='currentContent']/div[@class='address']/div[@class='f_left']/ol/li[@class='address1 required']")
				.getText().trim().equals(map.get("address").trim())
				&& findElementByXpath("(//div[@class='addressElement currentAddressElement'])/div[@class='currentContent']/div[@class='address']/div[@class='f_left']/ol/li[@class='city']").getText()
						.trim().equals(map.get("city").trim())
				&& findElementByXpath("(//div[@class='addressElement currentAddressElement'])/div[@class='currentContent']/div[@class='address']/div[@class='f_left']/ol/li[@class='state']").getText()
						.trim().equals(map.get("state").trim())
				&& findElementByXpath(
						"(//div[@class='addressElement currentAddressElement'])/div[@class='currentContent']/div[@class='address']/div[@class='f_left']/ol/li[@class='postalCode1 required']/span")
						.getText().trim().equals(map.get("postalCode").trim());
	}

	// shipping information on account overview page
	public boolean isFirstShippingInformation() {
		firstShippingInformation();
		return firstName.getAttribute("value").equals(ExpressRegisterPage.FIRSTNAME) && lastName.getAttribute("value").equals(ExpressRegisterPage.LASTNAME)
				&& zipCode.getAttribute("value").equals(ExpressRegisterPage.ZIPCODE);

	}

	// checkout step2 page
	public boolean isSecondShippingInformation() {
		secondShippingInformation();
		return firstName.getAttribute("value").equals(ExpressRegisterPage.FIRSTNAME) && lastName.getAttribute("value").equals(ExpressRegisterPage.LASTNAME)
				&& zipCode.getAttribute("value").equals(ExpressRegisterPage.ZIPCODE);
	}

	public void editFirstShippingInformation() {
		firstShippingInformation();
		typeTextBox(address, "6600 N MILITARY TRL # 6");
		typeTextBox(city, "BOCA RATON");
		typeTextBox(company, "od");
		selectOptionInListByText(state, "FL - Florida");
		typeTextBox(findElementByCSS("#phoneNumber1-0"), "135");
		typeTextBox(findElementByCSS("#phoneNumber2-0"), "555");
		typeTextBox(findElementByCSS("#phoneNumber3-0"), "6666");

		clickOn(findElementByXpath("//input[@title='Update']"));
	}

	public boolean checkFirstPaymentInformation() {
		String payInfo = findElementById("paymentInfo").findElement(By.cssSelector(".address")).getText();
		return payInfo.contains("BOCA RATON");
	}

	private void firstShippingInformation() {
		firstName = findElementByCSS("#firstName-0");
		lastName = findElementByCSS("#lastName-0");
		address = findElementByCSS("#address1-0");
		city = findElementByCSS("#city-0");
		company = findElementById("shiptoName-0");
		state = findElementById("state-0");
		zipCode = findElementByCSS("#postalCode1-0");
	}

	private void secondShippingInformation() {
		firstName = findElementByCSS("#firstName-1");
		lastName = findElementByCSS("#lastName-1");
		address = findElementByCSS("#address1-1");
		city = findElementByCSS("#city-1");
		company = findElementById("shiptoName-1");
		state = findElementById("state-1");
		zipCode = findElementByCSS("#postalCode1-1");
	}

	public void editSecondShippingInformation() {
		secondShippingInformation();
		typeTextBox(address, "6600 N MILITARY TRL # 6");
		typeTextBox(city, "BOCA RATON");
		typeTextBox(company, "od");
		selectOptionInListByText(state, "FL - Florida");
		typeTextBox(findElementByCSS("#phoneNumber1-1"), "561");
		typeTextBox(findElementByCSS("#phoneNumber2-1"), "222");
		typeTextBox(findElementByCSS("#phoneNumber3-1"), "2323");

	}

	public boolean faxFieldPresent() {
		return isElementPresentBySelenium("css=.faxNumber1");
	}

	public boolean isAddressEditable(String address) {
		WebElement linksSection = findElementBy(By.cssSelector(".lineOfLinks.section"));
		return linksSection.findElements(By.cssSelector("a")).size() < 2;
	}
}
