package com.officedepot.jbehave.www.pages.graybar;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class OrderByItemPage extends BasePage {
	
	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Order By Item Number");
	}
}
