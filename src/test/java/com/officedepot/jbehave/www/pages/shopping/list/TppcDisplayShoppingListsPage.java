package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class TppcDisplayShoppingListsPage extends BasePage {

	private WebElement createNewListButton;
	private WebElement createSchoolListButton;


	public boolean isInChooseOrStartNewListDialog() {

		return findElementById("createListButton") != null;
	}
	
	public boolean isInStartANewListDialog() {
		return findElementByName("startSchoolList") != null;
	}

	public void createNewList() throws Exception {
		createNewListButton = findElementById("createListButton");
		clickOn(createNewListButton);
	}

	public TppcEditListPage createSchoolList() throws Exception {
		createSchoolListButton = findElementByName("startSchoolList");
		clickOn(createSchoolListButton);
		return new TppcEditListPage();
	}
}
