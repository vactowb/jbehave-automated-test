package com.officedepot.jbehave.www.pages.sku;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CustomizePage extends BasePage {
	private float price;
	private By cancelBtn = By.id("uploader_cancel");
	private By viewSampleLink = By.linkText("View Sample");
	private By nowdocsPrice = By.cssSelector(".price.background.border.border-thin");

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isChargeTotalShown() {
		return isElementPresent(By.cssSelector(".div_ChargeTotal"));
	}

	public boolean isUploadContainViewShown() {
		return isElementPresent(By.id("uploader_container"));
	}

	public void cancelUploadFile() {
		clickBy(cancelBtn);
	}

	public void viewSampleFile() {
		clickBy(viewSampleLink);
	}

	public boolean iShouldSeeLblPriceInViewSampleFilePage() {
		if(findElementsBy(nowdocsPrice).size() == 0)
			return false;
		return checkPrice(findElementsBy(nowdocsPrice).get(0));
	}

	public boolean isComparePriceShownInConfiguratorForVendor(String vendorName) {
		By tempById = null;
		if("Harland".equalsIgnoreCase(vendorName)){
			tempById = By.id("totPrice");
		}
		if("Holland".equalsIgnoreCase(vendorName)){
			tempById = By.cssSelector("tr:nth-child(2) .prc");
		}
		if("NowDocs".equalsIgnoreCase(vendorName)){
			tempById = By.cssSelector(".price.background.border.border-thin");
		}
		if("Taylor".equalsIgnoreCase(vendorName)){
			tempById = By.id("MasterContent_Lblprice");
		}
		WebElement tempElement = findElementBy(tempById); 
		return checkPrice(tempElement);
	}
	
	private boolean checkPrice(WebElement element) {
		int count = 5;
		float priceCurrent = 0;
		while (count > 0) {
			String priceStr = "";
			if(element.getText().startsWith("$"))
				priceStr = element.getText().substring(1);
			if(priceStr.contains(" ")){
				priceStr = priceStr.split(" ")[0];
			}
			try {
				priceCurrent = Float.parseFloat(priceStr);
			}catch(Exception e) {
				System.out.println("Something incorrect happened about the price");
				priceCurrent = 0;
			}
			System.out.println(priceCurrent);
			count--;
			if (priceCurrent > 0)
				break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return priceCurrent == price;

	}
}
