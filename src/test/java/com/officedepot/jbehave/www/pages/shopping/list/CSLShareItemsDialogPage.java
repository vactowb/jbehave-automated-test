package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CSLShareItemsDialogPage extends BasePage {

	private WebElement mailToTextArea;
	private WebElement sendEmailButton;
	private WebElement collabRequestContentBody;
	
	By collabRequestContentBodyBy = By.id("collabRequestContentBody");


	public void sendEmail() throws Exception {
		collabRequestContentBody = findElementById("collabRequestContentBody");
		mailToTextArea = findChildElementById(collabRequestContentBody, "mailTo");
		sendEmailButton = findChildElementById(collabRequestContentBody, "sendEmail");
		typeTextBox(mailToTextArea, "whatever@officedepot.com");
		clickOn(sendEmailButton);
	}

	public boolean isSendEmailSuccessful() throws Exception {
		return isTextPresentInElement(collabRequestContentBodyBy,"Your email has been sent.");
	}

}
