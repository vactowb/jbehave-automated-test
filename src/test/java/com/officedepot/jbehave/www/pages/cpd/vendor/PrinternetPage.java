package com.officedepot.jbehave.www.pages.cpd.vendor;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class PrinternetPage extends BasePage {

	boolean orderFound;

	public void openPrinternetPage() {
		String url = "https://odtest.printernet.com/order-lookup.aspx";
		openUrl(url);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("https://odtest.printernet.com/default.aspx?ReturnUrl=%2forder-lookup.aspx", getCurrentUrl());
		System.out.println("opened: " + url);
		selectOptionInListByValue(findElementById("ddlSites"), "all");
		clickOn(findElementById("btnLogin"));
	}

	public boolean orderFound(String orderNumber) {
		String url = "https://odtest.printernet.com/order-details.aspx?oid=" + orderNumber;
		openUrl(url);
		System.out.println("opened: " + url);
		WebElement el = findElementById("pnlContent");
		orderFound = el.getText().contains(orderNumber);
		return orderFound;
	}
}
