package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ChatNowWindow extends BasePage {

	private By pageVerifyItem = By.cssSelector("#ui-id-1");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Office Depot Live Chat");
	}
}
