package com.officedepot.jbehave.www.pages.obi;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;

public class ObiPage extends BasePage {

	private WebElement skuEntryField0;
	private WebElement skuEntryField1;
	private WebElement addToCartButton;
	private WebElement skuIdLabelElem;
	
	public void openPage() {
		this.openUrl("/csl/quickAddBySku.do");
	}
	
	public void enterSkuAndTabAway(String sku) {
		skuEntryField0 = findElementById("item[0].sku");
		typeTextBox(skuEntryField0, sku);
		skuEntryField1=findElementById("item[1].sku");
		clickOn(skuEntryField1);
	}
	
	public boolean isSkuLoaded(String skuId) {
		addToCartButton = findElementByName("cmd_addSKU.button");
		clickOn(addToCartButton);
		skuIdLabelElem = findElementBy(By.className("item_sku"));
		if(skuIdLabelElem.getText().indexOf(skuId) != -1) {
			return true;
		} else {
			return false;
		}
	}
	
}
