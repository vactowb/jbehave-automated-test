package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ListDetailsPage extends BasePage {
	
	private WebElement deleteListLink;
	private WebElement listName;
	
	public DeleteListConfirmPage deleteMyList() throws Exception {
		deleteListLink = findElementByXpath("//*[@id='csl_MultiActions']/ul/li[3]/a");
		clickOn(deleteListLink);
		return new DeleteListConfirmPage();
	}
	
	public String getListName() throws Exception{
		listName = findElementByXpath("//*[@id='m02']/div[1]/div/div[1]/div/ul[1]/li[1]/span/input");
		return getValue(listName);
	}
	
}
