package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.browser.ScholasticBrowserPage;
import com.officedepot.test.jbehave.BasePage;

public class OfficeSuppliesPage extends BasePage{
	private By pageVerifyItem = By.cssSelector(".lastBreadCrumb");
	private WebElement scholasticLink;
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Office Supplies");
	}
	
	public ScholasticBrowserPage clickOnScholasticLeftNav() throws Exception {

		scholasticLink = this.findElementByXpath("//ul[@id='shopByItems']/li[2]/a");
		clickOn(scholasticLink);
		return new ScholasticBrowserPage();
	}
	
}
