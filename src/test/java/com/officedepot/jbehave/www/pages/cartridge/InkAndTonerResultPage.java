package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class InkAndTonerResultPage extends BasePage  {

	private By pageVerifyItem = By.cssSelector(".skuList_results");

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Results");
	}
}
