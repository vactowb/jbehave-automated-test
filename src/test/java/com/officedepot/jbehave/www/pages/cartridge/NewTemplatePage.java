package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class NewTemplatePage extends BasePage {

	private String pageVerifyItem = "css=.product_block";
	private List<WebElement> skuDetailPage;
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isElementPresentBySelenium(pageVerifyItem);
	}
	
	public CategoriesPage clickOnShopMeButton() throws Exception {
		skuDetailPage = findElementsByLinkText("BUY THIS");
		clickOn(skuDetailPage.get(0));
		return new CategoriesPage();
	}
	
}
