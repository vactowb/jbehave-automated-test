package com.officedepot.jbehave.www.pages.sku;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.deals.OfficeSuppliesPage;
import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;

public class SkuDetailPage extends BasePage {

	private WebElement addToCartButton;
	private WebElement viewCartButton;
	private By qtyBox = By.id("mainqtybox");
	private WebElement privatePriceQtyBox;
	private WebElement skuDesc;
	private WebElement clickViewCartLink;
	private WebElement seePriceInCartLink;
	private WebElement keepInMyCartButton;
	private WebElement linkElement;
	private List<WebElement> breadScrumOfficeSupplies;
	private By skuIdItem = By.cssSelector(".item_sku");
	private By storeStatusItem = By.cssSelector(".stockStore.instock_grn");
	private By saveToListButton = By.cssSelector(".button.cslRequestDialog");

	private By pageVerifyItem = By.cssSelector("#siteBreadcrumb");

	private By customizeBtn = By.id("buttonCustomize1");
	private By priceText = By.cssSelector(".cuote_price.ng-binding");
	private By bccSkuItem = By.id("preload-thub");
	private By getStartButton = By.className("button-customize");

	public boolean isInCurrentPage() {
		return isPageLoaded(pageVerifyItem, "Product Details");
	}

	public String getPageLink() {
		return getCurrentUrl();
	}

	public void clickAddToList() {
		clickBy(saveToListButton);
	}

	public boolean containSkuDeatil(String detail) throws Exception {
		skuDesc = findElementByCSS("div#skuHeading > h1.fn");
		if (skuDesc.getText().equals(detail)) {
			return true;
		}
		return false;
	}

	public boolean isNavigateToExceptedSkuDetails(String skuId) {
		if (isTextPresentInElement(skuIdItem, skuId)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isNavigateToDiscontinuedSkuPage(String label) {
		skuDesc = findElementByXpath("/html/body/div[2]/div[11]/div/div/form/div/div/div/div/p");
		if (skuDesc.getText().contains(label)) {
			return true;
		}
		return false;
	}

	public ShoppingCartPage viewShoppingCart() throws Exception {
		pageVerifyItem = By.xpath("//*[@id='atc_addedHeading']/span");
		Assert.assertTrue("---Failed to add sku into cart!---", isPageLoaded(pageVerifyItem, "This product has been added to your cart"));
		viewCartButton = findElementByXpath("//div[@id='atc_cartActions']/a[2]");
		clickOn(viewCartButton);
		return new ShoppingCartPage();
	}

	public boolean isSkuPriceInPublic() throws Exception {
		return isElementVisiable(By.name("cmd_addSKU.button.INDEX[0]"));
	}

	public void seeSalePriceInCart() throws Exception {
		seePriceInCartLink = findElementByCSS("span.promoPriceLink");
		clickOn(seePriceInCartLink);
	}

	public void addToCartViaPrivatePrice() throws Exception {
		keepInMyCartButton = findElementById("map_addtocart");
		clickOn(keepInMyCartButton);
	}

	public void addToCartViaPublicPrice() throws Exception {
		addToCartButton = findElementByName("cmd_addSKU.button.INDEX[0]");
		clickOn(addToCartButton);
	}

	public void addToCart() throws Exception {
		if (isSkuPriceInPublic()) {
			addToCartViaPublicPrice();
		} else {
			seeSalePriceInCart();
			addToCartViaPrivatePrice();
		}
	}

	public void addSkuToCartAndViewCart() throws Exception {
		addToCart();
		viewCart();
	}

	public void addToCartAndContinueShopping() throws Exception {
		addToCart();
		if (isElementPresentBySelenium("css=.atc_button")) {
			WebElement viewShoppingCart = waitForPresenceOfElementsLocated(By.cssSelector(".atc_button")).get(0);
			clickOn(viewShoppingCart);
		} else if (isElementPresentBySelenium("id=atc_cartActions")) {
			clickOn(findElementByXpath("//a[contains(text(),'Continue Shopping')]"));
		}

	}

	public void addSkusToCartWithQty(String qty) throws Exception {
		if (isSkuPriceInPublic()) {
			Thread.sleep(1500);
			typeTextBoxBy(qtyBox, qty);
			Thread.sleep(1500);
			if(!selenium.getValue("id=mainqtybox").equals(qty)) {
				typeTextBoxBy(qtyBox, qty);
			}
			System.out.println(selenium.getValue("id=mainqtybox"));
			clickBy(qtyBox);
			addToCartViaPublicPrice();
		} else {
			seeSalePriceInCart();
			privatePriceQtyBox = findElementById("qvQty");
			typeTextBox(privatePriceQtyBox, qty);
			Thread.sleep(1500);
			addToCartViaPrivatePrice();
		}
	}

	public void addSkusToCartWithQtyAndViewCart(String qty) throws Exception {
		addSkusToCartWithQty(qty);
		viewCart();
	}

	public ShoppingCartPage openShoppingCartPage() throws Exception {
		clickViewCartLink = findElementByCSS(".cart");
		clickOn(clickViewCartLink);
		return new ShoppingCartPage();
	}

	public boolean containsStoresDetails(String detail) throws Exception {
		skuDesc = findElementByXpath("/html/body/div[2]/div[10]/div/div/form/div/div/div/div[2]/div[3]/div[2]/div[2]/div/div/table/tbody/tr/td");
		if (skuDesc.getText().contains(detail)) {
			return true;
		}
		return false;
	}

	public boolean isInStock() {
		return isTextPresentInElement(storeStatusItem, "In Stock");
	}

	public boolean isNotInStock() {
		return isTextPresentInElement(storeStatusItem, "Not in Stock");
	}

	public boolean isLinkPresent(String link) {
		linkElement = findElementByLinkText(link);
		if (linkElement.getText().equals(link)) {
			return true;
		}
		return false;
	}

	public boolean containsSkuLowQtyMessageElement() throws Exception {
		return isElementPresentBySelenium("id=skuLowQtyMessage");
	}

	public boolean containsHpMedallionElement() throws Exception {
		WebElement val = findElementByCSS(".hpMedallion");
		if (val != null)
			return true;
		else
			return false;
	}

	public OfficeSuppliesPage backToOfficeSuppliesPage() throws Exception {
		breadScrumOfficeSupplies = findElementsByCSS("#siteBreadcrumb>span>span>a");
		clickOn(breadScrumOfficeSupplies.get(0));
		return new OfficeSuppliesPage();
	}

	private void viewCart() {

		clickOn(findElementByXpath("//*[@id='atc_cartActions']/div/a"));
		// if (isElementPresentBySelenium("css=.atc_button")) {
		// WebElement viewShoppingCart = waitForPresenceOfElementsLocated(By.cssSelector(".atc_button")).get(1);
		// clickOn(viewShoppingCart);
		// } else if (isElementPresentBySelenium("id=atc_cartActions")) {
		// clickOn(findElementByXpath("//a[contains(text(),'VIEW CART & CHECKOUT')]"));
		// } else {
		// selenium.click("link=Continue to cart");
		// }
	}

	public boolean isShownOMaxODStores() {
		return (isElementPresent(By.xpath("//img[contains(@src,'od-logo.png')]")) || isElementPresent(By.xpath("//img[contains(@src,'od-logo.png')]"))) && isTextPresentOnPage("View Hours");
	}

	public boolean isShownOMaxODStoresBrand() {
		return (isElementPresent(By.xpath("//img[contains(@src,'od-logo.png')]")) || isElementPresent(By.xpath("//img[contains(@src,'od-logo.png')]")));
	}

	public boolean isInventoryAvailable() {
		return isTextPresentOnPage("Store inventory is not available online.");
	}

	public void customizeCpdSku() {
		clickBy(customizeBtn);
	}

	public boolean isPriceGreaterZero(String flag) {
		if (flag.equals("bcc"))
			priceText = By.cssSelector(".quote_price.ng-binding");
		waitForVisibilityOfElementLocated(priceText);
		int count = 5;
		float pricef = 0;
		while (count > 0) {
			String price = findElementBy(priceText).getText();
			System.out.println(price);
			pricef = Float.valueOf(price.trim().substring(1));
			count--;
			if (pricef > 0)
				break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return pricef > 0;
	}

	public void selectOneBccSku() {
		clickOn(waitForPresenceOfElementsLocated(bccSkuItem).get(3));
		for (WebElement e : findElementsBy(getStartButton)) {
			if (e.isDisplayed()) {
				clickOn(e.findElement(By.cssSelector(".button.customize")));
				break;
			}
		}
	}

	public CustomizePage customizeProduct() {
		float price = Float.parseFloat(findElementByCSSWait(".price_amount").getText().substring(1));
		System.out.println(price);
		CustomizePage customizePage = new CustomizePage();
		customizePage.setPrice(price);

		clickBy(customizeBtn);

		return customizePage;
	}

}
