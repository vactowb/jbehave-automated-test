package com.officedepot.jbehave.www.pages.orderhistory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class OrderDetailsPage extends BasePage {

	private By cancelOrderLink = By.linkText("Cancel");
	private By cancelOrderYesButton = By.cssSelector(".cancelOrder_True");
	private By statusBarTitle = By.cssSelector(".blackberryStatusColor");
	private By barcodeText = By.cssSelector(".col1.first");
	private By createAccountButton = By.name("createAccountButton");
	private By passwordField = By.name("loginForm.password");
	private By confirmPasswordField = By.name("loginForm.passwordConfirm");
	private By reorderbutton = By.cssSelector("li.reorder>.b1a>.button");

	public boolean isNavigateToExceptedOrderDetail() throws Exception {
		return isTextPresentOnPage("Order Detail");
	}

	public void cancelOrder() throws Exception {
		clickBy(cancelOrderLink);
		List<WebElement> a = findElementsBy(cancelOrderYesButton);
		for (WebElement b : a) {
			if (b.isDisplayed()) {
				clickOn(b);
			}
		}
	}

	public boolean isOrderCancelled() throws Exception {
		return isTextPresentOnPage("Cancelled");
	}

	public boolean checkBarcodeDigitsOnOrderDetailsPage() {
		return findElementBy(barcodeText).getText().contains("Store POS Return Barcode:");
	}

	public void enterPasswordAndCreateAccount(String password, String confirmPassword) {
		typeTextBoxBy(passwordField, password);
		typeTextBoxBy(confirmPasswordField, confirmPassword);
		clickBy(createAccountButton);
	}

	public void reorderFromDetailsPage() {
		clickBy(reorderbutton);
	}

}
