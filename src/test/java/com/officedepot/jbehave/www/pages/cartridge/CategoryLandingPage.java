package  com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CategoryLandingPage extends BasePage {

	private WebElement categoryLink;
	
	
	public boolean diplaySeoContent() throws Exception {
		return isElementPresentBySelenium("id=bottomSeo");
	}

	public boolean diplayRelatedCategories() throws Exception {
		return isElementPresentBySelenium("css=.related_title");
	}
	
	public CategoryLandingPage openCategoryLandingPageInOfficeSupplies(String category) throws Exception {		
		categoryLink = findElementByXpath("(//a[contains(text(),'"+category+"')])[2]");
		clickOn(categoryLink);
		return new CategoryLandingPage();
	}
	
	public CategoryLandingPage openCategoryLandingPageInTechnology(String category) throws Exception {
		categoryLink = findElementByXpath("(//span[contains(text(),'"+category+"')])");
		clickOn(categoryLink);
		return new CategoryLandingPage();
	}
	
	public void openSpecificUrl(String url) throws Exception {
		openUrl(url);
	}
	
	public WebElement chooseLinkTag(String keyWord) throws Exception {
		WebElement navElement = findElementByXpath("//form//a[contains(text(), '" + keyWord + "')]");
		return navElement;
	}

	public boolean isPrevElementPresent() throws Exception {
		return this.selenium.isElementPresent("//head/link[@rel='prev']");
	}

	public boolean isNextElementPresent() throws Exception {
		return this.selenium.isElementPresent("//head/link[@rel='next']");
	}
}
