package com.officedepot.jbehave.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CheckoutBillingPage extends BasePage {

	private By addCouponButton = By.cssSelector("div.b2 > a.button");
	private By couponDialogCloseLink = By.linkText("Close");

	private By address1_1 = By.id("address1-1");
	private By phoneNumber1_1 = By.id("phoneNumber1-1");
	private By phoneNumber2_1 = By.id("phoneNumber2-1");
	private By phoneNumber3_1 = By.id("phoneNumber3-1");
	private By phoneNumber4_1 = By.id("phoneNumber4-1");
	private By city_1 = By.id("city-1");
	private By state_1 = By.id("state-1");
	private By firstName_1 = By.id("firstName-1");
	private By lastName_1 = By.id("lastName-1");
	private By postalCode1_1 = By.id("postalCode1-1");
	private By email_1 = By.id("email-1");
	private By emailConfirm_1 = By.id("emailConfirm-1");
	private By errorMessageAddressPop = By.cssSelector(".errorList");

	private WebElement removeGiftcardButton;
	private By cardIDCVV2 = By.name("paymentFormInfo.creditCardCvv");
	private By cardIDCVV2_input = By.name("paymentFormInfo.creditCardCvvPreauth");
	private By termsConditionsCheckBox = By.name("paymentFormInfo.subscriptionTermsAndConditionsChecked");
	private By creditCardStoreCheckBox = By.name("paymentFormInfo.creditCardStore");
	private WebElement continueCheckOutButton;
	private By pageVerifyItem = By.cssSelector(".oCol1of2.sub_heading.first>h1");
	private By couponDialog = By.id("dialog");
	private By validCouponDisplayContainer = By.cssSelector(".validCouponDisplay");
	private By creditCardRadio = By.id("tenderRadioCR");
	private By creditCardNumber = By.name("paymentFormInfo.creditCardNumber");
	private By expirationDateMonth = By.name("paymentFormInfo.creditCardExpMonth");
	private By expirationDateYear = By.name("paymentFormInfo.creditCardExpYear");

	private By couponCodeField = By.id("referralCode");
	private By giftcardNumberField = By.name("paymentFormInfo.storedValueCardNumber");
	private By giftcardPinNumberField = By.name("paymentFormInfo.storedValueCardPin");
	private By applyGiftcardButton = By.id("applyGiftCard");

	private By cvvToolTip = By.id("js-cidTip");
	private By paypalRadio = By.id("tenderRadioPP");
	private By cashRadio = By.id("tenderRadioCS");
	private By checkRadio = By.id("tenderRadioCH");
	private By billConfirmButton = By.id("confirm");

	private By sourceCodeField = By.name("sourceCode");
	private By poNameField = By.name("poName");
	private By commentTextField = By.name("commentText");
	private By seeOfferDetailsButton = By.id("seeOfferDetails");

	private By changePreauthedCard = By.id("changePreauthedCard");
	private By enterNewCard = By.id("enterNewCard");
	private By errorMsg = By.id("error");

	private By wlrNumber = By.id("wlrNumber");
	private By maxNumber = By.id("odrMemberIdField");
	private By odrMemberInput = By.id("odrMemberIdField");
	private By noLinkedAcct_odrMemberId = By.id("noLinkedAcct_odrMemberId");
	private By applyLoyalty = By.name("cmd_diffLoyalty");
	private By mpMemberInput = By.id("mpMemberIdField");
	private By rdLoyaltyRadioBtn = By.id("rdLoyaltyId_2");

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Billing");
	}

	public void addCouponAtCheckoutBillingStep(String coupon) throws Exception {
		typeTextBoxBy(couponCodeField, coupon);
		clickBy(addCouponButton);

		clickBy(seeOfferDetailsButton);
		// return closeCouponDialog();
	}

	public boolean isCouponAppliedAtCheckoutThirdStep(String couponCode) throws Exception {
		return isTextPresentInElement(validCouponDisplayContainer, couponCode);
	}

	private boolean closeCouponDialog() throws Exception {
		return isTextPresentInElement(couponDialog, "This coupon has been applied to your order.");
	}

	public void applyAGiftcard(String giftcardNum, String pinNumber) throws Exception {
		typeTextBoxBy(giftcardNumberField, giftcardNum);
		typeTextBoxBy(giftcardPinNumberField, pinNumber);
		clickBy(applyGiftcardButton);
	}

	public boolean isGiftcardApplied() throws Exception {
		return isElementPresentBySelenium("id=giftCardsAppliedList");
	}

	public void payByCreditCard() throws Exception {
		InputCreditCard("4111111111111111");
		inputCVVIfNeeded();
	}

	public void InputCreditCard(String cardNumber) throws Exception {
		clickBy(creditCardRadio);
		typeTextBoxBy(creditCardNumber, cardNumber);
		selectOptionInListByText(findElementBy(expirationDateMonth), "12");
		selectOptionInListByText(findElementBy(expirationDateYear), "2018");
	}

	public void inputODCard(String cardNumber) {
		clickBy(creditCardRadio);
		typeTextBoxBy(creditCardNumber, cardNumber);
	}

	public void clickCvvToolTip() {
		clickBy(cvvToolTip);
	}

	public void payByPayPal() throws Exception {
		clickBy(paypalRadio);
	}

	public CheckoutReviewPage continueCheckoutAtThirdStep() throws Exception {
		clickBy(billConfirmButton);
		if (inputCVVIfNeeded())
			clickBy(billConfirmButton);
		return new CheckoutReviewPage();
	}

	public void acceptTermsConditions() throws Exception {
		clickBy(termsConditionsCheckBox);
	}

	public CheckoutReviewPage checkoutWithPaypal() throws Exception {
		continueCheckOutButton = findElementById("cmd_paypal");
		clickOn(continueCheckOutButton);
		return new CheckoutReviewPage();
	}

	public boolean inputCVVIfNeeded() throws Exception {
		if (isElementPresentBySelenium("name=paymentFormInfo.creditCardCvvPreauth") && findElementBy(cardIDCVV2_input).isDisplayed()) {
			typeTextBox(waitForPresenceOfElementLocated(cardIDCVV2_input), "123");
			return true;
		} else if (isElementPresentBySelenium("name=paymentFormInfo.creditCardCvv") && findElementBy(cardIDCVV2).isDisplayed()) {
			typeTextBox(waitForPresenceOfElementLocated(cardIDCVV2), "123");
			return true;
		}
		return false;
	}

	public void changePayment(String payment) {
		if (payment.equals("Cash")) {
			clickBy(cashRadio);
		} else if (payment.equals("Check")) {
			clickBy(checkRadio);
		}
	}

	public void saveCreditCartInformation() {
		clickBy(creditCardStoreCheckBox);
	}

	public boolean diplayAmexImage() {
		return findElementById("cidAmex").isDisplayed();
	}

	public boolean diplayVisaImage() {
		return findElementById("cidVisa").isDisplayed();
	}

	public void clickOnCloseCvvToolTip() {
		for (WebElement b : findElementsBy(By.xpath("//div[@id='cidInstructions']/span"))) {
			if (b.isDisplayed()) {
				clickOn(b);
			}
		}
	}

	public boolean isheBillingInforBlank() {
		return !isElementPresentBySelenium("id=firstName-1");
	}

	public void inputAddressAtCheckoutStep2ExpressReg(String address, String city, String state) {
		typeTextBoxBy(firstName_1, "ODtest");
		typeTextBoxBy(lastName_1, "tester");
		typeTextBoxBy(postalCode1_1, "33496");
		typeTextBoxBy(email_1, "test@t.com");
		typeTextBoxBy(emailConfirm_1, "test@t.com");

		typeTextBoxBy(address1_1, address);
		typeTextBoxBy(city_1, city);
		selectOptionInListByText(findElementBy(state_1), state);
		typeTextBoxBy(phoneNumber1_1, "559");
		typeTextBoxBy(phoneNumber2_1, "999");
		typeTextBoxBy(phoneNumber3_1, "9999");
		typeTextBoxBy(phoneNumber4_1, "9999");

	}

	public boolean checkErrorAddressPopForExpressReg(String errorMessage) {
		return findElementBy(errorMessageAddressPop).getText().contains(errorMessage);
	}

	public boolean isCvvInputBlank() {
		return findElementBy(cardIDCVV2).getAttribute("value").isEmpty() && findElementBy(expirationDateMonth).getAttribute("value").equals(" ")
				&& findElementBy(expirationDateYear).getAttribute("value").equals("-1");
	}

	public boolean isCreditCardBlank() {
		return findElementBy(creditCardNumber).getAttribute("value").isEmpty();
	}

	public void inputCustomerPO() {
		typeTextBoxBy(sourceCodeField, "00539");
		typeTextBoxBy(poNameField, "123456");
		typeTextBoxBy(commentTextField, "testMessage");
	}

	public void changePaymentAtThirdStep() {
		clickBy(changePreauthedCard);
	}

	public void enterNewCardAtThirdStep() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		clickBy(enterNewCard);
	}

	public boolean seeBlankCardInputField() {
		return isElementPresent(creditCardNumber);
	}

	public boolean seeErrorMsgCreditCardNeeded() {
		return findElementBy(errorMsg).getText().contains("Credit Card Number is a required field");
	}

	public boolean seeWarningMessagePaymentInforRequired() {
		return findElementBy(errorMsg).getText().contains("Payment information is required");
	}

	public boolean seeLinkedODRNumber(String num) {
		return isElementPresent(wlrNumber);
		//return findElementBy(wlrNumber).getText().contains(num);
	}

	public boolean seeLinkedMaxNumber(String num) {		
		return findElementBy(maxNumber).getAttribute("value").contains(num);
	}

	public void clickSelectOtherODR() {
		clickBy(rdLoyaltyRadioBtn);
	}

	public boolean seeOtherODRAndOMNumber() {
		return isElementPresent(odrMemberInput);
	}

	public void enterOdrNumber(String num) {
		typeTextBoxBy(odrMemberInput, num);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isYourMemIncorrect() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return isTextPresentOnPage("is incorrect");
	}

	public void removeGigtCard() {
		clickOn(findElementByLinkText("Remove"));
	}

	public boolean seeMsgNotUseCash() {
		return isTextPresentOnPage("available for pickup orders containing configurable / customizable items");
	}

	public void applyOdrNumber() {
		clickBy(applyLoyalty);
		
	}

}
