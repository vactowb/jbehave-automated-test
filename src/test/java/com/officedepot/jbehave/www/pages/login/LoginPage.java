package com.officedepot.jbehave.www.pages.login;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class LoginPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".intro.auto.flcl");

	private By login_UserNameFacebookPage = By.id("email");
	private By login_PasswordFacebookPage = By.id("pass");
	private By login_SubmitLoginFacebookPage = By.name("login");
	private By login_UserNameGooglePage = By.id("Email");
	private By login_PasswordGooglePage = By.id("Passwd");
	private By login_SubmitLoginGooglePage = By.id("signIn");
	private By acceptGoogleButton = By.id("submit_approve_access");
	private WebElement loginLightboxbody;

	private By loginInLink = By.id("headerAccountLogin");
	private By logOutLink = By.xpath("//a[contains(text(),'Logout')]");
	private By accountLink = By.linkText("Account");
	private By facebookIconLink = By.xpath("//div[@gigid='facebook']");
	private By googleIconLink = By.xpath("//div[@gigid='google']");

	public void mouseHoverAccount() {
		mouseHoverOn(findElementBy(accountLink));
	}

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Or log in with your social account:");
	}

	// facebook icon on lightbox
	public void clickGigya() {
		loginLightboxbody = findElementById("loginContentBody");
		List<WebElement> a = loginLightboxbody.findElements(facebookIconLink);
		clickDisplayItem(a);
	}

	// facebook icon on lightbox
	public void clickGigyaGoogle() {
		loginLightboxbody = findElementById("loginContentBody");
		List<WebElement> a = loginLightboxbody.findElements(googleIconLink);
		clickDisplayItem(a);
	}

	public void clickGigyaHomePage() {
		loginLightboxbody = findElementById("accountContent");
		List<WebElement> a = loginLightboxbody.findElements(facebookIconLink);
		clickDisplayItem(a);
	}

	public void clickLoginLink() {
		mouseHoverAccount();
		clickBy(loginInLink);
	}

	public void loginFacebook(String username, String password, String window) throws Exception {
		focusOnNewWindow();
		typeTextBoxBy(login_UserNameFacebookPage, username);
		typeTextBoxBy(login_PasswordFacebookPage, password);
		clickBy(login_SubmitLoginFacebookPage);

		clickCloseFacebookPage();
		switchToMainWindow(window);
	}

	public void loginGoogle(String username, String password, String window) throws Exception {
		focusOnNewWindow();
		typeTextBoxBy(login_UserNameGooglePage, username);
		typeTextBoxBy(login_PasswordGooglePage, password);
		clickBy(login_SubmitLoginGooglePage);
		if (isElementPresentBySelenium("id=submit_approve_access"))
			clickBy(acceptGoogleButton);
		clickCloseFacebookPage();
		switchToMainWindow(window);
	}

	public void clickGigyaFromHomePage() {
		mouseHoverAccount();
		clickGigyaHomePage();
	}

	public void clickCloseFacebookLoginPage(String window) throws Exception {
		focusOnNewWindow();
		clickCloseFacebookPage();
		switchToMainWindow(window);
	}

	// Gigya login need 3 min
	public void waitGigyaLogin() {
		if (null != findChildElementByCSS(findElementById("loginContentBody"), ".button")) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}

	public boolean isLoginSuccessfully() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		mouseHoverAccount();
		return isTextPresentOnPage("Logout");
	}

	private void clickCloseFacebookPage() {
		clickOn(findElementByLinkTextWait("Close Window"));
	}

	private void clickDisplayItem(List<WebElement> webElements) {
		for (WebElement b : webElements) {
			if (b.isDisplayed()) {
				clickOn(b);
			}
		}
	}
}
