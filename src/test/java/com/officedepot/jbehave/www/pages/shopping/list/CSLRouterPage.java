package com.officedepot.jbehave.www.pages.shopping.list;

import com.officedepot.test.jbehave.BasePage;

public class CSLRouterPage extends BasePage {


	public boolean isSkusFromShoppingListAddedToCartSuccessful() throws Exception {
		return findElementById("addedItems") != null;
	}
}
