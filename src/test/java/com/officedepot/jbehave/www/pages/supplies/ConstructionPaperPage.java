package com.officedepot.jbehave.www.pages.supplies;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ConstructionPaperPage extends BasePage {
	private By constructionPaper;
	
	public boolean isInCurrentPage(){
		constructionPaper = By.cssSelector(".lastBreadCrumb");
		return this.isTextPresentInElement(constructionPaper, "Construction Paper");
	}
}
