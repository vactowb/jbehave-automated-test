package com.officedepot.jbehave.www.pages.loyalty;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.register.ExpressRegisterPage;
import com.officedepot.jbehave.www.utils.PromptMessageTestData;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BasePage;

public class JoinAndActiveRewardsPage extends BasePage {

	private static final String JIONANDACTIVEWELCOMEURL = "/a/loyalty-programs/office-depot-rewards/";
	private static final String EMAILID = "email";
	private static final String FIRSTNAMEID = "provide_first_name";
	private static final String LASTNAMEID = "provide_last_name";
	private static final String ADDRESSID = "provide_address";
	private static final String CITYID = "provide_city";
	private static final String STATEID = "state";
	private static final String ZIPCODEID = "provide_zip_code";

	private static final String PASSWORDID = "password";
	private static final String MEMBERSHIPTYPE = "membership_type_join";
	private static final String CONFIRMPASSWORDID = "confirm_password";
	private static final String QUESTIONID = "question";
	private static final String SECURITYANSWERID = "security_answer";
	private static final String ADDRESS = "6600 N MILITARY TRL # 6";

	private static String emailAddress = "";

	private By joinNowButton = By.id("simplified_join");
	private By activateButton = By.id("activate");
	private By memberIdField = By.id("loyMemberId");
	private By memberIdError = By.cssSelector(".errorList");
	private By activateNextButton = By.id("member_id_submit");
	private By haveODAccountY = By.id("account_answer_yes");
	private By haveODAccountN = By.id("account_answer_no");
	private By headerInfo = By.cssSelector(".header_info");
	private By membership_type = By.id(MEMBERSHIPTYPE);
	private By membership_type_error = getLabelWebElement(MEMBERSHIPTYPE);
	private By continueButton = By.cssSelector("input.button.continue");
	private By regOdrFormButton = By.id("regOdrForm");

	private By emailField = By.name(EMAILID);
	private By emailError = getLabelWebElement(EMAILID);

	private By firstNameField = By.id(FIRSTNAMEID);
	private By firstNameError = getLabelWebElement(FIRSTNAMEID);
	private By lastNameField = By.id(LASTNAMEID);
	private By zipCodeFieldError = getLabelWebElement(ZIPCODEID);
	private By zipCodeField = By.id(ZIPCODEID);
	private By phoneError = getLabelWebElement("phone_group");
	public By phone1Field = By.id("phone1");
	public By phone2Field = By.id("phone2");
	public By phone3Field = By.id("phone3");
	private By passwordField = By.name(PASSWORDID);
	private By addressField = By.id(ADDRESSID);

	private By email = By.name("emailAddr");
	private By address = By.name("addr1");

	private By passwordConfirmField = By.id(CONFIRMPASSWORDID);
	private By passwordConfirmError = getLabelWebElement(CONFIRMPASSWORDID);
	private By passwordError = getLabelWebElement("provide_password");
	private By questionSelect = By.id(QUESTIONID);
	private By questionSelectError = getLabelWebElement(QUESTIONID);
	private By answerField = By.id(SECURITYANSWERID);
	private By answerFieldError = getLabelWebElement(SECURITYANSWERID);
	private By emailFoundError = By.id("emailFoundError");
	private By cityField = By.id(CITYID);
	private By state = By.id(STATEID);

	private By userNameLoginField = By.name("loginName");
	private By passwordLoginField = By.name("password");
	private By nextButton = By.xpath("//input[@title='Next']");
	private By loginRewardsLink = By.id("simplified_login");

	private By odrLoginButton = By.cssSelector("input.button_submit.button");

	private By forgotLoyIdLink = By.id("forgotLoyId");
	private By showforgotLogIdLink = By.cssSelector("li.first > a.showModalLoyaltyIdLookup");
	private By forgotLoyPhone0Field = By.id("forgotLoyPhone0");
	private By forgotLoyPhone1Field = By.id("forgotLoyPhone1");
	private By forgotLoyPhone2Field = By.id("forgotLoyPhone2");
	private By forgotLoySubmit = By.xpath("//input[@value='submit']");
	private By forgotLoyIdDialogErrors = By.id("forgotLoyIdDialogErrors");
	private By registerNewLink = By.id("registerNew");
	private By loyaltyPromptContent = By.cssSelector(".loyaltyPromptContent");
	private By loyaltyLinkAccountMsg = By.cssSelector(".loyaltyLinkAccountMsg");
	private By loyaltyWelcome = By.cssSelector(".odr_welcome>h2");
	private By okButton = By.cssSelector("input.button.close-modal-button");
	private By chooseDeliveryMethodButton = By.id("submitDelMethod");
	private By regCodeInput = By.id("regCode");
	private By submitRegCode = By.id("submitRegCode");

	private By memberNumSSI = By.cssSelector(".blue_box");
	private By loginNameSSI = By.name("loginName");
	private By addressSSI = By.name("address1");
	private By citySSI = By.name("city");
	private By postalCodeSSI = By.name("postalCode");
	private By errorLogin = By.cssSelector("ul.errorList");

	public void clickForgotLoyIdLink() {
		sleep2Wait();
		if (isElementPresentBySelenium("link=close") && selenium.isVisible("link=close"))
			selenium.click("link=close");
		clickBy(forgotLoyIdLink);
	}

	public void inputMemberPhoneNum(String phoneNumber) {

		String phoneNumber0 = phoneNumber.substring(0, 3);
		String phoneNumber1 = phoneNumber.substring(3, 6);
		String phoneNumber2 = phoneNumber.substring(6, 10);
		typeTextBoxBy(forgotLoyPhone0Field, phoneNumber0);
		typeTextBoxBy(forgotLoyPhone1Field, phoneNumber1);
		typeTextBoxBy(forgotLoyPhone2Field, phoneNumber2);

		clickBy(forgotLoySubmit);
	}

	public boolean seeForgotLoyErrorMeg(String msg) {
		sleep2Wait();
		Boolean flag = findElementBy(forgotLoyIdDialogErrors).getText().contains(msg);
		clickOn(findElementByCSS(".ui-dialog-titlebar-close.ui-corner-all"));
		return flag;
	}

	public static By getLabelWebElement(String forText) {
		return By.xpath("//label[@class='error'][@for='" + forText + "']");
	}

	public void openWelcomePage() {
		openUrl(JIONANDACTIVEWELCOMEURL);
		if (isElementPresentBySelenium("id=odrChalkBoardDismiss") && findElementById("odrChalkBoardDismiss").isDisplayed())
			clickOn(findElementById("odrChalkBoardDismiss"));
	}

	public boolean isJoinAndActiveWelcomePageOpen() {
		return isElementPresent(joinNowButton);
	}

	public void clickHaveODAccountY() {
		clickBy(haveODAccountY);
	}

	public void clickHaveODAccountN() {
		clickBy(haveODAccountN);
	}

	public void clickJoinNow() {
		clickBy(joinNowButton);
	}

	public void clickContinueButton() {
		sleep2Wait();
		System.out.println(findElementBy(continueButton).isDisplayed());
		if (!findElementBy(continueButton).isDisplayed()) {
			sleep2Wait();
		}
		clickBy(continueButton);
	}

	public static String setEmailAddress() {
		emailAddress = "test" + System.currentTimeMillis() + "@tester.com";
		return emailAddress;
	}

	public void inputAllInfo() {
		setEmailAddress();
		inputInforWithEmail(emailAddress);
	}

	public void inputInforWithConfirmPsw(String password) {
		setEmailAddress();
		inputAllInforBasic(emailAddress, ADDRESS, ExpressRegisterPage.ZIPCODE, "tester1234", password);
	}

	public void inputInforWithEmail(String email) {
		inputAllInforInit(email, ADDRESS, ExpressRegisterPage.ZIPCODE, ExpressRegisterPage.PASSWORD, ExpressRegisterPage.CONFIRMPASSWORD, SeleniumUtilsWWW.randomNum(3), SeleniumUtilsWWW.randomNum(3),
				SeleniumUtilsWWW.randomNum(4), "Personal");
	}

	public void inputInforWithTpye(String type) {
		setEmailAddress();
		inputAllInforInit(emailAddress, ADDRESS, ExpressRegisterPage.ZIPCODE, ExpressRegisterPage.PASSWORD, ExpressRegisterPage.CONFIRMPASSWORD, SeleniumUtilsWWW.randomNum(3),
				SeleniumUtilsWWW.randomNum(3), SeleniumUtilsWWW.randomNum(4), type);
	}

	// phone is used
	public void inputAllInforBasic(String emailAddress, String address, String zipCode, String pwd, String pwdConfirm) {
		seleniumWaitForPageToLoad(1000);
		inputAllInforInit(emailAddress, address, zipCode, pwd, pwdConfirm, "452", "454", "7854", "Personal");
	}

	public void inputAllInforInit(String emailAddress, String address, String zipCode, String pwd, String pwdConfirm, String phone1, String phone2, String phone3, String type) {
		typeTextBoxBy(cityField, "BOCA RATON");
		selectOptionInListByText(findElementBy(state), "FL - Florida");
		selectOptionInListByText(findElementBy(questionSelect), "What was your first job?");
		selectOptionInListByText(findElementBy(membership_type), type);
		typeTextBoxBy(answerField, ExpressRegisterPage.SECURITYANSWER);
		typeTextBoxBy(firstNameField, ExpressRegisterPage.FIRSTNAME);
		typeTextBoxBy(lastNameField, ExpressRegisterPage.LASTNAME);
		typeTextBoxBy(addressField, address);
		typeTextBoxBy(zipCodeField, zipCode);
		typeTextBoxBy(emailField, emailAddress);
		typeTextBox(findElementById("provide_info").findElement(passwordField), pwd);
		typeTextBoxBy(passwordConfirmField, pwdConfirm);
		typeTextBoxBy(phone1Field, phone1);
		typeTextBoxBy(phone2Field, phone2);
		typeTextBoxBy(phone3Field, phone3);

	}

	public void clickPswConfirm() {
		clickBy(passwordConfirmField);
	}

	public boolean isErrorMsgAllReqFieldsNeed() {
		boolean flag = false;
		flag = isPageLoaded(emailError, PromptMessageTestData.loyaltyFieldRequired) && isPageLoaded(firstNameError, PromptMessageTestData.loyaltyFieldRequired)
				&& isPageLoaded(questionSelectError, PromptMessageTestData.loyaltyFieldRequired) && isPageLoaded(membership_type_error, PromptMessageTestData.loyaltyFieldRequired)
				&& isPageLoaded(phoneError, PromptMessageTestData.loyaltyFieldRequired);
		return flag;
	}

	public boolean isErrorMsgPasswordNotMatch() {
		return isPageLoaded(passwordConfirmError, PromptMessageTestData.loyaltyPasswordConfirm);
	}

	public boolean isErrorMsgInvalidEmail() {
		return isPageLoaded(emailError, PromptMessageTestData.loyaltyInvalidEmail);
	}

	public boolean isErrorMsgEmailLinked() {
		clickBy(firstNameField);
		return isPageLoaded(emailFoundError, PromptMessageTestData.loyaltyUsedEmail) || isPageLoaded(By.cssSelector(".errorList"), PromptMessageTestData.loyaltyUsedEmail2);
	}

	public void loginToLinkOdAccount(String loginUsername, String loginPassword) {
		if (isElementPresentBySelenium("id=login_od")) {
			typeTextBox(findElementById("login_od").findElement(userNameLoginField), loginUsername);
			typeTextBox(findElementById("login_od").findElement(passwordLoginField), loginPassword);
			clickOn(findElementByXpath("(//button[@type='submit'])[2]"));
		} else {
			typeTextBoxBy(userNameLoginField, loginUsername);
			typeTextBox(findElementBy(passwordLoginField), loginPassword);
			clickOn(findElementByXpath("//input[@value='Log in']"));
		}

	}

	public boolean isAllRequiredFieldsFilledOut(String username, String phoneNum, String address1) {
		boolean flag = false;
		flag = findElementBy(email).getAttribute("value").toUpperCase().contains(username) && findElementBy(address).getAttribute("value").contains(address1)
				&& phoneNum.contains(findElementByName("suffix").getText());
		return flag;
	}

	public void click2ActivateRewards() {
		clickJoinNow();
		clickHaveODAccountN();
	}

	public void activateRewardsEnterMember(String member) {
		// clickBy(activateButton);
		click2ActivateRewards();
		typeTextBoxBy(memberIdField, member);
		clickBy(emailField);

	}

	public void activateEnterMember(String member) {
		typeTextBoxBy(memberIdField, member);
		clickBy(email);
	}

	public boolean isMemberIdvalid() {
		return isTextPresentOnPage(PromptMessageTestData.loyaltyMemberInvalid);
	}

	public boolean isMemberCanUsed() {
		return isTextPresentOnPage(PromptMessageTestData.logyltyMemberLinked);
	}

	public void click2LoginRewards() {
		clickBy(loginRewardsLink);
	}

	public boolean isLoginRewardsLightboxShow() {
		return isElementPresent(By.name("userNameOrMemberNbr"));
	}

	public void enter2LoginAuth(String loginUsername, String loginPassword) {
		WebElement odrAccountLogin = findElementById("simplifiedLoginEnterUsernameOrNumber");
		WebElement odrLoginMemberId = odrAccountLogin.findElement(By.name("userNameOrMemberNbr"));
		typeTextBox(odrLoginMemberId, loginUsername);
		clickBy(odrLoginButton);
		if (!loginPassword.equals("null")) {
			odrAccountLogin = findElementById("simplifiedLoginEnterPassword");
			WebElement odrLoginPassword = odrAccountLogin.findElement(By.name("password"));
			typeTextBox(odrLoginPassword, loginPassword);
			clickOn(odrAccountLogin.findElement(odrLoginButton));
		}
	}

	public void clickForgotLoyId() {
		clickBy(showforgotLogIdLink);
	}

	public void clickRegisterNew() {
		clickBy(registerNewLink);
	}

	public boolean isRegisterRewardsPageShown() {
		return isElementPresentBySelenium("id=loyMemberId");
	}

	public boolean isMemberIdFilled(String member) {
		sleep2Wait();
		return selenium.getValue("xpath=//input[@name='loyMemberId']").equalsIgnoreCase(member);
	}

	public boolean isActivateRewardsAccountSucc() {
		return findElementById("pagetitle").getText().contains("Account Overview");
	}

	private void sleep2Wait() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isMemberFilledAtAccountLoginPage(String member) {
		sleep2Wait();
		return selenium.getValue("xpath=//input[@name='userNameOrMemberNbr']").equalsIgnoreCase(member);
	}

	public boolean isPhoneInUse() {
		return isPageLoaded(phoneError, PromptMessageTestData.loyaltyUsedPhone);
	}

	public boolean isPasswordMiniReq() {
		return isPageLoaded(passwordError, PromptMessageTestData.loyaltyMiniReqPassword);
	}

	public boolean isErrorLoginShow() {
		return isTextPresentOnPage(PromptMessageTestData.loginFail);
	}

	public boolean isErrorLoyaltyAlreadyAssociated() {
		return isTextPresentOnPage(PromptMessageTestData.loyaltyUserLinked);
	}

	public void makeEmailBlank() {
		typeTextBoxBy(email, "");
		clickContinueButton();
	}

	public boolean isBlankEmailErrorDisplay() {
		return isPageLoaded(emailError, PromptMessageTestData.loyaltyFieldRequired);
	}

	public void inputAllNeedInfoWithInvalidPhoneNumber() {
		setEmailAddress();
		inputAllInforInit(emailAddress, ADDRESS, ExpressRegisterPage.ZIPCODE, ExpressRegisterPage.PASSWORD, ExpressRegisterPage.CONFIRMPASSWORD, "12", "25", "54", "Personal");
	}

	public boolean isPhoneInvalid() {
		return isPageLoaded(phoneError, PromptMessageTestData.loyaltyInvalidPhone);
	}

	public void inputAllNeedInfoWithInvalidZipcode() {
		setEmailAddress();
		inputAllInforInit(emailAddress, ADDRESS, "123", ExpressRegisterPage.PASSWORD, ExpressRegisterPage.CONFIRMPASSWORD, "121", "225", "5454", "Personal");
	}

	public boolean isZipcodeInvalid() {
		return isPageLoaded(zipCodeFieldError, PromptMessageTestData.loyaltyInvalidZipcode);
	}

	public boolean isMemberNumberShow() {
		return findElementById("wlrNumber").isDisplayed();
	}

	public boolean isVerifyInfoModalPopUp() {
		return isElementPresentBySelenium("id=verify_info");
	}

	public boolean isWelcomeFirstnameDisplayed() {
		return isPageLoaded(loyaltyWelcome, "Welcome") && isPageLoaded(loyaltyWelcome, "Tester!");
	}

	public boolean isActivateAndJoinButtonDisplay() {
		// TODO Auto-generated method stub
		return false;
	}

	public void click2TropoOk() {
		sleep2Wait();
		selenium.click("css=input.close-modal-button");
	}

	public boolean isVerifyIdentityLightboxShow() {
		return isPageLoaded(loyaltyPromptContent, PromptMessageTestData.loyaltyVerifyIdentity);
	}

	public boolean isShownChooseDeliveryMethodForCode() {
		return isPageLoaded(loyaltyLinkAccountMsg, PromptMessageTestData.loyaltyRegistrationCode);
	}

	public boolean isUserAlreadyLinked() {
		return isTextPresentOnPage(PromptMessageTestData.loyaltyUserLinked);
	}

	public boolean isInputFieldforMemberShipType(String name) {
		return isTextPresentOnPage(name);
	}

	public void chooseDeliveryMethod() {
		clickBy(chooseDeliveryMethodButton);
	}

	public boolean isRegistrationCodeSent() {
		return isPageLoaded(loyaltyLinkAccountMsg, "An email with a registration code has been sent to the following address");
	}

	public boolean isMembershipTypeFilledIn(String membershipType) {
		return findElementBy(membership_type).getText().contains(membershipType);
	}

	public void inputInvalidRegistrationCode() {
		typeTextBoxBy(regCodeInput, "1111");
		clickBy(submitRegCode);

	}

	public boolean isRegistrationCodeInvalid() {
		return isTextPresentOnPage("There was a problem verifying your identity");
	}

	public boolean isPageSSIDisplay() {
		boolean flag = false;
		System.out.println(findElementBy(loginNameSSI).getAttribute("value").contains("LEAHSMITH122"));
		System.out.println(findElementBy(addressSSI).getAttribute("value").contains("122 PAUL REVERE DR"));
		System.out.println(findElementBy(citySSI).getAttribute("value").contains("HOUSTON"));
		System.out.println(findElementBy(postalCodeSSI).getAttribute("value").contains("770246107"));
		System.out.println(findElementBy(memberNumSSI).getText().contains("1808711178"));

		flag = findElementBy(loginNameSSI).getAttribute("value").contains("LEAHSMITH122") && findElementBy(addressSSI).getAttribute("value").contains("122 PAUL REVERE DR")
				&& findElementBy(citySSI).getAttribute("value").contains("HOUSTON") && findElementBy(postalCodeSSI).getAttribute("value").contains("770246107");
		return flag;
	}

	public boolean isMemberNumDisplay() {
		return findElementBy(memberNumSSI).getText().contains("1808711178");
	}

	public boolean isMsgMemberNumDisplay() {
		for (WebElement element : findElementsBy(errorLogin)) {
			if (element.isDisplayed()) {
				return element.getText().contains("Phone number is linked to another rewards account");
			}
		}
		return false;
	}
}
