package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class FilteredCategoriesByTheFirstBrandPage extends BasePage{

		private By pageVerifyItem = By.cssSelector(".skuList_results");
		private WebElement xMark;

		public boolean isInCurrentPage() throws Exception {
			return isTextPresentInElement(pageVerifyItem, "Results");
		}
		
		public FilteredCategoriesByTheFirstTypePage cancelCurrnetFilterByXMark() throws Exception {
			xMark = findElementByCSS(".refV2.checked>span");
			clickOn(xMark);
			return new FilteredCategoriesByTheFirstTypePage();
		}
		
}
