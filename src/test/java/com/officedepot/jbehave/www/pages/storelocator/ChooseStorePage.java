package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.shopping.cart.ShoppingCartPage;
import com.officedepot.test.jbehave.BasePage;

public class ChooseStorePage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");
	private WebElement firstStoreChoosingButton;
	private WebElement storesIcon;
	
	public void goToChooseStorePage() {
		storesIcon = findElementByXpath("/html/body/div/div[2]/div/div/div[2]/div[3]/ul/li/a");
		clickOn(storesIcon);
	}
	public boolean isNavigateOnThisPage() {
		return isPageLoaded(pageVerifyItem, "Choose a Store");
	}
	
	public void searchStoresByZipCode(String zipCode) throws Exception {
		this.redirectFrame(0);
		WebElement zipCodeInput = findElementByCSS("#inputaddress");
		zipCodeInput.clear();
		typeTextBox(zipCodeInput, zipCode);
		findElementByCSS("#locator_submit2").click();
	}
	
	public ShoppingCartPage chooseFirstStore() throws Exception {
		firstStoreChoosingButton = findElementByXpath("//div[@id='panel']/table/tbody/tr[1]/td[2]/div/span/input");
		clickOn(firstStoreChoosingButton);
		return new ShoppingCartPage();
	}
	
	public boolean canSelectStoreViaRedLink() throws Exception {
		if (isElementPresentBySelenium("//iframe")) {
			return true;
		}
		return false;
	}
	
}
