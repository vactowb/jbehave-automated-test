package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class StoreLinkPage  extends BasePage {

	private By pageVerifyItem = By.cssSelector("#content>div>div");

	public boolean isInCurrentPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}
	
}
