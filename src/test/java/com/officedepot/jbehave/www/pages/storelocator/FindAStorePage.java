package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class FindAStorePage extends BasePage {
	private WebElement chooseThisStore;
	private WebElement chooseOfficeMaxStores;
	private WebElement makeThisMyStoreButton;
	private By onlyShowOfficeMaxStores;
	private WebElement viewAllOfficeDepotLocationsLink;

	public boolean isInCurrentPage() throws Exception {
		
		return isTextPresentInElement(By.cssSelector(".brandColor_tp2"), "Find a Store");
	}

	public void chooseThisStore() throws Exception {
		Thread.sleep(5000);
		chooseThisStore = findElementByCSSWait("#buttontd_91>input");
		clickOn(chooseThisStore);
	}
	
	public LocalStorePage makeThisMyStore() throws Exception {
		makeThisMyStoreButton = findElementByCSS("#buttontd_2471 > input[name='selectStore']");
		clickOn(makeThisMyStoreButton);
		return new LocalStorePage();
	}

	public void chooseOfficeMaxStores() throws Exception {
		chooseOfficeMaxStores = findElementById("btnOM");
		clickOn(chooseOfficeMaxStores);
	}
	
	public boolean isOnlyShowOfficemax() throws Exception {
		onlyShowOfficeMaxStores = By.cssSelector(".showOnlyOMofficemax");
		return this.isElementPresent(onlyShowOfficeMaxStores);
	}
	
	public AllStatesPage clickOnViewAllOfficeDepotLocationsLink() throws Exception {
		viewAllOfficeDepotLocationsLink = findElementByCSS("#content>div>a");
		clickOn(viewAllOfficeDepotLocationsLink);
		return new AllStatesPage();
	}
	
}
