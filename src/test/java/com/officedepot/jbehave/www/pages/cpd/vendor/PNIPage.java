package com.officedepot.jbehave.www.pages.cpd.vendor;

import com.officedepot.test.jbehave.BasePage;

public class PNIPage extends BasePage{
	boolean orderFound;

	public void openPNIPage() {
		openUrl("http://odpus-pms.pnistaging.com/#/orderlist");		
	}
	
	public boolean orderFound(String orderNumber){
		  // PNI does not have a stable UI yet , so for now you cannot see your orders in PNI Queue yet
		  orderFound=false;
		  return orderFound;
	}
	
	public void completeOrder(String orderNumber){

	}
}
