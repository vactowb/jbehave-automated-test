package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CleaningPage  extends BasePage{
	private By pageVerifyItem = By.cssSelector(".lastBreadCrumb.bold");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Cleaning");
	}
}
