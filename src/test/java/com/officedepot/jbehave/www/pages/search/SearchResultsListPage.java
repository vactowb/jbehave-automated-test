package com.officedepot.jbehave.www.pages.search;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.officedepot.test.jbehave.BasePage;

public class SearchResultsListPage extends BasePage {
	
	private WebElement compareButton;
	private List<WebElement> checkboxes;

	public boolean isSortKeyAvailable(String key) {
		WebElement element = findElementBy(By.name("sorter"));
		boolean result = false;
		List<WebElement> options = new Select(element).getOptions();
		for (WebElement option : options) {
			if (option.getText().equalsIgnoreCase(key)) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	public boolean isNavigateToExceptedSearchResults(String searchContent) {
		return this.isTextPresentOnPage(searchContent);
	}
	
	public void selectRecords(int count) throws Exception {
		checkboxes = waitForPresenceOfElementsLocated(By.cssSelector(".sku_comparebox>input"));
		for (int i = 0; i < count; i++) {
			clickOn(checkboxes.get(i));
		}
		
	}
	
	public ProductComparisonPage goToProductComparison() throws Exception {
		compareButton = findElementByName("cmd_compare");
		clickOn(compareButton);
		return new ProductComparisonPage();
	}
	
	public boolean containsSearchBanner() throws Exception {
		WebElement we = null;
		try {
		 we = findElementByCSS(".pb_offers_wrap");
		}
		catch (Exception e){
			// do nothing. Selenium throws exception when element not found
		}
		return  (we != null) ? true : false;
	}
	
	
	public boolean containsCheckboxInLeftNav() throws Exception {
		try {
		     return findElementByXpath("//*[@class='list_refinement']/li/input[@type='checkbox']") == null ? false : true;
		}
		catch(org.openqa.selenium.InvalidSelectorException e){
			return false;
		}	 
	}
	
	public boolean goToRichRelevanceZone() throws Exception {
		if(findElementByXpath("//*[@id='s02']/div/div[8]/div/div") != null)
			return true;
		else
			return false;
		
	}
}
