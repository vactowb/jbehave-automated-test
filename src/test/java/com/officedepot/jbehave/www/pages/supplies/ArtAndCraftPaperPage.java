package com.officedepot.jbehave.www.pages.supplies;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ArtAndCraftPaperPage extends BasePage{
	private By artAndCraft;
	
	public boolean isInCurrentPage(){
		artAndCraft = By.cssSelector(".lastBreadCrumb");
		return this.isTextPresentInElement(artAndCraft, "Art & Craft Paper");
	}
}
