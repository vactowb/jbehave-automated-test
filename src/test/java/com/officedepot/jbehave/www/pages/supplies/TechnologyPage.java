package com.officedepot.jbehave.www.pages.supplies;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.cartridge.AudioCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.CamerasAndCamcordersCategoryLandingPage;
import com.officedepot.jbehave.www.pages.cartridge.TechnologyFeaturedCategoriesPage;
import com.officedepot.test.jbehave.BasePage;

public class TechnologyPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".lastBreadCrumb");
	private WebElement theFirstAdListItem;
	private WebElement shareButton;
	private WebElement viewAllItemsLink;
	private WebElement camerasAndCamcordersCategoryLink;
	private List<WebElement> showAudioCategoriesLink;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Technology");
	}

	public void shareTheFirstAdListItem() throws Exception {
		theFirstAdListItem = findElementByCSSWait(".merch_block.share");
		shareButton = findChildElementByLinkText(theFirstAdListItem, "Share");
		clickOn(shareButton);
	}

	public TechnologyFeaturedCategoriesPage clickOnViewAllItemsLink() throws Exception {
		viewAllItemsLink = findElementByCSS(".cat_all_count>a");
		clickOn(viewAllItemsLink);
		return new TechnologyFeaturedCategoriesPage();
	}

	public CamerasAndCamcordersCategoryLandingPage navigateToCamerasAndCamcordersCategoryLandingPage(String category) throws Exception {
		camerasAndCamcordersCategoryLink = findElementByLinkTextWait(category);
		clickOn(camerasAndCamcordersCategoryLink);
		return new CamerasAndCamcordersCategoryLandingPage();
	}

	public AudioCategoriesPage navigateToAudioPageFromShowAllCategories() throws Exception {
		showAudioCategoriesLink = findElementsByCSS(".cat_arrow.all_cats>li>a");
		clickOn(showAudioCategoriesLink.get(0));
		return new AudioCategoriesPage();
	}

}
