package com.officedepot.jbehave.www.pages.account;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.home.PageHeader;
import com.officedepot.jbehave.www.pages.loyalty.JoinAndActiveRewardsPage;
import com.officedepot.jbehave.www.pages.register.ExpressRegisterPage;
import com.officedepot.jbehave.www.pages.shopping.list.MyListPage;
import com.officedepot.jbehave.www.utils.PromptMessageTestData;
import com.officedepot.jbehave.www.utils.SeleniumUtilsWWW;
import com.officedepot.test.jbehave.BasePage;

public class AccountOverviewPage extends BasePage {

	private JoinAndActiveRewardsPage joinAndActiveRewardsPage;
	private PageHeader pageHeader;
	private WebElement viewAllListsLink;
	private WebElement findOrderLink;
	private List<WebElement> detailContainers;

	private String loginName;
	private String password = SeleniumUtilsWWW.PASSWORD;

	private By editPaymentInfoButton = By.xpath("//div[@id='paymentInfo']/div[2]/div[3]/a");
	private By manageCreditCardsLink = By.xpath("//div[@id='paymentInfo']/div[3]/ul/li[1]/a");
	private By editShippingAddress = By.cssSelector("div#userInfo > div.b1 > a.button");
	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");
	private By addOdrAccountButton = By.cssSelector("#addLoyaltyAcctFromAccountSummary");
	private By odrNumberField = By.id("odrNumber");
	private By submitMemberNumberButton = By.id("submitMemberNumber");
	private By emailPreferencesLink = By.linkText("Email/Mobile Preferences");
	private By emailSubscriptionCheckbox = By.name("displayPromotionalPreferencesForm.inputSubscription[0].optIn");
	private By welcomeMsgText = By.cssSelector(".h28");
	private By loyaltyLinkAccountMsgText = By.cssSelector(".loyaltyLinkAccountMsg");
	private By changeShippingAddress = By.xpath("//a[contains(text(),'Add/Change Address')]");
	private By deleteShippingAddressLink = By.xpath("//a[contains(text(),'Delete')]");
	private By deleteSuccText = By.cssSelector(".greenColor");
	private By loginSettingsLink = By.xpath("//a[contains(text(),'Login Name, Password & Security Question')]");
	private By phoneNumber1_0 = By.id("phoneNumber1-0");
	private By phoneNumber2_0 = By.id("phoneNumber2-0");
	private By phoneNumber3_0 = By.id("phoneNumber3-0");
	private By phoneNumber4_0 = By.id("phoneNumber4-0");
	private By loginNameField = By.name("loginName");
	private By oldPasswordField = By.name("oldPassword");
	private By newPasswordField = By.name("password");
	private By confirmPasswordField = By.name("passwordConfirm");
	private By updateLoginSettingButton = By.id("checkEmailError");
	private By editContactInfoLink = By.cssSelector("#userInfo > div.b1 > a.button");
	private By loyaltyPromptContent = By.cssSelector(".loyaltyNewPromptContent");
	private By forgotLoyIdLink = By.id("forgotLoyId");
	private By forgotLoyIdDialogErrors = By.id("forgotLoyIdDialogErrors");
	private By invalidRewardsNum = By.xpath("//label[@class='error'][@for='odrNumber']");
	private By loyDateOfBirth = By.id("loyDateOfBirth");
	private By changeLocationButton = By.xpath("//a[contains(text(),'Change Location')]");
	private By selectStoreButton = By.id("selectStoreButton0");
	private By mergeMyAccountLink = By.cssSelector(".iCol1of4.omx_manual_merge_link a");
	private By infoHintForMergeAccount = By.id("maxPerksTooltip");
	private By infoHintDIV = By.cssSelector(".odTooltip");
	private By editLoyPhoneLink = By.xpath("//div[@id='editLoyPhone']/span");
	private By editLoyphone1Field = By.id("phoneArea");
	private By editLoyphone2Field = By.id("phonePfx");
	private By editLoyphone3Field = By.id("phoneLastNo");
	private By editPhoneSubmit = By.xpath("//input[@value='submit']");
	private By editLoyPhoneDialogErrors = By.id("editLoyPhoneDialogErrors");
	private By toCurrentAccountLink = By.cssSelector("#addLoyaltyAcctFromAccountSummary a");
	private By accountLinkingDialog = By.cssSelector(".loyaltyNewPromptContent");
	private By joinToCurrentAccountButton = By.cssSelector(".btn.primary");

	public MyListPage viewAllLists() throws Exception {
		detailContainers = waitForPresenceOfElementsLocated(By.cssSelector(".moduleStructContent.modStyle0"));
		viewAllListsLink = findChildElementByCSS(detailContainers.get(detailContainers.size() - 1), ".button.listAllCSL");
		clickOn(viewAllListsLink);
		return new MyListPage();
	}

	public EditPaymentInfoPage editPaymentInfo() throws Exception {
		clickBy(editPaymentInfoButton);
		return new EditPaymentInfoPage();
	}

	public ManageCreditCardsPage manageCreditCards() throws Exception {
		clickBy(manageCreditCardsLink);
		return new ManageCreditCardsPage();
	}

	public LoginSettingPage clickOnLoginSettingsAndSecurityQuestionLink() throws Exception {
		clickBy(loginSettingsLink);
		return new LoginSettingPage();
	}

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Account Overview");
	}

	public void clickAddAccount() {
		clickBy(addOdrAccountButton);
	}

	public void clickAddOdrAccount(String number) {
		typeTextBoxBy(odrNumberField, number);
		clickBy(submitMemberNumberButton);
	}

	public boolean isReceivePromotionsSelected() {
		clickBy(emailPreferencesLink);
		return findElementBy(emailSubscriptionCheckbox).isSelected();
	}

	public boolean checkWelcomeMessage() {
		return isPageLoaded(welcomeMsgText, "Welcome, Odtest!");
	}

	public boolean isPaymentInfoPresent() {
		return isElementPresentBySelenium("paymentInfo");
	}

	// Edit Contact Info button
	public ShippingListDisplayPage editMyShippingInformation() {
		clickBy(editShippingAddress);
		return new ShippingListDisplayPage();
	}

	public boolean checkOdrAlreadyLinked() {
		return findElementBy(loyaltyLinkAccountMsgText).getText().contains("Please verify your Office Depot");
	}

	public boolean SeeChooseDeliveryMethodForRegistration() {
		findElementByIdWait("delMethodPhone");
		return findElementBy(loyaltyLinkAccountMsgText).getText().contains("Please choose the delivery method for your registration code");
	}

	public void changeShippingAddress() {
		clickBy(changeShippingAddress);
	}

	public void deleteShiptoAddress() {
		clickBy(deleteShippingAddressLink);
	}

	public boolean isDeleteShippingAddressSucc() {
		return findElementBy(deleteSuccText).getText().contains("Shipping address was successfully deleted.");
	}

	public ManageCreditCardsPage clickManageCreditCard() {
		clickBy(manageCreditCardsLink);
		return new ManageCreditCardsPage();
	}

	public void editContactInfoLink() {
		clickBy(editContactInfoLink);
	}

	public boolean isPhoneNumberBlank() {
		return findElementBy(phoneNumber1_0).getAttribute("value").equals("135") && findElementBy(phoneNumber2_0).getAttribute("value").equals("555")
				&& findElementBy(phoneNumber3_0).getAttribute("value").equals("6666") && findElementBy(phoneNumber4_0).getAttribute("value").equals("");
	}

	public void clickLoginSetting() {
		clickBy(loginSettingsLink);
	}

	public boolean shouldOpenLoginSettingPage() {
		return findElementBy(loginNameField).getAttribute("value").contains("@");
	}

	public void changeLoginName() {
		loginName = ExpressRegisterPage.setEmailAddress();
		typeTextBoxBy(loginNameField, loginName);
		typeTextBoxBy(oldPasswordField, password);
		typeTextBoxBy(newPasswordField, password);
		typeTextBoxBy(confirmPasswordField, password);

		clickBy(updateLoginSettingButton);
	}

	public void login() {
		pageHeader = new PageHeader();
		try {
			pageHeader.iRequestToLoginFromNewHeader(loginName, password);
		} catch (Exception e) {
		}
	}

	public boolean loginFaultgFromGigay() {
		return false;
	}

	public boolean isMsgNotifySingleSignIn() {
		return findElementBy(loyaltyPromptContent).getText().contains("Two Sites. One Login");
	}

	public void lookupRewardsWithPhone(String phoneNumber) {
		clickBy(forgotLoyIdLink);
		joinAndActiveRewardsPage = new JoinAndActiveRewardsPage();
		joinAndActiveRewardsPage.inputMemberPhoneNum(phoneNumber);
	}

	public boolean isPhoneNumberInvalid() {
		return isPageLoaded(forgotLoyIdDialogErrors, PromptMessageTestData.loyaltyInvalidPhone2);
	}

	public boolean isMemberIdFilled(String memberId) {
		return selenium.getValue("xpath=//input[@name='odrNumber']").equalsIgnoreCase(memberId);
	}

	public boolean iShouldSeeInvalidRewardNum() {
		return isPageLoaded(invalidRewardsNum, "enter a valid rewards number");
	}

	public boolean isViewBirthdayOnMyAccountPage() {
		return findElementBy(loyDateOfBirth).getText().isEmpty();
	}

	public void clickChangeLocation() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

		}

		clickBy(changeLocationButton);
	}

	public boolean isShownSelectStoreButton() {
		return findElementBy(selectStoreButton).getAttribute("value").equals("Select Store");
	}

	public boolean isMergeLinkAppear() {
		return isElementPresent(mergeMyAccountLink);
	}

	public boolean isInfoForMergeLinkAppear() {
		mouseHoverOn(findElementBy(infoHintForMergeAccount));
		return findElementBy(infoHintDIV).isDisplayed();
	}

	public void editRewardsPhone() throws InterruptedException {
		Thread.sleep(1000);
		clickBy(editLoyPhoneLink);
	}

	public void editRewardsPhone(String phoneNumber) {

		String phoneNumber0 = phoneNumber.substring(0, 3);
		String phoneNumber1 = phoneNumber.substring(3, 6);
		String phoneNumber2 = phoneNumber.substring(6, 10);
		typeTextBoxBy(editLoyphone1Field, phoneNumber0);
		typeTextBoxBy(editLoyphone2Field, phoneNumber1);
		typeTextBoxBy(editLoyphone3Field, phoneNumber2);
		clickBy(editPhoneSubmit);
	}

	public boolean errorMsgPhoneUsed() {
		return findElementBy(editLoyPhoneDialogErrors).getText().contains(PromptMessageTestData.loyaltyNumberUsed);
	}

	public void mergeOMXAccount() {
		clickBy(mergeMyAccountLink);
	}

	public void linkToCurrentAccount() {
		clickBy(toCurrentAccountLink);
	}

	public void joinToCurrentAccount() {
		clickBy(joinToCurrentAccountButton);
	}

	public boolean isAccountLinkingDialogAppear() {
		return isElementPresent(accountLinkingDialog);
	}

	public void navigateToFindOrderPage() {
		findOrderLink = findElementsBy(By.cssSelector(".iCol1of4")).get(5).findElement(By.cssSelector("a"));
		clickOn(findOrderLink);
	}

}
