package com.officedepot.jbehave.www.pages.orderhistory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.shopping.FindYourProductPage;
import com.officedepot.test.jbehave.BasePage;

public class ShowingAllOrdersPage extends BasePage {

	private WebElement detailView;
	private final String FROM_DATE = "01/01/2000";
	private final String TO_DATE = "01/01/2015";

	private Map<String, ArrayList<String>> statusMap;

	private WebElement selectAllCheckbox;
	private WebElement deSelectAllCheckbox;
	private By pageVerifyItem = By.cssSelector(".cpd_addMessage>span");

	private By orderStatusField = By.name("orderStatus");
	private By orderTypeField = By.name("orderType");
	private By showButton = By.id("searchButtonV2");
	private By itemStatus = By.cssSelector(".td4.status");

	private By reorderFromShowAllItemButton = By.name("cmd_addCart");
	private By errorListText = By.cssSelector(".errorList");
	private String reorderQty = "";
	private By reorderLink = By.xpath("//a[contains(text(),'Reorder')]");
	private By isShowAllItems = By.id("ohItems");
	private By orderNumberField = By.name("searchValue");

	private List<WebElement> ordersRecords;
	private WebElement nextPageButton;
	private WebElement reorderButton;

	public ShowingAllOrdersPage() {
		ArrayList<String> statusList = new ArrayList<String>();
		statusMap = new HashMap<String, ArrayList<String>>();
		statusList.add("Cancelled");
		statusList.add("Disapproved");
		statusMap.put("Cancelled", statusList);
		statusList = new ArrayList<String>();
		statusList.add("Store Purchase");
		statusMap.put("Store Purchase", statusList);
	}

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Please add the selected items to the cart before moving to the next page");
	}

	public void changeStatusCriteriaAndSearch(String searchStatus) throws Exception {
		WebElement orderStatusList = findElementByCSS("SELECT#cmb-orderstatus");
		selectOptionInListByText(orderStatusList, searchStatus);
		WebElement searchButton = waitForPresenceOfElementsLocated(showButton).get(0);
		clickOn(searchButton);
	}

	public boolean isThereAnySearchResultsForStatusSearch(String searchStatus) throws Exception {
		List<WebElement> results = waitForPresenceOfElementsLocated(itemStatus);
		List<String> statusList = statusMap.get(searchStatus);
		for (WebElement status : results) {
			int i = statusList.size();
			for (String str : statusList) {
				if (str.equalsIgnoreCase(status.getText().trim())) {
					break;
				} else {
					if (i == 1) {
						return false;
					} else {
						continue;
					}
				}
			}
			i--;
		}
		return true;
	}

	public void changeDateCriteriaAndSearch() throws Exception {
		WebElement fromDate = findElementById("fromDate0");
		WebElement toDate = findElementById("toDate0");
		WebElement searchButton = waitForPresenceOfElementsLocated(showButton).get(0);

		typeTextBox(fromDate, FROM_DATE);
		typeTextBox(toDate, TO_DATE);
		clickOn(searchButton);
	}

	public boolean isThereAnySearchResultsForDateSearch() throws Exception {
		List<WebElement> results = waitForPresenceOfElementsLocated(By.cssSelector(".td2.date"));
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date resultDate = new Date();
		Date fromDate = sdf.parse(FROM_DATE);
		Date toDate = sdf.parse(TO_DATE);
		for (WebElement date : results) {
			resultDate = sdf.parse(date.getText().trim());
			if (resultDate.before(fromDate) || resultDate.after(toDate)) {
				return false;
			}
		}
		return true;
	}

	public void searchOrderByOrderType(String orderType) {
		WebElement orderTypeList = waitForPresenceOfElementsLocated(orderTypeField).get(0);
		WebElement searchButton = waitForPresenceOfElementsLocated(showButton).get(0);
		selectOptionInListByText(orderTypeList, orderType);
		clickOn(searchButton);

	}

	public void searchOrderByOrderNumber(String number) {
		typeTextBoxBy(orderNumberField, number);
		clickBy(showButton);
	}

	public boolean iSeeDateSearchResultEmpty() {
		return isElementPresentBySelenium("css=.td3.amount");
	}

	public boolean iSeeOrderTypeShow(String orderType) {
		return findElementByCSS(".order_type").getText().contains(orderType);
	}

	public boolean isInCurrentPage() throws Exception {
		return findElementById("all-orders") != null;
	}

	public String chooseFirstRecord() throws Exception {
		List<WebElement> Records = waitForPresenceOfElementsLocated(By.cssSelector(".iconstyle3"));
		String orderNumber = Records.get(0).getText();
		clickOn(Records.get(0));
		return orderNumber;
	}

	public FindYourProductPage chooseOneRecord() throws Exception {
		List<WebElement> Records = waitForPresenceOfElementsLocated(reorderLink);
		clickOn(Records.get(0));
		return new FindYourProductPage();
	}

	public boolean findorderHistory(String orderNumber, String sku1) {
		findElementByCSS(".trigger.minus");
		detailView = findElementByCSS(".detail-view");
		List<String> item = new ArrayList<String>();
		for (WebElement sku : findElementsByCSS(".sku")) {
			item.add(sku.getText());
		}
		System.out.println(detailView.findElement(By.cssSelector(".current>.noWrap")).getText().equals(orderNumber));
		return detailView.findElement(By.cssSelector(".current>.noWrap")).getText().equals(orderNumber) && item.contains(sku1);
	}

	public void iSearchByOrderNumber(String verifyOrderId) {
		WebElement ordervalue = waitForPresenceOfElementsLocated(By.name("searchValue")).get(0);
		WebElement searchButton = waitForPresenceOfElementsLocated(By.id("searchButtonV2")).get(0);
		typeTextBox(ordervalue, verifyOrderId);
		clickOn(searchButton);
	}

	public void reorderForSpecifiedOrder(String orderNumber) {
		boolean isOrderFound = false;
		while (!isOrderFound) {
			ordersRecords = findElementsByCSS("#all-orders .iconstyle3");
			for (WebElement el : ordersRecords) {
				if (el.getText().contains(orderNumber))
					isOrderFound = true;
				if (isOrderFound && el.getText().equalsIgnoreCase("Reorder")) {
					reorderButton = el;
					clickOn(reorderButton);
					seleniumWaitForPageToLoad(10000);
					return;
				}
			}
			nextPageButton = findElementByCSS("div.next.pg_btn > a");
			clickOn(nextPageButton);
			seleniumWaitForPageToLoad(10000);
		}
	}

	public OrderDetailsPage showAllItems() throws Exception {
		clickOn(findElementByCSS("#tab2>a"));
		return new OrderDetailsPage();
	}

	public void selectAllItems() throws Exception {
		selectAllCheckbox = this.waitForPresenceOfElementLocated(By.xpath("//*[@id='selAll']"));
		clickOn(selectAllCheckbox);
	}

	public boolean isAllItemsSelected() throws Exception {
		List<WebElement> selects = waitForPresenceOfElementsLocated(By.cssSelector(".td8.selectAll>input"));
		return selects.get(0).isSelected() && selects.get(selects.size() - 1).isSelected();
	}

	public void deSelectAllItems() throws Exception {
		deSelectAllCheckbox = findElementById("selAll");
		clickOn(deSelectAllCheckbox);
	}

	public boolean isAllItemsDeselected() throws Exception {
		List<WebElement> selects = waitForPresenceOfElementsLocated(By.cssSelector(".td8.selectAll>input"));
		return !(selects.get(0).isSelected() && selects.get(selects.size() - 1).isSelected());
	}

	public boolean isReorderQTYSameAsLastOrderQTY() throws Exception {
		WebElement reorderQTY = waitForPresenceOfElementLocated(By.name("entryFormList[0].reorderqty"));
		WebElement lastorderQTY = waitForPresenceOfElementLocated(By.name("entryFormList[0].qty"));
		reorderQty = reorderQTY.getAttribute("value");
		return reorderQTY.getAttribute("value").equals(lastorderQTY.getAttribute("value"));
	}

	public void selectFirstRecord() throws Exception {
		List<WebElement> checkboxes = waitForPresenceOfElementsLocated(By.cssSelector(".td8.selectAll>input"));
		clickOn(checkboxes.get(0));
	}

	public void selectInvalidFirstRecord() throws Exception {
		selectFirstRecord();
		WebElement inputQtyField = findElementByName("entryFormList[0].reorderqty");
		typeTextBox(inputQtyField, "-1");
	}

	public void reorderFromShowAllItem() {
		clickBy(reorderFromShowAllItemButton);
	}

	public boolean isSeeErrorMessageFromShowAllItem() {
		return findElementBy(errorListText).getText().contains("You must specify which one you want to reorder");
	}

	public boolean isSeeQtyErrorMessageFromShowAllItem() {
		return findElementBy(errorListText).getText().contains("Reorder Qty must be an integer which larger than 0");
	}

	public boolean isQtySameInCart() {
		WebElement quantity = waitForPresenceOfElementLocated(By.name("cartRow[0].quantity"));
		return quantity.getAttribute("value").equals("1");
	}

	public void selectSkuFromShowAllItemsPage(String sku) {
		for (WebElement webElement : findElementsByXpath("//table[@id='ohItems']/tbody/tr")) {
			if (webElement.getText().contains(sku)) {
				clickOn(webElement.findElement(By.name("reorder_checkbox")));
				break;
			}
		}
	}

	public boolean seeShowAllItemSection() {
		return isElementPresent(isShowAllItems);
	}
}
