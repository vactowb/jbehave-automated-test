package com.officedepot.jbehave.www.pages.supplies;

import com.officedepot.test.jbehave.BasePage;

public class InkAndTonerBrandPage extends BasePage {
	
	private String pageVerifyItem = "css=#pagetitle>h1";
	
	public boolean isNavigateOnThisPage(){
		return isElementPresentBySelenium(pageVerifyItem);
	}
	
}
