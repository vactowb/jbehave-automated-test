package com.officedepot.jbehave.www.pages.richrelevance;

import com.officedepot.test.jbehave.BasePage;

public class CarouselPage extends BasePage {

	public void selectNext() throws Exception {
		clickOn(findElementByXpathWait("/html/body/div[3]/div[10]/div/div/div[10]/div[6]/div/button[2]"));
		clickOn(findElementByXpathWait("/html/body/div[3]/div[10]/div/div/div[10]/div[6]/div/button"));
	}

	public void selectPrev() throws Exception {
		clickOn(findElementByXpathWait("/html/body/div[3]/div[10]/div/div/div[10]/div[7]/div/button[2]"));
		clickOn(findElementByXpathWait("/html/body/div[3]/div[10]/div/div/div[10]/div[7]/div/button"));
	}

	public boolean isCarouselScrollsRight() {
		return isElementPresentBySelenium("/html/body/div[3]/div[10]/div/div/div[10]/div[6]/div/button");
	}

	public boolean isCarouselScrollsLeft() {
		return isElementPresentBySelenium("/html/body/div[3]/div[10]/div/div/div[10]/div[6]/div/button[2]");
	}
}
