package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class AudioCategoriesPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#categoryHeader>h1");

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Audio");
	}
}
