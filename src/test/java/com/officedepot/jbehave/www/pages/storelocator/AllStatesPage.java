package com.officedepot.jbehave.www.pages.storelocator;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class AllStatesPage extends BasePage {

	private By pageVerifyItem = By.cssSelector("#content>div>div");
	private WebElement stateLink;

	public boolean isInCurrentPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}
	
	public CityPage clickOnFirstStateLink() throws Exception {
		stateLink = findElementByLinkText("Alaska (AK)");
		clickOn(stateLink);
		return new CityPage();
	}
	
}
