package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class OnlineCatalogPage extends BasePage {

private By pageVerifyItem = By.cssSelector(".span1");
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Catalog Request");
	}
	
	public void goBackToPreWindow(String window) throws Exception {
		switchToMainWindow(window);
	}
	
}
