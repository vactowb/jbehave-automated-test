package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.others.NoLongerAvailablePage;
import com.officedepot.test.jbehave.BasePage;

public class LocalStorePage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".lsp_my_store");
	private WebElement storeNameElement;
	private WebElement breadScrumb;
	private WebElement breadScrumbFindAStore;
	private WebElement breadScrumbAllStates;
	private WebElement findAnotherStoreLink;
	private WebElement findMoreStoresSearchBox;
	private WebElement findMoreStoresSearchButton;
	private By localServiceIcon;
	private By storeManagerName;
	

	public String getStorePageStoreName() throws Exception {
		storeNameElement = findElementByCSSWait(".lsp_number");
		return getText(storeNameElement);
	}

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "MY STORE");
	}

	public boolean isStoreBrandServiceOccur() throws Exception {
		localServiceIcon = By.cssSelector(".local_store_services");
		return isElementPresent(localServiceIcon);
	}
	
	public boolean isStoreManagerNameOccur() throws Exception {
		storeManagerName = By.cssSelector(".section>h3");
		return isElementPresent(storeManagerName);
	}
	
	public boolean isTheStoreExpected(String storeNumber) throws Exception {
		return getStorePageStoreName().indexOf(storeNumber) != -1;
	}

	public FindAStorePage clickOnBreadScrumbFindAStore() throws Exception {
		breadScrumbFindAStore = findElementByLinkText("Find A Store");
		clickOn(breadScrumbFindAStore);
		return new FindAStorePage();
	}

	public AllStatesPage clickOnBreadScrumbAllStates() throws Exception {
		breadScrumbAllStates = findElementByLinkText("All States");
		clickOn(breadScrumbAllStates);
		return new AllStatesPage();
	}

	public StoreDetailsPage clickOnFindAnotherStoreLink() throws Exception {
		findAnotherStoreLink = findElementByCSS(".find_local_store>a");
		clickOn(findAnotherStoreLink);
		return new StoreDetailsPage();
	}

	public FindAStorePage searchFindMoreStores() throws Exception {
		findMoreStoresSearchBox = findElementById("lspStoreInput");
		typeTextBox(findMoreStoresSearchBox, "33496");
		findMoreStoresSearchButton = findElementById("lspGo");
		clickOn(findMoreStoresSearchButton);
		return new FindAStorePage();
	}
	
	public NoLongerAvailablePage navigateToRedirectLocalStorePageFromUrl() throws Exception {
		openUrl("/storelocator/fl/boca-raton/sdasda/");
		return new NoLongerAvailablePage();
	}

}
