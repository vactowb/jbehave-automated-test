package com.officedepot.jbehave.www.pages.topnavigation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.cartridge.BasicSuppliesCategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.BreakroomPage;
import com.officedepot.jbehave.www.pages.cartridge.CleaningChemicalsPage;
import com.officedepot.jbehave.www.pages.cartridge.CleaningPage;
import com.officedepot.jbehave.www.pages.cartridge.FurniturePage;
import com.officedepot.jbehave.www.pages.cartridge.TapeAndAdhesivesPage;
import com.officedepot.jbehave.www.pages.deals.DealCenterPage;
import com.officedepot.jbehave.www.pages.deals.OfficeSuppliesPage;
import com.officedepot.jbehave.www.pages.emailsignup.EmailSignUpPage;
import com.officedepot.jbehave.www.pages.emailsignup.PrivacyStatementPage;
import com.officedepot.jbehave.www.pages.shopping.list.SchoolSuppliesPage;
import com.officedepot.jbehave.www.pages.sku.SkuDetailPage;
import com.officedepot.jbehave.www.pages.supplies.ArtAndCraftPaperPage;
import com.officedepot.jbehave.www.pages.supplies.InkAndTonerBrandPage;
import com.officedepot.jbehave.www.pages.supplies.InkAndTonerPage;
import com.officedepot.jbehave.www.pages.supplies.InkDepotShowingResultsPage;
import com.officedepot.jbehave.www.pages.supplies.PaperPage;
import com.officedepot.jbehave.www.pages.supplies.TechnologyPage;
import com.officedepot.test.jbehave.BasePage;

public class TopNavigationPage extends BasePage {

	private WebElement officeSuppliesNavigation;
	private WebElement inkAndTonerNavigation;
	private WebElement inkAndTonerNavigatorLink;
	private WebElement searchByCartridgeNumberBox;
	private WebElement searchByCartridgeNumberButton;
	private WebElement searchByPrinterModelBox;
	private WebElement searchByPrinterModelButton;
	private WebElement paperNavigation;
	private WebElement paperNavigatorLink;
	private WebElement schoolSuppliesNavigation;
	private WebElement schoolSuppliesNavigatorLink;
	private WebElement recommendedLink;
	private WebElement officeSuppliesNavigatorLink;
	private WebElement technologyNavigatorLink;
	private WebElement basicSuppliesTextLink;
	private WebElement tapeAndAdhesivesLink;
	private WebElement dealNavigation;
	private WebElement dealsNavigatorLink;
	private WebElement onSaleIcon;
	private WebElement breakroomNavigatorLink;
	private WebElement cleaningNavigatorLink;
	private WebElement privacyPolicyLink;
	private WebElement emailSignUpGoButton;
	private WebElement emailInputBox;
	private WebElement cleaningChemicalsLink;
	private WebElement furnitureNavigatorLink;
	private WebElement dealsFlyoutGoButton;
	private By thankYouForSigningUpMessage;
	private By savingEmailMeassage;
	private By emailSignUpErrorMessage;

	public OfficeSuppliesPage clickOnOfficeSuppliesNavigation() {
		officeSuppliesNavigation = findElementByLinkText("Office Supplies");
		clickOn(officeSuppliesNavigation);
		return new OfficeSuppliesPage();
	}

	public InkAndTonerPage clickOnInkAndTonerNavigation() {
		inkAndTonerNavigation = findElementByLinkText("Ink & Toner");
		clickOn(inkAndTonerNavigation);
		seleniumWaitForPageToLoad(30000);
		return new InkAndTonerPage();
	}

	public DealCenterPage clickOnDealsTopNavigation() {
		dealNavigation = findElementByCSS(".deals_icon.nav_a1");
		clickOn(dealNavigation);
		return new DealCenterPage();
	}

	public void viewInkAndTonerFlyoutOnTopNavigation() {
		inkAndTonerNavigatorLink = findElementByLinkText("Ink & Toner");
		mouseHoverOn(inkAndTonerNavigatorLink);
	}

	public void viewPaperNavigatorOnTopNavigation() {
		paperNavigatorLink = findElementsBy(By.cssSelector(".nav_a1")).get(2);
		mouseHoverOn(paperNavigatorLink);
	}
	
	public void viewDealsNavigatorOnTopNavigation() {
		dealsNavigatorLink = findElementByCSS(".deals_icon.nav_a1");
		mouseHoverOn(dealsNavigatorLink);
		seleniumWaitForPageToLoad(30000);
	}
	
	public void viewCleaningNavigatorOnTopNavigation() {
		cleaningNavigatorLink = findElementByLinkText("Cleaning");
		mouseHoverOn(dealsNavigatorLink);
		seleniumWaitForPageToLoad(30000);
	}

	public SkuDetailPage searchByCartridgeNumber(String cartridgeNumber) {
		searchByCartridgeNumberBox = findElementByIdWait("headerInkSearchFieldCartridge");
		typeTextBox(searchByCartridgeNumberBox, cartridgeNumber);
		searchByCartridgeNumberButton = findElementByIdWait("cartrOrKywdSubmitHeaderCartridge");
		clickOn(searchByCartridgeNumberButton);
		return new SkuDetailPage();
	}

	public InkDepotShowingResultsPage searchByPrinterModel(String printer) throws Exception {
		searchByPrinterModelBox = findElementByIdWait("headerInkSearchFieldPrinter");
		typeTextBox(searchByPrinterModelBox, printer);
		searchByPrinterModelButton = findElementByIdWait("cartrOrKywdSubmitHeaderPrinter");
		clickOn(searchByPrinterModelButton);
		return new InkDepotShowingResultsPage();
	}

	public InkAndTonerBrandPage clickOnBrandsIcon() throws Exception {
		if (findElementByCSSWait(".nav.sfhover") != null) {
			clickOn(findElementsBy(By.cssSelector(".brand_icons>li>a>img")).get(0));
		}
		return new InkAndTonerBrandPage();
	}

	public PaperPage clickOnPaperNavigation() {
		paperNavigation = findElementByLinkText("Paper");
		clickOn(paperNavigation);
		return new PaperPage();
	}
	
	public DealCenterPage clickOnSaleIcon() {
		onSaleIcon = findElementByXpath("//*[@id='modalDealLeft']/div[1]/ul/li[1]/a");
		clickOn(onSaleIcon);
		seleniumWaitForPageToLoad(30000);
		return new DealCenterPage();
	}
	
	public CleaningChemicalsPage clickOnCleaningChemicalsLink() {
		cleaningChemicalsLink = findElementByLinkText("Cleaning Chemicals");
		clickOn(cleaningChemicalsLink);
		seleniumWaitForPageToLoad(30000);
		return new CleaningChemicalsPage();
	}

	public ArtAndCraftPaperPage clickOnTopCategoriesPapersIcon() {
		if (findElementByCSSWait(".nav.sfhover") != null) {
			clickOn(findElementsBy(By.cssSelector(".category_links>li>a>img")).get(1));
		}
		return new ArtAndCraftPaperPage();
	}

	public SchoolSuppliesPage clickOnSchoolSuppliesNavigation() {
		schoolSuppliesNavigation = findElementByLinkText("School Supplies");
		clickOn(schoolSuppliesNavigation);
		return new SchoolSuppliesPage();
	}

	public void viewSchoolSuppliesNavigatorOnTopNavigation() {
		schoolSuppliesNavigatorLink = findElementsBy(By.cssSelector(".nav_a1")).get(7);
		mouseHoverOn(schoolSuppliesNavigatorLink);
	}

	public void viewOfficeSuppliesNavigatorOnTopNavigation() {
		officeSuppliesNavigatorLink = findElementByLinkText("Office Supplies");
		mouseHoverOn(officeSuppliesNavigatorLink);
	}

	public DealCenterPage clickOnRecommendedLink() {
		recommendedLink = findElementByLinkText("Shop All Office Supply Deals");
		clickOn(recommendedLink);
		return new DealCenterPage();
	}

	public PrivacyStatementPage clickOnPrivacyPolicyLink() throws Exception {
		privacyPolicyLink = findElementByLinkText("Privacy Policy");
		clickOn(privacyPolicyLink);
		focusOnNewWindow();
		return new PrivacyStatementPage();
	}
	
	public void inputEmailAndClickOnEmailSignUpGoButton() {
		emailInputBox = findElementById("modalDealInput");
		typeTextBox(emailInputBox, "od@officedepot.com");
		emailSignUpGoButton = findElementByCSS(".button_go.allowMultipleClick");
		clickOn(emailSignUpGoButton);
	}
	
	public TechnologyPage clickOnTechnologyNavigation() {
		technologyNavigatorLink = findElementByLinkText("Technology");
		clickOn(technologyNavigatorLink);
		return new TechnologyPage();
	}

	public BasicSuppliesCategoriesPage navigateToBasicSuppliesCategoriesPage() {
		basicSuppliesTextLink = findElementByLinkText("Basic Supplies");
		clickOn(basicSuppliesTextLink);
		return new BasicSuppliesCategoriesPage();
	}

	public TapeAndAdhesivesPage navigateToTapeAndAdhesivesPageCategoriesPage() {
		tapeAndAdhesivesLink = findElementByLinkText("Tape & Adhesives");
		clickOn(tapeAndAdhesivesLink);
		return new TapeAndAdhesivesPage();
	}

	public BreakroomPage clickOnBreakroomNavigation() {
		breakroomNavigatorLink = findElementByLinkText("Breakroom");
		clickOn(breakroomNavigatorLink);
		return new BreakroomPage();
	}
	
	public Boolean isSavingEmailMeassageOccor() {
		savingEmailMeassage = By.linkText("Sign up for Email Savings");
		return isElementPresent(savingEmailMeassage);
	}
	
	public Boolean isEmailSignUpErrorMessageOccur() {
		return isTextPresentOnPage("This field is required.");
	}
	
	public Boolean isThankYouForSigningUpMessageOccur() {
		return isTextPresentOnPage("Thank you for signing up");
	}
	
	public CleaningPage clickOnCleaningNavigation() {
		cleaningNavigatorLink = findElementByLinkText("Cleaning");
		clickOn(cleaningNavigatorLink);
		return new CleaningPage();
	}
	
	public FurniturePage clickOnFurnitureNavigation() {
		furnitureNavigatorLink = findElementByLinkText("Furniture");
		clickOn(furnitureNavigatorLink);
		return new FurniturePage();
	}
	
	public void clickOnDealsFlyoutGoButton() {
		dealsFlyoutGoButton = findElementByCSS(".button_go.allowMultipleClick");
		clickOn(dealsFlyoutGoButton);
	}
	
}
