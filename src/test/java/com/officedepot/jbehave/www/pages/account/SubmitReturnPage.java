package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class SubmitReturnPage extends BasePage {

	private final String PAGE_TITLE = "Orders";

	private By pageVerifyItem = By.cssSelector("#pagetitle>h1");

	public boolean isInCurrentPage() {
		return isTextPresentInElement(pageVerifyItem, PAGE_TITLE);
	}

}
