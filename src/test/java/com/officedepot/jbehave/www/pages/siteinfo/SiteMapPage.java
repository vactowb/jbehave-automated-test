package com.officedepot.jbehave.www.pages.siteinfo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SiteMapPage extends BasePage{
	private By pageVerifyItem = By.cssSelector(".modStyle1");
	
	public boolean isInCurrentPage(){
	
		return	isTextPresentInElement(pageVerifyItem, "Site Map");
	
	}
}
