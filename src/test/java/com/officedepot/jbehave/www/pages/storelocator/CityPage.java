package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CityPage extends BasePage {
	
	private By pageVerifyItem = By.cssSelector("#content>div>div>a");
	private WebElement cityLink;

	public boolean isInCurrentPage() throws Exception {
		return isElementPresent(pageVerifyItem);
	}
	
	public StoreLinkPage clickOnFirstCityLink() throws Exception {
		cityLink = findElementByLinkText("ANCHORAGE");
		clickOn(cityLink);
		return new StoreLinkPage();
	}
	
}
