package com.officedepot.jbehave.www.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SetSecurityQuestionPage extends BasePage {

	private final String PAGE_TITLE = "Security Question";
	private By pageVerifyItem = By.cssSelector(".h28.brandColor_tp2");
	private WebElement noThanksButton;

	public boolean isInCurrentPage() {
		return isTextPresentInElement(pageVerifyItem, PAGE_TITLE);
	}

	public AccountOverviewPage cancelSetting() {
		noThanksButton = findElementBy(By.name("cancelSubmit"));
		clickOn(noThanksButton);
		return new AccountOverviewPage();
	}

}
