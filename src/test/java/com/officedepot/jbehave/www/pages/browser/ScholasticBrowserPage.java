package com.officedepot.jbehave.www.pages.browser;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ScholasticBrowserPage extends BasePage{
	
	private By pageVerifyItem = By.cssSelector(".brandColor_tp2.vspace_top");
	private By skuDescribe;
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "On The Go Gifts Center");
	}
	
	public boolean isFilteredByScholastic() throws Exception {
		skuDescribe = By.cssSelector(".description>a");
		return isTextPresentInElement(skuDescribe, "Scholastic");
	}
	
	
	
}
