package com.officedepot.jbehave.www.pages.checkout;

import com.officedepot.test.jbehave.BasePage;

public class CartRemovedCouponPage extends BasePage {

	public boolean isCouponRemoved() {
		return isElementPresentBySelenium("css=a.iconstyle2.firepath-matching-node");
	}
}