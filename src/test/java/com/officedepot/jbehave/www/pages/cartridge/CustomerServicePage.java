package com.officedepot.jbehave.www.pages.cartridge;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class CustomerServicePage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".nopad");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Let us help you!");
	}
}
