package com.officedepot.jbehave.www.pages.deals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.cartridge.CategoriesPage;
import com.officedepot.jbehave.www.pages.cartridge.ProductDetailsPage;
import com.officedepot.jbehave.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BasePage;

public class DealCenterPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".brandColor_tp2.vspace_top");
	private WebElement dealsNav;
	private WebElement officeSuppliesNav;
	private WebElement basicSuppliesNav;
	private WebElement couponSavingTab;
	private List<WebElement> adListCartridge;
	private List<WebElement> shopAllLink;
	private WebElement shopCurrentCouponOffersLink;
	private WebElement pageTitle;
	private WebElement adBranding;
	private String productName;
	private String pageTitleName;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Deal Center");
	}

	public void goBackToPreWindow(String window) throws Exception {
		switchToMainWindow(window);
	}

	public OfficeSuppliesPage openOfficeSuppliesPage() throws Exception {
		dealsNav = findElementById("cats");
		officeSuppliesNav = findChildElementByLinkText(dealsNav, "Office Supplies");
		clickOn(officeSuppliesNav);
		return new OfficeSuppliesPage();
	}

	public BasicSuppliesPage openBasicSuppliesPage() throws Exception {
		dealsNav = findElementById("cats");
		basicSuppliesNav = findChildElementByLinkText(dealsNav, "Basic Supplies(3)");
		clickOn(basicSuppliesNav);
		return new BasicSuppliesPage();
	}

	public CouponSavingsPage openCouponSavingsPage() throws Exception {
		couponSavingTab = findElementByLinkText("Coupon Savings");
		clickOn(couponSavingTab);
		return new CouponSavingsPage();
	}

	public String getTheFirstAdlistProductName() throws Exception {
		adListCartridge = findElementsByCSS(".skuLink");
		productName = adListCartridge.get(0).getText();
		return productName;
	}

	public ProductDetailsPage navigateToTheFirstAdListProductDetailsPage() throws Exception {
		adListCartridge = findElementsByCSS(".skuLink");
		clickOn(adListCartridge.get(0));
		return new ProductDetailsPage();
	}

	public CategoriesPage navigateToTheCategoriesPage() throws Exception {
		shopAllLink = findElementsByCSS(".f_right.shopall");
		clickOn(shopAllLink.get(0));
		return new CategoriesPage();
	}

	public CouponCenterPage navigateToTheCouponPageFromShopCurrentCouponOffersLink() throws Exception {
		shopCurrentCouponOffersLink = findElementByCSS(".pb_offers_link");
		clickOn(shopCurrentCouponOffersLink);
		return new CouponCenterPage();
	}

	public String getTitle() throws Exception {
		pageTitle = findElementByCSS(".brandColor_tp2.vspace_top.f_left>span");
		pageTitleName = pageTitle.getText();
		return pageTitleName;
	}

	public HomePage clickOnAdBranding() throws Exception {
		adBranding = findElementByCSS("#dc_top_banner_landing>img");
		clickOn(adBranding);
		return new HomePage();
	}

}
