package com.officedepot.jbehave.www.pages.deals;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class BasicSuppliesPage extends BasePage{
	private By pageVerifyItem = By.cssSelector("#categoryHead");
	
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Basic Supplies");
	}
}
