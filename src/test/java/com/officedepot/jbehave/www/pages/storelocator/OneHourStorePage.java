package com.officedepot.jbehave.www.pages.storelocator;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class OneHourStorePage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".rd");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "FREE PICKUP");
	}

}
