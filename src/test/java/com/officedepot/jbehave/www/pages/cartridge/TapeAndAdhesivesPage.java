package com.officedepot.jbehave.www.pages.cartridge;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class TapeAndAdhesivesPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".gradient_brand>a");
	private List<WebElement> allCategoriesLink = findElementsByCSS("#pageBuilderFootList>div>ul>li>a");

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Tape and Adhesives");
	}

	public TapeAndAdhesivesPage openTapeAndAdhesivesPage() throws Exception {
		this.openUrl("http://wwwsqs8.uschecomrnd.net/a/browse/tape-and-adhesives/N=5+371557/");
		return new TapeAndAdhesivesPage();
	}

	public String getTheFirstCategoriesNameFromAllCategories() throws Exception {
		return getText(allCategoriesLink.get(0));
	}

	public CategoriesPage navigateToTheFirstCategoriesPageFromAllCategories() throws Exception {
		clickOn(allCategoriesLink.get(0));
		return new CategoriesPage();
	}

}
