package com.officedepot.jbehave.www.pages.shopping.list;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.www.pages.cartridge.ShopByGradePage;
import com.officedepot.test.jbehave.BasePage;

public class SchoolListDetailsPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".span1");
	private WebElement listNameTextBox;
	private WebElement deleteListLink;
	private WebElement shopForMoreCategoriesLink;

	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Edit My List");
	}

	public String getListName() throws Exception {
		listNameTextBox = findElementByXpath("//*[@id='m02']/div[2]/div/div[1]/table/tbody/tr[2]/td/input");
		return getValue(listNameTextBox);
	}

	public DeleteListConfirmPage deleteMySchoolList() throws Exception {
		deleteListLink = findElementByXpath("//*[@id='csl_MultiActions']/ul/li[4]/a");
		clickOn(deleteListLink);
		return new DeleteListConfirmPage();
	}

	public ShopByGradePage navigateToShopByGradePage() throws Exception {
		shopForMoreCategoriesLink = findElementByCSS(".buttonwrapper.tppc_links_top>li>a");
		clickOn(shopForMoreCategoriesLink);
		return new ShopByGradePage();
	}
}
