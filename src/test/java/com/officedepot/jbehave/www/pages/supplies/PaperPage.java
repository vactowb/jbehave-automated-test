package com.officedepot.jbehave.www.pages.supplies;

import com.officedepot.test.jbehave.BasePage;

public class PaperPage extends BasePage {
	
	private String pageVerifyItem = "link=Paper";
	
	public boolean isNavigateOnThisPage(){
		return isElementPresentBySelenium(pageVerifyItem);
	}
	
}
