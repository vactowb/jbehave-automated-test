package com.officedepot.jbehave.mobile.www.steps.cart;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.category.ChooseCategoryPage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.shoppingcart.CartPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CartSteps extends BaseStep {

	private CartPage cartPage;
	private ChooseCategoryPage chooseCategoryPage;
	private PageHeader pageHeader;
	private String exceptedQty;

	@When("I navigate to cart from header")
	public void iNavigateToCartFromHeader() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.openCart();
	}

	@Then("I should navigate on shopping cart page")
	public void isAtShoppingCartPage() throws Exception {
		if (cartPage == null) {
			cartPage = new CartPage();
		}
		Assert.assertTrue("--- Fail to navigate on shoppng cart page! ---", cartPage.isNavigateOnPage());
	}

	@When("I update the qty of no. \"$index\" item to \"$qty\"")
	public void updateQtyForFirstSku(int index, String qty) throws Exception {
		cartPage.updateSkuQty(index - 1, qty);
		exceptedQty = qty;
	}

	@Then("I should see the qty of no. \"$index\" item is updated")
	public void compareUpdatedQunityofFirstSku(int index) throws Exception {
		Assert.assertEquals("--- Fail to update quntity! ---", exceptedQty, cartPage.getQtyValue(index - 1));
	}

	@When("I request to continue shopping")
	public void continueToShopping() throws Exception {
		chooseCategoryPage = cartPage.continueToShopping();
	}

	@Then("I should navigate on choose category page")
	public void canContinueToShopping() throws Exception {
		if (chooseCategoryPage == null) {
			chooseCategoryPage = new ChooseCategoryPage();
		}
		Assert.assertTrue("--- Fail to continue to shopping! ---", chooseCategoryPage.isNavigateOnThisPage());
	}

	@When("I remove the no. \"$index\" item")
	public void removeItem(int index) throws Exception {
		cartPage.removeItem(index - 1);
	}

	@Then("I should see the item has been removed")
	public void checkRemovedItem() throws Exception {
		Assert.assertTrue("--- Fail to update quntity for sku item! ---", cartPage.isCartUpdated());
	}

	@When("I save the no. \"$index\" item for later")
	public void saveSkuForLater(int index) throws Exception {
		cartPage.saveItemForLater(index - 1);
	}

	@Then("I should see the item has been saved")
	public void checkSavedItem() throws Exception {
		Assert.assertTrue("--- Fail to save sku item for later! ---", cartPage.isCartUpdated());
	}

	@When("I choose to continue as guest")
	public void checkoutInCartAnonymously() throws Exception {
		cartPage.continueAsGuest();
	}

	@When("I empty my cart")
	public void clearCart() throws Exception {
		cartPage.emptyCart();
	}

	@Then("I should see my cart is empty")
	public void isEmptyForCart() throws Exception {
		Assert.assertTrue("---Fail to save sku item for later!---", cartPage.isEmptyForCart());
	}

	@When("I process check out from shopping cart")
	public void processCheckOut() throws Exception {
		if (cartPage == null) {
			cartPage = new CartPage();
		}
		cartPage.checkOut();
	}

	@When("I choose to login or register")
	public void loginOrRegister() throws Exception {
		cartPage.loginOrRegister();
	}

	@Then("I should navigate to shopping cart page")
	public void navigateToShoppingCart() throws Exception {
		Assert.assertTrue("--- Fail to navigate on shopping cart page! ---", cartPage.isNavigateOnPage());
	}

	@Then("I should see two checkout buttons on page")
	public void iShouldSeeTwoCheckoutButtons() throws Exception {
		Assert.assertEquals(2, cartPage.getCheckoutButtonsNumber());
	}

	@Then("I should see only one checkout button on page")
	public void iShouldSeeOneCheckoutButton() throws Exception {
		Assert.assertEquals(1, cartPage.getCheckoutButtonsNumber());
	}

}
