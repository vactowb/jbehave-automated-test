package com.officedepot.jbehave.mobile.www.pages.store;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class StoreLocatorPage extends BasePage {

	protected By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement locationField;
	private WebElement findStoresButton;
	private WebElement nearMeButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Store Locator");
	}

	public void inputLocation(String location) throws Exception {
		locationField = findElementById("zipCityState");
		typeTextBox(locationField, location);
	}

	public StoreNearPage findStores() throws Exception {
		findStoresButton = findElementById("jst_storeSubmit");
		clickOn(findStoresButton);
		return new StoreNearPage();
	}

	public void findCurrentStore() throws Exception {
		nearMeButton = findElementById("geoLocation");
		clickOn(nearMeButton);
	}

	public boolean displayErrorMessage(String content) {
		return isTextPresentOnPage(content);
	}
}
