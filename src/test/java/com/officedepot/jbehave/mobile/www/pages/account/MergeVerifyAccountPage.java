package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class MergeVerifyAccountPage extends BasePage {

	private WebElement accountInfoField;
	private WebElement submitButton;
	private WebElement cancelButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Your combined account will use your Office Depot login and password");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void submit() {
		submitButton = findElementByCSS(".button.b1.f_right");
		clickOn(submitButton);
	}

	public void cancel() {
		cancelButton = findElementByCSS(".button.f_left");
		clickOn(cancelButton);
	}

	public void fillAccountInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputAccountField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputAccountField(String field, String value) {
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		if (field.equalsIgnoreCase("username")) {
			accountInfoField = findElementById("mergeAcct");
		}
		if (field.equalsIgnoreCase("password")) {
			accountInfoField = findElementById("password1");
		}
		typeTextBox(accountInfoField, value);
	}
}
