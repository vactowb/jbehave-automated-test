package com.officedepot.jbehave.mobile.www.pages.category;

import java.util.List;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ChooseCategoryPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private List<WebElement> categoryListItems;
	private List<WebElement> addToCartButtons;
	private WebElement categoryListItem;
	private WebElement brandSelectList;
	private WebElement modelSelectList;
	private WebElement recentSearchSelectList;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Choose a Category");
	}

	public boolean isNavigateOnExceptedCategory(String text) throws Exception {
		if (findElementByCSS(".lastBreadCrumb").getText().equals(text)) {
			return true;
		}
		return false;
	}

	public void selectFromCategoryList(int index) throws Exception {
		categoryListItems = findElementsByCSS(".ell");
		clickOn(categoryListItems.get(index));
	}

	public void selectFromCategoryListByText(String text) throws Exception {
		categoryListItem = findElementByLinkText(text);
		clickOn(categoryListItem);
	}

	public void addFromResultsList(int index) throws Exception {
		addToCartButtons = findElementsByName("cmd_addCart.button");
		clickOn(addToCartButtons.get(index));
	}

	public void selectInk(ExamplesTable infoTable) throws Exception {
		brandSelectList = findElementById("js_brandId");
		selectOptionInListByText(brandSelectList, infoTable.getRow(0).get("Brand"));
		seleniumWaitForPageToLoad(30000);
		modelSelectList = findElementById("js_modelId");
		selectOptionInListByText(modelSelectList, infoTable.getRow(0).get("Model"));
		seleniumWaitForPageToLoad(30000);
	}

	public boolean isFinderResultsExcepted(String content) throws Exception {
		return isTextPresentInElement(By.cssSelector(".pad_left.line_bottom"), content);
	}

	public void selectRecentSearch(String content) throws Exception {
		recentSearchSelectList = findElementById("js_brandModel");
		selectOptionInListByText(recentSearchSelectList, content);
	}

}
