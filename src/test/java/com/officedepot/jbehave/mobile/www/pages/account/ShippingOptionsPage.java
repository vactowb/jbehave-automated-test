package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ShippingOptionsPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private List<WebElement> addressEditButtons;
	private WebElement cancelButton;
	private WebElement addNewShippingAddressButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Shipping Options");
	}

	public EditShippingAddressPage editCurrentAddress() {
		addressEditButtons = findElementsByCSS(".f_right.arrow_right");
		clickOn(addressEditButtons.get(0));
		seleniumWaitForPageToLoad(30000);
		return new EditShippingAddressPage();
	}

	public void cancelEditShippingInfo() {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
	}

	public AddNewShippingAddressPage addNewShippingAddress() {
		addNewShippingAddressButton = findElementByCSS(".button.b1.display_block");
		clickOn(addNewShippingAddressButton);
		return new AddNewShippingAddressPage();
	}

}
