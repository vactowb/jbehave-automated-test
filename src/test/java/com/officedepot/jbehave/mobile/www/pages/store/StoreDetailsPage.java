package com.officedepot.jbehave.mobile.www.pages.store;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.deals.WeeklyAdsPage;
import com.officedepot.test.jbehave.BasePage;

public class StoreDetailsPage extends BasePage {

	private WebElement changeMyStoreLink;
	private WebElement setAsMyStoreButton;
	private WebElement viewWeeklyAdsButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Store Details");
	}

	public boolean isStoreSetAsMyStore() {
		return isTextPresentOnPage("Store successfully set as My Store");
	}

	public boolean isMyStore() {
		return isTextPresentInElement(By.cssSelector(".button"), "My Store");
	}

	public String getStoreNumber() {
		String store = getText(findElementByCSS(".guide2.sub_title_3"));
		return store.substring(store.indexOf("#") + 1, store.length());
	}

	public StoreLocatorPage changeMyStore() {
		changeMyStoreLink = findElementByCSS("a.no_decoration");
		clickOn(changeMyStoreLink);
		return new StoreLocatorPage();
	}

	public void setAsMyStore() {
		setAsMyStoreButton = findElementByCSS("a.button.b1");
		clickOn(setAsMyStoreButton);
	}

	public WeeklyAdsPage viewWeeklyAds() {
		viewWeeklyAdsButton = findElementByLinkText("View Weekly Ads");
		clickOn(viewWeeklyAdsButton);
		return new WeeklyAdsPage();
	}

}
