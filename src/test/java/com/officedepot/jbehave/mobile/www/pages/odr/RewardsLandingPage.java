package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class RewardsLandingPage extends BasePage {

	private WebElement joinButton;
	private WebElement createAccountButton;
	private WebElement maxperksLink;
	private By pageVerifyItem = By.cssSelector(".page_title");

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "My Rewards");
	}

	public CreateAccountPage createAccount() {
		if (findElementsByCSS(".button.loyaltyBtn1.uppercase.center.display_block.vspace_bottom").size() == 2) {
			createAccountButton = findElementsByCSS(".button.loyaltyBtn1.uppercase.center.display_block.vspace_bottom").get(0);
		} else {
			createAccountButton = findElementsByCSS(".button.loyaltyBtn1.uppercase.center.display_block.vspace_bottom").get(1);
		}
		clickOn(createAccountButton);
		return new CreateAccountPage();
	}

	public void join() {
		if (findElementsByCSS(".button.loyaltyBtn1.uppercase.center.display_block.vspace_bottom").size() == 2) {
			joinButton = findElementsByCSS(".button.loyaltyBtn1.uppercase.center.display_block.vspace_bottom").get(1);
		} else {
			joinButton = findElementsByCSS(".button.loyaltyBtn1.uppercase.center.display_block.vspace_bottom").get(2);
		}
		clickOn(joinButton);
	}

	public MaxperksRedirectPage transformMaxperks() {
		maxperksLink = findElementByCSS(".vspace_top.center>a");
		clickOn(maxperksLink);
		return new MaxperksRedirectPage();
	}

}
