package com.officedepot.jbehave.mobile.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.EditShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.giftcard.GiftCardPage;
import com.officedepot.test.jbehave.BasePage;

public class ReviewOrderPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement paymentSection;
	private WebElement billingInfoSection;
	private WebElement changeBillingLink;
	private WebElement shippingInfoSection;
	private WebElement changeShippingLink;
	private WebElement editShippingLink;
	private WebElement changePaymentLink;
	private WebElement cvvField;
	private WebElement lightingBoxPlaceOrderButton;
	private WebElement placeOrderButton;
	private WebElement changeMethodLink;
	private WebElement backToReviewOrderButton;
	private WebElement giftCardSection;
	private WebElement addGiftCardLink;
	private WebElement removeGiftCardLink;
	private WebElement addNewShippingAddressLink;
	private WebElement enterNewPaymentLink;
	private WebElement editPaymentLink;

	public ReviewOrderPage() {
		seleniumWaitForPageToLoad(3000);
	}

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Review & Order") && isElementPresentBySelenium("css=.f_left.step4.active");
	}

	public GiftCardPage applyGiftCard() throws Exception {
		expandGiftCardScetion();
		addGiftCardLink = findElementByLinkText("Add");
		clickOn(addGiftCardLink);
		return new GiftCardPage();
	}

	public boolean isGiftCardAppliedSuccessfully() throws Exception {
		return isElementPresentBySelenium("link=Remove");
	}

	public void expandGiftCardScetion() {
		giftCardSection = findElementsByCSS(".jst_accordion_trigger").get(2);
		clickOn(giftCardSection);
	}

	public void removeGiftCard() throws Exception {
		expandGiftCardScetion();
		removeGiftCardLink = findElementByLinkText("Remove");
		clickOn(removeGiftCardLink);
	}

	public boolean isGiftCardRemoved() throws Exception {
		return isTextPresentOnPage("If you have a Gift/Rewards card, please enter the card number and PIN");
	}

	public boolean addPayPalSuccess() throws Exception {
		return isElementPresentBySelenium("css=span.display_block.paypal_small");
	}

	public void placeOrder() throws Exception {
		placeOrderButton = findElementByXpath("(//input[@value='Place Order'])[1]");
		clickOn(placeOrderButton);
	}

	public void expandPaymentSection() {
		paymentSection = findElementByCSS("#paymentInformation > a.jst_accordion_trigger");
		clickOn(paymentSection);
	}

	public PaymentOptionsPage changePayment() {
		expandPaymentSection();
		changePaymentLink = findElementByLinkText("Change");
		clickOn(changePaymentLink);
		return new PaymentOptionsPage();
	}

	public EditPaymentPage editPayment() {
		expandPaymentSection();
		editPaymentLink = findElementByCSS("#paymentInformation > div.collapse_content.guide2 > a.f_right.right_option");
		clickOn(editPaymentLink);
		return new EditPaymentPage();
	}

	public ThankYouPage enterSecurityCode() {
		cvvField = findElementByXpath("(//input[@id='cid'])[2]");
		typeTextBox(cvvField, "123");
		lightingBoxPlaceOrderButton = findElementByXpath("(//input[@value='Place Order'])[5]");
		clickOn(lightingBoxPlaceOrderButton);
		waitForAjaxCall(10000);
		return new ThankYouPage();
	}

	public ThankYouPage confirmChange() {
		lightingBoxPlaceOrderButton = findElementByXpath("(//input[@id='placeOrder'])[2]");
		clickOn(lightingBoxPlaceOrderButton);
		waitForAjaxCall(10000);
		return new ThankYouPage();
	}

	public boolean isSecurityCodeBoxPresent() {
		return isElementPresentBySelenium("//div[@id='boxesTopContent']/span[1]/b");
	}

	public boolean isConfirmChangeBoxPresent() {
		return isElementPresentBySelenium("css=.box_title");
	}

	public PaymentOptionsPage changePaymentMethodFromCVVBox() {
		changeMethodLink = findElementByCSS("#boxesBottomContent > div.box_bottom > a.no_decoration");
		clickOn(changeMethodLink);
		return new PaymentOptionsPage();
	}

	public void backToReviewOrder() {
		backToReviewOrderButton = findElementByXpath("(//a[contains(text(),'Back to Review Order')])[3]");
		clickOn(backToReviewOrderButton);
	}

	public EditBillingInfoPage changeBillingInfo() {
		billingInfoSection = findElementByCSS("a.jst_accordion_trigger");
		clickOn(billingInfoSection);
		changeBillingLink = findElementByCSS("a.f_right.right_option");
		clickOn(changeBillingLink);
		return new EditBillingInfoPage();
	}

	public void expandShippingSection() {
		shippingInfoSection = findElementsByCSS(".jst_accordion_trigger").get(3);
		clickOn(shippingInfoSection);
	}

	public ShippingOptionsPage changeShippingInfo() {
		expandShippingSection();
		changeShippingLink = findElementByLinkText("Change");
		clickOn(changeShippingLink);
		return new ShippingOptionsPage();
	}

	public EditShippingAddressPage editShippingInfo() {
		expandShippingSection();
		editShippingLink = findElementByLinkText("Edit");
		clickOn(editShippingLink);
		return new EditShippingAddressPage();
	}

	public NewShippingAddressPage enterNewShippingAddress() {
		expandShippingSection();
		addNewShippingAddressLink = findElementByXpath("(//a[contains(text(),'Enter New')])[2]");
		clickOn(addNewShippingAddressLink);
		return new NewShippingAddressPage();
	}

	public NewPaymentPage enterNewPayment() {
		expandPaymentSection();
		enterNewPaymentLink = findElementByLinkText("Enter New");
		clickOn(enterNewPaymentLink);
		return new NewPaymentPage();
	}
}
