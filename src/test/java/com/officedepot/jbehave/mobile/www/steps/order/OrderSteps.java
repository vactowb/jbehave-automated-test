package com.officedepot.jbehave.mobile.www.steps.order;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.account.ManageOrdersPage;
import com.officedepot.jbehave.mobile.www.pages.account.OrderDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.account.TrackOrdersPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.ThankYouPage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class OrderSteps extends BaseStep {

	private PageHeader pageHeader;
	private ManageOrdersPage manageOrdersPage;
	private OrderDetailsPage orderDetailsPage;
	private TrackOrdersPage trackOrdersPage;
	private ThankYouPage thankYouPage;

	private String expectedOrderNumber;

	@When("I request to track orders from header")
	public void checkOrderHistory() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.trackOrders();
	}

	@Then("I should navigate on manage orders page")
	public void displayOrderHistory() throws Exception {
		if (manageOrdersPage == null) {
			manageOrdersPage = new ManageOrdersPage();
		}
		Assert.assertTrue("---Fail to redirect manage orders page!---", manageOrdersPage.isNavigateOnThisPage());
	}

	@When("I request to view details for the first order")
	public void viewOrder() throws Exception {
		if (manageOrdersPage == null) {
			manageOrdersPage = new ManageOrdersPage();
		}
		this.setParameter("order number", manageOrdersPage.getFirstOrderNumber());
		orderDetailsPage = manageOrdersPage.viewFirstOrderDetails();
	}

	@When("I request to cancel the first order from manage orders page")
	public void cancelOrder() throws Exception {
		manageOrdersPage.cancelOrder(0);
	}

	@Then("I should see the order cancelled")
	public void displayOrderCancelledMessage() throws Exception {
		Assert.assertTrue("---Fail to cancel order!---", manageOrdersPage.isMessageDisplayed("Order Cancelled, Order Number " + (String) this.getParameter("order number")));
	}

	@When("I request to view next page")
	public void nextPage() throws Exception {
		manageOrdersPage.nextPage();
	}

	@Then("I should see the previous page link")
	public void getPrevLink() throws Exception {
		Assert.assertTrue("---Fail to get Pre link!", manageOrdersPage.getPrevLink());
	}

	@Then("I should see the first page link")
	public void getFirstlink() throws Exception {
		Assert.assertTrue("---Fail to get first link!", manageOrdersPage.getFirstlink());
	}

	@When("I request to view previous page")
	public void prevPage() throws Exception {
		manageOrdersPage.prevPage();
	}

	@When("I request to view first page")
	public void firstPage() throws Exception {
		manageOrdersPage.firstPage();
	}

	@Then("I should see the next page link")
	public void getNextLink() throws Exception {
		Assert.assertTrue("---Fail to get Next link!", manageOrdersPage.getNextLink());
	}

	@When("I request to reorder the first order")
	public void reorder() throws Exception {
		manageOrdersPage.reorder(0);
	}

	@Then("I should navigate on track orders page")
	public void iShouldNavigateOnTrackOrdersPage() throws Exception {
		if (trackOrdersPage == null) {
			trackOrdersPage = new TrackOrdersPage();
		}
		Assert.assertTrue("--- Fail to redirect track orders page! ---", trackOrdersPage.isNavigateOnThisPage());
	}

	@When("I request to track order with following info: $infoTable")
	public void iRequestToTrackOrderWithFollowingInfo(ExamplesTable infoTable) throws Exception {
		trackOrdersPage.fillOrderInfo(infoTable);
		expectedOrderNumber = trackOrdersPage.getInputtedOrderNumber();
		trackOrdersPage.submit();
	}

	@Then("I should be noticed that the order number with invalid format")
	public void iShouldBeNoticedThatTheOrderNumberWithInvalidFormat() throws Exception {
		Assert.assertTrue("--- Fail to see error message! ---", trackOrdersPage.isMessageDisplayed("Please enter Order Numbers in the following format"));
	}

	@Then("I should be noticed that the order number is invalid")
	public void iShouldBeNoticedThatTheOrderNumberIsInvalid() throws Exception {
		Assert.assertTrue("--- Fail to see error message! ---", trackOrdersPage.isMessageDisplayed("We could not find an order matching the number"));
	}

	@Then("I should be noticed that the phone number is invalid")
	public void iShouldBeNoticedThatThePhoneNumberIsInvalid() throws Exception {
		Assert.assertTrue("--- Fail to see error message! ---", trackOrdersPage.isMessageDisplayed("The Phone Number or Account Number Provided did not match that on the Order Requested"));
	}

	@Then("I should be noticed that the account number is invalid")
	public void iShouldBeNoticedThatTheAccountNumberIsInvalid() throws Exception {
		Assert.assertTrue("--- Fail to see error message! ---", trackOrdersPage.isMessageDisplayed("The Phone Number or Account Number Provided did not match that on the Order Requested"));
	}

	@Then("I should navigate on expected order details page")
	public void iShouldNavigateOnExpectedOrderDetailsPage() throws Exception {
		if (orderDetailsPage == null) {
			orderDetailsPage = new OrderDetailsPage();
		}
		Assert.assertTrue("---Fail to navigate on expected order details page!---", orderDetailsPage.isNavigateOnThisPage() && orderDetailsPage.isOrderExpected(expectedOrderNumber));
	}

	@When("I request to login to track orders")
	public void iRequestToLoginToTrackOrders() throws Exception {
		trackOrdersPage.loginToTrackOrders();
	}

	@When("I submit to track order with no info inputted")
	public void trackOrderWithNoInfoInputted() throws Exception {
		trackOrdersPage.submit();
	}

	@Then("I should be noticed to fill the required field")
	public void iShouldBeNoticedToFillTheRequiredFiled() throws Exception {
		Assert.assertTrue("--- Fail to see error message! ---", trackOrdersPage.isMessageDisplayed("Order Number is Required"));
	}

	@Then("I should see no orders on manage orders page")
	public void iShouldNoOrder() throws Exception {
		if (manageOrdersPage == null) {
			manageOrdersPage = new ManageOrdersPage();
		}
		Assert.assertTrue("--- The order list is not blank! ---", manageOrdersPage.isMessageDisplayed("You currently have no orders"));
	}

	@Then("I should see my placed order on manage orders page")
	public void iShouldSeeMyOrder() throws Exception {
		if (manageOrdersPage == null) {
			manageOrdersPage = new ManageOrdersPage();
		}
		Assert.assertTrue("--- Failed to see my placed order! ---", manageOrdersPage.isOrderOnList((String) this.getParameter("order number")));
	}

	@When("I request to view order details from thank you page")
	public void viewOrderFromThankYouPage() throws Exception {
		thankYouPage = new ThankYouPage();
		expectedOrderNumber = (String) this.getParameter("order number");
		orderDetailsPage = thankYouPage.viewOrderDetails(0);
	}

	@When("I request to cancel order from order details page")
	public void iRequestToCancelOrderFromOrderDetailsPage() throws Exception {
		orderDetailsPage.cancelOrder();
	}

	@Then("I should see the cancel order light box")
	public void iShouldSeeTheCancelOrderLightBox() throws Exception {
		Assert.assertTrue("--- Failed to see cancel order light box! ---", orderDetailsPage.isCancelOrderBoxDisplayed());
	}

	@Then("I should see the cancel order light box on manage orders page")
	public void iShouldSeeTheCancelOrderLightBoxOnManageOrdersPage() throws Exception {
		Assert.assertTrue("--- Failed to see cancel order light box! ---", manageOrdersPage.isCancelOrderBoxDisplayed());
	}

	@When("I reject to cancel order")
	public void iRejectToCancelOrder() throws Exception {
		orderDetailsPage.rejectCancelOrder();
	}

	@When("I reject to cancel order from manage orders page")
	public void iRejectToCancelOrderFromManageOrdersPage() throws Exception {
		manageOrdersPage.rejectCancelOrder();
	}

	@Then("I should see the order is not cancelled")
	public void iShouldSeeTheOrderIsNotCancelled() throws Exception {
		Assert.assertTrue("--- The order is already cancelled! ---", orderDetailsPage.isOrderCanBeCancelled());
	}

	@Then("I should see the first order is not cancelled")
	public void iShouldSeeTheFirstOrderIsNotCancelled() throws Exception {
		Assert.assertTrue("--- The first order is already cancelled! ---", manageOrdersPage.isOrderCanBeCancelled());
	}

	@When("I accept to cancel order")
	public void iAcceptToCancelOrder() throws Exception {
		orderDetailsPage.confirmCancelOrder();
	}

	@When("I accept to cancel order from manage orders page")
	public void iAcceptToCancelOrderFromManageOrderPage() throws Exception {
		manageOrdersPage.confirmCancelOrder();
	}

	@Then("I should see the order is cancelled successfully")
	public void iShouldSeeTheOrderIsCancelledSuccessfully() throws Exception {
		Assert.assertTrue("--- Failed to cancel order! ---", orderDetailsPage.isOrderCancelledSuccessfully());
	}

	@When("I close the cancel order light box")
	public void iCloseTheCancelOrderLightBox() throws Exception {
		orderDetailsPage.closeCancelOrderBox();
	}

	@When("I close the cancel order light box from manage orders page")
	public void iCloseTheCancelOrderLightBoxFromManageOrdersPage() throws Exception {
		manageOrdersPage.closeCancelOrderBox();
	}

	@When("I navigate to manage orders from bread crumb")
	public void iNavigateToManageOrdersFromBreadCrumb() throws Exception {
		orderDetailsPage.goToManageOrdersFromBreadCrumb();
	}

	@When("I request to reorder items from order details page")
	public void iRequestToReorderItmesFromOrderDetailsPage() throws Exception {
		orderDetailsPage.reorder();
	}

	@Then("I shoulde see the order status is \"$status\" on order details page")
	public void iShouldSeeOrderStatusOnOrderDetailsPage(String status) throws Exception {
		Assert.assertTrue("--- Failed to verify the order status! ---", orderDetailsPage.getOrderStatus().equalsIgnoreCase(status));
	}

	@Then("I should see the first order status is \"$status\" on manage orders page")
	public void iShouldSeeOrderStatusOnManageOrdersPage(String status) throws Exception {
		Assert.assertTrue("--- Failed to verify the order status! ---", manageOrdersPage.getFirstOrderStatus().equalsIgnoreCase(status));
	}

}
