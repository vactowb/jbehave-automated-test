package com.officedepot.jbehave.mobile.www.pages.login;


public class LinkedinAccountLogInPage extends SocialAccountLogInPage {
	 
    @Override
    public void logIn(String accountName, String pwd) {
            typeTextBox(findElementById("session_key-oauthAuthorizeForm"), accountName);
            typeTextBox(findElementById("session_password-oauthAuthorizeForm"), pwd);
            clickOn(findElementByXpath("//input[@value='Sign in and allow']"));
    }

}
