package com.officedepot.jbehave.mobile.www.pages.store;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.deals.WeeklyAdsPage;
import com.officedepot.test.jbehave.BasePage;

public class StoreNearPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private List<WebElement> storeTitleLinks;
	private List<WebElement> viewWeeklyAdsLinks;
	private WebElement setAsMyStoreButton;
	private WebElement refineBoxButton;
	private WebElement refineButton;
	private WebElement refineServiceCheckBox;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Stores Near");
	}

	public boolean isStoreNearTheLocation(String location) {
		return isTextPresentOnPage(location);
	}

	public StoreDetailsPage viewDetails(int index) throws Exception {
		storeTitleLinks = findElementsByCSS(".bold.display_block.arrow_primary_right");
		clickOn(storeTitleLinks.get(index));
		return new StoreDetailsPage();
	}

	public WeeklyAdsPage viewWeeklyAds() throws Exception {
		viewWeeklyAdsLinks = findElementsByCSS(".f_right.paddedText.h2.bold.blue_text");
		clickOn(viewWeeklyAdsLinks.get(0));
		return new WeeklyAdsPage();
	}

	public String setAsMyStore(int index) {
		setAsMyStoreButton = findElementByXpath("//*[@id='storeResults']/ol/li[" + index + "]/div/section[4]/a[1]");
		String store = getText(findElementByXpath("//*[@id='storeResults']/ol/li[" + index + "]/div/h2/a"));
		clickOn(setAsMyStoreButton);
		return store.substring(store.indexOf("#") + 1, store.length());
	}

	public int findFirstNonMyStore() {
		storeTitleLinks = findElementsByCSS(".bold.display_block.arrow_primary_right");
		for (int i = 1; i <= storeTitleLinks.size(); i++) {
			if (isElementPresentBySelenium("//*[@id='storeResults']/ol/li[" + i + "]/div/section[4]")) {
				if (getText(findElementByXpath("//*[@id='storeResults']/ol/li[" + i + "]/div/section[4]")).indexOf("Set as My Store") != -1) {
					return i;
				}
			}
		}
		return 0;
	}

	public int findMyStore() {
		storeTitleLinks = findElementsByCSS(".bold.display_block.arrow_primary_right");
		for (int i = 1; i <= storeTitleLinks.size(); i++) {
			if (isElementPresentBySelenium("//*[@id='storeResults']/ol/li[" + i + "]/div/section[4]/span")) {
				return i;
			}
		}
		return 0;
	}

	public void openRefineBox() {
		refineBoxButton = findElementByCSS("#jst_refine");
		clickOn(refineBoxButton);
	}

	public boolean isRefineBoxDisplayed() {
		return isElementVisibleBySelenium("css=.line_bottom.bold.vspace_top.vpadding_bottom.vspace_bottom");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void refineResult() {
		refineButton = findElementByCSS("#jst_refineBtn");
		clickOn(refineButton);
	}

	public void selectAllRefineService() {
		for (int i = 0; i < findElementsByCSS("#checkTech").size(); i++) {
			selectRefineServiceByIndex(i);
		}
	}

	public void selectRefineServiceByIndex(int index) {
		refineServiceCheckBox = findElementsByCSS("#checkTech").get(index);
		clickOn(refineServiceCheckBox);
	}

}
