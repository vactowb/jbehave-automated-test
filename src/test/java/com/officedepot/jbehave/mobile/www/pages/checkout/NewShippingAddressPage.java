package com.officedepot.jbehave.mobile.www.pages.checkout;

import org.openqa.selenium.By;

import com.officedepot.jbehave.mobile.www.pages.account.AddNewShippingAddressPage;

public class NewShippingAddressPage extends AddNewShippingAddressPage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	
	@Override
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "New Shipping Address") 
				&& isElementPresentBySelenium("css=.f_left.step2.active");
	}
	
}
