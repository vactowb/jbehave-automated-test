package com.officedepot.jbehave.mobile.www.steps.odr;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.odr.MaxperksRedirectPage;
import com.officedepot.jbehave.mobile.www.pages.odr.RewardsLandingPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class LoyaltyIntegrationSteps extends BaseStep {

	private MaxperksRedirectPage maxperksRedirectPage;
	private RewardsLandingPage rewardsLandingPage;

	@When("I want to transform my maxperks rewards account")
	public void iAccessTheMaxperksTransitionRedirectPage() throws Exception {
		rewardsLandingPage = new RewardsLandingPage();
		maxperksRedirectPage = rewardsLandingPage.transformMaxperks();
	}

	@Then("I should navigate on maxperks transition redirect page")
	public void iShouldNavigateOnMaxperksTransitionRedirectPage() throws Exception {
		Assert.assertTrue("---Failed navigate to maxperks transition redirect page! ---", maxperksRedirectPage.isNavigateOnThisPage());
	}

	@When("I request to login with the following maxperks: $infoTable")
	public void iRequestToLoginWithTheFollowingMaxperks(ExamplesTable infoTable) throws Exception {
		maxperksRedirectPage.fillMaxperksMemberInfo(infoTable);
		maxperksRedirectPage.active();
	}

	@Then("I should be noticed that my account is invalid")
	public void iShouldBeNoticedThatMyAccountIsInvalid() throws Exception {
		Assert.assertTrue("---Failed navigate to see error message! ---",
				maxperksRedirectPage.isMessageDisplayed("Sorry, either your Member Number, Username or Password is invalid. Please try again."));
	}

	@When("I request to lookup my maxperks account info by phone number \"$phone\"")
	public void iRequestToLookupMyMaxperksAccountInfoByPhoneNumber(String phone) throws Exception {
		maxperksRedirectPage.fillPhoneNumber(phone);
		maxperksRedirectPage.lookup();
	}

	@Then("I should noticed that the lookup button is not available")
	public void iShouldNoticedThatTheLookupButtonIsNotAvailable() throws Exception {
		Assert.assertFalse("---Lookup button is available now! ---", maxperksRedirectPage.isLookupButtonAvailable());
	}

	@Then("I should be noticed that there's no member number with this phone number")
	public void iShouldBeNoticedThatTheresNoMemberNumber() throws Exception {
		Assert.assertTrue("---Failed navigate to see error message! ---", maxperksRedirectPage.isMessageDisplayed("find the membership number please try again"));
	}

	@When("I request to join rewards from maxperks transition redirect page")
	public void iRequestToJoinRewardsFromMaxperksTransitionRedirectPage() throws Exception {
		maxperksRedirectPage.join();
	}

	@Then("I should see the filled phone number is \"$expectedPhone\"")
	public void iShouldSeeTheFilledPhoneNumber(String expectedPhone) throws Exception {
		Assert.assertTrue("---Failed to see the expected phone number! ---", maxperksRedirectPage.getFilledPhoneNumber().equals(expectedPhone));
	}

	@When("I fill the phone number with \"$phone\" for lookup my maxperks account info")
	public void iFillThePhoneNumber(String phone) throws Exception {
		maxperksRedirectPage.fillPhoneNumber(phone);
	}

}
