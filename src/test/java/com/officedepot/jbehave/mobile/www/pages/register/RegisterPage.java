package com.officedepot.jbehave.mobile.www.pages.register;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.checkout.ReviewOrderPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialAccountLogInPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialLogInPageProvider;
import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class RegisterPage extends BasePage {

//	EComContext ecomSiteType = EComContext.OD_EN_US_BUSINESS;
//	String expressRegisterFlag = ConfigurationService.lookup("/od/config/feature/mobile/express/registration/enabled", "true", ecomSiteType);

	private WebElement createAccountButton;
	protected WebElement registrationField;
	private WebElement noRadioButton;
	private WebElement socialLoginButton;
	private WebElement loginButton;
	private WebElement passwordField;
	private WebElement continueAsGuestButton;
	private WebElement memberNumberField;
	private WebElement memberNumberNextButton;
	private WebElement notMemberCheckbox;

	public boolean isNavigateOnThisPage() throws Exception {
//		if (expressRegisterFlag.equals("true")) {
			return isTextPresentOnPage("Registration");
//		} else {
//			return isTextPresentOnPage("Register");
//		}
	}

	public boolean isMessageDisplayed(String content) throws Exception {
		return isTextPresentOnPage(content);
	}

	public boolean isFieldPrefilled(String field) {
		if (field.equalsIgnoreCase("first name")) {
			registrationField = findElementByName("firstName");
		}
		if (field.equalsIgnoreCase("last name")) {
			registrationField = findElementByName("lastName");
		}
		if (field.equalsIgnoreCase("zip code")) {
			registrationField = findElementByName("postalCode");
		}
		if (field.equalsIgnoreCase("email")) {
			registrationField = findElementByName("email");
		}
		return getText(registrationField) != null;
	}

	public void fillRegistrationInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputRegisterField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputRegisterField(String field, String value) {
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		if (value.indexOf("RANNUM") != -1) {
			value = SeleniumUtils.genRandomNumberByTalbeParam(value);
		}
		if (value.indexOf("RANPHONE") != -1) {
			value = SeleniumUtils.genRandomPhoneNumberByTalbeParam();
		}
		if (field.equalsIgnoreCase("first name")) {
			registrationField = findElementByName("firstName");
		}
		if (field.equalsIgnoreCase("last name")) {
			registrationField = findElementByName("lastName");
		}
		if (field.equalsIgnoreCase("zip code")) {
			registrationField = findElementByName("postalCode");
		}
		if (field.equalsIgnoreCase("email")) {
			if (value.indexOf("random") != -1) {
				value = SeleniumUtils.giveUniqueIdBefore(value);
			}
			registrationField = findElementByName("email");
		}
		if (field.equalsIgnoreCase("password")) {
//			if (expressRegisterFlag.equals("true")) {
				registrationField = findElementByCSS("#password-xr");
//			} else {
//				registrationField = findElementByName("password");
//			}
		}
		if (field.equalsIgnoreCase("password confirm")) {
//			if (expressRegisterFlag.equals("true")) {
				registrationField = findElementByCSS("#password2-xr");
//			} else {
//				registrationField = findElementByName("passwordConfirm");
//			}
		}
		if (field.equalsIgnoreCase("security question")) {
			registrationField = findElementById("question");
			selectOptionInListByText(registrationField, value);
			return;
		}
		if (field.equalsIgnoreCase("answer")) {
			registrationField = findElementById("answer");
		}
		if (field.equalsIgnoreCase("birthday month")) {
			registrationField = findElementById("bday_month");
			selectOptionInListByText(registrationField, value);
			return;
		}
		if (field.equalsIgnoreCase("birthday day")) {
			registrationField = findElementById("bday_day");
			selectOptionInListByText(registrationField, value);
			return;
		}
		if (field.equalsIgnoreCase("membership type")) {
			registrationField = findElementByName("membershipType");
			selectOptionInListByText(registrationField, value);
			return;
		}
		if (field.equalsIgnoreCase("address")) {
			registrationField = findElementByName("address1");
		}
		if (field.equalsIgnoreCase("city")) {
			registrationField = findElementByName("city");
		}
		if (field.equalsIgnoreCase("state")) {
			registrationField = findElementByName("state");
		}
		if (field.equalsIgnoreCase("phone number") || field.equalsIgnoreCase("odr phone number")) {
			registrationField = findElementByName("phone");
		}
		if (field.equalsIgnoreCase("member type")) {
			registrationField = findElementById("membershipType");
		}
		if (field.equalsIgnoreCase("company name")) {
			registrationField = findElementByName("companyName");
		}
		if (field.equalsIgnoreCase("school name")) {
			registrationField = findElementByName("schoolName");
		}
		typeTextBox(registrationField, value);
	}

	public void submitToCreatAccount() {
//		if (expressRegisterFlag.equals("true")) {
			createAccountButton = findElementByName("cmd_registration");
//		} else {
//			createAccountButton = findElementByCSS("input.button.b1a");
//		}
		clickOn(createAccountButton);
		seleniumWaitForPageToLoad(30000);
	}

	public void typePassword(String pwd) throws Exception {
		passwordField = findElementByName("password");
		typeTextBox(passwordField, pwd);
	}

	public ReviewOrderPage continueAsGuest() {
		continueAsGuestButton = findElementByCSS("div.f_right > a");
		clickOn(continueAsGuestButton);
		return new ReviewOrderPage();
	}

	public ReviewOrderPage login() throws Exception {
		loginButton = findElementByCSS("div.f_right > input");
		clickOn(loginButton);
		return new ReviewOrderPage();
	}

	public void selectMarketingEmail() {
		noRadioButton = findElementByXpath("//*[@id='xregAddress']/ol/li/div/input[2]");
		clickOn(noRadioButton);
	}

	public SocialAccountLogInPage socialLogin(String type, String username, String password) throws Exception {
		if (type.equalsIgnoreCase("facebook")) {
			socialLoginButton = findElementByCSS("img");
		} else {
			socialLoginButton = findElementByCSS("div[alt='" + type + "'] > div > div");
		}
		clickOn(socialLoginButton);
		focusOnNewWindow();
		SocialAccountLogInPage socialLogInPage = SocialLogInPageProvider.getSocialLogInPage(type);
		socialLogInPage.logIn(username, password);
		return socialLogInPage;
	}

	public void fillMemberNumber(String number) {
		memberNumberField = findElementById("odrNumber");
		typeTextBox(memberNumberField, number);
	}

	public void submitMemberNumber() {
		memberNumberNextButton = findElementByXpath("//input[@value='Next']");
		clickOn(memberNumberNextButton);
	}

	public void checkNotAMember() {
		notMemberCheckbox = findElementById("joinODR");
		clickOn(notMemberCheckbox);
	}

	public boolean isNotAMemberChecked() {
		return isCheckBoxChecked("id=joinODR");
	}

	public boolean isMemberNumberFieldDisplayed() {
		return isElementPresentBySelenium("id=odrNumber");
	}

}
