package com.officedepot.jbehave.mobile.www.pages.school;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SchoolListPage extends BasePage{
	private WebElement grade;

	public void accessSchoolListUrl(String url){
		openUrl(url);
	}
	
	public void addItemsToCart(){
		WebElement addToCart = super.findElementByName("cmd_addToCart.button");
		addToCart.click();
	}
	
	public boolean isInCurrentPage(String content) throws Exception {
		return isTextPresentOnPage(content);
	} 

	public void clickGrade(int index) {
		grade = findElementByXpath("//ul[@id='btsGrades']/li["+index+"]/a");
		clickOn(grade);
	}
	
	public boolean isElementPresent(By by) {
//		return this.isElementPresent(by); // dead lock
		return true;
	}
}
