package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ChangeSecurityQuestionsPage extends BasePage {

	private WebElement answerField;
	private WebElement questionSelectList;
	private WebElement saveButton;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Change Security Question");
	}

	public void inputValidSecurityQuestionInfo() {
		questionSelectList = findElementByCSS("#securityQuestion");
		selectOptionInListByText(questionSelectList, "What is your mother's maiden name?");
		inputSecurityQuestionInfo(1, "tester");
		inputSecurityQuestionInfo(2, "tester");
	}

	public void inputSecurityQuestionInfo(int index, String value) {
		answerField = findElementByCSS("#questionAnswer" + index);
		typeTextBox(answerField, value);
	}

	public MyAccountPage clickOnUpdateButton() {
		saveButton = findElementById("jst_submit");
		clickOn(saveButton);
		return new MyAccountPage();
	}

}
