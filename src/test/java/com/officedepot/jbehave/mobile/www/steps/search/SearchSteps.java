package com.officedepot.jbehave.mobile.www.steps.search;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.refinement.SearchProductResultsListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SearchSteps extends BaseStep {

	private SearchProductResultsListPage searchProductResultsListPage;
	private PageHeader pageHeader;

	@When("I search product with product name : \"$productName\"")
	public void iSearchProductName(String productName) throws Exception {
		pageHeader = new PageHeader();
		searchProductResultsListPage = pageHeader.searchByProductName(productName);
	}

	@When("I refine search results with brands pilot")
	public void iRefineResultsNarrowByBrandsPilot() throws Exception {
		searchProductResultsListPage.refineTheSearch();
		searchProductResultsListPage.refineByBrandsPilot();
	}

	@Then("I should see the search results of \"$productName\"")
	public void iShouldSeeTheSearchResultsOf(String productName) throws Exception {
		Assert.assertTrue("--- Search for the incorrect results! ---", searchProductResultsListPage.isProductNameDisplayedOnPage(productName));
	}

	@When("I request to go to page \"$number\" of search results")
	public void iRequestToGoToNextPageOfSearchResults(String number) throws Exception {
		searchProductResultsListPage.navigateOnOtherPage(number);
	}

}
