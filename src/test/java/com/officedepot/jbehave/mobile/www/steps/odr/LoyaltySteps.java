package com.officedepot.jbehave.mobile.www.steps.odr;

import junit.framework.Assert;

import org.apache.commons.lang3.RandomStringUtils;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.login.LoginOrRegisterPage;
import com.officedepot.jbehave.mobile.www.pages.odr.CreateAccountPage;
import com.officedepot.jbehave.mobile.www.pages.odr.CreateMemberIDPage;
import com.officedepot.jbehave.mobile.www.pages.odr.JoinODRLinkPage;
import com.officedepot.jbehave.mobile.www.pages.odr.MemberNumberEnteringPage;
import com.officedepot.jbehave.mobile.www.pages.odr.MyRewardsPage;
import com.officedepot.jbehave.mobile.www.pages.odr.ODLoginPage;
import com.officedepot.jbehave.mobile.www.pages.odr.ODRExpressRegisterPage;
import com.officedepot.jbehave.mobile.www.pages.odr.RegistrationCodeVerificationPage;
import com.officedepot.jbehave.mobile.www.pages.odr.RewardsLandingPage;
import com.officedepot.jbehave.mobile.www.pages.odr.VerifyAccountPage;
import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;
import com.officedepot.test.jbehave.BaseStep;
import com.officedepot.test.jbehave.SeleniumTestContext;

@Component
@Scope("prototype")
public class LoyaltySteps extends BaseStep {

	private PageHeader pageHeader;
	private LoginOrRegisterPage loginOrRegisterPage;
	private HomePage homePage;
	private MyRewardsPage myRewardsPage;
	private RewardsLandingPage rewardsLandingPage;
	private ODRExpressRegisterPage odrExpressRegisterPage;
	private CreateAccountPage activeODRPage;
	private ODLoginPage odLoginPage;
	private JoinODRLinkPage joinODRLinkPage;
	private VerifyAccountPage verifyAccountPage;
	private RegistrationCodeVerificationPage regtrationCodeVerificationPage;
	private MemberNumberEnteringPage memberNumberEnteringPage;
	private CreateMemberIDPage createMemberIDPage;
	private RegisterPage registerPage;

	@When("I request to access office depot rewards from header")
	public void iLogInODRAccountFromHeaderPage() throws Exception {
		pageHeader = new PageHeader();
		rewardsLandingPage = pageHeader.accessODR();
	}

	@Then("I should navigate on login page")
	public void ishouldSeeLoginPageSuccessfully() throws Exception {
		if (loginOrRegisterPage == null) {
			loginOrRegisterPage = new LoginOrRegisterPage();
		}
		Assert.assertTrue("---Failed navigate to login page! ---", loginOrRegisterPage.isNavigateOnThisPage());
	}

	@When("I request to view my rewards from home page")
	public void iRequestToViewMyRewardsFromHomePage() throws Exception {
		homePage = new HomePage();
		myRewardsPage = homePage.viewMyRewards();
	}

	@Then("I should navigate on my rewards page")
	public void iShouldNavigateOnODRPage() throws Exception {
		if (myRewardsPage == null) {
			myRewardsPage = new MyRewardsPage();
		}
		Assert.assertTrue("---Failed navigate to ODR page! ---", myRewardsPage.isNavigateOnMyRewardsPage());
	}

	@When("I request to access ODR from home page")
	public void iRequestToAccessODRFromHomePage() throws Exception {
		homePage = new HomePage();
		rewardsLandingPage = homePage.joinOrActiveMyRewards();
	}

	@Then("I should navigate on office depot rewards landing page")
	public void iShouldNavigateOnOfficeDepotRewardsLandingPage() throws Exception {
		Assert.assertTrue("---Failed navigate to ODR landing page! ---", rewardsLandingPage.isNavigateOnThisPage());
	}

	@When("I request to join ODR")
	public void iRequestToJoinODR() throws Exception {
		rewardsLandingPage.join();
	}

	@Then("I should navigate on ODR express register page")
	public void iShouldNavigateOnJoinOfficeDepotRewardsPage() throws Exception {
		if (odrExpressRegisterPage == null) {
			odrExpressRegisterPage = new ODRExpressRegisterPage();
		}
		Assert.assertTrue("---Failed navigate to join ODR page! ---", odrExpressRegisterPage.isNavigateOnThisPage());
	}

	@Then("I should see \"$field\" is already prefilled on join ODR page")
	public void iShouldSeePhoneNumberIsAlreadyPrefilledOnJoinODRPage(String field) throws Exception {
		Assert.assertTrue("---The field is not prefilled! ---", odrExpressRegisterPage.isFieldPrefilled(field));
	}

	@Then("I should see \"$field\" is already prefilled on join ODR link page")
	public void iShouldSeePhoneNumberIsAlreadyPrefilledOnJoinODRLinkPage(String field) throws Exception {
		Assert.assertTrue("---The field is not prefilled! ---", joinODRLinkPage.isFieldPrefilled(field));
	}

	@When("I register with the following info for join ODR: $infoTable")
	public void registerWithFollowingInfoForJoinODR(ExamplesTable infoTable) throws Exception {
		odrExpressRegisterPage.fillRegistrationInfo(infoTable);
		odrExpressRegisterPage.submitToCreatAccount();
	}

	@Then("I should be noticed that the phone number is already linked to another ODR account")
	public void iShouldBeNoticedThatPhoneNumberIsAlreadyLinked() throws Exception {
		Assert.assertTrue("---Failed navigate to see error message! ---", odrExpressRegisterPage.isMessageDisplayed("This phone number is already in use"));
	}

	@Then("I should be noticed that the email is already linked to another ODR account")
	public void iShouldBeNoticedThatEmailIsAlreadyLinked() throws Exception {
		Assert.assertTrue("----Failed navigate to see error message! ---", odrExpressRegisterPage.isMessageDisplayed("This email address is already in use"));
	}

	@When("I request to active ODR")
	public void iRequestToActiveODR() throws Exception {
		activeODRPage = rewardsLandingPage.createAccount();
	}

	@Then("I should navigate on active office depot rewards page")
	public void iShouldNavigateOnActiveOfficeDepotRewardsPage() throws Exception {
		Assert.assertTrue("---Failed navigate to active ODR page! ---", activeODRPage.isNavigateOnThisPage());
	}

	@When("I request to active ODR account with member number \"$number\"")
	public void iRequestToActiveODRWithMemberNumber(String number) throws Exception {
		activeODRPage.inputMemberNumber(number);
		activeODRPage.submitActive();
	}

	@Then("I should be noticed that the member number must be ten numeric digits")
	public void iShouldBeNoticedNumeric() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", activeODRPage.isMessageDisplayed("must be ten numeric digits"));
	}

	@Then("I should be noticed that the member number was not found")
	public void iShouldBeNoticedNotValid() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", activeODRPage.isMessageDisplayed("The Member Number you entered is not valid"));
	}

	@Then("I should be noticed that the member number is already linked")
	public void iShouldBeNoticedLinked() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", activeODRPage.isMessageDisplayed("The member number you are trying to link is already linked"));
	}

	@Then("I should see the light box of asking me if I have OD account")
	public void iShouldTheLightBoxOfODAccount() throws Exception {
		if (activeODRPage == null) {
			activeODRPage = new CreateAccountPage();
		}
		Assert.assertTrue("---Failed to see question light box! ---", activeODRPage.isQuestionLightBoxDisplayed());
	}

	@When("I choose \"$option\" for the question of asking me if I have OD account")
	public void iChooseNoForQuestion(String option) throws Exception {
		activeODRPage.chooseOptionForQuestion(option);
	}

	@Then("I should navigate on OD login page")
	public void iShouldNavigateOnODRLoginPage() throws Exception {
		if (odLoginPage == null) {
			odLoginPage = new ODLoginPage();
		}
		Assert.assertTrue("---Failed to navigate on OD login page! ---", odLoginPage.isNavigateOnThisPage());
	}

	@When("I login with my account from OD login page")
	public void iLoginFromODLoginPage(@Named("username") String username, @Named("password") String password) throws Exception {
		if (odLoginPage == null) {
			odLoginPage = new ODLoginPage();
		}
		if (username.indexOf("#") != -1) {
			username = username.replace('#', '@');
		} else if (SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("sqs") != -1) {
			if (!(username.equals("auto_reid") || username.equals("user_www"))) {
				if (username.indexOf("invalid") != -1) {
					username = username + RandomStringUtils.randomAlphanumeric(6);
				}
				username = username + "@test.com";
			}
		}
		System.out.println("username: " + username);
		System.out.println("password: " + password);
		odLoginPage.login(username, password);
	}

	@Then("I should be noticed that my login name or password is invalid")
	public void loginNameOrPasswordInvalid() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---",
				odLoginPage.isMessageDisplayed("We could not find information for user") || odLoginPage.isMessageDisplayed("either your Login Name or Password is invalid"));
	}

	@Then("I should navigate on join ODR link page")
	public void iShouldNavigateOnJoinODRLinkPage() throws Exception {
		if (joinODRLinkPage == null) {
			joinODRLinkPage = new JoinODRLinkPage();
		}
		Assert.assertTrue("---Failed to navigate on join odr link page! ---", joinODRLinkPage.isNavigateOnThisPage());
	}

	@When("I continue to link the login account with member number by phone number \"$number\"")
	public void iContinueToLinkTheLoginAccountWithMemberNumberByPhoneNumber(String number) throws Exception {
		joinODRLinkPage.inputPhoneNumber(number);
		joinODRLinkPage.continueLink();
	}

	@Then("I should be noticed that phone number is invalid")
	public void iShouldBeNoticedThatPhoneNumberIsInvalid() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", joinODRLinkPage.isMessageDisplayed("Phone number is invalid"));
	}

	@Then("I should be noticed that phone number must be unique")
	public void iShouldBeNoticedThatPhoneNumberMustBeUnique() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", joinODRLinkPage.isMessageDisplayed("phone number is already in use"));
	}

	@When("I cancel to link the login account with member number")
	public void iCancelToLinkTheLoginAccountWithMemberNumber() throws Exception {
		joinODRLinkPage.cancelLink();
	}

	@Then("I should navigate on verify account page")
	public void iShouldNavigateOnVerifyAccountPage() throws Exception {
		if (verifyAccountPage == null) {
			verifyAccountPage = new VerifyAccountPage();
		}
		Assert.assertTrue("---Failed to navigate on verify account page! ---", verifyAccountPage.isNaviageOnThisPage());
	}

	@When("I cancel to verify account")
	public void iCancelToVerifyAccount() throws Exception {
		verifyAccountPage.cancel();
	}

	@Then("I should see I can select an option between voice and sms")
	public void iShouldSeeICanSelectAnOptionBetweenVoiceAndSms() throws Exception {
		Assert.assertTrue("---Failed to verify options! ---", verifyAccountPage.verifyOptions("phone"));
	}

	@When("I select to verify account by \"$option\"")
	public void iSelectToVerifyAccountBy(String option) throws Exception {
		verifyAccountPage.seleceOption(option);
		verifyAccountPage.submit();
	}

	@Then("I should navigate on \"$option\" registration code verification page")
	public void iShouldNavigateOnSmsVerificationPage(String option) throws Exception {
		if (regtrationCodeVerificationPage == null) {
			regtrationCodeVerificationPage = new RegistrationCodeVerificationPage();
		}
		Assert.assertTrue("---Failed to navigate on expected page! ---", regtrationCodeVerificationPage.isNavigateOnExpectedPage(option));
	}

	@Then("I should see I can select an option of email")
	public void iShouldSeeICanSelectAnOptionOfEmail() throws Exception {
		Assert.assertTrue("---Failed to verify options! ---", verifyAccountPage.verifyOptions("email"));
	}

	@Then("I should see I can select an option among email, voice and sms")
	public void iShouldSeeICanSelectAnOptionAmongEmailVoiceAndSms() throws Exception {
		Assert.assertTrue("---Failed to verify options! ---", verifyAccountPage.verifyOptions("all"));
	}

	@When("I submit the invalid registration code")
	public void iSubmitInvalidRegCode() throws Exception {
		regtrationCodeVerificationPage.fillRegCode("abc123");
		regtrationCodeVerificationPage.submit();
	}

	@When("I cancel to verify registration code")
	public void iCancelVerifyRegCode() throws Exception {
		regtrationCodeVerificationPage.cancel();
	}

	@Then("I should be noticed that the registration code is invalid")
	public void iShouldBeNoticedRegCodeInvalid() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", regtrationCodeVerificationPage.isMessageDisplayed(""));
	}

	@Then("I should navigate on ODR thank you page")
	public void iShouldNavigateOnODRThankYouPage() throws Exception {
		if (myRewardsPage == null) {
			myRewardsPage = new MyRewardsPage();
		}
		Assert.assertTrue("---Failed navigate to ODR thank you page! ---", myRewardsPage.isNavigateOnThankYouPage());
	}

	@When("I request to register OD account for ODR enrollment")
	public void iRequestToRegisterODAccountForODREnrollment() throws Exception {
		if (odLoginPage == null) {
			odLoginPage = new ODLoginPage();
		}
		odLoginPage.register();
	}

	@Then("I should navigate on member number entering page")
	public void iShouldNavigateOnMemberNumberEnteringPage() throws Exception {
		if (memberNumberEnteringPage == null) {
			memberNumberEnteringPage = new MemberNumberEnteringPage();
		}
		Assert.assertTrue("---Failed navigate to member number page! ---", memberNumberEnteringPage.isNavigateOnThisPage());
	}

	@When("I submit my member number \"$number\" from member number entering page")
	public void iSubmitMemberNumber(String number) throws Exception {
		memberNumberEnteringPage.inputMemberNumber(number);
		memberNumberEnteringPage.submitActive();
	}

	@Then("I should be noticed that the member number must be ten numeric digits from member number entering page")
	public void iShouldBeNoticedNumericFromMemeberPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", memberNumberEnteringPage.isMessageDisplayed("must be ten numeric digits"));
	}

	@Then("I should be noticed that the member number is not valid from member number entering page")
	public void iShouldBeNoticedNumericNotValidFromMemeberPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", memberNumberEnteringPage.isMessageDisplayed("The Member Number you entered is not valid"));
	}

	@Then("I should be noticed that the member number is already linked from member number entering page")
	public void iShouldBeNoticedNumericIsAlreadyLinkedFromMemeberPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", memberNumberEnteringPage.isMessageDisplayed("already linked"));
	}

	@When("I request to register a new ODR account from member number entering page")
	public void registerNewMemberID() throws Exception {
		if (memberNumberEnteringPage == null) {
			memberNumberEnteringPage = new MemberNumberEnteringPage();
		}
		createMemberIDPage = memberNumberEnteringPage.registerForNewMember();
	}

	@Then("I should navigate on create member ID page")
	public void iShouldNavigateOnCreateMemberIDPage() throws Exception {
		Assert.assertTrue("---Failed navigate to create member ID page! ---", createMemberIDPage.isNavigateOnThisPage());
	}

	@Then("I should see \"$field\" is already prefilled on create member ID page")
	public void iShouldSeePhoneNumberIsAlreadyPrefilledOnCreateMemberIDPage(String field) throws Exception {
		Assert.assertTrue("---The field is not prefilled! ---", createMemberIDPage.isFieldPrefilled("phone number"));
	}

	@When("I continue to link OD account with the following contact info from create member ID page: $table")
	public void iContinueToLinkODAccountWithTheFollowingContactInfoFromCreateMemberIDPage(ExamplesTable table) throws Exception {
		createMemberIDPage.fillContactInfo(table);
		createMemberIDPage.continueToLink();
	}

	@When("I cancel to link with OD account from create member ID page")
	public void iCancelToLink() throws Exception {
		createMemberIDPage.cancel();
	}

	@Then("I should be noticed that the phone number is not valid on create member ID page")
	public void iShouldBeNoticedThatThePhoneNumberIsNotValidOnCreateMemberIDPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", createMemberIDPage.isMessageDisplayed("Phone number is invalid"));
	}

	@Then("I should be noticed that the phone number is already linked to another ODR account on create member ID page")
	public void iShouldBeNoticedThatThePhoneNumberIsAlreadyLinkedToAnotherODRAccountOnCreateMemberIDPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", createMemberIDPage.isMessageDisplayed("phone number you entered is already associated"));
	}

	@When("I request to active ODR account with member number \"$number\" from register page")
	public void iRequestToActiveODRAccountWithMemberNumberFromRegisterPage(String number) throws Exception {
		if (registerPage == null) {
			registerPage = new RegisterPage();
		}
		registerPage.fillMemberNumber(number);
		registerPage.submitMemberNumber();
	}

	@Then("I should be noticed that the member number must be ten numeric digits from register page")
	public void iShouldBeNoticedThatTheMemberNumberMustBeTenNumericDigitsFromRegisterPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", registerPage.isMessageDisplayed("Number must be ten numeric digits"));
	}

	@Then("I should be noticed that the member number was not found from register page")
	public void iShouldBeNoticedThatTheMemberNumberWasNotFoundFromRegisterPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", registerPage.isMessageDisplayed("The Member Number you entered is not valid"));
	}

	@Then("I should be noticed that the member number is already linked from register page")
	public void iShouldBeNoticedThatTheMemberNumberIsAlreadyLinkedFromRegisterPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---",
				registerPage.isMessageDisplayed("The member number you are trying to link is already linked to another Office Depot online shopping account"));
	}

	@Then("I should be noticed that the phone number is invalid from register page")
	public void iShouldBeNoticedThatThePhoneNumberIsInvalidFromRegisterPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", registerPage.isMessageDisplayed("Phone number is invalid"));
	}

	@When("I register with join ODR by the following info: $table")
	public void iRegisterWithJoinODRByTheFollowingInfo(ExamplesTable table) throws Exception {
		if (registerPage == null) {
			registerPage = new RegisterPage();
		}
		if (!registerPage.isNotAMemberChecked()) {
			registerPage.checkNotAMember();
		}
		registerPage.fillRegistrationInfo(table);
		registerPage.submitToCreatAccount();
	}

	@Then("I should be noticed that the phone number is already linked to another ODR account from register page")
	public void iShouldBeNoticedThatThePhoneNumberIsAlreadyLinkedToAnotherODRAccountFromRegisterPage() throws Exception {
		Assert.assertTrue("---Failed to see error message! ---", registerPage.isMessageDisplayed("This phone number is already in use"));
	}

}
