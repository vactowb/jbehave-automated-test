package com.officedepot.jbehave.mobile.www.pages.sku;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SkuDescriptionPage extends BasePage {

	private WebElement addToCartButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Description");
	}

	public void addToCart() {
		addToCartButton = findElementByName("cmd_addCart.button");
		clickOn(addToCartButton);
	}

}
