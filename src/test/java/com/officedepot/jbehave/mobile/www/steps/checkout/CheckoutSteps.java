package com.officedepot.jbehave.mobile.www.steps.checkout;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.account.EditShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.CheckoutOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.EditBillingInfoPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.EditPaymentPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.EnterNewPaymentMethodPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.NewPaymentPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.NewShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.PaymentOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.ReviewOrderPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.ShippingOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.ThankYouPage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.paypal.PayPalPage;
import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;
import com.officedepot.jbehave.mobile.www.pages.shoppingcart.CartPage;
import com.officedepot.test.jbehave.BaseStep;
import com.officedepot.test.jbehave.SeleniumTestContext;

@Component
@Scope("prototype")
public class CheckoutSteps extends BaseStep {

	private CheckoutOptionsPage checkOutOptionsPage;
	private ReviewOrderPage reviewOrderPage;
	private ThankYouPage thankYouPage;
	private PaymentOptionsPage paymentOptionsPage;
	private NewPaymentPage newPaymentPage;
	private EditBillingInfoPage editBillingInfoPage;
	private ShippingOptionsPage shippingOptionsPage;
	private EditShippingAddressPage editShippingAddressPage;
	private EnterNewPaymentMethodPage enterNewPaymentMethodPage;
	private NewShippingAddressPage newShippingAddressPage;
	private EditPaymentPage editPaymentPage;
	private PayPalPage paypalPage;
	private RegisterPage registerPage;
	private CartPage cartPage;
	private PageHeader pageHeader;

	@Then("I should navigate on review order page")
	public void atOrderConfirmPage() throws Exception {
		reviewOrderPage = new ReviewOrderPage();
		assertTrue(reviewOrderPage.isNavigateOnThisPage());
	}

	@Then("I should navigate on checkout options page")
	public void checkIfInLoginRequiredPageForCheckout() throws Exception {
		checkOutOptionsPage = new CheckoutOptionsPage();
		assertTrue(checkOutOptionsPage.isNavigateOnThisPage());
	}

	@When("I login from checkout options with the following info: $infoTable")
	public void loginWithOutUserNameInCheckoutStep(ExamplesTable infoTable) throws Exception {
		checkOutOptionsPage.loginFromCheckoutOptions(infoTable.getRow(0).get("Username"), infoTable.getRow(0).get("Password"));
	}

	@When("I login from checkout options with the given account")
	public void loginWithAvalibleAccount(@Named("username") String username, @Named("password") String password) throws Exception {
		if (username.indexOf("#") != -1) {
			username = username.replace('#', '@');
		} else if (SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("sqs") != -1) {
			if (!(username.equals("auto_reid") || username.equals("user_www"))) {
				username = username + "@test.com";
			}
		}
		checkOutOptionsPage = new CheckoutOptionsPage();
		checkOutOptionsPage.loginFromCheckoutOptions(username, password);
	}

	@When("I choose to check out with paypal from checkout options page")
	public void checkoutByPayPal() throws Exception {
		checkOutOptionsPage = new CheckoutOptionsPage();
		checkOutOptionsPage.checkoutWithPaypal();
	}

	@Then("I should be noticed that to enter username and password")
	public void checkIfThereIsErrorMessageForEmptyLoginNameInAnonymousCheckoutStep() throws Exception {
		assertTrue(checkOutOptionsPage.isMessageDisplayed("Please enter a User ID") && checkOutOptionsPage.isMessageDisplayed("Please enter a password"));
	}

	@Then("I should be noticed that the username or password is invalid")
	public void checkIfthereIsErrorMessageForInvalidUserNameLoginInAnonymousCheckoutStep() throws Exception {
		assertTrue(checkOutOptionsPage.isMessageDisplayed("Login Name or Password is invalid"));
	}

	@When("I checkout as guest from checkout options page")
	public void checkoutAsGuestInAnonymousCheckoutStep() throws Exception {
		checkOutOptionsPage = new CheckoutOptionsPage();
		newShippingAddressPage = checkOutOptionsPage.checkoutAsGuest();
	}

	@When("I request to register from checkout options page")
	public void requestToRegisterFromCheckoutOptionsPage() throws Exception {
		checkOutOptionsPage.register();
	}

	@When("I confirm my new payment method")
	public void confirmChange() throws Exception {
		thankYouPage = reviewOrderPage.confirmChange();
	}

	@When("I request to place order")
	public void requestToPlaceOrder() throws Exception {
		if (reviewOrderPage == null) {
			reviewOrderPage = new ReviewOrderPage();
		}
		reviewOrderPage.placeOrder();
	}

	@When("I place order")
	public void iPlaceOrder() throws Exception {
		if (reviewOrderPage == null) {
			reviewOrderPage = new ReviewOrderPage();
		}
		reviewOrderPage.placeOrder();
		if (reviewOrderPage.isSecurityCodeBoxPresent()) {
			reviewOrderPage.enterSecurityCode();
		}
	}

	@Then("I should see my order is placed successfully")
	public void isSuccessPlaceOrder() throws Exception {
		if (thankYouPage == null) {
			thankYouPage = new ThankYouPage();
		}
		thankYouPage.isNavigateOnThisPage();
		this.setParameter("order number", thankYouPage.getOrderNumber(0));
	}

	@Then("I should see there are \"$qty\" orders placed successfully")
	public void isShouldSeeMultipleOrdersPlaced(int qty) throws Exception {
		if (thankYouPage == null) {
			thankYouPage = new ThankYouPage();
		}
		assertTrue("--- Failed to assert order qty! ---", thankYouPage.getOrderQty() == qty);
	}

	@When("I requtest to change my payment from review order page")
	public void iRequestToChangePaymentInfoFromReviewOrderPage() throws Exception {
		paymentOptionsPage = reviewOrderPage.changePayment();
	}

	@When("I requtest to edit my payment from review order page")
	public void iRequestToEditPaymentInfoFromReviewOrderPage() throws Exception {
		editPaymentPage = reviewOrderPage.editPayment();
	}

	@Then("I should navigate on payment options page")
	public void goToCheckOut3rdStep() throws Exception {
		assertTrue("--- Failed to navigate on payment options page! ---", paymentOptionsPage.isNavigateOnThisPage());
	}

	@When("I request to change payment method from security code box")
	public void changePaymentMethodFromCVVBox() throws Exception {
		paymentOptionsPage = reviewOrderPage.changePaymentMethodFromCVVBox();
	}

	@Then("I should be required to enter security code")
	public void enterSecurityCode() throws Exception {
		assertTrue("--- Failed to see the CVV light box! ---", reviewOrderPage.isSecurityCodeBoxPresent());
	}

	@Then("I should navigate on new payment page")
	public void iShouldNavigateOnNewPaymentPage() throws Exception {
		if (newPaymentPage == null) {
			newPaymentPage = new NewPaymentPage();
		}
		assertTrue("--- Failed to navigate on new payment page! ---", newPaymentPage.isNavigateOnThisPage());
	}

	@Then("I should navigate on enter new payment method page")
	public void iShouldNavigateOnEnterNewPaymentMethodPage() throws Exception {
		enterNewPaymentMethodPage = new EnterNewPaymentMethodPage();
		assertTrue("--- Failed to navigate on enter new payment method page! ---", enterNewPaymentMethodPage.isNavigateOnThisPage());
	}

	@When("I add a new credit card with following info: $infoTable")
	public void addNewCreditCardFromCheckOutThirdStep(ExamplesTable infoTable) throws Exception {
		newPaymentPage.fillPaymentInfo(infoTable);
		newPaymentPage.submit();
	}

	@When("I add a new credit card for the new account with following info: $infoTable")
	public void addNewCreditCardFromCheckOut(ExamplesTable infoTable) throws Exception {
		enterNewPaymentMethodPage = new EnterNewPaymentMethodPage();
		enterNewPaymentMethodPage.fillPaymentInfo(infoTable);
		enterNewPaymentMethodPage.enterCVVIfNeeds();
		enterNewPaymentMethodPage.chooseSameAsBilling();
		enterNewPaymentMethodPage.submit();
	}

	@When("I enter my credit card security code")
	public void enterCreditCardCVV() throws Exception {
		thankYouPage = reviewOrderPage.enterSecurityCode();
	}

	@Then("I should see confirm payment change box")
	public void iShouldSeeConfirmPaymentChange() throws Exception {
		reviewOrderPage = new ReviewOrderPage();
		assertTrue("--- Failed to see confirm payment change box! ---", reviewOrderPage.isConfirmChangeBoxPresent());
	}

	@When("I back to review order")
	public void backToReviewOrder() throws Exception {
		reviewOrderPage.backToReviewOrder();
	}

	@When("I edit my default credit card")
	public void editDefaultCreditCard() throws Exception {
		editPaymentPage = paymentOptionsPage.editDefaultCreditCard();
	}

	@When("I update my credit card with following info: $infoTable")
	public void editMyPayment(ExamplesTable infoTable) throws Exception {
		editPaymentPage.fillPaymentInfo(infoTable);
		editPaymentPage.submit();
	}

	@When("I select another credit card as my payment")
	public void iSelectAnotherCreditCardAsMyPayment() throws Exception {
		paymentOptionsPage.selectAnotherCreditCard();
	}

	@When("I request to change billing info from review order page")
	public void iRequestToChangeBillingInfoFromReviewOrderPage() throws Exception {
		if (reviewOrderPage == null) {
			reviewOrderPage = new ReviewOrderPage();
		}
		editBillingInfoPage = reviewOrderPage.changeBillingInfo();
	}

	@When("I requtest to add new payment from review order page")
	public void iRequestToAddNewPaymentFromReviewOrderPage() throws Exception {
		newPaymentPage = reviewOrderPage.enterNewPayment();
	}

	@Then("I should navigate on edit billing info page")
	public void iShouldNavigateToEditBillingInfoPage() throws Exception {
		assertTrue("--- Failed to navigate on edit billing info page! ---", editBillingInfoPage.isNavigateOnThisPage());
	}

	@When("I cancel to edit billing")
	public void iCancelToEditBilling() throws Exception {
		editBillingInfoPage.cancelEditBillingInfo();
	}

	@When("I edit my biling information with following info: $infoTable")
	public void iEditMyBillingInfo(ExamplesTable infoTable) throws Exception {
		editBillingInfoPage.fillBillingInfo(infoTable);
		editBillingInfoPage.updateBillingInfo();
	}

	@When("I request to change shipping info from review order page")
	public void iRequestToChangeShippingInfoFromReviewOrderPage() throws Exception {
		shippingOptionsPage = reviewOrderPage.changeShippingInfo();
	}

	@When("I request to enter new shipping address from review order page")
	public void iRequestToEnterNewShippingInfoFromReviewOrderPage() throws Exception {
		newShippingAddressPage = reviewOrderPage.enterNewShippingAddress();
	}

	@Then("I should navigate on new shipping address page")
	public void iShouldNavigateOnNewShippingAddressPage() throws Exception {
		if (newShippingAddressPage == null) {
			newShippingAddressPage = new NewShippingAddressPage();
		}
		assertTrue("--- Failed to navigate on new shipping address page! ---", newShippingAddressPage.isNavigateOnThisPage());
	}

	@When("I choose to ship to another address")
	public void iChooseToShipToAnotherAddress() throws Exception {
		shippingOptionsPage.shipToAnotherAddress();
	}

	@Then("I should navigate on shipping options page")
	public void iShouldNavigateToShippingOptionsPage() throws Exception {
		assertTrue("--- Failed to navigate on shipping options page! ---", shippingOptionsPage.isNavigateOnThisPage());
	}

	@When("I edit my default shipping address")
	public void iEditMyDefaultShippingAddress() throws Exception {
		editShippingAddressPage = shippingOptionsPage.editDefaultAddress();
	}

	@When("I update my shipping address with following info: $infoTable")
	public void iUpdateMyShippingAddress(ExamplesTable infoTable) throws Exception {
		editShippingAddressPage.fillShippingAddressInfo(infoTable);
		editShippingAddressPage.submit();
	}

	@When("I add new shipping address with following info: $infoTable")
	public void iAddNewShippingAddress(ExamplesTable infoTable) throws Exception {
		newShippingAddressPage.fillShippingAddressInfo(infoTable);
		newShippingAddressPage.submit();
	}

	@Then("I should navigate on edit shipping address page")
	public void iShouldSeeAddShippingAddressInformationPage() throws Exception {
		if (editShippingAddressPage == null) {
			editShippingAddressPage = new EditShippingAddressPage();
		}
		editShippingAddressPage.isNavigateOnThisPage();
	}

	@When("I choose to check out with paypal")
	public void checkOutWithPaypal() throws Exception {
		cartPage = new CartPage();
		paypalPage = cartPage.paypalCheckOut();
	}

	@When("I login paypal with the following account: $paypalAccount")
	public void logInPaypal(ExamplesTable paypalAccount) throws Exception {
		if (null == paypalPage) {
			paypalPage = new PayPalPage();
		}
		paypalPage.loginPaypal(paypalAccount.getRow(0).get("Username"), paypalAccount.getRow(0).get("Password"));
	}

	@When("I continue to check out with this paypal account")
	public void processCheckOut() throws Exception {
		registerPage = paypalPage.processCheckOut();
	}

	@When("I login with the tied OD account with password \"$pwd\"")
	public void loginWithRegisteredAddr(String pwd) throws Exception {
		registerPage.typePassword(pwd);
		reviewOrderPage = registerPage.login();
	}

	@When("I continue as guest")
	public void continueAsGuest() throws Exception {
		reviewOrderPage = registerPage.continueAsGuest();
	}

	@Then("I should see the payment method of my order is paypal")
	public void addPayPalSuccess() throws Exception {
		Assert.assertTrue("---Fail to return order confirm home page!---", reviewOrderPage.isNavigateOnThisPage());
		Assert.assertTrue("---Fail to add paypal into order!---", reviewOrderPage.addPayPalSuccess());
	}

	@When("I choose paypal as my payment method")
	public void changeToPayPal() throws Exception {
		paypalPage = paymentOptionsPage.changeToPayPal();
	}

	@Then("I should only see home and login options on main menu")
	public void iShouldOnlySeeHomeAndLoginOnMenu() throws Exception {
		if (pageHeader == null) {
			pageHeader = new PageHeader();
		}
		pageHeader.openMainNavigator();
		Assert.assertTrue("--- Fail to see menu options! ---", pageHeader.isOptionPresentOnMenu("Home") && pageHeader.isOptionPresentOnMenu("Login"));
		Assert.assertTrue("--- Fail to verify menu options number! ---", pageHeader.getMenuOptionsNum() == 2);
		pageHeader.openMainNavigator();
	}

	@Then("I should only see home and account options on main menu")
	public void iShouldOnlySeeHomeAndAccountOnMenu() throws Exception {
		if (pageHeader == null) {
			pageHeader = new PageHeader();
		}
		pageHeader.openMainNavigator();
		Assert.assertTrue("--- Fail to see menu options! ---", pageHeader.isOptionPresentOnMenu("Home") && pageHeader.isOptionPresentOnMenu("MyAccount"));
		Assert.assertTrue("--- Fail to verify menu options number! ---", pageHeader.getMenuOptionsNum() == 2);
		pageHeader.openMainNavigator();
	}
}
