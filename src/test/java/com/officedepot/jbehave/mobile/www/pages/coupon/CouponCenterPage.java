package com.officedepot.jbehave.mobile.www.pages.coupon;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CouponCenterPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement couponCodeField;
	private WebElement applyCouponButton;
	private WebElement cancelButton;
	private List<WebElement> backToCartButtons;
	private WebElement removeCouponButton;
	private List<WebElement> couponCodeLables;

	public void applyCoupon(String couponcode) {
		couponCodeField = findElementByName("couponCode");
		typeTextBox(couponCodeField, couponcode);
		applyCouponButton = findElementByName("submitOptional");
		clickOn(applyCouponButton);
	}

	public boolean compareResultMessage(String expectMsg) {
		return isTextPresentOnPage(expectMsg);
	}

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Coupon Center");
	}

	public void cancelAddCoupon() throws Exception {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
	}

	public void removeCoupon(String couponcode) throws Exception {
		removeCouponButton = findElementByXpath("//a[@rel='" + couponcode + "']");
		clickOn(removeCouponButton);
		clickOn(findElementByLinkText("Yes"));
	}

	public void backToCart() {
		backToCartButtons = findElementsByCSS(".button.b1.f_right");
		clickOn(backToCartButtons.get(0));
	}

	public boolean isCouponAdded(String code) {
		if (isElementPresentBySelenium("css=.couponDescription>span")) {
			couponCodeLables = findElementsByCSS(".couponDescription>span");
			for (WebElement couponCode : couponCodeLables) {
				if (getText(couponCode).indexOf(code) != -1) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isMessageDisplayed(String expectMsg) {
		return isTextPresentOnPage(expectMsg);
	}

}
