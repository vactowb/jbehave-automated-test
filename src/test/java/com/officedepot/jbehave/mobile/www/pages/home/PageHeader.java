package com.officedepot.jbehave.mobile.www.pages.home;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.ManageOrdersPage;
import com.officedepot.jbehave.mobile.www.pages.account.MyAccountPage;
import com.officedepot.jbehave.mobile.www.pages.category.ChooseCategoryPage;
import com.officedepot.jbehave.mobile.www.pages.deals.DealsPage;
import com.officedepot.jbehave.mobile.www.pages.login.LoginOrRegisterPage;
import com.officedepot.jbehave.mobile.www.pages.odr.RewardsLandingPage;
import com.officedepot.jbehave.mobile.www.pages.others.ContactUsPage;
import com.officedepot.jbehave.mobile.www.pages.others.PrivacyPolicyPage;
import com.officedepot.jbehave.mobile.www.pages.others.TermsPage;
import com.officedepot.jbehave.mobile.www.pages.refinement.SearchProductResultsListPage;
import com.officedepot.jbehave.mobile.www.pages.shoppingcart.CartPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.store.StoreLocatorPage;
import com.officedepot.test.jbehave.BasePage;

public class PageHeader extends BasePage {

	private WebElement cartLink;
	private WebElement loginNavigator;
	private WebElement logoutNavigator;
	private WebElement mainNavigator;
	private WebElement trackOrdersNavigator;
	private WebElement termsNavigator;
	private WebElement myAccountNavigator;
	private WebElement contactUsNavigator;
	private WebElement productNavigator;
	private WebElement dealsNavigator;
	private WebElement findStoresNavigator;
	private WebElement searchFiled;
	private WebElement oDRNavigator;
	private WebElement homeNavigator;
	private WebElement inkFinderNavigator;
	private WebElement privacyNavigator;

	public HomePage openHomePage() {
		openMainNavigator();
		homeNavigator = findElementById("t_navHome");
		clickOn(homeNavigator);
		return new HomePage();
	}

	public SkuDetailsPage searchBy(String skuId) {
		searchFiled = findElementById("mobileSearchField");
		typeTextBox(searchFiled, skuId);
		searchFiled.sendKeys(Keys.RETURN);
		return new SkuDetailsPage();
	}

	public SearchProductResultsListPage searchByProductName(String productName) {
		searchFiled = findElementById("mobileSearchField");
		typeTextBox(searchFiled, productName);
		searchFiled.sendKeys(Keys.RETURN);
		return new SearchProductResultsListPage();
	}

	public CartPage openCart() {
		cartLink = findElementByCSS("a.cart_link");
		clickOn(cartLink);
		return new CartPage();
	}

	public void openMainNavigator() {
		mainNavigator = findElementById("mainNav");
		clickOn(mainNavigator);
	}

	public LoginOrRegisterPage enterLoginOrRegisterPage() {
		openMainNavigator();
		loginNavigator = findElementById("t_navLogin");
		clickOn(loginNavigator);
		return new LoginOrRegisterPage();
	}

	public void logout() {
		openMainNavigator();
		logoutNavigator = findElementById("t_navLogout");
		clickOn(logoutNavigator);
	}

	public TermsPage clickOnTermsLinkToTermsPage() {
		openMainNavigator();
		termsNavigator = findElementById("t_navTermsOfUse");
		clickOn(termsNavigator);
		return new TermsPage();
	}

	public MyAccountPage manageAccount() throws Exception {
		openMainNavigator();
		myAccountNavigator = findElementById("t_navMyAccount");
		clickOn(myAccountNavigator);
		return new MyAccountPage();
	}

	public ManageOrdersPage trackOrders() throws Exception {
		openMainNavigator();
		trackOrdersNavigator = findElementById("t_navOrders");
		clickOn(trackOrdersNavigator);
		return new ManageOrdersPage();
	}

	public ContactUsPage goToContactUsPage() {
		openMainNavigator();
		contactUsNavigator = findElementById("t_navCustomerService");
		clickOn(contactUsNavigator);
		return new ContactUsPage();
	}

	public ChooseCategoryPage goToProducts() {
		openMainNavigator();
		productNavigator = findElementById("t_navBrowse");
		clickOn(productNavigator);
		return new ChooseCategoryPage();
	}

	public DealsPage goToDeals() {
		openMainNavigator();
		dealsNavigator = findElementById("t_navDeals");
		clickOn(dealsNavigator);
		return new DealsPage();
	}

	public StoreLocatorPage findStores() throws Exception {
		openMainNavigator();
		findStoresNavigator = findElementById("t_navFindStores");
		clickOn(findStoresNavigator);
		return new StoreLocatorPage();
	}

	public RewardsLandingPage accessODR() {
		openMainNavigator();
		oDRNavigator = findElementById("t_navRewards");
		clickOn(oDRNavigator);
		return new RewardsLandingPage();
	}

	public boolean isOptionPresentOnMenu(String option) {
		return isElementPresentBySelenium("t_nav" + option);
	}

	public int getMenuOptionsNum() {
		return findElementsByCSS(".menuNav > li").size();
	}

	public ChooseCategoryPage findInk() throws Exception {
		openMainNavigator();
		inkFinderNavigator = findElementById("t_navInk");
		clickOn(inkFinderNavigator);
		return new ChooseCategoryPage();
	}

	public PrivacyPolicyPage goToPrivacy() {
		openMainNavigator();
		privacyNavigator = findElementById("t_navPrivacy");
		clickOn(privacyNavigator);
		return new PrivacyPolicyPage();
	}

}
