package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class AddNewShippingAddressPage extends BasePage {

	protected WebElement submitButton;
	protected WebElement shippingAddressField;
	protected ExamplesTable actualFilledInfo = new ExamplesTable("");
	protected List<Map<String, String>> actualFilledRows = new ArrayList<Map<String, String>>();

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium("name=submitOptional");
	}

	public ExamplesTable fillShippingAddressInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputShippingAddressField(infoRow.get("Field"), infoRow.get("Value"));
		}
		return actualFilledInfo.withRows(actualFilledRows);
	}

	public void submit() {
		submitButton = findElementByName("submitOptional");
		clickOn(submitButton);
	}

	public void inputShippingAddressField(String field, String value) {
		Map<String, String> row = new HashMap<String, String>();
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		if (field.equalsIgnoreCase("first name")) {
			shippingAddressField = findElementById("firstName-0");
		}
		if (field.equalsIgnoreCase("last name")) {
			shippingAddressField = findElementById("lastName-0");
		}
		if (field.equalsIgnoreCase("address 1")) {
			shippingAddressField = findElementById("address1-0");
		}
		if (field.equalsIgnoreCase("address 2")) {
			shippingAddressField = findElementById("address2-0");
		}
		if (field.equalsIgnoreCase("city")) {
			shippingAddressField = findElementById("city-0");
		}
		if (field.equalsIgnoreCase("state")) {
			shippingAddressField = findElementById("state-0");
			selectOptionInListByText(shippingAddressField, value);
			row.put("Field", field);
			row.put("Value", value);
			actualFilledRows.add(row);
			return;
		}
		if (field.equalsIgnoreCase("post code")) {
			shippingAddressField = findElementById("postalCode1-0");
		}
		if (field.equalsIgnoreCase("email")) {
			value = SeleniumUtils.giveUniqueIdBefore(value);
			shippingAddressField = findElementById("email-0");
			typeTextBox(shippingAddressField, value);
			shippingAddressField = findElementById("emailConfirm-0");
			typeTextBox(shippingAddressField, value);
			row.put("Field", field);
			row.put("Value", value);
			actualFilledRows.add(row);
			return;
		}
		if (field.equalsIgnoreCase("phone number")) {
			shippingAddressField = findElementById("phoneNumber1-0");
		}
		typeTextBox(shippingAddressField, value);
		row.put("Field", field);
		row.put("Value", value);
		actualFilledRows.add(row);
	}
}
