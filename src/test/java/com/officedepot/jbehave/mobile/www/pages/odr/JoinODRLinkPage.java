package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class JoinODRLinkPage extends BasePage {

	private WebElement continueButton;
	private WebElement cancelButton;
	private WebElement contactInfoField;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Join Office Depot Rewards") && isElementPresentBySelenium("css=.inforList>li");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void cancelLink() {
		cancelButton = findElementByCSS(".button.f_left");
		clickOn(cancelButton);
	}

	public void continueLink() {
		continueButton = findElementByCSS(".button.b1.f_right");
		clickOn(continueButton);
	}

	public void inputPhoneNumber(String number) {
		contactInfoField = findElementById("phone");
		typeTextBox(contactInfoField, number);
	}

	public void inputEmail(String email) {
		contactInfoField = findElementById("email");
		typeTextBox(contactInfoField, email);
	}

	public boolean isFieldPrefilled(String field) {
		if (field.equalsIgnoreCase("phone number")) {
			contactInfoField = findElementById("phone");
		} else if (field.equalsIgnoreCase("email")) {
			contactInfoField = findElementById("email");
		}
		return !getValue(contactInfoField).isEmpty();
	}

}
