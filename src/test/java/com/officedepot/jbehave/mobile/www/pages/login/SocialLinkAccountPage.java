package com.officedepot.jbehave.mobile.www.pages.login;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.register.SocialRegistrationPage;
import com.officedepot.test.jbehave.BasePage;

public class SocialLinkAccountPage extends BasePage {

	private WebElement registerButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Link with Office Depot Account");
	}

	public SocialRegistrationPage registerWithSocial() {
		registerButton = findElementByLinkText("Register");
		clickOn(registerButton);
		return new SocialRegistrationPage();
	}

}
