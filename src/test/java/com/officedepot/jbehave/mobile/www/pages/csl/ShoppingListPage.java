package com.officedepot.jbehave.mobile.www.pages.csl;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ShoppingListPage extends BasePage {

	private final String NO_LIST_PROMPT = "You do not have any shopping lists. Enter a list name, and click the \"Create List\" button to create one.";
	private List<WebElement> viewEditLinks;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Shopping Lists");
	}

	public ShoppingListDetailPage viewOrEditShoppingList(int index) throws Exception {
		viewEditLinks = findElementsByCSS(".f_right.touch-safe");
		clickOn(viewEditLinks.get(index));
		return new ShoppingListDetailPage();
	}

	public boolean isShoppingListEmpty() throws Exception {
		return isTextPresentOnPage(NO_LIST_PROMPT);
	}

}
