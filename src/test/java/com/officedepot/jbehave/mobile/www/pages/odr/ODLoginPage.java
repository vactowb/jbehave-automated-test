package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ODLoginPage extends BasePage {

	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement loginButton;
	private WebElement registerButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Login");
	}

	public void login(String username, String password) {
		usernameField = findElementById("loginName");
		typeTextBox(usernameField, username);
		passwordField = findElementById("password");
		typeTextBox(passwordField, password);
		loginButton = findElementByCSS(".button.b1.f_right");
		clickOn(loginButton);
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void register() {
		registerButton = findElementByLinkText("Register");
		clickOn(registerButton);
	}

}
