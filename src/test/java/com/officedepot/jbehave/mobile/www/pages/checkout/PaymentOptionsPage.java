package com.officedepot.jbehave.mobile.www.pages.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.PaymentSettingsPage;
import com.officedepot.jbehave.mobile.www.pages.paypal.PayPalPage;

public class PaymentOptionsPage extends PaymentSettingsPage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private List<WebElement> selectCreditCardLinks;
	private WebElement paypalAddAccountButton;

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium("css=.f_left.step3.active") && isTextPresentInElement(pageVerifyItem, "Payment Options");
	}

	public NewPaymentPage addNewCard() throws Exception {
		super.enterNewCard();
		return new NewPaymentPage();
	}

	public EditPaymentPage editDefaultCreditCard() throws Exception {
		super.editDefaultCard();
		return new EditPaymentPage();
	}

	public void selectAnotherCreditCard() {
		selectCreditCardLinks = findElementsByLinkText("Select");
		clickOn(selectCreditCardLinks.get(0));
	}

	public PayPalPage changeToPayPal() throws Exception {
		paypalAddAccountButton = findElementByCSS("a.ppCheckout");
		clickOn(paypalAddAccountButton);
		return new PayPalPage();
	}

}
