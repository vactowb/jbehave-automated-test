package com.officedepot.jbehave.mobile.www.pages.shoppingcart;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.AddNewShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.category.ChooseCategoryPage;
import com.officedepot.jbehave.mobile.www.pages.checkout.CheckoutOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.coupon.CouponCenterPage;
import com.officedepot.jbehave.mobile.www.pages.paypal.PayPalPage;
import com.officedepot.test.jbehave.BasePage;

public class CartPage extends BasePage {

	private List<WebElement> checkoutButtons;
	private WebElement guestCheckoutButton;
	private WebElement paypalCheckOutButton;
	private WebElement loginOrRegisterButton;
	private WebElement emptyCartButton;
	private WebElement continueShoppingLink;
	private List<WebElement> qtyTextBoxes;
	private List<WebElement> saveItemForLaterButtons;
	private List<WebElement> removeItemButtons;
	private WebElement updateQtyButton;
	private WebElement couponBoxContinueShoppingLink;
	private WebElement couponBoxContinueWithCheckoutLink;
	private List<WebElement> removeCouponLinks;
	private WebElement optionBoxButton;
	private List<WebElement> couponCodeLables;
	private WebElement couponCodeField;
	private WebElement addCouponButton;
	private List<WebElement> editCouponLinks;

	public boolean isNavigateOnPage() throws Exception {
		return isTextPresentOnPage("Shopping Cart");
	}

	public void updateSkuQty(int index, String qty) throws Exception {
		qtyTextBoxes = findElementsByName("cartRow[0].quantity");
		qtyTextBoxes.get(index).clear();
		typeTextBox(qtyTextBoxes.get(index), qty);
		updateQtyButton = findElementByName("cmd_updateCartEntry.buttons.index[0]");
		clickOn(updateQtyButton);
	}

	public ChooseCategoryPage continueToShopping() throws Exception {
		continueShoppingLink = findElementByLinkText("Continue Shopping");
		clickOn(continueShoppingLink);
		return new ChooseCategoryPage();
	}

	public String getQtyValue(int index) {
		qtyTextBoxes = findElementsByName("cartRow[0].quantity");
		return getValue(qtyTextBoxes.get(index));
	}

	public boolean isCartUpdated() throws Exception {
		if (getCurrentUrl().endsWith("updateCartRouter.do")) {
			return true;
		}
		return false;
	}

	public void removeItem(int index) throws Exception {
		removeItemButtons = findElementsByCSS(".flo_right");
		clickOn(removeItemButtons.get(index));
	}

	public void saveItemForLater(int index) throws Exception {
		saveItemForLaterButtons = findElementsByCSS(".saveForLaterBtn");
		clickOn(saveItemForLaterButtons.get(index));
	}

	public AddNewShippingAddressPage continueAsGuest() throws Exception {
		guestCheckoutButton = findElementByLinkTextWait("Guest Checkout");
		clickOn(guestCheckoutButton);
		return new AddNewShippingAddressPage();
	}

	public boolean isMessageDisplayed(String expectMsg) {
		return isTextPresentOnPage(expectMsg);
	}

	public void emptyCart() throws Exception {
		emptyCartButton = findElementById("jst_emptyCartBtn");
		clickOn(emptyCartButton);
		acceptAlert("Are you sure you want to empty your cart?");
	}

	public boolean isEmptyForCart() throws Exception {
		return isTextPresentOnPage("You have no items in your cart");
	}

	public boolean isCouponInCartLightBoxDisplayed() {
		return isTextPresentInElement(By.cssSelector(".box_title"), "Coupon in Cart");
	}

	public void checkOut() throws Exception {
		checkoutButtons = findElementsByCSS(".button.b1a");
		clickOn(checkoutButtons.get(0));
	}

	public int getCheckoutButtonsNumber() {
		checkoutButtons = findElementsByCSS(".button.b1a");
		return checkoutButtons.size();
	}

	public PayPalPage paypalCheckOut() throws Exception {
		paypalCheckOutButton = findElementByCSS("#boxes_content > div.center > a.paypal_checkout_options.vspace_top");
		clickOn(paypalCheckOutButton);
		return new PayPalPage();
	}

	public CheckoutOptionsPage loginOrRegister() throws Exception {
		loginOrRegisterButton = findElementByXpath("(//a[contains(text(),'Login / Register')])[2]");
		clickOn(loginOrRegisterButton);
		return new CheckoutOptionsPage();
	}

	public void continueShoppingFromCouponBox() {
		couponBoxContinueShoppingLink = findElementByCSS("div.box_bottom > a.no_decoration.jst_continue");
		clickOn(couponBoxContinueShoppingLink);
	}

	public void continueWithCheckoutFromCouponBox() {
		couponBoxContinueWithCheckoutLink = findElementByXpath("//div[@id='boxes_content']/div/a");
		clickOn(couponBoxContinueWithCheckoutLink);
	}

	public void removeCoupon(int index) {
		removeCouponLinks = findElementsByCSS(".flo_right.jst_remove_coupon");
		clickOn(removeCouponLinks.get(index));
	}

	public void confirmRemoveCoupon(String option) {
		if (option.equals("No")) {
			optionBoxButton = findElementByCSS(".button.f_left.jst_close_boxes");
		} else {
			optionBoxButton = findElementByCSS(".button.b1.flo_right.jst_confirm_remove");
		}
		clickOn(optionBoxButton);
	}

	public boolean isLightingBoxDisplayed(String title) {
		return isTextPresentInElement(By.cssSelector(".box_title"), title);
	}

	public boolean isCouponAdded(String code) {
		if (isElementPresentBySelenium("css=.clear.vspace_bottom.couponDash")) {
			couponCodeLables = findElementsByCSS(".smaller_text");
			for (WebElement couponCode : couponCodeLables) {
				if (getText(couponCode).indexOf(code) != -1) {
					return true;
				}
			}
		}
		return false;
	}

	public CouponCenterPage applyCoupon(String couponcode) {
		couponCodeField = findElementByName("couponCode");
		typeTextBox(couponCodeField, couponcode);
		addCouponButton = findElementByName("submitOptional");
		clickOn(addCouponButton);
		return new CouponCenterPage();
	}

	public CouponCenterPage editCoupon(int index) {
		editCouponLinks = findElementsByLinkText("Edit");
		clickOn(editCouponLinks.get(index));
		return new CouponCenterPage();
	}

}
