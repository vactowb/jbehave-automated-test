package com.officedepot.jbehave.mobile.www.pages.checkout;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.AddNewShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.account.EditShippingAddressPage;
import com.officedepot.test.jbehave.BasePage;

public class ShippingOptionsPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement addNewAddressButton;
	private List<WebElement> editAddressButtons;
	private List<WebElement> shipToThisAddressButtons;
	
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Shipping Options") 
				&& isElementPresentBySelenium("css=.f_left.step2.active");
	}
	
	public AddNewShippingAddressPage addNewShippingAddress() {
		addNewAddressButton = findElementByLinkText("Add New Shipping Address");
		clickOn(addNewAddressButton);
		return new AddNewShippingAddressPage();
	}
	
	public EditShippingAddressPage editDefaultAddress() {
		editAddressButtons = findElementsByCSS("a.f_right.arrow_right");
		clickOn(editAddressButtons.get(0));
		return new EditShippingAddressPage();
	}
	
	public void shipToAnotherAddress() {
		shipToThisAddressButtons = findElementsByLinkText("Ship To This Address");
		clickOn(shipToThisAddressButtons.get(0));
	}
	
}
