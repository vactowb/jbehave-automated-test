package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class RegistrationCodeVerificationPage extends BasePage {

	private WebElement cancelButton;
	private WebElement submitButton;
	private WebElement regCodeField;

	public boolean isNavigateOnExpectedPage(String option) {
		return isTextPresentInElement(By.cssSelector(".page_title"), option);
	}

	public void fillRegCode(String code) {
		regCodeField = findElementById("regCode");
		typeTextBox(regCodeField, code);
	}

	public void cancel() {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
	}

	public void submit() {
		submitButton = findElementByName("submit");
		clickOn(submitButton);
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

}
