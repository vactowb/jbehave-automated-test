package com.officedepot.jbehave.mobile.www.steps.giftcard;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.checkout.ReviewOrderPage;
import com.officedepot.jbehave.mobile.www.pages.giftcard.GiftCardPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class GiftCardSteps extends BaseStep {

	private ReviewOrderPage reviewOrderPage;
	private GiftCardPage giftCardPage;

	@When("I request to add gift card from review order page")
	public void addGiftCard() throws Exception {
		reviewOrderPage = new ReviewOrderPage();
		giftCardPage = reviewOrderPage.applyGiftCard();
	}

	@Then("I should navigate on gift card page")
	public void displayGiftCardPage() throws Exception {
		Assert.assertTrue(giftCardPage.isInCurrentPage());
	}

	@When("I fill the gift card with following info: $cardInfo")
	public void fillAndSubmitInformation(ExamplesTable cardInfo) throws Exception {
		giftCardPage.fillGiftCardInformation(cardInfo);
	}

	@Then("I should see the gift card is apllied successfully")
	public void isSuccessForGiftCard() throws Exception {
		reviewOrderPage.expandGiftCardScetion();
		Assert.assertTrue("---Gift card is not appllied successfully!---", reviewOrderPage.isGiftCardAppliedSuccessfully());
		reviewOrderPage.expandGiftCardScetion();
	}

	@When("I apply the gift card")
	public void applyGiftCard() throws Exception {
		giftCardPage.applyGiftCard();
	}

	@When("I remove the gift card")
	public void removeGiftCard() throws Exception {
		reviewOrderPage.removeGiftCard();
	}

	@Then("I should see the gift card has been removed successfully")
	public void isRemoveGiftCard() throws Exception {
		reviewOrderPage.expandGiftCardScetion();
		Assert.assertTrue("--- Failed to remove gift card! ---", reviewOrderPage.isGiftCardRemoved());
		reviewOrderPage.expandGiftCardScetion();
	}

	@When("I cancel to apply gift card")
	public void cancelGiftCard() throws Exception {
		reviewOrderPage = giftCardPage.cancel();
	}

	@Then("I should see the error message")
	public void dispalyError() throws Exception {
		Assert.assertTrue(giftCardPage.isErrorDisplayed());
	}
}
