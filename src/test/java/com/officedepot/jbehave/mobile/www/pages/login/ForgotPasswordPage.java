package com.officedepot.jbehave.mobile.www.pages.login;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ForgotPasswordPage extends BasePage {

	private WebElement submitButton;
	private WebElement answerField;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Forgot Password");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void submitQuestionAnswer(String answer) {
		answerField = findElementByName("answer");
		typeTextBox(answerField, answer);
		submitButton = findElementByName("Submit");
		clickOn(submitButton);
	}

}
