package com.officedepot.jbehave.mobile.www.steps.sku;

import static org.junit.Assert.assertTrue;
import junit.framework.Assert;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.checkout.ThankYouPage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.sku.AlsoAvailableInPage;
import com.officedepot.jbehave.mobile.www.pages.sku.CheckInStoreAvailbilityPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuDescriptionPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuProductDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuRatingsReviewsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SkuSteps extends BaseStep {

	private SkuDetailsPage skuDetailsPage;
	private PageHeader pageHeader;
	private ThankYouPage thankYouPage;
	private SkuDescriptionPage skuDescriptionPage;
	private SkuProductDetailsPage skuProductDetailsPage;
	private SkuRatingsReviewsPage skuRatingsReviewsPage;
	private CheckInStoreAvailbilityPage checkInStoreAvailbilityPage;
	private AlsoAvailableInPage alsoAvailableInPage;

	@When("I search for sku with the given sku id")
	public void iSearchFor(@Named("skuid") String skuId) throws Exception {
		pageHeader = new PageHeader();
		skuDetailsPage = pageHeader.searchBy(skuId);
	}

	@When("I add the sku \"$skuId\" to cart")
	public void iAddSkuToCart(String skuId) throws Exception {
		pageHeader = new PageHeader();
		skuDetailsPage = pageHeader.searchBy(skuId);
		skuDetailsPage.clickAddToCart();
	}

	@Then("I should see sku details page of the given sku")
	public void iSeeSKUDetailPage(@Named("skuid") String skuId) throws Exception {
		assertTrue("--- Failed to navigate on the given sku details page! ---", skuDetailsPage.isSearchForExceptedSku(skuId));
	}

	@Then("I should navigate on sku details page")
	public void iSeeSKUDetailPage() throws Exception {
		assertTrue("--- Failed to navigate on sku details page! ---", skuDetailsPage.isNavigateOnThisPage());
	}

	@When("I add the sku to list")
	public void iAddTheSKUToList() throws Exception {
		skuDetailsPage = new SkuDetailsPage();
		skuDetailsPage.inputQty("1");
		skuDetailsPage.clickAddToList();
	}

	@When("I add the sku to shopping cart")
	public void iAddTheSKUToCart() throws Exception {
		skuDetailsPage = new SkuDetailsPage();
		skuDetailsPage.clickAddToCart();
	}

	@When("I request to plus the sku qty")
	public void iRequestToPlusSkuQty() throws Exception {
		skuDetailsPage.plusQty();
	}

	@When("I request to minus the sku qty")
	public void iRequestToMinusSkuQty() throws Exception {
		skuDetailsPage.minusQty();
	}

	@Then("I should see the sku qty is \"$qty\"")
	public void iShouldSeeSkuQtyIs(String qty) throws Exception {
		Assert.assertEquals(skuDetailsPage.getQtyValue(), qty);
	}

	@When("I change the sku qty into \"$qty\" from sku details page")
	public void iChangeSkuQtyFromSkuDetailsPage(String qty) throws Exception {
		skuDetailsPage.inputQty(qty);
	}

	@Then("I should see the sku is in status of \"$status\"")
	public void iShouldTheSkuIsInStock(String status) throws Exception {
		Assert.assertTrue("--- The sku is not in the expected status! ---", skuDetailsPage.isSkuInStatusOf(status));
	}

	@When("I request to view sku details from thank you page")
	public void iRequestToViewSkuDetailsFromThankYouPage() throws Exception {
		thankYouPage = new ThankYouPage();
		skuDetailsPage = thankYouPage.viewSkuDetails();
	}

	@When("I request to view sku descritpion")
	public void iRequestToViewSkuDescritpion() throws Exception {
		skuDescriptionPage = skuDetailsPage.viewDescription();
	}

	@Then("I should navigate on sku description page")
	public void iShouldNavigateOnSkuDescriptionPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on sku decription page! ---", skuDescriptionPage.isNavigateOnThisPage());
	}

	@When("I add the sku to cart from description page")
	public void iAddTheSkuToCartFromDescriptionPage() throws Exception {
		skuDescriptionPage.addToCart();
	}

	@When("I request to view sku product details")
	public void iRequestToViewSkuProductDetails() throws Exception {
		skuProductDetailsPage = skuDetailsPage.viewProductDetails();
	}

	@Then("I should navigate on the expected sku product details page")
	public void iShouldNavigateOnSkuProductDetailsPage(@Named("skuid") String skuId) throws Exception {
		Assert.assertTrue("--- Failed to navigate on sku product details page! ---", skuProductDetailsPage.isNavigateOnThisPage() && skuProductDetailsPage.isForExpectedSku(skuId));
	}

	@When("I add the sku to cart from product details page")
	public void iAddTheSkuToCartFromProductDetailsPage() throws Exception {
		skuProductDetailsPage.addToCart();
	}

	@When("I request to view sku ratings and reviews")
	public void iRequestToViewSkuRatingsAndReviews() throws Exception {
		skuRatingsReviewsPage = skuDetailsPage.viewRatingsAndReviews();
	}

	@Then("I should navigate on sku ratings and reviews page")
	public void iShouldNavigateOnSkuRatingsAndReviewsPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on sku ratings and reviews page! ---", skuRatingsReviewsPage.isNavigateOnThisPage());
	}

	@When("I add the sku to cart from ratings and reviews page")
	public void iAddTheSkuToCartFromRatingsAndReviewsPage() throws Exception {
		skuRatingsReviewsPage.addToCart();
	}

	@When("I back to sku details from breadcrumb of sku product details page")
	public void iBackToSkuDetailsPageFromBreadcrumb() throws Exception {
		skuProductDetailsPage.backToSkuDetailsFromBreadcrumb();
	}

	@When("I back to sku details from sku icon of sku product details page")
	public void iBackToSkuDetailsPageFromIcon() throws Exception {
		skuProductDetailsPage.backToSkuDetailsFromIcon();
	}

	@When("I back to sku details from breadcrumb of sku ratings and reviews page")
	public void iBackToSkuDetailsPageFromBreadcrumbReviews() throws Exception {
		skuRatingsReviewsPage.backToSkuDetailsFromBreadcrumb();
	}

	@When("I back to sku details from sku icon of sku ratings and reviews page")
	public void iBackToSkuDetailsPageFromIconReviews() throws Exception {
		skuRatingsReviewsPage.backToSkuDetailsFromIcon();
	}

	@When("I request to check in-store availbility from sku details page")
	public void iRequestToCheckInstoreAvailbilityFromSkuDetailsPage() throws Exception {
		checkInStoreAvailbilityPage = skuDetailsPage.checkAvailbility();
	}

	@Then("I should navigate on in-store availbility page for the expected sku")
	public void iShouldNavigateOnInstoreAvailbilityPage(@Named("skuid") String skuId) throws Exception {
		Assert.assertTrue("--- Failed to navigate on in-store availbility page! ---",
				checkInStoreAvailbilityPage.isNavigateOnThisPage() && checkInStoreAvailbilityPage.isNavigateOnThePageForExpectedSku(skuId));
	}

	@When("I request to check availbility by \"$location\"")
	public void iRequestToCheckAvailbilityBy(String location) throws Exception {
		checkInStoreAvailbilityPage.inputLocation(location);
		checkInStoreAvailbilityPage.checkAvailbility();
	}

	@Then("I should notice that the check availbility button is not available")
	public void iShouldNoticeThatTheCheckAvailbilityButtonIsNotAvailable() throws Exception {
		Assert.assertFalse("--- Check availbility button is available! ---", checkInStoreAvailbilityPage.isCheckAvailbilityButtonAvailable());
	}

	@Then("I should see the store list of in-store availbility")
	public void iShouldSeeTheStoreListOfInstoreAvailbility() throws Exception {
		Assert.assertTrue("--- Failed to see store list! ---", checkInStoreAvailbilityPage.isStoreListDisplayed());
	}

	@When("I request to view store details from store list of in-store availbility")
	public void iRequestToViewStoreDetails() throws Exception {
		checkInStoreAvailbilityPage.viewStoreDetails(0);
	}

	@When("I request to check also available in")
	public void iRequestToCheckAlsoAvailableIn() throws Exception {
		alsoAvailableInPage = skuDetailsPage.checkAlsoAvailableIn();
	}

	@Then("I should navigate on also available in page")
	public void iShouldNavigateOnAlsoAvailableInPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on also available in page! ---", alsoAvailableInPage.isNavigateOnThisPage());
	}

	@When("I back to sku details from breadcrumb of also available in page")
	public void iBackToSkuDetailsFromBreadcrumbOfAlsoAvailableInPage() throws Exception {
		alsoAvailableInPage.backToSkuDetailsFromBreadcrumb();
	}

	@When("I back to sku details from sku icon of also available in page")
	public void iBackToSkuDetailsFromSkuIconOfAlsoAvailableInPage() throws Exception {
		alsoAvailableInPage.backToSkuDetailsFromIcon();
	}

	@When("I add the sku to cart from also available in page")
	public void iAddTheSkuToCartFromAlsoAvailableInPage() throws Exception {
		alsoAvailableInPage.addToCart();
	}

	@When("I request to view sku details from the also available in list")
	public void iRequestToViewSkuDetailsFromAlsoAvailableInPage() throws Exception {
		alsoAvailableInPage.viewAvailableSkuDetails(0);
	}

	@Then("I should see select shopping list lighting box")
	public void iShouldSeeSelectShoppingListBox() throws Exception {
		Assert.assertTrue("--- Failed to see select shopping list box! ---", skuDetailsPage.isExpectedLightingBoxDisplayed("Select Shopping List"));
	}

	@Then("I should see create new list lighting box")
	public void iShouldSeeCreateShoppingListBox() throws Exception {
		Assert.assertTrue("--- Failed to see create shopping list box! ---", skuDetailsPage.isExpectedLightingBoxDisplayed("Create New List"));
	}

}
