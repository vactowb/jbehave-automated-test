package com.officedepot.jbehave.mobile.www.pages.csl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class ShoppingListDetailPage extends BasePage {

	private String expectedListName = "";
	private String expectedReminderDate = "";

	private WebElement selectAllCheckbox;
	private WebElement addToCartButton;
	private WebElement listNameBoxField;
	private WebElement commentsBoxField;
	private WebElement updateBoxButton;
	private WebElement reminderDateBoxField;
	private WebElement skuIdBoxFiled;
	private WebElement qtyBoxField;
	private WebElement addToListBoxButton;
	private WebElement editButton;
	private WebElement quickOrderLink;
	private WebElement cancelQuickOrderBoxButton;
	private WebElement removeButton;
	private List<WebElement> entryCheckBoxes;

	public boolean isNavigateOnThisPage() throws Exception {
		return isElementPresentBySelenium("css=#jst_modal_quickSku");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void addAllEntriesIntoCart() throws Exception {
		selectAllCheckbox = findElementById("selectAll");
		clickOn(selectAllCheckbox);
		addToCart();
	}

	public void addToCart() {
		addToCartButton = findElementByName("cmd_addToCartFromCsl.button");
		clickOn(addToCartButton);
	}

	public void updateListWithNewNameAndComments() throws Exception {
		listNameBoxField = findElementByXpath("(//input[@id='listName'])[2]");
		String newListName = SeleniumUtils.giveUniqueIdAfter("CSL").substring(0, 9);
		expectedListName = newListName;
		typeTextBox(listNameBoxField, newListName);
		commentsBoxField = findElementByCSS("#boxes_content > #listCslDetails > input[name='description']");
		String newListComments = SeleniumUtils.giveUniqueIdAfter("CSLComments").substring(0, 15);
		typeTextBox(commentsBoxField, newListComments);
		updateListInfo();
	}

	public void updateListInfo() {
		updateBoxButton = findElementByCSS("#boxes_content > #listCslDetails > div.line1_height > input[name='cmd_updateCsl']");
		clickOn(updateBoxButton);
	}

	public boolean isListNameChangedAsExpected() throws Exception {
		return expectedListName.equals(getText(findElementByCSS("#subListName>p")));
	}

	@SuppressWarnings("deprecation")
	public void updateReminderDate() throws Exception {
		reminderDateBoxField = findElementByXpath("(//input[@id='reminderDate'])[2]");
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date reminderDate = formatter.parse(reminderDateBoxField.getAttribute("value"));
		reminderDate.setYear(reminderDate.getYear() + 1);
		expectedReminderDate = formatter.format(reminderDate);
		typeTextBox(reminderDateBoxField, expectedReminderDate);
		updateListInfo();
	}

	public boolean isReminderDateUpadted() throws Exception {
		return expectedReminderDate.equals(getText(findElementByCSS("#subReminders>p")));
	}

	public void quickOrderEntry(String skuId, String qty) throws Exception {
		skuIdBoxFiled = findElementByXpath("(//input[@id='skuAddToCsl'])[2]");
		typeTextBox(skuIdBoxFiled, skuId);
		qtyBoxField = findElementByXpath("(//input[@name='entryFormList[0].qty'])[2]");
		typeTextBox(qtyBoxField, qty);
		addToListBoxButton = findElementByXpath("(//input[@name='cmd_addSkuToCsl.button'])[2]");
		clickOn(addToListBoxButton);
		seleniumWaitForPageToLoad(30000);
	}

	public boolean isEntryAddedSuccessful(int entryQty) throws Exception {
		return (entryQty + 1) == findElementsByCSS(".checkbox.large_check").size();
	}

	public boolean isEntryRemovedSuccessful(int entryQty) throws Exception {
		return (entryQty - 1) == findElementsByCSS(".checkbox.large_check").size();
	}

	public void editShoppingList() {
		editButton = findElementById("jst_editListButton");
		clickOn(editButton);
	}

	public boolean isLightingBoxDisplayed(String title) {
		return isTextPresentInElement(By.cssSelector(".box_title"), title);
	}

	public void quickOrder() {
		quickOrderLink = findElementByCSS("#jst_modal_quickSku");
		clickOn(quickOrderLink);
	}

	public int getEntryQty() {
		if (isElementPresentBySelenium("css=.checkbox.large_check")) {
			return findElementsByCSS(".checkbox.large_check").size();
		} else {
			return 1;
		}
	}

	public void cancelQuickOrder() {
		cancelQuickOrderBoxButton = findElementByCSS("#boxes_content > #listCslDetails > div.clear.line1_height > span.button.jst_close_boxes");
		clickOn(cancelQuickOrderBoxButton);
	}

	public void removeEntry() {
		removeButton = findElementByName("cmd_removeItemsFromCsl.button");
		clickOn(removeButton);
	}

	public void selectEntry(int index) {
		entryCheckBoxes = findElementsByCSS(".checkbox.large_check");
		clickOn(entryCheckBoxes.get(index));
	}

}
