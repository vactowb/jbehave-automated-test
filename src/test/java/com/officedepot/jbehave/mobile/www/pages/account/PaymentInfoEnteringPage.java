package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class PaymentInfoEnteringPage extends BasePage {

	protected WebElement submitButton;
	protected WebElement useShipmentAddCheckBox;
	protected WebElement paymentInfoField;
	protected By pageVerifyItem = By.cssSelector(".page_title");

	public void fillPaymentInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputPaymentInfoField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public WebElement getPaymentInfoField(String field) {
		if (field.equalsIgnoreCase("nick name")) {
			return findElementById("jst_getPlaceholder");
		}
		if (field.equalsIgnoreCase("holder name")) {
			return findElementByName("creditCardHolderName");
		}
		if (field.equalsIgnoreCase("card number")) {
			return findElementByName("creditCardNumber");
		}
		if (field.equalsIgnoreCase("month")) {
			return findElementById("monthId");
		}
		if (field.equalsIgnoreCase("year")) {
			return findElementById("yearId");
		}
		if (field.equalsIgnoreCase("address")) {
			return findElementById("address1-0");
		}
		if (field.equalsIgnoreCase("city")) {
			return findElementById("city-0");
		}
		if (field.equalsIgnoreCase("state")) {
			return findElementById("state-0");
		}
		if (field.equalsIgnoreCase("post code")) {
			return findElementById("postalCode1-0");
		}
		return null;
	}

	public void submit() throws Exception {
		submitButton = findElementByXpath("//input[@value='Submit']");
		clickOn(submitButton);
	}

	public String getPaymentInfoFieldValue(String field) {
		paymentInfoField = getPaymentInfoField(field);
		return getValue(paymentInfoField);
	}

	public void inputPaymentInfoField(String field, String value) {
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		paymentInfoField = getPaymentInfoField(field);
		if (field.equalsIgnoreCase("month") || field.equalsIgnoreCase("year") || field.equalsIgnoreCase("state")) {
			selectOptionInListByText(paymentInfoField, value);
		} else {
			typeTextBox(paymentInfoField, value);
		}
	}

	public void chooseSameAsBilling() throws Exception {
		useShipmentAddCheckBox = findElementById("useBillingAddress");
		clickOn(useShipmentAddCheckBox);
	}

}
