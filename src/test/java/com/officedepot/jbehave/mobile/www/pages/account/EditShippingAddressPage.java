package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EditShippingAddressPage extends AddNewShippingAddressPage {

	private WebElement cancelButton;
	private By pageVerifyItem = By.cssSelector(".page_title");

	
	@Override
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Edit Shipping Address");
	}	
	
	public void cancelEdit() {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
	}
	
}
