package com.officedepot.jbehave.mobile.www.pages.odr;

import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;

public class ODRExpressRegisterPage extends RegisterPage {

	@Override
	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Join Now");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public boolean isFieldPrefilled(String field) {
		if (field.equalsIgnoreCase("phone number")) {
			registrationField = findElementByName("phone");
		} else if (field.equalsIgnoreCase("email")) {
			registrationField = findElementByName("email");
		}
		return !getValue(registrationField).isEmpty();
	}

}
