package com.officedepot.jbehave.mobile.www.steps.list;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.school.SchoolListPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SchoolListSteps extends BaseStep{
	
	private SchoolListPage schoolListPage = new SchoolListPage();
	
	@When("I access a school list")
	public void accessSchoolList() throws Exception{
		schoolListPage.accessSchoolListUrl("/a/ssc/3970/PatrickTest/");
	}
	
	@When("I add items to cart")
	public void addItemsToCart() throws Exception{
		schoolListPage.addItemsToCart();
	}
	
	@Then("I should see cart page")
	public void iShouldSeeCartPage() throws Exception{
		schoolListPage.isElementPresent(By.name("Your Shopping Cart"));
	}
	
	
	@When("I access a BTS link")
	public void accessBTSLink() throws Exception{
		schoolListPage.accessSchoolListUrl("/a/browse/school-supplies/N=5+502600/?hijack=back%20to%20school&type=Search");
	}
	
	@When("I choose grade 3")
	public void chooseGradeThree() throws Exception{
		assertTrue(schoolListPage.isInCurrentPage("grades k - 3"));
		schoolListPage.clickGrade(1);
	}
	
	@Then("I see category under grade 3")
	public void iSeeCategoryOfGrade3() throws Exception{
		assertTrue(schoolListPage.isInCurrentPage("Grades K - 3"));
	}
	
	@Then("I see the markers subcategory")
	public void iSeeTheMarkers() throws Exception{
		assertTrue(schoolListPage.isInCurrentPage("Markers"));
	}
	
}
