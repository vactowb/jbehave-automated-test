package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class MyRewardsPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");

	public boolean isNavigateOnMyRewardsPage() {
		return isTextPresentInElement(pageVerifyItem, "My Rewards");
	}

	public boolean isNavigateOnThankYouPage() {
		return isTextPresentInElement(pageVerifyItem, "Thank You");
	}

}
