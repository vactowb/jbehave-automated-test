package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.By;

import com.officedepot.test.jbehave.BasePage;

public class ChooseDeliveryMethodPage extends BasePage {

	public boolean isInCurrentPage() throws Exception {
		return this.isTextPresentInElement(By.cssSelector(".page_title.center.vspace_bottom>strong"), "Office Depot Rewards");
	}
	
}
