package com.officedepot.jbehave.mobile.www.pages.sku;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SkuRatingsReviewsPage extends BasePage {

	private WebElement addToCartButton;
	private WebElement breadcrumbLink;
	private WebElement skuIcon;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Reviews");
	}

	public void addToCart() {
		addToCartButton = findElementByName("cmd_addCart.button");
		clickOn(addToCartButton);
	}

	public void backToSkuDetailsFromBreadcrumb() {
		breadcrumbLink = findElementByLinkText("Product Details");
		clickOn(breadcrumbLink);
	}

	public void backToSkuDetailsFromIcon() {
		skuIcon = findElementByCSS("div.img_wrapper > a > img");
		clickOn(skuIcon);
	}

}
