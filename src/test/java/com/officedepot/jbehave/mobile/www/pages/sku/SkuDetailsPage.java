package com.officedepot.jbehave.mobile.www.pages.sku;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.csl.CreateShoppingListPage;
import com.officedepot.jbehave.mobile.www.pages.csl.SelectShoppingListPage;
import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class SkuDetailsPage extends BasePage {

	private By skuIdItem = By.cssSelector("#itemStars");
	private WebElement addToListButton;
	private WebElement addToCartButton;
	private WebElement qtyTextBox;
	private WebElement minusQtyButton;
	private WebElement plusQtyButton;
	private WebElement descriptionSection;
	private WebElement productDetailsSection;
	private WebElement ratingsReviewsSection;
	private WebElement checkAvailbilitySection;
	private WebElement alsoAvailableInSection;
	private WebElement addToListBoxButton;
	private List<WebElement> existingListRadioButtons;
	private WebElement createNewListBoxButton;
	private WebElement cancelAddToListBoxButton;
	private WebElement cancelCreateListBoxButton;
	private WebElement createListBoxButton;
	private WebElement setDefaultListBoxCheckBox;
	private WebElement showMoreBoxButton;
	private WebElement listNameBoxField;
	private WebElement descriptionBoxField;
	private String filledListName;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Product Details");
	}

	public boolean isSearchForExceptedSku(String skuId) {
		return isTextPresentInElement(skuIdItem, skuId);
	}

	public void clickAddToList() {
		addToListButton = findElementByName("cmd_addItemsToCsl.button");
		clickOn(addToListButton);
		seleniumWaitForPageToLoad(30000);
	}

	public void clickAddToCart() {
		addToCartButton = findElementByName("cmd_addCart.button");
		clickOn(addToCartButton);
	}

	public void inputQty(String number) {
		qtyTextBox = findElementByCSS(".jst_sku_quantity");
		typeTextBox(qtyTextBox, number);
	}

	public String getQtyValue() {
		qtyTextBox = findElementByCSS(".jst_sku_quantity");
		return getValue(qtyTextBox);
	}

	public void plusQty() {
		plusQtyButton = findElementByCSS("input.jst_item_plus");
		clickOn(plusQtyButton);
	}

	public void minusQty() {
		minusQtyButton = findElementByCSS("input.jst_item_minus");
		clickOn(minusQtyButton);
	}

	public boolean isSkuInStatusOf(String status) {
		return isTextPresentInElement(By.xpath("//*[@id='sku']/section[5]/div[1]/ul/li[1]"), status);
	}

	public SkuDescriptionPage viewDescription() {
		descriptionSection = findElementByLinkText("Description");
		clickOn(descriptionSection);
		return new SkuDescriptionPage();
	}

	public SkuProductDetailsPage viewProductDetails() {
		productDetailsSection = findElementByLinkText("Product Details");
		clickOn(productDetailsSection);
		return new SkuProductDetailsPage();
	}

	public SkuRatingsReviewsPage viewRatingsAndReviews() {
		ratingsReviewsSection = findElementByCSS("span.span3");
		clickOn(ratingsReviewsSection);
		return new SkuRatingsReviewsPage();
	}

	public CheckInStoreAvailbilityPage checkAvailbility() {
		checkAvailbilitySection = findElementById("btn_inStoreAvailability");
		clickOn(checkAvailbilitySection);
		return new CheckInStoreAvailbilityPage();
	}

	public AlsoAvailableInPage checkAlsoAvailableIn() {
		alsoAvailableInSection = findElementByCSS("a.bold");
		clickOn(alsoAvailableInSection);
		return new AlsoAvailableInPage();
	}

	public boolean isExpectedLightingBoxDisplayed(String title) {
		return isTextPresentInElement(By.cssSelector(".box_title"), title);
	}

	public void selectAnExistingList(int index) throws Exception {
		existingListRadioButtons = findElementsByCSS(".v2_radioLabel");
		clickOn(existingListRadioButtons.get(index));
	}

	public void addToListFromBox() {
		addToListBoxButton = findElementByName("cmd_addItemsToCsl");
		clickOn(addToListBoxButton);
	}

	public void cancelToAddToListFromBox() {
		cancelAddToListBoxButton = findElementById("cancel_close");
		clickOn(cancelAddToListBoxButton);
	}

	public void setDefaultList() {
		setDefaultListBoxCheckBox = findElementByCSS("#defaultList");
		clickOn(setDefaultListBoxCheckBox);
	}

	public SelectShoppingListPage showMoreLists() {
		showMoreBoxButton = findElementByName("cmd_addItemsInput");
		clickOn(showMoreBoxButton);
		return new SelectShoppingListPage();
	}

	public String getListNameFromBox(int index) {
		existingListRadioButtons = findElementsByCSS(".v2_radioLabel");
		return getText(existingListRadioButtons.get(index));
	}

	public boolean isDefaultListExistingFromBox() {
		existingListRadioButtons = findElementsByCSS(".v2_radioLabel");
		return getText(existingListRadioButtons.get(0)).indexOf("Default") != -1;
	}

	public CreateShoppingListPage createNewListFromBox() {
		createNewListBoxButton = findElementByCSS(".button.b1a.display_block");
		clickOn(createNewListBoxButton);
		return new CreateShoppingListPage();
	}

	public void cancelToCreateListFromBox() {
		cancelCreateListBoxButton = findElementByCSS(".button.b2.f_left");
		clickOn(cancelCreateListBoxButton);
	}

	public void createListFromBox() {
		createListBoxButton = findElementByCSS(".button.atl.b1.f_right");
		clickOn(createListBoxButton);
	}

	public void fillListDetailsFromBox() {
		listNameBoxField = findElementByName("listName");
		filledListName = SeleniumUtils.giveUniqueIdAfter("CSL").substring(0, 9);
		typeTextBox(listNameBoxField, filledListName);
		descriptionBoxField = findElementByName("description");
		typeTextBox(descriptionBoxField, SeleniumUtils.giveUniqueIdAfter("CSLComments").substring(0, 15));
	}

	public String getFilledListName() {
		return filledListName;
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

}
