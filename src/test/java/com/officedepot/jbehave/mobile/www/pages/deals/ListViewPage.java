package com.officedepot.jbehave.mobile.www.pages.deals;

import com.officedepot.test.jbehave.BasePage;

public class ListViewPage extends BasePage {

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("List View");
	}

}
