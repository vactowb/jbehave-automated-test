package com.officedepot.jbehave.mobile.www.pages.login;

import org.openqa.selenium.WebElement;


public class GmailAccountLoginPage extends SocialAccountLogInPage{

	private WebElement email;
	private WebElement password;
	private WebElement bContinue;
	
	 @Override
	 public void logIn(String accountName, String pwd) {
	 	email = findElementById("Email");
	 	password = findElementById("Passwd");
	 	typeTextBox(email, accountName);
	 	typeTextBox(password, pwd);
	 	bContinue = findElementById("signIn");
    	clickOn(bContinue);
}
}
