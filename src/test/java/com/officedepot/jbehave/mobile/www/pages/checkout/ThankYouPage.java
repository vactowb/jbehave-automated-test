package com.officedepot.jbehave.mobile.www.pages.checkout;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.MergeAccountsOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.account.OrderDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.survey.SurveyPage;
import com.officedepot.test.jbehave.BasePage;

public class ThankYouPage extends BasePage {

	private WebElement takeSurveyButton;
	private List<WebElement> orderLinks;
	private WebElement skuIcon;
	private WebElement mergeAccountsButton;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Thank You for Shopping");
	}

	public SurveyPage takeSurvey() {
		takeSurveyButton = findElementByCSS("a.b6.fullwidth");
		clickOn(takeSurveyButton);
		seleniumWaitForPageToLoad(30000);
		return new SurveyPage();
	}

	public boolean isSurveyButtonDisplayed() {
		return isElementPresentBySelenium("css=a.b6.fullwidth");
	}

	public String getOrderNumber(int index) {
		orderLinks = findElementsByCSS(".ab_order_ids");
		return getText(orderLinks.get(index));
	}

	public OrderDetailsPage viewOrderDetails(int index) {
		orderLinks = findElementsByCSS(".ab_order_ids");
		clickOn(orderLinks.get(index));
		return new OrderDetailsPage();
	}

	public int getOrderQty() {
		orderLinks = findElementsByCSS(".ab_order_ids");
		return orderLinks.size();
	}

	public SkuDetailsPage viewSkuDetails() {
		skuIcon = findElementByCSS("img.delivery_item_image");
		clickOn(skuIcon);
		return new SkuDetailsPage();
	}

	public boolean isMergeAccountsSectionDisplayed() {
		return isTextPresentOnPage("Would you like to merge it with your");
	}

	public MergeAccountsOptionsPage mergeAccounts() {
		mergeAccountsButton = findElementByCSS(".b6.vspace_top.vspace_bottom_half");
		clickOn(mergeAccountsButton);
		return new MergeAccountsOptionsPage();
	}
}
