package com.officedepot.jbehave.mobile.www.pages.home;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.others.FullSiteHomePage;
import com.officedepot.jbehave.mobile.www.pages.others.PrivacyPolicyPage;
import com.officedepot.jbehave.mobile.www.pages.others.TermsPage;
import com.officedepot.jbehave.mobile.www.pages.signup.EmailSMSSignupPage;
import com.officedepot.test.jbehave.BasePage;

public class PageFooter extends BasePage {

	private WebElement signUpField;
	private WebElement joinButton;
	private WebElement policyLink;
	private WebElement termsLink;
	private WebElement fullSiteLink;

	public void inputSignUpField(String contact) {
		signUpField = findElementByName("emailOrPhone");
		typeTextBox(signUpField, contact);
	}

	public EmailSMSSignupPage join() {
		joinButton = findElementByCSS(".button.b3r.flo_right");
		clickOn(joinButton);
		return new EmailSMSSignupPage();
	}

	public PrivacyPolicyPage viewPolicy() {
		policyLink = findElementByXpath("(//a[contains(text(),'Privacy Policy')])[2]");
		clickOn(policyLink);
		return new PrivacyPolicyPage();
	}

	public TermsPage viewTerms() {
		termsLink = findElementByLinkText("Terms of Use");
		clickOn(termsLink);
		return new TermsPage();
	}

	public FullSiteHomePage accessFullSite() {
		fullSiteLink = findElementsByCSS(".text").get(2);
		clickOn(fullSiteLink);
		return new FullSiteHomePage();
	}

}
