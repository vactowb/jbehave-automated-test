package com.officedepot.jbehave.mobile.www.pages.others;

import com.officedepot.test.jbehave.BasePage;

public class PrivacyPolicyPage extends BasePage {

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Privacy Statement");
	}

}
