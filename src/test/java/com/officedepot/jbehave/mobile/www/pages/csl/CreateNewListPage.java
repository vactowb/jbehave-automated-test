package com.officedepot.jbehave.mobile.www.pages.csl;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class CreateNewListPage extends BasePage {

	private WebElement listNameField;
	private WebElement descriptionField;
	private WebElement createButton;
	private WebElement cancelButton;
	private WebElement setAsDefaultCheckBox;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Create New List");
	}

	public void cancel() {
		cancelButton = findElementByCSS("a.button");
		clickOn(cancelButton);
	}

	public void createList() {
		createButton = findElementByCSS(".button.b1.f_right");
		clickOn(createButton);
	}

	public void fillListDetails() {
		listNameField = findElementByName("listName");
		typeTextBox(listNameField, SeleniumUtils.giveUniqueIdAfter("CSL").substring(0, 9));
		descriptionField = findElementByName("description");
		typeTextBox(descriptionField, SeleniumUtils.giveUniqueIdAfter("CSLComments").substring(0, 15));
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void setAsDefault() {
		setAsDefaultCheckBox = findElementById("defaultList");
		clickOn(setAsDefaultCheckBox);
	}

}
