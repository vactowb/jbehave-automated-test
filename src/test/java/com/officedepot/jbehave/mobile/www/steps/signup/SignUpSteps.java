package com.officedepot.jbehave.mobile.www.steps.signup;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.PageFooter;
import com.officedepot.jbehave.mobile.www.pages.signup.EmailSMSSignupPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SignUpSteps extends BaseStep {

	private PageFooter pageFooter;
	private EmailSMSSignupPage signUpPage;

	@When("I sign up for offer")
	public void signUpOffer() throws Exception {
		pageFooter = new PageFooter();
		signUpPage = pageFooter.join();
	}

	@Then("I should navigate on sign up page")
	public void displySignUpPage() throws Exception {
		if (signUpPage == null) {
			signUpPage = new EmailSMSSignupPage();
		}
		Assert.assertTrue("---Fail to redirect email sign up page!---", signUpPage.isNavigateOnThisPage());
	}

	@When("I fill the sign up fields with following info: $table")
	public void fillAllFields(ExamplesTable table) throws Exception {
		signUpPage.fillSignUpInfo(table);
	}

	@When("I submit to signup")
	public void submit() throws Exception {
		signUpPage.submitToSignUp();
	}

	@Then("I should see my request submitted")
	public void successSignUp() throws Exception {
		Assert.assertTrue("---Fail to email sign up!---", signUpPage.successSignUp());
	}

	@Then("I should be noticed that the \"$content\" is required from sign up page")
	public void errorMessageFillMandatory(String content) throws Exception {
		Assert.assertTrue("---Fail to see error message!---", signUpPage.isErrorMessageDisplayed(content + " is required"));
	}

	@Then("I should be noticed that the phone number is invalid from sign up page")
	public void errorMessagePhoneNumberInvalid() throws Exception {
		Assert.assertTrue("---Fail to see error message!---", signUpPage.isErrorMessageDisplayed("is a numeric field"));
	}

	@Then("I should be noticed that the zip code is invalid from sign up page")
	public void errorMessageZipCodeInvalid() throws Exception {
		Assert.assertTrue("---Fail to see error message!---", signUpPage.isErrorMessageDisplayed("Zip Code is invalid"));
	}

	@Then("I should be noticed that the email or phone number is invalid")
	public void iShouldBeNoticedThatTheEmailOrPhoneNumberIsInvalid() throws Exception {
		if (signUpPage == null) {
			signUpPage = new EmailSMSSignupPage();
		}
		Assert.assertTrue("---Fail to see error message!---", signUpPage.isErrorMessageDisplayed("Invalid email address or phone number"));
	}

	@Then("I should see the email is filled")
	public void iShouldSeeTheEmailIsFilled() throws Exception {
		if (signUpPage == null) {
			signUpPage = new EmailSMSSignupPage();
		}
		Assert.assertTrue("---Fail to verify the filled email!---", signUpPage.getFilledEmail().equals(getParameter("sign up contact")));
	}

	@Then("I should see the phone number is filled")
	public void iShouldSeeThePhoneNumberIsFilled() throws Exception {
		if (signUpPage == null) {
			signUpPage = new EmailSMSSignupPage();
		}
		Assert.assertTrue("---Fail to verify the filled phone number!---", signUpPage.isPhoneNumberFilled((String) getParameter("sign up contact")));
	}
}
