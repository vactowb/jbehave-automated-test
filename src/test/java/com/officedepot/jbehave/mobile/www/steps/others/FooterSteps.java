package com.officedepot.jbehave.mobile.www.steps.others;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.PageFooter;
import com.officedepot.jbehave.mobile.www.pages.others.FullSiteHomePage;
import com.officedepot.jbehave.mobile.www.pages.others.PrivacyPolicyPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class FooterSteps extends BaseStep {

	private PageFooter pageFooter;
	private PrivacyPolicyPage privacyPolicyPage;
	private FullSiteHomePage fullSiteHomePage;

	@When("I request to sign up with \"$content\" from footer")
	public void iRequestToSignUpWithFromFooter(String contact) throws Exception {
		pageFooter = new PageFooter();
		this.setParameter("sign up contact", contact);
		pageFooter.inputSignUpField(contact);
		pageFooter.join();
	}

	@When("I request to view privacy policy from footer")
	public void iRequestToViewPrivacyPlicyFromFooter() throws Exception {
		privacyPolicyPage = pageFooter.viewPolicy();
	}

	@Then("I should navigate on privacy policy page")
	public void iShouldNavigateOnPrivacyPlicyPage() throws Exception {
		if (privacyPolicyPage == null) {
			privacyPolicyPage = new PrivacyPolicyPage();
		}
		Assert.assertTrue("---Fail to navigate on policy page!---", privacyPolicyPage.isNavigateOnThisPage());
	}

	@When("I request to view terms from footer")
	public void iRequestToViewTermsFromFooter() throws Exception {
		pageFooter.viewTerms();
	}

	@When("I request to access full site from footer")
	public void iRequestToAccessFullSiteFromFooter() throws Exception {
		fullSiteHomePage = pageFooter.accessFullSite();
	}

	@Then("I should navigate on full site")
	public void iShouldNavigateOnFullSite() throws Exception {
		Assert.assertTrue("---Fail to navigate on full site home page!---", fullSiteHomePage.isNavigateOnThisPage());
	}

	@When("I go back to mobile site")
	public void iGoBackToMobileSite() throws Exception {
		fullSiteHomePage.goBackToMobileSite();
	}

}
