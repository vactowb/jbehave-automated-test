package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.WebElement;

public class MemberNumberEnteringPage extends CreateAccountPage {

	private WebElement registerButton;

	@Override
	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Member Number");
	}

	public CreateMemberIDPage registerForNewMember() {
		registerButton = findElementByLinkText("Register");
		clickOn(registerButton);
		return new CreateMemberIDPage();
	}

}
