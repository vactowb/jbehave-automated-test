package com.officedepot.jbehave.mobile.www.pages.sku;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SkuProductDetailsPage extends BasePage {

	private WebElement addToCartButton;
	private WebElement breadcrumbLink;
	private WebElement skuIcon;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Product details");
	}

	public void addToCart() {
		addToCartButton = findElementByName("cmd_addCart.button");
		clickOn(addToCartButton);
	}

	public boolean isForExpectedSku(String skuId) {
		return isTextPresentInElement(By.xpath("//*[@id='mainContent']/section[2]/ul/li[1]/div[2]"), skuId);
	}

	public void backToSkuDetailsFromBreadcrumb() {
		breadcrumbLink = findElementByLinkText("Product Details");
		clickOn(breadcrumbLink);
	}

	public void backToSkuDetailsFromIcon() {
		skuIcon = findElementByCSS("div.img_wrapper > a > img");
		clickOn(skuIcon);
	}

}
