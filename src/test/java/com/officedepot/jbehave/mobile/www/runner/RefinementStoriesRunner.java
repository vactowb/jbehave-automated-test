package com.officedepot.jbehave.mobile.www.runner;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import com.officedepot.jbehave.mobile.www.MobileWWWBaseRunner;
import com.officedepot.test.common.annotation.Brand;
import com.officedepot.test.common.annotation.Locale;
import com.officedepot.test.common.model.Site;
import com.officedepot.test.jbehave.annotation.Browsers;
import com.officedepot.test.jbehave.model.Browser;

@Brand(Site.OD)
@Locale(language="en", country="US")
@Browsers(Browser.MOBILE)
public class RefinementStoriesRunner extends MobileWWWBaseRunner {

	@Override
	protected List<String> storyPaths() {
		String codeLocation = codeLocationFromClass(this.getClass()).getFile();
		return new StoryFinder().findPaths(codeLocation, asList("**/stories/mobile/www/refinement/*.story"), asList(""));
	}

}