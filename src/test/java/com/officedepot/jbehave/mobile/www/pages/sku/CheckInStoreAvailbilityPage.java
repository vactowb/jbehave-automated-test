package com.officedepot.jbehave.mobile.www.pages.sku;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.store.StoreDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.store.StoreLocatorPage;

public class CheckInStoreAvailbilityPage extends StoreLocatorPage {

	private WebElement checkAvailbilityButton;
	private List<WebElement> storeLinks;

	@Override
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "In-Store Availability");
	}

	public void checkAvailbility() throws Exception {
		checkAvailbilityButton = findElementById("jst_storeSubmit");
		clickOn(checkAvailbilityButton);
	}

	public boolean isCheckAvailbilityButtonAvailable() {
		return isElementPresentBySelenium("css=button f_right b1");
	}

	public boolean isStoreListDisplayed() {
		return isElementPresentBySelenium("css=.bold.display_block.arrow_primary_right");
	}

	public StoreDetailsPage viewStoreDetails(int index) {
		storeLinks = findElementsByCSS(".bold.display_block.arrow_primary_right");
		clickOn(storeLinks.get(index));
		return new StoreDetailsPage();
	}

	public boolean isNavigateOnThePageForExpectedSku(String skuId) {
		return getText(findElementByCSS(".item_sku.f_right")).indexOf(skuId) != -1;
	}
}
