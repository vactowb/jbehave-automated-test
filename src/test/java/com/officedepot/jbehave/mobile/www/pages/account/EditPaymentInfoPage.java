package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class EditPaymentInfoPage extends BasePage {

	private WebElement deleteButton;

	public void deleteCreditCard() throws Exception {
		deleteButton = findElementByLinkText("DELETE");
		clickOn(deleteButton);
		seleniumWaitForPageToLoad(30000);
	}

}
