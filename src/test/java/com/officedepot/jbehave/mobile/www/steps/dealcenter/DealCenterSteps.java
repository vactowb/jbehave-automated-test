package com.officedepot.jbehave.mobile.www.steps.dealcenter;

import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.deals.DealsPage;
import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class DealCenterSteps extends BaseStep {

	private PageHeader pageHeader;
	private DealsPage dealsPage;
	private HomePage homePage;

	@When("I view deals from header")
	public void viewDealsFromHeader() throws Exception {
		pageHeader = new PageHeader();
		dealsPage = pageHeader.goToDeals();
	}

	@When("I request to view deals from home page")
	public void viewDealsFromHomePage() throws Exception {
		homePage = new HomePage();
		dealsPage = homePage.viewDeals();
	}

	@When("I navigate to weekly ads from deals page")
	public void checkWeeklyAds() throws Exception {
		dealsPage.goToWeeklyAds();
	}

	@When("I navigate to deal center from deals page")
	public void checkDealCenter() throws Exception {
		dealsPage.goToDealCenter();
	}

	@When("I request to change my store from deals page")
	public void changeMyStore() throws Exception {
		dealsPage.changeMyStore();
	}

}
