package com.officedepot.jbehave.mobile.www.steps.survey;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.checkout.ThankYouPage;
import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.survey.SurveyPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SurveySteps extends BaseStep {

	private HomePage homePage;
	private SurveyPage surveyPage;
	private ThankYouPage thankYouPage;

	@When("I take survey from home page")
	public void iTakeSurveyFromHomePage() throws Exception {
		homePage = new HomePage();
		surveyPage = homePage.openSurveyPage();
	}

	@Then("I should navigate to survey page")
	public void iShouldNavigateToSurveyPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on survey page! ---", surveyPage.isNavigateOnThisPage());
	}

	@When("I submit survey with no answer selected")
	public void iSubmitSurveyWithNoAnswerSelected() throws Exception {
		surveyPage.submitSurvey();
	}

	@Then("I should be noticed that the answers are required")
	public void iShouldBeNoticedThatTheAnswersAreRequired() throws Exception {
		Assert.assertTrue("--- I should see the error tip! ---", surveyPage.isAnswerUnselected());
	}

	@When("I submit survey with valid info")
	public void iSubmitSurveyWithValidInfo() throws Exception {
		surveyPage.fillSurveyAnswer("Browse or research products", "Extremely Successful");
		surveyPage.fillComment("test");
		surveyPage.submitSurvey();
	}

	@Then("I should navigate to home page")
	public void iShouldNavigateToHomePage() throws Exception {
		Assert.assertTrue("--- Failed to navigate on home page! ---", homePage.isNavigateOnThisPage());
	}

	@When("I take survey from thank you page")
	public void iTakeSurveyFromThankYouPage() throws Exception {
		thankYouPage = new ThankYouPage();
		surveyPage = thankYouPage.takeSurvey();
	}

	@Then("I should not see the take survey button from thank you page")
	public void iShouldNotSeeSurveyButtonFromThankYouPage() throws Exception {
		thankYouPage = new ThankYouPage();
		Assert.assertFalse("--- I should not see the survey button! ---", thankYouPage.isSurveyButtonDisplayed());
	}

}
