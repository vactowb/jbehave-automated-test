package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ChangePasswordPage extends BasePage {

	private WebElement registrationField;
	private WebElement saveButton;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Change Password");
	}

	public void inputPasswordInfo(String oldPassword, String newPassword) {
		inputRegistrationInfo("oldPassword", oldPassword);
		inputRegistrationInfo("newPassword", newPassword);
		inputRegistrationInfo("confirmNewPassword", newPassword);
	}

	public void inputRegistrationInfo(String field, String value) {
		registrationField = findElementByName(field);
		typeTextBox(registrationField, value);
	}

	public void clickOnUpdateButton() {
		saveButton = findElementByName("Save");
		clickOn(saveButton);
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

}
