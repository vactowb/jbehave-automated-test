package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class MyProfilePage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".sub_title.guide1");
	private WebElement changePasswordLink;
	private WebElement securityQuestionLink;
	private WebElement combineOMXAccountLink;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "My Profile");
	}

	public ChangeSecurityQuestionsPage clickOnChangeSecurityQuestionLink() throws Exception {
		securityQuestionLink = findElementByLinkText("Change Security Question");
		clickOn(securityQuestionLink);
		return new ChangeSecurityQuestionsPage();
	}

	public ChangePasswordPage clickOnChangePasswordLink() throws Exception {
		changePasswordLink = findElementByLinkText("Change Password");
		clickOn(changePasswordLink);
		return new ChangePasswordPage();
	}

	public MergeVerifyAccountPage combineOMXAccount() {
		combineOMXAccountLink = findElementByLinkText("Combine OfficeMax Account");
		clickOn(combineOMXAccountLink);
		return new MergeVerifyAccountPage();
	}

}
