package com.officedepot.jbehave.mobile.www.pages.checkout;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.PaymentInfoEnteringPage;

public class EnterNewPaymentMethodPage extends PaymentInfoEnteringPage {

	private WebElement cvvField;
	
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "New Payment")
				&& isElementPresentBySelenium("css=.f_left.step3.active");
	}
	
	public void enterCVVIfNeeds() {
		if(isElementPresentBySelenium("cvvId")) {
			cvvField = findElementById("cvvId");
	    	typeTextBox(cvvField, "123");
		}
    }
	
}
