package com.officedepot.jbehave.mobile.www.pages.survey;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SurveyPage extends BasePage {

	private WebElement submitButton;
	private WebElement reasonAnswerSelectList;
	private WebElement successAnswerSelectList;
	private WebElement commentsTextField;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("What was your primary reason for visiting OfficeDepot.com today?");
	}

	public void submitSurvey() {
		submitButton = findElementById("forwardbutton");
		clickOn(submitButton);
	}

	public boolean isAnswerUnselected() {
		return isTextPresentInElement(By.cssSelector("#Reason_error"), "Please select an answer") || isTextPresentInElement(By.cssSelector("#Success_error"), "Please select an answer");
	}

	public void fillSurveyAnswer(String answer1, String answer2) {
		reasonAnswerSelectList = findElementById("Reason");
		selectOptionInListByText(reasonAnswerSelectList, answer1);
		successAnswerSelectList = findElementById("Success");
		selectOptionInListByText(successAnswerSelectList, answer2);
	}

	public void fillComment(String text) {
		commentsTextField = findElementById("comment");
		typeTextBox(commentsTextField, text);
	}

}
