package com.officedepot.jbehave.mobile.www.pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class CheckoutOptionsPage extends BasePage {

	private WebElement checkOutAsGuestButton;
	private WebElement paypalCheckoutButton;
	private By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement loginUserNameField;
	private WebElement loginPassWordField;
	private WebElement loginButton;
	private WebElement registerLink;

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Checkout Options");
	}

	public void loginFromCheckoutOptions(String username, String password) {
		if (username.indexOf("RANDOM") != -1) {
			username = SeleniumUtils.genRandomStringByTableParam(username);
		}
		if (password.indexOf("RANDOM") != -1) {
			password = SeleniumUtils.genRandomStringByTableParam(password);
		}
		loginUserNameField = findElementByName("loginName");
		loginPassWordField = findElementByName("password");
		loginButton = findElementByCSS("input.button.b1a");
		typeTextBox(loginUserNameField, username);
		typeTextBox(loginPassWordField, password);
		clickOn(loginButton);
	}

	public void register() {
		registerLink = findElementByLinkText("Register");
		clickOn(registerLink);
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public NewShippingAddressPage checkoutAsGuest() throws Exception {
		checkOutAsGuestButton = findElementByLinkText("Checkout As Guest");
		clickOn(checkOutAsGuestButton);
		return new NewShippingAddressPage();
	}

	public void checkoutWithPaypal() {
		paypalCheckoutButton = findElementByCSS(".paypal_checkout.display_block.vspace_top");
		clickOn(paypalCheckoutButton);
	}

}
