package com.officedepot.jbehave.mobile.www.pages.coupon;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SurprizePage extends BasePage {

	private WebElement couponField;
	private WebElement submitButton;

	public boolean isSurprizeItemDisplayed() {
		return isElementPresentBySelenium("css=#jst_scratchOff");
	}

	public void submitCoupon(String coupon) {
		couponField = findElementById("couponInput");
		typeTextBox(couponField, coupon);
		submitButton = findElementByName("submit");
		clickOn(submitButton);
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

}
