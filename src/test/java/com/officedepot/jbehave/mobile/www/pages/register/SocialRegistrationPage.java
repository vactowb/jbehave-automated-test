package com.officedepot.jbehave.mobile.www.pages.register;

import org.openqa.selenium.By;


public class SocialRegistrationPage extends RegisterPage {

	private By pageVerifyItem = By.cssSelector(".span3.t_right.gigyaHeight.bold");
	
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Social Login");
	}
		
}
