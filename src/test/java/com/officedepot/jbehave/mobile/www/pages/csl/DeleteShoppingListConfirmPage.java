package com.officedepot.jbehave.mobile.www.pages.csl;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class DeleteShoppingListConfirmPage extends BasePage {

	private WebElement deleteButton;
	private WebElement cancelButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("You are about to delete your shopping list");
	}

	public ShoppingListPage deleteList() throws Exception {
		deleteButton = findElementByName("cmd_delete");
		clickOn(deleteButton);
		return new ShoppingListPage();
	}

	public void cancelDelete() {
		cancelButton = findElementByName("cmd_cancel");
		clickOn(cancelButton);
	}

}
