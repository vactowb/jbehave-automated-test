package com.officedepot.jbehave.mobile.www.pages.checkout;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.PaymentInfoEnteringPage;

public class EditPaymentPage extends PaymentInfoEnteringPage {

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Edit Payment") 
				&& isElementPresentBySelenium("css=.f_left.step3.active");
	}
	
	@Override
	public WebElement getPaymentInfoField(String field) { 	
		if(field.equalsIgnoreCase("nick name")) {
			return findElementById("creditCardAlias");
		}
		if(field.equalsIgnoreCase("holder name")) {
			return findElementByName("creditCardHolderName");		
		}
		if(field.equalsIgnoreCase("card type")) {
			return findElementById("cardTypeId");
		}
		if(field.equalsIgnoreCase("card number")) {
			return findElementByName("creditCardNumber");
		}
		if(field.equalsIgnoreCase("month")) {
			return findElementById("monthId");
		}
		if(field.equalsIgnoreCase("year")) {
			return findElementById("yearId");
		}
		return null;
    }
	
}
