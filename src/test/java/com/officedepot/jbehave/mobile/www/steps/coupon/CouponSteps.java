package com.officedepot.jbehave.mobile.www.steps.coupon;

import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.coupon.CouponCenterPage;
import com.officedepot.jbehave.mobile.www.pages.coupon.SurprizePage;
import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.shoppingcart.CartPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CouponSteps extends BaseStep {

	private CartPage cartPage;
	private CouponCenterPage couponCenterPage;
	private HomePage homePage;
	private SurprizePage surPrizePage;
	private String couponCode = null;

	@Then("I should navigate to coupon center page")
	public void ishouldseeapagetoletmeinputthecouponcode() throws Exception {
		if (couponCenterPage == null) {
			couponCenterPage = new CouponCenterPage();
		}
		Assert.assertTrue("--- Failed to navigate on coupon center page! ---", couponCenterPage.isNavigateOnThisPage());
	}

	@When("I apply with the coupon code \"$code\"")
	public void iapplywithacouponcodelike(String code) throws Exception {
		if (cartPage == null) {
			cartPage = new CartPage();
		}
		couponCenterPage = cartPage.applyCoupon(code);
		this.couponCode = code;
	}

	@Then("I should see the error message \"$message\" from cart page")
	public void ishouldseethismessagefrompage(String message) throws Exception {
		Assert.assertTrue("--- Failed to see error message! ---", cartPage.isMessageDisplayed(message));
	}

	@When("I cancel to add coupon")
	public void icancelcouponinput() throws Exception {
		couponCenterPage.cancelAddCoupon();
	}

	@Then("I should see coupon added successfully from coupon center page")
	public void couponshouldsuccessfullyaddtocartCenterPage() throws Exception {
		Assert.assertTrue(couponCenterPage.isMessageDisplayed("Coupon Added to Cart") && couponCenterPage.isCouponAdded(couponCode));
	}

	@Then("I should see coupon added successfully from cart page")
	public void couponshouldsuccessfullyaddtomycart() throws Exception {
		Assert.assertTrue("--- Failed to add coupon! ---", cartPage.isCouponAdded(couponCode));
	}

	@When("I back to my cart")
	public void ireturntomycartwithcancel() throws Exception {
		couponCenterPage.backToCart();
	}

	@Then("I should see my items has been discounted in my cart")
	public void ishouldseeitemshasdiscountedinmycart() throws Exception {
		Assert.assertTrue(cartPage.isMessageDisplayed("after coupon"));
	}

	@When("I edit the added coupon from my cart")
	public void iviewcouponsfrommycart() throws Exception {
		if (cartPage == null) {
			cartPage = new CartPage();
		}
		cartPage.editCoupon(0);
	}

	@Then("I should see the coupons which I added")
	public void ishouldseethecouponswhichIadded() throws Exception {
		Assert.assertTrue(cartPage.isMessageDisplayed(couponCode));
	}

	@When("I removed the coupon")
	public void iremovedthecoupon() throws Exception {
		couponCenterPage.removeCoupon(couponCode);
	}

	@Then("I should see the coupon has been removed")
	public void iShouldSeeCouponRemoved() throws Exception {
		Assert.assertFalse("--- Failed to remove coupon! ---", couponCenterPage.isCouponAdded(couponCode));
	}

	@Then("I should see the coupon in cart light box")
	public void iShouldSeeCouponsInCartLightBox() throws Exception {
		Assert.assertTrue("---Fail to see coupons in cart light box!---", cartPage.isCouponInCartLightBoxDisplayed());
	}

	@When("I request to continue shopping from coupons in cart light box")
	public void continueToShoppingCoupon() throws Exception {
		cartPage.continueShoppingFromCouponBox();
	}

	@When("I request to continue with checkout to ignore coupon criteria")
	public void continueWithCheckoutCoupon() throws Exception {
		cartPage.continueWithCheckoutFromCouponBox();
	}

	@Then("I should be notice that I have not met qualifications for the selected coupons")
	public void iShouldSeeCouponsNotMetCriteria() throws Exception {
		Assert.assertTrue("--- Fail to see coupons error message!---", cartPage.isMessageDisplayed("Items in cart do not meet the criteria needed for the coupon to be applied"));
	}

	@When("I navigate on surprize page")
	public void iNavigateOnSurprizePage() throws Exception {
		if (homePage == null) {
			homePage = new HomePage();
		}
		surPrizePage = homePage.openSurPrizePage();
	}

	@When("I submit with coupon \"$coupon\" form surprize page")
	public void iSubmitWithCouponForSurprize(String coupon) throws Exception {
		surPrizePage.submitCoupon(coupon);
	}

	@Then("I should be request to reveal my surprize")
	public void iShouldNavigateOnRevealYourSurprizePage() throws Exception {
		Assert.assertTrue("--- Fail to submit for reveal!---", surPrizePage.isSurprizeItemDisplayed());
	}

	@Then("I should be noticed that entry is invalid")
	public void iShouldSeeErrorMessage() throws Exception {
		Assert.assertTrue("--- Fail to see error message!---", surPrizePage.isMessageDisplayed("Invalid entry"));
	}

	@When("I request to removed the coupon from cart")
	public void iRequestToRemovedTheCouponFromCart() throws Exception {
		cartPage.removeCoupon(0);
	}

	@When("I choose \"$option\" from the remove coupon confirm lighting box")
	public void iChooseFromTheRemoveCouponConfirmLightingBox(String option) throws Exception {
		cartPage.confirmRemoveCoupon(option);
	}

	@Then("I should see the added coupon has been moved")
	public void iShouldSeeTheAddedCouponHasBeenMoved() throws Exception {
		Assert.assertFalse("--- Failed to remove coupon! ---", cartPage.isCouponAdded(couponCode));
	}

	@Then("I should see the remove coupon confirm lighting box")
	public void iShouldSeeRemoveCouponLightingBox() throws Exception {
		Assert.assertTrue("--- Fail to see remove coupon confirm lighting box!---", cartPage.isLightingBoxDisplayed("Remove Coupon"));
	}
}
