package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class MergeAccountsOptionsPage extends BasePage {

	private WebElement skipButton;
	private WebElement mergeButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Combine My Accounts");
	}

	public void skipToMergeAccounts() {
		skipButton = findElementByCSS("#OMXUserNewAccountSignUpTrigger");
		clickOn(skipButton);
	}

	public MergeVerifyAccountPage mergeAccounts() {
		mergeButton = findElementByCSS(".button.b1.display_block");
		clickOn(mergeButton);
		return new MergeVerifyAccountPage();
	}

}
