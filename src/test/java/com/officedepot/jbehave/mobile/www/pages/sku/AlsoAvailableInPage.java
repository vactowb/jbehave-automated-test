package com.officedepot.jbehave.mobile.www.pages.sku;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class AlsoAvailableInPage extends BasePage {

	private WebElement addToCartButton;
	private WebElement breadcrumbLink;
	private WebElement skuIcon;
	private List<WebElement> availableSkuList;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Also Available In");
	}

	public void addToCart() {
		addToCartButton = findElementByName("cmd_addCart.button");
		clickOn(addToCartButton);
	}

	public void backToSkuDetailsFromBreadcrumb() {
		breadcrumbLink = findElementByLinkText("Product Details");
		clickOn(breadcrumbLink);
	}

	public void backToSkuDetailsFromIcon() {
		skuIcon = findElementByCSS("div.img_wrapper > a > img");
		clickOn(skuIcon);
	}

	public void viewAvailableSkuDetails(int index) {
		availableSkuList = findElementsByCSS(".option_link>a");
		clickOn(availableSkuList.get(index));
	}

}
