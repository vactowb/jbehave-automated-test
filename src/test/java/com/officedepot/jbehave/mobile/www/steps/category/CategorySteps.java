package com.officedepot.jbehave.mobile.www.steps.category;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.category.ChooseCategoryPage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CategorySteps extends BaseStep {

	private ChooseCategoryPage chooseCategoryPage;
	private PageHeader pageHeader;

	@When("I navigate to \"$category\" product list")
	public void searchItems(String category) throws Exception {
		pageHeader = new PageHeader();
		chooseCategoryPage = pageHeader.goToProducts();
		chooseCategoryPage.selectFromCategoryListByText(category);
	}

	@When("I request to find ink from header")
	public void findInkFromHeader() throws Exception {
		pageHeader = new PageHeader();
		chooseCategoryPage = pageHeader.findInk();
	}

	@Then("I should see the category list of \"$category\"")
	public void findRefineResults(String category) throws Exception {
		Assert.assertTrue("--- Failed to see excepted category! ---", chooseCategoryPage.isNavigateOnExceptedCategory(category));
	}

	@When("I add a sku through category list")
	public void checkItemDetails() throws Exception {
		chooseCategoryPage.selectFromCategoryList(1);
		chooseCategoryPage.selectFromCategoryList(0);
		chooseCategoryPage.addFromResultsList(0);
	}

	@When("I select the ink with following details: $infoTable")
	public void selectInkWithFollowingDetails(ExamplesTable infoTable) throws Exception {
		chooseCategoryPage.selectInk(infoTable);
	}

	@Then("I should see the ink finder results are about: \"$expr\"")
	public void getSkuList(String expr) throws Exception {
		Assert.assertTrue("--- Failed to get excepted ink finder results! ---", chooseCategoryPage.isFinderResultsExcepted(expr));
	}

	@When("I select the previous search: \"$pre\"")
	public void selectPrevousSearch(String pre) throws Exception {
		chooseCategoryPage.selectRecentSearch(pre);
	}
}