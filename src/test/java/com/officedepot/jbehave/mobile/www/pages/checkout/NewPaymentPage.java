package com.officedepot.jbehave.mobile.www.pages.checkout;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.PaymentInfoEnteringPage;

public class NewPaymentPage extends PaymentInfoEnteringPage {
	
	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "New Payment")
				&& isElementPresentBySelenium("css=.f_left.step3.active");
	}
	
	@Override
	public WebElement getPaymentInfoField(String field) { 	
		if(field.equalsIgnoreCase("nick name")) {
			return findElementById("creditCardAlias");
		}
		if(field.equalsIgnoreCase("holder name")) {
			return findElementById("creditCardHolderName");		
		}
		if(field.equalsIgnoreCase("card type")) {
			return findElementById("cardTypeId");
		}
		if(field.equalsIgnoreCase("card number")) {
			return findElementById("creditCardNumber");
		}
		if(field.equalsIgnoreCase("month")) {
			return findElementById("monthId");
		}
		if(field.equalsIgnoreCase("year")) {
			return findElementById("yearId");
		}
		if(field.equalsIgnoreCase("address")) {
			return findElementById("address1-0");
		}
		if(field.equalsIgnoreCase("city")) {
			return findElementById("city-0");
		}
		if(field.equalsIgnoreCase("state")) {
			return findElementById("state-0");
		}
		if(field.equalsIgnoreCase("post code")) {
			return findElementById("postalCode1-0");
		}
		return null;
    }
	
}
