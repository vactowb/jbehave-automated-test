package com.officedepot.jbehave.mobile.www.pages.login;

public class SocialLogInPageProvider {

	public static SocialAccountLogInPage getSocialLogInPage(String type) throws Exception {
		if (type.equalsIgnoreCase("google+")) {
			return new GmailAccountLoginPage();
		} else if (type.equalsIgnoreCase("linkedin")) {
			return new LinkedinAccountLogInPage();
		} else if (type.equalsIgnoreCase("facebook")) {
			return new FaceBookAccountLogInPage();
		} else {
			throw new Exception("Unavailable type of social log in!!!");
		}
	}
}
