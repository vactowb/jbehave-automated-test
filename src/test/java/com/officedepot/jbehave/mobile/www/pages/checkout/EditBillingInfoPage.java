package com.officedepot.jbehave.mobile.www.pages.checkout;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class EditBillingInfoPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement updateButton;
	private WebElement cancelButton;
	private WebElement billingInfoField;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Edit Billing Information") && isElementPresentBySelenium("css=.f_left.step4.active");
	}

	public void updateBillingInfo() {
		updateButton = findElementByName("cmd_edit");
		clickOn(updateButton);
	}

	public void cancelEditBillingInfo() {
		cancelButton = findElementByCSS("a.button.f_left");
		clickOn(cancelButton);
	}

	public void fillBillingInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputBillingInfoField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputBillingInfoField(String field, String value) {
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		if (field.equalsIgnoreCase("first name")) {
			billingInfoField = findElementById("firstName-0");
		}
		if (field.equalsIgnoreCase("last name")) {
			billingInfoField = findElementById("lastName-0");
		}
		if (field.equalsIgnoreCase("address 1")) {
			billingInfoField = findElementById("address1-0");
		}
		if (field.equalsIgnoreCase("city")) {
			billingInfoField = findElementById("city-0");
		}
		if (field.equalsIgnoreCase("state")) {
			billingInfoField = findElementById("state-0");
			selectOptionInListByText(billingInfoField, value);
			return;
		}
		if (field.equalsIgnoreCase("post code")) {
			billingInfoField = findElementById("postalCode1-0");
		}
		if (field.equalsIgnoreCase("phone number")) {
			billingInfoField = findElementById("phoneNumber1-0");
		}
		if (field.equalsIgnoreCase("email")) {
			value = SeleniumUtils.giveUniqueIdBefore(value);
			billingInfoField = findElementById("email-0");
			typeTextBox(billingInfoField, value);
			billingInfoField = findElementById("emailConfirm-0");
			typeTextBox(billingInfoField, value);
			return;
		}
		typeTextBox(billingInfoField, value);
	}

}
