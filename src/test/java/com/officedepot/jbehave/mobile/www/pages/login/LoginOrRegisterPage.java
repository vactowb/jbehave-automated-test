package com.officedepot.jbehave.mobile.www.pages.login;

import java.util.Iterator;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;
import com.officedepot.test.jbehave.BasePage;

public class LoginOrRegisterPage extends BasePage {
	private WebElement loginButton;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement socialButton;
	private WebElement forgotLoginOrPassLink;
	private WebElement registerLink;

	public void createAccount() {
		loginButton = findElementByCSS("a.no_decoration.f_right");
		clickOn(loginButton);
	}

	public void loginWithAccount(String username, String password) {
		usernameField = findElementByName("loginName");
		typeTextBox(usernameField, username);
		passwordField = findElementByName("password");
		typeTextBox(passwordField, password);
		loginButton = findElementByCSS("input.button.b1a");
		clickOn(loginButton);
	}

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentOnPage("Login or Register");
	}

	public boolean isGotTheErrorMessageForInvalidLoginName() throws Exception {
		return isTextPresentInElement(By.cssSelector(".errorList>li"), "Login Name or Password is invalid");
	}

	public boolean isGotTheErrorMessageForEmptyLoginName() throws Exception {
		return isAlertPrecent("Login is a required field.");
	}

	public SocialAccountLogInPage socialLogIn(ExamplesTable accountDetails) throws Exception {
		String type = accountDetails.getRow(0).get("Type");
		if (type.equalsIgnoreCase("facebook")) {
			socialButton = findElementByCSS("img");
		} else {
			socialButton = findElementByCSS("div[alt='" + type + "'] > div > div");
		}
		clickOn(socialButton);
		focusOnSocialLoginWindow();
		SocialAccountLogInPage socialLogInPage = SocialLogInPageProvider.getSocialLogInPage(type);
		socialLogInPage.logIn(accountDetails.getRow(0).get("Username"), accountDetails.getRow(0).get("Password"));
		return socialLogInPage;
	}

	private void focusOnSocialLoginWindow() {
		Iterator<String> it = webDriver.getWindowHandles().iterator();
		while (it.hasNext()) {
			it.next();
			String popWindow = it.next();
			webDriver.switchTo().window(popWindow);
		}
	}

	public ForgotLoginOrPasswordPage forgotLoginOrPass() {
		forgotLoginOrPassLink = findElementByCSS("a.touch-safe.display_inline_block");
		clickOn(forgotLoginOrPassLink);
		return new ForgotLoginOrPasswordPage();
	}

	public RegisterPage clickOnRegisterLink() {
		registerLink = findElementByLinkText("Register");
		clickOn(registerLink);
		return new RegisterPage();
	}

}
