package com.officedepot.jbehave.mobile.www.pages.deals;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class WeeklyAdsPage extends BasePage {

	private WebElement weeklyAdItems;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Weekly Ads");
	}

	public WeeklyAdPagesPage viewWeeklyAd(int index) {
		weeklyAdItems = findElementsByCSS(".display_block.arrow_primary_right").get(index);
		clickOn(weeklyAdItems);
		return new WeeklyAdPagesPage();
	}

}
