package com.officedepot.jbehave.mobile.www.pages.odr;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class MaxperksRedirectPage extends BasePage {

	private WebElement memberInfoField;
	private WebElement activeButton;
	private WebElement phoneNumberField;
	private WebElement lookupButton;
	private WebElement joinButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Get started with your transition");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void fillPhoneNumber(String phone) {
		phoneNumberField = findElementById("phone");
		typeTextBox(phoneNumberField, phone);
	}

	public void lookup() {
		lookupButton = this.findElementById("jst_phoneLookup");
		clickOn(lookupButton);
	}

	public boolean isLookupButtonAvailable() {
		return !isElementPresentBySelenium("css=.button flo_right b1_disabled");
	}

	public void fillMaxperksMemberInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputMemeberInfo(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputMemeberInfo(String field, String value) {
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		if (field.equalsIgnoreCase("account")) {
			memberInfoField = findElementById("mpMemberNumber");
		}
		if (field.equalsIgnoreCase("password")) {
			memberInfoField = findElementById("mpPassword");
		}
		typeTextBox(memberInfoField, value);
	}

	public void active() {
		activeButton = this.findElementByCSS(".flo_right.button.b1");
		clickOn(activeButton);
	}

	public ODLoginPage join() {
		joinButton = findElementByLinkText("JOIN NOW");
		clickOn(joinButton);
		return new ODLoginPage();
	}

	public String getFilledPhoneNumber() {
		phoneNumberField = findElementById("phone");
		return getValue(phoneNumberField);
	}

}
