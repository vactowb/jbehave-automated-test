package com.officedepot.jbehave.mobile.www.pages.deals;

import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.category.ChooseCategoryPage;
import com.officedepot.jbehave.mobile.www.pages.store.StoreLocatorPage;
import com.officedepot.test.jbehave.BasePage;

public class DealsPage extends BasePage {

	private String pageVerifyItem = "css=.arrow_primary_right";
	private WebElement dealCenterSection;
	private WebElement weeklyAdsSection;
	private WebElement changeStoreLink;

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium(pageVerifyItem);
	}

	public ChooseCategoryPage goToDealCenter() {
		dealCenterSection = findElementsByCSS(".arrow_primary_right.see_more").get(0);
		clickOn(dealCenterSection);
		return new ChooseCategoryPage();
	}

	public WeeklyAdsPage goToWeeklyAds() {
		weeklyAdsSection = findElementsByCSS(".arrow_primary_right.see_more").get(2);
		clickOn(weeklyAdsSection);
		return new WeeklyAdsPage();
	}

	public StoreLocatorPage changeMyStore() {
		changeStoreLink = findElementByCSS(".arrow_primary_right.change");
		clickOn(changeStoreLink);
		return new StoreLocatorPage();
	}

}
