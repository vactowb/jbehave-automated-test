package com.officedepot.jbehave.mobile.www.pages.others;

import com.officedepot.test.jbehave.BasePage;

public class TermsPage extends BasePage {
	
	private String pageVerifyItem = "css=.faded";
	
	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium(pageVerifyItem);
	}
}
