package com.officedepot.jbehave.mobile.www;

import com.officedepot.test.jbehave.BaseRunner;

public abstract class MobileWWWBaseRunner extends BaseRunner {
	
	protected String[] getStepsBasePackages() {
		return new String[]{"com.officedepot.jbehave.mobile.www.steps"};
	}
	
}
