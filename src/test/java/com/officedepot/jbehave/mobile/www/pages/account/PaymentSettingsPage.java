package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class PaymentSettingsPage extends BasePage {

	protected WebElement addButton;
	protected List<WebElement> editButtons;

	public PaymentInfoEnteringPage enterNewCard() throws Exception {
		addButton = findElementByCSS(".button.b1.display_block");
		clickOn(addButton);
		seleniumWaitForPageToLoad(30000);
		return new PaymentInfoEnteringPage();
	}

	public EditPaymentInfoPage editDefaultCard() throws Exception {
		editButtons = findElementsByCSS(".flo_right.arrowRight.vspace_top");
		clickOn(editButtons.get(0));
		return new EditPaymentInfoPage();
	}

	public boolean isCreditCardExist() throws Exception {
		return isElementPresentBySelenium("css=.flo_right.arrowRight.vspace_top");
	}

	public boolean isDeleteCardSuccessful() throws Exception {
		return isTextPresentOnPage("Credit card was deleted successfully.");
	}

}
