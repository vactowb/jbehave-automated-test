package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class VerifyAccountPage extends BasePage {

	private WebElement submitButton;
	private WebElement cancelButton;
	private WebElement optionRadioButton;

	public boolean isNaviageOnThisPage() {
		return isTextPresentOnPage("Verify Account");
	}

	public void cancel() {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
		seleniumWaitForPageToLoad(30000);
	}

	public void submit() {
		submitButton = findElementByName("submit");
		clickOn(submitButton);
	}

	public boolean verifyOptions(String type) {
		if (type.equals("all")) {
			return findElementsByCSS(".span2>label").size() == 3;
		} else if (type.equals("email")) {
			return findElementsByCSS(".span2>label").size() == 1;
		} else if (type.equals("phone")) {
			return findElementsByCSS(".span2>label").size() == 2;
		} else {
			return false;
		}
	}

	public void seleceOption(String option) {
		optionRadioButton = findElementById("delMethod" + option);
		clickOn(optionRadioButton);
	}

}
