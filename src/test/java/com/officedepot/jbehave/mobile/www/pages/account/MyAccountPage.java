package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.csl.ShoppingListPage;
import com.officedepot.test.jbehave.BasePage;

public class MyAccountPage extends BasePage {

	private WebElement shoppingAddressButton;
	private WebElement shippingInfo;
	private WebElement billingInfo;
	private WebElement contactInfo;
	private WebElement paymentOptionsButton;
	private WebElement accountHomeButton;
	private WebElement yesMarketingOpt;
	private WebElement myProfileButton;
	private WebElement myStoreButton;
	private WebElement trackOrdersButton;
	private WebElement myRewardsButton;
	private WebElement shoppingListsButton;
	private By pageVerifieItem = By.cssSelector(".page_title");

	public void goToAccountHome() throws Exception {
		accountHomeButton = findElementById("acctHome");
		clickOn(accountHomeButton);
	}

	public MyProfilePage goToMyProfile() {
		myProfileButton = findElementByCSS("a.acct_icon_myprofile");
		clickOn(myProfileButton);
		seleniumWaitForPageToLoad(5000);
		return new MyProfilePage();
	}

	public PaymentSettingsPage selectPaymentInfo() throws Exception {
		paymentOptionsButton = findElementByCSS("a.acct_icon_payment");
		clickOn(paymentOptionsButton);
		seleniumWaitForPageToLoad(5000);
		return new PaymentSettingsPage();
	}

	public boolean findAddedAlias(String alias) throws Exception {
		return isTextPresentOnPage(alias);
	}

	public boolean isNavigateOnThisPage() throws Exception {
		return isTextPresentInElement(pageVerifieItem, "My Account");
	}

	public ShippingOptionsPage editShoppingInfo() throws Exception {
		shoppingAddressButton = findElementByCSS("a.acct_icon_shipping");
		clickOn(shoppingAddressButton);
		seleniumWaitForPageToLoad(30000);
		return new ShippingOptionsPage();
	}

	public boolean isShippingInfoExcepted(ExamplesTable exceptedInfo) throws Exception {
		for (Map<String, String> infoRow : exceptedInfo.getRows()) {
			if (getShippingInfo(infoRow.get("Field")).indexOf(infoRow.get("Value").toUpperCase()) == -1) {
				return false;
			}
		}
		return true;
	}

	public String getShippingInfo(String field) throws Exception {
		if (field.equalsIgnoreCase("first name") || field.equalsIgnoreCase("last name") || field.equalsIgnoreCase("name")) {
			shippingInfo = findElementByXpath("//div[@id='mainContent']/section[1]/div/div/ol/li[4]");
		}
		return getText(shippingInfo);
	}

	public boolean isBillingInfoExcepted(ExamplesTable exceptedInfo) throws Exception {
		for (Map<String, String> infoRow : exceptedInfo.getRows()) {
			if (getBillingInfo(infoRow.get("Field")).indexOf(infoRow.get("Value").toUpperCase()) == -1) {
				return false;
			}
		}
		return true;
	}

	public String getBillingInfo(String field) throws Exception {
		if (field.equalsIgnoreCase("company name")) {
			billingInfo = findElementByXpath("//div[@id='mainContent']/section[2]/div/div/ol/li[1]");
		}
		return getText(billingInfo);
	}

	public boolean isContactInfoExcepted(ExamplesTable exceptedInfo) throws Exception {
		for (Map<String, String> infoRow : exceptedInfo.getRows()) {
			if (getContactInfo(infoRow.get("Field")).indexOf(infoRow.get("Value").toUpperCase()) == -1) {
				return false;
			}
		}
		return true;
	}

	public String getContactInfo(String field) throws Exception {
		if (field.equalsIgnoreCase("first name") || field.equalsIgnoreCase("last name")) {
			contactInfo = findElementByXpath("//div[@id='mainContent']/section[3]/div/div/ol/li[1]");
		}
		return getText(contactInfo);
	}

	public void selectMarketingOpt() throws Exception {
		yesMarketingOpt = findElementsByCSS(".f_right>input").get(0);
		clickOn(yesMarketingOpt);
	}

	public boolean isChangePasswordSuccessfulMessageOccur() throws Exception {
		return isTextPresentOnPage("Your password has been successfully updated");
	}

	public void clickNoneSalesRepCheckbox() {
		WebElement SalesRepCheckBox = findElementBy(By.id("communicationModulePermissionCheckBox"));
		if (SalesRepCheckBox.isSelected()) {
			clickOn(SalesRepCheckBox);
		}
	}

	public void viewMyStore() throws Exception {
		myStoreButton = findElementByCSS("a.acct_icon_mystore");
		clickOn(myStoreButton);
	}

	public ManageOrdersPage trackOrders() {
		trackOrdersButton = findElementByCSS("a.acct_icon_orders");
		clickOn(trackOrdersButton);
		return new ManageOrdersPage();
	}

	public void viewMyRewards() {
		myRewardsButton = findElementByCSS("a.acct_icon_loyalty");
		clickOn(myRewardsButton);
	}

	public ShoppingListPage viewMyShoppingListPage() throws Exception {
		shoppingListsButton = findElementByCSS("a.acct_icon_lists");
		clickOn(shoppingListsButton);
		return new ShoppingListPage();
	}

}
