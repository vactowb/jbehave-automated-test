package com.officedepot.jbehave.mobile.www.steps.register;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.login.LoginOrRegisterPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialAccountLogInPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialLinkAccountPage;
import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;
import com.officedepot.jbehave.mobile.www.pages.register.SocialRegistrationPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class RegisterSteps extends BaseStep {

//	private EComContext ecomSiteType = EComContext.OD_EN_US_BUSINESS;
//	private String expressRegisterFlag = ConfigurationService.lookup("/od/config/feature/mobile/express/registration/enabled", "true", ecomSiteType);

	private RegisterPage registerPage;
	private LoginOrRegisterPage loginPage;
	private PageHeader pageHeader;
	private HomePage homePage;
	private SocialLinkAccountPage socialLinkAccountPage;
	private SocialAccountLogInPage socialAccountLogInPage;
	private SocialRegistrationPage socialRegistrationPage;

	@When("I request to create new account from login page")
	public void iClickLoginIcon() {
		if (loginPage == null) {
			loginPage = new LoginOrRegisterPage();
		}
		loginPage.createAccount();
	}

	@Then("I should navigate on registration page")
	public void iSeeRegisterPage() throws Exception {
		registerPage = new RegisterPage();
		assertTrue("--- Failed to navigate on register page! ---", registerPage.isNavigateOnThisPage());
	}

	@When("I register with the following info: $infoTable")
	public void registerWithFollowingInfo(ExamplesTable infoTable) throws Exception {
		registerPage.fillRegistrationInfo(infoTable);
		registerPage.submitToCreatAccount();
	}

	@When("I logout of m-site")
	public void ilogout() throws Exception {
		pageHeader.logout();
	}

	@Then("I should be noticed that zip code is invalid")
	public void iSeeZipCodeError() throws Exception {
		assertTrue(registerPage.isMessageDisplayed("Zip Code is invalid"));
	}

	@When("I register with existed account name")
	public void inputUsedAccount() {
		loginPage = pageHeader.enterLoginOrRegisterPage();
		loginPage.createAccount();
	}

	@Then("I should be noticed that email already in used")
	public void iSeeAccountExisted() throws Exception {
		assertTrue("--- Failed to see error message! ---", registerPage.isMessageDisplayed("This email address is already in use."));
	}

	@Then("I should be noticed that I need to set security question")
	public void iShouldBeNoticedThatINeedToSetSecurityQuestion() throws Exception {
//		if (expressRegisterFlag.equals("true")) {
			assertTrue(registerPage.isMessageDisplayed("Please select a security question"));
//		}
	}

	@When("I request to register from login page")
	public void iEntryExpressRegisterPageFromHomePage() throws Exception {
		if (loginPage == null) {
			loginPage = new LoginOrRegisterPage();
		}
		registerPage = loginPage.clickOnRegisterLink();
	}

	@When("I login by the given social account from registration page")
	public void iLoginByTheGivenSocialAccountFromRegiter(@Named("type") String type, @Named("username") String username, @Named("password") String password) throws Exception {
		socialAccountLogInPage = registerPage.socialLogin(type, username, password);
	}

	@When("I login by the given social account from login register section")
	public void iLoginByTheGivenSocialAccountFromRegiterSection(@Named("type") String type, @Named("username") String username, @Named("password") String password) throws Exception {
		homePage = new HomePage();
		socialAccountLogInPage = homePage.socialLogin(type, username, password);
	}

	@Then("I should login successfully by social account")
	public void iShouldSocialLoginSuccessfully() throws Exception {
		Assert.assertTrue("---Failed to login with social account!---", socialAccountLogInPage.isSuccessForLogIn());
	}

	@When("I close the social login window via registration")
	public void iCloseTheSocialLoginWindow() throws Exception {
		socialAccountLogInPage.closeWindow();
	}

	@Then("I should see link account page")
	public void iShouldSeeLinkAccountPage() throws Exception {
		socialLinkAccountPage = new SocialLinkAccountPage();
		Assert.assertTrue("--- Failed to navigate to link account page! ---", socialLinkAccountPage.isNavigateOnThisPage());
	}

	@When("I choose to register with social account")
	public void iChooseToRegisterWithSocialAccount() throws Exception {
		socialRegistrationPage = socialLinkAccountPage.registerWithSocial();
	}

	@Then("I should see social registration page")
	public void iShouldSeeSocialRegistrationPage() throws Exception {
		Assert.assertTrue("--- Failed to navigate to social registration page! ---", socialRegistrationPage.isNavigateOnThisPage());
	}

	@When("I register with social by following info: $infoTable")
	public void registerWithSocialByFollowingInfo(ExamplesTable infoTable) throws Exception {
		socialRegistrationPage.fillRegistrationInfo(infoTable);
		socialRegistrationPage.submitToCreatAccount();
	}

	@Then("I should be noticed that zip code is invalid via registration")
	public void iSeeZipCodeErrorViaRegister() throws Exception {
		assertTrue(socialRegistrationPage.isMessageDisplayed("Zip Code is invalid"));
	}

	@Then("I should be noticed that email already in used via registration")
	public void iSeeAccountExistedViaRegister() throws Exception {
		assertTrue(socialRegistrationPage.isMessageDisplayed("has already been used"));
	}

	@Then("I should not see the ODR member number field section on page")
	public void iShouldNotSeeODRSection() throws Exception {
		assertFalse("--- ODR section is on page! ---", registerPage.isMemberNumberFieldDisplayed());
	}
}
