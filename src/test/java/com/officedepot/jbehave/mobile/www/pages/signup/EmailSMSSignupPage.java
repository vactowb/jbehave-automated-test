package com.officedepot.jbehave.mobile.www.pages.signup;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class EmailSMSSignupPage extends BasePage {

	private WebElement signUpButton;
	private WebElement infoField;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Sign up for special offers");
	}

	public boolean isErrorMessageDisplayed(String msg) {
		return isTextPresentOnPage(msg);
	}

	public void fillSignUpInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputSignUpInfoField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputSignUpInfoField(String field, String value) {
		if (field.equalsIgnoreCase("subscribe to")) {
			infoField = findElementById("typeId");
			selectOptionInListByText(infoField, value);
		}
		if (field.equalsIgnoreCase("email")) {
			infoField = findElementsByCSS("#alertsEmail").get(0);
			typeTextBox(infoField, value);
			infoField = findElementsByCSS("#alertsEmail").get(1);
			typeTextBox(infoField, value);
		}
		if (field.equalsIgnoreCase("zip code")) {
			infoField = findElementByName("alertsAddressForm.postalCode1");
			typeTextBox(infoField, value);
		}
		if (field.equalsIgnoreCase("phone number")) {
			fillPhoneNumber(value);
		}
	}

	public void fillMandatoryFields(ExamplesTable data) throws Exception {
		infoField = findElementById("alertsEmail");
		typeTextBox(infoField, data.getRow(0).get("Email"));
		infoField = findElementByName("alertsAddressForm.firstName");
		typeTextBox(infoField, data.getRow(0).get("First Name"));
	}

	public String getFilledEmail() {
		infoField = findElementById("alertsEmail");
		return getValue(infoField);
	}

	public void submitToSignUp() throws Exception {
		signUpButton = findElementByCSS(".button.b1.fullwidth");
		clickOn(signUpButton);
	}

	public boolean successSignUp() throws Exception {
		return isTextPresentOnPage("Request Submitted");
	}

	public void fillPhoneNumber(String number) {
		String splittedNumber[] = number.split("-");
		infoField = findElementById("phoneNumber1-0");
		typeTextBox(infoField, splittedNumber[0]);
		infoField = findElementById("phoneNumber2-0");
		typeTextBox(infoField, splittedNumber[1]);
		infoField = findElementById("phoneNumber3-0");
		typeTextBox(infoField, splittedNumber[2]);
	}

	public boolean isPhoneNumberFilled(String number) {
		StringBuffer sb = new StringBuffer();
		infoField = findElementById("phoneNumber1-0");
		sb.append(getValue(infoField));
		infoField = findElementById("phoneNumber2-0");
		sb.append(getValue(infoField));
		infoField = findElementById("phoneNumber3-0");
		sb.append(getValue(infoField));
		return number.equals(sb.toString());
	}

}
