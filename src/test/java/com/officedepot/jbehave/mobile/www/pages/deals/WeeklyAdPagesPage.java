package com.officedepot.jbehave.mobile.www.pages.deals;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class WeeklyAdPagesPage extends BasePage {

	private WebElement viewListOfItemsButton;

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium("css=.line_around.vspace_top");
	}

	public ListViewPage viewListOfItemsOnPage() {
		viewListOfItemsButton = findElementByLinkText("View List Of Items On Page");
		clickOn(viewListOfItemsButton);
		return new ListViewPage();
	}

}
