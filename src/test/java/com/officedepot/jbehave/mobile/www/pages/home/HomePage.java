package com.officedepot.jbehave.mobile.www.pages.home;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.account.MyAccountPage;
import com.officedepot.jbehave.mobile.www.pages.coupon.SurprizePage;
import com.officedepot.jbehave.mobile.www.pages.deals.DealsPage;
import com.officedepot.jbehave.mobile.www.pages.login.LoginOrRegisterPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialAccountLogInPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialLogInPageProvider;
import com.officedepot.jbehave.mobile.www.pages.odr.MyRewardsPage;
import com.officedepot.jbehave.mobile.www.pages.odr.RewardsLandingPage;
import com.officedepot.jbehave.mobile.www.pages.store.StoreLocatorPage;
import com.officedepot.jbehave.mobile.www.pages.survey.SurveyPage;
import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumTestContext;

public class HomePage extends BasePage {

	private String pageVerifyItem = "css=#br_products";
	private WebElement loginAndRegisterSection;
	private WebElement myAccountSetion;
	private WebElement socialLoginButton;
	private WebElement findStoreSection;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement loginButton;
	private WebElement odrSection;
	private WebElement dealsSection;

	public void openHomePage() throws Exception {
		String currentURL = SeleniumTestContext.getInstance().getTargetBaseURL();
		if (currentURL.indexOf("wwwbeta") != -1 || currentURL.indexOf("sq") != -1) {
			openHomePageViaGetCookie();
		} else {
			openUrl("/");
		}
		logout();
		// setCookie("MOBILE_CHECKOUT_V2", "true");
		// setCookie("MOBILE_SKU_LIST_V2", "true");
		Assert.assertTrue("---Failed to navigate on OD home page!---", isNavigateOnThisPage());
	}

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium(pageVerifyItem);
	}

	public void openHomePageViaGetCookie() throws Exception {
		openUrl("/iphone/");
		seleniumWaitForPageToLoad(10000);
		clickOn(findElementByLinkText("Click for Access Authorization"));
	}

	public void openHomePageByDefault() {
		openUrl("/");
	}

	public void logout() throws Exception {
		openUrl("/mb/logout.do");
	}

	public LoginOrRegisterPage loginRegister() {
		loginAndRegisterSection = findElementById("jst_myaccount");
		clickOn(loginAndRegisterSection);
		return new LoginOrRegisterPage();
	}

	public MyAccountPage goToMyAccount() {
		myAccountSetion = findElementByXpath("//ul[@id='homePanels']/li[4]/a/span[2]/strong");
		clickOn(myAccountSetion);
		return new MyAccountPage();
	}

	public SocialAccountLogInPage socialLogin(String type, String username, String password) throws Exception {
		loginRegister();
		socialLoginButton = findElementByCSS("div[alt='" + type + "'] > div > div");
		clickOn(socialLoginButton);
		focusOnNewWindow();
		SocialAccountLogInPage socialLogInPage = SocialLogInPageProvider.getSocialLogInPage(type);
		socialLogInPage.logIn(username, password);
		return socialLogInPage;
	}

	public SurveyPage openSurveyPage() {
		openUrl("/mb/survey/display.do");
		redirectFrame(findElementById("survey"));
		return new SurveyPage();
	}

	public StoreLocatorPage findStore() {
		findStoreSection = findElementById("find_store");
		clickOn(findStoreSection);
		return new StoreLocatorPage();
	}

	public SurprizePage openSurPrizePage() {
		openUrl("/a/promo/mysterycoupon/mysterycoupon/");
		return new SurprizePage();
	}

	public void login(String username, String password) {
		loginRegister();
		usernameField = findElementByName("loginName");
		typeTextBox(usernameField, username);
		passwordField = findElementByName("password");
		typeTextBox(passwordField, password);
		loginButton = findElementByCSS("input.b6.blue");
		clickOn(loginButton);
	}

	public MyRewardsPage viewMyRewards() {
		odrSection = findElementByXpath("//ul[@id='homePanels']/li[6]/a");
		clickOn(odrSection);
		return new MyRewardsPage();
	}

	public RewardsLandingPage joinOrActiveMyRewards() {
		odrSection = findElementByXpath("//ul[@id='homePanels']/li[6]/a");
		clickOn(odrSection);
		return new RewardsLandingPage();
	}

	public DealsPage viewDeals() {
		dealsSection = findElementByXpath("//ul[@id='homePanels']/li[2]/a");
		clickOn(dealsSection);
		return new DealsPage();
	}

}
