package com.officedepot.jbehave.mobile.www.pages.odr;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class CreateMemberIDPage extends BasePage {

	private WebElement contactInfoField;
	private WebElement cancelButton;
	private WebElement continueButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Join Now") && isTextPresentOnPage("Membership Type");
	}

	public boolean isFieldPrefilled(String field) {
		if (field.equalsIgnoreCase("phone number")) {
			contactInfoField = findElementById("phone");
		}
		return !getValue(contactInfoField).isEmpty();
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void continueToLink() {
		continueButton = findElementByCSS(".button.b1.f_right");
		clickOn(continueButton);
	}

	public void cancel() {
		cancelButton = findElementByCSS(".button.f_left");
		clickOn(cancelButton);
	}

	public void fillContactInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputRegisterField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputRegisterField(String field, String value) {
		if (value.indexOf("RANDOM") != -1) {
			value = SeleniumUtils.genRandomStringByTableParam(value);
		}
		if (value.indexOf("RANNUM") != -1) {
			value = SeleniumUtils.genRandomNumberByTalbeParam(value);
		}
		if (field.equalsIgnoreCase("membership type")) {
			contactInfoField = findElementById("memberType");
			selectOptionInListByText(contactInfoField, value);
			return;
		}
		if (field.equalsIgnoreCase("phone number")) {
			contactInfoField = findElementById("phone");
		}
		typeTextBox(contactInfoField, value);
	}

}
