package com.officedepot.jbehave.mobile.www.pages.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class OrderDetailsPage extends BasePage {

	private WebElement reorderButton;
	private WebElement cancelButton;
	private WebElement yesCancelButton;
	private WebElement noCancelButton;
	private WebElement closeCancelBoxButton;
	private WebElement manageOrdersBreadCrumbLink;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(By.cssSelector(".page_title"), "Order Details");
	}

	public boolean isOrderExpected(String order) {
		return isTextPresentInElement(By.cssSelector(".sub_title_4>strong"), order);
	}

	public void cancelOrder() {
		cancelButton = findElementByCSS(".script.jst_cancel_order.button.f_left");
		clickOn(cancelButton);
	}

	public void reorder() {
		reorderButton = findElementByCSS(".button.b1.f_right");
		clickOn(reorderButton);
	}

	public boolean isCancelOrderBoxDisplayed() {
		return isElementVisibleBySelenium("css=#boxesTopContent");
	}

	public void confirmCancelOrder() {
		yesCancelButton = findElementByXpath("(//input[@value='Yes'])[2]");
		clickOn(yesCancelButton);
	}

	public void rejectCancelOrder() {
		noCancelButton = findElementByCSS("#boxes_content > div.t_left > div.otd_yes_no > input.jst_cancel_order_no.button");
		clickOn(noCancelButton);
	}

	public String getOrderStatus() {
		return getText(findElementByCSS("#jst_status"));
	}

	public boolean isOrderCancelledSuccessfully() {
		return isTextPresentInElement(By.cssSelector("#boxes_content>div"), "Order Cancelled");
	}

	public boolean isOrderCanBeCancelled() {
		return isElementPresentBySelenium("css=.script.jst_cancel_order.button.f_left");
	}

	public void closeCancelOrderBox() {
		closeCancelBoxButton = findElementByCSS("#boxes_close");
		clickOn(closeCancelBoxButton);
	}

	public void goToManageOrdersFromBreadCrumb() {
		manageOrdersBreadCrumbLink = findElementByLinkText("Manage Orders");
		clickOn(manageOrdersBreadCrumbLink);
	}

}
