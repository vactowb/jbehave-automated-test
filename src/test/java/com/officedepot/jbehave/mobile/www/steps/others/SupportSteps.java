package com.officedepot.jbehave.mobile.www.steps.others;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.others.ContactUsPage;
import com.officedepot.jbehave.mobile.www.pages.others.PrivacyPolicyPage;
import com.officedepot.jbehave.mobile.www.pages.others.TermsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class SupportSteps extends BaseStep {

	private PageHeader pageHeader;
	private HomePage homePage;
	private TermsPage termsPage;
	private ContactUsPage contactUsPage;
	private PrivacyPolicyPage privacyPolicyPage;

	@Then("I should see home page")
	public void checkIfInHomePage() throws Exception {
		assertTrue(homePage.isNavigateOnThisPage());
	}

	@When("I request to access terms from header")
	public void iNavigateToTermsPage() throws Exception {
		pageHeader = new PageHeader();
		termsPage = pageHeader.clickOnTermsLinkToTermsPage();
	}

	@Then("I should navigate on terms page")
	public void checkIfInTermsPage() throws Exception {
		if (termsPage == null) {
			termsPage = new TermsPage();
		}
		assertTrue("--- Failed to navigate on terms page! ---", termsPage.isNavigateOnThisPage());
	}

	@When("I request to access privacy policy from header")
	public void iNavigateToPrivacyPage() throws Exception {
		pageHeader = new PageHeader();
		privacyPolicyPage = pageHeader.goToPrivacy();
	}

	@When("I request to access contact us from header")
	public void iNavigateToContactUsPage() throws Exception {
		pageHeader = new PageHeader();
		contactUsPage = pageHeader.goToContactUsPage();
	}

	@Then("I should navigate on contact us page")
	public void checkIfInContactUsPage() throws Exception {
		assertTrue(contactUsPage.isNavigateOnThisPage());
	}

	@When("I send email without input")
	public void iSendEmailWithoutInput() throws Exception {
		contactUsPage.clickOnSendEmailButton();
	}

	@Then("I should see attention message")
	public void iShouldSeeAttentionMessage() throws Exception {
		assertTrue(contactUsPage.isMessageDisplayed("Name is required"));
		assertTrue(contactUsPage.isMessageDisplayed("Email Address is required"));
		assertTrue(contactUsPage.isMessageDisplayed("Please select a subject"));
		assertTrue(contactUsPage.isMessageDisplayed("Text message is required"));

	}

	@When("I send email with input the full items")
	public void iSendEmailWithInputTheFullItems() throws Exception {
		contactUsPage.inputTheFullItems("Test Name", "testEmail@163.com", "12312312", "1111", "2222", "Here is your message!");
		contactUsPage.clickOnSendEmailButton();
	}

	@Then("I should see contact us successful message")
	public void iShouldSeeContactUsSuccessfulMessage() throws Exception {
		assertTrue(contactUsPage.isMessageDisplayed("Thank you for shopping Office Depot Online"));
	}
}
