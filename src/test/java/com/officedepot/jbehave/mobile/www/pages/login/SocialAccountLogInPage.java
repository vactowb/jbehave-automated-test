package com.officedepot.jbehave.mobile.www.pages.login;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public abstract class SocialAccountLogInPage extends BasePage {

	protected WebElement closeWindowButton;

	public abstract void logIn(String accountName, String pwd);

	public boolean isSuccessForLogIn() {
		return isElementPresentBySelenium("id=btnClose");
	}

	public void closeWindow() throws Exception {
		closeWindowButton = findElementById("btnClose");
		clickOn(closeWindowButton);
		focusOnNewWindow();
	}

	public boolean dispalyErrorMessage() {
		return isTextPresentOnPage("The email or password you entered is incorrect.");
	}
}
