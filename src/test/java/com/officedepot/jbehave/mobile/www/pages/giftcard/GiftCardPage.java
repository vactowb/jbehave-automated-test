package com.officedepot.jbehave.mobile.www.pages.giftcard;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.checkout.ReviewOrderPage;
import com.officedepot.test.jbehave.BasePage;

public class GiftCardPage extends BasePage {

	private By pageVerifyItem = By.cssSelector(".page_title");
	private WebElement applyButton;
	private WebElement giftCardNumberField;
	private WebElement giftCardPinField;
	private WebElement cancelButton;
	
	public boolean isInCurrentPage() throws Exception {
		return isTextPresentInElement(pageVerifyItem, "Enter a Gift Card");
	}
	
	public void fillGiftCardInformation(ExamplesTable cardInfo) {
		giftCardNumberField = findElementByName("storedValueCardNumber");
		typeTextBox(giftCardNumberField, cardInfo.getRow(0).get("Card Number"));
		giftCardPinField = findElementByName("storedValueCardPin");
		typeTextBox(giftCardPinField, cardInfo.getRow(0).get("Pin"));
	}
	
	public void applyGiftCard() {
		applyButton = findElementByName("submitOptional");
		clickOn(applyButton);
	}
	
	public ReviewOrderPage cancel() throws Exception {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
		return new ReviewOrderPage();
	}
	
	public boolean isErrorDisplayed() throws Exception {
		return isElementPresentBySelenium("css=.errorHeader");
	}
	
	
}
