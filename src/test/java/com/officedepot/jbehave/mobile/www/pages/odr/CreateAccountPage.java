package com.officedepot.jbehave.mobile.www.pages.odr;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class CreateAccountPage extends BasePage {

	private WebElement memberNumberField;
	private WebElement cancelButton;
	private WebElement nextButton;
	private WebElement answerOptionButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Create Online Account");
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public void inputMemberNumber(String number) {
		memberNumberField = findElementById("memberId");
		typeTextBox(memberNumberField, number);
	}

	public void cancelActive() {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
	}

	public void submitActive() {
		nextButton = findElementById("jst_linkaccount");
		clickOn(nextButton);
	}

	public boolean isQuestionLightBoxDisplayed() {
		return isTextPresentInElement(By.cssSelector("#boxes_content"), "Do you have an Office Depot online shopping account?");
	}

	public void chooseOptionForQuestion(String option) {
		answerOptionButton = findElementByXpath("(//a[contains(text(),'" + option + "')])[2]");
		clickOn(answerOptionButton);
	}

}
