package com.officedepot.jbehave.mobile.www.pages.others;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class FullSiteHomePage extends BasePage {

	private WebElement goBackLink;

	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium("link=Click here to go back to mobile site");
	}

	public void goBackToMobileSite() {
		goBackLink = this.findElementByLinkText("Click here to go back to mobile site");
		clickOn(goBackLink);
	}

}
