package com.officedepot.jbehave.mobile.www.steps.account;

import org.apache.commons.lang3.RandomStringUtils;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.account.AddNewShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.account.ChangePasswordPage;
import com.officedepot.jbehave.mobile.www.pages.account.ChangeSecurityQuestionsPage;
import com.officedepot.jbehave.mobile.www.pages.account.EditPaymentInfoPage;
import com.officedepot.jbehave.mobile.www.pages.account.EditShippingAddressPage;
import com.officedepot.jbehave.mobile.www.pages.account.MergeAccountsOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.account.MyAccountPage;
import com.officedepot.jbehave.mobile.www.pages.account.MyProfilePage;
import com.officedepot.jbehave.mobile.www.pages.account.PaymentInfoEnteringPage;
import com.officedepot.jbehave.mobile.www.pages.account.PaymentSettingsPage;
import com.officedepot.jbehave.mobile.www.pages.account.ShippingOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class AccountSteps extends BaseStep {

	private HomePage homePage;
	private MyAccountPage myAccountPage;
	private PaymentSettingsPage paymentSettingsPage;
	private PaymentInfoEnteringPage paymentInfoEnteringPage;
	private ShippingOptionsPage shippingOptionsPage;
	private EditShippingAddressPage editShippingAddressPage;
	private ExamplesTable exceptedShippingInfo;
	private EditPaymentInfoPage editPaymentInfoPage;
	private AddNewShippingAddressPage addNewShippingAddressPage;
	private ChangePasswordPage changePasswordPage;
	private ChangeSecurityQuestionsPage changeSecurityQuestionsPage;
	private MyProfilePage myProfilePage;
	private MergeAccountsOptionsPage mergeAccountsPage;
	private boolean isCreditCardExist = false;

	private String exceptedCardAlias;

	@When("I go manage my account")
	public void goManageMyAccount() throws Exception {
		homePage = new HomePage();
		myAccountPage = homePage.goToMyAccount();
	}

	@When("I choose to edit my payment information")
	public void choosePaymentInformation() throws Exception {
		paymentSettingsPage = myAccountPage.selectPaymentInfo();
	}

	@When("I add a new credit card")
	public void enterNewCreditCard() throws Exception {
		paymentInfoEnteringPage = paymentSettingsPage.enterNewCard();
	}

	@When("I fill with the following credit card information: $infoTable")
	public void enterValidCardInformation(ExamplesTable infoTable) throws Exception {
		exceptedCardAlias = RandomStringUtils.randomAlphanumeric(6);
		paymentInfoEnteringPage.inputPaymentInfoField("nick name", exceptedCardAlias);
		paymentInfoEnteringPage.fillPaymentInfo(infoTable);
	}

	@When("I submit the new credit card")
	public void submit() throws Exception {
		paymentInfoEnteringPage.submit();
	}

	@Then("I should see the new credit card has been successfully added")
	public void newCardHasBeenSuccessfullyAdded() throws Exception {
		Assert.assertTrue("---Fail to add new card!---", myAccountPage.findAddedAlias(exceptedCardAlias.toUpperCase()));
	}

	@When("I delete this credit card")
	public void deleteAddedCreditCard() throws Exception {
		if (isCreditCardExist) {
			editPaymentInfoPage.deleteCreditCard();
		}
	}

	@When("I edit the added credit card")
	public void editAddedCreditCard() throws Exception {
		if (paymentSettingsPage.isCreditCardExist()) {
			editPaymentInfoPage = paymentSettingsPage.editDefaultCard();
			isCreditCardExist = true;
		} else {
			isCreditCardExist = false;
		}
	}

	@When("I edit the default credit card")
	public void editDefaultCreditCard() throws Exception {
		if (paymentSettingsPage.isCreditCardExist()) {
			editPaymentInfoPage = paymentSettingsPage.editDefaultCard();
			isCreditCardExist = true;
		} else {
			isCreditCardExist = false;
		}
	}

	@Then("I should see the credit card has been successfully deleted")
	public void cardCanBeSuccessfullyDeleted() throws Exception {
		if (isCreditCardExist) {
			Assert.assertTrue("---Fail to delete added new card!---", paymentSettingsPage.isDeleteCardSuccessful());
		}
	}

	@Then("I should navigate on my account page")
	public void iShouldNavigateToMyAccountPage() throws Exception {
		if (myAccountPage == null) {
			myAccountPage = new MyAccountPage();
		}
		Assert.assertTrue("----- Failed to navigate on my account page!---", myAccountPage.isNavigateOnThisPage());
	}

	@When("I cancel the shipping infomation edit")
	public void iCancelShippingInfoEdit() throws Exception {
		shippingOptionsPage.cancelEditShippingInfo();
	}

	@When("I edit my shipping information")
	public void iEditMyShoppingInformation() throws Exception {
		shippingOptionsPage = myAccountPage.editShoppingInfo();
	}

	@Then("I should see shipping options page")
	public void iShouldSeeCurrentAddressesPage() throws Exception {
		Assert.assertTrue("---- Failed to navigate on shipping options page!---", shippingOptionsPage.isNavigateOnThisPage());
	}

	@When("I cancel to edit the shipping address")
	public void iCancelEditMyShippingAddress() throws Exception {
		editShippingAddressPage.cancelEdit();
	}

	@When("I edit my shipping address which in use")
	public void iEditMyShippingAddressWhichInUse() throws Exception {
		editShippingAddressPage = shippingOptionsPage.editCurrentAddress();
	}

	@When("I update my shipping address with the following information: $infoTable")
	public void iUpdateMyShippingAddressWithTheFollowingInformation(ExamplesTable infoTable) throws Exception {
		exceptedShippingInfo = editShippingAddressPage.fillShippingAddressInfo(infoTable);
		editShippingAddressPage.submit();
	}

	@Then("I should see my shipping address changed to another one")
	public void iShouldSeeMyShippingAddressUpdated() throws Exception {
		myAccountPage.isNavigateOnThisPage();
		Assert.assertTrue("---- Failed to verify the shipping info!---", !myAccountPage.isShippingInfoExcepted(exceptedShippingInfo));
	}

	@When("I add new shipping address")
	public void iAddNewShippingAddress() throws Exception {
		addNewShippingAddressPage = shippingOptionsPage.addNewShippingAddress();
	}

	@Then("I should navigate to add new shipping address page")
	public void iShouldNavigateToAddNewShippingAddressPage() throws Exception {
		addNewShippingAddressPage = new AddNewShippingAddressPage();
		Assert.assertTrue("---- Failed to navigate on add new shipping address page!---", addNewShippingAddressPage.isNavigateOnThisPage());
	}

	@When("I add new shipping address from my account with following info: $infoTable")
	public void iSubmitTheShippingAddressWithValidInfo(ExamplesTable infoTable) throws Exception {
		exceptedShippingInfo = addNewShippingAddressPage.fillShippingAddressInfo(infoTable);
		addNewShippingAddressPage.submit();
	}

	@Then("I should see my new shipping address added")
	public void iShouldSeeMyNewShippingAddressAdded() throws Exception {
		Assert.assertTrue("---- Failed to navigate on current addresses page!---", shippingOptionsPage.isNavigateOnThisPage());
	}

	@When("I navigate to my profile page")
	public void iNavigateToMyProfilePage() throws Exception {
		myProfilePage = myAccountPage.goToMyProfile();
	}

	@When("I navigate to change password page")
	public void iNavigateToChangePasswordPage() throws Exception {
		changePasswordPage = myProfilePage.clickOnChangePasswordLink();
	}

	@Then("I should see change password page")
	public void iShouldSeeChangePasswordPage() throws Exception {
		Assert.assertTrue("---Failed navigate to change password page!---", changePasswordPage.isNavigateOnThisPage());
	}

	@When("I change my password with \"$newPassword\"")
	public void iChangeMyPassword(String newPassword) throws Exception {
		String oldPassword = (String) getParameter("current password");
		changePasswordPage.inputPasswordInfo(oldPassword, newPassword);
		changePasswordPage.clickOnUpdateButton();
	}

	@Then("I should be noticed that the password should contains at least 8 characters")
	public void iShouldBeNoticedThatPasswordShouldContains8Characters() throws Exception {
		Assert.assertTrue("--- Failed to see error message! ---", changePasswordPage.isMessageDisplayed("Password must contain: 8-10 characters, a capitalized letter and at least one number"));
	}

	@Then("I should be noticed that the password should contains at least one uppercase letter and one number")
	public void iShouldBeNoticedThatPasswordShouldContainsAtLeastOneNumber() throws Exception {
		Assert.assertTrue("--- Failed to see error message! ---",
				changePasswordPage.isMessageDisplayed("Password must contain 8-10 characters with at least one uppercase letter, at least one lower case letter and one number"));
	}

	@Then("I should see change password successful message and back to my account page")
	public void iShouldSeeChangePasswordSuccessfulMessageAndBackToMyAccountPage() throws Exception {
		if (myAccountPage == null) {
			myAccountPage = new MyAccountPage();
		}
		Assert.assertTrue("---Failed to see change password successful message!---", myAccountPage.isChangePasswordSuccessfulMessageOccur());
		Assert.assertTrue("---Failed back to my account page!---", myAccountPage.isNavigateOnThisPage());
	}

	@When("I navigate to change security questions page")
	public void iNavigateToChangeSecurityQuestionsPage() throws Exception {
		changeSecurityQuestionsPage = myProfilePage.clickOnChangeSecurityQuestionLink();
	}

	@Then("I should see change security questions page")
	public void iShouldSeeChangeSecurityQuestionsPage() throws Exception {
		Assert.assertTrue("---Failed navigate to change security questions page!---", changeSecurityQuestionsPage.isNavigateOnThisPage());
	}

	@When("I change my security question")
	public void iChangeMySecurityQuestion() throws Exception {
		changeSecurityQuestionsPage.inputValidSecurityQuestionInfo();
		myAccountPage = changeSecurityQuestionsPage.clickOnUpdateButton();
	}

	@Then("I should see change security questions successful message and back to my account page")
	public void iShouldSeeChangeSecurityQuestionsSuccessfulMessageAndBackToMyAccountPage() throws Exception {
		Assert.assertTrue("---Failed back to my account page!---", myAccountPage.isNavigateOnThisPage());
	}

	@When("I choose to view my store from my account")
	public void iChooseToViewMyStoreFromMyAccount() throws Exception {
		myAccountPage.viewMyStore();
	}

	@When("I choose to track orders from my account")
	public void iChooseToTrackOrdersFromMyAccount() throws Exception {
		myAccountPage.trackOrders();
	}

	@When("I choose to view my rewards from my account")
	public void iChooseToViewMyRewardsFromMyAccount() throws Exception {
		if (myAccountPage == null) {
			myAccountPage = new MyAccountPage();
		}
		myAccountPage.viewMyRewards();
	}

	@When("I choose to skip to merge my accounts")
	public void iChooseToSkipMergeMyAccount() throws Exception {
		if (mergeAccountsPage == null) {
			mergeAccountsPage = new MergeAccountsOptionsPage();
		}
		mergeAccountsPage.skipToMergeAccounts();
	}

}
