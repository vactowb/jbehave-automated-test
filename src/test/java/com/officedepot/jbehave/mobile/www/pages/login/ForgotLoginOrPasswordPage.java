package com.officedepot.jbehave.mobile.www.pages.login;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ForgotLoginOrPasswordPage extends BasePage {

	private WebElement submitButton;
	private WebElement textField;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Forgot Login or Password");
	}

	public void submitForgotLogin() {
		submitButton = findElementsByCSS(".b6.blue").get(1);
		clickOn(submitButton);
	}

	public void submitForgotPassword() {
		submitButton = findElementsByCSS(".b6.blue").get(0);
		clickOn(submitButton);
	}

	public void inputField(String field, String value) {
		if (field.equalsIgnoreCase("first name")) {
			textField = findElementByName("firstName");
		} else if (field.equalsIgnoreCase("last name")) {
			textField = findElementByName("lastName");
		} else if (field.equalsIgnoreCase("email")) {
			textField = findElementByName("email");
		} else if (field.equalsIgnoreCase("login name")) {
			textField = findElementByName("loginName");
		}
		typeTextBox(textField, value);
	}

	public void inputForgotLoginQuestions(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputForgotPasswordQuestion(String loginName) {
		inputField("login name", loginName);
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}
}
