package com.officedepot.jbehave.mobile.www.pages.refinement;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class SearchProductResultsListPage extends BasePage {

	private WebElement refineButton;
	private WebElement paginationButton;
	private WebElement refineBoxOption;

	public void refineTheSearch() {
		refineButton = findElementById("searchRefineButton");
		clickOn(refineButton);
	}

	public boolean isProductNameDisplayedOnPage(String productName) {
		return isTextPresentOnPage(productName);
	}

	public void refineByBrandsPilot() {
		refineBoxOption = findElementByCSS(".jst_searchSubTitle");
		clickOn(refineBoxOption);
		clickOn(findElementByXpath("(//input[@id='rf_1-2'])[2]"));
	}

	public void navigateOnOtherPage(String index) {
		if (Integer.parseInt(index) == 1) {
			paginationButton = findElementByXpath("//div[@id='paginationSlider']/div/div/a");
		} else {
			paginationButton = findElementByXpath("//div[@id='paginationSlider']/div/div/a[" + index + "]");
		}
		clickOn(paginationButton);
	}

}
