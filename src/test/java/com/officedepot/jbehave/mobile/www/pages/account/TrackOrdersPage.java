package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.Map;

import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.login.LoginOrRegisterPage;
import com.officedepot.test.jbehave.BasePage;

public class TrackOrdersPage extends BasePage {

	private WebElement orderInfoField;
	private WebElement loginButton;
	private WebElement submitButton;
	private WebElement cancelButton;
	private String orderNumber;

	private By pageVerifyItem = By.cssSelector("#siteBreadcrumb");

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(pageVerifyItem, "Track Orders");
	}

	public void submit() {
		submitButton = findElementByXpath("//input[@value='Submit']");
		clickOn(submitButton);
	}

	public void cancel() {
		cancelButton = findElementByLinkText("Cancel");
		clickOn(cancelButton);
	}

	public LoginOrRegisterPage loginToTrackOrders() {
		loginButton = findElementByLinkText("Login");
		clickOn(loginButton);
		return new LoginOrRegisterPage();
	}

	public void fillOrderInfo(ExamplesTable infoTable) {
		for (Map<String, String> infoRow : infoTable.getRows()) {
			inputOrderField(infoRow.get("Field"), infoRow.get("Value"));
		}
	}

	public void inputOrderField(String field, String value) {
		if (field.equalsIgnoreCase("order number")) {
			orderInfoField = findElementById("orderNum");
			orderNumber = value;
		}
		if (field.equalsIgnoreCase("phone number")) {
			orderInfoField = findElementById("phoneNumber1-0");
		}
		if (field.equalsIgnoreCase("account number")) {
			orderInfoField = findElementById("acctNum");
		}
		typeTextBox(orderInfoField, value);
	}

	public String getInputtedOrderNumber() {
		return orderNumber;
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

}
