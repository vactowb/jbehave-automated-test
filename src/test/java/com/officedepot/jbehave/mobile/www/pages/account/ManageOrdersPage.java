package com.officedepot.jbehave.mobile.www.pages.account;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.officedepot.jbehave.mobile.www.pages.shoppingcart.CartPage;
import com.officedepot.test.jbehave.BasePage;

public class ManageOrdersPage extends BasePage {

	private WebElement nextLink;
	private WebElement prevLink;
	private WebElement firstLink;
	private WebElement orderLink;
	private List<WebElement> reorderLinks;
	private List<WebElement> cancelOrderLinks;
	private WebElement yesCancelButton;
	private WebElement noCancelButton;
	private WebElement closeCancelBoxButton;

	public boolean isNavigateOnThisPage() {
		return isTextPresentInElement(By.cssSelector("#siteBreadcrumb"), "Manage Orders");
	}

	public OrderDetailsPage viewFirstOrderDetails() {
		orderLink = findElementByXpath("//*[@id='mainContent']/section[2]/div[1]/a");
		clickOn(orderLink);
		return new OrderDetailsPage();
	}

	public String getFirstOrderNumber() {
		orderLink = findElementByXpath("//*[@id='mainContent']/section[2]/div[1]/a");
		return getText(orderLink);
	}

	public String getFirstOrderStatus() {
		if (isElementPresentBySelenium("//*[@id='mainContent']/section[2]/div[2]/p[3]/span[2]")) {
			return getText(findElementByXpath("//*[@id='mainContent']/section[2]/div[2]/p[3]/span[2]"));
		} else {
			return getText(findElementByXpath("//*[@id='mainContent']/section[2]/div[2]/p[2]/span[2]"));
		}
	}

	public void cancelOrder(int index) {
		cancelOrderLinks = findElementsByLinkText("Cancel Order");
		clickOn(cancelOrderLinks.get(index));
	}

	public boolean isOrderCanBeCancelled() {
		return isElementPresentBySelenium("link=Cancel Order");
	}

	public void nextPage() {
		nextLink = findElementByCSS("li.next > a");
		clickOn(nextLink);
	}

	public boolean getPrevLink() {
		return isTextPresentInElement(By.cssSelector("li.prev > a"), "Prev");
	}

	public boolean getFirstlink() {
		return isTextPresentInElement(By.cssSelector("li.first > a"), "First");
	}

	public void prevPage() {
		prevLink = findElementByCSS("li.prev > a");
		clickOn(prevLink);
	}

	public void firstPage() {
		firstLink = findElementByCSS("li.first > a");
		clickOn(firstLink);
	}

	public boolean getNextLink() {
		return isTextPresentInElement(By.cssSelector("li.next > a"), "Next");
	}

	public CartPage reorder(int index) throws Exception {
		reorderLinks = findElementsByLinkText("Reorder");
		clickOn(reorderLinks.get(index));
		return new CartPage();
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

	public boolean isOrderOnList(String order) {
		return isTextPresentOnPage(order);
	}

	public void confirmCancelOrder() {
		yesCancelButton = findElementByXpath("(//input[@value='Yes'])[2]");
		clickOn(yesCancelButton);
	}

	public void rejectCancelOrder() {
		noCancelButton = findElementByCSS("#boxes_content > div.t_left > div.otd_yes_no > input.jst_cancel_order_no.button");
		clickOn(noCancelButton);
	}

	public void closeCancelOrderBox() {
		closeCancelBoxButton = findElementByCSS("#boxes_close");
		clickOn(closeCancelBoxButton);
	}

	public boolean isCancelOrderBoxDisplayed() {
		return isElementVisibleBySelenium("css=#boxesTopContent");
	}

}
