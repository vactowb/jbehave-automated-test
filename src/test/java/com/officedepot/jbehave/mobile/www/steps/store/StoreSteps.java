package com.officedepot.jbehave.mobile.www.steps.store;

import static org.junit.Assert.assertTrue;
import junit.framework.Assert;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.deals.DealsPage;
import com.officedepot.jbehave.mobile.www.pages.deals.ListViewPage;
import com.officedepot.jbehave.mobile.www.pages.deals.WeeklyAdPagesPage;
import com.officedepot.jbehave.mobile.www.pages.deals.WeeklyAdsPage;
import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.store.StoreDetailsPage;
import com.officedepot.jbehave.mobile.www.pages.store.StoreLocatorPage;
import com.officedepot.jbehave.mobile.www.pages.store.StoreNearPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class StoreSteps extends BaseStep {

	private PageHeader pageHeader;
	private HomePage homePage;
	private StoreLocatorPage storeLocatorPage;
	private StoreNearPage storeNearPage;
	private StoreDetailsPage storeDetailsPage;
	private DealsPage dealsPage;
	private WeeklyAdsPage weeklyAdsPage;
	private String storeNumber;
	private WeeklyAdPagesPage weeklyAdPagesPage;
	private ListViewPage listViewPage;

	@When("I request to find store from header")
	public void findStoresFromHeader() throws Exception {
		pageHeader = new PageHeader();
		storeLocatorPage = pageHeader.findStores();
	}

	@When("I request to find store from home page")
	public void findStoresFromHomePage() throws Exception {
		homePage = new HomePage();
		storeLocatorPage = homePage.findStore();
	}

	@Then("I should navigate on store locator page")
	public void displayFindStoresPage() throws Exception {
		if (storeLocatorPage == null) {
			storeLocatorPage = new StoreLocatorPage();
		}
		assertTrue("---Fail to redirect find stores page!---", storeLocatorPage.isNavigateOnThisPage());
	}

	@When("I provide the zipcode: \"$zip\"")
	public void inputZipCode(String zip) throws Exception {
		storeLocatorPage.inputLocation(zip);
	}

	@When("I provide city and state: \"$location\"")
	public void inputCityAndState(String location) throws Exception {
		storeLocatorPage.inputLocation(location);
	}

	@When("I find stores")
	public void findStore() throws Exception {
		storeNearPage = storeLocatorPage.findStores();
	}

	@Then("I should see the store list which near \"$location\"")
	public void displayStoreList(String location) throws Exception {
		assertTrue("---Fail to redirect store near page!---", storeNearPage.isNavigateOnThisPage());
		assertTrue("---Fail to list the correct stores!---", storeNearPage.isStoreNearTheLocation(location));
	}

	@When("I check details of the first store which is not my store")
	public void checkStoreDetail() throws Exception {
		if (storeNearPage == null) {
			storeNearPage = new StoreNearPage();
		}
		storeDetailsPage = storeNearPage.viewDetails(storeNearPage.findFirstNonMyStore() - 1);
		storeNumber = storeDetailsPage.getStoreNumber();
	}

	@Then("I should navigate on store details page")
	public void displayStoreDetail() throws Exception {
		if (storeDetailsPage == null) {
			storeDetailsPage = new StoreDetailsPage();
		}
		assertTrue("---Fail to redirect store detail page!---", storeDetailsPage.isNavigateOnThisPage());
	}

	@Then("I should be noticed that the store has been successfully set as the default store")
	public void successfullySetAsDefaultStore() throws Exception {
		assertTrue("---Fail to set the store as default store!---", storeDetailsPage.isStoreSetAsMyStore() && storeDetailsPage.isMyStore());
	}

	@Then("I should be noticed this store is my store")
	public void isMyDefaultStore() throws Exception {
		assertTrue("---Fail to set the store as default store!---", storeDetailsPage.isMyStore());
	}

	@Then("I should be noticed that the zip or city state is invalid")
	public void zipCityStateInvalid() throws Exception {
		assertTrue("---Fail to see the invalid info!---", storeLocatorPage.displayErrorMessage("Invalid zip or city, state. Please try again."));
	}

	@When("I view weekly ads of the first store")
	public void viewWeeklyAds() throws Exception {
		weeklyAdsPage = storeNearPage.viewWeeklyAds();
	}

	@Then("I should navigate on weekly ads page")
	public void iShouldNavigateOnWeeklyAdsPage() throws Exception {
		if (weeklyAdsPage == null) {
			weeklyAdsPage = new WeeklyAdsPage();
		}
		assertTrue("---Fail to redirect weekly ads page!---", weeklyAdsPage.isNavigateOnThisPage());
	}

	@Then("I should navigate on deals page")
	public void dispalyDealPage() throws Exception {
		if (dealsPage == null) {
			dealsPage = new DealsPage();
		}
		assertTrue("---Fail to redirect store deal page!---", dealsPage.isNavigateOnThisPage());
	}

	@When("I find a stor by current location")
	public void findStoreByLocation() throws Exception {
		storeLocatorPage.findCurrentStore();
	}

	@Then("I should be noticed that these are no store around this location")
	public void getErrorMessage() throws Exception {
		assertTrue("---Fail to get error message!---", storeLocatorPage.displayErrorMessage("There are no Office Depot stores within 100 miles of your entered search criteria"));
	}

	@When("I set a store as my store")
	public void iSetAStoreAsMyStore() throws Exception {
		storeNumber = storeNearPage.setAsMyStore(storeNearPage.findFirstNonMyStore());
	}

	@Then("I shoulde see my store is the selected store before")
	public void iShouldSeeMyStoreIsTheSelected() throws Exception {
		Assert.assertEquals("--- My store is not the selected store before! ---", storeNumber, storeDetailsPage.getStoreNumber());
	}

	@When("I request to change my store from store details page")
	public void iRequestToChangeMyStoreFromStoreDetails() throws Exception {
		storeLocatorPage = storeDetailsPage.changeMyStore();
	}

	@When("I set the store as my store from store details page")
	public void iSetAsMyStoreFromStoreDetails() throws Exception {
		storeDetailsPage.setAsMyStore();
	}

	@When("I check details of my store from stores near list")
	public void iCheckDetailsOfMyStore() throws Exception {
		storeNearPage.viewDetails(storeNearPage.findMyStore() - 1);
	}

	@When("I request to refine the store search results")
	public void iRequestToRefineTheStoreSearchResult() throws Exception {
		storeNearPage.openRefineBox();
	}

	@Then("I should see the adjust results box")
	public void iShouldSeeTheAdjustResultsBox() throws Exception {
		assertTrue("--- Failed to open refine box! ---", storeNearPage.isRefineBoxDisplayed());
	}

	@When("I refine results with all services")
	public void iRefineResultWithAllService() throws Exception {
		storeNearPage.selectAllRefineService();
		storeNearPage.refineResult();
	}

	@Then("I should be noticed that no results were found")
	public void iShouldBeNoticedThatNoResultsWereFound() throws Exception {
		assertTrue("--- Failed to see the error message! ---", storeNearPage.isMessageDisplayed("The store refinements you selected did not find any results"));
	}

	@When("I view weekly ads from store details page")
	public void iViewWeeklyAdsFromStoreDetails() throws Exception {
		weeklyAdsPage = storeDetailsPage.viewWeeklyAds();
	}

	@When("I view one weekly ad details")
	public void iViewWeeklyAdsPagesDetails() throws Exception {
		weeklyAdPagesPage = weeklyAdsPage.viewWeeklyAd(0);
	}

	@Then("I should navigate on weekly ad pages page")
	public void iShouldSeeTheWeeklyAdsPagesDetails() throws Exception {
		assertTrue("--- Failed to navigate on weekly ad pages page! ---", weeklyAdPagesPage.isNavigateOnThisPage());
	}

	@When("I view the list of items which on ad page")
	public void iViewTheListOfItemsWhichOnAdPage() throws Exception {
		listViewPage = weeklyAdPagesPage.viewListOfItemsOnPage();
	}

	@Then("I should navigate on list view page")
	public void iShouldNavigateOnListViewPage() throws Exception {
		assertTrue("--- Failed to navigate on list view page! ---", listViewPage.isNavigateOnThisPage());
	}

}
