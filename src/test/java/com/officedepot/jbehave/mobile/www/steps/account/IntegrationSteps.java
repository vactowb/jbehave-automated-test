package com.officedepot.jbehave.mobile.www.steps.account;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.account.MergeAccountsOptionsPage;
import com.officedepot.jbehave.mobile.www.pages.account.MergeVerifyAccountPage;
import com.officedepot.jbehave.mobile.www.pages.account.MyProfilePage;
import com.officedepot.jbehave.mobile.www.pages.checkout.ThankYouPage;
import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class IntegrationSteps extends BaseStep {

	private MergeAccountsOptionsPage mergeAccountsOptionsPage;
	private MergeVerifyAccountPage mergeVerifyAccountPage;
	private RegisterPage registerPage;
	private ThankYouPage thankYouPage;
	private MyProfilePage myProfilePage;

	@Then("I should navigate on merge accounts options page")
	public void iShouldNavigateOnMergeAccountsOptionsPage() throws Exception {
		if (mergeAccountsOptionsPage == null) {
			mergeAccountsOptionsPage = new MergeAccountsOptionsPage();
		}
		Assert.assertTrue("---Fail to navigate on merge accounts options page!---", mergeAccountsOptionsPage.isNavigateOnThisPage());
	}

	@When("I choose to combine OMX account from my profile page")
	public void iChooseToMergeAccountFromMyProfile() throws Exception {
		if (myProfilePage == null) {
			myProfilePage = new MyProfilePage();
		}
		mergeVerifyAccountPage = myProfilePage.combineOMXAccount();
	}

	@When("I choose to keep my accounts seperate from merge accounts options page")
	public void iChooseToKeepMyAccountsSeperateFromMergeAccountsOptionsPage() throws Exception {
		mergeAccountsOptionsPage.skipToMergeAccounts();
	}

	@When("I choose to merge my accounts from merge accounts options page")
	public void iChooseToMergeMyAccountsFromMergeAccountsOptionsPage() throws Exception {
		mergeVerifyAccountPage = mergeAccountsOptionsPage.mergeAccounts();
	}

	@Then("I should navigate on verify account page of merge accounts")
	public void iShouldNavigateOnVerifyAccountPageOfMergeAccounts() throws Exception {
		Assert.assertTrue("---Fail to navigate on verify account page of merge accounts!---", mergeVerifyAccountPage.isNavigateOnThisPage());
	}

	@When("I cancel to verify account from verify account page of merge accounts")
	public void iCancelToMergeMyAccountsFromVerifyAccountsPage() throws Exception {
		mergeVerifyAccountPage.cancel();
	}

	@When("I login with the following account to merge: $table")
	public void iLoginWithTheFollowingOMXAccountToMerge(ExamplesTable table) throws Exception {
		mergeVerifyAccountPage.fillAccountInfo(table);
	}

	@Then("I should be noticed that the login account is invalid")
	public void iShouldBeNoticedThatTheOmxAccountIsInvalid() throws Exception {
		Assert.assertTrue("---Fail to see error message!---", mergeVerifyAccountPage.isMessageDisplayed("either your Login Name or Password is invalid"));
	}

	@Then("I should be noticed to create OD account to access your OMX history and account information")
	public void iShouldBeNoticedToCreateODAccount() throws Exception {
		if (registerPage == null) {
			registerPage = new RegisterPage();
		}
		Assert.assertTrue("---Fail to see message!---", registerPage.isMessageDisplayed("Create an Office Depot account to access your Office Max history and account information"));
	}

	@Then("I should see merge accounts section is displayed on thank you page")
	public void iShouldSeeMergeAccountsSectionOnThankYouPage() throws Exception {
		if (thankYouPage == null) {
			thankYouPage = new ThankYouPage();
		}
		Assert.assertTrue("---Failed to see merge accounts section on thank you page!---", thankYouPage.isMergeAccountsSectionDisplayed());
	}

	@Then("I should see all fields are pre-filled by OMX account info on registration page")
	public void iShouldAllFieldsArePrefilledByOMXInfo() throws Exception {
		Assert.assertTrue("---All fields are not pre-filled!---",
				registerPage.isFieldPrefilled("first name") && registerPage.isFieldPrefilled("last name") && registerPage.isFieldPrefilled("zip code") && registerPage.isFieldPrefilled("email"));
	}

	@When("I request to merge accouts from thank you page")
	public void iRequestToMergeAccountsFromThankYouPage() throws Exception {
		mergeAccountsOptionsPage = thankYouPage.mergeAccounts();
	}

}
