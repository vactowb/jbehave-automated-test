package com.officedepot.jbehave.mobile.www.pages.paypal;


import com.officedepot.jbehave.mobile.www.pages.register.RegisterPage;
import com.officedepot.test.jbehave.BasePage;

public class PayPalPage extends BasePage {

	public void loginPaypal(String account, String password) {
		typeTextBox(findElementById("email"), account);
		typeTextBox(findElementById("password"), password);
		clickOn(findElementById("login"));
	}
	
	public RegisterPage processCheckOut() {
		clickOn(findElementById("continue"));
		seleniumWaitForPageToLoad(30000);
		return new RegisterPage();
	}
}
