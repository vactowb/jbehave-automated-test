package com.officedepot.jbehave.mobile.www.steps.login;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.home.HomePage;
import com.officedepot.jbehave.mobile.www.pages.home.PageHeader;
import com.officedepot.jbehave.mobile.www.pages.login.ForgotLoginOrPasswordPage;
import com.officedepot.jbehave.mobile.www.pages.login.ForgotPasswordPage;
import com.officedepot.jbehave.mobile.www.pages.login.LoginOrRegisterPage;
import com.officedepot.jbehave.mobile.www.pages.login.SocialAccountLogInPage;
import com.officedepot.test.jbehave.BaseStep;
import com.officedepot.test.jbehave.SeleniumTestContext;
import com.officedepot.test.jbehave.SeleniumUtils;

@Component
@Scope("prototype")
public class LoginSteps extends BaseStep {

	private HomePage homePage;
	private LoginOrRegisterPage loginPage;
	private SocialAccountLogInPage socialAccountLogInPage;
	private String loginUsername;
	private String loginPassword;
	private PageHeader pageHeader;
	private ForgotLoginOrPasswordPage forgotLoginOrPasswordPage;
	private ForgotPasswordPage forgotPasswordPage;

	@Given("I am a www mobile customer with the given account")
	public void iAmAnODCustomerWithGivenAccount(@Named("username") String username, @Named("password") String password) {
		if (username.indexOf("#") != -1) {
			username = username.replace('#', '@');
		} else if (SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("sqs") != -1 || SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("localhost") != -1) {
			if (!(username.equals("auto_reid") || username.equals("user_www"))) {
				username = username + "@test.com";
			}
		}
		loginUsername = username;
		loginPassword = password;
		setParameter("current password", password);
	}

	@When("I access www mobile site")
	public void openHomePage() throws Exception {
		homePage = new HomePage();
		homePage.openHomePage();
	}

	@When("I access home page of www mobile site")
	public void iAccesshomePageOfMSite() throws Exception {
		homePage = new HomePage();
		homePage.openHomePageByDefault();
	}

	@When("I access home page from header")
	public void iAccesshomePageFromHeader() throws Exception {
		pageHeader = new PageHeader();
		pageHeader.openHomePage();
	}

	@Then("I should see the home page of mobile site")
	public void checkIfInHomePage() throws Exception {
		if (homePage == null) {
			homePage = new HomePage();
		}
		assertTrue("--- Failed to navigate on home page! ---", homePage.isNavigateOnThisPage());
	}

	@When("I return the home page and log out")
	public void openHomePageAndLogOut() throws Exception {
		homePage = new HomePage();
		homePage.openHomePage();
	}

	@When("I go to login page from header")
	public void goToLoginPage() throws Exception {
		pageHeader = new PageHeader();
		loginPage = pageHeader.enterLoginOrRegisterPage();
	}

	@When("I go to login page from home page")
	public void goToLoginPageFromHomePage() throws Exception {
		homePage = new HomePage();
		loginPage = homePage.loginRegister();
	}

	@When("I login with my account")
	public void iLoginWithMyAccount() throws Exception {
		if (loginPage == null) {
			loginPage = new LoginOrRegisterPage();
		}
		loginPage.loginWithAccount(loginUsername, loginPassword);
	}

	@When("I login with my account from home page")
	public void iLoginWithMyAccountFromHomePage() throws Exception {
		homePage.login(loginUsername, loginPassword);
	}

	@Then("I should login successfully")
	public void iLoginSuccessfully() throws Exception {
		assertTrue("---- Failed to login! -----", pageHeader.isOptionPresentOnMenu("Logout"));
		pageHeader.openMainNavigator();
	}

	@When("I login with wrong username password")
	public void loginWithWrongUserNameOrPassword() throws Exception {
		loginPage.loginWithAccount(SeleniumUtils.giveUniqueIdAfter("").substring(0, 8), SeleniumUtils.giveUniqueIdAfter("").substring(0, 8));
	}

	@Then("I see the error message for invalid login name")
	public void checkIfThereIsErrorMessageForInvalidLoginName() throws Exception {
		assertTrue(loginPage.isGotTheErrorMessageForInvalidLoginName());
	}

	@When("I login with empty username password")
	public void loginWithEmptyUserName() throws Exception {
		loginPage.loginWithAccount("", "");
	}

	@Then("I see the error message for empty login name")
	public void checkIfThereIsErrorMessageForEmptyLoginName() throws Exception {
		assertTrue(loginPage.isGotTheErrorMessageForEmptyLoginName());
	}

	@Given("I am a OD customer with gmail account linked to my OD account")
	public void withGmailAccountLinkedToODAccount() throws Exception {
		homePage = new HomePage();
		pageHeader = new PageHeader();
		homePage.openHomePage();
		loginPage = pageHeader.enterLoginOrRegisterPage();
	}

	@When("I login with the following social account: $accountDetails")
	public void socialLogIn(ExamplesTable accountDetails) throws Exception {
		socialAccountLogInPage = loginPage.socialLogIn(accountDetails);
	}

	@When("I close the social login window")
	public void iCloseTheSocialLoginWindow() throws Exception {
		socialAccountLogInPage.closeWindow();
	}

	@Then("I should social login successfully")
	public void iShouldLoginSuccessfully() throws Exception {
		Assert.assertTrue("---Fali to socail log in!---", socialAccountLogInPage.isSuccessForLogIn());
	}

	@Then("I should get the error message for social log in")
	public void iShouldNotLoginSuccessfully() throws Exception {
		Assert.assertTrue("---Fali to get error message in socail log in!---", socialAccountLogInPage.dispalyErrorMessage());
	}

	@When("I request to find my username back from login page")
	public void iRequestToFindMyUsernameBack() throws Exception {
		forgotLoginOrPasswordPage = loginPage.forgotLoginOrPass();
	}

	@When("I request to find my password back from login page")
	public void iRequestToFindMyPasswordBack() throws Exception {
		forgotLoginOrPasswordPage = loginPage.forgotLoginOrPass();
	}

	@Then("I should navigate on forgot login or password page")
	public void iShouldSeeForgotLoginOrPasswordPage() throws Exception {
		Assert.assertTrue("---Fali to navigate on forgot login or password page!---", forgotLoginOrPasswordPage.isNavigateOnThisPage());
	}

	@When("I submit with the following username question information: $infoTable")
	public void iSubmitWithInvaildUsernameQuestionInformation(ExamplesTable infoTable) throws Exception {
		forgotLoginOrPasswordPage.inputForgotLoginQuestions(infoTable);
		forgotLoginOrPasswordPage.submitForgotLogin();
	}

	@When("I submit with invaild password question information")
	public void iSubmitWithInvaildPasswordQuestionInformation() throws Exception {
		forgotLoginOrPasswordPage.inputForgotPasswordQuestion("invalid_login");
		forgotLoginOrPasswordPage.submitForgotPassword();
	}

	@Then("I should be noticed that my login name is invalid")
	public void iShouldBeNoticedThatMyLoginNameIsInvaild() throws Exception {
		Assert.assertTrue(forgotLoginOrPasswordPage.isMessageDisplayed("Your login name is invalid"));
	}

	@Then("I should be noticed that there are no users matching")
	public void iShouldBeNoticedThatThereAreNoUsersMatching() throws Exception {
		Assert.assertTrue(forgotLoginOrPasswordPage.isMessageDisplayed("There are no Users matching the Email Address and FirstName/LastName"));
	}

	@When("I submit with vaild password question information")
	public void iSubmitWithVaildPasswordQuestionInformation(@Named("username") String username) throws Exception {
		if (username.indexOf("#") != -1) {
			username = username.replace('#', '@');
		} else if (SeleniumTestContext.getInstance().getTargetBaseURL().indexOf("sqs") != -1) {
			if (!(username.equals("auto_reid") || username.equals("user_www"))) {
				username = username + "@test.com";
			}
		}
		forgotLoginOrPasswordPage.inputForgotPasswordQuestion(username);
		forgotLoginOrPasswordPage.submitForgotPassword();
	}

	@Then("I should be noticed that the change login name email successfully sent")
	public void iShouldBeNoticedThatEmailSuccessfullySent() throws Exception {
		Assert.assertTrue(forgotLoginOrPasswordPage.isMessageDisplayed("Email Successfully Sent"));
	}

	@Then("I should navigate on forgot password page")
	public void iShouldSeeForgotPasswordPage() throws Exception {
		forgotPasswordPage = new ForgotPasswordPage();
		Assert.assertTrue("--- Failed to navigate on forgot password page! ---", forgotPasswordPage.isNavigateOnThisPage());
	}

	@When("I submit with vaild security question answer")
	public void iSubmitWithVaildSecurityQuestionAnswer() throws Exception {
		forgotPasswordPage.submitQuestionAnswer("test");
	}

	@Then("I shoulde be noticed that the change password email successfully sent")
	public void iShouldeBeNoticedThatTheChangePasswordEmailSuccessfullySent() throws Exception {
		Assert.assertTrue(forgotPasswordPage.isMessageDisplayed("Email Successfully Sent"));
	}

	@When("I submit with invaild security question answer")
	public void iSubmitWithInvaildSecurityQuestionAnswer() throws Exception {
		forgotPasswordPage.submitQuestionAnswer("123");
	}

	@Then("I should be noticed that my answer is incorrect")
	public void iShouldBeNoticedThatMyAnswerIsIncorrect() throws Exception {
		Assert.assertTrue(forgotLoginOrPasswordPage.isMessageDisplayed("Please verify your answer"));
	}

}
