package com.officedepot.jbehave.mobile.www.steps.list;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.officedepot.jbehave.mobile.www.pages.account.MyAccountPage;
import com.officedepot.jbehave.mobile.www.pages.csl.CreateNewListPage;
import com.officedepot.jbehave.mobile.www.pages.csl.CreateShoppingListPage;
import com.officedepot.jbehave.mobile.www.pages.csl.DeleteShoppingListConfirmPage;
import com.officedepot.jbehave.mobile.www.pages.csl.SelectShoppingListPage;
import com.officedepot.jbehave.mobile.www.pages.csl.ShoppingListDetailPage;
import com.officedepot.jbehave.mobile.www.pages.csl.ShoppingListPage;
import com.officedepot.jbehave.mobile.www.pages.sku.SkuDetailsPage;
import com.officedepot.test.jbehave.BaseStep;

@Component
@Scope("prototype")
public class CustomShoppingListSteps extends BaseStep {

	private SelectShoppingListPage selectShoppingListPage;
	private ShoppingListDetailPage shoppingListDetailPage;
	private ShoppingListPage shoppingListPage;
	private DeleteShoppingListConfirmPage deleteShoppingListConfirmPage;
	private MyAccountPage myAccountPage;
	private SkuDetailsPage skuDetailsPage;
	private CreateShoppingListPage createShoppingListPage;
	private CreateNewListPage createNewListPage;
	private String expectedListName;
	private int entryQty;

	@When("I request view my shopping list from my account")
	public void viewMyShoppingList() throws Exception {
		myAccountPage = new MyAccountPage();
		myAccountPage.viewMyShoppingListPage();
	}

	@When("I create a new shopping list from select shopping list page")
	public void createNewList() throws Exception {
		if (selectShoppingListPage == null) {
			selectShoppingListPage = new SelectShoppingListPage();
		}
		selectShoppingListPage.fillListInfo();
		shoppingListDetailPage = selectShoppingListPage.createNewList();
	}

	@When("I create a new shopping list with no info input from select shopping list page")
	public void createNewListWitNoInfo() throws Exception {
		if (selectShoppingListPage == null) {
			selectShoppingListPage = new SelectShoppingListPage();
		}
		selectShoppingListPage.createNewList();
	}

	@When("I request to create a new list for the added sku from select shopping list lighting box")
	public void createNewListFromBox() throws Exception {
		if (skuDetailsPage == null) {
			skuDetailsPage = new SkuDetailsPage();
		}
		createShoppingListPage = skuDetailsPage.createNewListFromBox();
	}

	@Then("I should navigate on create shopping list page")
	public void checkIfInCreateListPage() throws Exception {
		assertTrue("--- Failed to navigate on create shopping list page! ---", createShoppingListPage.isNavigateOnThisPage());
	}

	@Then("I should navigate on the shopping list details page")
	public void checkIfInListDetailPage() throws Exception {
		if (shoppingListDetailPage == null) {
			shoppingListDetailPage = new ShoppingListDetailPage();
		}
		assertTrue("--- Failed to navigate on my shopping list details page! ---", shoppingListDetailPage.isNavigateOnThisPage());
	}

	@Then("I should navigate on select shopping list page")
	public void checkIfInSelectShoppingListPage() throws Exception {
		if (selectShoppingListPage == null) {
			selectShoppingListPage = new SelectShoppingListPage();
		}
		assertTrue("--- Failed to navigate on select shopping list page! ---", selectShoppingListPage.isNavigateOnThisPage());
	}

	@Then("I should see the shopping list page")
	public void checkIfInShoppingListPage() throws Exception {
		shoppingListPage = new ShoppingListPage();
		assertTrue(shoppingListPage.isNavigateOnThisPage());
	}

	@When("I add all the entries of list into cart")
	public void addAllSkusIntoCart() throws Exception {
		shoppingListDetailPage.addAllEntriesIntoCart();
	}

	@When("I update the list with new name and comments")
	public void updateTheListWithNewNameAndComments() throws Exception {
		shoppingListDetailPage.updateListWithNewNameAndComments();
	}

	@Then("I should see the list name has been changed as expected")
	public void checkIfListNameHasBeenChangedAsExpected() throws Exception {
		assertTrue("--- Failed to assert the list name! ---", shoppingListDetailPage.isListNameChangedAsExpected());
	}

	@When("I update the reminder date of shopping list")
	public void updateReminderDateInCSL() throws Exception {
		shoppingListDetailPage.updateReminderDate();
	}

	@Then("I should see the reminder has been updated")
	public void checkIfReminderHasBeenUpdated() throws Exception {
		assertTrue("--- Failed to assert the reminder date! ---", shoppingListDetailPage.isReminderDateUpadted());
	}

	@When("I quick add sku \"$skuId\" with qty \"$qty\" into list")
	public void quickAddSkuWithQTY(String skuId, String qty) throws Exception {
		shoppingListDetailPage.quickOrderEntry(skuId, qty);
	}

	@Then("I should see the sku has been added")
	public void checkIfTheSkuHasBeenAdded() throws Exception {
		assertTrue("--- Failed to assert sku qty! ---", shoppingListDetailPage.isEntryAddedSuccessful(entryQty));
	}

	@Then("I see the no list prompt")
	public void checkIfNoListPromptExisted() throws Exception {
		shoppingListPage.isShoppingListEmpty();
	}

	@When("I request to edit the shopping list")
	public void iRequestToEditCSL() throws Exception {
		shoppingListDetailPage.editShoppingList();
	}

	@Then("I should see the edit shopping list lighting box")
	public void iShouldSeeEditCSLLightingBox() throws Exception {
		assertTrue("--- Failed to see edit shopping list lighting box! ---", shoppingListDetailPage.isLightingBoxDisplayed("Edit"));
	}

	@Then("I should navigate on delete shopping list confirm page")
	public void iShouldNavigateOnDeleteCSLPage() throws Exception {
		assertTrue("--- Failed to navigate on delete shopping list confirm page! ---", deleteShoppingListConfirmPage.isNavigateOnThisPage());
	}

	@When("I request to quick order")
	public void iRequestToQuickOrder() throws Exception {
		entryQty = shoppingListDetailPage.getEntryQty();
		shoppingListDetailPage.quickOrder();
	}

	@When("I cancel to quick order entry")
	public void iCancelToQuickOrder() throws Exception {
		shoppingListDetailPage.cancelQuickOrder();
	}

	@Then("I should see the quick order entry lighting box")
	public void iShouldSeeQuickOrderLightingBox() throws Exception {
		assertTrue("--- Failed to see quick order lighting box! ---", shoppingListDetailPage.isLightingBoxDisplayed("Quick Order Entry"));
	}

	@When("I cancel to add to list from select shopping list page")
	public void iCancelToAddToListFromSelectShoppingListPage() throws Exception {
		selectShoppingListPage.cancelToAddToList();
	}

	@When("I add the sku to a existing list and set it as default from select shopping list page")
	public void iAddToListFromSelectShoppingListPage() throws Exception {
		selectShoppingListPage.selectAnExistingList(1);
		expectedListName = selectShoppingListPage.getListName(1);
		selectShoppingListPage.setDefaultList();
		selectShoppingListPage.addToList();
	}

	@When("I cancel to add to list from select shopping list lighting box")
	public void iCancelToAddToListFromSelectShoppingListLightingBox() throws Exception {
		if (skuDetailsPage == null) {
			skuDetailsPage = new SkuDetailsPage();
		}
		skuDetailsPage.cancelToAddToListFromBox();
	}

	@When("I add the sku to a existing list and set it as default from select shopping list lighting box")
	public void iAddToListFromSelectShoppingListLightingBox() throws Exception {
		if (skuDetailsPage == null) {
			skuDetailsPage = new SkuDetailsPage();
		}
		skuDetailsPage.selectAnExistingList(1);
		expectedListName = skuDetailsPage.getListNameFromBox(1);
		skuDetailsPage.setDefaultList();
		skuDetailsPage.addToListFromBox();
	}

	@When("I request to show more lists from select shopping list lighting box")
	public void iRequestToShowMoreListsFromSelectShoppingListBox() throws Exception {
		selectShoppingListPage = skuDetailsPage.showMoreLists();
	}

	@Then("I should see the default list which is expected from select shopping list lighting box")
	public void iShouldSeeDefaultListFromLightingBox() throws Exception {
		if (skuDetailsPage == null) {
			skuDetailsPage = new SkuDetailsPage();
		}
		assertTrue("--- Failed to see default list from lighting box! ---", skuDetailsPage.isDefaultListExistingFromBox() && skuDetailsPage.getListNameFromBox(0).indexOf(expectedListName) != -1);
	}

	@Then("I should see the default list which is expected from select shopping list page")
	public void iShouldSeeDefaultList() throws Exception {
		assertTrue("--- Failed to see default list from select csl page! ---", selectShoppingListPage.isDefaultListExisting() && selectShoppingListPage.getListName(0).indexOf(expectedListName) != -1);
	}

	@When("I cancel to create shopping list from create shopping list page")
	public void iCancelToCreateShoppingListFromCreateShoppingListPage() throws Exception {
		createShoppingListPage.cancel();
	}

	@When("I create a new shopping list from create shopping list page")
	public void iCreateNewShoppingListFromCreateShoppingListPage() throws Exception {
		createShoppingListPage.fillListDetails();
		createShoppingListPage.createList();
	}

	@When("I create a new shopping list with no info input from create shopping list page")
	public void iCreateNewShoppingListWithNoInfoFromCreateShoppingListPage() throws Exception {
		createShoppingListPage.createList();
	}

	@Then("I should be noticed that list name is required from create shopping list page")
	public void iShouldBeNoticedListNameIsRequiredFromCreateShoppingListPage() throws Exception {
		assertTrue("--- Failed to see error message on create shopping list page! ---", createShoppingListPage.isMessageDisplayed("List Name is required"));
	}

	@Then("I should be noticed that list name is required from select shopping list page")
	public void iShouldBeNoticedListNameIsRequiredFromSelectShoppingListPage() throws Exception {
		assertTrue("--- Failed to see error message on select shopping list page! ---", selectShoppingListPage.isMessageDisplayed("List Name is required"));
	}

	@Then("I should navigate on create new list page")
	public void iShouldNavigateOnCreateNewListPage() throws Exception {
		if (createNewListPage == null) {
			createNewListPage = new CreateNewListPage();
		}
		assertTrue("--- Failed to navigate on create new list page! ---", createNewListPage.isNavigateOnThisPage());
	}

	@When("I create a new shopping list with no info input from create new list page")
	public void iCreateANewShoppingListWithNoInfoInputFromCreateNewListPage() throws Exception {
		createNewListPage.createList();
	}

	@When("I cancel to create a new shopping list with no info input from create new list page")
	public void iCancelCreateANewShoppingListWithNoInfoInputFromCreateNewListPage() throws Exception {
		createNewListPage.cancel();
	}

	@Then("I should be noticed that list name is required from create new list page")
	public void iShouldBeNoticedThatListNameIsRequiredFromCreateNewListPage() throws Exception {
		assertTrue("--- Failed to see error message on select shopping list page! ---", createNewListPage.isMessageDisplayed("List Name is required"));
	}

	@When("I cancel to create shopping list from create new list lighting box")
	public void iCancelToCreateShoppingListFromCreateNewListLightingBox() throws Exception {
		if (skuDetailsPage == null) {
			skuDetailsPage = new SkuDetailsPage();
		}
		skuDetailsPage.cancelToCreateListFromBox();
	}

	@When("I create a new shopping list with no info input from create new list lighting box")
	public void iCreateANewShoppingListWithNoInfoInputFromCreateNewListLightingBox() throws Exception {
		skuDetailsPage.createListFromBox();
	}

	@Then("I should be noticed that list name is required from create new list lighting box")
	public void iShouldBeNoticedThatListNameIsRequiredFromCreateNewListLightingBox() throws Exception {
		assertTrue("--- Failed to see error message on select shopping list page! ---", skuDetailsPage.isMessageDisplayed("List Name is required"));
	}

	@When("I create a new shopping list and set as default from create new list lighting box")
	public void iCreateANewShoppingListAndSetAsDefaultFromCreateNewListLightingBox() throws Exception {
		skuDetailsPage.fillListDetailsFromBox();
		expectedListName = skuDetailsPage.getFilledListName();
		skuDetailsPage.setDefaultList();
		skuDetailsPage.createListFromBox();
	}

	@When("I request to remove with no entry selected")
	public void iRequestToRemoveWithNoEntrySelected() throws Exception {
		shoppingListDetailPage.removeEntry();
	}

	@When("I request to add to cart with no entry selected")
	public void iRequestToAddToCartWithNoEntrySelected() throws Exception {
		shoppingListDetailPage.addToCart();
	}

	@Then("I should be noticed that to select at least one entry")
	public void iShouldBeNoticedThatSelectAtLeastOneEntry() throws Exception {
		assertTrue("--- Failed to see error message on shopping list details page! ---", shoppingListDetailPage.isMessageDisplayed("Please select at least one Entry"));
	}

	@When("I request to remove with a entry from list")
	public void iRequestToRemoveEntry() throws Exception {
		entryQty = shoppingListDetailPage.getEntryQty();
		shoppingListDetailPage.selectEntry(0);
		shoppingListDetailPage.removeEntry();
	}

	@Then("I should see the entry has been removed")
	public void iShouldSeeEntryRemoved() throws Exception {
		assertTrue("--- Failed to remove the entry! ---", shoppingListDetailPage.isEntryRemovedSuccessful(entryQty));
	}
}
