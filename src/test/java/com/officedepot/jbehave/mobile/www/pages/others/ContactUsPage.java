package com.officedepot.jbehave.mobile.www.pages.others;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;

public class ContactUsPage extends BasePage {
	
	private String pageVerifyItem = "css=.icon_phone";
	private WebElement nameField;
	private WebElement emailField;
	private WebElement subjectSelectList;
	private WebElement phoneNumberField;
	private WebElement orderNumberField;
	private WebElement accountNumberField;
	private WebElement messageField;
	private WebElement responseSelectList;
	private WebElement sendEmailButton;
	
	public boolean isNavigateOnThisPage() {
		return isElementPresentBySelenium(pageVerifyItem);
	}
	
	public ContactUsPage clickOnSendEmailButton(){
		sendEmailButton = findElementByCSS(".button.b1.f_right");
		clickOn(sendEmailButton);
		return new ContactUsPage();
	}
	
	public boolean isMessageDisplayed(String message){
		return isTextPresentOnPage(message);
	}
	
	public void inputTheFullItems(String customerName,String emailAddress,String phoneNumber,String orderNumber,String accountNumber,String yourMessageHere){
		nameField = findElementByName("customerName");
		typeTextBox(nameField, customerName);
		emailField = findElementByName("email");
		typeTextBox(emailField, emailAddress);
		subjectSelectList = findElementByName("subject");
		selectOptionInListByText(subjectSelectList, "Catalog Requests");
		phoneNumberField = findElementByName("phone");
		typeTextBox(phoneNumberField, phoneNumber);
		orderNumberField = findElementByName("invoiceNumber");
		typeTextBox(orderNumberField, orderNumber);
		accountNumberField = findElementByName("webAccountNumber");
		typeTextBox(accountNumberField, accountNumber);
		messageField = findElementByName("textMessage");
		typeTextBox(messageField, yourMessageHere);
		responseSelectList = findElementByName("response");
		selectOptionInListByText(responseSelectList, "Yes");		
	}
}

