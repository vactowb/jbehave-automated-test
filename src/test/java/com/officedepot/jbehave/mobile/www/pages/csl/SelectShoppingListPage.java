package com.officedepot.jbehave.mobile.www.pages.csl;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.officedepot.test.jbehave.BasePage;
import com.officedepot.test.jbehave.SeleniumUtils;

public class SelectShoppingListPage extends BasePage {

	private WebElement newListNameField;
	private WebElement newListCommentsField;
	private WebElement addToListButton;
	private List<WebElement> existingListRadioButtons;
	private WebElement createNewListButton;
	private WebElement cancelButton;
	private WebElement setDefaultListCheckBox;

	public boolean isNavigateOnThisPage() {
		return isTextPresentOnPage("Select Shopping List");
	}

	public ShoppingListDetailPage createNewList() throws Exception {
		createNewListButton = findElementByCSS("li.vspace_bottom > input[name='cmd_addItemsToCsl']");
		clickOn(createNewListButton);
		return new ShoppingListDetailPage();
	}

	public void fillListInfo() {
		newListNameField = findElementById("cslName");
		typeTextBox(newListNameField, SeleniumUtils.giveUniqueIdAfter("CSL").substring(0, 9));
		newListCommentsField = findElementById("cslComments");
		typeTextBox(newListCommentsField, SeleniumUtils.giveUniqueIdAfter("CSLComments").substring(0, 15));
	}

	public void selectAnExistingList(int index) throws Exception {
		existingListRadioButtons = findElementsByCSS(".v2_radioLabel");
		clickOn(existingListRadioButtons.get(index));
	}

	public void addToList() {
		addToListButton = findElementByName("cmd_addItemsToCsl");
		clickOn(addToListButton);
	}

	public void cancelToAddToList() {
		cancelButton = findElementByName("cmd_cancel");
		clickOn(cancelButton);
	}

	public void setDefaultList() {
		setDefaultListCheckBox = findElementByCSS("#defaultList");
		clickOn(setDefaultListCheckBox);
	}

	public String getListName(int index) {
		existingListRadioButtons = findElementsByCSS(".v2_radioLabel");
		return getText(existingListRadioButtons.get(index));
	}

	public boolean isDefaultListExisting() {
		existingListRadioButtons = findElementsByCSS(".v2_radioLabel");
		return getText(existingListRadioButtons.get(0)).indexOf("Default") != -1;
	}

	public boolean isMessageDisplayed(String content) {
		return isTextPresentOnPage(content);
	}

}
